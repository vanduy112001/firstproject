<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// api kết nối của dashboard
Route::group(['namespace' => 'Admin', 'middleware' => 'check.api'], function() {
    // cập nhật status vps
    Route::put('/responsive_vps', 'Api\VpsController@index')->name('api.responsive_vps');
    // cập nhật change ip
    Route::put('/change_ip_vps', 'Api\VpsController@change_ip_vps')->name('api.change_ip_vps');
    // cập nhật vps
    Route::put('/update_reset', 'Api\VpsController@update_reset')->name('api.update_reset');
    // thêm vps từ dashboard
    Route::post('/add_vps', 'Api\VpsController@add_vps')->name('api.add_vps');
    // lấy sản phẩm theo customer
    Route::get('/get_product_vps_with_user', 'Api\VpsController@get_product_vps_with_user')->name('api.get_vps_product_with_user');
    // lấy user trong portal
    Route::get('/get_all_user', 'UserController@get_all_user')->name('api.get_all_user');
    // lấy vps trên trong portal
    Route::get('/get_vps', 'Api\VpsController@get_all_vps')->name('api.get_all_vps');
    // lấy vps bằng vmid trong portal
    Route::get('/get_vps_by_vmid', 'Api\VpsController@get_vps_by_vmid')->name('api.get_vps_by_vmid');
    // lấy vps us trên trong portal
    Route::get('/get_vps_us', 'Api\VpsController@get_all_vps_us')->name('api.get_all_vps_us');
    // lấy vps us bằng vmid trong portal
    Route::get('/get_vps_us_by_vmid', 'Api\VpsController@get_vps_us_by_vmid')->name('api.get_vps_us_by_vmid');
    // đổi thông tin vps
    Route::put('/change_vps', 'Api\VpsController@change_vps')->name('api.change_vps');
    // TODO: lay nhung thanh toan chua thanh toan hoac da xac nhan
    Route::get('/get_payment_unpaid', 'Api\PaymentController@get_payment_unpaid')->name('api.get_payment_unpaid');
    // TODO: api confirm thanh toán
    Route::put('/confirm_payment', 'Api\PaymentController@confirm_payment')->name('api.confirm_payment');
    // thêm proxy từ dashboard
    Route::post('/add_proxy', 'Api\VpsController@add_proxy')->name('api.add_proxy');
});



Route::post('/login', 'Admin\Api\LoginController@login')->name('api.login');
Route::post('/test_connection', 'Admin\Api\LoginController@test_login')->name('api.test_login');

Route::group(['namespace' => 'Admin', 'middleware' => 'check.agency_api'], function() {
  Route::get('/check_credit', 'Api\LoginController@check_credit')->name('api.check_credit');
  Route::get('/get_product', 'Api\LoginController@get_product')->name('api.get_product');

  Route::post('/order', 'Api\OrderController@order')->name('api.order');
  Route::put('/vps/action', 'Api\ServicesController@vps_action')->name('api.vps.vps_action');
  Route::put('/nat_vps/action', 'Api\ServicesController@nat_vps_action')->name('api.nat_vps.vps_action');
  Route::put('/hosting/action', 'Api\ServicesController@hosting_action')->name('api.hosting.hosting_action');
  Route::put('/expire', 'Api\OrderController@expire')->name('api.expire');
});

Route::post('/admin/login', 'Admin\Api\LoginController@adminLogin')->name('api.admin.login');
Route::group(['namespace' => 'Admin', 'middleware' => 'check.jwt'], function() {
/**USER */
  Route::get('/admin/listUser', 'Api\UserController@listUser')->name('api.admin.listUser');
  Route::post('/admin/user/createUser', 'Api\UserController@createUser')->name('api.admin.user.createUser');
  Route::put('/admin/user/editUser', 'Api\UserController@editUser')->name('api.admin.user.editUser');
  Route::put('/admin/user/updateCredit', 'Api\UserController@updateCredit')->name('api.admin.user.updateCredit');
  Route::get('/admin/user/getInfoUser/{userId}', 'Api\UserController@getInfoUser')->name('api.admin.user.getInfoUser');
  Route::get('/admin/user/getEditUser/{userId}', 'Api\UserController@getEditUser')->name('api.admin.user.getEditUser');
  Route::get('/admin/user/getHistoryPayment/{userId}', 'Api\UserController@historyPayment')->name('api.admin.user.historyPayment');
  Route::get('/admin/user/getHistoryOrder/{userId}', 'Api\UserController@historyOrder')->name('api.admin.user.historyOrder');
  Route::delete('/admin/user/delete/{userId}', 'Api\UserController@deleteUser')->name('api.admin.user.deleteUser');
/**GROUP USER*/
  Route::get('/admin/group-user/getListGroupUser', 'Api\UserController@listGroupUser')->name('api.admin.groupUser.listGroupUser');
  Route::get('/admin/group-user/listGroupUser', 'Api\UserController@getlistGroupUser')->name('api.admin.groupUser.getlistGroupUser');
  Route::get('/admin/group-user/getDetailGroupUser/{groupUserId}', 'Api\UserController@getDetailGroupUser')->name('api.admin.groupUser.getDetailGroupUser');
  Route::get('/admin/group-user/listUserByGroupUser/{groupUserId}', 'Api\UserController@listUserByGroupUser')->name('api.admin.groupUser.listUserByGroupUser');
  Route::post('/admin/group-user/createGroupUser', 'Api\UserController@createGroupUser')->name('api.admin.groupUser.createGroupUser');
  Route::put('/admin/group-user/editGroupUser', 'Api\UserController@editGroupUser')->name('api.admin.groupUser.editGroupUser');
/**PRODUCT*/
  //GROUPPRODUCT
  Route::get('/admin/group-product/listGroupProductByGroupUser/{groupUserId}', 'Api\ProductController@listGroupProductByGroupUser')->name('api.admin.groupProduct.listGroupProductByGroupUser');
  Route::get('/admin/group-product/detailGroupProduct/{groupProductId}', 'Api\ProductController@detailGroupProduct')->name('api.admin.groupProduct.detailGroupProduct');
  Route::post('/admin/group-product/createGroupProduct', 'Api\ProductController@createGroupProduct')->name('api.admin.groupProduct.createGroupProduct');
  Route::put('/admin/group-product/updateGroupProduct', 'Api\ProductController@updateGroupProduct')->name('api.admin.groupProduct.updateGroupProduct');
  //PRODUCT
  Route::get('/admin/product/getConfigProduct', 'Api\ProductController@getConfigProduct')->name('api.admin.product.getConfigProduct');
  Route::get('/admin/product/getDetailProduct/{productId}', 'Api\ProductController@detailProduct')->name('api.admin.product.getDetailProduct');
  Route::post('/admin/product/createProduct', 'Api\ProductController@createProduct')->name('api.admin.product.createProduct');
});
