<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateServer1607 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( Schema::hasColumn('servers', 'addon_id')) {
            Schema::table('servers', function (Blueprint $table) {
                $table->dropColumn('addon_id');
            });
        }
        if ( Schema::hasColumn('servers', 'addon_qtt')) {
            Schema::table('servers', function (Blueprint $table) {
                $table->dropColumn('addon_qtt');
            });
        }
        if ( Schema::hasColumn('servers', 'expired_id')) {
            Schema::table('servers', function (Blueprint $table) {
                $table->dropColumn('expired_id');
            });
        }
        if ( ! Schema::hasColumn('servers', 'server_name')) {
            Schema::table('servers', function (Blueprint $table) {
                $table->string('server_name')->nullable();
            });
        }
        if ( ! Schema::hasColumn('servers', 'chip')) {
            Schema::table('servers', function (Blueprint $table) {
                $table->string('chip')->nullable();
            });
        }
        if ( ! Schema::hasColumn('servers', 'ram')) {
            Schema::table('servers', function (Blueprint $table) {
                $table->string('ram')->nullable();
            });
        }
        if ( ! Schema::hasColumn('servers', 'disk')) {
            Schema::table('servers', function (Blueprint $table) {
                $table->string('disk')->nullable();
            });
        }
        if ( ! Schema::hasColumn('servers', 'location')) {
            Schema::table('servers', function (Blueprint $table) {
                $table->string('location')->nullable();
            });
        }
        if ( ! Schema::hasColumn('servers', 'amount')) {
            Schema::table('servers', function (Blueprint $table) {
                $table->string('amount')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('servers', function (Blueprint $table) {
            //
        });
    }
}
