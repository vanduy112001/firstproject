<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateGroupProduct2306 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasColumn('group_products', 'type')) {
            Schema::table('group_products', function (Blueprint $table) {
                $table->string('type')->nullable();
            });
        }
        if (! Schema::hasColumn('group_products', 'stt')) {
            Schema::table('group_products', function (Blueprint $table) {
                $table->string('stt')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('group_products', function (Blueprint $table) {
            //
        });
    }
}
