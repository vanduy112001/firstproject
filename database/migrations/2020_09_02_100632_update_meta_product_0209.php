<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMetaProduct0209 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meta_products', function (Blueprint $table) {
            //
        });
        if ( ! Schema::hasColumn('meta_products', 'qtt_email')) {
              Schema::table('meta_products', function (Blueprint $table) {
                  $table->string('qtt_email')->nullable();
              });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta_products', function (Blueprint $table) {
            //
        });
    }
}
