<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDomainProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('domain_products', function (Blueprint $table) {
            $table->string('annually_exp')->nullable();
            $table->string('biennially_exp')->nullable();
            $table->string('triennially_exp')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domain_products', function (Blueprint $table) {
            //
        });
    }
}
