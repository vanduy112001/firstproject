<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMetaProduct20211027 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('meta_products', 'email_config_finish')) {
            Schema::table('meta_products', function (Blueprint $table) {
                $table->bigInteger('email_config_finish')->default(false);
            });
        }
        if (!Schema::hasColumn('servers', 'send_mail_create')) {
            Schema::table('servers', function (Blueprint $table) {
                $table->boolean('send_mail_create')->default(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta_products', function (Blueprint $table) {
            //
        });
    }
}
