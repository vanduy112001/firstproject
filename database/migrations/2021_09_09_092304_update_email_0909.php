<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateEmail0909 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('emails', 'type')){
            Schema::table('emails', function (Blueprint $table) {
                $table->integer('type')->default(1);
            });
        }
        if (!Schema::hasColumn('emails', 'status')) {
            Schema::table('emails', function (Blueprint $table) {
                $table->boolean('status')->default(false);
            });
        }
        if (!Schema::hasColumn('meta_emails', 'type_group')) {
            Schema::table('meta_emails', function (Blueprint $table) {
                $table->boolean('type_group')->default(true);
            });
        }
        if (!Schema::hasColumn('meta_emails', 'time')) {
            Schema::table('meta_emails', function (Blueprint $table) {
                $table->string('time')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('emails', function (Blueprint $table) {
            //
        });
    }
}
