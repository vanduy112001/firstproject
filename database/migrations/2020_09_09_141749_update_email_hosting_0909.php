<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateEmailHosting0909 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasColumn('email_hostings', 'expire_billing_cycle')) {
              Schema::table('email_hostings', function (Blueprint $table) {
                  $table->string('expire_billing_cycle')->nullable();
              });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('email_hostings', function (Blueprint $table) {
            //
        });
    }
}
