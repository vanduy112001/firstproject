<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserDetail09082021 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('users', 'tutorial')){
            Schema::table('users', function (Blueprint $table) {
                $table->boolean('tutorial')->default(false);
            });
        }
        if (!Schema::hasColumn('users', 'marketing')){
            Schema::table('users', function (Blueprint $table) {
                $table->boolean('marketing')->default(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_metas', function (Blueprint $table) {
            //
        });
    }
}
