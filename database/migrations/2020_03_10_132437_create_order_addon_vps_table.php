<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderAddonVpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_addon_vps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('detail_order_id')->nullable();
            $table->bigInteger('vps_id')->nullable();
            $table->bigInteger('ram')->nullable();
            $table->bigInteger('cpu')->nullable();
            $table->bigInteger('disk')->nullable();
            $table->bigInteger('ip')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_addon_vps');
    }
}
