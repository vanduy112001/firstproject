<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateHosting1004 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasColumn('hostings', 'location')) {
            Schema::table('hostings', function (Blueprint $table) {
                $table->string('location')->default('vn');
            });
        }
        if (! Schema::hasColumn('hostings', 'price_override')) {
            Schema::table('hostings', function (Blueprint $table) {
                $table->string('price_override')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hostings', function (Blueprint $table) {
            //
        });
    }
}
