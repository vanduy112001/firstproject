<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateLaptopHosting1422020 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasColumn('hostings', 'addon_qtt')) {
            Schema::table('hostings', function (Blueprint $table) {
                $table->bigInteger('addon_qtt')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hosting', function (Blueprint $table) {
            //
        });
    }
}
