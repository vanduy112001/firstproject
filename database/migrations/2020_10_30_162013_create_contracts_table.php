<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('title')->nullable();
            $table->bigInteger('user_id');
            $table->string('contract_number');
            $table->string('company');
            $table->string('deputy');
            $table->string('deputy_position');
            $table->string('address');
            $table->string('phone');
            $table->string('email');
            $table->string('mst');
            $table->date('date_create')->nullable();
            $table->date('date_end')->nullable();         
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
