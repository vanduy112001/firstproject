<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateServer20211022 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('servers', 'raid')) {
            Schema::table('servers', function (Blueprint $table) {
                $table->string('raid')->nullable();
            });
        }
        if (!Schema::hasColumn('servers', 'server_management')) {
            Schema::table('servers', function (Blueprint $table) {
                $table->string('server_management')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('servers', function (Blueprint $table) {
            //
        });
    }
}
