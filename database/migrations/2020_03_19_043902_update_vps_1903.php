<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateVps1903 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasColumn('vps', 'ma_kh')) {
            Schema::table('vps', function (Blueprint $table) {
                $table->string('ma_kh')->nullable();
            });
        }
        if ( ! Schema::hasColumn('vps', 'description')) {
            Schema::table('vps', function (Blueprint $table) {
                $table->string('description')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vps', function (Blueprint $table) {
            //
        });
    }
}
