<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDomain0505 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (  Schema::hasColumn('domains', 'cmnd_after')) {
            Schema::table('domains', function (Blueprint $table) {
                $table->text('cmnd_after')->nullable()->change();
            });
        }
        if (  Schema::hasColumn('domains', 'cmnd_before')) {
            Schema::table('domains', function (Blueprint $table) {
                $table->text('cmnd_before')->nullable()->change();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domains', function (Blueprint $table) {
            //
        });
    }
}
