<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVietcombanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vietcombanks', function (Blueprint $table) {
            $table->string('sothamchieu')->unique()->nullable();
            $table->string('total')->nullable();
            $table->text('mota')->nullable();
            $table->date('ngaygd')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vietcombanks');
    }
}
