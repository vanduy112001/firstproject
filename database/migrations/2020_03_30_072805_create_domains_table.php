<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDomainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domains', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->bigInteger('detail_order_id');
            $table->bigInteger('user_id');
            $table->bigInteger('product_domain_id');          
            $table->string('billing_cycle');
            $table->string('status');
            $table->string('domain');
            $table->string('domain_ext');
            $table->string('domain_name');
            $table->string('domain_year');
            $table->string('password_domain');
            $table->date('next_due_date')->nullable();
            $table->string('customer_domain');
            $table->string('owner_name');
            $table->string('ownerid_number')->nullable();
            $table->string('owner_taxcode')->nullable();
            $table->string('owner_address');
            $table->string('owner_email');
            $table->string('owner_phone');
            $table->string('ui_name');
            $table->string('uiid_number')->nullable();
            $table->string('ui_taxcode')->nullable();
            $table->string('ui_gender')->nullable();
            $table->date('ui_birthdate')->nullable();
            $table->string('ui_address');
            $table->string('ui_province');
            $table->string('ui_email');
            $table->string('ui_phone');
            $table->string('admin_name')->nullable();
            $table->string('adminid_number')->nullable();
            $table->string('admin_gender')->nullable();
            $table->date('admin_birthdate')->nullable();
            $table->string('admin_address')->nullable();
            $table->string('admin_province')->nullable();
            $table->string('admin_email')->nullable();
            $table->string('admin_phone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domains');
    }
}
