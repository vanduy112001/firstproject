<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotationDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('quotation_id')->nullable();
            $table->bigInteger('product_id')->nullable();
            $table->string('type_product')->nullable();
            $table->integer('addon_cpu')->nullable();
            $table->integer('addon_ram')->nullable();
            $table->integer('addon_disk')->nullable();
            $table->integer('qtt_ip')->nullable();
            $table->integer('qtt')->nullable();
            $table->string('os')->nullable();
            $table->string('billing_cycle')->nullable();
            $table->string('domain')->nullable();
            $table->bigInteger('amount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation_details');
    }
}
