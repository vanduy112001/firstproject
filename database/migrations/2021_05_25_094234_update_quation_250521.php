<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateQuation250521 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('quotations', 'product_id')){
            Schema::table('quotations', function (Blueprint $table) {
                $table->dropColumn('product_id');
            });
        }
        if (Schema::hasColumn('quotations', 'phone_replace')){
            Schema::table('quotations', function (Blueprint $table) {
                $table->dropColumn('phone_replace');
            });
        }
        if (Schema::hasColumn('quotations', 'email_replace')){
            Schema::table('quotations', function (Blueprint $table) {
                $table->dropColumn('email_replace');
            });
        }
        if (Schema::hasColumn('quotations', 'company_replace')){
            Schema::table('quotations', function (Blueprint $table) {
                $table->dropColumn('company_replace');
            });
        }
        if (Schema::hasColumn('quotations', 'qtt_ip')){
            Schema::table('quotations', function (Blueprint $table) {
                $table->dropColumn('qtt_ip');
            });
        }
        if (Schema::hasColumn('quotations', 'os')){
            Schema::table('quotations', function (Blueprint $table) {
                $table->dropColumn('os');
            });
        }
        if (Schema::hasColumn('quotations', 'amount')){
            Schema::table('quotations', function (Blueprint $table) {
                $table->dropColumn('amount');
            });
        }
        if (Schema::hasColumn('quotations', 'billing_cycle')){
            Schema::table('quotations', function (Blueprint $table) {
                $table->dropColumn('billing_cycle');
            });
        }
        if (!Schema::hasColumn('quotations', 'total')){
            Schema::table('quotations', function (Blueprint $table) {
                $table->bigInteger('total')->nullable();
            });
        }
        if (!Schema::hasColumn('quotations', 'text_total')){
            Schema::table('quotations', function (Blueprint $table) {
                $table->string('text_total')->nullable();
            });
        }
        if (!Schema::hasColumn('quotations', 'payment_cycle')){
            Schema::table('quotations', function (Blueprint $table) {
                $table->string('payment_cycle')->nullable();
            });
        }
        if (!Schema::hasColumn('quotations', 'vat')){
            Schema::table('quotations', function (Blueprint $table) {
                $table->boolean('vat')->default(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotations', function (Blueprint $table) {
            //
        });
    }
}
