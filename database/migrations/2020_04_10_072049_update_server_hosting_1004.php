<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateServerHosting1004 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasColumn('server_hostings', 'location')) {
            Schema::table('server_hostings', function (Blueprint $table) {
                $table->string('location')->default('vn');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('server_hostings', function (Blueprint $table) {
            //
        });
    }
}
