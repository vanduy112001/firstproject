<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVpsConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vps_configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('vps_id');
            $table->integer('ip');
            $table->double('disk');
            $table->double('ram');
            $table->double('cpu');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vps_configs');
    }
}
