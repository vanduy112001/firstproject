<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateSendMails1006 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('send_mails', function (Blueprint $table) {
            //
        });
        if (!Schema::hasColumn('send_mails', 'send_time')){
            Schema::table('send_mails', function (Blueprint $table) {
                $table->dateTime('send_time')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('send_mails', function (Blueprint $table) {
            //
        });
    }
}
