<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePromotions3107 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasColumn('promotions', 'type_order') ) {
            Schema::table('promotions', function (Blueprint $table) {
                $table->string('type_order')->nullable();
            });
        }
        if ( ! Schema::hasColumn('promotions', 'apply_one') ) {
            Schema::table('promotions', function (Blueprint $table) {
                $table->boolean('apply_one')->default(false);
            });
        }
        if ( ! Schema::hasColumn('promotions', 'upgrade') ) {
            Schema::table('promotions', function (Blueprint $table) {
                $table->boolean('upgrade')->default(false);
            });
        }
        if ( ! Schema::hasColumn('promotions', 'life_time') ) {
            Schema::table('promotions', function (Blueprint $table) {
                $table->boolean('life_time')->default(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('promotions', function (Blueprint $table) {
            //
        });
    }
}
