<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuotationServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quotation_servers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('quotation_detail_id')->nullable();
            $table->bigInteger('addon_ram')->nullable();
            $table->bigInteger('addon_ip')->nullable();
            $table->bigInteger('addon_disk2')->nullable();
            $table->bigInteger('addon_disk3')->nullable();
            $table->bigInteger('addon_disk4')->nullable();
            $table->bigInteger('addon_disk5')->nullable();
            $table->bigInteger('addon_disk6')->nullable();
            $table->bigInteger('addon_disk7')->nullable();
            $table->bigInteger('addon_disk8')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quotation_servers');
    }
}
