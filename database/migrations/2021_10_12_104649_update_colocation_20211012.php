<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateColocation20211012 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('colocations', function (Blueprint $table) {
            //
        });
        if (!Schema::hasColumn('colocations', 'rack')) {
            Schema::table('colocations', function (Blueprint $table) {
                $table->string('rack')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('colocations', function (Blueprint $table) {
            //
        });
    }
}
