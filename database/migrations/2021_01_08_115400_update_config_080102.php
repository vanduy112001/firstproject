<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateConfig080102 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasColumn('configs', 'setupkey') ) {
            Schema::table('configs', function (Blueprint $table) {
                $table->string('setupkey')->nullable();
                $table->string('imei')->nullable();
                $table->string('rkey')->nullable();
                $table->string('onesignal')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('configs', function (Blueprint $table) {
            //
        });
    }
}
