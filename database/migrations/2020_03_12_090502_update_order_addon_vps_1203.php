<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrderAddonVps1203 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasColumn('order_addon_vps', 'month')) {
            Schema::table('order_addon_vps', function (Blueprint $table) {
                $table->integer('month')->default(0);
            });
        }
        if ( ! Schema::hasColumn('order_addon_vps', 'day')) {
            Schema::table('order_addon_vps', function (Blueprint $table) {
                $table->integer('day')->default(0);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_addon_vps', function (Blueprint $table) {
            //
        });
    }
}
