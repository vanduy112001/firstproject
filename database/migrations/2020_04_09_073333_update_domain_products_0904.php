<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateDomainProducts0904 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('domain_products', 'annually')) {
            Schema::table('domain_products', function (Blueprint $table) {
                $table->string('annually')->nullable()->change();
            });
        }     
        if (Schema::hasColumn('domain_products', 'biennially')) {
            Schema::table('domain_products', function (Blueprint $table) {
                $table->string('biennially')->nullable()->change();
            });
        }     
        if (Schema::hasColumn('domain_products', 'triennially')) {
            Schema::table('domain_products', function (Blueprint $table) {
                $table->string('triennially')->nullable()->change();
            });
        }     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('domain_products', function (Blueprint $table) {
            //
        });
    }
}
