<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateServerConfig20211022 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('server_configs', 'disk2')) {
            Schema::table('server_configs', function (Blueprint $table) {
                $table->bigInteger('disk2')->nullable();
            });
        }
        if (!Schema::hasColumn('server_configs', 'disk3')) {
            Schema::table('server_configs', function (Blueprint $table) {
                $table->bigInteger('disk3')->nullable();
            });
        }
        if (!Schema::hasColumn('server_configs', 'disk4')) {
            Schema::table('server_configs', function (Blueprint $table) {
                $table->bigInteger('disk4')->nullable();
            });
        }
        if (!Schema::hasColumn('server_configs', 'disk5')) {
            Schema::table('server_configs', function (Blueprint $table) {
                $table->bigInteger('disk5')->nullable();
            });
        }
        if (!Schema::hasColumn('server_configs', 'disk6')) {
            Schema::table('server_configs', function (Blueprint $table) {
                $table->bigInteger('disk6')->nullable();
            });
        }
        if (!Schema::hasColumn('server_configs', 'disk7')) {
            Schema::table('server_configs', function (Blueprint $table) {
                $table->bigInteger('disk7')->nullable();
            });
        }
        if (!Schema::hasColumn('server_configs', 'disk8')) {
            Schema::table('server_configs', function (Blueprint $table) {
                $table->bigInteger('disk8')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('server_configs', function (Blueprint $table) {
            //
        });
    }
}
