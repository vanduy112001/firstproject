<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateServerHosting2403 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasColumn('server_hostings', 'type_server_hosting')) {
            Schema::table('server_hostings', function (Blueprint $table) {
                $table->string('type_server_hosting');
            });
        }
        if (! Schema::hasColumn('server_hostings', 'name')) {
            Schema::table('server_hostings', function (Blueprint $table) {
                $table->string('name');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('server_hostings', function (Blueprint $table) {
            //
        });
    }
}
