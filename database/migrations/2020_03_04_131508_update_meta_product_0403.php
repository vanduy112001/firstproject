<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMetaProduct0403 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( ! Schema::hasColumn('meta_products', 'email_expired')) {
            Schema::table('meta_products', function (Blueprint $table) {
                $table->bigInteger('email_expired')->nullable();
            });
        }
        if ( ! Schema::hasColumn('meta_products', 'email_expired_finish')) {
            Schema::table('meta_products', function (Blueprint $table) {
                $table->bigInteger('email_expired_finish')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta_products', function (Blueprint $table) {
            //
        });
    }
}
