<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateQuotationDetail0206 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('quotation_details', 'start_date')){
            Schema::table('quotation_details', function (Blueprint $table) {
                $table->date('start_date')->nullable();
            });
        }
        if (!Schema::hasColumn('quotation_details', 'end_date')){
            Schema::table('quotation_details', function (Blueprint $table) {
                $table->date('end_date')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('quotation_details', function (Blueprint $table) {
            //
        });
    }
}
