<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderAddonServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_addon_servers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('server_id')->nullable();
            $table->bigInteger('ip')->nullable();
            $table->text('list_ip')->nullable();
            $table->bigInteger('ram')->nullable();
            $table->bigInteger('disk')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_addon_servers');
    }
}
