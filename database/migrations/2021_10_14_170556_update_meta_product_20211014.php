<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateMetaProduct20211014 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasColumn('meta_products', 'chassis')) {
            Schema::table('meta_products', function (Blueprint $table) {
                $table->string('chassis')->nullable();
            });
        }
        if (!Schema::hasColumn('meta_products', 'raid')) {
            Schema::table('meta_products', function (Blueprint $table) {
                $table->string('raid')->nullable();
            });
        }
        if (!Schema::hasColumn('meta_products', 'datacenter')) {
            Schema::table('meta_products', function (Blueprint $table) {
                $table->string('datacenter')->nullable();
            });
        }
        if (!Schema::hasColumn('meta_products', 'server_management')) {
            Schema::table('meta_products', function (Blueprint $table) {
                $table->string('server_management')->nullable();
            });
        }
        if (!Schema::hasColumn('meta_products', 'backup')) {
            Schema::table('meta_products', function (Blueprint $table) {
                $table->string('backup')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meta_products', function (Blueprint $table) {
            //
        });
    }
}
