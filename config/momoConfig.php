<?php
return [
    'aaid'                  => '',
    'idfa'                  => '',
    'csp'                   => 'Mobifone',
    'icc'                   => '',
    'mcc'                   => '452',
    'mnc'                   => '04',
    'cname'                 => 'Vietnam',
    'ccode'                 => '084',
    'channel'               => 'APP',
    'lang'                  => 'vi',
    'device'                => 'iPhone',
    'firmware'              => '13.5.1',
    'manufacture'           => 'Apple',
    'hardware'              => 'iPhone',
    'simulator'             => false,
    'appVer'                => '21462',
    'appCode'               => "2.1.46",
    'deviceOS'              => "IOS"
];
