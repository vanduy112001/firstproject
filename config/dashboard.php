<?php
return [
  'Authorization' => 'Token 2134da915025e8ba3ef9eb6fde4c2627f11d98c2',
  'url_create_vps' => 'https://dashboard.cloudzone.vn/api/vms/',
  'url_create_vps_us' => 'https://dashboard.cloudzone.vn/api/usrs/',
  'url_create_customer' => 'https://dashboard.cloudzone.vn/api/customers/',
  'url_status' => 'https://dashboard.cloudzone.vn/api/vms/',
  'url_proxy' => 'https://dashboard.cloudzone.vn/api/proxy/',
];
?>
<!-- Body tao VPS
{
    "customer": 91, //id customer tren dashboard
    "created_date": "2020-02-21", // Ngay tao
    "leased_time": 1, //Thoi gian
    "end_date": "2020-03-21", //ngay ket thuc
    "mob": "150000", //Gia
    "template": 2, //
    "cpu": 1,
    "ram": "1",
    "disk": 30
} -->
<!--
  Body tạo customer
  {
    "name": "Đỗ Ngọc Châu Giang",
    "email": "giangdnc@cloudzone.vn"
}

 -->
 <!--
 https://dashboard.cloudzone.vn/api/vms/19062/
 {
     "mob": "update_leased_time", // action
     "name": 19062, // vm_id
     "created_date": '2020-01-01', // ngay bat dau
     "leased_time": 1 // thời gian thuê
 }
-->
