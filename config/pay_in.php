<?php
return [
    'pay_in_office' => 'Trực tiếp tại văn phòng',
    'bank_transfer_vietcombank' => 'Chuyển khoản ngân hàng Vietcombank',
    'bank_transfer_techcombank' => 'Chuyển khoản ngân hàng Techcombank',
    'momo' => 'Thanh toán qua MOMO',
    'invoice' => 'Hóa đơn',
    'credit' => 'Số dư tài khoản',
    'cloudzone_point' => 'Cloudzone Point',
];
