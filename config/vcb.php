<?php

return [
  'username' => env('USER_VCB'),
  'password' => env('PASSWORD_VCB'),
  'account_number' => env('ACCOUNT_VCB'),
  'url' => env('URL_VCB'),
];

?>
