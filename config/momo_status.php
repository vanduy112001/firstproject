<?php
return [
    "36" => 'Giao dịch đã hết hạn.',
    "37" => 'Tài khoản hết hạn mức giao dịch trong ngày.',
    "38" => 'Tài khoản khách hàng không đủ tiền',
    "49" => 'Khách hàng huỷ giao dịch',
    "9043" => 'Khách hàng chưa liên kết tài khoản ngân hàng',
    "80" => 'Xác thực khách hàng không thành công
- Đăng nhập mật khẩu, mã xác thực (OTP) vượt quá số lần quy định
- Phiên làm việc đã hết hạn (Trên app MoMo)',
];
