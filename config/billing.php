<?php
return [
    'monthly' => '1 Tháng',
    'twomonthly' => '2 Tháng',
    'quarterly' => '3 Tháng',
    'semi_annually' => '6 Tháng',
    'annually' => '1 Năm',
    'biennially' => '2 Năm',
    'triennially' => '3 Năm',
    'one_time_pay' => 'Vĩnh viễn',
    'free' => 'Miễn phí',
];
