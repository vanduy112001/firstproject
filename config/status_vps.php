<?php
return [
   'off' => 'Tắt VPS',
   'on' => 'Bật VPS',
   'admin_off' => 'Admin Tắt VPS',
   'progressing' => 'Đang tạo VPS',
   'rebuild' => 'Đang tạo lại VPS',
   'cancel' => 'Hủy VPS',
   'delete_vps' => 'Đã xóa',
   'change_user' => 'Đã chuyển',
   'reset_password' => 'Đang cài lại mật khẩu',
   'change_ip' => 'Đang cài IP',
   'expire' => 'Hết hạn',
];


?>
