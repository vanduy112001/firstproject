<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'facebook' => [
        'client_id' => env('FACEBOOK_APP_ID', '2668079046654493'),
        'client_secret' => env('FACEBOOK_APP_SECRET', 'd27f5cdb4c70d9fc3a9012f11c065aa8'),
        'redirect' => env('FACEBOOK_APP_CALLBACK_URL', '/callback/facebook'),
    ],
    'google' => [
        'client_id' => env('GOOGLE_APP_ID', '325741912469-2a17vqp7qp1ibsrl0gv7j04n0sn6s29r.apps.googleusercontent.com'),
        'client_secret' => env('GOOGLE_APP_SECRET', 'umWRt39d9t-hQ5sqYb_a7Hzk'),
        'redirect' => env('GOOGLE_APP_CALLBACK_URL', '/callback/google'),
    ],

];
