<?php
return [
    'annually_exp' => '1',
    'biennially_exp' => '2',
    'triennially_exp' => '3',
    'quadrennially_exp' => '4',
    'quinquennially_exp' => '5'
];
