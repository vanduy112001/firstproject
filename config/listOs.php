<?php
  return [
    '1' => 'Windows 10 64bit',
    '2' => 'Windows Server 2012 R2',
    '3' => 'Windows Server 2016',
    '4' => 'Linux CentOS 7 64bit',
    '5' => 'Windows Server 2012 R2 - GAME',
    '11' => 'Windows 10 64bit',
    '12' => 'Windows Server 2012 R2 - Không có bảo mật',
    '13' => 'Windows Server 2016 - Không có bảo mật',
    '14' => 'Linux CentOS 7 64bit - Không có bảo mật',
    '15' => 'Windows Server 2019',
    // Temlate không public
    '30' => 'Windows Server 2012 R2 - 512M RAM',
    '31' => 'Linux Ubuntu-20.04',
  ];
