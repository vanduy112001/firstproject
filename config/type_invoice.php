<?php
  return [
    'VPS' => "VPS",
    'VPS US' => "VPS US",
    'VPS-US' => "VPS US",
    ' US' => "VPS US",
    'Server' => "Server",
    'Proxy' => "Proxy",
    'Colocation' => "Colocation",
    'Hosting' => 'Hosting',
    'Email Hosting' => 'Email Hosting',
    'Hosting-Singapore' => 'Hosting Singapore',
    'hosting' => 'Hosting',
    'NAT-VPS' => 'NAT-VPS',
    'change_ip' => 'Đổi IP VPS',
    'addon_vps' => 'Nâng cấp cấu hình VPS',
    'addon_server' => 'Nâng cấp Server',
    'upgrade_hosting' => 'Nâng cấp Hosting',
    'Domain' => 'Domain',
    'Domain_exp' => 'Domain',
  ];

?>
