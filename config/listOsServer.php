<?php
  return [
    '1' => 'Windows 10 64bit',
    '2' => 'Windows Server 2012 R2',
    '3' => 'Windows Server 2016',
    '4' => 'Linux CentOS 7 64bit',
    '5' => 'Windows Server 2019',
    '6' => 'Linux Ubuntu-20.04',
    '7' => 'VMware ESXi 6.7'
  ];
