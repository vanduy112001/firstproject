<?php
return [
    'monthly' => 1,
    'twomonthly' => 2,
    'quarterly' => 3,
    'semi_annually' => 6,
    'annually' => 12,
    'biennially' => 24,
    'triennially' => 36,
];
