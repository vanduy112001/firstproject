<?php
return [
	// User
	'listUser' => 'Danh sách tài khoản',
	'createUser' => 'Tạo tài khoản',
	'editUser' => 'Chỉnh sửa tài khoản',
	'deleteUser' => 'Xóa tài khoản',  
	'multideleteUser' => 'Xóa nhiều tài khoản',
	'loginAsUser' => 'Đăng nhập khách hàng khác',
	'updateCreditUser' => 'Cập nhật số dư tài khoản',
	'updateCreditTotal' => 'Cập nhật tổng số tiền đã nạp',
	'detailUser' => 'Chi tiết tài khoản',  
	'historyPayUser' => 'Lịch sử thanh toán',  
	'addPayment' => 'Tạo nạp tiền vào tài khoản', 
	//  Group User
	'listGroupUser' => 'Danh sách  đại lý',
	'createGroupUser' => 'Tạo đại lý',
	'detailGroupUser' => 'Chi tiết đại lý',  
	'deleteGroupUser' => 'Xóa đại lý',  

];