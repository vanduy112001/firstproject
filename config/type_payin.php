<?php
return [
    1 => 'Nạp tiền vào tài khoản',
    2 => 'Tạo mới',
    3 => 'Gia hạn',
    4 => 'Nâng cấp VPS',
    5 => 'Thay đổi IP cho VPS',
    6 => 'Thanh toán dịch vụ tên miền',
    7 => 'Gia hạn tên miền',
    8 => 'Nâng cấp Hosting',
    9 => 'Nâng cấp Server',
    98 => 'Hệ thống cộng tiền vào tài khoản',
    99 => 'Hệ thống trừ tiền tài khoản',
];
