<?php 
	return [
		'request_payment' => 'Yêu cầu nạp tiền',
		'finish_payment' => 'Hoàn thành nạp tiền',
		'finish_change_ip' => 'Hoàn thành thay đổi IP VPS US',
		'finish_change_ip_vn' => 'Hoàn thành thay đổi IP VPS VN',
		'finish_addon_vps' => 'Hoàn thành thanh toán addon VPS',
		'error_auto_refurn' => 'Lỗi tự động gia hạn VPS (Số dư tài khoản)',
		'create_user' => 'Đăng ký tài khoản',
		'verify_user' => 'Kích hoạt tài khoản',
		'forgot_password' => 'Yêu cầu đặt lại mật khẩu của tài khoản',
	];
	
?>