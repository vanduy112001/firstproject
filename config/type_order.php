<?php

return [
    'Pending' => 'Chưa thanh toán',
    'Finish' => 'Đã thanh toán',
    'Active' => 'Đang chờ tạo',
    'Cancelled' => 'Đã hủy',
];
