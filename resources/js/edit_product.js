$(document).ready(function () {
    $('.select2').select2();
    $('.selectRaid').select2();
    var type_product = $('.package').attr('data-type');
    var package_product = $('.package').val();
    loadPackage(type_product, package_product);
    function loadPackage(type_product, package_product='') {
        if (type_product == 'Hosting') {
            $.ajax({
                url: '/admin/directAdmin/showPackage',
                data: {location: 'vn'},
                dataType: 'json',
                beforeSend: function(){
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#package').html(html);
                },
                success: function (data) {
                    var html = '';
                    if (data != '' || data != null) {
                        html += '<div class="row form-group">';
                        html += '<label>Package</label>'
                        html += '<select name="package"  class="form-control">';
                        $.each(data, function(index, value) {
                            if (package_product == value) {
                                html += '<option value="'+value+'" selected>'+value+'</option>';
                            } else {
                                html += '<option value="'+value+'">'+value+'</option>';
                            }

                        });
                        html += '</select>';
                        html += '</div>';
                    } else {
                        html += '<p class="text-danger">Không có Package trên Direct Admin.<p>';
                    }
                    $('#package').html(html);
                },
                error: function (e) {
                    console.log(e);
                    $('#package').html('<p class="text-danger">Truy vấn Direct Admin lỗi!<p>');
                }
            });
        }
        else if (type_product == 'Hosting-Singapore') {
            $.ajax({
                url: '/admin/directAdmin/showPackage',
                data: {location: 'si'},
                dataType: 'json',
                beforeSend: function(){
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#package').html(html);
                },
                success: function (data) {
                    var html = '';
                    if (data != '' || data != null) {
                        html += '<div class="row form-group">';
                        html += '<label>Package</label>'
                        html += '<select name="package"  class="form-control">';
                        $.each(data, function(index, value) {
                            if (package_product == value) {
                                html += '<option value="'+value+'" selected>'+value+'</option>';
                            } else {
                                html += '<option value="'+value+'">'+value+'</option>';
                            }

                        });
                        html += '</select>';
                        html += '</div>';
                    } else {
                        html += '<p class="text-danger">Không có Package trên Direct Admin.<p>';
                    }
                    $('#package').html(html);
                },
                error: function (e) {
                    console.log(e);
                    $('#package').html('<p class="text-danger">Truy vấn Direct Admin lỗi!<p>');
                }
            });
        }
    }

    $('#type_product').on('change', function() {
        var type_product = $(this).val();
        var html = '';
        if (type_product == 'VPS' || type_product == 'VPS-US' || type_product == 'NAT-VPS') {
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="cpu">CPU</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="cpu" value="" class="form-control" id="cpu" placeholder="CPU">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="ram">RAM</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="ram" value="" class="form-control" id="ram" placeholder="Memory">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="disk">DISK</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="disk" value="" class="form-control" id="disk" placeholder="Disk">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="bandwidth">Băng thông</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="bandwidth" value="" class="form-control" id="bandwidth" placeholder="Băng thông">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="ip">IP</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="ip" value="" class="form-control" id="ip" placeholder="Số IP public">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="os">Hệ điều hành (content)</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="os" value="" class="form-control" id="os" placeholder="Hệ điều hành">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="os">Hệ điều hành (cho VPS)</label>';
            html += '</div><div class="col col-md-10" id="select_product_form">';
            html += '<select class="form-control select2" id="os_vps" multiple data-placeholder="Chọn hệ điều hành cho VPS" name="os_vps[]">';
            html += '<option value="1">Windows 7 64bit</option>';
            html += '<option value="2">Windows Server 2012 R2</option>';
            html += '<option value="3">Windows Server 2016</option>';
            html += '<option value="4">Linux CentOS 7 64bit</option>';
            html += '</select>';
            html += '</div>';
            html += '</div>';
        } else if(type_product == 'Server') {
            // name server
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="name_server">Tên máy chủ</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="name_server" value="" class="form-control" id="name_server" placeholder="Tên máy chủ">';
            html += '</div>';
            html += '</div>';
            // CPU
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="cpu">CPU</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="cpu" value="" class="form-control" id="cpu" placeholder="CPU">';
            html += '</div>';
            html += '</div>';
            // cores
            html += `
                <div class="row form-group">
                    <div class="col-md-2">
                        <label for="cores" class="">Cores</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" name="cores" value="" class="form-control" id="cores" placeholder="Cores">
                    </div>
                </div>
            `;
            // RAN
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="ram">RAM</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="ram" value="" class="form-control" id="ram" placeholder="Memory">';
            html += '</div>';
            html += '</div>';
            // Chassis
            html += `
                <div class="row form-group">
                    <div class="col-md-2">
                        <label for="chassis" class="">Số lượng ổ đĩa</label>
                    </div>
                    <div class="col-md-10">
                        <select name="chassis" id="chassis" class="form-control">
                            <option value="4">4 ổ đĩa (SATA/SSD)</option>
                            <option value="8">8 ổ đĩa (SATA/SSD)</option>
                        </select>
                    </div>
                </div>
            `;
            // disk
            html += `
                <div class="row form-group">
                    <div class="col-md-2">
                        <label for="drive1" class="">Disk số 1</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" name="drive1" value="" class="form-control" id="drive1" placeholder="Disk số 1">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-2">
                        <label for="drive2" class="">Disk số 2</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" name="drive2" value="" class="form-control" id="drive2" placeholder="Disk số 2">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-2">
                        <label for="drive3" class="">Disk số 3</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" name="drive3" value="" class="form-control" id="drive3" placeholder="Disk số 3">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col-md-2">
                        <label for="drive4" class="">Disk số 4</label>
                    </div>
                    <div class="col-md-10">
                        <input type="text" name="drive4" value="" class="form-control" id="drive4" placeholder="Disk số 4">
                    </div>
                </div>
            `;
            html += `
                <div class="row form-group">
                    <div class="col-md-2">
                        <label for="raid" class="">Raid</label>
                    </div>
                    <div class="col-md-10">
                        <select class="form-control selectRaid" id="raid" multiple data-placeholder="Chọn Raid" name="raidSelect[]">
                            <option value="None">None</option>
                            <option value="Raid 0">Raid 0</option>
                            <option value="Raid 1">Raid 1</option>
                            <option value="Raid 10">Raid 10</option>
                        </select>    
                    </div>
                </div>
            `;
            html += `
                <div class="row form-group">
                    <div class="col-md-2">
                        <label for="port_network" class="">Port Network</label>
                    </div>
                    <div class="col-md-10">
                        <select name="port_network" id="port_network" class="form-control">
                            <option value="100 Mb">100 MB</option>
                            <option value="1 Gb">1 GB</option>
                            <option value="10 Gb">10 GB</option>
                        </select>
                    </div>
                </div>
            `;
            // băng thông
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="bandwidth">Băng thông</label>';
            html += '</div><div class="col col-md-10">';
            html += '<select name="bandwidth" id="bandwidth" class="form-control">';
            html += `<option value="100 Mb trong nước/10 Mb quốc tế (chia sẻ)">100 Mb trong nước/10 Mb quốc tế (chia sẻ)</option>
                    <option value="1 Gb trong nước/10 Mb quốc tế (chia sẻ)">1 Gb trong nước/10 Mb quốc tế (chia sẻ)</option>`;
            html += '</select>'
            html += '</div>';
            html += '</div>';
            // ip
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="ip">IP</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="ip" value="" class="form-control" id="ip" placeholder="Số IP public">';
            html += '</div>';
            html += '</div>';
            // datacenter
            html +=`
                <div class="row form-group">
                    <div class="col-md-2">
                        <label for="datacenter" class="">Datacenter</label>
                    </div>
                    <div class="col-md-10">
                        <select class="form-control select2" id="datacenter" multiple data-placeholder="Chọn Datacenter" name="datacenterSelect[]">
                            <option value="Đà Nẵng">Đà Nẵng</option>
                            <option value="Hồ Chí Minh">Hồ Chí Minh</option>
                            <option value="Hà Nội">Hà Nội</option>
                        </select>
                    </div>
                </div>
            `;
            // os
            html += `
                <div class="row form-group">
                    <div class="col-md-2">
                        <label for="os_vps" class="">Hệ điều hành (cho Server)</label>
                    </div>
                    <div class="col-md-10" id="select_product_form">
                        <select class="form-control select2" id="os_vps" multiple data-placeholder="Chọn hệ điều hành cho Server" name="os_vps[]">
                            <option value="1">Windows 10 64bit</option>
                            <option value="2">Windows Server 2012 R2</option>
                            <option value="3">Windows Server 2016</option>
                            <option value="4">Linux CentOS 7 64bit</option>
                            <option value="5">Windows Server 2019</option>
                            <option value="6">Linux Ubuntu-20.04</option>
                            <option value="7">VMware ESXi 6.7</option>
                        </select>
                    </div>
                </div>
            `;
            // server_management
            html += `
                <div class="row form-group">
                    <div class="col-md-2">
                        <label for="server_management" class="">Server Management</label>
                    </div>
                    <div class="col-md-10">
                        <select class="form-control select2" id="server_management" multiple data-placeholder="Chọn Server Management" name="serverManagementSelect[]">
                            <option value="Unmanaged">Unmanaged</option>
                            <option value="Managed">Managed</option>
                        </select>
                    </div>
                </div>
            `;
        } else if(type_product == 'Addon-VPS' || type_product == 'Addon-Server' ) {
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="cpu">CPU</label>';
            html += '</div>';
            html += '<div class="col col-md-10">';
            html += '<input type="text" name="cpu" value="" class="form-control" id="cpu" placeholder="CPU">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="ram">RAM</label>';
            html += '</div>';
            html += '<div class="col col-md-10">';
            html += '<input type="text" name="ram" value="" class="form-control" id="ram" placeholder="Memory">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="disk">DISK</label>';
            html += '</div>';
            html += '<div class="col col-md-10">';
            html += '<input type="text" name="disk" value="" class="form-control" id="disk" placeholder="Disk">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="ip">IP</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="ip" value="" class="form-control" id="ip" placeholder="Số IP public">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="type_addon">Loại Addon</label>';
            html += '</div>';
            html += '<div class="col col-md-10">';
            html += '<select class="form-control" id="type_addon" name="type_addon">';
            html += '<option value="" disabled selected>---Chọn loại Addon---</option>';
            html += '<option value="addon_cpu" >Addon CPU</option>';
            html += '<option value="addon_ram" >Addon RAM</option>';
            html += '<option value="addon_disk">Addon DISK</option>';
            html += '<option value="addon_ip">Addon IP</option>';
            html += '</select>';
            html += '</div>';
            html += '</div>';
        } else if (type_product == 'Addon-Hosting') {
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="storage">Storage</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="storage" value="" class="form-control" id="storage" placeholder="Storage">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="domain">Số lượng domain</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="domain" value="" class="form-control" id="domain" placeholder="Số lượng domain">';
            html += '</div>';
            html += '</div>';
        } else {
            loadPackage('Hosting', '');
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="storage">Storage</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="storage" value="" class="form-control" id="storage" placeholder="Storage">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="domain">Số lượng domain</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="domain" value="" class="form-control" id="domain" placeholder="Số lượng domain">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="sub_domain">Số lượng sub domain</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="sub_domain" value="" class="form-control" id="sub_domain" placeholder="Số lượng sub domain">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="alias_domain">Alias domain</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="alias_domain" value="" class="form-control" id="alias_domain" placeholder="Alias Domain">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="database">Database</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="database" value="" class="form-control" id="database" placeholder="Database">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="ftp">Ftp</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="ftp" value="" class="form-control" id="ftp" placeholder="Ftp">';
            html += '</div>';
            html += '</div>';
            html += '<div class="row form-group">';
            html += '<div class="col col-md-2">';
            html += '<label for="panel">Control panel</label>';
            html += '</div><div class="col col-md-10">';
            html += '<input type="text" name="panel" value="" class="form-control" id="panel" placeholder="Control panel">';
            html += '</div>';
            html += '</div>';
        }
        $('#config').html(html);
        $('.selectRaid').select2();
        $('.select2').select2();
    });

    $(document).on('change', '#chassis',function() {
        var sl = $(this).val();
        if ( sl == 8 ) {
            var html = `<div class="row form-group">
                            <div class="col-md-2">
                                <label for="drive5" class="">Disk số 5</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" name="drive5" value="" class="form-control" id="drive5" placeholder="Disk số 5">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-2">
                                <label for="drive6" class="">Disk số 6</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" name="drive6" value="" class="form-control" id="drive6" placeholder="Disk số 6">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-md-2">
                                <label for="drive7" class="">Disk số 7</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" name="drive7" value="" class="form-control" id="drive7" placeholder="Disk số 7">
                            </div>
                        </div><div class="row form-group">
                            <div class="col-md-2">
                                <label for="drive4" class="">Disk số 8</label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" name="drive8" value="" class="form-control" id="drive8" placeholder="Disk số 8">
                            </div>
                        </div>
                        `;
            $('#chassis8').fadeIn();          
            $('#chassis8').html(html);
        } else {
            $('#chassis8').fadeOut();
        }
    });

    $(document).on('change', '#raid', function () {
        if ( $(this).val() == 'All' ) {
            var optionRaid = $('#raid option');
            $.each(optionRaid, function() {
                if ( $(this).val() == 'All' ) {
                    $(this).remove();
                }
                else {
                    $(this).attr('selected', true);
                }
            })
            $('.selectRaid').select2();
        }
    })

    $(document).on('change', '#datacenter', function () {
        if ( $(this).val() == 'All' ) {
            var optionRaid = $('#datacenter option');
            $.each(optionRaid, function() {
                if ( $(this).val() == 'All' ) {
                    $(this).remove();
                }
                else {
                    $(this).attr('selected', true);
                }
            })
            $('.select2').select2();
        }
    })

    $('.createGroup').on('click', function()
    {
        var groupId = $(this).attr('data-id');
        $('#modal-product').modal('show');
        $('.modal-title').text('Tạo nhóm sản phẩm');
        $('#button-submit').val('Tạo nhóm sản phẩm');
        $('#button-submit').attr('data-id', groupId);
        $('#notication').html("");
        $('#form-group-product')[0].reset();
        $('#action').val('create');
        $('#groupId').val(groupId);
    })

    $('#button-submit').on('click', function() {

        var form_data = $('#form-group-product').serialize();
        var name = $('#name').val();
        var title = $('#title').val();
        var type = $('#type').val();
        var validate = group_product_validate(name, title, type);
        var groupId = $(this).attr('data-id');
        // console.log(form_data);
        if (validate == false) {
          $.ajax({
            type: "post",
            url: "/admin/products/create-product-by-group-user",
            data: form_data,
            dataType: "json",
            beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#notication').html(html);
              $('#form-group-product').css('opacity', 0.5);
              $('#button-submit').attr('disabled', true);
              $('#form-group-product').attr('disabled', true);
            },
            success: function (response) {
                if (response.error == 0) {
                    var html = '<p class="text-success text-center m-4">Tạo nhóm sản phẩm thành công</p>';
                    $('#notication').html(html);
                    $('#form-group-product')[0].reset();
                    $('#button-submit').val('Tạo nhóm sản phẩm');
                    $('.modal-title').text('Tạo nhóm sản phẩm');
                    $('#form-group-product').css('opacity', 1);
                    $('#button-submit').attr('disabled', false);
                    $('#form-group-product').attr('disabled', false);
                    var html = '';
                    $.each(response.groupProduct, function (index, groupProduct) { 
                        html += '<option value="'+ groupProduct.id +'">'+ groupProduct.name +'</option>';
                    });
                    $('#group_product'+ groupId).html(html);
                } else {
                    var html = '<p class="text-danger">Tạo nhóm sản phẩm thất bại</p>';
                    $('#notication').html(html);
                    $('#form-group-product').css('opacity', 1);
                    $('#button-submit').attr('disabled', false);
                    $('#form-group-product').attr('disabled', false)
                }
            },
            error: function(e) {
              console.log(e);
              var html = '<p class="text-danger">Tạo / Chỉnh sửa nhóm sản phẩm thất bại</p>';
              $('#notication').html(html);
              $('#form-group-product').css('opacity', 1);
              $('#button-submit').attr('disabled', false);
              $('#form-group-product').attr('disabled', false);
            }
          });
        }
    
      });

      // validate group product
        function group_product_validate(name, title, type) {

            if (name == null || name == '') {
            $('#notication').text('Tên nhóm sản phẩm không được để trống.')
            return true;
            } else if(name.length < 5) {
            $('#notication').text('Số ký tự của tên nhóm sán phẩm không được nhỏ hơn 5 ký tự.')
            return true;
            } else if(name.length > 100) {
            $('#notication').text('Số ký tự của tên nhóm sán phẩm không được lớn hơn 100 ký tự.')
            return true;
            } else if(title.length < 5) {
            $('#notication').text('Số ký tự của tiều đề hiển thị không được nhỏ hơn 5 ký tự.')
            return true;
            } else if(title.length > 200) {
            $('#notication').text('Số ký tự của tiều đề hiển thị không được lớn hơn 200 ký tự.')
            return true;
            } else if( title == null || title == '' ) {
            $('#notication').text('Tiều đề hiển thị không được để trống.')
            return true;
            } else if(type == null || type == '') {
            $('#notication').text('Loại sản phẩm không được để trống.')
            return true;
            } else {
            return false;
            }



        }

});
