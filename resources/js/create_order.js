$(document).ready(function () {
    $('.select2').select2();
    $('#type_products').on('change', function() {
        var type_product = $(this).val();
        var user_id = $('#user').val();
        if (user_id != null) {
          if (type_product ==  'Hosting') {
              $.ajax({
                  type: "get",
                  url: "/admin/orders/list_product?",
                  data: {action: type_product, user_id:user_id},
                  dataType: "json",
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('.order_product').html(html);
                  },
                  success: function (data) {
                      // console.log(data);
                      var html = '';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="type_products">Sản phẩm</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="product_id" id="product_id" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Hosting">';
                      html += '<option disabled selected>Chọn sản phẩm</option>';
                      if (data.product != '') {
                          $.each(data.product, function (index, product) {
                              html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                          });
                      } else {
                          html += '<option disabled class="text-danger">Không có sản phẩm Hosting</option>';
                      }
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="domain">Domain</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right domain">';
                      html += '<input id="domain" name="domain" class="form-control" type="text" placeholder="Domain">';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="billing_cycle">Thời gian</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="billing_cycle" id="billing_cycle" class="select2 form-control"  style="width: 100%;">';
                      $.each(data.billing, function (index2, billing) {
                          html += '<option value="'+ index2 +'">'+ billing +'</option>';
                      });
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="server_hosting">Server Hosting</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="server_hosting" id="server_hosting" class="form-control"  style="width: 100%;">';
                      html += '<option value="0">DA Portal (103.95.196.23)</option>';
                      $.each(data.server_hosting, function (index2, server_hosting) {
                          html += '<option value="'+ server_hosting.id +'">'+ server_hosting.name + ' ('+ server_hosting.ip +')' +'</option>';
                      });
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="quantity_hosting">Số lượng</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="quantity_hosting" name="quantity" class="form-control" type="text" placeholder="Số lượng" value="1">';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="price_override">Giá ghi đè</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<div class=" icheck-primary d-inline">';
                      html += '<input class="custom-control-input mb-2" type="checkbox" name="check_price_override" id="check_price_override" value="1">';
                      html += '<label for="check_price_override"></label>';
                      html += '</div>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="total_price_override">Sửa giá</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="total_price_override" name="total_price_override" class="form-control" type="text" placeholder="Ghi đè giá cho đơn đặt hàng" value="">';
                      html += '</div>';
                      html += '</div>';
                      $('.order_product').html(html);
                  },
                  error: function(e) {
                      console.log(e);
                      var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                      $('.order_product').html(html);
                  }
              });
          }
          else if ( type_product ==  'Hosting-Singapore' ) {
              $.ajax({
                  type: "get",
                  url: "/admin/orders/list_product?",
                  data: {action: type_product, user_id:user_id},
                  dataType: "json",
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('.order_product').html(html);
                  },
                  success: function (data) {
                      var html = '';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="type_products">Sản phẩm</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="product_id" id="product_id" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Hosting">';
                      html += '<option disabled selected>Chọn sản phẩm</option>';
                      if (data.product != '') {
                          $.each(data.product, function (index, product) {
                              html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                          });
                      } else {
                          html += '<option disabled class="text-danger">Không có sản phẩm Hosting</option>';
                      }
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="domain">Domain</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right domain">';
                      html += '<input id="domain" name="domain" class="form-control" type="text" placeholder="Domain">';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="billing_cycle">Thời gian</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="billing_cycle" id="billing_cycle" class="select2 form-control"  style="width: 100%;">';
                      $.each(data.billing, function (index2, billing) {
                          html += '<option value="'+ index2 +'">'+ billing +'</option>';
                      });
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="quantity_hosting">Số lượng</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="quantity_hosting" name="quantity" class="form-control" type="text" placeholder="Số lượng" value="1">';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="price_override">Giá ghi đè</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<div class=" icheck-primary d-inline">';
                      html += '<input class="custom-control-input mb-2" type="checkbox" name="check_price_override" id="check_price_override" value="1">';
                      html += '<label for="check_price_override"></label>';
                      html += '</div>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="total_price_override">Sửa giá</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="total_price_override" name="total_price_override" class="form-control" type="text" placeholder="Ghi đè giá cho đơn đặt hàng" value="">';
                      html += '</div>';
                      html += '</div>';
                      $('.order_product').html(html);
                  },
                  error: function(e) {
                      console.log(e);
                      var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                      $('.order_product').html(html);
                  }
              });
          }
          else if ( type_product ==  'Email Hosting' ) {
              $.ajax({
                  type: "get",
                  url: "/admin/orders/list_product?",
                  data: {action: type_product, user_id:user_id},
                  dataType: "json",
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('.order_product').html(html);
                  },
                  success: function (data) {
                      var html = '';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="type_products">Sản phẩm</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="product_id" id="product_id" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Hosting">';
                      html += '<option disabled selected>Chọn sản phẩm</option>';
                      if (data.product != '') {
                          $.each(data.product, function (index, product) {
                              html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                          });
                      } else {
                          html += '<option disabled class="text-danger">Không có sản phẩm Hosting</option>';
                      }
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="domain">Domain</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right domain">';
                      html += '<input id="domain" name="domain" class="form-control" type="text" placeholder="Domain">';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="billing_cycle">Thời gian</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="billing_cycle" id="billing_cycle" class="select2 form-control"  style="width: 100%;">';
                      $.each(data.billing, function (index2, billing) {
                          var selected = '';
                          if (index2 == 'annually') {
                            selected = 'selected';
                          }
                          html += '<option value="'+ index2 +'" '+ selected +'>'+ billing +'</option>';
                      });
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      // html += '<div class="row form-group">';
                      // html += '<div class="col col-md-3 text-right ">';
                      // html += '<label for="quantity_hosting">Số lượng</label>';
                      // html += '</div>';
                      // html += '<div class="col col-md-8 order-right">';
                      html += '<input id="quantity_hosting" name="quantity" class="form-control" type="hidden" placeholder="Số lượng" value="1">';
                      // html += '</div>';
                      // html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="user_name">Tài khoản</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="user_name" name="user_name" class="form-control" type="text" placeholder="Tài khoản Server">';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="password">Mật khẩu</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="password" name="password" class="form-control" type="text" placeholder="Mật khẩu">';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="price_override">Giá ghi đè</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<div class=" icheck-primary d-inline">';
                      html += '<input class="custom-control-input mb-2" type="checkbox" name="check_price_override" id="check_price_override" value="1">';
                      html += '<label for="check_price_override"></label>';
                      html += '</div>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="total_price_override">Sửa giá</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="total_price_override" name="total_price_override" class="form-control" type="text" placeholder="Ghi đè giá cho đơn đặt hàng" value="">';
                      html += '</div>';
                      html += '</div>';
                      $('.order_product').html(html);
                  },
                  error: function(e) {
                      console.log(e);
                      var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                      $('.order_product').html(html);
                  }
              });
          }
          else if (type_product ==  'Server') {
              var html = '';
              // 1 form-group
              // html += '<div class="row form-group">';
              // html += '<div class="col col-md-3 text-right ">';
              // html += '<label for="type_products">Sản phẩm</label>';
              // html += '</div>';
              // html += '<div class="col col-md-8 order-right">';
              // html += '<select name="product_id" id="product_id" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Server">';
              // html += '<option disabled selected>Chọn sản phẩm</option>';
              // if (data.product != '') {
              //     $.each(data.product, function (index, product) {
              //         html += '<option value="'+ product.id +'">'+ product.name +'</option>';
              //     });
              // } else {
              //     html += '<option disabled class="text-danger">Không có sản phẩm Server</option>';
              // }
              // html += '</select>';
              // html += '</div>';
              // html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="type_server">Loại Server</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<select name="type_server" id="type_server" class="select2 form-control"  style="width: 100%;">';
              html += '<option value="1 U">1 U</option>';
              html += '<option value="2 U">2 U</option>';
              html += '</select>';
              html += '</div>';
              html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="server_name">Dòng server</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<input id="server_name" name="server_name" class="form-control" type="text" placeholder="Dòng Server">';
              html += '</div>';
              html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="chip">Chip</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<input id="chip" name="chip" class="form-control" type="text" placeholder="Chip">';
              html += '</div>';
              html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="ram">RAM</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<input id="ram" name="ram" class="form-control" type="text" placeholder="GB">';
              html += '</div>';
              html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="disk">DISK</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<input id="disk" name="disk" class="form-control" type="text" placeholder="GB">';
              html += '</div>';
              html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="ip">IP</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<input id="ip" name="ip" class="form-control" type="text" placeholder="Địa chỉ IP">';
              html += '</div>';
              html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="location">Data Center</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<select name="location" id="location" class="form-control"  style="width: 100%;">';
              html += '<option value="DNG">DNG</option>';
              html += '<option value="DNG-CMC">DNG-CMC</option>';
              html += '<option value="HCM-Viettel HHT">HCM-Viettel HHT</option>';
              html += '<option value="HCM-ODS">HCM-ODS</option>';
              html += '<option value="HN-FPT">HN-FPT</option>';
              html += '</select>';
              html += '</div>';
              html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="billing_cycle">Thời gian</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<select name="billing_cycle" id="billing_cycle" class="select2 form-control"  style="width: 100%;">';
              html += '<option value="monthly">1 Tháng</option><option value="twomonthly">2 Tháng</option><option value="quarterly">3 Tháng</option><option value="semi_annually">6 Tháng</option><option value="annually">1 Năm</option><option value="biennially">2 Năm</option><option value="triennially">3 Năm</option><option value="one_time_pay">Vĩnh viễn</option><option value="free">Miễn phí</option>';
              html += '</select>';
              html += '</div>';
              html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="server_quantity">Số lượng</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<input id="server_quantity" name="quantity" class="form-control" type="number" placeholder="Số lượng" value="1">';
              html += '</div>';
              html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="amount">Giá</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<input id="amount" name="amount" class="form-control" type="number" placeholder="Giá cho thuê">';
              html += '</div>';
              html += '</div>';
              // // 1 form-group
              // html += '<div class="row form-group">';
              // html += '<div class="col col-md-3 text-right ">';
              // html += '<label for="user_name">Tài khoản</label>';
              // html += '</div>';
              // html += '<div class="col col-md-8 order-right">';
              // html += '<input id="user_name" name="user_name" class="form-control" type="text" placeholder="Tài khoản Server">';
              // html += '</div>';
              // html += '</div>';
              // // 1 form-group
              // html += '<div class="row form-group">';
              // html += '<div class="col col-md-3 text-right ">';
              // html += '<label for="password">Mật khẩu</label>';
              // html += '</div>';
              // html += '<div class="col col-md-8 order-right">';
              // html += '<input id="password" name="password" class="form-control" type="text" placeholder="Mật khẩu">';
              // html += '</div>';
              // html += '</div>';
              // // 1 form-group
              // html += '<div class="row form-group">';
              // html += '<div class="col col-md-3 text-right ">';
              // html += '<label for="os">Hệ điều hành</label>';
              // html += '</div>';
              // html += '<div class="col col-md-8 order-right">';
              // html += '<input id="os" name="os" class="form-control" type="text" placeholder="Hệ điều hành" value="">';
              // html += '</div>';
              // html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="description">Ghi chú</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<input id="description" name="description" class="form-control" type="text" placeholder="Ghi chú" value="">';
              html += '</div>';
              html += '</div>';

              $('.order_product').html(html);
          }
          else if (type_product ==  'Colocation') {
              var html = '';
              // 1 form-group
              // html += '<div class="row form-group">';
              // html += '<div class="col col-md-3 text-right ">';
              // html += '<label for="type_products">Sản phẩm</label>';
              // html += '</div>';
              // html += '<div class="col col-md-8 order-right">';
              // html += '<select name="product_id" id="product_id" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Server">';
              // html += '<option disabled selected>Chọn sản phẩm</option>';
              // if (data.product != '') {
              //     $.each(data.product, function (index, product) {
              //         html += '<option value="'+ product.id +'">'+ product.name +'</option>';
              //     });
              // } else {
              //     html += '<option disabled class="text-danger">Không có sản phẩm Server</option>';
              // }
              // html += '</select>';
              // html += '</div>';
              // html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="type_colo">Loại Colocation</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<select name="type_colo" id="type_colo" class="select2 form-control"  style="width: 100%;">';
              html += '<option value="1 U">1 U</option>';
              html += '<option value="2 U">2 U</option>';
              html += '<option value="4 U">4 U</option>';
              html += '</select>';
              html += '</div>';
              html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="ip">IP</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<input id="ip" name="ip" class="form-control" type="text" placeholder="Địa chỉ IP">';
              html += '</div>';
              html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="location">Data Center</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<select name="location" id="location" class="form-control"  style="width: 100%;">';
              html += '<option value="DNG">DNG</option>';
              html += '<option value="DNG-CMC">DNG-CMC</option>';
              html += '<option value="HCM-Viettel HHT">HCM-Viettel HHT</option>';
              html += '<option value="HCM-ODS">HCM-ODS</option>';
              html += '<option value="HN-FPT">HN-FPT</option>';
              html += '</select>';
              html += '</div>';
              html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="bandwidth">Băng thông</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<input id="bandwidth" name="bandwidth" class="form-control" type="text" placeholder="Băng thông">';
              html += '</div>';
              html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="power">Công suất nguồn</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<input id="power" name="power" class="form-control" type="text" placeholder="Công suất nguồn">';
              html += '</div>';
              html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="billing_cycle">Thời gian</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<select name="billing_cycle" id="billing_cycle" class="select2 form-control"  style="width: 100%;">';
              html += '<option value="monthly">1 Tháng</option><option value="twomonthly">2 Tháng</option><option value="quarterly">3 Tháng</option><option value="semi_annually">6 Tháng</option><option value="annually">1 Năm</option><option value="biennially">2 Năm</option><option value="triennially">3 Năm</option><option value="one_time_pay">Vĩnh viễn</option><option value="free">Miễn phí</option>';
              html += '</select>';
              html += '</div>';
              html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="colo_quantity">Số lượng</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<input id="colo_quantity" name="quantity" class="form-control" type="number" placeholder="Số lượng" value="1">';
              html += '</div>';
              html += '</div>';
              // 1 form-group
              html += '<div class="row form-group">';
              html += '<div class="col col-md-3 text-right ">';
              html += '<label for="col_amount">Giá</label>';
              html += '</div>';
              html += '<div class="col col-md-8 order-right">';
              html += '<input id="col_amount" name="amount" class="form-control" type="number" placeholder="Giá cho thuê">';
              html += '</div>';
              html += '</div>';
              $('.ordersummary_product .text-none').text('Dịch vụ Colocation ');
              $('.order_product').html(html);
          }
          else if (type_product ==  'VPS' || type_product ==  'NAT-VPS' || type_product ==  'VPS-US') {
              $.ajax({
                  type: "get",
                  url: "/admin/orders/list_product",
                  data: {action: type_product, user_id: user_id},
                  dataType: "json",
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('.order_product').html(html);
                  },
                  success: function (data) {
                      var html = '';
                      // 1 form-group
                      html += '<div class="notification_service mt-4 text-center"></div>';
                      html += '<div class="row form-group mt-4">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="type_products">Sản phẩm</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="product_id" id="product_id" data-value="Vps" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;">';
                      html += '<option disabled selected>Chọn sản phẩm</option>';
                      if (data.product != null) {
                          $.each(data.product, function (index, product) {
                              html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                          });
                      } else {
                          html += '<option disabled class="text-danger">Không có sản phẩm VPS</option>';
                      }
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right check_addon_vps">';
                      html += '<button class="button_vps btn btn-default"><i class="far fa-plus-square"></i> Thêm cấu hình</button>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="billing_cycle">Thời gian</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="billing_cycle" id="billing_cycle" class="select2 form-control"  style="width: 100%;">';
                      $.each(data.billing, function (index2, billing) {
                          html += '<option value="'+ index2 +'">'+ billing +'</option>';
                      });
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="quantity">Số lượng</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="quantity" name="quantity" class="form-control" type="text" placeholder="Số lượng" value="1">';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="os">Hệ điều hành</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right" id="change_os">';
                      html += '<input id="os" name="os" class="form-control" type="text" placeholder="Hệ điều hành" value="" disabled>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="check_security">Bảo mật</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<div class=" icheck-primary d-inline">';
                      html += '<input class="custom-control-input mb-2" checked type="checkbox" name="check_security" id="check_security" value="1">';
                      html += '<label for="check_security"></label>';
                      html += '</div>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="price_override">Giá ghi đè</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<div class=" icheck-primary d-inline">';
                      html += '<input class="custom-control-input mb-2" type="checkbox" name="check_price_override" id="check_price_override" value="1">';
                      html += '<label for="check_price_override"></label>';
                      html += '</div>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="total_price_override">Sửa giá</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="total_price_override" name="total_price_override" class="form-control" type="text" placeholder="Ghi đè giá cho đơn đặt hàng" value="">';
                      html += '</div>';
                      html += '</div>';
                      $('.order_product').html(html);
                  },
                  error: function(e) {
                      console.log(e);
                      var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                      $('.order_product').html(html);
                  }
              });
          }
          else if (type_product == 'Addon-VPS') {
              $.ajax({
                  type: "get",
                  url: "/admin/orders/list_product_addon",
                  data: {user_id:user_id},
                  dataType: "json",
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('.order_product').html(html);
                  },
                  success: function (data) {
                      var html = '';
                      // console.log('da den');
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="vps">Chọn VPS</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="vps_id" id="choose_addon_vps" class="select_vps form-control" data-placeholder="Chọn VPS" style="width: 100%;" data-value="Addon-VPS">';
                      html += '<option disabled selected>Chọn VPS</option>';
                      if (data.list_vps != '') {
                          $.each(data.list_vps, function (index, vps) {
                              html += '<option data-billing="'+ vps.billing_cycle +'" value="'+ vps.id +'">'+ vps.ip;
                              html += ' - ' + vps.config;
                              html += '</option>';
                          });
                      } else {
                          html += '<option disabled class="text-danger">Khách hàng không có VPS</option>';
                      }
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="addon_vps_2 text-center">';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="total_price_override">Sửa giá</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="total_price_override" name="total_price_override" class="form-control" type="text" placeholder="Ghi đè giá cho đơn đặt hàng" value="">';
                      html += '</div>';
                      html += '</div>';
                      $('.order_product').html(html);
                      $('.select_vps').select2();
                  },
                  error: function(e) {
                      console.log(e);
                      var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                      $('.order_product').html(html);
                  }
              });
          }
          else if (type_product == 'Addon-Server') {
              $.ajax({
                  type: "get",
                  url: "/admin/orders/list_expire_server",
                  data: {user_id:user_id},
                  dataType: "json",
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('.order_product').html(html);
                  },
                  success: function (data) {
                      var html = '';
                      // console.log('da den');
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="choose_addon_server">Chọn Server</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="server_id" id="choose_addon_server" class="select_server form-control" data-placeholder="Chọn Server" style="width: 100%;" data-value="Addon-VPS">';
                      html += '<option disabled selected>Chọn Server</option>';
                      if (data.list_server != '') {
                          $.each(data.list_server, function (index, server) {
                              html += '<option  value="'+ server.id +'">'+ server.ip;
                              html += '</option>';
                          });
                      } else {
                          html += '<option disabled class="text-danger">Khách hàng không có VPS</option>';
                      }
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="">Addon IP</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="addon_ip_server" name="addon_ip_server" class="form-control" type="number" placeholder="Addon IP" value="">';
                      html += '</div>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="">Addon RAM</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="addon_ram_server" name="addon_ram_server" class="form-control" type="number" placeholder="Addon RAM" value="">';
                      html += '</div>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="">Addon DISK</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="addon_disk_server" name="addon_disk_server" class="form-control" type="number" placeholder="Addon DISK" value="">';
                      html += '</div>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="total_addon_server">Giá đơn hàng</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="total_addon_server" name="total_addon_server" class="form-control" type="number" placeholder="Ghi đè giá cho đơn đặt hàng" value="">';
                      html += '</div>';
                      html += '</div>';
                      $('.order_product').html(html);
                      $('.select_server').select2();
                  },
                  error: function(e) {
                      console.log(e);
                      var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                      $('.order_product').html(html);
                  }
              });
          }
          else if (type_product == 'Upgrade-Hosting') {
              $.ajax({
                  type: "get",
                  url: "/admin/orders/get_product_upgrade_hosting",
                  data: {user_id:user_id},
                  dataType: "json",
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('.order_product').html(html);
                  },
                  success: function (data) {
                      var html = '';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="hosting">Chọn Hosting</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="hosting_id" id="choose_upgrade_hosting" class="select_vps form-control" data-placeholder="Chọn Hosting" style="width: 100%;" data-value="Upgrade-hosting">';
                      html += '<option disabled selected>Chọn Hosting</option>';
                      if (data != '') {
                          $.each(data, function (index, hosting) {
                              html += '<option data-billing="'+ hosting.billing_cycle +'" value="'+ hosting.id +'">'+ hosting.domain;
                              html += ' - ' + hosting.product.name;
                              html += '</option>';
                          });
                      } else {
                          html += '<option disabled class="text-danger">Khách hàng này không có gói Hosting nào cả!</option>';
                      }
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="upgrade_hosting_2 text-center">';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="total_price_override">Sửa giá</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="total_price_override" name="total_price_override" class="form-control" type="text" placeholder="Ghi đè giá cho đơn đặt hàng" value="">';
                      html += '</div>';
                      html += '</div>';
                      $('.order_product').html(html);
                      $('.select_vps').select2();
                  },
                  error: function(e) {
                      console.log(e);
                      var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                      $('.order_product').html(html);
                  }
              });
          }
          else if (type_product == 'Change-IP') {
              $.ajax({
                  type: "get",
                  url: "/admin/orders/list_product_addon",
                  data: {user_id:user_id},
                  dataType: "json",
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('.order_product').html(html);
                  },
                  success: function (data) {
                      var html = '';
                      // console.log(data);
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="vps">Chọn VPS</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="vps_id" id="choose_change_ip_vps" class="select_vps form-control" data-placeholder="Chọn VPS" style="width: 100%;" data-value="Addon-VPS">';
                      html += '<option disabled selected>Chọn VPS</option>';
                      if (data.list_vps != '') {
                          $.each(data.list_vps, function (index, vps) {
                              html += '<option  value="'+ vps.id +'">'+ vps.ip;
                              html += '</option>';
                          });
                      } else {
                          html += '<option disabled class="text-danger">Khách hàng không có VPS</option>';
                      }
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="addon_vps_2 text-center">';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="total_price_override">Sửa giá</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="total_price_override" name="total_price_override" class="form-control" type="text" placeholder="Ghi đè giá cho đơn đặt hàng" value="">';
                      html += '</div>';
                      html += '</div>';
                      $('.order_product').html(html);
                      $('.select_vps').select2();
                  },
                  error: function(e) {
                      console.log(e);
                      var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                      $('.order_product').html(html);
                  }
              });
          }
          else if (type_product == 'Gia hạn VPS') {
              $.ajax({
                  type: "get",
                  url: "/admin/orders/list_expire_vps",
                  data: {user_id:user_id},
                  dataType: "json",
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('.order_product').html(html);
                  },
                  success: function (data) {
                      var html = '';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="vps">Chọn VPS</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="vps_id[]" id="choose_expire_ip_vps" multiple  class="select_vps form-control" data-placeholder="Chọn VPS" style="width: 100%;" data-value="Addon-VPS">';
                      html += '<option disabled>Chọn VPS</option>';
                      if (data.list_vps != '') {
                          $.each(data.list_vps, function (index, vps) {
                              html += '<option  value="'+ vps.id +'" data-total="'+ vps.total_vps +'">'+ vps.ip + vps.text_price;
                              html += '</option>';
                          });
                      } else {
                          html += '<option disabled class="text-danger">Khách hàng không có VPS</option>';
                      }
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="billing_cycle">Thời gian</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="billing_cycle" id="billing_cycle_expire_vps" class="select2 form-control"  style="width: 100%;">';
                      $.each(data.billing, function (index2, billing) {
                          html += '<option value="'+ index2 +'">'+ billing +'</option>';
                      });
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="total_price_override">Giá ghi đè</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="total_price_override" name="total_price_override" class="form-control" type="text" placeholder="Ghi đè giá cho đơn đặt hàng" value="">';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="price_override">Trừ tiền tài khoản</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<div class=" icheck-primary d-inline">';
                      html += '<input class="custom-control-input mb-2" type="checkbox" name="check_remove_credit" id="check_remove_credit" value="1">';
                      html += '<label for="check_remove_credit"></label>';
                      html += '</div>';
                      html += '</div>';
                      html += '</div>';
                      $('.order_product').html(html);
                      $('.select_vps').select2();
                  },
                  error: function(e) {
                      console.log(e);
                      var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                      $('.order_product').html(html);
                  }
              });
          }
          else if (type_product == 'Gia hạn VPS US') {
              $.ajax({
                  type: "get",
                  url: "/admin/orders/list_expire_vps_us",
                  data: {user_id:user_id},
                  dataType: "json",
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('.order_product').html(html);
                  },
                  success: function (data) {
                      var html = '';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="vps">Chọn VPS</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="vps_id[]" id="choose_expire_ip_vps" multiple class="select_vps form-control" data-placeholder="Chọn VPS" style="width: 100%;" data-value="Addon-VPS">';
                      html += '<option disabled>Chọn VPS</option>';
                      if (data.list_vps != '') {
                          $.each(data.list_vps, function (index, vps) {
                              html += '<option  value="'+ vps.id +'" data-total="'+ vps.total_vps +'">'+ vps.ip + vps.text_price;
                              html += '</option>';
                          });
                      } else {
                          html += '<option disabled class="text-danger">Khách hàng không có VPS</option>';
                      }
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="billing_cycle">Thời gian</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="billing_cycle" id="billing_cycle_expire_vps" class="select2 form-control"  style="width: 100%;">';
                      $.each(data.billing, function (index2, billing) {
                          html += '<option value="'+ index2 +'">'+ billing +'</option>';
                      });
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="total_price_override">Giá ghi đè</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="total_price_override" name="total_price_override" class="form-control" type="text" placeholder="Ghi đè giá cho đơn đặt hàng" value="">';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="price_override">Trừ tiền tài khoản</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<div class=" icheck-primary d-inline">';
                      html += '<input class="custom-control-input mb-2" type="checkbox" name="check_remove_credit" id="check_remove_credit" value="1">';
                      html += '<label for="check_remove_credit"></label>';
                      html += '</div>';
                      html += '</div>';
                      html += '</div>';
                      $('.order_product').html(html);
                      $('.select_vps').select2();
                  },
                  error: function(e) {
                      console.log(e);
                      var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                      $('.order_product').html(html);
                  }
              });
          }
          else if (type_product == 'Gia hạn Server') {
              // console.log('da den');
              $.ajax({
                  type: "get",
                  url: "/admin/orders/list_expire_server",
                  data: {user_id:user_id},
                  dataType: "json",
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('.order_product').html(html);
                  },
                  success: function (data) {
                      // console.log(data);
                      var html = '';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="choose_expire_ip_server">Chọn Server</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="server_id" id="choose_expire_ip_server" class="select_server form-control" data-placeholder="Chọn Server" style="width: 100%;" data-value="Addon-VPS">';
                      html += '<option disabled selected>Chọn Server</option>';
                      if (data.list_server != '') {
                          $.each(data.list_server, function (index, server) {
                              html += '<option  value="'+ server.id +'" data-total="'+ server.total_vps +'">'+ server.ip + server.text_price;
                              html += '</option>';
                          });
                      } else {
                          html += '<option disabled class="text-danger">Khách hàng không có VPS</option>';
                      }
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="billing_cycle_expire_server">Thời gian</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="billing_cycle" id="billing_cycle_expire_server" class="select2 form-control"  style="width: 100%;">';
                      $.each(data.billing, function (index2, billing) {
                          html += '<option value="'+ index2 +'">'+ billing +'</option>';
                      });
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="total_price_override">Giá</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="total_override_server" name="total_override_server" class="form-control" type="number" placeholder="Giá cho đơn đặt hàng" value="">';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="price_override">Trừ tiền tài khoản</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<div class=" icheck-primary d-inline">';
                      html += '<input class="custom-control-input mb-2" type="checkbox" name="check_remove_credit" id="check_remove_credit" value="1">';
                      html += '<label for="check_remove_credit"></label>';
                      html += '</div>';
                      html += '</div>';
                      html += '</div>';
                      $('.order_product').html(html);
                      $('.select_vps').select2();
                  },
                  error: function(e) {
                      console.log(e);
                      var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                      $('.order_product').html(html);
                  }
              });
          }
          else if (type_product == 'Gia hạn Colocation') {
              // console.log('da den');
              $.ajax({
                  type: "get",
                  url: "/admin/orders/list_expire_colocation",
                  data: {user_id:user_id},
                  dataType: "json",
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('.order_product').html(html);
                  },
                  success: function (data) {
                      // console.log(data);
                      var html = '';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="choose_expire_ip_colocation">Chọn Colocation</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="colo_id" id="choose_expire_ip_colocation" class="select_server form-control" data-placeholder="Chọn Server" style="width: 100%;" data-value="Addon-VPS">';
                      html += '<option disabled selected>Chọn Colocation</option>';
                      if (data.list_colo != '') {
                          $.each(data.list_colo, function (index, colo) {
                              html += '<option  value="'+ colo.id +'" data-total="'+ colo.total_colo +'">'+ colo.ip + colo.text_price;
                              html += '</option>';
                          });
                      } else {
                          html += '<option disabled class="text-danger">Khách hàng không có Colocation</option>';
                      }
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="billing_cycle_expire_colo">Thời gian</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="billing_cycle" id="billing_cycle_expire_colo" class="select2 form-control"  style="width: 100%;">';
                      $.each(data.billing, function (index2, billing) {
                          html += '<option value="'+ index2 +'">'+ billing +'</option>';
                      });
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="col_amount">Giá</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="col_amount" name="col_amount" class="form-control" type="number" placeholder="Giá cho đơn đặt hàng" value="">';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="price_override">Trừ tiền tài khoản</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<div class=" icheck-primary d-inline">';
                      html += '<input class="custom-control-input mb-2" type="checkbox" name="check_remove_credit" id="check_remove_credit" value="1">';
                      html += '<label for="check_remove_credit"></label>';
                      html += '</div>';
                      html += '</div>';
                      html += '</div>';
                      $('.order_product').html(html);
                      $('.select_vps').select2();
                  },
                  error: function(e) {
                      console.log(e);
                      var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                      $('.order_product').html(html);
                  }
              });
          }
          else if (type_product == 'Gia hạn Hosting') {
              $.ajax({
                  type: "get",
                  url: "/admin/orders/list_expire_hosting",
                  data: {user_id:user_id},
                  dataType: "json",
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('.order_product').html(html);
                  },
                  success: function (data) {
                      // console.log(data);
                      var html = '';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="choose_expire_id_hosting">Chọn Hosting</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="hosting_id" id="choose_expire_id_hosting" class="select_hosting form-control" data-placeholder="Chọn Hosting" style="width: 100%;" data-value="Gia-han-Hosting">';
                      html += '<option disabled selected>Chọn Hosting</option>';
                      if (data.list_hosting != '') {
                          $.each(data.list_hosting, function (index, hosting) {
                              html += '<option  value="'+ hosting.id +'" data-total="'+ hosting.total_hosting +'">'+ hosting.domain + hosting.text_price;
                              html += '</option>';
                          });
                      } else {
                          html += '<option disabled class="text-danger">Khách hàng không có Hosting</option>';
                      }
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="billing_cycle">Thời gian</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select disabled name="billing_cycle" id="billing_cycle_expire_hosting" class="select2 form-control"  style="width: 100%;">';
                      $.each(data.billing, function (index2, billing) {
                          html += '<option value="'+ index2 +'">'+ billing +'</option>';
                      });
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="total_price_override">Giá ghi đè</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="total_price_override" name="total_price_override_hosting" class="form-control" type="text" placeholder="Ghi đè giá cho đơn đặt hàng" value="">';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="price_override">Trừ tiền tài khoản</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<div class=" icheck-primary d-inline">';
                      html += '<input class="custom-control-input mb-2" type="checkbox" name="check_remove_credit" id="check_remove_credit" value="1">';
                      html += '<label for="check_remove_credit"></label>';
                      html += '</div>';
                      html += '</div>';
                      html += '</div>';
                      $('.order_product').html(html);
                      $('.select_hosting').select2();
                  },
                  error: function(e) {
                      console.log(e);
                      var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                      $('.order_product').html(html);
                  }
              });
          }
          else if (type_product == 'Gia hạn Email Hosting') {
              $.ajax({
                  type: "get",
                  url: "/admin/orders/list_expire_email_hosting",
                  data: {user_id:user_id},
                  dataType: "json",
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('.order_product').html(html);
                  },
                  success: function (data) {
                      // console.log(data);
                      var html = '';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="choose_expire_id_email_hosting">Chọn Email Hosting</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select name="email_hosting_id" id="choose_expire_id_email_hosting" class="select_hosting form-control" data-placeholder="Chọn Hosting" style="width: 100%;" data-value="Gia-han-Hosting">';
                      html += '<option disabled selected>Chọn Email Hosting</option>';
                      if (data.list_hosting != '') {
                          $.each(data.list_hosting, function (index, hosting) {
                              html += '<option  value="'+ hosting.id +'" data-total="'+ hosting.total_hosting +'">'+ hosting.domain + hosting.text_price;
                              html += '</option>';
                          });
                      } else {
                          html += '<option disabled class="text-danger">Khách hàng không có Hosting</option>';
                      }
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="billing_cycle">Thời gian</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<select disabled name="billing_cycle" id="billing_cycle_expire_email_hosting" class="select2 form-control"  style="width: 100%;">';
                      $.each(data.billing, function (index2, billing) {
                          html += '<option value="'+ index2 +'">'+ billing +'</option>';
                      });
                      html += '</select>';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="total_price_override">Giá ghi đè</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<input id="total_price_override" name="total_price_override_hosting" class="form-control" type="text" placeholder="Ghi đè giá cho đơn đặt hàng" value="">';
                      html += '</div>';
                      html += '</div>';
                      // 1 form-group
                      html += '<div class="row form-group">';
                      html += '<div class="col col-md-3 text-right ">';
                      html += '<label for="price_override">Trừ tiền tài khoản</label>';
                      html += '</div>';
                      html += '<div class="col col-md-8 order-right">';
                      html += '<div class=" icheck-primary d-inline">';
                      html += '<input class="custom-control-input mb-2" type="checkbox" name="check_remove_credit" id="check_remove_credit" value="1">';
                      html += '<label for="check_remove_credit"></label>';
                      html += '</div>';
                      html += '</div>';
                      html += '</div>';
                      $('.order_product').html(html);
                      $('.select_hosting').select2();
                  },
                  error: function(e) {
                      console.log(e);
                      var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                      $('.order_product').html(html);
                  }
              });
          }
        }
        else {
          $('.notification').html('<span class="text-danger">Vui lòng chọn khách hàng trước khi chọn loại dịch vụ.</span>');
        }
    });

    $(document).on('change', '#choose_addon_server', function () {
        // console.log(total);
        var text_name_server = $('#choose_addon_server :selected').text();
        $('.ordersummary_product .text-none').text('Addon Server ' + text_name_server);
    })

    $(document).on('change', '#choose_expire_ip_colocation', function () {
        // console.log(total);
        var text_name_colocation = $('#choose_expire_ip_colocation :selected').text();
        $('.ordersummary_product .text-none').text('Gia hạn Colocation ' + text_name_colocation);
    })

    $(document).on('keyup', '#total_addon_server', function () {
        // console.log(total);
        var total = $(this).val();
        $('.sub_total_pricing').html( addCommas(total) + ' VNĐ' );
        $('.total_pricing').html( addCommas(total) + ' VNĐ' );
    })

    $(document).on('keyup', '#col_amount', function () {
        // console.log(total);
        var total = $(this).val();
        $('.sub_total_pricing').html( addCommas(total) + ' VNĐ' );
        $('.total_pricing').html( addCommas(total) + ' VNĐ' );
    })

    $(document).on('change', '#choose_expire_ip_server', function () {
        var total = $('#choose_expire_ip_server :selected').attr('data-total');
        // console.log(total);
        var text_name_server = $('#choose_expire_ip_server :selected').text();
        $('.ordersummary_product .text-none').text(text_name_server);
        $('.sub_total_pricing').html( addCommas(total) + ' VNĐ' );
        $('.total_pricing').html( addCommas(total) + ' VNĐ' );
        $('#total_override_server').val(total);
    })

    $(document).on('keyup', '#total_override_server', function () {
        var total = $(this).val();
        $('.sub_total_pricing').html( addCommas(total) + ' VNĐ' );
        $('.total_pricing').html( addCommas(total) + ' VNĐ' );
    })

    $(document).on('keyup', '#amount', function () {
        var amount = $(this).val();
        var qtt = $('#server_quantity').val();
        var server_name = $('#server_name').val();
        total = amount * qtt;
        $('.ordersummary_product .text-none').text('Server ' + server_name);
        $('.sub_total_pricing').html( addCommas(amount) + ' VNĐ' );
        $('.total_pricing').html( addCommas(total) + ' VNĐ' );
    })

    $(document).on('keyup', '#server_quantity', function () {
        var amount = $('#amount').val();
        var qtt = $('#server_quantity').val();
        var server_name = $('#server_name').val();
        total = amount * qtt;
        $('.ordersummary_product .text-none').text('Server ' + server_name);
        $('.sub_total_pricing').html( addCommas(amount) + ' VNĐ' );
        $('.total_pricing').html( addCommas(total) + ' VNĐ' );
    })
    // gia hạn VPS
    $(document).on('change', '#choose_expire_ip_vps', function () {
        var total = $('#choose_expire_ip_vps :selected').attr('data-total');
        var text_name_vps = $('#choose_expire_ip_vps :selected').text();
        $('.ordersummary_product .text-none').text(text_name_vps);
        $('.sub_total_pricing').html( addCommas(total) + ' VNĐ' );
        $('.total_pricing').html( addCommas(total) + ' VNĐ' );
    })
    $(document).on('change', '#billing_cycle_expire_vps', function () {
        var billing_cycle = $(this).val();
        var vps_id = $('#choose_expire_ip_vps :selected').val();
        $.ajax({
            type: "get",
            url: "/admin/orders/get_total_vps_expire",
            data: { id: vps_id , billing_cycle: billing_cycle },
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.ordersummary_product').html(html);
            },
            success: function (data) {
                console.log(data);
                var html = '';
                $('.sub_total_pricing').html( addCommas(data) + ' VNĐ' );
                $('.total_pricing').html( addCommas(data) + ' VNĐ' );
                $('.ordersummary_product').html('');
            },
            error: function(e) {
                console.log(e);
            }
        });
    })
    $(document).on('keyup', '#total_price_override', function () {
        var total = $(this).val();
        $('.sub_total_pricing').html( addCommas(total) + ' VNĐ' );
        $('.total_pricing').html( addCommas(total) + ' VNĐ' );
    })
    // gia hạn VPS
    $(document).on('change', '#choose_expire_id_hosting', function () {
        var total = $('#choose_expire_id_hosting :selected').attr('data-total');
        var text_name_vps = $('#choose_expire_id_hosting :selected').text();
        $('.ordersummary_product .text-none').text(text_name_vps);
        $('.sub_total_pricing').html( addCommas(total) + ' VNĐ' );
        $('.total_pricing').html( addCommas(total) + ' VNĐ' );
        $('#billing_cycle_expire_hosting').attr('disabled', false);
    })
    $(document).on('change', '#billing_cycle_expire_hosting', function () {
        var billing_cycle = $(this).val();
        var vps_id = $('#choose_expire_id_hosting :selected').val();
        $.ajax({
            type: "get",
            url: "/admin/orders/get_total_hosting_expire",
            data: { id: vps_id , billing_cycle: billing_cycle },
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.ordersummary_product').html(html);
            },
            success: function (data) {
                var html = '';
                $('.sub_total_pricing').html( addCommas(data) + ' VNĐ' );
                $('.total_pricing').html( addCommas(data) + ' VNĐ' );
                $('.ordersummary_product').html('');
            },
            error: function(e) {
                console.log(e);
            }
        });
    })
    // gia hạn email hosting
    $(document).on('change', '#choose_expire_id_email_hosting', function () {
        var total = $('#choose_expire_id_email_hosting :selected').attr('data-total');
        var text_name_vps = $('#choose_expire_id_email_hosting :selected').text();
        $('.ordersummary_product .text-none').text('Gia hạn Email Hosting' + text_name_vps);
        $('.sub_total_pricing').html( addCommas(total) + ' VNĐ' );
        $('.total_pricing').html( addCommas(total) + ' VNĐ' );
        $('#billing_cycle_expire_email_hosting').attr('disabled', false);
    })
    $(document).on('change', '#billing_cycle_expire_email_hosting', function () {
        var billing_cycle = $(this).val();
        var vps_id = $('#choose_expire_id_email_hosting :selected').val();
        $.ajax({
            type: "get",
            url: "/admin/orders/get_total_email_hosting_expire",
            data: { id: vps_id , billing_cycle: billing_cycle },
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.ordersummary_product').html(html);
            },
            success: function (data) {
                var html = '';
                $('.sub_total_pricing').html( addCommas(data) + ' VNĐ' );
                $('.total_pricing').html( addCommas(data) + ' VNĐ' );
                $('.ordersummary_product').html('');
            },
            error: function(e) {
                console.log(e);
            }
        });
    })
    $(document).on('keyup', '#total_price_override_hosting', function () {
        var total = $(this).val();
        $('.sub_total_pricing').html( addCommas(total) + ' VNĐ' );
        $('.total_pricing').html( addCommas(total) + ' VNĐ' );
    })

    $(document).on('change', '#choose_addon_vps', function () {
       var billing = $('#choose_addon_vps :selected').attr('data-billing');
       var vps_id = $(this).val();
       var user_id = $('#user').val();
       // console.log(billing, user_id);
       $.ajax({
           type: "get",
           url: "/admin/orders/choose_addon_vps",
           data: {user_id:user_id, vps_id:vps_id},
           dataType: "json",
           beforeSend: function(){
               var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
               $('.addon_vps_2').html(html);
           },
           success: function (data) {
              // console.log(data);
               var html = '';
               if (data != '' || data != null) {
                   html += '<div class="form-group addon-product mt-4 mb-4 col-12">';
                   html += '<input name="addon" value="1" id="addon-hidden" type="hidden">';
                   html += '<label for="addon" class="text-danger mb-4">Chọn gói Addon</label>';
                   html += '<div class="col-12">';
                   $.each(data, function(index, product) {
                       var type = '';
                       var label = '';
                       // console.log(product, product.meta_product.type_addon);
                       if (product.meta_product.type_addon == 'addon_cpu') {
                           type = 'cpu';
                           label = 'CPU';
                           html += '<input name="id-addon-cpu" value="'+product.id+'" id="addon-hidden" type="hidden">';
                       }
                       if (product.meta_product.type_addon == 'addon_ram') {
                           type = 'ram';
                           label = 'Ram';
                           html += '<input name="id-addon-ram" value="'+product.id+'" id="addon-hidden" type="hidden">';
                       }
                       if (product.meta_product.type_addon == 'addon_disk') {
                           type = 'disk';
                           label = 'Disk';
                           html += '<input name="id-addon-disk" value="'+product.id+'" id="addon-hidden" type="hidden">';
                       }
                       if (type == 'disk') {
                           html += '<div class="form-group row">';
                           html += '<div class="col col-md-3 text-right ">';
                           html += '<label>' + label + '</lable> <br>';
                           html += '</div>';
                           html += '<div class="col-md-8 order-right vps_addon_'+type+'">';
                           html += '<input name="addon_'+type+'" type="number" value="0" min="0" max="200" step="10" data-name="'+ product.name +'" data-pricing="'+ product.total +'" />';
                           html += '</div>';
                           html += '</div>';
                       } else {
                           html += '<div class="form-group row">';
                           html += '<div class="col col-md-3 text-right ">';
                           html += '<label>' + label + '</lable> <br>';
                           html += '</div>';
                           html += '<div class="col-md-8 order-right text-center vps_addon_'+type+'">';
                           html += '<input name="addon_'+type+'" type="number" value="0" min="0" max="16" step="1" data-name="'+ product.name +'" data-pricing="'+ product.total +'" />';
                           html += '</div>';
                           html += '</div>';
                       }
                   });
                   html += '</div>';
                   html += '</div>';
                   $('.addon_vps_2').html(html);
                   $("input[type='number']").inputSpinner()
               } else {
                   $('.addon_vps_2').html('Không có gói Addon Product cho gói sản phẩm này.');
               }
           },
           error: function (e) {
             console.log(e);
             $('.addon_vps_2').html('<span class="text-danger">Truy vẫn thêm cấu hình của sản phẩm lỗi.</span>');
           }
       })
    });

    $(document).on('change', '#choose_upgrade_hosting', function () {
        var billing = $('#choose_upgrade_hosting :selected').attr('data-billing');
        var text_name_hosting = $('#choose_upgrade_hosting :selected').text();
        // console.log(text_name_hosting);
        var hosting_id = $(this).val();
        var user_id = $('#user').val();
        $('.ordersummary_product .text-none').text(text_name_hosting);
        $.ajax({
            type: "get",
            url: "/admin/orders/choose_upgrade_hosting",
            data: {user_id:user_id, hosting_id:hosting_id},
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.upgrade_hosting_2').html(html);
            },
            success: function (data) {
               // console.log(data);
                var html = '';
                if (data != '') {
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '<label for="hosting">Chọn gói Hosting nâng cấp</label>';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right">';
                    html += '<select name="product_upgradet" id="choose_upgrade_product" class="select_vps form-control" data-placeholder="Chọn gói Hosting nâng cấp" style="width: 100%;" data-value="Upgrade-hosting">';
                    html += '<option disabled selected>Chọn gói sản phẩm Hosting muốn nâng cấp</option>';
                    if (data != '') {
                        $.each(data, function (index, product) {
                            html += '<option data-pricing="'+ product.total +'" value="'+ product.id +'">'+ product.name;
                            html += '</option>';
                        });
                    } else {
                        html += '<option disabled class="text-danger">Khách hàng này không có gói Hosting nào cả!</option>';
                    }
                    html += '</select>';
                    html += '</div>';
                    html += '</div>';
                    $('.upgrade_hosting_2').html(html);
                    $('.select_vps').select2();
                } else {
                    $('.addon_vps_2').html('Không có gói Addon Product cho gói sản phẩm này.');
                    $('.upgrade_hosting_2').html(html);
                }
            },
            error: function (e) {
              console.log(e);
              $('.upgrade_hosting_2').html('<span class="text-danger">Truy vẫn thêm cấu hình của sản phẩm lỗi.</span>');
            }
        })
    })

    $(document).on('change', '#choose_change_ip_vps', function () {
       var user_id = $('#user').val();
       var text = $('#choose_change_ip_vps :selected').text();
       $('.ordersummary_product .text-none').text('Đổi IP của VPS: ' + text);
       $.ajax({
           type: "get",
           url: "/admin/orders/choose_change_ip_vps",
           data: {user_id:user_id},
           dataType: "json",
           beforeSend: function(){
             var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
             $('.addon_vps_2').html(html);
           },
           success: function (data) {
             var html = '';
             console.log(data);
             if (data != '') {
               $('.addon_vps_2').html(html);
               $('.sub_total_pricing').text(addCommas(data) + ' VNĐ');
               $('.total_pricing').text(addCommas(data) + ' VNĐ');
             } else {
               $('.addon_vps_2').html('<div class="text-center">Không có gói đổi IP cho VPS này </div>');
             }
           },
           error: function(e) {
             console.log(e);
             var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
             $('.addon_vps_2').html(html);
           }
       });
    })

    $(document).on('change', '#choose_upgrade_product', function () {
        var total = $('#choose_upgrade_product :selected').attr('data-pricing');
        // console.log(total);
        $('.sub_total_pricing').text(addCommas(total) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
    })

    $(document).on('change', '#product_id', function() {
        var id = $(this).val();
        var type_products = $('#type_products').val();
        if (type_products == 'Hosting' || type_products == 'Hosting-Singapore' || type_products == 'Email Hosting') {
            var qtt = $('#quantity_hosting').val();
        } else {
            var qtt = $('#quantity').val();
        }
        var billing = $('#billing_cycle').val();
        $.ajax({
            type: "get",
            url: "/admin/orders/get_product",
            data: { id: id },
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.ordersummary_product').html(html);
            },
            success: function (data) {
                var html = '';
                if(data.pricing.type) {
                    if(data.pricing.type == 'free') {
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">0 VNĐ</div>';
                            html += '</div></div>';
                        }
                        $('#form_total').val(0);
                        $('#form_sub_total').val(0);
                    } else if(data.pricing.type == 'recurring') {
                        var price = 0;
                        var sub_total = 0;
                        var total = 0;
                        $.each(data.pricing, function (index_pricing, pricing) {
                            if(index_pricing == billing) {
                                if (pricing == 0) {
                                    if(data.pricing.monthly > 0) {
                                        price = data.pricing.monthly;
                                        billing = 'monthly';
                                    }
                                    else if (data.pricing.quarterly > 0) {
                                        price = data.pricing.quarterly;
                                        billing = 'quarterly';
                                    }
                                    else if (data.pricing.semi_annually > 0) {
                                        price = data.pricing.semi_annually;
                                        billing = 'semi_annually';
                                    }
                                    else if (data.pricing.annually > 0) {
                                        price = data.pricing.annually;
                                        billing = 'annually';
                                    }
                                    else if (data.pricing.biennially > 0) {
                                        price = data.pricing.biennially;
                                        billing = 'biennially';
                                    } else {
                                        price = data.pricing.triennially;
                                        billing = 'triennially';
                                    }
                                } else {
                                    price = pricing;
                                }
                            }
                        });
                        for (let i = 0; i < qtt; i++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ addCommas(price) +' VNĐ</div>';
                            html += '</div></div>';
                        }
                        total = price * qtt;
                        sub_total = price * qtt;
                        $('#form_total').val(total);
                        $('#form_sub_total').val(sub_total);
                        $('.sub_total_pricing').html( addCommas(sub_total) + ' VNĐ');
                        $('#sub_total').val(sub_total);
                        $('.total_pricing').html( addCommas(total) + ' VNĐ');
                    } else {
                        var sub_total = 0;
                        var total = 0;
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">Vĩnh viễn</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ data.pricing.one_time_pay +' VNĐ</div>';
                            html += '</div></div>';
                        }
                        total = data.pricing.one_time_pay * qtt;
                        sub_total = data.pricing.one_time_pay * qtt;
                        $('#form_total').val(total);
                        $('#form_sub_total').val(sub_total);
                        $('.sub_total_pricing').html( addCommas(sub_total) + ' VNĐ');
                        $('#sub_total').val(sub_total);
                        $('.total_pricing').html( addCommas(total) + ' VNĐ');
                    }
                    $('.ordersummary_product').html(html);
                }
                if (type_products == 'VPS' || type_products == 'NAT-VPS' || type_products == 'VPS-US') {
                  var html2 = '<select class="form-control" id="os" name="os">';
                  if (data.os_product != '') {
                    $.each(data.os_product, function (index, os_product) {
                       html2 += '<option value="' + os_product.os + '">'+ data.os[os_product.os]  +'</option>'
                    })
                  }
                  else {
                    $.each(data.os, function (key, os_os) {
                       html2 += '<option value="' + key + '">'+ os_os  +'</option>'
                    })
                  }
                  html2 += '</select>';
                }
                $('#change_os').html(html2);
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

    $(document).on('change', '#billing_cycle', function() {
        var id = $('#product_id').val();
        var type_products = $('#type_products').val();
        if (type_products == 'Hosting' || type_products == 'Hosting-Singapore' || type_products == 'Email Hosting') {
            var qtt = $('#quantity_hosting').val();
        } else {
            var qtt = $('#quantity').val();
        }
        var billing = $(this).val();
        $.ajax({
            type: "get",
            url: "/admin/orders/get_product",
            data: { id: id },
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.ordersummary_product').html(html);
            },
            success: function (data) {
                var html = '';
                if(data.pricing.type) {
                    if(data.pricing.type == 'free') {
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">0 VNĐ</div>';
                            html += '</div></div>';
                        }
                        $('#form_total').val(0);
                        $('#form_sub_total').val(0);
                    } else if(data.pricing.type == 'recurring') {
                        var price = 0;
                        var sub_total = 0;
                        var total = 0;
                        $.each(data.pricing, function (index_pricing, pricing) {
                            if(index_pricing == billing) {
                                if (pricing == 0) {
                                    if(data.pricing.monthly > 0) {
                                        price = data.pricing.monthly;
                                        billing = 'monthly';
                                    }
                                    else if (data.pricing.quarterly > 0) {
                                        price = data.pricing.quarterly;
                                        billing = 'quarterly';
                                    }
                                    else if (data.pricing.semi_annually > 0) {
                                        price = data.pricing.semi_annually;
                                        billing = 'semi_annually';
                                    }
                                    else if (data.pricing.annually > 0) {
                                        price = data.pricing.annually;
                                        billing = 'annually';
                                    }
                                    else if (data.pricing.biennially > 0) {
                                        price = data.pricing.biennially;
                                        billing = 'biennially';
                                    } else {
                                        price = data.pricing.triennially;
                                        billing = 'triennially';
                                    }
                                } else {
                                    price = pricing;
                                }
                            }
                        });
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ addCommas(price) +' VNĐ</div>';
                            html += '</div></div>';
                        }
                        total = price * qtt;
                        sub_total = price * qtt;
                        $('#form_total').val(total);
                        $('#form_sub_total').val(sub_total);
                        $('.sub_total_pricing').html( addCommas(sub_total) + ' VNĐ');
                        $('.total_pricing').html( addCommas(total) + ' VNĐ');
                    } else {
                        var sub_total = 0;
                        var total = 0;
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">Vĩnh viễn</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ addCommas(data.pricing.one_time_pay) +' VNĐ</div>';
                            html += '</div></div>';
                        }
                        total = data.pricing.one_time_pay * qtt;
                        sub_total = data.pricing.one_time_pay * qtt;
                        $('#form_total').val(total);
                        $('#form_sub_total').val(sub_total);
                        $('.sub_total_pricing').html( addCommas(sub_total) + ' VNĐ');
                        $('.total_pricing').html( addCommas(total) + ' VNĐ');
                    }
                    $('.ordersummary_product').html(html);
                }
                var html2 = '';
                html2 += '<button class="button_vps btn btn-default"><i class="far fa-plus-square"></i> Thêm cấu hình</button>';
                $('.check_addon_vps').html(html2);
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

    $(document).on('keyup', '#quantity', function() {
        var id = $('#product_id').val();
        var type_products = $('#type_products').val();
        if (type_products == 'Hosting') {
            var qtt = $('#quantity_hosting').val();
        } else {
            var qtt = $('#quantity').val();
        }
        var billing = $('#billing_cycle').val();
        $.ajax({
            type: "get",
            url: "/admin/orders/get_product",
            data: { id: id },
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.ordersummary_product').html(html);
            },
            success: function (data) {
                var html = '';
                if(data.pricing.type) {
                    if(data.pricing.type == 'free') {
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">0 VNĐ</div>';
                            html += '</div></div>';
                        }
                        $('#form_total').val(0);
                        $('#form_sub_total').val(0);
                    } else if(data.pricing.type == 'recurring') {
                        var price = 0;
                        var sub_total = 0;
                        var total = 0;
                        $.each(data.pricing, function (index_pricing, pricing) {
                            if(index_pricing == billing) {
                                if (pricing == 0) {
                                    if(data.pricing.monthly > 0) {
                                        price = data.pricing.monthly;
                                        billing = 'monthly';
                                    }
                                    else if (data.pricing.quarterly > 0) {
                                        price = data.pricing.quarterly;
                                        billing = 'quarterly';
                                    }
                                    else if (data.pricing.semi_annually > 0) {
                                        price = data.pricing.semi_annually;
                                        billing = 'semi_annually';
                                    }
                                    else if (data.pricing.annually > 0) {
                                        price = data.pricing.annually;
                                        billing = 'annually';
                                    }
                                    else if (data.pricing.biennially > 0) {
                                        price = data.pricing.biennially;
                                        billing = 'biennially';
                                    } else {
                                        price = data.pricing.triennially;
                                        billing = 'triennially';
                                    }
                                } else {
                                    price = pricing;
                                }
                            }
                        });
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ addCommas(price) +' VNĐ</div>';
                            html += '</div></div>';
                        }
                        total = price * qtt;
                        sub_total = price * qtt;
                        $('#form_total').val(total);
                        $('#form_sub_total').val(sub_total);
                        $('.sub_total_pricing').html( addCommas(sub_total) + ' VNĐ');
                        $('.total_pricing').html( addCommas(total) + ' VNĐ');
                    } else {
                        var sub_total = 0;
                        var total = 0;
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">Vĩnh viễn</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ addCommas(data.pricing.one_time_pay) +' VNĐ</div>';
                            html += '</div></div>';
                        }
                        total = data.pricing.one_time_pay * qtt;
                        sub_total = data.pricing.one_time_pay * qtt;
                        $('#form_total').val(total);
                        $('#form_sub_total').val(sub_total);
                        $('.sub_total_pricing').html( addCommas(sub_total) + ' VNĐ');
                        $('.total_pricing').html( addCommas(total) + ' VNĐ');
                    }
                    $('.ordersummary_product').html(html);
                }
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

    $(document).on('keyup', '#quantity_hosting', function() {
        var id = $('#product_id').val();
        var qtt = $('#quantity_hosting').val();
        var billing = $('#billing_cycle').val();
        $.ajax({
            type: "get",
            url: "/admin/orders/get_product",
            data: { id: id },
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.ordersummary_product').html(html);
            },
            success: function (data) {
                var html = '';
                var html_domain = '';
                if(data.pricing.type) {
                    if(data.pricing.type == 'free') {
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">0 VNĐ</div>';
                            html += '</div></div>';
                        }
                        $('#form_total').val(0);
                        $('#form_sub_total').val(0);
                    } else if(data.pricing.type == 'recurring') {
                        var price = 0;
                        var sub_total = 0;
                        var total = 0;
                        $.each(data.pricing, function (index_pricing, pricing) {
                            if(index_pricing == billing) {
                                if (pricing == 0) {
                                    if(data.pricing.monthly > 0) {
                                        price = data.pricing.monthly;
                                        billing = 'monthly';
                                    }
                                    else if (data.pricing.quarterly > 0) {
                                        price = data.pricing.quarterly;
                                        billing = 'quarterly';
                                    }
                                    else if (data.pricing.semi_annually > 0) {
                                        price = data.pricing.semi_annually;
                                        billing = 'semi_annually';
                                    }
                                    else if (data.pricing.annually > 0) {
                                        price = data.pricing.annually;
                                        billing = 'annually';
                                    }
                                    else if (data.pricing.biennially > 0) {
                                        price = data.pricing.biennially;
                                        billing = 'biennially';
                                    } else {
                                        price = data.pricing.triennially;
                                        billing = 'triennially';
                                    }
                                } else {
                                    price = pricing;
                                }
                            }
                        });
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ price +' VNĐ</div>';
                            html += '</div></div>';
                            // them the input de nhap domain
                            var n = index;
                            if (n > 0) {
                                html_domain += '<input name="domain[]" class="form-control" type="text" placeholder="Domain '+ (n + 1) +'">';
                            }
                        }
                        total = price * qtt;
                        sub_total = price * qtt;
                        $('#form_total').val(total);
                        $('#form_sub_total').val(sub_total);
                        $('.sub_total_pricing').html( addCommas(sub_total) + ' VNĐ');
                        $('.total_pricing').html( addCommas(total) + ' VNĐ');
                    } else {
                        var sub_total = 0;
                        var total = 0;
                        for (let index = 0; index < qtt; index++) {

                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">Vĩnh viễn</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ data.pricing.one_time_pay +' VNĐ</div>';
                            html += '</div></div>';
                        }
                        total = data.pricing.one_time_pay * qtt;
                        sub_total = data.pricing.one_time_pay * qtt;
                        $('#form_total').val(total);
                        $('#form_sub_total').val(sub_total);
                        $('.sub_total_pricing').html( addCommas(sub_total) + ' VNĐ');
                        $('.total_pricing').html( addCommas(total) + ' VNĐ');
                    }
                    $('.ordersummary_product').html(html);
                    $('#domain').attr('name', 'domain[]');
                    $('.domain').append(html_domain);
                }
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

    $(document).on('click', '.button_vps', function (event) {
        event.preventDefault();
        var user_id = $('#user').val();
        var billing = $('#billing_cycle').val();
        $.ajax({
            type: "get",
            url: "/admin/orders/get_addon_product",
            data: {user_id:user_id},
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.check_addon_vps').html(html);
            },
            success: function (data) {
                var html = '';
                if (data != '' || data != null) {
                    html += '<div class="form-group addon-product text-center mt-4 mb-4">';
                    html += '<input name="addon" value="1" id="addon-hidden" type="hidden">';
                    html += '<label for="addon" class="text-danger mb-4">Chọn gói Addon</label>';
                    html += '<div class="row">';
                    $.each(data, function(index, product) {
                        var type = '';
                        var label = '';
                        // console.log(product, product.meta_product.type_addon);
                        if (product.meta_product.type_addon == 'addon_cpu') {
                            type = 'cpu';
                            label = 'CPU';
                            html += '<input name="id-addon-cpu" value="'+product.id+'" id="addon-hidden" type="hidden">';
                        }
                        if (product.meta_product.type_addon == 'addon_ram') {
                            type = 'ram';
                            label = 'Ram';
                            html += '<input name="id-addon-ram" value="'+product.id+'" id="addon-hidden" type="hidden">';
                        }
                        if (product.meta_product.type_addon == 'addon_disk') {
                            type = 'disk';
                            label = 'Disk';
                            html += '<input name="id-addon-disk" value="'+product.id+'" id="addon-hidden" type="hidden">';
                        }
                        if (type == 'disk') {
                            html += '<div class="col-md-4 text-center addon-'+type+'">';
                            html += '<label>' + label + '</lable> <br>';
                            html += '<input name="addon_'+type+'" type="number" value="0" min="0" max="200" step="10" data-name="'+ product.name +'" data-pricing="'+ product.pricing[billing] +'" />';
                            html += '</div>';
                        } else {
                            html += '<div class="col-md-4 text-center addon-'+type+'">';
                            html += '<label>' + label + '</lable> <br>';
                            html += '<input name="addon_'+type+'" type="number" value="0" min="0" max="16" step="1" data-name="'+ product.name +'" data-pricing="'+ product.pricing[billing] +'" />';
                            html += '</div>';
                        }
                    });
                    html += '</div>';
                    html += '</div>';
                    $('.check_addon_vps').html(html);
                    $("input[type='number']").inputSpinner()
                } else {
                    $('.check_addon_vps').html('Không có gói Addon Product cho gói sản phẩm này.');
                }
            },
            error: function (e) {
              console.log(e);
              $('.check_addon_vps').html('<span class="text-danger">Truy vẫn thêm cấu hình của sản phẩm lỗi.</span>');
            }
        })
    })

    // $(document).on('click', '#check_price_override', function () {
    //    if ($(this).is(':checked')) {
    //       $('#price_override').attr('disabled', false);
    //    } else {
    //       $('#price_override').attr('disabled', true);
    //    }
    // })

    // khi click vào chọn gói addon sẽ tăng set lại sub_total
    $(document).on('click', '.addon-cpu' , function() {
        var qtt_addon = parseInt($(".addon-cpu input").val());
        var pricing = parseInt($(".addon-cpu input").attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var qtt_product = parseInt($('#quantity').val());
        var name_addon = $(".addon-cpu input").attr('data-name');
        // lay cac addon con lai
        var qtt_addon_ram = parseInt($(".addon-ram input").val());
        var pricing_ram = parseInt($(".addon-ram input").attr('data-pricing'));
        var qtt_addon_disk = parseInt($(".addon-disk input").val());
        var pricing_disk = parseInt($(".addon-disk input").attr('data-pricing'));
        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_ram * pricing_ram) + (qtt_addon_disk * pricing_disk/10);
        console.log(sub_total,qtt_product, qtt_addon_ram, pricing_ram, qtt_addon_disk, pricing_disk);
        total =  sub_total * qtt_product + (qtt_addon * pricing * qtt_product) + (qtt_addon_ram * pricing_ram * qtt_product) + (qtt_addon_disk * pricing_disk/10 * qtt_product) ;
        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon + ' cores';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-cpu').html(html);
        // console.log(total);
    });
    // khi click vào chọn gói addon sẽ tăng set lại sub_total
    $(document).on('click', '.addon-ram' , function() {
        var qtt_addon = parseInt($('.addon-ram input').val());
        var pricing = parseInt($('.addon-ram input').attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var qtt_product = parseInt($('#quantity').val());
        var name_addon = $('.addon-ram input').attr('data-name');
        // lay cac addon con load
        var qtt_addon_cpu = parseInt($(".addon-cpu input").val());
        var pricing_cpu = parseInt($(".addon-cpu input").attr('data-pricing'));
        var qtt_addon_disk = parseInt($(".addon-disk input").val());
        var pricing_disk = parseInt($(".addon-disk input").attr('data-pricing'));

        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_cpu * pricing_cpu) + (qtt_addon_disk*pricing_disk/10);
        total =  sub_total * qtt_product + (qtt_addon * pricing * qtt_product)  + (qtt_addon_disk * pricing_disk/10 * qtt_product)  + (qtt_addon_cpu * pricing_cpu * qtt_product);
        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon + ' GB';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-ram').html(html);
        // console.log(total);
    });
    // khi click vào chọn gói addon sẽ tăng set lại sub_total
    $(document).on('click', '.addon-disk' , function() {
        var qtt_addon = parseInt($('.addon-disk input').val()) / 10;
        var pricing = parseInt($('.addon-disk input').attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var qtt_product = parseInt($('#quantity').val());
        var name_addon = $('.addon-disk input').attr('data-name');
        // lay cac addon con lai
        var qtt_addon_cpu = parseInt($(".addon-cpu input").val());
        var pricing_cpu = parseInt($(".addon-cpu input").attr('data-pricing'));
        var qtt_addon_ram = parseInt($(".addon-ram input").val());
        var pricing_ram = parseInt($(".addon-ram input").attr('data-pricing'));

        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_cpu * pricing_cpu) + (qtt_addon_ram * pricing_ram);
        total =  sub_total * qtt_product + (qtt_addon * pricing * qtt_product) + (qtt_addon_cpu * pricing_cpu * qtt_product) + (qtt_addon_ram * pricing_ram * qtt_product);

        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon * 10 + ' GB';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-disk').html(html);
        // console.log(total);
    });
    // khi click vào chọn gói addon sẽ tăng set lại sub_total
    $(document).on('click', '.vps_addon_cpu' , function() {
        var qtt_addon = parseInt($(".vps_addon_cpu input").val());
        var pricing = parseInt($(".vps_addon_cpu input").attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var name_addon = $(".vps_addon_cpu input").attr('data-name');
        // lay cac addon con lai
        var qtt_addon_ram = parseInt($(".vps_addon_ram input").val());
        var pricing_ram = parseInt($(".vps_addon_ram input").attr('data-pricing'));
        var qtt_addon_disk = parseInt($(".vps_addon_disk input").val());
        var pricing_disk = parseInt($(".vps_addon_disk input").attr('data-pricing'));
        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_ram * pricing_ram) + (qtt_addon_disk * pricing_disk/10);
        // console.log(sub_total_new,sub_total, qtt_addon_ram, pricing_ram, qtt_addon_disk, pricing_disk);
        total =  sub_total + (qtt_addon * pricing) + (qtt_addon_ram * pricing_ram) + (qtt_addon_disk * pricing_disk/10) ;
        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon + ' cores';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-cpu').html(html);
        // console.log(total);
    });
    // khi click vào chọn gói addon sẽ tăng set lại sub_total
    $(document).on('click', '.vps_addon_ram' , function() {
        var qtt_addon = parseInt($('.vps_addon_ram input').val());
        var pricing = parseInt($('.vps_addon_ram input').attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var qtt_product = parseInt($('#quantity').val());
        var name_addon = $('.vps_addon_ram input').attr('data-name');
        // lay cac addon con load
        var qtt_addon_cpu = parseInt($(".vps_addon_cpu input").val());
        var pricing_cpu = parseInt($(".vps_addon_cpu input").attr('data-pricing'));
        var qtt_addon_disk = parseInt($(".vps_addon_disk input").val());
        var pricing_disk = parseInt($(".vps_addon_disk input").attr('data-pricing'));

        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_cpu * pricing_cpu) + (qtt_addon_disk*pricing_disk/10);
        total =  sub_total + (qtt_addon * pricing)  + (qtt_addon_disk * pricing_disk/10)  + (qtt_addon_cpu * pricing_cpu);
        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon + ' GB';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-ram').html(html);
        // console.log(total);
    });
    // khi click vào chọn gói addon sẽ tăng set lại sub_total
    $(document).on('click', '.vps_addon_disk' , function() {
        var qtt_addon = parseInt($('.vps_addon_disk input').val()) / 10;
        var pricing = parseInt($('.vps_addon_disk input').attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var name_addon = $('.vps_addon_disk input').attr('data-name');
        // lay cac addon con lai
        var qtt_addon_cpu = parseInt($(".vps_addon_cpu input").val());
        var pricing_cpu = parseInt($(".vps_addon_cpu input").attr('data-pricing'));
        var qtt_addon_ram = parseInt($(".vps_addon_ram input").val());
        var pricing_ram = parseInt($(".vps_addon_ram input").attr('data-pricing'));

        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_cpu * pricing_cpu) + (qtt_addon_ram * pricing_ram);
        total =  sub_total + (qtt_addon * pricing) + (qtt_addon_cpu * pricing_cpu) + (qtt_addon_ram * pricing_ram);

        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon * 10 + ' GB';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-disk').html(html);
        // console.log(total);
    });

    // khi click vào chọn gói addon sẽ tăng set lại sub_total
    $(document).on('keyup', '.addon-cpu' , function() {
        var qtt_addon = parseInt($(".addon-cpu input").val());
        var pricing = parseInt($(".addon-cpu input").attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var qtt_product = parseInt($('#quantity').val());
        var name_addon = $(".addon-cpu input").attr('data-name');
        // lay cac addon con lai
        var qtt_addon_ram = parseInt($(".addon-ram input").val());
        var pricing_ram = parseInt($(".addon-ram input").attr('data-pricing'));
        var qtt_addon_disk = parseInt($(".addon-disk input").val());
        var pricing_disk = parseInt($(".addon-disk input").attr('data-pricing'));
        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_ram * pricing_ram) + (qtt_addon_disk * pricing_disk/10);
        console.log(sub_total,qtt_product, qtt_addon_ram, pricing_ram, qtt_addon_disk, pricing_disk);
        total =  sub_total * qtt_product + (qtt_addon * pricing * qtt_product) + (qtt_addon_ram * pricing_ram * qtt_product) + (qtt_addon_disk * pricing_disk/10 * qtt_product) ;
        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon + ' cores';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-cpu').html(html);
        // console.log(total);
    });
    // khi click vào chọn gói addon sẽ tăng set lại sub_total
    $(document).on('keyup', '.addon-ram' , function() {
        var qtt_addon = parseInt($('.addon-ram input').val());
        var pricing = parseInt($('.addon-ram input').attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var qtt_product = parseInt($('#quantity').val());
        var name_addon = $('.addon-ram input').attr('data-name');
        // lay cac addon con load
        var qtt_addon_cpu = parseInt($(".addon-cpu input").val());
        var pricing_cpu = parseInt($(".addon-cpu input").attr('data-pricing'));
        var qtt_addon_disk = parseInt($(".addon-disk input").val());
        var pricing_disk = parseInt($(".addon-disk input").attr('data-pricing'));

        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_cpu * pricing_cpu) + (qtt_addon_disk*pricing_disk/10);
        total =  sub_total * qtt_product + (qtt_addon * pricing * qtt_product)  + (qtt_addon_disk * pricing_disk/10 * qtt_product)  + (qtt_addon_cpu * pricing_cpu * qtt_product);
        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon + ' GB';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-ram').html(html);
        // console.log(total);
    });
    // khi click vào chọn gói addon sẽ tăng set lại sub_total
    $(document).on('keyup', '.addon-disk' , function() {
        var qtt_addon = parseInt($('.addon-disk input').val()) / 10;
        var pricing = parseInt($('.addon-disk input').attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var qtt_product = parseInt($('#quantity').val());
        var name_addon = $('.addon-disk input').attr('data-name');
        // lay cac addon con lai
        var qtt_addon_cpu = parseInt($(".addon-cpu input").val());
        var pricing_cpu = parseInt($(".addon-cpu input").attr('data-pricing'));
        var qtt_addon_ram = parseInt($(".addon-ram input").val());
        var pricing_ram = parseInt($(".addon-ram input").attr('data-pricing'));

        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_cpu * pricing_cpu) + (qtt_addon_ram * pricing_ram);
        total =  sub_total * qtt_product + (qtt_addon * pricing * qtt_product) + (qtt_addon_cpu * pricing_cpu * qtt_product) + (qtt_addon_ram * pricing_ram * qtt_product);

        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon * 10 + ' GB';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-disk').html(html);
        // console.log(total);
    });
    // khi click vào chọn gói addon sẽ tăng set lại sub_total
    $(document).on('keyup', '.vps_addon_cpu' , function() {
        var qtt_addon = parseInt($(".vps_addon_cpu input").val());
        var pricing = parseInt($(".vps_addon_cpu input").attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var name_addon = $(".vps_addon_cpu input").attr('data-name');
        // lay cac addon con lai
        var qtt_addon_ram = parseInt($(".vps_addon_ram input").val());
        var pricing_ram = parseInt($(".vps_addon_ram input").attr('data-pricing'));
        var qtt_addon_disk = parseInt($(".vps_addon_disk input").val());
        var pricing_disk = parseInt($(".vps_addon_disk input").attr('data-pricing'));
        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_ram * pricing_ram) + (qtt_addon_disk * pricing_disk/10);
        // console.log(sub_total_new,sub_total, qtt_addon_ram, pricing_ram, qtt_addon_disk, pricing_disk);
        total =  sub_total + (qtt_addon * pricing) + (qtt_addon_ram * pricing_ram) + (qtt_addon_disk * pricing_disk/10) ;
        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon + ' cores';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-cpu').html(html);
        // console.log(total);
    });
    // khi click vào chọn gói addon sẽ tăng set lại sub_total
    $(document).on('keyup', '.vps_addon_ram' , function() {
        var qtt_addon = parseInt($('.vps_addon_ram input').val());
        var pricing = parseInt($('.vps_addon_ram input').attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var qtt_product = parseInt($('#quantity').val());
        var name_addon = $('.vps_addon_ram input').attr('data-name');
        // lay cac addon con load
        var qtt_addon_cpu = parseInt($(".vps_addon_cpu input").val());
        var pricing_cpu = parseInt($(".vps_addon_cpu input").attr('data-pricing'));
        var qtt_addon_disk = parseInt($(".vps_addon_disk input").val());
        var pricing_disk = parseInt($(".vps_addon_disk input").attr('data-pricing'));

        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_cpu * pricing_cpu) + (qtt_addon_disk*pricing_disk/10);
        total =  sub_total + (qtt_addon * pricing)  + (qtt_addon_disk * pricing_disk/10)  + (qtt_addon_cpu * pricing_cpu);
        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon + ' GB';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-ram').html(html);
        // console.log(total);
    });
    // khi click vào chọn gói addon sẽ tăng set lại sub_total
    $(document).on('keyup', '.vps_addon_disk' , function() {
        var qtt_addon = parseInt($('.vps_addon_disk input').val()) / 10;
        var pricing = parseInt($('.vps_addon_disk input').attr('data-pricing'));
        var sub_total = parseInt($('#sub_total').val());
        var name_addon = $('.vps_addon_disk input').attr('data-name');
        // lay cac addon con lai
        var qtt_addon_cpu = parseInt($(".vps_addon_cpu input").val());
        var pricing_cpu = parseInt($(".vps_addon_cpu input").attr('data-pricing'));
        var qtt_addon_ram = parseInt($(".vps_addon_ram input").val());
        var pricing_ram = parseInt($(".vps_addon_ram input").attr('data-pricing'));

        sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_cpu * pricing_cpu) + (qtt_addon_ram * pricing_ram);
        total =  sub_total + (qtt_addon * pricing) + (qtt_addon_cpu * pricing_cpu) + (qtt_addon_ram * pricing_ram);

        $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
        $('.total_pricing').text(addCommas(total) + ' VNĐ');
        var html = '';
        html += '<div class="item">';
        html += '<div class="itemtitle">';
        html += name_addon;
        html += '</div>';
        html += '<div class="row">';
        html += '<div class="col col-md-6 item_billing">';
        html += qtt_addon * 10 + ' GB';
        html += '</div>';
        html += '<div class="col col-md-6 item_pricing text-right">';
        html += addCommas(pricing * qtt_addon) + ' VNĐ';
        html += '</div>';
        html += '</div>';
        html += '</div>';
        $('.addon-item-disk').html(html);
        // console.log(total);
    });

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

});
