$(document).ready(function () {
   $('.invoice_classic').on('change', function () {
      var sl = $(this).val();
      $.ajax({
        url: '/order/list_invoices_with_sl',
        data: {sl : sl},
        dataType: 'json',
        beforeSend: function(){
            var html = '<td class="text-center"  colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
            $('tbody').html(html);
        },
        success: function (data) {
            if ( data.data.length > 0 ) {
              list_invoice_screent(data);
              $('[data-toggle="collapse"]').tooltip();
            } else {
              $('tbody').html('<td class="text-center text-danger"  colspan="6">Không có hóa đơn dữ liệu.</td>');
              $('tfoot').html('');
            }
        },
        error: function (e) {
            console.log(e);
            $('tbody').html('<td class="text-center text-danger"  colspan="6">Lỗi truy vấn dữ liệu hóa đơn!</td>');
            $('tfoot').html('');
        }
      })
   })

   function list_invoice_screent(data) {
      // console.log(data);
      var html = '';
      $.each(data.data, function (index , invoice) {
         html += '<tr>';
         // invoice
         html += '<td><a href="/order/check-invoices/'+ invoice.id +'">#'+ invoice.id +'</a></td>';
         // loai
         html += '<td>' + invoice.text_type + '</td>';
         // ngay tao
         html += '<td><span>' + invoice.date_create + '</span></td>';
         // ngay ket thuc
         html += '<td class="next_due_date">';
         html += '<span>' + invoice.next_due_date + '<span>';
         html += '</td>';
         // giá
         html += '<td>' + addCommas(invoice.total) + ' VNĐ</td>';
         // hành động
         html += '<td>';
         if (invoice.status == 'paid') {
           html += '<a href="/order/check-invoices/'+ invoice.id +'" class="btn btn-outline-success">Đã thanh toán</a>';
         } else {
           html += '<a href="/order/check-invoices/'+ invoice.id +'" class="btn btn-outline-danger">Chưa thanh toán</a>';
         }
         html += '</td>';
         html += '</tr>';
      })
      $('tbody').html(html);
      // phân trang cho vps
      var total = data.total;
      var per_page = data.perPage;
      var current_page = data.current_page;
      var html_page = '';
      if (total > per_page) {
          var page = total/per_page + 1;
          html_page += '<td colspan="13" class="text-center link-right">';
          html_page += '<ul class="pagination" id="pagination_invoice">';
          if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
          } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
          }
          for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                  active = 'active';
              }
              if (active == 'active') {
                html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

          }
          if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
          } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
          }
          html_page += '</ul>';
          html_page += '</td>';
      }
      $('tfoot').html(html_page);
   }

   // ajax phân trang invoice
   $(document).on('click', '#pagination_invoice a', function(event) {
       event.preventDefault();
       var page = $(this).attr('data-page');
       var sl = $('.invoice_classic').val();
       $.ajax({
           url: '/order/list_invoices_with_sl',
           data: {sl: sl, page: page},
           dataType: 'json',
           beforeSend: function(){
               var html = '<td class="text-center"  colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('tbody').html(html);
           },
           success: function (data) {
               if ( data.data.length > 0 ) {
                 list_invoice_screent(data);
                 $('[data-toggle="collapse"]').tooltip();
               } else {
                 $('tbody').html('<td class="text-center text-danger"  colspan="6">Không có hóa đơn trong dữ liệu.</td>');
                 $('tfoot').html('');
               }
           },
           error: function (e) {
               console.log(e);
               $('tbody').html('<td class="text-center text-danger"  colspan="6">Lỗi truy vấn dữ liệu hóa đơn!</td>');
               $('tfoot').html('');
           }
       });
   });

   //  invoice paid
   $('.invoice_paid_classic').on('change', function () {
      var sl = $(this).val();
      $.ajax({
        url: '/order/list_invoices_paid_with_sl',
        data: {sl : sl},
        dataType: 'json',
        beforeSend: function(){
            var html = '<td class="text-center"  colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
            $('tbody').html(html);
        },
        success: function (data) {
            if ( data.data.length > 0 ) {
              list_invoice_paid_screent(data);
            } else {
              $('tbody').html('<td class="text-center text-danger"  colspan="6">Không có hóa đơn dữ liệu.</td>');
              $('tfoot').html('');
            }
        },
        error: function (e) {
            console.log(e);
            $('tbody').html('<td class="text-center text-danger"  colspan="6">Lỗi truy vấn dữ liệu hóa đơn!</td>');
            $('tfoot').html('');
        }
      })
   })

   function list_invoice_paid_screent(data) {
      // console.log(data);
      var html = '';
      $.each(data.data, function (index , invoice) {
         html += '<tr>';
         // invoice
         html += '<td><a href="/order/check-invoices/'+ invoice.id +'">#'+ invoice.id +'</a></td>';
         // loai
         html += '<td>' + invoice.text_type + '</td>';
         // ngay tao
         html += '<td><span>' + invoice.date_create + '</span></td>';
         // ngay ket thuc
         html += '<td class="next_due_date">';
         html += '<span>' + invoice.next_due_date + '<span>';
         html += '</td>';
         // giá
         html += '<td>' + addCommas(invoice.total) + ' VNĐ</td>';
         // hành động
         html += '<td>';
         if (invoice.status == 'paid') {
           html += '<a href="/order/check-invoices/'+ invoice.id +'" class="btn btn-outline-success">Đã thanh toán</a>';
         } else {
           html += '<a href="/order/check-invoices/'+ invoice.id +'" class="btn btn-outline-danger">Chưa thanh toán</a>';
         }
         html += '</td>';
         html += '</tr>';
      })
      $('tbody').html(html);
      // phân trang cho vps
      var total = data.total;
      var per_page = data.perPage;
      var current_page = data.current_page;
      var html_page = '';
      if (total > per_page) {
          var page = total/per_page + 1;
          html_page += '<td colspan="13" class="text-center link-right">';
          html_page += '<ul class="pagination" id="pagination_invoice_paid">';
          if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
          } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
          }
          for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                  active = 'active';
              }
              if (active == 'active') {
                html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

          }
          if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
          } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
          }
          html_page += '</ul>';
          html_page += '</td>';
      }
      $('tfoot').html(html_page);
   }

   // ajax phân trang invoice
   $(document).on('click', '#pagination_invoice_paid a', function(event) {
       event.preventDefault();
       var page = $(this).attr('data-page');
       var sl = $('.invoice_paid_classic').val();
       $.ajax({
           url: '/order/list_invoices_paid_with_sl',
           data: {sl: sl, page: page},
           dataType: 'json',
           beforeSend: function(){
               var html = '<td class="text-center"  colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('tbody').html(html);
           },
           success: function (data) {
               if ( data.data.length > 0 ) {
                 list_invoice_screent(data);
               } else {
                 $('tbody').html('<td class="text-center text-danger"  colspan="6">Không có hóa đơn trong dữ liệu.</td>');
                 $('tfoot').html('');
               }
           },
           error: function (e) {
               console.log(e);
               $('tbody').html('<td class="text-center text-danger"  colspan="6">Lỗi truy vấn dữ liệu hóa đơn!</td>');
               $('tfoot').html('');
           }
       });
   });

  //  invoice unpaid
   $('.invoice_unpaid_classic').on('change', function () {
      var sl = $(this).val();
      $.ajax({
        url: '/order/list_invoices_unpaid_with_sl',
        data: {sl : sl},
        dataType: 'json',
        beforeSend: function(){
            var html = '<td class="text-center"  colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
            $('tbody').html(html);
        },
        success: function (data) {
            if ( data.data.length > 0 ) {
              list_invoice_paid_screent(data);
            } else {
              $('tbody').html('<td class="text-center text-danger"  colspan="6">Không có hóa đơn dữ liệu.</td>');
              $('tfoot').html('');
            }
        },
        error: function (e) {
            console.log(e);
            $('tbody').html('<td class="text-center text-danger"  colspan="6">Lỗi truy vấn dữ liệu hóa đơn!</td>');
            $('tfoot').html('');
        }
      })
   })

   function list_invoice_paid_screent(data) {
      // console.log(data);
      var html = '';
      $.each(data.data, function (index , invoice) {
         html += '<tr>';
         // invoice
         html += '<td><a href="/order/check-invoices/'+ invoice.id +'">#'+ invoice.id +'</a></td>';
         // loai
         html += '<td>' + invoice.text_type + '</td>';
         // ngay tao
         html += '<td><span>' + invoice.date_create + '</span></td>';
         // ngay ket thuc
         html += '<td class="next_due_date">';
         html += '<span>' + invoice.next_due_date + '<span>';
         html += '</td>';
         // giá
         html += '<td>' + addCommas(invoice.total) + ' VNĐ</td>';
         // hành động
         html += '<td>';
         if (invoice.status == 'paid') {
           html += '<a href="/order/check-invoices/'+ invoice.id +'" class="btn btn-outline-success">Đã thanh toán</a>';
         } else {
           html += '<a href="/order/check-invoices/'+ invoice.id +'" class="btn btn-outline-danger">Chưa thanh toán</a>';
         }
         html += '</td>';
         html += '</tr>';
      })
      $('tbody').html(html);
      // phân trang cho vps
      var total = data.total;
      var per_page = data.perPage;
      var current_page = data.current_page;
      var html_page = '';
      if (total > per_page) {
          var page = total/per_page + 1;
          html_page += '<td colspan="13" class="text-center link-right">';
          html_page += '<ul class="pagination" id="pagination_invoice_unpaid">';
          if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
          } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
          }
          for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                  active = 'active';
              }
              if (active == 'active') {
                html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

          }
          if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
          } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
          }
          html_page += '</ul>';
          html_page += '</td>';
      }
      $('tfoot').html(html_page);
   }

   // ajax phân trang invoice
   $(document).on('click', '#pagination_invoice_unpaid a', function(event) {
       event.preventDefault();
       var page = $(this).attr('data-page');
       var sl = $('.invoice_unpaid_classic').val();
       $.ajax({
           url: '/order/list_invoices_unpaid_with_sl',
           data: {sl: sl, page: page},
           dataType: 'json',
           beforeSend: function(){
               var html = '<td class="text-center"  colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('tbody').html(html);
           },
           success: function (data) {
               if ( data.data.length > 0 ) {
                 list_invoice_screent(data);
               } else {
                 $('tbody').html('<td class="text-center text-danger"  colspan="6">Không có hóa đơn trong dữ liệu.</td>');
                 $('tfoot').html('');
               }
           },
           error: function (e) {
               console.log(e);
               $('tbody').html('<td class="text-center text-danger"  colspan="6">Lỗi truy vấn dữ liệu hóa đơn!</td>');
               $('tfoot').html('');
           }
       });
   });

   function addCommas(nStr)
   {
       nStr += '';
       x = nStr.split('.');
       x1 = x[0];
       x2 = x.length > 1 ? '.' + x[1] : '';
       var rgx = /(\d+)(\d{3})/;
       while (rgx.test(x1)) {
           x1 = x1.replace(rgx, '$1' + '.' + '$2');
       }
       return x1 + x2;
   }

})
