$(document).ready(function() {
    // khởi tạo modal Toast
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });
    // Nhóm khách hàng
    loadUsers();
    function loadUsers() {
        var id = $('#id-group').val();
        $.ajax({
            url: '/admin/user/load_user_with_group',
            dataType: 'json',
            data: {id: id},
            beforeSend: function(){
              var html = '<tr><td class="text-center" colspan="3"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td></tr>';
              $('#users tbody').html(html);
            },
            success: function (data) {
              // console.log(data);
                var html = '';
                if (data != '') {
                    $.each(data, function(index, user) {
                        html += '<tr>';
                        html += '<td>' + user.name + '</td>';
                        html += '<td>' + user.email + '</td>';
                        html += '<td>';
                        html += '<button type="button" name="button" class="btn btn-danger delete_user" data-id="'+ user.id +'" data-name="'+ user.name +'" data-toggle="tooltip" data-placement="top" title="Xóa tài khoản khỏi nhóm đại lý"><i class="fas fa-trash-alt"></i></button>';
                        html += '</td>';
                        html += '</tr>';
                    })
                    $('#users tbody').html(html);
                } else {
                    $('#users tbody').html('<td class="text-center text-danger" colspan="3">Không có tài khoản trong nhóm đại lý này.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('#users tbody').html('<td class="text-center text-danger" colspan="3">Truy vấn nhóm đại lý lỗi.</span>');
            }
        })
    }
    // Thêm user vào group user
    $('#add_user').on('click', function () {
        $('#modal-create').modal('show');
        $('#modal-create .modal-title').text('Thêm tài khoản vào nhóm đại lý');
        $('#type').val('user');
        $('#change_group').html('');
        var group_id = $('#id-group').val();
        $.ajax({
            url: '/admin/user/load_users',
            dataType: 'json',
            data: {id: group_id},
            beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#notication').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data != '') {
                    html += '<label>Chọn khách hàng cho nhóm đại lý</label>';
                    html += '<select class="form-control select2" name="user_id[]" data-placeholder="Chọn khách hàng" multiple style="width:100%;">';
                    $.each(data, function (index, user) {
                        html += '<option value="' + user.id + '">';
                        html += user.name + ' - ' + user.email;
                        html += '</option>';
                    });
                    html += '</select>';
                    $('#notication').html('');
                    $('#change_group').html(html);
                    $('.select2').select2();
                } else {
                    $('#notication').html('<span class="text-center text-danger">Không có tài khoản nào phù hợp với yêu cầu.</span>');
                }
            },
            error: function (e) {
                console.log(e);
                $('#notication').html('<span class="text-center text-danger">Truy vấn nhóm đại lý lỗi.</span>');
            }
        })
    })

    $('#button-create').on('click', function() {
        var form = $('#form-group-product').serialize();
        var type = $('#type').val();
        $.ajax({
            url: '/admin/user/update_user_with_group_user',
            type: 'post',
            dataType: 'json',
            data: form,
            beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#notication').html(html);
              $('#change_group').html('');
              $('#button-create').attr('disabled', true);
            },
            success: function (data) {
              console.log(data);
              var html = '';

              if (data != '') {
                  $('#notication').html('');
                  $('#button-create').attr('disabled', false);
                  $('#modal-create').modal('hide');
                  if (type == 'user') {
                    $(document).Toasts('create', {
                        class: 'bg-success',
                        title: 'Đại lý',
                        subtitle: 'thêm tài khoản',
                        body: "Thêm tài khoản vào đại lý thành công.",
                    })
                    loadUsers();
                  } else {
                    $(document).Toasts('create', {
                        class: 'bg-success',
                        title: 'Đại lý',
                        subtitle: 'thêm nhóm sản phẩm',
                        body: "Thêm nhóm sản phẩm vào đại lý thành công.",
                    })
                    loadGroupProduct();
                  }

              } else {
                  $('#notication').html('<span class="text-danger text-center">Thêm khách hàng vào nhóm đại lý không thành công.</span> ');
                  $('#button-create').attr('disabled', false);
              }
            },
            error: function (e) {
              console.log(e);
              $('#notication').html('<span class="text-danger text-center">Truy vấn nhóm đại lý lỗi.</span> ');
              $('#button-create').attr('disabled', false);
            }
        });
    })

    $(document).on('click', '.delete_user', function () {
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        $('#modal-delete').modal('show');
        $('#modal-delete .modal-title').text('Xóa tài khoản trong nhóm đại lý');
        $('#notication-delete').html('<span>Bạn có muốn xóa tài khoản <b class="text-danger">' + name + '</b> ra khỏi nhóm đại lý này không?');
        $('#button-delete').attr('data-id', id);
        $('#button-delete').attr('data-type', 'user');
        $('#button-delete').fadeIn();
        $('#button-finish').fadeOut();
    });

    $('#button-delete').on('click', function () {
        var id  = $(this).attr('data-id');
        var type  = $(this).attr('data-type');
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/admin/user/delete_user_with_group_user',
            type: 'post',
            dataType: 'json',
            data: {'_token': token, id: id, type: type},
            beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#notication-delete').html(html);
              $('#button-delete').attr('disabled', true);
            },
            success: function (data) {
                if (data) {
                    $('#notication-delete').html('<span class="text-success text-center">Xóa tài khoản trong nhóm đại lý thành công.</span> ');
                    $('#button-delete').attr('disabled', false);
                    $('#button-delete').fadeOut();
                    $('#button-finish').fadeIn();
                    if (type == 'user') {
                        loadUsers();
                    }
                    else {
                        loadGroupProduct();
                    }
                } else {
                    $('#notication-delete').html('<span class="text-danger text-center">Xóa tài khoản trong nhóm đại lý thất bại.</span> ');
                    $('#button-delete').attr('disabled', false);
                }
            },
            error: function (e) {
                console.log(e);
                $('#notication-delete').html('<span class="text-danger text-center">Truy vấn nhóm đại lý lỗi.</span> ');
                $('#button-delete').attr('disabled', false);
            }
        });
    })
    // Nhóm sản phẩm
    loadGroupProduct();
    function loadGroupProduct() {
        var id = $('#id-group').val();
        $.ajax({
            url: '/admin/user/load_group_product_with_group_user',
            dataType: 'json',
            data: {id: id},
            beforeSend: function(){
              var html = '<tr><td class="text-center" colspan="3"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td></tr>';
              $('#group_products tbody').html(html);
            },
            success: function (data) {
              // console.log(data);
                var html = '';
                if (data != '') {
                    $.each(data, function(index, group_product) {
                        html += '<tr>';
                        html += '<td>' + group_product.id + '</td>';
                        html += '<td>' + group_product.name + '</td>';
                        html += '<td>';
                        html += '<button type="button" name="button" class="btn btn-danger delete_group_product" data-id="'+ group_product.id +'" data-name="'+ group_product.name +'" data-toggle="tooltip" data-placement="top" title="Xóa nhóm sản phẩm khỏi nhóm đại lý"><i class="fas fa-trash-alt"></i></button>';
                        html += '</td>';
                        html += '</tr>';
                    })
                    $('#group_products tbody').html(html);
                } else {
                    $('#group_products tbody').html('<td class="text-center text-danger" colspan="3">Không có nhóm sản phẩm trong nhóm đại lý này.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('#group_products tbody').html('<td class="text-center text-danger" colspan="3">Truy vấn nhóm đại lý lỗi.</span>');
            }
        })
    }
    // Thêm nhóm sản phẩm vào group user
    $('#add_group_product').on('click', function () {
        $('#modal-create').modal('show');
        $('#modal-create .modal-title').text('Thêm nhóm sản phẩm vào nhóm đại lý');
        $('#type').val('group_product');
        $('#change_group').html('');
        var group_id = $('#id-group').val();
        $.ajax({
            url: '/admin/user/load_group_products',
            dataType: 'json',
            data: {id: group_id},
            beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#notication').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data != '') {
                    html += '<label>Chọn nhóm sản phẩm cho nhóm đại lý</label>';
                    html += '<select class="form-control select2" name="group_product_id[]" data-placeholder="Chọn nhóm sản phẩm" multiple style="width:100%;">';
                    $.each(data, function (index, group_product) {
                        html += '<option value="' + group_product.id + '">';
                        html += group_product.name;
                        html += '</option>';
                    });
                    html += '</select>';
                    $('#notication').html('');
                    $('#change_group').html(html);
                    $('.select2').select2();
                } else {
                    $('#notication').html('<span class="text-center text-danger">Không nhóm sản phẩm nào phù hợp với yêu cầu.</span>');
                }
            },
            error: function (e) {
                console.log(e);
                $('#notication').html('<span class="text-center text-danger">Truy vấn nhóm đại lý lỗi.</span>');
            }
        })
    })

    $(document).on('click', '.delete_group_product', function () {
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        $('#modal-delete').modal('show');
        $('#modal-delete .modal-title').text('Xóa nhóm sản phẩm trong nhóm đại lý');
        $('#notication-delete').html('<span>Bạn có muốn xóa nhóm sản phẩm <b class="text-danger">' + name + '</b> ra khỏi nhóm đại lý này không?');
        $('#button-delete').attr('data-id', id);
        $('#button-delete').attr('data-type', 'group_product');
        $('#button-delete').fadeIn();
        $('#button-finish').fadeOut();
    });

})
