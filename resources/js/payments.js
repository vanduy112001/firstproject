$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    // Xác nhận giao dịch
    $(document).on('click', '.confirm-payment' ,function() {
        $('tr').removeClass('confirm-payment-row');
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        var data_magd = $(this).attr('data-magd');
        var html = 'Bạn có muốn xác nhận thanh toán <b class="text-danger">ID: ' + id + '</b> của <span class="text-danger">' + name + '</span> không?';
        $('#delete-invoice').modal('show');
        $('#notication-invoice').html(html);
        $('#button-invoice').fadeOut();
        $('#button-finish').fadeOut();
        $('#button-confirm').fadeIn();
        $('#button-confirm').attr('data-id' , id);
        $('#button-confirm').val('Xác nhận giao dịch');
        $('.modal-title').text('Xác nhận giao dịch');
        $(this).closest('tr').addClass('confirm-payment-row');
    });

    $('#button-confirm').on('click' , function () {
        var id = $(this).attr('data-id');
        $.ajax({
            url: '/admin/payment/confirm',
            data: {id: id},
            dataType: 'json',
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-confirm').attr('disabled', true);
                $('#notication-invoice').html(html);
            },
            success: function (data) {
                if(data) {
                    var html = '<p class="text-success">Xác nhận thanh toán thành công</p>';
                    $('.confirm-payment-row .payment-status span').removeClass('text-danger');
                    $('.confirm-payment-row .payment-status span').addClass('text-success');
                    $('.confirm-payment-row .payment-status span').text('Đã thanh toán');
                    $('.confirm-payment-row .confirm-payment').fadeOut();
                } else {
                    var html = '<p class="text-danger">Xác nhận nạp tiền thất bại</p>';
                    $('tr').removeClass('confirm-payment-row');
                }
                $('#notication-invoice').html(html);
                $('#button-confirm').attr('disabled', false);
                $('#button-finish').fadeIn();
                $('#button-confirm').fadeOut();
            },
            error: function (e) {
                console.log(e);
                var html = "<p class='text-danger'>Truy vấn quản lý thanh toán lỗi!</p>";
                $('#notication-invoice').html(html);
                $('#button-confirm').attr('disabled', false);
                $('.confirm-payment').removeClass('confirm-payment');
            }
        });
    });
    // Kết thúc giao dịch
    // Xóa giao dịch
    $(document).on('click', '.delete-payment' ,function (e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        var html = 'Bạn có muốn xóa thanh toán <b class="text-danger">ID: ' + id + '</b> của <span class="text-danger">' + name + '</span> không?';
        $('#delete-invoice').modal('show');
        $('#notication-invoice').html(html);
        $('#button-invoice').fadeIn();
        $('#button-confirm').fadeOut();
        $('#button-finish').fadeOut();
        $('#button-invoice').attr('data-id' , id);
        $('.modal-title').text('Xóa giao dịch');
        $('#button-invoice').val('Xóa giao dịch');
        $(this).closest('tr').addClass('remove-payment');
    });

    $('#button-cancel').on('click', function () {
        $('tr').removeClass('remove-payment');
    });

    $('#button-invoice').on('click', function () {
        var id = $(this).attr('data-id');
        $.ajax({
            type: 'get',
            url: '/admin/payment/delete',
            dataType: 'json',
            data: {id: id},
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-invoice').attr('disabled', true);
                $('#notication-invoice').html(html);
            },
            success: function (data) {
                if(data) {
                    var html = '<p class="text-success">Xóa giao dịch thành công</p>';
                    $('.remove-payment').fadeOut(1500);
                } else {
                    var html = '<p class="text-danger">Xóa giao dịch thất bại</p>';
                    $('.remove-payment').removeClass('remove-payment');
                }
                $('#notication-invoice').html(html);
                $('#button-invoice').attr('disabled', false);
                $('#button-invoice').fadeOut();
                $('#button-finish').fadeIn();
            },
            error: function (e) {
                console.log(e);
                var html = "<p class='text-danger'>Truy vấn quản lý thanh toán lỗi!</p>";
                $('#notication-invoice').html(html);
                $('#button-invoice').attr('disabled', false);
                $('.remove-payment').removeClass('remove-payment');
            }
        });
    });
    /**
      Thanh toán nạp tiền
     **/
    //search
    $('#search').on('keyup', function () {
        var q = $(this).val();
        var user_id = $('#user_payment').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_classic').val();
        var text_user_name = $('#user_payment select :selected').text();
        $.ajax({
          url: '/admin/payments/danh-sach-nap-tien-theo-user-sl',
          data: {user_id: user_id, type: type, status: status, sl: sl, q: q},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_user_name;
                  html += '<span class="float-right user">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_user" value="'+ user_id +'">';
                  html += '</span>';
                  $('#user_payment').html(html);
                  screent_payment(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Khách hàng này không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_user_name;
                  html2 += '<span class="float-right user">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_user" value="">';
                  html2 += '</span>';
                  $('#user_payment').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    });
    // filter user
    $(document).on( 'click' , '#user_payment .user' , function() {
        $.ajax({
          url: '/admin/users/list_all_user',
          dataType: 'json',
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#user_payment').html(html);
          },
          success: function (data) {
              var html = '';
              html += '<select style="width: 100%;">';
              html += '<option value="" selected disabled>Chọn khách hàng</option>';
              $.each(data, function (index, user) {
                html += '<option value="'+ user.id +'">'+ user.name + ' - ' + user.email +'</option>';
              })
              html += '</select>';
              $('#user_payment').html(html);
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += 'Lỗi truy vấn';
            html += '<span class="float-right user">';
            html += '<i class="fas fa-filter"></i>';
            html += '<input type="hidden" id="input_filter_user" value="">';
            html += '</span>';
            $('#user_payment').html(html);
          }
        })
    })

    $(document).on('change', '#user_payment select' , function () {
        var user_id = $(this).val();
        var q = $('#search').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_classic').val();
        var text_user_name = $('#user_payment select :selected').text();
        $.ajax({
          url: '/admin/payments/nap-tien-theo-user',
          data: {user_id: user_id, type: type, status: status, sl: sl, q: q},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_user_name;
                  html += '<span class="float-right user">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_user" value="'+ user_id +'">';
                  html += '</span>';
                  $('#user_payment').html(html);
                  screent_payment(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Khách hàng này không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_user_name;
                  html2 += '<span class="float-right user">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_user" value="">';
                  html2 += '</span>';
                  $('#user_payment').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })
    // filter giao dich
    $(document).on('click', '#type_payment span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn hình thức thanh toán</option>';
        html += '<option value="pay_in_office">Trực tiếp tại văn phòng</option>';
        html += '<option value="bank_transfer_vietcombank">Chuyển khoản VietComBank</option>';
        html += '<option value="bank_transfer_techcombank">Chuyển khoản TechComBank</option>';
        html += '<option value="momo">MoMo</option>';
        html += '</select>';
        $('#type_payment').html(html);
    })

    $(document).on('change', '#type_payment select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $(this).val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_classic').val();
        var text_type = $('#type_payment select :selected').text();
        var q = $('#search').val();
        $.ajax({
          url: '/admin/payments/nap-tien-theo-loai',
          data: {user_id: user_id, type: type, status: status, sl: sl, q: q},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_type;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_type" value="'+ type +'">';
                  html += '</span>';
                  $('#type_payment').html(html);
                  screent_payment(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_type;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_type" value="">';
                  html2 += '</span>';
                  $('#type_payment').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })
    // filter trạng thái
    $(document).on('click', '#status_payment span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn trạng thái</option>';
        html += '<option value="Pending">Chưa thanh toán</option>';
        html += '<option value="Active">Đã thanh toán</option>';
        html += '</select>';
        $('#status_payment').html(html);
    })

    $(document).on('change', '#status_payment select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $(this).val();
        var sl = $('.payment_classic').val();
        var text_stauts = $('#status_payment select :selected').text();
        var q = $('#search').val();
        $.ajax({
          url: '/admin/payments/nap-tien-theo-trang-thai',
          data: {user_id: user_id, type: type, status: status, sl: sl, q: q},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += 'Trạng thái ' + text_stauts;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html += '</span>';
                  $('#status_payment').html(html);
                  screent_payment(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += 'Trạng thái ' + text_stauts;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html2 += '</span>';
                  $('#status_payment').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })
    // so luong row
    $(document).on('change', '.payment_classic' , function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $(this).val();
        var q = $('#search').val();
        $.ajax({
          url: '/admin/payments/danh-sach-nap-tien-theo-user-sl',
          data: {user_id: user_id, type: type, status: status, sl: sl, q: q},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
            // console.log(data);
              if (data.data != '') {
                  screent_payment(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    $(document).on('click', '.pagination_history_pay a', function (event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_classic').val();
        var q = $('#search').val();
        $.ajax({
            url: '/admin/payments/danh-sach-nap-tien-theo-user-sl',
            data: {user_id: user_id, type: type, status: status, sl: sl, page: page, q: q},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data != '') {
                    screent_payment(data);
                } else {
                    var html = '';
                    html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                    $('.paginate_top').html('');
                }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
        });
    })

    $(document).on('click', '#credit_type_user', function () {
      $('#type_user_personal').val('personal');
      $.ajax({
         url: '/admin/payments/list_payment_credit_of_personal',
         dataType: 'json',
         beforeSend: function(){
             var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
             $('tbody').html(html);
         },
         success: function (data) {
              // console.log(data.data);
             if (data.data.length > 0) {
                 screent_payment_btn_list(data);
             } else {
                 var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
                 $('tbody').html(html);
                 $('tfoot').html('');
             }
         },
         error: function (e) {
             var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
             $('tbody').html(html);
         }
      })
    })

    $(document).on('click', '#credit_type_enterprise', function () {
      $('#type_user_personal').val('enterprise');
      $.ajax({
         url: '/admin/payments/list_payment_credit_of_enterprise',
         dataType: 'json',
         beforeSend: function(){
             var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
             $('tbody').html(html);
         },
         success: function (data) {
              // console.log(data);
             if (data.data.length > 0) {
                 screent_payment_btn_list(data);
             } else {
                 var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
                 $('tbody').html(html);
                 $('tfoot').html('');
             }
         },
         error: function (e) {
             var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
             $('tbody').html(html);
         }
      })
    })

    function screent_payment(data) {
        var html = '';
        $.each(data.data, function (index , payment) {
           html += '<tr>';
           // id
           html += '<td>' + payment.id + '</td>';
           // magd
           html += '<td>' + payment.ma_gd + '</td>';
           // user
           html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
           // loai giao dịch
           html += '<td> ' + payment.loai_gd + ' </td>';
           // total
           html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
           // date create
           html += '<td>' + payment.text_created_at + '</td>';
           // date_gd
           html += '<td>' + payment.text_date_gd + '</td>';
           // status
           html += '<td class="payment-status">';
           if (payment.status == 'Active') {
             html += '<span class="text-success">Đã thanh toán</span>';
           } else if (payment.status == 'Pending') {
             html += '<span class="text-danger">Chưa thanh toán</span>';
           } else {
             html += '<span class="text-danger">Thanh toán lỗi</span>';
           }
           html += '</td>';
           // action
           html += '<td class="table-button">';
           if (payment.status == 'Pending') {
             html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
           }
           html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
           html += '</td>';
           html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_history_pay>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_history_pay>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_history_pay">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_history_pay">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    function screent_payment_btn_list(data) {
        var html = '';
        $.each(data.data, function (index , payment) {
           html += '<tr>';
           // id
           html += '<td>' + payment.id + '</td>';
           // magd
           html += '<td>' + payment.ma_gd + '</td>';
           // user
           html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
           // loai giao dịch
           html += '<td> ' + payment.loai_gd + ' </td>';
           // total
           html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
           // date create
           html += '<td>' + payment.text_created_at + '</td>';
           // date_gd
           html += '<td>' + payment.text_date_gd + '</td>';
           // status
           html += '<td class="payment-status">';
           if (payment.status == 'Active') {
             html += '<span class="text-success">Đã thanh toán</span>';
           } else if (payment.status == 'Pending') {
             html += '<span class="text-danger">Chưa thanh toán</span>';
           } else {
             html += '<span class="text-danger">Thanh toán lỗi</span>';
           }
           html += '</td>';
           // action
           html += '<td class="table-button">';
           if (payment.status == 'Pending') {
             html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
           }
           html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
           html += '</td>';
           html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_user_history_pay>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_list_vps>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_user_history_pay">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_user_history_pay">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    /**
      Thanh toán tạo mới
    **/
    // so luong row
    $(document).on('change', '.payment_create_classic' , function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $(this).val();
        $.ajax({
          url: '/admin/payments/danh-sach-tao-moi-theo-sl',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
            // console.log(data);
              if (data.data != '') {
                  screent_payment_create(data);
              } else {
                  var html = '';
                  html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // ajax phân Trang
    $(document).on('click', '.pagination_payment_create a', function (event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_create_classic').val();
        $.ajax({
            url: '/admin/payments/danh-sach-tao-moi-theo-sl',
            data: {user_id: user_id, type: type, status: status, sl: sl, page: page},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data != '') {
                    screent_payment_create(data);
                } else {
                    var html = '';
                    html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                    $('.paginate_top').html('');
                }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
        });
    })

    $(document).on('click', '.pagination_user_history_pay a', function (event) {
        event.preventDefault();
        var type_user = $('#type_user_personal').val();
        var page = $(this).attr('data-page');
        if ( type_user ==  'personal' ) {
            var link = '/admin/payments/list_payment_credit_of_personal';
        } else {
            var link = '/admin/payments/list_payment_credit_of_enterprise';
        }

        $.ajax({
            url: link,
            data: {page: page},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data != '') {
                    screent_payment_btn_list(data);
                } else {
                    var html = '';
                    html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                    $('.paginate_top').html('');
                }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
        });
    })

    // filter user
    $(document).on( 'click' , '#user_payment_create .user' , function() {
        $.ajax({
          url: '/admin/users/list_all_user',
          dataType: 'json',
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#user_payment_create').html(html);
          },
          success: function (data) {
              var html = '';
              html += '<select style="width: 100%;">';
              html += '<option value="" selected disabled>Chọn khách hàng</option>';
              $.each(data, function (index, user) {
                html += '<option value="'+ user.id +'">'+ user.name + ' - ' + user.email +'</option>';
              })
              html += '</select>';
              $('#user_payment_create').html(html);
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += 'Lỗi truy vấn';
            html += '<span class="float-right user">';
            html += '<i class="fas fa-filter"></i>';
            html += '<input type="hidden" id="input_filter_user" value="">';
            html += '</span>';
            $('#user_payment_create').html(html);
          }
        })
    })

    $(document).on('change', '#user_payment_create select' , function () {
        var user_id = $(this).val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_create_classic').val();
        var text_user_name = $('#user_payment_create select :selected').text();
        $.ajax({
          url: '/admin/payments/danh-sach-tao-moi-theo-user',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_user_name;
                  html += '<span class="float-right user">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_user" value="'+ user_id +'">';
                  html += '</span>';
                  $('#user_payment_create').html(html);
                  screent_payment_create(data);
              } else {
                  var html = '';
                  html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Khách hàng này không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_user_name;
                  html2 += '<span class="float-right user">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_user" value="">';
                  html2 += '</span>';
                  $('#user_payment_create').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter giao dich
    $(document).on('click', '#type_payment_create span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn hình thức thanh toán</option>';
        html += '<option value="credit">Số dư tài khoản</option>';
        html += '<option value="pay_in_office">Trực tiếp tại văn phòng</option>';
        html += '<option value="bank_transfer_vietcombank">Chuyển khoản VietComBank</option>';
        html += '<option value="bank_transfer_techcombank">Chuyển khoản TechComBank</option>';
        html += '<option value="momo">MoMo</option>';
        html += '<option value="cloudzone_point">Cloudzone Point</option>';
        html += '</select>';
        $('#type_payment_create').html(html);
    })

    $(document).on('change', '#type_payment_create select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $(this).val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_create_classic').val();
        var text_type = $('#type_payment_create select :selected').text();
        $.ajax({
          url: '/admin/payments/danh-sach-tao-moi-theo-sl',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_type;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_type" value="'+ type +'">';
                  html += '</span>';
                  $('#type_payment_create').html(html);
                  screent_payment_create(data);
              } else {
                  var html = '';
                  html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_type;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_type" value="">';
                  html2 += '</span>';
                  $('#type_payment_create').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter trạng thái
    $(document).on('click', '#status_payment_create span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn trạng thái</option>';
        html += '<option value="Pending">Chưa thanh toán</option>';
        html += '<option value="Active">Đã thanh toán</option>';
        html += '<option value="confirm">Đã xác nhận</option>';
        html += '</select>';
        $('#status_payment_create').html(html);
    })

    $(document).on('change', '#status_payment_create select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $(this).val();
        var sl = $('.payment_create_classic').val();
        var text_stauts = $('#status_payment_create select :selected').text();
        $.ajax({
          url: '/admin/payments/danh-sach-tao-moi-theo-sl',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += 'Trạng thái ' + text_stauts;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html += '</span>';
                  $('#status_payment_create').html(html);
                  screent_payment_create(data);
              } else {
                  var html = '';
                  html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += 'Trạng thái ' + text_stauts;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html2 += '</span>';
                  $('#status_payment_create').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    $(document).on('click', '#create_type_user', function () {
      $('#type_user_personal').val('personal');
      $.ajax({
        url: '/admin/payments/list_payment_create_of_personal',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data.data);
          if (data.data.length > 0) {
            screent_payment_create_list(data);
          } else {
            var html = '<td colspan="10" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
            $('tbody').html(html);
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<td colspan="10" class="text-center">Truy vấn thanh toán lỗi!</td>';
          $('tbody').html(html);
        }
      })
    })

    $(document).on('click', '#create_type_enterprise', function () {
      $('#type_user_personal').val('enterprise');
      $.ajax({
        url: '/admin/payments/list_payment_create_of_enterprise',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data);
          if (data.data.length > 0) {
            screent_payment_create_list(data);
          } else {
            var html = '<td colspan="10" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
            $('tbody').html(html);
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<td colspan="10" class="text-center">Truy vấn thanh toán lỗi!</td>';
          $('tbody').html(html);
        }
      })
    })

    function screent_payment_create_list(data) {
        var html = '';
        $.each(data.data, function (index , payment) {
           html += '<tr>';
           // id
           html += '<td>' + payment.id + '</td>';
           // magd
           html += '<td>' + payment.ma_gd + '</td>';
           // user
           html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
           // Loại
           html += '<td>' + payment.text_type + '</td>';
           // hình thức giao dịch
           html += '<td> ' + payment.loai_gd + ' </td>';
           // total
           html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
           // date create
           html += '<td>' + payment.text_created_at + '</td>';
           // date_gd
           html += '<td>' + payment.text_date_gd + '</td>';
           // status
           html += '<td class="payment-status">';
           if (payment.status == 'Active') {
             html += '<span class="text-success">Đã thanh toán</span>';
           }
           else if (payment.status == 'Pending') {
             html += '<span class="text-danger">Chưa thanh toán</span>';
           }
           else if (payment.status == 'confirm') {
             html += '<span class="text-info">Đã xác nhận</span>';
           }
           else {
             html += '<span class="text-danger">Thanh toán lỗi</span>';
           }
           html += '</td>';
           // action
           html += '<td class="table-button">';
           if (payment.status == 'Pending' || payment.status == 'confirm') {
             html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
           }
           html += '<a href="/admin/payment/detail/'+ payment.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>';
           html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
           html += '</td>';
           html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_create_personal>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_create_personal>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_create_personal">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_create_personal">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    function screent_payment_create(data) {
        var html = '';
        $.each(data.data, function (index , payment) {
           html += '<tr>';
           // id
           html += '<td>' + payment.id + '</td>';
           // magd
           html += '<td>' + payment.ma_gd + '</td>';
           // user
           html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
           // Loại
           html += '<td>' + payment.text_type + '</td>';
           // hình thức giao dịch
           html += '<td> ' + payment.loai_gd + ' </td>';
           // total
           html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
           // date create
           html += '<td>' + payment.text_created_at + '</td>';
           // date_gd
           html += '<td>' + payment.text_date_gd + '</td>';
           // status
           html += '<td class="payment-status">';
           if (payment.status == 'Active') {
             html += '<span class="text-success">Đã thanh toán</span>';
           }
           else if (payment.status == 'Pending') {
             html += '<span class="text-danger">Chưa thanh toán</span>';
           }
           else if (payment.status == 'confirm') {
             html += '<span class="text-info">Đã xác nhận</span>';
           }
           else {
             html += '<span class="text-danger">Thanh toán lỗi</span>';
           }
           html += '</td>';
           // action
           html += '<td class="table-button">';
           if (payment.status == 'Pending' || payment.status == 'confirm') {
             html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
           }
           html += '<a href="/admin/payment/detail/'+ payment.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>';
           html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
           html += '</td>';
           html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_create>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_create>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_create">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_create">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    $(document).on('click', '.pagination_payment_create_personal a', function (event) {
        event.preventDefault();
        var type_user = $('#type_user_personal').val();
        var page = $(this).attr('data-page');
        if ( type_user ==  'personal' ) {
            var link = '/admin/payments/list_payment_create_of_personal';
        } else {
            var link = '/admin/payments/list_payment_create_of_enterprise';
        }

        $.ajax({
            url: link,
            data: {page: page},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data != '') {
                    screent_payment_create_list(data);
                } else {
                    var html = '';
                    html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                    $('.paginate_top').html('');
                }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
        });
    })

    /**
      Thanh toán addon
    **/
    // so luong row
    $(document).on('change', '.payment_addon_classic' , function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $(this).val();
        $.ajax({
          url: '/admin/payments/danh-sach-addon-theo-sl',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
            // console.log(data);
              if (data.data != '') {
                  screent_payment_addon(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // ajax phân Trang
    $(document).on('click', '.pagination_payment_addon a', function (event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_addon_classic').val();
        $.ajax({
            url: '/admin/payments/danh-sach-addon-theo-sl',
            data: {user_id: user_id, type: type, status: status, sl: sl, page: page},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data != '') {
                    screent_payment_addon(data);
                } else {
                    var html = '';
                    html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                    $('.paginate_top').html('');
                }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
        });
    })

    // filter user
    $(document).on( 'click' , '#user_payment_addon .user' , function() {
        $.ajax({
          url: '/admin/users/list_all_user',
          dataType: 'json',
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#user_payment_addon').html(html);
          },
          success: function (data) {
              var html = '';
              html += '<select style="width: 100%;">';
              html += '<option value="" selected disabled>Chọn khách hàng</option>';
              $.each(data, function (index, user) {
                html += '<option value="'+ user.id +'">'+ user.name + ' - ' + user.email +'</option>';
              })
              html += '</select>';
              $('#user_payment_addon').html(html);
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += 'Lỗi truy vấn';
            html += '<span class="float-right user">';
            html += '<i class="fas fa-filter"></i>';
            html += '<input type="hidden" id="input_filter_user" value="">';
            html += '</span>';
            $('#user_payment_addon').html(html);
          }
        })
    })

    $(document).on('change', '#user_payment_addon select' , function () {
        var user_id = $(this).val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_addon_classic').val();
        var text_user_name = $('#user_payment_addon select :selected').text();
        $.ajax({
          url: '/admin/payments/danh-sach-addon-theo-sl',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_user_name;
                  html += '<span class="float-right user">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_user" value="'+ user_id +'">';
                  html += '</span>';
                  $('#user_payment_addon').html(html);
                  screent_payment_addon(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Khách hàng này không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_user_name;
                  html2 += '<span class="float-right user">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_user" value="">';
                  html2 += '</span>';
                  $('#user_payment_addon').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter giao dich
    $(document).on('click', '#type_payment_addon span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn hình thức thanh toán</option>';
        html += '<option value="credit">Số dư tài khoản</option>';
        html += '<option value="pay_in_office">Trực tiếp tại văn phòng</option>';
        html += '<option value="bank_transfer_vietcombank">Chuyển khoản VietComBank</option>';
        html += '<option value="bank_transfer_techcombank">Chuyển khoản TechComBank</option>';
        html += '<option value="momo">MoMo</option>';
        html += '<option value="cloudzone_point">Cloudzone Point</option>';
        html += '</select>';
        $('#type_payment_addon').html(html);
    })

    $(document).on('change', '#type_payment_addon select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $(this).val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_addon_classic').val();
        var text_type = $('#type_payment_addon select :selected').text();
        $.ajax({
          url: '/admin/payments/danh-sach-addon-theo-sl',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_type;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_type" value="'+ type +'">';
                  html += '</span>';
                  $('#type_payment_addon').html(html);
                  screent_payment_addon(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_type;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_type" value="">';
                  html2 += '</span>';
                  $('#type_payment_addon').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter trạng thái
    $(document).on('click', '#status_payment_addon span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn trạng thái</option>';
        html += '<option value="Pending">Chưa thanh toán</option>';
        html += '<option value="Active">Đã thanh toán</option>';
        html += '<option value="confirm">Đã xác nhận</option>';
        html += '</select>';
        $('#status_payment_addon').html(html);
    })

    $(document).on('change', '#status_payment_addon select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $(this).val();
        var sl = $('.payment_addon_classic').val();
        var text_stauts = $('#status_payment_addon select :selected').text();
        $.ajax({
          url: '/admin/payments/danh-sach-addon-theo-sl',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += 'Trạng thái ' + text_stauts;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html += '</span>';
                  $('#status_payment_addon').html(html);
                  screent_payment_addon(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += 'Trạng thái ' + text_stauts;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html2 += '</span>';
                  $('#status_payment_addon').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    $(document).on('click', '#addon_type_user', function () {
      $('#type_user_personal').val('personal');
      $.ajax({
        url: '/admin/payments/list_payment_addon_of_personal',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data.data);
          if (data.data.length > 0) {
            screent_payment_addon_btn_list(data);
          } else {
            var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
            $('tbody').html(html);
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
          $('tbody').html(html);
        }
      })
    })

    $(document).on('click', '#addon_type_enterprise', function () {
      $('#type_user_personal').val('enterprise');
      $.ajax({
        url: '/admin/payments/list_payment_addon_of_enterprise',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data);
          if (data.data.length > 0) {
            screent_payment_addon_btn_list(data);
          } else {
            var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
            $('tbody').html(html);
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
          $('tbody').html(html);
        }
      })
    })

    function screent_payment_addon(data) {
        var html = '';
        $.each(data.data, function (index , payment) {
           html += '<tr>';
           // id
           html += '<td>' + payment.id + '</td>';
           // magd
           html += '<td>' + payment.ma_gd + '</td>';
           // user
           html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
           // hình thức giao dịch
           html += '<td> ' + payment.loai_gd + ' </td>';
           // total
           html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
           // date create
           html += '<td>' + payment.text_created_at + '</td>';
           // date_gd
           html += '<td>' + payment.text_date_gd + '</td>';
           // status
           html += '<td class="payment-status">';
           if (payment.status == 'Active') {
             html += '<span class="text-success">Đã thanh toán</span>';
           }
           else if (payment.status == 'Pending') {
             html += '<span class="text-danger">Chưa thanh toán</span>';
           }
           else if (payment.status == 'confirm') {
             html += '<span class="text-info">Đã xác nhận</span>';
           }
           else {
             html += '<span class="text-danger">Thanh toán lỗi</span>';
           }
           html += '</td>';
           // action
           html += '<td class="table-button">';
           if (payment.status == 'Pending' || payment.status == 'confirm') {
             html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
           }
           html += '<a href="/admin/payment/detail/'+ payment.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>';
           html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
           html += '</td>';
           html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_addon>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_addon>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_addon">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_addon">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    function screent_payment_addon_btn_list(data) {
        var html = '';
        $.each(data.data, function (index , payment) {
           html += '<tr>';
           // id
           html += '<td>' + payment.id + '</td>';
           // magd
           html += '<td>' + payment.ma_gd + '</td>';
           // user
           html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
           // hình thức giao dịch
           html += '<td> ' + payment.loai_gd + ' </td>';
           // total
           html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
           // date create
           html += '<td>' + payment.text_created_at + '</td>';
           // date_gd
           html += '<td>' + payment.text_date_gd + '</td>';
           // status
           html += '<td class="payment-status">';
           if (payment.status == 'Active') {
             html += '<span class="text-success">Đã thanh toán</span>';
           }
           else if (payment.status == 'Pending') {
             html += '<span class="text-danger">Chưa thanh toán</span>';
           }
           else if (payment.status == 'confirm') {
             html += '<span class="text-info">Đã xác nhận</span>';
           }
           else {
             html += '<span class="text-danger">Thanh toán lỗi</span>';
           }
           html += '</td>';
           // action
           html += '<td class="table-button">';
           if (payment.status == 'Pending' || payment.status == 'confirm') {
             html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
           }
           html += '<a href="/admin/payment/detail/'+ payment.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>';
           html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
           html += '</td>';
           html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_addon_personal>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_addon_personal>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_addon_personal">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_addon_personal">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    $(document).on('click', '.pagination_payment_addon_personal a', function (event) {
        event.preventDefault();
        var type_user = $('#type_user_personal').val();
        var page = $(this).attr('data-page');
        if ( type_user ==  'personal' ) {
            var link = '/admin/payments/list_payment_addon_of_personal';
        } else {
            var link = '/admin/payments/list_payment_addon_of_enterprise';
        }

        $.ajax({
            url: link,
            data: {page: page},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data != '') {
                    screent_payment_addon_btn_list(data);
                } else {
                    var html = '';
                    html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                    $('.paginate_top').html('');
                }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
        });
    })

    /**
      Thanh toán upgrade hosting
    **/
    // so luong row
    $(document).on('change', '.payment_upgrade_classic' , function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $(this).val();
        $.ajax({
          url: '/admin/payments/list_upgrade_hosting_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
            // console.log(data);
              if (data.data != '') {
                  screent_payment_upgrade_hosting(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // ajax phân Trang
    $(document).on('click', '.pagination_payment_upgrade_hosting a', function (event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_upgrade_classic').val();
        $.ajax({
            url: '/admin/payments/list_upgrade_hosting_with_qtt',
            data: {user_id: user_id, type: type, status: status, sl: sl, page: page},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data != '') {
                    screent_payment_upgrade_hosting(data);
                } else {
                    var html = '';
                    html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                    $('.paginate_top').html('');
                }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
        });
    })

    // filter user
    $(document).on( 'click' , '#user_payment_upgrade .user' , function() {
        $.ajax({
          url: '/admin/users/list_all_user',
          dataType: 'json',
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#user_payment_upgrade').html(html);
          },
          success: function (data) {
              var html = '';
              html += '<select style="width: 100%;">';
              html += '<option value="" selected disabled>Chọn khách hàng</option>';
              $.each(data, function (index, user) {
                html += '<option value="'+ user.id +'">'+ user.name + ' - ' + user.email +'</option>';
              })
              html += '</select>';
              $('#user_payment_upgrade').html(html);
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += 'Lỗi truy vấn';
            html += '<span class="float-right user">';
            html += '<i class="fas fa-filter"></i>';
            html += '<input type="hidden" id="input_filter_user" value="">';
            html += '</span>';
            $('#user_payment_upgrade').html(html);
          }
        })
    })

    $(document).on('change', '#user_payment_upgrade select' , function () {
        var user_id = $(this).val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_upgrade_classic').val();
        var text_user_name = $('#user_payment_upgrade select :selected').text();
        $.ajax({
          url: '/admin/payments/list_upgrade_hosting_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_user_name;
                  html += '<span class="float-right user">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_user" value="'+ user_id +'">';
                  html += '</span>';
                  $('#user_payment_upgrade').html(html);
                  screent_payment_upgrade_hosting(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Khách hàng này không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_user_name;
                  html2 += '<span class="float-right user">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_user" value="">';
                  html2 += '</span>';
                  $('#user_payment_upgrade').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter giao dich
    $(document).on('click', '#type_payment_upgrade span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn hình thức thanh toán</option>';
        html += '<option value="credit">Số dư tài khoản</option>';
        html += '<option value="pay_in_office">Trực tiếp tại văn phòng</option>';
        html += '<option value="bank_transfer_vietcombank">Chuyển khoản VietComBank</option>';
        html += '<option value="bank_transfer_techcombank">Chuyển khoản TechComBank</option>';
        html += '<option value="momo">MoMo</option>';
        html += '<option value="cloudzone_point">Cloudzone Point</option>';
        html += '</select>';
        $('#type_payment_upgrade').html(html);
    })

    $(document).on('change', '#type_payment_upgrade select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $(this).val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_upgrade_classic').val();
        var text_type = $('#type_payment_upgrade select :selected').text();
        $.ajax({
          url: '/admin/payments/list_upgrade_hosting_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_type;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_type" value="'+ type +'">';
                  html += '</span>';
                  $('#type_payment_upgrade').html(html);
                  screent_payment_upgrade_hosting(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_type;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_type" value="">';
                  html2 += '</span>';
                  $('#type_payment_upgrade').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter trạng thái
    $(document).on('click', '#status_payment_upgrade span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn trạng thái</option>';
        html += '<option value="Pending">Chưa thanh toán</option>';
        html += '<option value="Active">Đã thanh toán</option>';
        html += '<option value="confirm">Đã xác nhận</option>';
        html += '</select>';
        $('#status_payment_upgrade').html(html);
    })

    $(document).on('change', '#status_payment_upgrade select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $(this).val();
        var sl = $('.payment_upgrade_classic').val();
        var text_stauts = $('#status_payment_addon select :selected').text();
        $.ajax({
          url: '/admin/payments/list_upgrade_hosting_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += 'Trạng thái ' + text_stauts;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html += '</span>';
                  $('#status_payment_upgrade').html(html);
                  screent_payment_upgrade_hosting(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += 'Trạng thái ' + text_stauts;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html2 += '</span>';
                  $('#status_payment_upgrade').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    $(document).on('click', '#upgrade_hosting_type_user', function () {
      $('#type_user_personal').val('personal');
      $.ajax({
        url: '/admin/payments/list_payment_upgrade_hosting_of_personal',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data.data);
          if (data.data.length > 0) {
            screent_payment_upgrade_hosting_list(data);
          } else {
            var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
            $('tbody').html(html);
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
          $('tbody').html(html);
        }
      })
    })

    $(document).on('click', '#upgrade_hosting_type_enterprise', function () {
      $('#type_user_personal').val('enterprise');
      $.ajax({
        url: '/admin/payments/list_payment_upgrade_hosting_of_enterprise',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data);
          if (data.data.length > 0) {
            screent_payment_upgrade_hosting_list(data);
          } else {
            var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
            $('tbody').html(html);
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
          $('tbody').html(html);
        }
      })
    })

    function screent_payment_upgrade_hosting(data) {
            var html = '';
            $.each(data.data, function (index , payment) {
               html += '<tr>';
               // id
               html += '<td>' + payment.id + '</td>';
               // magd
               html += '<td>' + payment.ma_gd + '</td>';
               // user
               html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
               // hình thức giao dịch
               html += '<td> ' + payment.loai_gd + ' </td>';
               // total
               html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
               // date create
               html += '<td>' + payment.text_created_at + '</td>';
               // date_gd
               html += '<td>' + payment.text_date_gd + '</td>';
               // status
               html += '<td class="payment-status">';
               if (payment.status == 'Active') {
                 html += '<span class="text-success">Đã thanh toán</span>';
               }
               else if (payment.status == 'Pending') {
                 html += '<span class="text-danger">Chưa thanh toán</span>';
               }
               else if (payment.status == 'confirm') {
                 html += '<span class="text-info">Đã xác nhận</span>';
               }
               else {
                 html += '<span class="text-danger">Thanh toán lỗi</span>';
               }
               html += '</td>';
               // action
               html += '<td class="table-button">';
               if (payment.status == 'Pending' || payment.status == 'confirm') {
                 html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
               }
               html += '<a href="/admin/payment/detail/'+ payment.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>';
               html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
               html += '</td>';
               html += '</tr>';
            })
            $('tbody').html(html);
            // phân trang cho vps
            var total = data.total;
            var per_page = data.perPage;
            var current_page = data.current_page;
            var html_page = '';
            if (total > per_page) {
              if ( total / per_page > 11 ) {
                var page = parseInt(total/per_page + 1);
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_payment_upgrade_hosting>';
                if (current_page != 1) {
                  html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                } else {
                  html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                  for (var i = 1; i < 9; i++) {
                    var active = '';
                    if (i == current_page) {
                      active = 'active';
                    }
                    if (active == 'active') {
                      html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                  }
                  html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = page - 1; i <= page; i++) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                else if (current_page >= 7 && current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page +3; i++) {
                      var active = '';
                      if (i == current_page) {
                        active = 'active';
                      }
                      if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                      } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                      }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                }
                else if (current_page >= page - 6) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                  html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = page - 6; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                      active = 'active';
                    }
                    if (active == 'active') {
                      html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                  }
                }

                if (current_page != page.toPrecision(1)) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                } else {
                  html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '<nav>';
                html_page += '</td>';
              } else {
                var page = total/per_page + 1;
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_payment_upgrade_hosting>';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                      html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '<nav>';
                html_page += '</td>';
              }
            }
            $('tfoot').html(html_page);
            var html_page_top = '';
            if (total > per_page) {
              if ( total / per_page > 11 ) {
                var page = parseInt(total/per_page + 1);
                html_page_top += '<nav>';
                html_page_top += '<ul class="pagination pagination_payment_upgrade_hosting">';
                if (current_page != 1) {
                  html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                } else {
                  html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                  for (var i = 1; i < 9; i++) {
                    var active = '';
                    if (i == current_page) {
                      active = 'active';
                    }
                    if (active == 'active') {
                      html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                  }
                  html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = page - 1; i <= page; i++) {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                else if (current_page >= 7 && current_page <= page - 6) {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                      var active = '';
                      if (i == current_page) {
                        active = 'active';
                      }
                      if (active == 'active') {
                        html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                      } else {
                        html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                      }
                    }
                    html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                      html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                }
                else if (current_page > page - 6) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                  html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = page - 6; i <= page; i++) {
                    var active = '';
                    if (i == current_page) {
                      active = 'active';
                    }
                    if (active == 'active') {
                      html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                  }
                }

                if (current_page != page.toPrecision(1)) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                } else {
                  html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '<nav>';
              } else {
                var page = total/per_page + 1;
                html_page_top += '<nav>';
                html_page_top += '<ul class="pagination pagination_payment_upgrade_hosting">';
                if (current_page != 1) {
                  html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                } else {
                  html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }

                }
                if (current_page != page.toPrecision(1)) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                } else {
                  html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page_top += '</ul>';
                html_page_top += '<nav>';
              }
            }
            $('.paginate_top').html(html_page_top);
        }

    function screent_payment_upgrade_hosting_list(data) {
            var html = '';
            $.each(data.data, function (index , payment) {
               html += '<tr>';
               // id
               html += '<td>' + payment.id + '</td>';
               // magd
               html += '<td>' + payment.ma_gd + '</td>';
               // user
               html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
               // hình thức giao dịch
               html += '<td> ' + payment.loai_gd + ' </td>';
               // total
               html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
               // date create
               html += '<td>' + payment.text_created_at + '</td>';
               // date_gd
               html += '<td>' + payment.text_date_gd + '</td>';
               // status
               html += '<td class="payment-status">';
               if (payment.status == 'Active') {
                 html += '<span class="text-success">Đã thanh toán</span>';
               }
               else if (payment.status == 'Pending') {
                 html += '<span class="text-danger">Chưa thanh toán</span>';
               }
               else if (payment.status == 'confirm') {
                 html += '<span class="text-info">Đã xác nhận</span>';
               }
               else {
                 html += '<span class="text-danger">Thanh toán lỗi</span>';
               }
               html += '</td>';
               // action
               html += '<td class="table-button">';
               if (payment.status == 'Pending' || payment.status == 'confirm') {
                 html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
               }
               html += '<a href="/admin/payment/detail/'+ payment.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>';
               html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
               html += '</td>';
               html += '</tr>';
            })
            $('tbody').html(html);
            // phân trang cho vps
            var total = data.total;
            var per_page = data.perPage;
            var current_page = data.current_page;
            var html_page = '';
            if (total > per_page) {
              if ( total / per_page > 11 ) {
                var page = parseInt(total/per_page + 1);
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_payment_upgrade_hosting_personal>';
                if (current_page != 1) {
                  html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                } else {
                  html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                  for (var i = 1; i < 9; i++) {
                    var active = '';
                    if (i == current_page) {
                      active = 'active';
                    }
                    if (active == 'active') {
                      html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                  }
                  html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = page - 1; i <= page; i++) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                else if (current_page >= 7 && current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page +3; i++) {
                      var active = '';
                      if (i == current_page) {
                        active = 'active';
                      }
                      if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                      } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                      }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                }
                else if (current_page >= page - 6) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                  html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = page - 6; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                      active = 'active';
                    }
                    if (active == 'active') {
                      html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                  }
                }

                if (current_page != page.toPrecision(1)) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                } else {
                  html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '<nav>';
                html_page += '</td>';
              } else {
                var page = total/per_page + 1;
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_payment_upgrade_hosting_personal>';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                      html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '<nav>';
                html_page += '</td>';
              }
            }
            $('tfoot').html(html_page);
            var html_page_top = '';
            if (total > per_page) {
              if ( total / per_page > 11 ) {
                var page = parseInt(total/per_page + 1);
                html_page_top += '<nav>';
                html_page_top += '<ul class="pagination pagination_payment_upgrade_hosting_personal">';
                if (current_page != 1) {
                  html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                } else {
                  html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                  for (var i = 1; i < 9; i++) {
                    var active = '';
                    if (i == current_page) {
                      active = 'active';
                    }
                    if (active == 'active') {
                      html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                  }
                  html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = page - 1; i <= page; i++) {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                else if (current_page >= 7 && current_page <= page - 6) {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                      var active = '';
                      if (i == current_page) {
                        active = 'active';
                      }
                      if (active == 'active') {
                        html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                      } else {
                        html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                      }
                    }
                    html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                      html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                }
                else if (current_page > page - 6) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                  html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = page - 6; i <= page; i++) {
                    var active = '';
                    if (i == current_page) {
                      active = 'active';
                    }
                    if (active == 'active') {
                      html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                  }
                }

                if (current_page != page.toPrecision(1)) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                } else {
                  html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '<nav>';
              } else {
                var page = total/per_page + 1;
                html_page_top += '<nav>';
                html_page_top += '<ul class="pagination pagination_payment_upgrade_hosting_personal">';
                if (current_page != 1) {
                  html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                } else {
                  html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }

                }
                if (current_page != page.toPrecision(1)) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                } else {
                  html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page_top += '</ul>';
                html_page_top += '<nav>';
              }
            }
            $('.paginate_top').html(html_page_top);
        }

    $(document).on('click', '.pagination_payment_upgrade_hosting_personal a', function (event) {
          event.preventDefault();
          var type_user = $('#type_user_personal').val();
          var page = $(this).attr('data-page');
          if ( type_user ==  'personal' ) {
            var link = '/admin/payments/list_payment_upgrade_hosting_of_personal';
          } else {
            var link = '/admin/payments/list_payment_upgrade_hosting_of_enterprise';
          }

          $.ajax({
            url: link,
            data: {page: page},
            dataType: 'json',
            beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
            },
            success: function (data) {
              if (data.data != '') {
                screent_payment_upgrade_hosting_list(data);
              } else {
                var html = '';
                html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                $('tbody').html(html);
                $('tfoot').html('');
                $('.paginate_top').html('');
              }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
          });
    })
    /**
      Thanh toán change ip
    **/
    // so luong row
    $(document).on('change', '.payment_change_id_classic' , function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $(this).val();
        $.ajax({
          url: '/admin/payments/list_change_ip_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
            // console.log(data);
              if (data.data != '') {
                  screent_payment_change_ip(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // ajax phân Trang
    $(document).on('click', '.pagination_payment_change_ip a', function (event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_change_id_classic').val();
        $.ajax({
            url: '/admin/payments/list_change_ip_with_qtt',
            data: {user_id: user_id, type: type, status: status, sl: sl, page: page},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data != '') {
                    screent_payment_change_ip(data);
                } else {
                    var html = '';
                    html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                    $('.paginate_top').html('');
                }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
        });
    })

    // filter user
    $(document).on( 'click' , '#user_payment_change_id .user' , function() {
        $.ajax({
          url: '/admin/users/list_all_user',
          dataType: 'json',
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#user_payment_change_id').html(html);
          },
          success: function (data) {
              var html = '';
              html += '<select style="width: 100%;">';
              html += '<option value="" selected disabled>Chọn khách hàng</option>';
              $.each(data, function (index, user) {
                html += '<option value="'+ user.id +'">'+ user.name + ' - ' + user.email +'</option>';
              })
              html += '</select>';
              $('#user_payment_change_id').html(html);
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += 'Lỗi truy vấn';
            html += '<span class="float-right user">';
            html += '<i class="fas fa-filter"></i>';
            html += '<input type="hidden" id="input_filter_user" value="">';
            html += '</span>';
            $('#user_payment_change_id').html(html);
          }
        })
    })

    $(document).on('change', '#user_payment_change_id select' , function () {
        var user_id = $(this).val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_change_id_classic').val();
        var text_user_name = $('#user_payment_change_id select :selected').text();
        $.ajax({
          url: '/admin/payments/list_change_ip_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_user_name;
                  html += '<span class="float-right user">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_user" value="'+ user_id +'">';
                  html += '</span>';
                  $('#user_payment_change_id').html(html);
                  screent_payment_change_ip(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Khách hàng này không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_user_name;
                  html2 += '<span class="float-right user">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_user" value="">';
                  html2 += '</span>';
                  $('#user_payment_change_id').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter giao dich
    $(document).on('click', '#type_payment_change_id span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn hình thức thanh toán</option>';
        html += '<option value="credit">Số dư tài khoản</option>';
        html += '<option value="pay_in_office">Trực tiếp tại văn phòng</option>';
        html += '<option value="bank_transfer_vietcombank">Chuyển khoản VietComBank</option>';
        html += '<option value="bank_transfer_techcombank">Chuyển khoản TechComBank</option>';
        html += '<option value="momo">MoMo</option>';
        html += '<option value="cloudzone_point">Cloudzone Point</option>';
        html += '</select>';
        $('#type_payment_change_id').html(html);
    })

    $(document).on('change', '#type_payment_change_id select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $(this).val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_change_id_classic').val();
        var text_type = $('#type_payment_change_id select :selected').text();
        $.ajax({
          url: '/admin/payments/list_change_ip_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_type;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_type" value="'+ type +'">';
                  html += '</span>';
                  $('#type_payment_change_id').html(html);
                  screent_payment_change_ip(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_type;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_type" value="">';
                  html2 += '</span>';
                  $('#type_payment_change_id').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter trạng thái
    $(document).on('click', '#status_payment_change_id span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn trạng thái</option>';
        html += '<option value="Pending">Chưa thanh toán</option>';
        html += '<option value="Active">Đã thanh toán</option>';
        html += '<option value="confirm">Đã xác nhận</option>';
        html += '</select>';
        $('#status_payment_change_id').html(html);
    })

    $(document).on('change', '#status_payment_change_id select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $(this).val();
        var sl = $('.payment_change_id_classic').val();
        var text_stauts = $('#status_payment_change_id select :selected').text();
        $.ajax({
          url: '/admin/payments/list_change_ip_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += 'Trạng thái ' + text_stauts;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html += '</span>';
                  $('#status_payment_change_id').html(html);
                  screent_payment_change_ip(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += 'Trạng thái ' + text_stauts;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html2 += '</span>';
                  $('#status_payment_change_id').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    $(document).on('click', '#change_ip_type_user', function () {
      $('#type_user_personal').val('personal');
      $.ajax({
        url: '/admin/payments/list_payment_change_ip_of_personal',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data.data);
          if (data.data.length > 0) {
            screent_payment_change_ip_list(data);
          } else {
            var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
            $('tbody').html(html);
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
          $('tbody').html(html);
        }
      })
    })

    $(document).on('click', '#change_ip_type_enterprise', function () {
      $('#type_user_personal').val('enterprise');
      $.ajax({
        url: '/admin/payments/list_payment_change_ip_of_enterprise',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data);
          if (data.data.length > 0) {
            screent_payment_change_ip_list(data);
          } else {
            var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
            $('tbody').html(html);
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
          $('tbody').html(html);
        }
      })
    })

    function screent_payment_change_ip_list(data) {
        var html = '';
        $.each(data.data, function (index , payment) {
           html += '<tr>';
           // id
           html += '<td>' + payment.id + '</td>';
           // magd
           html += '<td>' + payment.ma_gd + '</td>';
           // user
           html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
           // hình thức giao dịch
           html += '<td> ' + payment.loai_gd + ' </td>';
           // total
           html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
           // date create
           html += '<td>' + payment.text_created_at + '</td>';
           // date_gd
           html += '<td>' + payment.text_date_gd + '</td>';
           // status
           html += '<td class="payment-status">';
           if (payment.status == 'Active') {
             html += '<span class="text-success">Đã thanh toán</span>';
           }
           else if (payment.status == 'Pending') {
             html += '<span class="text-danger">Chưa thanh toán</span>';
           }
           else if (payment.status == 'confirm') {
             html += '<span class="text-info">Đã xác nhận</span>';
           }
           else {
             html += '<span class="text-danger">Thanh toán lỗi</span>';
           }
           html += '</td>';
           // action
           html += '<td class="table-button">';
           if (payment.status == 'Pending' || payment.status == 'confirm') {
             html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
           }
           html += '<a href="/admin/payment/detail/'+ payment.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>';
           html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
           html += '</td>';
           html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_change_ip_personal>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_change_ip_personal>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_change_ip_personal">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_change_ip_personal">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    function screent_payment_change_ip(data) {
        var html = '';
        $.each(data.data, function (index , payment) {
           html += '<tr>';
           // id
           html += '<td>' + payment.id + '</td>';
           // magd
           html += '<td>' + payment.ma_gd + '</td>';
           // user
           html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
           // hình thức giao dịch
           html += '<td> ' + payment.loai_gd + ' </td>';
           // total
           html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
           // date create
           html += '<td>' + payment.text_created_at + '</td>';
           // date_gd
           html += '<td>' + payment.text_date_gd + '</td>';
           // status
           html += '<td class="payment-status">';
           if (payment.status == 'Active') {
             html += '<span class="text-success">Đã thanh toán</span>';
           }
           else if (payment.status == 'Pending') {
             html += '<span class="text-danger">Chưa thanh toán</span>';
           }
           else if (payment.status == 'confirm') {
             html += '<span class="text-info">Đã xác nhận</span>';
           }
           else {
             html += '<span class="text-danger">Thanh toán lỗi</span>';
           }
           html += '</td>';
           // action
           html += '<td class="table-button">';
           if (payment.status == 'Pending' || payment.status == 'confirm') {
             html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
           }
           html += '<a href="/admin/payment/detail/'+ payment.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>';
           html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
           html += '</td>';
           html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_change_ip>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_change_ip>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_change_ip">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_change_ip">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    $(document).on('click', '.pagination_payment_change_ip_personal a', function (event) {
          event.preventDefault();
          var type_user = $('#type_user_personal').val();
          var page = $(this).attr('data-page');
          if ( type_user ==  'personal' ) {
            var link = '/admin/payments/list_payment_change_ip_of_personal';
          } else {
            var link = '/admin/payments/list_payment_change_ip_of_enterprise';
          }

          $.ajax({
            url: link,
            data: {page: page},
            dataType: 'json',
            beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
            },
            success: function (data) {
              if (data.data != '') {
                screent_payment_change_ip_list(data);
              } else {
                var html = '';
                html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                $('tbody').html(html);
                $('tfoot').html('');
                $('.paginate_top').html('');
              }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
          });
    })
    /**
      Thanh toán gia han
    **/
    // so luong row
    $(document).on('change', '.payment_expired_classic' , function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $(this).val();
        $.ajax({
          url: '/admin/payments/list_expired_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
            // console.log(data);
              if (data.data != '') {
                  screent_payment_expired(data);
              } else {
                  var html = '';
                  html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // ajax phân Trang
    $(document).on('click', '.pagination_payment_expired a', function (event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_expired_classic').val();
        $.ajax({
            url: '/admin/payments/list_expired_with_qtt',
            data: {user_id: user_id, type: type, status: status, sl: sl, page: page},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data != '') {
                    screent_payment_expired(data);
                } else {
                    var html = '';
                    html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                    $('.paginate_top').html('');
                }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
        });
    })

    // filter user
    $(document).on( 'click' , '#user_payment_expired .user' , function() {
        $.ajax({
          url: '/admin/users/list_all_user',
          dataType: 'json',
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#user_payment_expired').html(html);
          },
          success: function (data) {
              var html = '';
              html += '<select style="width: 100%;">';
              html += '<option value="" selected disabled>Chọn khách hàng</option>';
              $.each(data, function (index, user) {
                html += '<option value="'+ user.id +'">'+ user.name + ' - ' + user.email +'</option>';
              })
              html += '</select>';
              $('#user_payment_expired').html(html);
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += 'Lỗi truy vấn';
            html += '<span class="float-right user">';
            html += '<i class="fas fa-filter"></i>';
            html += '<input type="hidden" id="input_filter_user" value="">';
            html += '</span>';
            $('#user_payment_expired').html(html);
          }
        })
    })

    $(document).on('change', '#user_payment_expired select' , function () {
        var user_id = $(this).val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_expired_classic').val();
        var text_user_name = $('#user_payment_expired select :selected').text();
        $.ajax({
          url: '/admin/payments/list_expired_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_user_name;
                  html += '<span class="float-right user">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_user" value="'+ user_id +'">';
                  html += '</span>';
                  $('#user_payment_expired').html(html);
                  screent_payment_expired(data);
              } else {
                  var html = '';
                  html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Khách hàng này không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_user_name;
                  html2 += '<span class="float-right user">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_user" value="">';
                  html2 += '</span>';
                  $('#user_payment_expired').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter giao dich
    $(document).on('click', '#type_payment_expired span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn hình thức thanh toán</option>';
        html += '<option value="credit">Số dư tài khoản</option>';
        html += '<option value="pay_in_office">Trực tiếp tại văn phòng</option>';
        html += '<option value="bank_transfer_vietcombank">Chuyển khoản VietComBank</option>';
        html += '<option value="bank_transfer_techcombank">Chuyển khoản TechComBank</option>';
        html += '<option value="momo">MoMo</option>';
        html += '<option value="cloudzone_point">Cloudzone Point</option>';
        html += '</select>';
        $('#type_payment_expired').html(html);
    })

    $(document).on('change', '#type_payment_expired select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $(this).val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_expired_classic').val();
        var text_type = $('#type_payment_expired select :selected').text();
        $.ajax({
          url: '/admin/payments/list_expired_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_type;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_type" value="'+ type +'">';
                  html += '</span>';
                  $('#type_payment_expired').html(html);
                  screent_payment_expired(data);
              } else {
                  var html = '';
                  html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_type;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_type" value="">';
                  html2 += '</span>';
                  $('#type_payment_expired').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter trạng thái
    $(document).on('click', '#status_payment_expired span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn trạng thái</option>';
        html += '<option value="Pending">Chưa thanh toán</option>';
        html += '<option value="Active">Đã thanh toán</option>';
        html += '<option value="confirm">Đã xác nhận</option>';
        html += '</select>';
        $('#status_payment_expired').html(html);
    })

    $(document).on('change', '#status_payment_expired select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $(this).val();
        var sl = $('.payment_expired_classic').val();
        var text_stauts = $('#status_payment_expired select :selected').text();
        $.ajax({
          url: '/admin/payments/list_expired_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += 'Trạng thái ' + text_stauts;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html += '</span>';
                  $('#status_payment_expired').html(html);
                  screent_payment_expired(data);
              } else {
                  var html = '';
                  html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += 'Trạng thái ' + text_stauts;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html2 += '</span>';
                  $('#status_payment_expired').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    $(document).on('click', '#expired_type_user', function () {
      $('#type_user_personal').val('personal');
      $.ajax({
        url: '/admin/payments/list_payment_expired_of_personal',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data.data);
          if (data.data.length > 0) {
            screent_payment_expire_list(data);
          } else {
            var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
            $('tbody').html(html);
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
          $('tbody').html(html);
        }
      })
    })

    $(document).on('click', '#expired_type_enterprise', function () {
      $('#type_user_personal').val('enterprise');
      $.ajax({
        url: '/admin/payments/list_payment_expired_of_enterprise',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data);
          if (data.data.length > 0) {
            screent_payment_expire_list(data);
          } else {
            var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
            $('tbody').html(html);
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
          $('tbody').html(html);
        }
      })
    })

    function screent_payment_expire_list(data) {
        var html = '';
        $.each(data.data, function (index , payment) {
           html += '<tr>';
           // id
           html += '<td>' + payment.id + '</td>';
           // magd
           html += '<td>' + payment.ma_gd + '</td>';
           // user
           html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
           // Loại
           html += '<td>' + payment.text_type + '</td>';
           // hình thức giao dịch
           html += '<td> ' + payment.loai_gd + ' </td>';
           // total
           html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
           // date create
           html += '<td>' + payment.text_created_at + '</td>';
           // date_gd
           html += '<td>' + payment.text_date_gd + '</td>';
           // status
           html += '<td class="payment-status">';
           if (payment.status == 'Active') {
             html += '<span class="text-success">Đã thanh toán</span>';
           }
           else if (payment.status == 'Pending') {
             html += '<span class="text-danger">Chưa thanh toán</span>';
           }
           else if (payment.status == 'confirm') {
             html += '<span class="text-info">Đã xác nhận</span>';
           }
           else {
             html += '<span class="text-danger">Thanh toán lỗi</span>';
           }
           html += '</td>';
           // action
           html += '<td class="table-button">';
           if (payment.status == 'Pending' || payment.status == 'confirm') {
             html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
           }
           html += '<a href="/admin/payment/detail/'+ payment.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>';
           html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
           html += '</td>';
           html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_expired_personal>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_expired_personal>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_expired_personal">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_expired_personal">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    function screent_payment_expired(data) {
        var html = '';
        $.each(data.data, function (index , payment) {
           html += '<tr>';
           // id
           html += '<td>' + payment.id + '</td>';
           // magd
           html += '<td>' + payment.ma_gd + '</td>';
           // user
           html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
           // Loại
           html += '<td>' + payment.text_type + '</td>';
           // hình thức giao dịch
           html += '<td> ' + payment.loai_gd + ' </td>';
           // total
           html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
           // date create
           html += '<td>' + payment.text_created_at + '</td>';
           // date_gd
           html += '<td>' + payment.text_date_gd + '</td>';
           // status
           html += '<td class="payment-status">';
           if (payment.status == 'Active') {
             html += '<span class="text-success">Đã thanh toán</span>';
           }
           else if (payment.status == 'Pending') {
             html += '<span class="text-danger">Chưa thanh toán</span>';
           }
           else if (payment.status == 'confirm') {
             html += '<span class="text-info">Đã xác nhận</span>';
           }
           else {
             html += '<span class="text-danger">Thanh toán lỗi</span>';
           }
           html += '</td>';
           // action
           html += '<td class="table-button">';
           if (payment.status == 'Pending' || payment.status == 'confirm') {
             html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
           }
           html += '<a href="/admin/payment/detail/'+ payment.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>';
           html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
           html += '</td>';
           html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_expired>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_expired>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_expired">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_expired">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    $(document).on('click', '.pagination_payment_expired_personal a', function (event) {
          event.preventDefault();
          var type_user = $('#type_user_personal').val();
          var page = $(this).attr('data-page');
          if ( type_user ==  'personal' ) {
            var link = '/admin/payments/list_payment_expired_of_personal';
          } else {
            var link = '/admin/payments/list_payment_expired_of_enterprise';
          }

          $.ajax({
            url: link,
            data: {page: page},
            dataType: 'json',
            beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
            },
            success: function (data) {
              if (data.data != '') {
                screent_payment_change_ip_list(data);
              } else {
                var html = '';
                html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                $('tbody').html(html);
                $('tfoot').html('');
                $('.paginate_top').html('');
              }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
          });
    })
    /**
      Thanh toán domain
    **/
    // so luong row
    $(document).on('change', '.payment_domain_classic' , function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $(this).val();
        $.ajax({
          url: '/admin/payments/list_domain_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
            // console.log(data);
              if (data.data != '') {
                  screent_payment_domain(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // ajax phân Trang
    $(document).on('click', '.pagination_payment_domain a', function (event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_domain_classic').val();
        $.ajax({
            url: '/admin/payments/list_domain_with_qtt',
            data: {user_id: user_id, type: type, status: status, sl: sl, page: page},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data != '') {
                    screent_payment_domain(data);
                } else {
                    var html = '';
                    html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                    $('.paginate_top').html('');
                }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
        });
    })

    // filter user
    $(document).on( 'click' , '#user_payment_domain .user' , function() {
        $.ajax({
          url: '/admin/users/list_all_user',
          dataType: 'json',
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#user_payment_domain').html(html);
          },
          success: function (data) {
              var html = '';
              html += '<select style="width: 100%;">';
              html += '<option value="" selected disabled>Chọn khách hàng</option>';
              $.each(data, function (index, user) {
                html += '<option value="'+ user.id +'">'+ user.name + ' - ' + user.email +'</option>';
              })
              html += '</select>';
              $('#user_payment_domain').html(html);
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += 'Lỗi truy vấn';
            html += '<span class="float-right user">';
            html += '<i class="fas fa-filter"></i>';
            html += '<input type="hidden" id="input_filter_user" value="">';
            html += '</span>';
            $('#user_payment_domain').html(html);
          }
        })
    })

    $(document).on('change', '#user_payment_domain select' , function () {
        var user_id = $(this).val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_domain_classic').val();
        var text_user_name = $('#user_payment_domain select :selected').text();
        $.ajax({
          url: '/admin/payments/list_domain_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_user_name;
                  html += '<span class="float-right user">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_user" value="'+ user_id +'">';
                  html += '</span>';
                  $('#user_payment_domain').html(html);
                  screent_payment_domain(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Khách hàng này không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_user_name;
                  html2 += '<span class="float-right user">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_user" value="">';
                  html2 += '</span>';
                  $('#user_payment_domain').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter giao dich
    $(document).on('click', '#type_payment_domain span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn hình thức thanh toán</option>';
        html += '<option value="credit">Số dư tài khoản</option>';
        html += '<option value="pay_in_office">Trực tiếp tại văn phòng</option>';
        html += '<option value="bank_transfer_vietcombank">Chuyển khoản VietComBank</option>';
        html += '<option value="bank_transfer_techcombank">Chuyển khoản TechComBank</option>';
        html += '<option value="momo">MoMo</option>';
        html += '<option value="cloudzone_point">Cloudzone Point</option>';
        html += '</select>';
        $('#type_payment_domain').html(html);
    })

    $(document).on('change', '#type_payment_domain select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $(this).val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_domain_classic').val();
        var text_type = $('#type_payment_domain select :selected').text();
        $.ajax({
          url: '/admin/payments/list_domain_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_type;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_type" value="'+ type +'">';
                  html += '</span>';
                  $('#type_payment_domain').html(html);
                  screent_payment_domain(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_type;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_type" value="">';
                  html2 += '</span>';
                  $('#type_payment_domain').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter trạng thái
    $(document).on('click', '#status_payment_domain span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn trạng thái</option>';
        html += '<option value="Pending">Chưa thanh toán</option>';
        html += '<option value="Active">Đã thanh toán</option>';
        html += '<option value="confirm">Đã xác nhận</option>';
        html += '</select>';
        $('#status_payment_domain').html(html);
    })

    $(document).on('change', '#status_payment_domain select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $(this).val();
        var sl = $('.payment_domain_classic').val();
        var text_stauts = $('#status_payment_domain select :selected').text();
        $.ajax({
          url: '/admin/payments/list_domain_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += 'Trạng thái ' + text_stauts;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html += '</span>';
                  $('#status_payment_domain').html(html);
                  screent_payment_domain(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += 'Trạng thái ' + text_stauts;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html2 += '</span>';
                  $('#status_payment_domain').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    $(document).on('click', '#create_domain_type_user', function () {
      $('#type_user_personal').val('personal');
      $.ajax({
        url: '/admin/payments/list_payment_create_domain_of_personal',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data.data);
          if (data.data.length > 0) {
            screent_payment_domain_list(data);
          } else {
            var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
            $('tbody').html(html);
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
          $('tbody').html(html);
        }
      })
    })

    $(document).on('click', '#create_domain_type_enterprise', function () {
      $('#type_user_personal').val('enterprise');
      $.ajax({
        url: '/admin/payments/list_payment_domain_create_of_enterprise',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data);
          if (data.data.length > 0) {
            screent_payment_domain_list(data);
          } else {
            var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
            $('tbody').html(html);
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
          $('tbody').html(html);
        }
      })
    })

    function screent_payment_domain_list(data) {
        var html = '';
        $.each(data.data, function (index , payment) {
           html += '<tr>';
           // id
           html += '<td>' + payment.id + '</td>';
           // magd
           html += '<td>' + payment.ma_gd + '</td>';
           // user
           html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
           // hình thức giao dịch
           html += '<td> ' + payment.loai_gd + ' </td>';
           // total
           html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
           // date create
           html += '<td>' + payment.text_created_at + '</td>';
           // date_gd
           html += '<td>' + payment.text_date_gd + '</td>';
           // status
           html += '<td class="payment-status">';
           if (payment.status == 'Active') {
             html += '<span class="text-success">Đã thanh toán</span>';
           }
           else if (payment.status == 'Pending') {
             html += '<span class="text-danger">Chưa thanh toán</span>';
           }
           else if (payment.status == 'confirm') {
             html += '<span class="text-info">Đã xác nhận</span>';
           }
           else {
             html += '<span class="text-danger">Thanh toán lỗi</span>';
           }
           html += '</td>';
           // action
           html += '<td class="table-button">';
           if (payment.status == 'Pending' || payment.status == 'confirm') {
             html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
           }
           html += '<a href="/admin/payment/detail/'+ payment.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>';
           html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
           html += '</td>';
           html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_domain_personal>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_domain_personal>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_domain_personal">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_domain_personal">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    function screent_payment_domain(data) {
        var html = '';
        $.each(data.data, function (index , payment) {
           html += '<tr>';
           // id
           html += '<td>' + payment.id + '</td>';
           // magd
           html += '<td>' + payment.ma_gd + '</td>';
           // user
           html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
           // hình thức giao dịch
           html += '<td> ' + payment.loai_gd + ' </td>';
           // total
           html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
           // date create
           html += '<td>' + payment.text_created_at + '</td>';
           // date_gd
           html += '<td>' + payment.text_date_gd + '</td>';
           // status
           html += '<td class="payment-status">';
           if (payment.status == 'Active') {
             html += '<span class="text-success">Đã thanh toán</span>';
           }
           else if (payment.status == 'Pending') {
             html += '<span class="text-danger">Chưa thanh toán</span>';
           }
           else if (payment.status == 'confirm') {
             html += '<span class="text-info">Đã xác nhận</span>';
           }
           else {
             html += '<span class="text-danger">Thanh toán lỗi</span>';
           }
           html += '</td>';
           // action
           html += '<td class="table-button">';
           if (payment.status == 'Pending' || payment.status == 'confirm') {
             html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
           }
           html += '<a href="/admin/payment/detail/'+ payment.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>';
           html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
           html += '</td>';
           html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_domain>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_domain>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_domain">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_domain">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    $(document).on('click', '.pagination_payment_domain_personal a', function (event) {
          event.preventDefault();
          var type_user = $('#type_user_personal').val();
          var page = $(this).attr('data-page');
          if ( type_user ==  'personal' ) {
            var link = '/admin/payments/list_payment_create_domain_of_personal';
          } else {
            var link = '/admin/payments/list_payment_create_domain_of_enterprise';
          }

          $.ajax({
            url: link,
            data: {page: page},
            dataType: 'json',
            beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
            },
            success: function (data) {
              if (data.data != '') {
                screent_payment_domain_list(data);
              } else {
                var html = '';
                html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                $('tbody').html(html);
                $('tfoot').html('');
                $('.paginate_top').html('');
              }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
          });
    })
    /**
      Thanh toán gia han domain
    **/
    // so luong row
    $(document).on('change', '.payment_domain_expired_classic' , function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $(this).val();
        $.ajax({
          url: '/admin/payments/list_domain_exp_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
            // console.log(data);
              if (data.data != '') {
                  screent_payment_domain_expired(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // ajax phân Trang
    $(document).on('click', '.pagination_payment_domain_expired a', function (event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_domain_expired_classic').val();
        $.ajax({
            url: '/admin/payments/list_domain_exp_with_qtt',
            data: {user_id: user_id, type: type, status: status, sl: sl, page: page},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data != '') {
                    screent_payment_domain_expired(data);
                } else {
                    var html = '';
                    html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                    $('.paginate_top').html('');
                }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
        });
    })

    // filter user
    $(document).on( 'click' , '#user_payment_domain_expired .user' , function() {
        $.ajax({
          url: '/admin/users/list_all_user',
          dataType: 'json',
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#user_payment_domain_expired').html(html);
          },
          success: function (data) {
              var html = '';
              html += '<select style="width: 100%;">';
              html += '<option value="" selected disabled>Chọn khách hàng</option>';
              $.each(data, function (index, user) {
                html += '<option value="'+ user.id +'">'+ user.name + ' - ' + user.email +'</option>';
              })
              html += '</select>';
              $('#user_payment_domain_expired').html(html);
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += 'Lỗi truy vấn';
            html += '<span class="float-right user">';
            html += '<i class="fas fa-filter"></i>';
            html += '<input type="hidden" id="input_filter_user" value="">';
            html += '</span>';
            $('#user_payment_domain_expired').html(html);
          }
        })
    })

    $(document).on('change', '#user_payment_domain_expired select' , function () {
        var user_id = $(this).val();
        var type = $('#input_filter_type').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_domain_expired_classic').val();
        var text_user_name = $('#user_payment_domain_expired select :selected').text();
        $.ajax({
          url: '/admin/payments/list_domain_exp_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_user_name;
                  html += '<span class="float-right user">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_user" value="'+ user_id +'">';
                  html += '</span>';
                  $('#user_payment_domain_expired').html(html);
                  screent_payment_domain_expired(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Khách hàng này không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_user_name;
                  html2 += '<span class="float-right user">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_user" value="">';
                  html2 += '</span>';
                  $('#user_payment_domain_expired').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter giao dich
    $(document).on('click', '#type_payment_domain_expired span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn hình thức thanh toán</option>';
        html += '<option value="credit">Số dư tài khoản</option>';
        html += '<option value="pay_in_office">Trực tiếp tại văn phòng</option>';
        html += '<option value="bank_transfer_vietcombank">Chuyển khoản VietComBank</option>';
        html += '<option value="bank_transfer_techcombank">Chuyển khoản TechComBank</option>';
        html += '<option value="momo">MoMo</option>';
        html += '<option value="cloudzone_point">Cloudzone Point</option>';
        html += '</select>';
        $('#type_payment_domain_expired').html(html);
    })

    $(document).on('change', '#type_payment_domain_expired select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $(this).val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_domain_expired_classic').val();
        var text_type = $('#type_payment_domain_expired select :selected').text();
        $.ajax({
          url: '/admin/payments/list_domain_exp_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_type;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_type" value="'+ type +'">';
                  html += '</span>';
                  $('#type_payment_domain_expired').html(html);
                  screent_payment_domain_expired(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_type;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_type" value="">';
                  html2 += '</span>';
                  $('#type_payment_domain_expired').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter trạng thái
    $(document).on('click', '#status_payment_domain_expired span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn trạng thái</option>';
        html += '<option value="Pending">Chưa thanh toán</option>';
        html += '<option value="Active">Đã thanh toán</option>';
        html += '<option value="confirm">Đã xác nhận</option>';
        html += '</select>';
        $('#status_payment_domain_expired').html(html);
    })

    $(document).on('change', '#status_payment_domain_expired select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var status = $(this).val();
        var sl = $('.payment_domain_expired_classic').val();
        var text_stauts = $('#status_payment_domain_expired select :selected').text();
        $.ajax({
          url: '/admin/payments/list_domain_exp_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += 'Trạng thái ' + text_stauts;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html += '</span>';
                  $('#status_payment_domain_expired').html(html);
                  screent_payment_domain_expired(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += 'Trạng thái ' + text_stauts;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html2 += '</span>';
                  $('#status_payment_domain_expired').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    $(document).on('click', '#expire_domain_type_user', function () {
      $('#type_user_personal').val('personal');
      $.ajax({
        url: '/admin/payments/list_payment_expire_domain_of_personal',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data.data);
          if (data.data.length > 0) {
            screent_payment_expire_domain_list(data);
          } else {
            var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
            $('tbody').html(html);
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
          $('tbody').html(html);
        }
      })
    })

    $(document).on('click', '#expire_domain_type_enterprise', function () {
      $('#type_user_personal').val('enterprise');
      $.ajax({
        url: '/admin/payments/list_payment_domain_expire_of_enterprise',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data);
          if (data.data.length > 0) {
            screent_payment_expire_domain_list(data);
          } else {
            var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
            $('tbody').html(html);
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
          $('tbody').html(html);
        }
      })
    })

    function screent_payment_expire_domain_list(data) {
        var html = '';
        $.each(data.data, function (index , payment) {
           html += '<tr>';
           // id
           html += '<td>' + payment.id + '</td>';
           // magd
           html += '<td>' + payment.ma_gd + '</td>';
           // user
           html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
           // hình thức giao dịch
           html += '<td> ' + payment.loai_gd + ' </td>';
           // total
           html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
           // date create
           html += '<td>' + payment.text_created_at + '</td>';
           // date_gd
           html += '<td>' + payment.text_date_gd + '</td>';
           // status
           html += '<td class="payment-status">';
           if (payment.status == 'Active') {
             html += '<span class="text-success">Đã thanh toán</span>';
           }
           else if (payment.status == 'Pending') {
             html += '<span class="text-danger">Chưa thanh toán</span>';
           }
           else if (payment.status == 'confirm') {
             html += '<span class="text-info">Đã xác nhận</span>';
           }
           else {
             html += '<span class="text-danger">Thanh toán lỗi</span>';
           }
           html += '</td>';
           // action
           html += '<td class="table-button">';
           if (payment.status == 'Pending' || payment.status == 'confirm') {
             html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
           }
           html += '<a href="/admin/payment/detail/'+ payment.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>';
           html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
           html += '</td>';
           html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_domain_expired_personal>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_domain_expired_personal>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_domain_expired_personal">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_domain_expired_personal">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    function screent_payment_domain_expired(data) {
        var html = '';
        $.each(data.data, function (index , payment) {
           html += '<tr>';
           // id
           html += '<td>' + payment.id + '</td>';
           // magd
           html += '<td>' + payment.ma_gd + '</td>';
           // user
           html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
           // hình thức giao dịch
           html += '<td> ' + payment.loai_gd + ' </td>';
           // total
           html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
           // date create
           html += '<td>' + payment.text_created_at + '</td>';
           // date_gd
           html += '<td>' + payment.text_date_gd + '</td>';
           // status
           html += '<td class="payment-status">';
           if (payment.status == 'Active') {
             html += '<span class="text-success">Đã thanh toán</span>';
           }
           else if (payment.status == 'Pending') {
             html += '<span class="text-danger">Chưa thanh toán</span>';
           }
           else if (payment.status == 'confirm') {
             html += '<span class="text-info">Đã xác nhận</span>';
           }
           else {
             html += '<span class="text-danger">Thanh toán lỗi</span>';
           }
           html += '</td>';
           // action
           html += '<td class="table-button">';
           if (payment.status == 'Pending' || payment.status == 'confirm') {
             html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
           }
           html += '<a href="/admin/payment/detail/'+ payment.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>';
           html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
           html += '</td>';
           html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_domain_expired>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_domain_expired>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_domain_expired">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_domain_expired">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    $(document).on('click', '.pagination_payment_domain_expired_personal a', function (event) {
          event.preventDefault();
          var type_user = $('#type_user_personal').val();
          var page = $(this).attr('data-page');
          if ( type_user ==  'personal' ) {
            var link = '/admin/payments/list_payment_expire_domain_of_personal';
          } else {
            var link = '/admin/payments/list_payment_domain_expire_of_enterprise';
          }

          $.ajax({
            url: link,
            data: {page: page},
            dataType: 'json',
            beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
            },
            success: function (data) {
              if (data.data != '') {
                screent_payment_expire_domain_list(data);
              } else {
                var html = '';
                html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                $('tbody').html(html);
                $('tfoot').html('');
                $('.paginate_top').html('');
              }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
          });
    })
    /**
      all
    **/
    // so luong row
    $(document).on('change', '.payment_all_classic' , function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var method = $('#input_filter_method').val();
        var status = $('#input_filter_status').val();
        var sl = $(this).val();
        $.ajax({
          url: '/admin/payments/list_all_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl, method: method},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
            // console.log(data);
              if (data.data != '') {
                  screent_payment_all(data);
              } else {
                  var html = '';
                  html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // ajax phân Trang
    $(document).on('click', '.pagination_payment_all a', function (event) {
        event.preventDefault();
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var method = $('#input_filter_method').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_all_classic').val();
        var page = $(this).attr('data-page');
        $.ajax({
            url: '/admin/payments/list_all_with_qtt',
            data: {user_id: user_id, type: type, status: status, sl: sl, page: page, method: method},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data != '') {
                    screent_payment_all(data);
                } else {
                    var html = '';
                    html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                    $('.paginate_top').html('');
                }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
        });
    })

    // filter user
    $(document).on( 'click' , '#user_payment_all .user' , function() {
        $.ajax({
          url: '/admin/users/list_all_user',
          dataType: 'json',
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#user_payment_all').html(html);
          },
          success: function (data) {
              var html = '';
              html += '<select style="width: 100%;">';
              html += '<option value="" selected disabled>Chọn khách hàng</option>';
              $.each(data, function (index, user) {
                html += '<option value="'+ user.id +'">'+ user.name + ' - ' + user.email +'</option>';
              })
              html += '</select>';
              $('#user_payment_all').html(html);
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += 'Lỗi truy vấn';
            html += '<span class="float-right user">';
            html += '<i class="fas fa-filter"></i>';
            html += '<input type="hidden" id="input_filter_user" value="">';
            html += '</span>';
            $('#user_payment_all').html(html);
          }
        })
    })

    $(document).on('change', '#user_payment_all select' , function () {
        var user_id = $(this).val();
        var type = $('#input_filter_type').val();
        var method = $('#input_filter_method').val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_all_classic').val();
        var text_user_name = $('#user_payment_all select :selected').text();
        $.ajax({
          url: '/admin/payments/list_all_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl, method: method},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_user_name;
                  html += '<span class="float-right user">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_user" value="'+ user_id +'">';
                  html += '</span>';
                  $('#user_payment_all').html(html);
                  screent_payment_all(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Khách hàng này không có thanh toán trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_user_name;
                  html2 += '<span class="float-right user">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_user" value="">';
                  html2 += '</span>';
                  $('#user_payment_all').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter giao dich
    $(document).on('click', '#method_payment_all span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn hình thức thanh toán</option>';
        html += '<option value="credit">Số dư tài khoản</option>';
        html += '<option value="pay_in_office">Trực tiếp tại văn phòng</option>';
        html += '<option value="bank_transfer_vietcombank">Chuyển khoản VietComBank</option>';
        html += '<option value="bank_transfer_techcombank">Chuyển khoản TechComBank</option>';
        html += '<option value="momo">MoMo</option>';
        html += '<option value="cloudzone_point">Cloudzone Point</option>';
        html += '</select>';
        $('#method_payment_all').html(html);
    })

    $(document).on('change', '#method_payment_all select', function () {
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var method = $(this).val();
        var status = $('#input_filter_status').val();
        var sl = $('.payment_all_classic').val();
        var text_type = $('#method_payment_all select :selected').text();
        $.ajax({
          url: '/admin/payments/list_all_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl, method: method},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += text_type;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_method" value="'+ type +'">';
                  html += '</span>';
                  $('#method_payment_all').html(html);
                  screent_payment_all(data);
              } else {
                  var html = '';
                  html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += text_type;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_method" value="">';
                  html2 += '</span>';
                  $('#method_payment_all').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter trạng thái
    $(document).on('click', '#status_payment_all span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn trạng thái</option>';
        html += '<option value="Pending">Chưa thanh toán</option>';
        html += '<option value="Active">Đã thanh toán</option>';
        html += '<option value="confirm">Đã xác nhận</option>';
        html += '</select>';
        $('#status_payment_all').html(html);
    })

    $(document).on('change', '#status_payment_all select', function () {
        var status = $(this).val();
        var sl = $('.payment_all_classic').val();
        var user_id = $('#input_filter_user').val();
        var type = $('#input_filter_type').val();
        var method = $('#input_filter_method').val();
        var text_stauts = $('#status_payment_all select :selected').text();
        $.ajax({
          url: '/admin/payments/list_all_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl, method: method},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += 'Trạng thái ' + text_stauts;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html += '</span>';
                  $('#status_payment_all').html(html);
                  screent_payment_all(data);
              } else {
                  var html = '';
                  html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += 'Trạng thái ' + text_stauts;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_status" value="'+ status +'">';
                  html2 += '</span>';
                  $('#status_payment_all').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    // filter loại
    $(document).on('click', '#type_payment_all span' , function () {
        var html = '';
        html += '<select style="width: 100%;">';
        html += '<option value="" selected disabled>Chọn loại giao dịch</option>';
        html += '<option value="1">Nạp tiền tài khoản</option>';
        html += '<option value="2">Tạo mới</option>';
        html += '<option value="3">Gia hạn</option>';
        html += '<option value="4">Addon VPS</option>';
        html += '<option value="5">Đổi IP</option>';
        html += '<option value="6">Tạo Domain</option>';
        html += '<option value="7">Gia hạn Domain</option>';
        html += '<option value="8">Nâng cấp Hosting</option>';
        html += '</select>';
        $('#type_payment_all').html(html);
    })

    $(document).on('change', '#type_payment_all select', function () {
        var status = $('#input_filter_status').val();
        var sl = $('.payment_all_classic').val();
        var user_id = $('#input_filter_user').val();
        var type = $(this).val();
        var method = $('#input_filter_method').val();
        var text_stauts = $('#type_payment_all select :selected').text();
        $.ajax({
          url: '/admin/payments/list_all_with_qtt',
          data: {user_id: user_id, type: type, status: status, sl: sl, method: method},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              if (data.data != '') {
                  var html = '';
                  html += 'Loại ' + text_stauts;
                  html += '<span class="float-right">';
                  html += '<i class="fas fa-filter"></i>';
                  html += '<input type="hidden" id="input_filter_type" value="'+ type +'">';
                  html += '</span>';
                  $('#type_payment_all').html(html);
                  screent_payment_all(data);
              } else {
                  var html = '';
                  html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Loại thanh toán này không có trong dữ liệu!</span></td>';
                  $('tbody').html(html);
                  var html2 = '';
                  html2 += 'Loại ' + text_stauts;
                  html2 += '<span class="float-right">';
                  html2 += '<i class="fas fa-filter"></i>';
                  html2 += '<input type="hidden" id="input_filter_type" value="'+ type +'">';
                  html2 += '</span>';
                  $('#type_payment_all').html(html2);
                  $('tfoot').html('');
                  $('.paginate_top').html('');
              }
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
            $('tbody').html(html);
            $('tfoot').html('');
            $('.paginate_top').html('');
          }
        })
    })

    $(document).on('click', '#all_type_user', function () {
      $('#type_user_personal').val('personal');
      $.ajax({
        url: '/admin/payments/list_payment_all_of_personal',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data.data);
          if (data.data.length > 0) {
            screent_payment_all_list(data);
          } else {
            var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
            $('tbody').html(html);
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
          $('tbody').html(html);
        }
      })
    })

    $(document).on('click', '#all_type_enterprise', function () {
      $('#type_user_personal').val('enterprise');
      $.ajax({
        url: '/admin/payments/list_payment_all_of_enterprise',
        dataType: 'json',
        beforeSend: function(){
          var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data);
          if (data.data.length > 0) {
            screent_payment_all_list(data);
          } else {
            var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
            $('tbody').html(html);
            $('tfoot').html('');
          }
        },
        error: function (e) {
          console.log(e);
          var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
          $('tbody').html(html);
        }
      })
    })

    function screent_payment_all_list(data) {
        // console.log('da den 2');
        var html = '';
        $.each(data.data, function (index , payment) {
           html += '<tr>';
           // id
           html += '<td>' + payment.id + '</td>';
           // magd
           html += '<td>' + payment.ma_gd + '</td>';
           // user
           html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
           // loại
           html += '<td> ' + payment.loai_gd + ' </td>';
           // hình thức giao dịch
           html += '<td> ' + payment.hinh_thuc_gd + ' </td>';
           // total
           html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
           // date create
           html += '<td>' + payment.text_created_at + '</td>';
           // date_gd
           html += '<td>' + payment.text_date_gd + '</td>';
           // status
           html += '<td class="payment-status">';
           if (payment.status == 'Active') {
             html += '<span class="text-success">Đã thanh toán</span>';
           }
           else if (payment.status == 'Pending') {
             html += '<span class="text-danger">Chưa thanh toán</span>';
           }
           else if (payment.status == 'confirm') {
             html += '<span class="text-info">Đã xác nhận</span>';
           }
           else {
             html += '<span class="text-danger">Thanh toán lỗi</span>';
           }
           html += '</td>';
           // action
           html += '<td class="table-button">';
           if (payment.status == 'Pending' || payment.status == 'confirm') {
             html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
           }
           if (payment.type_gd != 1) {
             html += '<a href="/admin/payment/detail/'+ payment.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>';
           }
           html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
           html += '</td>';
           html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        console.log(data.total, data.perPage, data.current_page);
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_all>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_all>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_all">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_all">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    function screent_payment_all(data) {
        var html = '';
        $.each(data.data, function (index , payment) {
           html += '<tr>';
           // id
           html += '<td>' + payment.id + '</td>';
           // magd
           html += '<td>' + payment.ma_gd + '</td>';
           // user
           html += '<td><a href=" ' + payment.user_id + '">' + payment.user_name + '</a></td>';
           // loại
           html += '<td> ' + payment.loai_gd + ' </td>';
           // hình thức giao dịch
           html += '<td> ' + payment.hinh_thuc_gd + ' </td>';
           // total
           html += '<td>'+ addCommas(payment.money) +' VNĐ</td>';
           // date create
           html += '<td>' + payment.text_created_at + '</td>';
           // date_gd
           html += '<td>' + payment.text_date_gd + '</td>';
           // status
           html += '<td class="payment-status">';
           if (payment.status == 'Active') {
             html += '<span class="text-success">Đã thanh toán</span>';
           }
           else if (payment.status == 'Pending') {
             html += '<span class="text-danger">Chưa thanh toán</span>';
           }
           else if (payment.status == 'confirm') {
             html += '<span class="text-info">Đã xác nhận</span>';
           }
           else {
             html += '<span class="text-danger">Thanh toán lỗi</span>';
           }
           html += '</td>';
           // action
           html += '<td class="table-button">';
           if (payment.status == 'Pending' || payment.status == 'confirm') {
             html += '<button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '" data-magd="' + payment.ma_gd + '"><i class="fas fa-check"></i></button>';
           }
           if (payment.type_gd != 1) {
             html += '<a href="/admin/payment/detail/'+ payment.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>';
           }
           html += '<a href="" class="btn btn-danger delete-payment" data-id="' + payment.id + '" data-name="' + payment.user_name + '"><i class="fas fa-trash-alt"></i></a>'
           html += '</td>';
           html += '</tr>';
        })
        $('tbody').html(html);
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_all>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_payment_all>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('tfoot').html(html_page);
        var html_page_top = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_all">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 6) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page + 3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page > page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i <= page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
          } else {
            var page = total/per_page + 1;
            html_page_top += '<nav>';
            html_page_top += '<ul class="pagination pagination_payment_all">';
            if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

            }
            if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page_top += '</ul>';
            html_page_top += '<nav>';
          }
        }
        $('.paginate_top').html(html_page_top);
    }

    $(document).on('click', '.pagination_payment_all_personal a', function (event) {
          event.preventDefault();
          var type_user = $('#type_user_personal').val();
          var page = $(this).attr('data-page');
          if ( type_user ==  'personal' ) {
            var link = '/admin/payments/list_payment_all_of_personal';
          } else {
            var link = '/admin/payments/list_payment_all_of_enterprise';
          }

          $.ajax({
            url: link,
            data: {page: page},
            dataType: 'json',
            beforeSend: function(){
              var html = '<td colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
            },
            success: function (data) {
              if (data.data != '') {
                screent_payment_all_list(data);
              } else {
                var html = '';
                html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                $('tbody').html(html);
                $('tfoot').html('');
                $('.paginate_top').html('');
              }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="10" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
          });
    })

    function addCommas(nStr)
    {
      nStr += '';
      x = nStr.split('.');
      x1 = x[0];
      x2 = x.length > 1 ? '.' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      return x1 + x2;
    }

});
