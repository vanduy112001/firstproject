$(document).ready(function () {

    list_log();

    function list_log() {
       $.ajax({
          type: "get",
          url: "/admin/activity-log/danh-sach-log",
          dataType: "json",
          beforeSend: function(){
              var html = '<td class="text-center" colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              // 1 form-group
              if (data != '') {
                  screent_log(data);
              } else {
                  var html = '<td class="text-center text-danger" colspan="6">Không có activity log trong dữ liệu!</td>';
                  $('tbody').html(html);
              }
          },
          error: function(e) {
              console.log(e);
              var html = '<td class="text-center text-danger" colspan="6">Truy vấn activity log lỗi!</td>';
              $('tbody').html(html);
          }
       })
    }

    function screent_log(data) {
       var html = '';
       $.each(data.data, function (index, value) {
          html += '<tr>';
          html += '<td>' + value.text_created_at + '</td>';
          html += '<td><a href="/admin/users/detail/'+ value.user_id +'">' + value.user_name  + '</a></td>';
          html += '<td>' + value.model + '</td>';
          html += '<td>' + value.action + '</td>';
          html += '<td>';
          if ( value.description != null ) {
            html += value.description;
          } else {
            html += value.description_user;
          }
          html += '</td>';
          if (value.service != null) {
            html += '<td>' + value.service + '</td>';
          } else {
            html += '<td></td>';
          }

          html += '<tr>';
       })
       $('tbody').html(html);
       // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        // console.log(per_page, per_page, current_page);
        if (total > per_page) {
            if ( total / per_page > 11 ) {
                var page = parseInt(total/per_page + 1);
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_list_vps>';
                if (current_page != 1) {
                  html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                } else {
                  html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                  for (var i = 1; i < 9; i++) {
                    var active = '';
                    if (i == current_page) {
                      active = 'active';
                    }
                    if (active == 'active') {
                      html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                  }
                  html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = page - 1; i <= page; i++) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                else if (current_page >= 7 && current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page +3; i++) {
                      var active = '';
                      if (i == current_page) {
                        active = 'active';
                      }
                      if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                      } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                      }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                }
                else if (current_page >= page - 6) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                  html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = page - 6; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                      active = 'active';
                    }
                    if (active == 'active') {
                      html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                  }
                }

                if (current_page != page.toPrecision(1)) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                } else {
                  html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '<nav>';
                html_page += '</td>';
            } else {
                var page = total/per_page + 1;
                html_page += '<td colspan="10" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_list_vps>';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                      html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '<nav>';
                html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
    }

    loadFilter();

    function loadFilter() {
      $.ajax({
        url: '/admin/users/list_all_user',
        dataType: 'json',
        beforeSend: function(){
            var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
            $('.fillter_log').html(html);
        },
        success: function (data) {
            var html = '';
            html += '<div class="row">';
            // model
            html += '<div class="col-md-3">';
            html += '<label>Chọn số lượng</label>';
            html += '<select style="width: 100%;" id="qtt" class="form-control">';
            html += '<option value="30" selected>30</option>';
            html += '<option value="50">50</option>';
            html += '<option value="100">100</option>';
            html += '<option value="200">200</option>';
            html += '<option value="300">300</option>';
            html += '</select>';
            html += '</div>';
            // user
            html += '<div class="col-md-3" id="select_product_form">';
            html += '<label>Chọn khách hàng</label>';
            html += '<select style="width: 100%;" id="user" class="form-control select2">';
            html += '<option value="" selected disabled>Chọn khách hàng</option>';
            html += '<option value="999999">Hệ thống Portal</option>';
            html += '<option value="999998">Hệ thống Dashboard</option>';
            html += '<option value="999997">Hệ thống DirectAdmin</option>';
            $.each(data, function (index, user) {
              html += '<option value="'+ user.id +'">'+ user.name + ' - ' + user.email +'</option>';
            })
            html += '</select>';
            html += '</div>';
            // action
            html += '<div class="col-md-3">';
            html += '<label>Chọn hành động</label>';
            html += '<select style="width: 100%;" id="action" class="form-control select2">';
            html += '<option value="" selected disabled>Chọn hành động</option>';
            html += '<option value="đăng nhập">Đăng nhập</option>';
            html += '<option value="đăng xuất">Đăng xuất</option>';
            html += '<option value="tạo">Tạo</option>';
            html += '<option value="chỉnh sửa">Chỉnh sửa</option>';
            html += '<option value="thêm">Thêm</option>';
            html += '<option value="xóa">Xóa</option>';
            html += '<option value="đặt hàng">Đặt hàng</option>';
            html += '<option value="Tạo nạp tiền">Tạo nạp tiền</option>';
            html += '<option value="yêu cầu nạp tiền">Yêu cầu nạp tiền</option>';
            html += '<option value="thanh toán">Thanh toán</option>';
            html += '<option value="xác nhân">Xác nhận</option>';
            html += '<option value="tắt">Tắt dịch vụ</option>';
            html += '<option value="bật dịch vụ">Bật dịch vụ</option>';
            html += '<option value="khởi động lại">Khởi động lại</option>';
            html += '<option value="truy cập console">Truy cập console</option>';
            html += '<option value="gia hạn">Gia hạn</option>';
            html += '<option value="cấu hình thêm">Cấu hình thêm</option>';
            html += '<option value="đổi IP">Đổi IP</option>';
            html += '<option value="hủy dịch vụ">Hủy dịch vụ</option>';
            html += '<option value="cập nhật">Cập nhật</option>';
            html += '<option value="tự động xác nhận dịch vụ">Tự động xác nhận dịch vụ</option>';
            html += '<option value="tự động">Tự động gia hạn</option>';
            html += '<option value="mail_cron_nearly">Nhắc gia hạn</option>';
            html += '</select>';
            html += '</div>';
            // service
            html += '<div class="col-md-3">';
            html += '<label>Tìm dịch vụ</label>';
            html += '<input type="text" id="service" class="form-control" placeholder="Tìm kiếm dịch vụ">';
            html += '</div>';

            html += '</div>';

            $('.fillter_log').html(html);
            $('.select2').select2();
        },
        error: function (e) {
          console.log(e);
          var html = '';
          html += 'Lỗi truy vấn';
          $('.fillter_log').html(html);
        }
      })
    }

    $(document).on('change', '#qtt', function () {
        var qtt = $(this).val();
        var user = $('#user').val();
        var action = $('#action').val();
        var service = $('#service').val();
        $.ajax({
           type: "get",
           url: "/admin/activity-log/danh-sach-log-theo-hanh-dong",
           data: { qtt: qtt, user: user, action: action, service: service  },
           dataType: "json",
           beforeSend: function(){
               var html = '<td class="text-center" colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('tbody').html(html);
           },
           success: function (data) {
               // 1 form-group
               if (data != '') {
                   screent_log(data);
               } else {
                   var html = '<td class="text-center text-danger" colspan="6">Không có activity log trong dữ liệu!</td>';
                   $('tbody').html(html);
               }
           },
           error: function(e) {
               console.log(e);
               var html = '<td class="text-center text-danger" colspan="6">Truy vấn activity log lỗi!</td>';
               $('tbody').html(html);
           }
        })

    })

    $(document).on('change', '#user', function () {
        var qtt = $('#qtt').val();
        var user = $(this).val();
        var action = $('#action').val();
        var service = $('#service').val();
        $.ajax({
           type: "get",
           url: "/admin/activity-log/danh-sach-log-theo-hanh-dong",
           data: { qtt: qtt, user: user, action: action, service: service  },
           dataType: "json",
           beforeSend: function(){
               var html = '<td class="text-center" colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('tbody').html(html);
           },
           success: function (data) {
               // 1 form-group
               if (data != '') {
                   screent_log(data);
               } else {
                   var html = '<td class="text-center text-danger" colspan="6">Không có activity log trong dữ liệu!</td>';
                   $('tbody').html(html);
               }
           },
           error: function(e) {
               console.log(e);
               var html = '<td class="text-center text-danger" colspan="6">Truy vấn activity log lỗi!</td>';
               $('tbody').html(html);
           }
        })

    })

    $(document).on('change', '#action', function () {
        var qtt = $('#qtt').val();
        var user = $('#user').val();
        var action = $('#action').val();
        var service = $('#service').val();
        $.ajax({
           type: "get",
           url: "/admin/activity-log/danh-sach-log-theo-hanh-dong",
           data: { qtt: qtt, user: user, action: action, service: service  },
           dataType: "json",
           beforeSend: function(){
               var html = '<td class="text-center" colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('tbody').html(html);
           },
           success: function (data) {
               // 1 form-group
               if (data != '') {
                   screent_log(data);
               } else {
                   var html = '<td class="text-center text-danger" colspan="6">Không có activity log trong dữ liệu!</td>';
                   $('tbody').html(html);
               }
           },
           error: function(e) {
               console.log(e);
               var html = '<td class="text-center text-danger" colspan="6">Truy vấn activity log lỗi!</td>';
               $('tbody').html(html);
           }
        })

    })

    $(document).on('keyup', '#service', function () {
        var qtt = $('#qtt').val();
        var user = $('#user').val();
        var action = $('#action').val();
        var service = $('#service').val();
        $.ajax({
           type: "get",
           url: "/admin/activity-log/danh-sach-log-theo-hanh-dong",
           data: { qtt: qtt, user: user, action: action, service: service  },
           dataType: "json",
           beforeSend: function(){
               var html = '<td class="text-center" colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('tbody').html(html);
           },
           success: function (data) {
               // 1 form-group
               if (data != '') {
                   screent_log(data);
               } else {
                   var html = '<td class="text-center text-danger" colspan="6">Không có activity log trong dữ liệu!</td>';
                   $('tbody').html(html);
               }
           },
           error: function(e) {
               console.log(e);
               var html = '<td class="text-center text-danger" colspan="6">Truy vấn activity log lỗi!</td>';
               $('tbody').html(html);
           }
        })

    })

    $(document).on('click', '.pagination a', function(event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var qtt = $('#qtt').val();
        var user = $('#user').val();
        var action = $('#action').val();
        var service = $('#service').val();
        $.ajax({
           type: "get",
           url: "/admin/activity-log/danh-sach-log-theo-hanh-dong",
           data: { qtt: qtt, user: user, action: action, service: service, page: page  },
           dataType: "json",
           beforeSend: function(){
               var html = '<td class="text-center" colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('tbody').html(html);
           },
           success: function (data) {
               // 1 form-group
               if (data != '') {
                   screent_log(data);
               } else {
                   var html = '<td class="text-center text-danger" colspan="6">Không có activity log trong dữ liệu!</td>';
                   $('tbody').html(html);
               }
           },
           error: function(e) {
               console.log(e);
               var html = '<td class="text-center text-danger" colspan="6">Truy vấn activity log lỗi!</td>';
               $('tbody').html(html);
           }
        });
    })

})
