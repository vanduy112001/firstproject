$(document).ready(function () {

    var method = $('input[name=method_pay]:checked').val();
    // console.log(method);
    method_selected(method);

    function method_selected(method) {
        // console.log(method);
        var html = '';
        if(method == 'pay_in_office') {
            $('.selected-pay-in').removeClass('text-center');
            $('.selected-pay-in').removeClass('text-right');
            $('.selected-pay-in').addClass('text-left');
            html += '<div class="company-name">Công ty TNHH MTV Đại Việt Số</div>';
            html += '<div class="info-company">';
            html += '<b>Địa chỉ Văn phòng:</b> 67 - Nguyễn Thị Định - quận Sơn Trà - Thành phố Đà Nẵng <br>';
            html += '<b>Điện thoại hỗ trợ:</b> 0236 4455 789';
            html += '</div>';
        }
        else if (method == 'bank_transfer_vietcombank') {
            $('.selected-pay-in').addClass('text-center');
            $('.selected-pay-in').removeClass('text-right');
            $('.selected-pay-in').removeClass('text-left');
            html += '<div class="info-company">';
            html += '<div class="info-bank">';
            html += '<b>Bank Name:</b> Vietcombank <br>';
            html +=  '<b>Account Name:</b> Nguyen Thi Ai Hoa <br>';
            html += '<b>Account Number:</b> 0041000830880 <br>';
            html += '</div>';
            html += '</div>';
        }
        else if (method == 'bank_transfer_techcombank') {
            $('.selected-pay-in').addClass('text-center');
            $('.selected-pay-in').removeClass('text-right');
            $('.selected-pay-in').removeClass('text-left');
            html += '<div class="info-company">';
            html += '<div class="info-bank">';
            html += '<b>Bank Name:</b> Techcombank <br>';
            html +=  '<b>Account Name:</b> Nguyen Thi Ai Hoa <br>';
            html += '<b>Account Number:</b> 19033827040017 <br>';
            html += '</div>';
            html += '</div>';
        }
        else if (method == 'momo') {
            $('.selected-pay-in').removeClass('text-center');
            $('.selected-pay-in').addClass('text-right');
            $('.selected-pay-in').removeClass('text-left');
            html += '<div class="info-company">';
            html += '<div class="info-bank">';
            html +=  '<b>Account Name:</b> Lê Minh Chí <br>';
            html += '<b>Account Number:</b> 0905 091 805 <br>';
            html += '</div>';
            html += '</div>';
        }
        $('.selected-pay-in').html(html);
    }

    $('#pay_in_office').on('click', function () {
        method_selected('pay_in_office');
    });
    $('#bank_transfer_vietcombank').on('click', function () {
        method_selected('bank_transfer_vietcombank');
    });
    $('#bank_transfer_techcombank').on('click', function () {
        method_selected('bank_transfer_techcombank');
    });
    $('#momo').on('click', function () {
        method_selected('momo');
    });

});
