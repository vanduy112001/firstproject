$(document).ready(function () {
  $('.select2').select2();
  function generatePassword() {
    var length = 24;
    charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    $('#password').val(retVal);
  }

  loadAgency();
  function loadAgency() {
    $.ajax({
       type: "get",
       url: "/admin/agency/danh-sach-agency",
       dataType: "json",
       beforeSend: function(){
           var html = '<td class="text-center" colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
           $('tbody').html(html);
       },
       success: function (data) {
           // 1 form-group
           if (data.data != '') {
               screent_agency(data);
               $('[data-toggle="tooltip"]').tooltip();
           } else {
               var html = '<td class="text-center text-danger" colspan="6">Không có tài khoản API trong dữ liệu!</td>';
               $('tbody').html(html);
           }
       },
       error: function(e) {
           console.log(e);
           var html = '<td class="text-center text-danger" colspan="6">Truy vấn tài khoản API lỗi!</td>';
           $('tbody').html(html);
       }
    })
  }

  function screent_agency(data) {
     var html = '';
     $.each(data.data , function (index, value) {
       html += '<tr>';
       html += '<td><a href="/admin/users/detail/'+ value.user_id +'">' + value.user_name  + '</a></td>';
       html += '<td>' + value.user_api + '</td>';
       html += '<td>' + value.password_api + '</td>';
       html += '<td>';
       html += '<button type="button" name="button" class="btn btn-warning edit_agency mr-1" data-id="'+ value.id +'" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa tài khoản API"><i class="far fa-edit"></i></button>';
       html += '<button type="button" name="button" class="btn btn-danger delete_agency" data-id="'+ value.id +'" data-toggle="tooltip" data-placement="top" title="Xóa tài khoản API"><i class="far fa-trash-alt"></i></button>';
       html += '</td>';
       html += '<tr>';
     });
     $('tbody').html(html);
     // phân trang cho vps
     var total = data.total;
     var per_page = data.perPage;
     var current_page = data.current_page;
     var html_page = '';
     if (total > per_page) {
         if ( total / per_page > 11 ) {
           var page = parseInt(total/per_page + 1);
           html_page += '<td colspan="13" class="text-center link-right">';
           html_page += '<nav>';
           html_page += '<ul class="pagination pagination_vps_on">';
           if (current_page != 1) {
             html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
           } else {
             html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
           }
           if (current_page < 7) {
             for (var i = 1; i < 9; i++) {
               var active = '';
               if (i == current_page) {
                 active = 'active';
               }
               if (active == 'active') {
                 html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
               } else {
                 html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
               }
             }
             html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
             for (var i = page - 1; i <= page; i++) {
               html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
             }
           }
           else if (current_page >= 7 || current_page <= page - 7) {
               html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
               html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
               html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
               for (var i = current_page - 3; i <= current_page +3; i++) {
                 var active = '';
                 if (i == current_page) {
                   active = 'active';
                 }
                 if (active == 'active') {
                   html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                 } else {
                   html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                 }
               }
               html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
               for (var i = page - 1; i <= page; i++) {
                 html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
               }
           }
           else if (current_page >= page - 6) {
             html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
             html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
             html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
             for (var i = page - 6; i < page; i++) {
               var active = '';
               if (i == current_page) {
                 active = 'active';
               }
               if (active == 'active') {
                 html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
               } else {
                 html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
               }
             }
           }

           if (current_page != page.toPrecision(1)) {
             html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
           } else {
             html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
           }
           html_page += '</ul>';
           html_page += '</nav>';
           html_page += '</td>';
         } else {
           var page = total/per_page + 1;
           html_page += '<td colspan="13" class="text-center link-right">';
           html_page += '<nav>';
           html_page += '<ul class="pagination pagination_vps_on">';
           if (current_page != 1) {
               html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
           } else {
               html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
           }
           for (var i = 1; i < page; i++) {
               var active = '';
               if (i == current_page) {
                   active = 'active';
               }
               if (active == 'active') {
                 html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
               } else {
                 html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
               }

           }
           if (current_page != page.toPrecision(1)) {
               html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
           } else {
               html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
           }
           html_page += '</ul>';
           html_page += '</nav>';
           html_page += '</td>';
         }
     }
     $('tfoot').html(html_page);
  }

  // ajax phân trang vps
  $(document).on('click', '.pagination_vps_on a', function(event) {
      event.preventDefault();
      var page = $(this).attr('data-page');
      $.ajax({
         url: '/admin/agency/danh-sach-agency',
         dataType: 'json',
         data: { page:page },
         beforeSend: function(){
             var html = '<td class="text-center"  colspan="13"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
             $('tbody').html(html);
         },
         success: function (data) {
             // console.log(data);
             if ( data.data.length > 0 ) {
               screent_agency(data);
             } else {
               $('tfoot').html('');
               var html = '<td class="text-center text-danger" colspan="6">Không có tài khoản trong dữ liệu!</td>';
               $('tbody').html(html);
             }
         },
         error: function (e) {
             console.log(e);
             $('tfoot').html('');
             var html = '<td class="text-center text-danger" colspan="6">Truy vấn tài khoản lỗi!</td>';
             $('tbody').html(html);
         }
      })
  });

  $(document).on('click', '.create_agency', function () {
    $('#modal-product').modal('show');
    $('.modal-title').text('Tạo tài khoản API');
    $('#button-submit').val('Tạo tài khoản API');
    $('#notication').html("");
    $('#form-group-product')[0].reset();
    $('#action').val('create');
    generatePassword();
  })

  $('#button-submit').on('click', function(event) {
    event.preventDefault();
    var form_data = $('#form-group-product').serialize();
    var user = $('#user').val();
    var user_api = $('#user_api').val();
    var password = $('#password').val();
    var validate = group_product_validate(user, user_api, password);
    // console.log(form_data);
    if (validate == false) {
      $.ajax({
        type: "post",
        url: "/admin/agency/create_agency",
        data: form_data,
        dataType: "json",
        beforeSend: function(){
          var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
          $('#notication').html(html);
          $('#form-group-product').css('opacity', 0.5);
          $('#button-submit').attr('disabled', true);
          $('#form-group-product').attr('disabled', true);
        },
        success: function (response) {
          // console.log(response);
          if (response) {
            var html = '<p class="text-success">'+response+'</p>';
            $('#notication').html(html);
            $('#form-group-product')[0].reset();
            $('.modal-title').text('Tạo tài khoản API');
            $('#button-submit').val('Tạo tài khoản API');
            $('#form-group-product').css('opacity', 1);
            $('#button-submit').attr('disabled', false);
            $('#form-group-product').attr('disabled', false);
          } else {
            var html = '<p class="text-danger">Tạo nhóm sản phẩm thất bại</p>';
            $('#notication').html(html);
            $('#form-group-product').css('opacity', 1);
            $('#button-submit').attr('disabled', false);
            $('#form-group-product').attr('disabled', false);
          }
          $('#modal-product').modal('hide');
          loadAgency();
        },
        error: function(e) {
          console.log(e);
          var html = '<p class="text-danger">Tạo / Chỉnh sửa nhóm sản phẩm thất bại</p>';
          $('#notication').html(html);
          $('#form-group-product').css('opacity', 1);
          $('#button-submit').attr('disabled', false);
          $('#form-group-product').attr('disabled', false);
          $('#modal-product').modal('hide');
        }
      });
    }
  });

  $(document).on('click', '.edit_agency', function() {
    var id = $(this).attr('data-id');
    // console.log(id, $(this));
    $('#form-group-product')[0].reset();
    $.ajax({
      url: "/admin/agency/chinh-sua-agency",
      data: {id: id},
      dataType: "json",
      success: function (data) {
        // console.log(data);
        $('#modal-product').modal('show');
        $('.modal-title').text('Sửa tài khoản API');
        $('#button-submit').val('Sửa tài khoản API');
        $('#notication').html("");
        $('#notication_vld').html("");
        $('#user_api').val(data.user_api);
        $('#password').val(data.password_api);
        $('#action').val('update');
        $('#agency_id').val(data.id);
        var html = '';
        html += '<option value="" selected disabled>Chọn khách hàng</option>';
        $.each(data.users , function (index, user) {
          var selected = '';
          if (user.id == data.user_id) {
            selected = 'selected';
          }
          html += '<option value="'+ user.id +'" '+ selected +'>'+ user.name +'</option>';
        })
        $('#user').html(html);
      },
      error: function (e) {
        console.log(e);
      }
    });
  });

  // delete product
  $(document).on('click', '.delete_agency', function() {
    $('#delete-product').modal('show');
    $('tr').removeClass('choose_agency');
    var id = $(this).attr('data-id');
    var html = 'Bạn có muốn xóa tài khoản API <b class="text-danger">'+ id +'</b> này không?';
    $('#button-product').attr('data-id', id);
    $('#button-product').attr('data-action', 'group');
    $('#button-product').val('Xóa tài khoản API');
    $(this).closest('tr').addClass('choose_agency');
    $('#notication-product').html(html);
  });

  // click vao button-product
  $('#button-product').on('click', function () {
    var id = $(this).attr('data-id');
    // //console.log(id, action);
    $.ajax({
      type: "get",
      url: "/admin/agency/delete_agency",
      data:  { id: id },
      dataType: "json",
      beforeSend: function(){
        var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
        $('#notication-product').html(html);
        $('#button-product').attr('disabled', true);
        $('#delete-product').css('opacity', 0.5);
      },
      success: function (data) {
        //console.log(data);
        var html = '<div class="text-center text-danger">' + data + '</div>';
        $('#notication').html(html);
        $('#delete-product').modal('hide');
        screent_agency();
      },
      error: function(e) {
        console.log(e);
        var html = '<div class="text-center text-danger">Lỗi khi xóa sản phẩm.</div>';
        $('#notication-product').html(html);
        $('#button-product').attr('disabled', false);
        $('#delete-product').css('opacity', 1);
      }
    });
  });



  function group_product_validate(name, title, type) {

    if (name == null || name == '') {
      $('#notication_vld').text('Khác hàng không được để trống.')
      return true;
    }
    else if(title.length < 5) {
      $('#notication_vld').text('Số ký tự của tài khoản không được nhỏ hơn 5 ký tự.')
      return true;
    } else if(title.length > 200) {
      $('#notication_vld').text('Số ký tự củatài khoản được lớn hơn 200 ký tự.')
      return true;
    } else if( title == null || title == '' ) {
      $('#notication_vld').text('Tài khoản không được để trống.')
      return true;
    } else if(type == null || type == '') {
      $('#notication_vld').text('Mật khẩu không được để trống.')
      return true;
    } else {
      return false;
    }



  }
})
