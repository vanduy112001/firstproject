$(document).ready(function() {

  var current_dropdown = $('.cd-select');
  for (var i = 0; i < current_dropdown.length; i++) {
      $('.cd-dropdown' + i).dropdown( {
        gutter : 5,
        stack : false,
        delay : 100,
        slidingIn : 100
      } );
  }
// .pricing-product input
  $('.btn_add_card').on('click', function (e) {
      e.preventDefault();
      $('.card-deck').removeClass('choose_add_card');
      $(this).closest('.card-deck').addClass('choose_add_card');
      var link = $(this).attr('href');
      var billing_cycle = $('.choose_add_card .billing_cycle_product').val();
      // console.log(billing_cycle);
      window.location = link + '?billing_cycle=' + billing_cycle;

  })

  $('.card-drop .toggle').on('click', function (event) {
      event.preventDefault();
      $('.card-drop').removeClass('toggle-active');
      if ( $(this).is(".active") ) {
          $(this).closest('.card-drop').children(".toggle").removeClass("active");
          $(this).closest('.card-drop').removeClass("active-icon");
          var li = $(this).closest('.card-drop').children("ul").children("li");
          setClosed(li);
      } else {
          $('.card-drop').removeClass('toggle-active');
          $(this).addClass('active');
          $(this).closest('div').addClass('toggle-active');
          $(this).closest('div').addClass('active-icon');
          var li = $('.toggle-active li');
          var count = li.length;
          $.each(li , function (index, value) {
            // console.log(index);
            $(this).removeClass('closed');
            $(this).css("z-index" , count - i);
            $(this).css("top" , 40 * (index + 1))
            .css("width" ,"100%")
            // .css("margin-left" , "10px");
          })
      }
  })

  function setClosed(li) {
      $.each(li , function (index, value) {
        width = li.outerWidth();
        $(this).addClass('closed');
        $(this).css("top" , 0)
        .css("width" , width - index *20)
        .css("margin-left" , (index*20)/2)
        .css("margin-left" , (index*20)/2);
      })
  }
  setClosed();

  $('.card-drop ul li a').on('click', function (event) {
     event.preventDefault();
     $(this).closest('.card-drop').children(".toggle").removeClass("active");
     $(this).closest('.card-drop').removeClass("active-icon");
     $(this).closest('.card-drop').children("ul").children("li").removeClass('active');
     $(this).closest('li').addClass('active');
     var label = $(this).attr('data-label');
     $(this).closest('.card-drop').children(".toggle").children(".label-active").text(label);
     var li = $(this).closest('.card-drop').children("ul").children("li");
     setClosed(li);
     $(this).closest('.card-body').addClass('choose_billing');
     var id_product = $(this).attr('data-product');
     var billing_cycle = $(this).attr('data-billing');
     $('.choose_billing .billing_cycle_product').val(billing_cycle);
     $.ajax({
        url: '/check-event-product',
        dataType: 'json',
        data: { id: id_product, billing_cycle: billing_cycle },
        beforeSend: function(){
            var html = '<div><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
            $('.choose_billing .cloudzone_point').html(html);
        },
        success: function (data) {
            if (data != null) {
              $('.choose_billing .cloudzone_point').text(data.point + ' Điểm');
            } else {
              $('.choose_billing .cloudzone_point').text('Không có chương trình khuyến mãi cho thời gian thuê này');
            }
            $('.card-body').removeClass('choose_billing');
        },
        error: function (e) {
           console.log(e);
           $('.choose_billing .cloudzone_point').text('Không có chương trình khuyến mãi cho thời gian thuê này');
           $('.card-body').removeClass('choose_billing');
        }
     });

  })

});
