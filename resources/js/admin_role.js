$(document).ready(function() {
	
	listGroupRole();

    function listGroupRole() { 
        var qtt = $('#qtt').val();
        var q = $('#search').val();
        $.ajax({
            url: "/admin/admin-roles/listGroup",
            data: { qtt: qtt, q: q },
            dataType: "json",
            beforeSend: function(){
                var html = '<td  colspan="6" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if ( data.data.length > 0 ) {
                    screentGroup(data);
                    $('[data-toggle="tooltip"]').tooltip();
                } else {
                    $('tbody').html('<td colspan="6" class="text-center text-danger">Không có nhóm quản trị trong cơ sở dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="6" class="text-center text-danger">Truy vấn nhóm quản trị lỗi.</td>');
            }
        });
    }

    $("#qtt").on("change", function () {
        listGroupRole();
    });
  
    $("#search").on("keyup", function () {
        listGroupRole();
    });

    function screentGroup(data){ 
        var html = '';
        $.each(data.data, function (index , group) {
            html += '<tr>';
            // name
            if ( group.id ) {
                html += '<td><a href="/admin/admin-roles/'+ group.id +'/chi-tiet-nhom-vai-tro-quan-tri-vien.html">'+ group.id +'</a></td>';
            } else {
                html += '<td>'+ group.id +'</td>';
            }
            if ( group.id ) {
                html += '<td><a href="/admin/admin-roles/'+ group.id +'/chi-tiet-nhom-vai-tro-quan-tri-vien.html">'+ group.name +'</a></td>';
            } else {
                html += '<td>'+ group.name +'</td>';
            }
            html += '<td>'+ group.description +'</td>';
            // action
            html += '<td>';
            if ( group.id ) {
                html += '<button type="button" class="btn btn-warning btn-sm text-light editGroup" data-name="'+ group.name +'"  data-id="'+ group.id +'" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa nhóm quản trị"><i class="far fa-edit"></i></button>';
                html += '<button type="button" class="btn btn-danger btn-sm deleteGroup" data-name="'+ group.name +'"  data-id="'+ group.id +'" data-toggle="tooltip" data-placement="top" title="Xóa nhóm quản trị"><i class="far fa-trash-alt"></i></button>';
            }
            html += '</td>';
            html += '</tr>';
        })
        $('tbody').html(html);
        $('[data-toggle="tooltip"]').tooltip();
        // phân trang cho user
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if ( total / per_page > 11 ) {
              var page = parseInt(total/per_page + 1);
              html_page += '<td colspan="11" class="text-center link-right">';
              html_page += '<nav>';
              html_page += '<ul class="pagination">';
              if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
              } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
              }
              if (current_page < 7) {
                for (var i = 1; i < 9; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              else if (current_page >= 7 || current_page <= page - 7) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                  html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = current_page - 3; i <= current_page +3; i++) {
                    var active = '';
                    if (i == current_page) {
                      active = 'active';
                    }
                    if (active == 'active') {
                      html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    } else {
                      html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                    }
                  }
                  html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                  for (var i = page - 1; i <= page; i++) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
              }
              else if (current_page >= page - 6) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 6; i < page; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
              }
   
              if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
              } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
              }
              html_page += '</ul>';
              html_page += '</nav>';
              html_page += '</td>';
            } else {
              var page = total/per_page + 1;
              html_page += '<td colspan="11" class="text-center link-right">';
              html_page += '<nav>';
              html_page += '<ul class="pagination">';
              if (current_page != 1) {
                  html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
              } else {
                  html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
              }
              for (var i = 1; i < page; i++) {
                  var active = '';
                  if (i == current_page) {
                      active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
   
              }
              if (current_page != page.toPrecision(1)) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
              } else {
                  html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
              }
              html_page += '</ul>';
              html_page += '</nav>';
              html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
    }

	$("#createGroup").on('click', function () {
        $('#modal').modal('show');
        $('#modal .modal-title').text('Tạo nhóm quản trị');
        $('#notication_modal').html("");
        $('#formAction')[0].reset();
        $('#action').val('create');
        $("#buttonSubmit").fadeIn();
        $("#buttonFinish").fadeOut();
    });

    $(document).on('click', ".editGroup", function () {
        $('#modal').modal('show');
        $('#modal .modal-title').text('Sửa nhóm quản trị');
        $('#notication_modal').html("");
        $("#buttonSubmit").fadeIn();
        $("#buttonFinish").fadeOut();
        var id = $(this).attr('data-id');
        $('#formAction')[0].reset();
        $.ajax({
            url: '/admin/admin-roles/loadDetailGroup',
            type: 'get',
            data: { id: id },
            dataType: 'json',
            beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#notication_modal').html(html);
              $('#formAction').css('opacity', 0.5);
              $('#buttonSubmit').attr('disabled', true);
              $('#formAction').attr('disabled', true);
            },
            success: function (data) {
              // console.log(data);
              if (data) {
              	$('#notication_modal').html('');
                $('#formAction').css('opacity', 1);
                $('#buttonSubmit').attr('disabled', false);
                $('#formAction').attr('disabled', false);
        		$('#action').val('edit');
        		$('#idGroup').val(data.id);
        		$('#nameGroup').val(data.name);
        		$('#description').val(data.description);
              } else {
                var html = '<p class="text-danger">Không có nhóm quản trị này trong dữ liệu</p>';
                $('#notication_modal').html(html);
                $('#formAction').css('opacity', 1);
                $('#buttonSubmitt').attr('disabled', false);
                $('#formAction').attr('disabled', false)
              }
            },
            error: function (e) {
               console.log(e);
               $('#notication_modal').html('<span class="text-danger">Truy vấn nhóm khách hàng thất bại.</span>');
            }
         })

    });


    $('#buttonSubmit').on('click', function () {
        var nameGroup = $('#nameGroup').val();
        if ( nameGroup.length == 0 || nameGroup == '' ) {
            $('#notication_modal').html("<span class='text-center text-danger'>Tên nhóm quản trị không được để trống</span>");
            return false;
        }
        var form = $('#formAction').serialize();
        $.ajax({
            url: '/admin/admin-roles/actionGroup',
            type: 'post',
            data: form,
            dataType: 'json',
            beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#notication_modal').html(html);
              $('#formAction').css('opacity', 0.5);
              $('#buttonSubmit').attr('disabled', true);
              $('#formAction').attr('disabled', true);
            },
            success: function (data) {
              // console.log(data);
              if (data) {
                if ( $('#action').val() == 'edit') {
                    var html = '<p class="text-success">Sửa nhóm quản lý thành công</p>';
                } else {
                    var html = '<p class="text-success">Tạo nhóm quản lý thành công</p>';
                }
                $('#notication_modal').html(html);
                $('#formAction')[0].reset();
                $('#modal .modal-title').text('Tạo nhóm quản lý');
                $('#formAction').css('opacity', 1);
                $('#buttonSubmit').attr('disabled', false);
                $('#formAction').attr('disabled', false);
                $('#action').val('create');
                listGroupRole();
              } else {
                if ( $('#action').val() == 'edit') {
                    var html = '<p class="text-danger">Sửa nhóm quản lý thất bại</p>';
                } else {
                    var html = '<p class="text-danger">Tạo nhóm quản lý  thất bại</p>';
                }
                $('#notication_modal').html(html);
                $('#formAction').css('opacity', 1);
                $('#buttonSubmitt').attr('disabled', false);
                $('#formAction').attr('disabled', false)
              }
            },
            error: function (e) {
               console.log(e);
               $('#notication_modal').html('<span class="text-danger">Truy vấn nhóm khách hàng thất bại.</span>');
            }
         })
    });

    $(document).on('click', '.deleteGroup', function() {
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        $("tr").removeClass('action');
        $('#modalAll').modal('show');
        $("#buttonSubmitAll").fadeIn();
        $("#buttonFinishAll").fadeOut();
        $("#buttonSubmitAll").attr('data-id', id);
        $('#noticationModalAll').html('<div class="text-center">Bạn có muốn xóa nhóm quản trị <b class="text-danger">' + name + '</b> này không?</div>');
        $(this).closest('tr').addClass('action');
    });

    $("#buttonSubmitAll").on("click", function () {
        var id = $(this).attr('data-id');
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            type: "post",
            url: "/admin/admin-roles/deleteGroup",
            data: { '_token': token, id: id },
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#noticationModalAll').html(html);
            },
            success: function (data) {
                if (data) {
                    $('#noticationModalAll').html('<div class="text-center">Xóa nhóm quản trị thành công.</div>');
                    $("#buttonSubmitAll").fadeOut();
                    $("#buttonFinishAll").fadeIn();
                    $(".action").fadeOut();
                } else {
                    $('#noticationModalAll').html('<div class="text-center">Xóa nhóm quản trị thất bại.</div>');
                }
            },
            error: function(e) {
                console.log(e);
                $('#noticationModalAll').html('<div class="text-center">Truy vấn nhóm quản trị thất bại.</div>');
            }
        });
    });

});