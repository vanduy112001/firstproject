$(document).ready(function () {

    list_log();

    function list_log() {
       $.ajax({
          type: "get",
          url: "/activity-log/danh-sach-log",
          dataType: "json",
          beforeSend: function(){
              var html = '<td class="text-center" colspan="4"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              // 1 form-group
              if (data != '') {
                  screent_log(data);
              } else {
                  var html = '<td class="text-center text-danger" colspan="4">Không có activity log trong dữ liệu!</td>';
                  $('tbody').html(html);
              }
          },
          error: function(e) {
              console.log(e);
              var html = '<td class="text-center text-danger" colspan="4">Truy vấn activity log lỗi!</td>';
              $('tbody').html(html);
          }
       })
    }

    function screent_log(data) {
       var html = '';
       $.each(data, function (index, value) {
          html += '<tr>';
          html += '<td>' + value.text_created_at + '</td>';
          html += '<td>' + value.action + '</td>';
          if (value.description_user == null) {
              html += '<td></td>';
          } else {
              html += '<td>' + value.description_user + '</td>';
          }
          if (value.service != null) {
            html += '<td>' + value.service + '</td>';
          } else {
            html += '<td></td>';
          }

          html += '<tr>';
       })
       $('tbody').html(html);
    }

    $('.btn_filter').on('click', function () {
        var html = '';
        html += '<div class="row">';
        // model
        html += '<div class="col-md-4">';
        html += '<label>Chọn số lượng</label>';
        html += '<select style="width: 100%;" id="qtt" class="form-control">';
        html += '<option value="30" selected>30</option>';
        html += '<option value="50">50</option>';
        html += '<option value="100">100</option>';
        html += '<option value="200">200</option>';
        html += '<option value="300">300</option>';
        html += '</select>';
        html += '</div>';
        // action
        html += '<div class="col-md-4">';
        html += '<label>Chọn hành động</label>';
        html += '<select style="width: 100%;" id="action" class="form-control">';
        html += '<option value="" selected disabled>Chọn hành động</option>';
        html += '<option value="đăng nhập">Đăng nhập</option>';
        html += '<option value="đăng xuất">Đăng xuất</option>';
        // html += '<option value="tạo">Tạo</option>';
        // html += '<option value="chỉnh sửa">Chỉnh sửa</option>';
        // html += '<option value="thêm">Thêm</option>';
        html += '<option value="xóa">Xóa</option>';
        html += '<option value="đặt hàng">Đặt hàng</option>';
        // html += '<option value="Tạo nạp tiền">Tạo nạp tiền</option>';
        html += '<option value="yêu cầu nạp tiền">Yêu cầu nạp tiền</option>';
        html += '<option value="thanh toán">Thanh toán</option>';
        html += '<option value="tắt">Tắt dịch vụ</option>';
        html += '<option value="bật">Bật dịch vụ</option>';
        html += '<option value="khởi động lại">Khởi động lại</option>';
        html += '<option value="truy cập console">Truy cập console</option>';
        html += '<option value="hủy dịch vụ">Hủy dịch vụ</option>';
        // html += '<option value="cập nhật">Cập nhật</option>';
        // html += '<option value="tự động xác nhận dịch vụ">Tự động xác nhận dịch vụ</option>';
        html += '</select>';
        html += '</div>';
        // service
        html += '<div class="col-md-4">';
        html += '<label>Tìm dịch vụ</label>';
        html += '<input type="text" id="service" class="form-control" placeholder="Tìm kiếm dịch vụ">';
        html += '</div>';

        html += '</div>';

        $('#user_fillter').html(html);
    })

    $(document).on('change', '#qtt', function () {
        var qtt = $(this).val();
        var action = $('#action').val();
        var service = $('#service').val();
        $.ajax({
           type: "get",
           url: "/activity-log/danh-sach-log-theo-hanh-dong",
           data: { qtt: qtt, action: action, service: service  },
           dataType: "json",
           beforeSend: function(){
               var html = '<td class="text-center" colspan="4"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('tbody').html(html);
           },
           success: function (data) {
               // 1 form-group
               if (data != '') {
                   screent_log(data);
               } else {
                   var html = '<td class="text-center text-danger" colspan="4">Không có activity log trong dữ liệu!</td>';
                   $('tbody').html(html);
               }
           },
           error: function(e) {
               console.log(e);
               var html = '<td class="text-center text-danger" colspan="4">Truy vấn activity log lỗi!</td>';
               $('tbody').html(html);
           }
        })

    })

    $(document).on('change', '#action', function () {
        var qtt = $('#qtt').val();
        var action = $('#action').val();
        var service = $('#service').val();
        $.ajax({
           type: "get",
           url: "/activity-log/danh-sach-log-theo-hanh-dong",
           data: { qtt: qtt, action: action, service: service  },
           dataType: "json",
           beforeSend: function(){
               var html = '<td class="text-center" colspan="4"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('tbody').html(html);
           },
           success: function (data) {
               // 1 form-group
               if (data != '') {
                   screent_log(data);
               } else {
                   var html = '<td class="text-center text-danger" colspan="4">Không có activity log trong dữ liệu!</td>';
                   $('tbody').html(html);
               }
           },
           error: function(e) {
               console.log(e);
               var html = '<td class="text-center text-danger" colspan="4">Truy vấn activity log lỗi!</td>';
               $('tbody').html(html);
           }
        })

    })

    $(document).on('keyup', '#service', function () {
        var qtt = $('#qtt').val();
        var action = $('#action').val();
        var service = $('#service').val();
        $.ajax({
           type: "get",
           url: "/activity-log/danh-sach-log-theo-hanh-dong",
           data: { qtt: qtt, action: action, service: service  },
           dataType: "json",
           beforeSend: function(){
               var html = '<td class="text-center" colspan="4"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('tbody').html(html);
           },
           success: function (data) {
               // 1 form-group
               if (data != '') {
                   screent_log(data);
               } else {
                   var html = '<td class="text-center text-danger" colspan="4">Không có activity log trong dữ liệu!</td>';
                   $('tbody').html(html);
               }
           },
           error: function(e) {
               console.log(e);
               var html = '<td class="text-center text-danger" colspan="4">Truy vấn activity log lỗi!</td>';
               $('tbody').html(html);
           }
        })

    })

})
