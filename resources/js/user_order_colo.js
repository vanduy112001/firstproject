$(document).ready(function () {

    loadAmount();

    $('#btn_addon_ip_colo').on('click', function() {
        // console.log($(this).attr('data-type'));
        if ( $(this).attr('data-type') == 'plus' ) {
            $('#btn_addon_ip_colo i').removeClass('fa-plus');
            $('#btn_addon_ip_colo i').addClass('fa-minus');
            $(this).attr('data-type', 'minus');
            $('#addon_ip_colo').fadeIn();
        } else {
            $('#btn_addon_ip_colo i').addClass('fa-plus');
            $('#btn_addon_ip_colo i').removeClass('fa-minus');
            $(this).attr('data-type', 'plus');
            $('#addon_ip_colo').fadeOut();
        }
    })

    $('#qtt').on('keyup', function() {
        if ( isNumber($(this).val()) ) {
            if ( $(this).val() > 0 ) {
                loadAmount();
            } else {
                alert('Số lượng phải lớn hơn 0');
            }
        } else {
            alert('Số lượng phải là ký tự số');
        }
    })

    $('#billing').on('change', function () {
        loadAmount();
    })

    $('#addon_ip').on('change', function () {
        loadAmount();
    })

    function loadAmount() {
        var qtt = $('#qtt').val();
        var billing = $('#billing').val();
        var addon_ip = $('#addon_ip').val();
        var product_id = $('#product_id').val();
        // console.log(data);
        $.ajax({
            url: "/order/loadAmountColo",
            data: { qtt: qtt, billing: billing, addon_ip: addon_ip, product_id: product_id },
            dataType: "json",
            beforeSend: function() {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.before-send').css('opacity', '0.5');
                $('.before-send').html(html);
            },
            success: function (response) {
                if ( response.error ) {
                    alert(response.message);
                } else {
                    // console.log(response, billing);
                    $('.quantity-printf').text(qtt);
                    $('.item_pricing_cell').text( addCommas(response.amount) + ' ₫' );
                    $('.total_pricing').text( addCommas(response.total) + ' ₫' );
                    // disk 2
                    var html_addon_ip = '';
                    if ( addon_ip != 0 ) {
                        $('#btn_addon_ip_colo i').removeClass('fa-plus');
                        $('#btn_addon_ip_colo i').addClass('fa-minus');
                        $(this).attr('data-type', 'minus');
                        $('#addon_ip_colo').fadeIn();
                        html_addon_ip += '<td>DISK 2: '+ response.addon_ip.name +'</td>';
                        html_addon_ip += '<td class="quantity-printf text-left">'+ qtt +'</td>';
                        html_addon_ip += '<td class="item_pricing_cell text-right">'+ addCommas(response.addon_ip.pricing) +' ₫</td>';
                        $('.addon-item-ip').html(html_addon_ip);
                        var html_select_addon_ip = '<option value="0">None</option>';
                        $.each(response.list_addon_ip, function (index, product_addon_ip) { 
                            var selected = '';
                            if ( product_addon_ip.id == addon_ip ) {
                                selected = 'selected';
                            }
                            html_select_addon_ip += '<option value="'+ product_addon_ip.id +'" '+ selected +'>';
                            html_select_addon_ip += product_addon_ip.name + ' ';
                            html_select_addon_ip += addCommas(product_addon_ip.text_pricing) + ' ₫/' + product_addon_ip.text_billing_cycle;
                            html_select_addon_ip += '</option>';
                        });
                        $('#addon_ip').html(html_select_addon_ip);
                    } else {
                        $('.addon-item-ip').html('');
                    }
                }
            },
            error: function(e) {
                console.log(e);
                alert('Truy vấn lỗi');
            }
        });
    }

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

    function isNumber(n) {
        return /^-?[\d.]+(?:e-?\d+)?$/.test(n);
    }

});