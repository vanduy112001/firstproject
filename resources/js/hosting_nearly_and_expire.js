$(document).ready(function () {
    load_hosting();

    function load_hosting() {
        $.ajax({
            url: '/dich-vu/hosting/list_hosting_nearly_and_expire',
            dataType: 'json',
            beforeSend: function(){
                var html = '<td class="text-center"  colspan="10"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data.data);
                if ( data.data.length > 0 ) {
                  list_hosting_nearly_and_expire_screent(data);
                  $("#data-hosting-table").DataTable({
                      "columnDefs": [
                           { "targets": [  1, 3 , 4, 6, 7, 8, 9 ], "orderable": false }
                      ],
                      "pageLength": 30,
                      "lengthMenu": [[ 25, 50, 100, -1], [25, 50, 100, "All"]],
                      "iDisplayLength": 25
                  });
                } else {
                  $('tfoot').html('');
                  $('tbody').html('<td class="text-center text-danger"  colspan="10">Không có dịch vụ hosting nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tfoot').html('');
                $('tbody').html('<td class="text-center text-danger"  colspan="10">Lỗi truy vấn dữ liệu dịch vụ hosting!</td>');
            }
        });
    }

    function list_hosting_nearly_and_expire_screent(data) {
       // console.log(data);
       var html = '';
       $.each(data.data, function (index , hosting) {
          html += '<tr>';
          // checkbox
          html += '<td><a href="/service/detail/'+ hosting.id +'?type=hosting">'+ hosting.product_name +'</a></td>'
          // ip
          html += '<td class="ip_hosting" data-ip="'+ hosting.domain +'"><a href="/service/detail/'+ hosting.id +'?type=hosting">'+ hosting.domain +'</a>';
          if (hosting.expired == true) {
            html += ' - <span class="text-danger">Hết hạn</span>';
          } else if (hosting.isExpire == true) {
            html += ' - <span class="text-danger">Gần hết hạn</span>';
          }
          html += '</td>';
          // loai
          if ( hosting.location == 'vn' ) {
            html += '<td><span class="text-danger">Việt Nam</span></td>';
          }
          else if ( hosting.location == 'si' ) {
            html += '<td><span class="text-danger">Singapore</span></td>';
          }
          else {
            html += '<td><span class="text-danger">Mỹ</span></td>';
          }

          // khach hang
          // ngay tao
          html += '<td><span>' + hosting.date_create + '</span></td>';
          // ngay ket thuc
          html += '<td class="next_due_date">';
          if (hosting.isExpire == true || hosting.expired == true) {
            html += '<span class="text-danger">' + hosting.next_due_date + '<span>';
          } else {
            html += '<span>' + hosting.next_due_date + '<span>';
          }
          html += '</td>';
          // thoi gian thue
          html += '<td>' + hosting.text_billing_cycle +  '</td>';
          // html += '<td>' + hosting.amount + ' VNĐ</td>';
          html += '<td class="hosting-status">' + hosting.text_status_vps + '</td>';
          html += '<td class="page-service-action">';
          if (hosting.status_hosting != 'delete_hosting' ) {
            if (hosting.status_hosting != 'cancel') {
              if (hosting.status_hosting != 'suspend') {
                if (hosting.isExpire == true || hosting.expired == true) {
                  html += '<button type="button" class="btn bg-gradient-secondary button-action-hosting expired" data-toggle="tooltip" data-placement="top" title="Gia hạn hosting" data-action="expired" data-id="' + hosting.id + '" data-domain="' + hosting.domain + '"><i class="fas fa-plus-circle"></i></button>';
                } else {
                  if (hosting.status_hosting != 'admin_off') {
                    if (hosting.status_hosting != 'cancel') {
                      html += '<button class="btn btn-outline-danger button-action-hosting terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ hosting" data-action="terminated" data-id="' + hosting.id + '" data-domain="' + hosting.domain + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                    }
                  }
                }
              }
            }
          }
          html += '</td>';
          html += '</tr>';
       })
       $('tbody').html(html);
       $('[data-toggle="tooltip"]').tooltip();
    }

    $(document).on('click', '.button-action-hosting', function () {
        $('tr').removeClass('action-row');
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var domain = $(this).attr('data-domain');
        $('#modal-service').modal('show');
        $('#button-service').fadeIn();
        switch (action) {
          case 'expired':
              $('#modal-service .modal-title').text('Yêu cầu gia hạn dịch vụ Hosting ' + domain);
              $.ajax({
                  url: '/service/request_expired_hosting',
                  data: {list_hosting: id},
                  dataType: 'json',
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('#button-service').attr('disabled', true);
                      $('#notication-service').html(html);
                  },
                  success: function (data) {
                      var html = '';
                      if (data.price_override != '') {
                          if (data.expire_billing_cycle == 1) {
                            html += '<div class="form-group">';
                            html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                            html += '<div class="mt-3 mb-3">';
                            html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                            html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                            html += '<option value="'+ data.price_override.billing_cycle +'">'+ data.price_override.text_billing_cycle +' / '+ data.price_override.total +'</option>';
                            html += '</select>';
                            html += '</div>';
                            html += '</div>';
                            $('#notication-service').html(html);
                            $('#button-service').attr('data-action', action);
                            $('#button-service').attr('data-type', 'hosting');
                            $('#button-service').attr('disabled', false);
                            $('.select_expired').select2();
                          } else {
                            $('#notication-invoice').html('<span class="text-center text-danger">Lỗi chọn VPS không đồng bộ thời gian. Quý khách vui lòng chọn VPS trùng với thời gian.</span>');
                            $('#button-service').attr('disabled', false);
                          }
                      } else {
                          html += '<div class="form-group">';
                          html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                          html += '<div class="mt-3 mb-3">';
                          html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                          html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                          if (data.total['monthly'] != '0 VNĐ') {
                            html += '<option value="monthly">1 Tháng / '+ data.total['monthly'] +'</option>';
                          }
                          if (data.total['twomonthly'] != '0 VNĐ') {
                            html += '<option value="twomonthly">2 Tháng / '+ data.total['twomonthly'] +'</option>';
                          }
                          if (data.total['quarterly'] != '0 VNĐ') {
                            html += '<option value="quarterly">3 Tháng / '+ data.total['quarterly'] +'</option>';
                          }
                          if (data.total['semi_annually'] != '0 VNĐ') {
                            html += '<option value="semi_annually">6 Tháng / '+ data.total['semi_annually'] +'</option>';
                          }
                          if (data.total['annually'] != '0 VNĐ') {
                            html += '<option value="annually">1 Năm / '+ data.total['annually'] +'</option>';
                          }
                          if (data.total['biennially'] != '0 VNĐ') {
                            html += '<option value="biennially">2 Năm / '+ data.total['biennially'] +'</option>';
                          }
                          if (data.total['triennially'] != '0 VNĐ') {
                            html += '<option value="triennially">3 Năm / '+ data.total['triennially'] +'</option>';
                          }
                          html += '</select>';
                          html += '</div>';
                          html += '</div>';
                          $('#notication-service').html(html);
                          $('#button-service').attr('data-action', action);
                          $('#button-service').attr('data-type', 'hosting');
                          $('#button-service').attr('disabled', false);
                          $('.select_expired').select2();
                      }
                  },
                  error: function (e) {
                    console.log(e);
                    $('#notication-service').html('<span class="text-center">Lỗi truy xuất dữ liệu khách hàng.</span>');
                    $('#button-service').attr('disabled', false);
                  },
              })
            break;
          case 'suspend':
              $('#modal-service .modal-title').text('Yêu cầu tắt dịch vụ Hosting');
              $('#notication-service').html('<span>Bạn có muốn tắt dịch vụ Hosting <b class="text-danger">(domain: '+ domain +')</b> này không ?</span>');
            break;
          case 'unsuspend':
              $('#modal-service .modal-title').text('Yêu cầu mở lại dịch vụ Hosting');
              $('#notication-service').html('<span>Bạn có muốn mở lại dịch vụ Hosting <b class="text-danger">(domain: '+ domain +')</b> này không ?</span>');
            break;
          case 'terminated':
              $('#modal-service .modal-title').text('Yêu cầu hủy dịch vụ Hosting');
              $('#notication-service').html('<span>Bạn có muốn hủy dịch vụ Hosting <b class="text-danger">(domain: '+ domain +')</b> này không ?</span>');
            break;
        }

        $('#button-service').attr('data-id', id);
        $('#button-service').attr('data-action', action);
        $('#button-service').attr('data-domain', domain);
        $('#button-service').attr('data-type', 'hosting');
        $(this).closest('tr').addClass('action-row');
        $('#button-service').fadeIn();
        $('#button-finish').fadeOut();
    });

    $('#button-service').on('click', function () {
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var billing_cycle = '';
        var type = $(this).attr('data-type');
        var token = $('meta[name="csrf-token"]').attr('content');
        if (type == 'hosting') {
          var domain = $(this).attr('data-domain');
        } else if (type == 'vps') {
          var ip = $(this).attr('data-ip');
        }
        if (action == 'expired') {
            billing_cycle = $('#select_billing_cycle').val();
        }
        $.ajax({
            url: '/services/action',
            type: 'post',
            dataType: 'json',
            data: {'_token': token, id: id, type: type, action: action, billing_cycle: billing_cycle},
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-service').attr('disabled', true);
                $('#notication-service').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data) {
                    if (data.error) {
                        if (data.error == 1) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái vps',
                                body: "Có 1 VPS chưa được tạo trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 2) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái vps',
                                body: "Có 1 VPS đang ở trạng thái bật trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 3) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái vps',
                                body: "Có 1 VPS đang ở trạng thái tắt trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                        else if (data.error == 4) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS do Admin off. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                        else if (data.error == 5) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS đang ở trạng thái hủy trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                         else if (data.error == 400) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái vps',
                                body: "Cập nhật trạng thái VPS thất bại. Vui lòng fresh lại website và thử lại",
                            })
                        } else if (data.error == 2223) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'mở lại hosting',
                                body: "Mở lại hosting thất bại. Hosting này đã bị Admin khóa lại, quý khách vui lòng liên hệ lại với chúng tôi.",
                            })
                        }
                        else if (data.error == 2224) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'mở lại hosting',
                                body: "Mở lại hosting thất bại. Hosting đã hủy, quý khách vui lòng liên hệ lại với chúng tôi.",
                            })
                        }
                        else if (data.error == 9998) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'Không thành công',
                                body: "Yêu cầu thất bại. Dịch vụ được chọn không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại dịch vụ được chọn.",
                            })
                        }
                        $('#modal-service').modal('hide');
                        $('#button-service').attr('disabled', false);
                    } else {
                        switch (type) {
                          case 'hosting':
                              if (action == 'expired') {
                                  $(document).Toasts('create', {
                                      class: 'bg-success',
                                      title: 'Hosting',
                                      subtitle: 'gia hạn',
                                      body: "Yêu cầu gia hạn hosting thành công",
                                  });
                                  $('.action-row .expired').fadeOut();
                                  $('#button-service').attr('disabled', false);
                                  html += '<span class="text-center">Yêu cầu gia hạn hosting thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/'+data+'">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                                  $('#notication-service').html(html);
                                  $('#button-service').fadeOut();
                                  $('#button-finish').fadeIn();
                                  $('tr').removeClass('action-row');
                              }
                              else if (action == 'suspend') {
                                  $(document).Toasts('create', {
                                      class: 'bg-warning',
                                      title: 'Hosting',
                                      subtitle: 'suspend',
                                      body: "Tắt hosting thành công",
                                  });
                                  $('.action-row .suspend').attr('disabled', true);
                                  $('.action-row .unsuspend').attr('disabled', false);
                                  $('.action-row .hosting-status').html('<span class="text-danger">Đã tắt</span>');
                                  $('#button-service').attr('disabled', false);
                                  $('#modal-service').modal('hide');
                                  $('tr').removeClass('action-row');
                              }
                              else if (action == 'unsuspend') {
                                  $(document).Toasts('create', {
                                      class: 'bg-info',
                                      title: 'Hosting',
                                      subtitle: 'unsuspend',
                                      body: "Mở lại hosting thành công",
                                  });
                                  $('.action-row .suspend').attr('disabled', false);
                                  $('.action-row .unsuspend').attr('disabled', true);
                                  $('.action-row .hosting-status').html('<span class="text-success">Đã bật</span>');
                                  $('#button-service').attr('disabled', false);
                                  $('#modal-service').modal('hide');
                                  $('tr').removeClass('action-row');
                              }
                              else if (action == 'terminated') {
                                  $(document).Toasts('create', {
                                      class: 'bg-danger',
                                      title: 'Hosting',
                                      subtitle: 'terminated',
                                      body: "Hủy dịch vụ hosting thành công",
                                  });
                                  $('.action-row .terminated').fadeOut();
                                  $('.action-row .hosting-status').html('<span class="text-danger">Đã hủy</span>');
                                  $('#button-service').attr('disabled', false);
                                  $('#modal-service').modal('hide');
                                  $('tr').removeClass('action-row');
                              }
                            break;
                          case 'vps':
                              if (action == 'expired') {
                                  $(document).Toasts('create', {
                                      class: 'bg-success',
                                      title: 'VPS',
                                      subtitle: 'gia hạn',
                                      body: "Yêu cầu gia hạn vps thành công",
                                  });
                                  $('.action-row-vps .expired').fadeOut();
                                  $('#button-service').attr('disabled', false);
                                  html += '<span class="text-center">Yêu cầu gia hạn VPS thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/'+data+'">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                                  $('#notication-service').html(html);
                                  $('#button-service').fadeOut();
                                  $('#button-finish').fadeIn();
                                  $('tr').removeClass('action-row-vps');
                              }
                              else if (action == 'off') {
                                  $(document).Toasts('create', {
                                      class: 'bg-success',
                                      title: 'VPS',
                                      subtitle: 'off',
                                      body: "Yêu cầu tắt VPS thành công",
                                  });
                                  $('.action-row-vps .off').attr('disabled', true);
                                  $('.action-row-vps .on').attr('disabled', false);
                                  $('.action-row-vps .vps-status').html('<span class="text-danger">Đã tắt</span>');
                                  $('#button-service').attr('disabled', false);
                                  $('#modal-service').modal('hide');
                                  $('tr').removeClass('action-row-vps');
                              }
                              else if (action == 'on') {
                                  $(document).Toasts('create', {
                                      class: 'bg-success',
                                      title: 'VPS',
                                      subtitle: 'on',
                                      body: "Yêu cầu bật VPS thành công",
                                  });
                                  $('.action-row-vps .off').attr('disabled', false);
                                  $('.action-row-vps .on').attr('disabled', true);
                                  $('.action-row-vps .vps-status').html('<span class="text-success">Đã bật</span>');
                                  $('#button-service').attr('disabled', false);
                                  $('#modal-service').modal('hide');
                                  $('tr').removeClass('action-row-vps');
                              }
                              else if (action == 'restart') {
                                  $(document).Toasts('create', {
                                      class: 'bg-success',
                                      title: 'VPS',
                                      subtitle: 'on',
                                      body: "Yêu cầu khởi động lại VPS thành công",
                                  });
                                  $('.action-row-vps .off').attr('disabled', false);
                                  $('.action-row-vps .on').attr('disabled', true);
                                  $('.action-row-vps .vps-status').html('<span class="text-success">Đang bật</span>');
                                  $('#button-service').attr('disabled', false);
                                  $('#modal-service').modal('hide');
                                  $('tr').removeClass('action-row-vps');
                                  setTimeout(function(){
                                      html = '<span class="text-danger">Đã bật</span>';
                                      $('.action-row-vps .vps-status').html(html);
                                  }, 40000);
                              }
                              else if (action == 'terminated') {
                                  $(document).Toasts('create', {
                                      class: 'bg-danger',
                                      title: 'VPS',
                                      subtitle: 'terminated',
                                      body: "Hủy dịch vụ VPS thành công",
                                  });
                                  $('.action-row-vps .terminated').fadeOut();
                                  $('.action-row-vps .vps-status').html('<span class="text-danger">Đã hủy</span>');
                                  $('#button-service').attr('disabled', false);
                                  $('#modal-service').modal('hide');
                                  $('tr').removeClass('action-row-vps');
                              }
                            break;
                        }
                        // $('#modal-service').modal('hide');
                        // $('#button-service').attr('disabled', false);
                    }
                } else {
                  $('#notication-service').html('<span class="text-danger">Hành động truy vấn đến quản lý dịch vụ thất bại!</span>');
                  $('#button-service').attr('disabled', false);
                }
            },
            error: function (e) {
                console.log(e);
                $('#notication-service').html('<span class="text-danger">Truy vấn đến quản lý dịch vụ lỗi. Quý khách vui lòng liên hệ với chúng tôi để được giúp đỡ.</span>');
                $('#button-service').attr('disabled', false);
            }
        });
    });

})
