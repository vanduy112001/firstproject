$(document).ready(function () {

    $('.delete-server-hosting').on('click', function () {
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        $('#button_invoice').fadeIn();
        $('#button_finish').fadeOut();
        $(this).closest('tr').addClass('choose-row');
        var html = '';

        $('#delete_invoice').modal('show');
        $('.modal-title').text('Xóa Server Hosting');
        html += '<span>Bạn có muốn xóa Server Hosting <b class="text-danger">' + name + '</b> này không?</span>';
        $('#notication-invoice').html(html);
        $('#button_invoice').attr('data-id', id);
        $('#button_invoice').attr('data-type', 'delete');
    })

    $('#button_invoice').on('click', function () {
        var type = $(this).attr('data-type');
        if (type == 'delete') {
            var id = $(this).attr('data-id');
            var token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
                url: '/admin/server_hosting/delete',
                type: 'post',
                dataType: 'json',
                data: {'_token': token ,id: id},
                beforeSend: function(){
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#button_invoice').attr('disabled', true);
                    $('#notication-invoice').html(html);
                },
                success: function (data) {
                    if (data != '') {
                        $('.choose-row').fadeOut();
                        $('#notication-invoice').html('<span class="text-success">Xóa Server Hosting thành công</span>');
                    } else {
                        $('#notication-invoice').html('<span class="text-danger">Xóa Server Hosting thất bại</span>');
                    }
                    $('#button_invoice').attr('disabled', false);
                    $('#button_invoice').fadeOut();
                    $('#button_finish').fadeIn();
                },
                error: function (e) {
                    console.log(e);
                    $('#button_invoice').attr('disabled', false);
                    $('#notication-invoice').html('<span class="text-danger">Lỗi truy vấn Server Hosting!</span>');
                }
            })
        }
        else {
            $('.choose-form').submit();
        }
    })

    $('.active-server-hosting').on('click', function (event) {
        event.preventDefault();
        $(this).closest('form').addClass('choose-form');
        var name = $(this).attr('data-name');
        $('#button_invoice').fadeIn();
        $('#button_finish').fadeOut();
        var html = '';

        $('#delete_invoice').modal('show');
        $('.modal-title').text('Kích hoạt Server Hosting');
        html += '<span>Bạn có muốn kích hoạt Server Hosting <b class="text-danger">' + name + '</b> này không?</span>';
        $('#notication-invoice').html(html);
        $('#button_invoice').attr('data-type', 'active');
    })

    $('.deactive-server-hosting').on('click', function (event) {
        event.preventDefault();
        $(this).closest('form').addClass('choose-form');
        var name = $(this).attr('data-name');
        $('#button_invoice').fadeIn();
        $('#button_finish').fadeOut();
        var html = '';

        $('#delete_invoice').modal('show');
        $('.modal-title').text('Kích hoạt Server Hosting');
        html += '<span>Bạn có muốn tắt kích hoạt Server Hosting <b class="text-danger">' + name + '</b> này không?</span>';
        $('#notication-invoice').html(html);
        $('#button_invoice').attr('data-type', 'active');
    })

});
