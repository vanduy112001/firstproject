$(document).ready(function() {
  $('[data-toggle="tooltip"]').tooltip();
  // khởi tạo modal Toast
  const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
  });

  $(document).on('click', '.button-action-hosting' , function () {
      $('tr').removeClass('choose-row');
      $('#button-finish-hosting').fadeOut();
      var action = $(this).attr('data-type');
      var id = $(this).attr('data-id');
      var domain = $(this).attr("data-domain");
      $(this).closest('tr').addClass('choose-row');
      $('#delete-vps').modal('show');
      switch (action) {
        case 'create':
            $('.modal-title').text('Tạo Hosting');
            $('#notication-invoice').html('Bạn có muốn tạo Hosting (<b class="text-danger">ID:' + id + ' - Domain: ' + domain + ' )</b> này không?');
            $('#button-submit-hosting').attr('data-id', id);
            $('#button-submit-hosting').attr('data-action', action);
          break;

        case 'suspend':
            $('.modal-title').text('Suspend Hosting');
            $('#notication-invoice').html('Bạn có muốn suspend Hosting (<b class="text-danger">ID:' + id + ' - Domain: ' + domain + ' )</b> này không?');
            $('#button-submit-hosting').attr('data-id', id);
            $('#button-submit-hosting').attr('data-action', action);
          break;

        case 'unsuspend':
            $('.modal-title').text('Unsuspend Hosting');
            $('#notication-invoice').html('Bạn có muốn Unsuspend Hosting (<b class="text-danger">ID:' + id + ' - Domain: ' + domain + ' )</b> này không?');
            $('#button-submit-hosting').attr('data-id', id);
            $('#button-submit-hosting').attr('data-action', action);
          break;

        case 'delete':
            $('.modal-title').text('Xóa Hosting');
            var html = '';
            html += 'Bạn có muốn xóa Hosting (<b class="text-danger">ID:' + id + ' - Domain: ' + domain + ' )</b> này không? <br>';
            html += '<span class="text-danger">Lưu ý: Khi bấm vào xác nhận sẽ xóa hosting này trên Porttal và trên Direct Admin Porttal. Vui lòng kiểm tra cẩn thận trước khi xóa.</span>';
            $('#notication-invoice').html(html);
            $('#button-submit-hosting').attr('data-id', id);
            $('#button-submit-hosting').attr('data-action', action);
          break;

      }
  });

  $('#button-submit-hosting').on('click', function () {
      var id = $(this).attr('data-id');
      var action = $(this).attr('data-action');
      var token = $('meta[name="csrf-token"]').attr('content');

      $.ajax({
        url: '/admin/hostings/action_hosting',
        type: 'post',
        dataType: 'json',
        data: { '_token': token, id: id, type: action },
        beforeSend: function(){
            var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
            $('#button-submit-hosting').attr('disabled', true);
            $('#notication-invoice').html(html);
        },
        success: function (data) {
            // console.log(data);
            var html = '';
            $('#button-order').fadeOut();
            $('#button-finish').fadeIn();
            if (data) {
                // console.log(data);
                switch (data) {
                  case 'admin-off':
                      $(document).Toasts('create', {
                          class: 'bg-danger',
                          title: 'Action Hosting',
                          subtitle: 'Suspend hosting',
                          body: "Suspend hosting thất bại. Hosting được chọn đã bị Admin off, vui lòng kiểm tra lại.",
                      })
                      $('#delete-vps').modal('hide');
                    break;
                  case 'admin-on':
                      $(document).Toasts('create', {
                          class: 'bg-danger',
                          title: 'Action Hosting',
                          subtitle: 'Suspend hosting',
                          body: "UnSuspend hosting thất bại. Hosting được chọn đang Suspend, vui lòng kiểm tra lại.",
                      })
                      $('#delete-vps').modal('hide');
                    break;
                  case 'create':
                      $(document).Toasts('create', {
                          class: 'bg-success',
                          title: 'Action hosting',
                          subtitle: 'hosting',
                          body: "Tạo Hosting thành công",
                      })
                      var date = new Date();
                      var data = date.getDate() + '-' + (date.getMonth()+1) + '-' + date.getFullYear();
                      $('.choose-row .data-create').text(data);
                      $('.choose-row .hosting-status').html('<span class="text-success">Đã tạo</span>');

                      var button_action = $('.button-action-services .button-action-hosting');
                      for (var i = 0; i < button_action.length; i++) {
                          var strt = '.button-action-services .button-action-hosting:nth-child('+i+')';
                          var type = $(strt).attr('data-type');
                          // console.log(type);
                          if (type == 'create') {
                              $(strt).attr('disabled', true);
                          }
                      }
                        $('#delete-vps').modal('hide');
                    break;
                  case 'unsuspend':
                    // console.log('da toi on');
                      $(document).Toasts('create', {
                          class: 'bg-info',
                          title: 'Action hosting',
                          subtitle: 'unsuspend hosting',
                          body: "UnSuspend Hosting thành công",
                      })
                      $('.choose-row .hosting-status').html('<span class="text-success">Đã bật</span>');
                      var button_action = $('.button-action-services .button-action-hosting');
                      // console.log(button_action);
                      for (var i = 0; i < button_action.length; i++) {
                          var strt = '.button-action-services .button-action-hosting:nth-child('+i+')';
                          var type = $(strt).attr('data-type');
                          // console.log(type);
                          if (type == 'unsuspend') {
                              $(strt).attr('disabled', true);
                          }
                          if (type == 'suspend') {
                              $(strt).attr('disabled', false);
                          }
                      }
                        $('#delete-vps').modal('hide');
                    break;
                  case 'suspend':
                  // ('da toi off');
                      $(document).Toasts('create', {
                          class: 'bg-warning',
                          title: 'Action hosting',
                          subtitle: 'suspend hosting',
                          body: "Suspend Hosting thành công",
                      })
                      // ($('a.button-action-hosting'));
                      $('.choose-row .hosting-status').html('<span class="text-success">Đã tắt</span>');
                      var button_action = $('.button-action-services .button-action-hosting');
                      // (button_action);
                      for (var i = 0; i < button_action.length; i++) {
                          var strt = '.button-action-services .button-action-hosting:nth-child('+i+')';
                          var type = $(strt).attr('data-type');
                          // (type);
                          if (type == 'suspend') {
                              $(strt).attr('disabled', true);
                          }
                          if (type == 'unsuspend') {
                              $(strt).attr('disabled', false);
                          }
                      }
                        $('#delete-vps').modal('hide');
                    break;
                  case 'delete':
                      $(document).Toasts('create', {
                          class: 'bg-danger',
                          title: 'Action hosting',
                          subtitle: 'delete hosting',
                          body: "Xóa Hosting thành công.",
                      })
                      $('.choose-row').fadeOut();
                      $('#delete-vps').modal('hide');
                    break;
                }
            } else {
                $(document).Toasts('create', {
                    class: 'bg-danger',
                    title: 'Action hosting',
                    subtitle: 'action warning',
                    body: "Thao tác với Hosting thất bại",
                })
                  $('#delete-vps').modal('hide');
                  $('#delete-vps').modal('hide');
            }
            $('#notication-invoice').html(html);
            $('#button-submit-hosting').attr('disabled', false);
        },
        error: function (e) {
            console.log(e);
            $('#notication-invoice').html('<span class="text-danger">Truy vấn dịch vụ Hosting lỗi!</span>');
            $('#button-submit-hosting').attr('disabled', false);
        }


      });

  });
  /*
  * Action nhiều hosting
  */
  $(document).on('click', '.hosting_checkbox' , function () {
      if ( $(this).is(':checked') ) {
          $(this).closest('tr').addClass('choose-multiple-hosting');
      } else {
          $(this).closest('tr').removeClass('choose-multiple-hosting');
      }

      var checked = $('.hosting_checkbox:checked');
      if(checked.length > 0) {
        $('.button-header').css('opacity', 0.9);
      } else {
        $('.button-header').css('opacity', 0);
      }
  });

  hosting_checkbox();

  function hosting_checkbox() {
    // var top = $('html, body').offset().top;
    var $vpsCheckbox = $('.hosting_checkbox');
    var lastChecked = null;
    $vpsCheckbox.click(function (e) {
        if (!lastChecked) {
            lastChecked = this;
            return;
        }
        if ( e.shiftKey ) {
            var start = $vpsCheckbox.index(this);
            var end = $vpsCheckbox.index(lastChecked);
            $vpsCheckbox.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
            $.each( $vpsCheckbox , function (index , value) {
                if( $(this).is(':checked') ) {
                    $(this).closest('tr').addClass('choose-multiple-hosting');
                } else {
                    $(this).closest('tr').removeClass('choose-multiple-hosting');
                }
            })
        }
        lastChecked = this;
    })

    var checked = $('.hosting_checkbox:checked');
    if(checked.length > 0) {
      $('.button-header').css('opacity', 0.9);
    } else {
      $('.button-header').css('opacity', 0);
    }

  }

  $('.button-multiple-action-hostings').on('click', function () {
      var action = $(this).attr('data-action');
      var checked = $('.hosting_checkbox:checked');
      // (checked);
      if (checked.length > 0) {
        $('#delete-multiple-hosting').modal('show');
        $('#button-submit-multi-hosting').fadeIn();
        $('#button-finish-multi-hosting').fadeOut();
        switch (action) {
          case 'create':
              $('#delete-multiple-hosting .modal-title').text('Tạo nhiều Hosting');
              $('#notication-hostings').html('<span class="text-danger">Bạn có muốn tạo các hosting vừa chọn không?</span>');
              $('#button-submit-multi-hosting').attr('data-action', action);
            break;

          case 'suspend':
              $('#delete-multiple-hosting .modal-title').text('Suspend nhiều Hosting');
              $('#notication-hostings').html('<span class="text-danger">Bạn có muốn suspend các hosting vừa chọn không?</span>');
              $('#button-submit-multi-hosting').attr('data-action', action);
            break;

          case 'unsuspend':
              $('#delete-multiple-hosting .modal-title').text('Unsuspend nhiều Hosting');
              $('#notication-hostings').html('<span class="text-danger">Bạn có muốn unsuspend các hosting vừa chọn không?</span>');
              $('#button-submit-multi-hosting').attr('data-action', action);
            break;

          case 'cancel':
              $('#delete-multiple-hosting .modal-title').text('Hủy dịch vụ nhiều Hosting');
              $('#notication-hostings').html('<span class="text-danger">Bạn có muốn hủy dịch vụ các hosting vừa chọn không?</span>');
              $('#button-submit-multi-hosting').attr('data-action', action);
            break;

          case 'delete':
              var html = '';
              $('#delete-multiple-hosting .modal-title').text('Xóa nhiều Hosting');
              html += '<span class="text-danger">Bạn có muốn xóa các hosting vừa chọn không?</span> <br>';
              html += '<span class="text-danger">Lưu ý: Khi bấm vào xác nhận sẽ xóa hosting này trên Porttal và trên Direct Admin Porttal. Vui lòng kiểm tra cẩn thận trước khi xóa.</span>';

              $('#notication-hostings').html(html);
              $('#button-submit-multi-hosting').attr('data-action', action);
            break;

        }
      } else {
        alert('Xin vui lòng chọn ít nhất một Hosting');
      }
  });
  //
  $(".hosting_checkbox").removeAttr('checked');
  $(".hosting_checkbox").attr('checked', false);


  $('#button-submit-multi-hosting').on('click', function () {
      var action = $(this).attr('data-action');
      var checked = $('.hosting_checkbox:checked');
      var token = $('meta[name="csrf-token"]').attr('content');
      var id_hostings = [];
      $.each(checked, function(index, value) {
          id_hostings.push($(this).val());
      });
      // ('da den');
      $.ajax({
          url: '/admin/hostings/multiple_action_hostings',
          type: 'post',
          dataType: 'json',
          data: {'_token': token, id_hostings: id_hostings, action: action },
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#button-submit-multi-hosting').attr('disabled', true);
              $('#notication-hostings').html(html);
          },
          success: function (data) {
              var html = '';
              // console.log(data);
              if (data) {
                switch (data) {
                    case 'admin-off':
                        $(document).Toasts('create', {
                            class: 'bg-danger',
                            title: 'Action Hosting',
                            subtitle: 'Suspend hosting',
                            body: "Suspend hosting thất bại. Có một số hosting được chọn đã bị Admin off, vui lòng kiểm tra lại.",
                        })
                        $('#delete-multiple-hosting').modal('hide');
                      break;
                    case 'create':
                        $(document).Toasts('create', {
                            class: 'bg-success',
                            title: 'Action Hosting',
                            subtitle: 'Tạo hosting',
                            body: "Tạo các hosting được chọn thành công",
                        })
                        var date = new Date();
                        var data = date.getDate() + '-' + (date.getMonth()+1) + '-' + date.getFullYear();
                        $('.choose-multiple-hosting .data-create').text(data);
                        $('.choose-multiple-hosting .hosting-status').html('<span class="text-success">Đã tạo</span>');

                        var button_action = $('.choose-multiple-hosting .button-action-services .btn');
                        for (var i = 0; i < button_action.length; i++) {
                            var n = i + 1;
                            var strt = '.choose-multiple-hosting .button-action-services .btn:nth-child('+n+')';
                            var type = $(strt).attr('data-type');
                            // (type);
                            if (type == 'create') {
                                $(strt).attr('disabled', true);
                            }
                        }
                        $('#delete-multiple-hosting').modal('hide');
                      break;
                    case 'suspend':
                        $(document).Toasts('create', {
                            class: 'bg-warning',
                            title: 'Action Hosting',
                            subtitle: 'Suspend hosting',
                            body: "Suspend các hosting được chọn thành công",
                        })
                        var button_action = $('.choose-multiple-hosting .button-action-services .btn');
                        // (button_action);
                        $('.choose-multiple-hosting .hosting-status').html('<span class="text-danger">Đã tắt</span>');
                        for (var i = 0; i < button_action.length; i++) {
                            var n = i + 1;
                            var strt = '.choose-multiple-hosting .button-action-services .btn:nth-child('+n+')';
                            var type = $(strt).attr('data-type');
                            // (type);
                            if (type == 'suspend') {
                                $(strt).attr('disabled', true);
                            }
                            if (type == 'unsuspend') {
                                $(strt).attr('disabled', false);
                            }
                        }
                        $('#delete-multiple-hosting').modal('hide');
                      break;
                    case 'unsuspend':
                        $(document).Toasts('create', {
                            class: 'bg-info',
                            title: 'Action Hosting',
                            subtitle: 'Unsuspend hosting',
                            body: "Unsuspend các hosting được chọn thành công",
                        })
                        $('.choose-multiple-hosting .hosting-status').html('<span class="text-success">Đã bật</span>');
                        var button_action = $('.choose-multiple-hosting .button-action-services .btn');
                        // (button_action);
                        for (var i = 0; i < button_action.length; i++) {
                            var n = i + 1;
                            var strt = '.choose-multiple-hosting .button-action-services .btn:nth-child('+n+')';
                            var type = $(strt).attr('data-type');
                            // console.log(strt, type);
                            if (type == 'unsuspend') {
                                $(strt).attr('disabled', true);
                            }
                            if (type == 'suspend') {
                                $(strt).attr('disabled', false);
                            }
                        }
                        $('#delete-multiple-hosting').modal('hide');
                      break;
                    case 'cancel':
                        html += '<span class="text-success">Hủy các hosting được chọn thành công</span>';
                        $('#notication-hostings').html(html);
                        $('.choose-multiple-hosting .hosting-status').html('<span class="text-danger">Đã hủy</span>');
                        $('#button-submit-multi-hosting').fadeOut();
                        $('#button-finish-multi-hosting').fadeIn();
                        $('.hosting_checkbox').attr('checked', false);
                        $('tr').removeClass('choose-multiple-hosting');
                      break;
                    case 'delete':
                        $('.choose-multiple-hosting').fadeOut();
                        if(checked.length > 0) {
                          $('.button-header').css('opacity', 0.9);
                        } else {
                          $('.button-header').css('opacity', 0);
                        }
                        html += '<span class="text-success">Xóa các hosting được chọn thành công</span>';
                        $('#notication-hostings').html(html);
                        $('#button-submit-multi-hosting').fadeOut();
                        $('#button-finish-multi-hosting').fadeIn();
                      break;

                }
              } else {
                  $(document).Toasts('create', {
                      class: 'bg-danger',
                      title: 'Action Hosting',
                      subtitle: 'hosting',
                      body: "Thao tác với dịch vụ Hosting thất bại!",
                  })
                  $('#delete-multiple-hosting').modal('hide');
              }
              $('#button-submit-multi-hosting').attr('disabled', false);
              $('#notication-hostings').html(html);
          },
          error: function (e) {
              console.log(e);
              $('#button-submit-multi-hosting').attr('disabled', false);
              $('#notication-hostings').html('<span class="text-danger">Truy vấn dịch vụ Hosting lỗi!</span>');
          }

      });

  });

  $('#select_status_hosting').on('change', function () {
      var status = $(this).val();
      var q = $('#search').val();
      var sort = $('.sort_type').val();
      // console.log(status , q);
      $('#type_user_personal').val('select_status_vps');
      $.ajax({
          url: '/admin/hostings/select_status_hosting',
          data: { status: status, q: q,sort: sort },
          dataType: 'json',
          beforeSend: function(){
              var html = '<td  colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              // console.log(data);
              var html = '';
              if (data.data != '') {
                  screen_vps(data);
              } else {
                  html = '<td  colspan="10" class="text-center text-danger">Không có Hosting trong dữ liệu</td>';
                  $('tbody').html(html);
              }
          },
          error: function (e) {
              console.log(e);
              var html = '<td  colspan="10" class="text-center text-danger">Truy vấn Hosting lỗi!</td>';
              $('tbody').html(html);
          }
      });
  });

  $('#search').on('keyup', function () {
      var q = $(this).val();
      var status = $('#select_status_hosting').val();
      var sort = $('.sort_type').val();
      $('#type_user_personal').val('search_hosting');
      $.ajax({
          url: '/admin/hostings/search',
          data: { status: status, q: q, sort:sort },
          dataType: 'json',
          beforeSend: function(){
              var html = '<td  colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              // console.log(data);
              var html = '';
              if (data.data != '') {
                  screen_vps(data);
              } else {
                  html = '<td  colspan="9" class="text-center text-danger">Không có dữ liệu Hosting</td>';
                  $('tbody').html(html);
              }
          },
          error: function (e) {
              console.log(e);
              var html = '<td  colspan="9" class="text-center text-danger">Truy vấn Hosting lỗi!</td>';
              $('tbody').html(html);
          }
      });
  })

  $('#search_user').on('keyup', function () {
      var q = $(this).val();
      $.ajax({
          url: '/admin/hostings/search_user',
          data: { q: q },
          dataType: 'json',
          beforeSend: function(){
              var html = '<td  colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              // console.log(data);
              var html = '';
              if (data != '') {
                  screen_vps(data);
              } else {
                  html = '<td  colspan="9" class="text-center text-danger">Không có dữ liệu Hosting</td>';
                  $('tbody').html(html);
              }
          },
          error: function (e) {
              console.log(e);
              var html = '<td  colspan="9" class="text-center text-danger">Truy vấn Hosting lỗi!</td>';
              $('tbody').html(html);
          }
      });
  })

  $(document).on('click', '#hosting_type_user', function () {
    $('#type_user_personal').val('personal');
    $.ajax({
       url: '/admin/hostings/list_hosting_of_personal',
       dataType: 'json',
       beforeSend: function(){
           var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
           $('tbody').html(html);
       },
       success: function (data) {
            // console.log(data);
           if (data.data.length > 0) {
               screen_list_vps(data);
           } else {
               var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
               $('tbody').html(html);
               $('tfoot').html('');
           }
       },
       error: function (e) {
            console.log(e);
           var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
           $('tbody').html(html);
       }
    })
  })

  $(document).on('click', '#hosting_type_enterprise', function () {
    $('#type_user_personal').val('enterprise');
    $.ajax({
       url: '/admin/hostings/list_hosting_of_enterprise',
       dataType: 'json',
       beforeSend: function(){
           var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
           $('tbody').html(html);
       },
       success: function (data) {
            // console.log(data);
           if (data.data.length > 0) {
               screen_list_vps(data);
           } else {
               var html = '<td colspan="9" class="text-center">Không có thanh toán của loại khách hàng này trong dữ liệu!</td>';
               $('tbody').html(html);
               $('tfoot').html('');
           }
       },
       error: function (e) {
          console.log(e);
           var html = '<td colspan="9" class="text-center">Truy vấn thanh toán lỗi!</td>';
           $('tbody').html(html);
       }
    })
  })

  function screen_list_vps(data) {
      var html = '';
      $.each(data.data, function (index , hosting) {
          html += '<tr>';
          html += '<td><input type="checkbox" value="'+ hosting.id +'" class="hosting_checkbox"></td>';
          html += '<td><a href="/admin/users/detail/'+ hosting.user_hosting.id +'">'+ hosting.user_hosting.name +'</a></td>';
          // IP VPS
          if (hosting.domain) {
              html += '<td><a href="/admin/hostings/detail/' + hosting.id + '">'+ hosting.domain +'</a></td>';
          } else {
              html += '<td class="text-danger">Chưa tạo</td>';
          }
          // cấu hình
          if (hosting.private) {
              html += '<td><a href="/admin/products/edit/'+ hosting.id_product +'">'+ hosting.name_product +'</a></td>';
          } else {
              html += '<td><a href="/admin/product_privates/edit/'+ hosting.id_product +'">'+ hosting.name_product +'</a></td>';
          }
          // Ngày tạo
          if (hosting.date_create) {
              var date = new Date(hosting.date_create);
              var day = date.getDate();
              if (day < 10) {
                day = '0' + day;
              }
              var month = date.getMonth() + 1;
              if (month < 10) {
                month = '0' + month;
              }

              html += '<td>';
              html += day + '-' + month + '-' + date.getFullYear();
              html += '</td>';
          } else {
              html += '<td class="text-danger">Chưa tạo</td>';
          }
          // Ngày kết thúc
          if (hosting.next_due_date) {
              var date2 = new Date(hosting.next_due_date);
              var day2 = date2.getDate();
              if (day2 < 10) {
                day2 = '0' + day2;
              }
              var month2 = date2.getMonth() + 1;
              if (month2 < 10) {
                month2 = '0' + month2;
              }
              html += '<td>';
              html += day2 + '-' + month2 + '-' + date2.getFullYear();
              html += '</td>';
          } else {
              html += '<td class="text-danger">Chưa tạo</td>';
          }
          if ( hosting.price_override != null ) {
              html += '<td class="text-danger"><b>' + addCommas(hosting.price_hosting) + ' VNĐ</b></td>';
          }
          else {
              html += '<td>' + addCommas(hosting.price_hosting) + ' VNĐ</td>';
          }

          // Trạng thái
          if (hosting.status == 'Active') {
              html += '<td class="text-success">Đã tạo</td>';
          }
          else if(hosting.status == 'Pending') {
              html += '<td class="text-danger">Chưa tạo</td>';
          }
          else if(hosting.status == 'progressing') {
              html += '<td class="text-info">Đang tạo</td>';
          }
          else if(hosting.status == 'rebuild') {
              html += '<td class="text-info">Đang cài lại</td>';
          }
          else if(hosting.status == 'change_ip') {
              html += '<td class="text-info">Đang đổi IP</td>';
          }
          else if(hosting.status == 'expire') {
              html += '<td class="text-info">Đã hết hạn</td>';
          }
          else {
              html += '<td class="text-secondary">Chưa tạo</td>';
          }
          // hành động
          html += '<td class="button-action">';
          html += '<a href="/admin/hostings/detail/'+ hosting.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>'
          html += '<button class="btn btn-outline-warning button-action-hosting mr-1" data-type="suspend" data-id="'+ hosting.id +'" data-domain="'+ hosting.domain +'" data-toggle="tooltip" title="Suspend Hosting"><i class="fas fa-power-off"></i></button>';
          html += '<button class="btn btn-outline-info button-action-hosting mr-1" data-type="unsuspend" data-id="'+ hosting.id +'" data-domain="'+ hosting.domain +'" data-toggle="tooltip" title="Unsuspend Hosting"><i class="fas fa-toggle-on"></i></button>';
          html += '<button class="btn btn-danger button-action-hosting" data-type="delete" data-id="'+ hosting.id +'" data-domain="'+ hosting.domain +'" data-toggle="tooltip" title="Xóa Hosting"><i class="fas fa-trash-alt"></i></button>';
          html += '</td>';
          html += '</tr>';
      })
      $('tbody').html(html);
      // phân trang cho vps
      var total = data.total;
      var per_page = data.perPage;
      var current_page = data.current_page;
      var html_page = '';
      if (total > per_page) {
          var page = total/per_page + 1;
          html_page += '<td colspan="9" class="text-center link-right">';
          html_page += '<nav>';
          html_page += '<ul class="pagination pagination_list_hosting">';
          if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
          } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
          }
          for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                  active = 'active';
              }
              if (active == 'active') {
                html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

          }
          if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
          } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
          }
          html_page += '</ul>';
          html_page += '<nav>';
          html_page += '</td>';
      }
      $('tfoot').html(html_page);
      var html_page_top = '';
      if (total > per_page) {
          var page = total/per_page + 1;
          html_page_top += '<nav>';
          html_page_top += '<ul class="pagination pagination_list_hosting">';
          if (current_page != 1) {
              html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
          } else {
              html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
          }
          for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                  active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

          }
          if (current_page != page.toPrecision(1)) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
          } else {
              html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
          }
          html_page_top += '</ul>';
          html_page_top += '<nav>';
      }
      $('.paginate_top').html(html_page_top);
  }

  function screen_vps(data) {
      var html = '';
      $.each(data.data, function (index , hosting) {
          html += '<tr>';
          html += '<td><input type="checkbox" value="'+ hosting.id +'" class="hosting_checkbox"></td>';
          html += '<td><a href="/admin/users/detail/'+ hosting.user_hosting.id +'">'+ hosting.user_hosting.name +'</a></td>';
          // IP VPS
          if (hosting.domain) {
              html += '<td><a href="/admin/hostings/detail/' + hosting.id + '">'+ hosting.domain +'</a></td>';
          } else {
              html += '<td class="text-danger">Chưa tạo</td>';
          }
          // cấu hình
          if (hosting.private) {
              html += '<td><a href="/admin/products/edit/'+ hosting.id_product +'">'+ hosting.name_product +'</a></td>';
          } else {
              html += '<td><a href="/admin/product_privates/edit/'+ hosting.id_product +'">'+ hosting.name_product +'</a></td>';
          }
          // Ngày tạo
          if (hosting.date_create) {
              var date = new Date(hosting.date_create);
              var day = date.getDate();
              if (day < 10) {
                day = '0' + day;
              }
              var month = date.getMonth() + 1;
              if (month < 10) {
                month = '0' + month;
              }

              html += '<td>';
              html += day + '-' + month + '-' + date.getFullYear();
              html += '</td>';
          } else {
              html += '<td class="text-danger">Chưa tạo</td>';
          }
          // Ngày kết thúc
          if (hosting.next_due_date) {
              var date2 = new Date(hosting.next_due_date);
              var day2 = date2.getDate();
              if (day2 < 10) {
                day2 = '0' + day2;
              }
              var month2 = date2.getMonth() + 1;
              if (month2 < 10) {
                month2 = '0' + month2;
              }
              html += '<td>';
              html += day2 + '-' + month2 + '-' + date2.getFullYear();
              html += '</td>';
          } else {
              html += '<td class="text-danger">Chưa tạo</td>';
          }
          if ( hosting.price_override != null ) {
              html += '<td class="text-danger"><b>' + addCommas(hosting.price_hosting) + ' VNĐ</b></td>';
          }
          else {
              html += '<td>' + addCommas(hosting.price_hosting) + ' VNĐ</td>';
          }

          // Trạng thái
          if (hosting.status == 'Active') {
              html += '<td class="text-success">Đã tạo</td>';
          }
          else if(hosting.status == 'Pending') {
              html += '<td class="text-danger">Chưa tạo</td>';
          }
          else if(hosting.status == 'progressing') {
              html += '<td class="text-info">Đang tạo</td>';
          }
          else if(hosting.status == 'rebuild') {
              html += '<td class="text-info">Đang cài lại</td>';
          }
          else if(hosting.status == 'change_ip') {
              html += '<td class="text-info">Đang đổi IP</td>';
          }
          else if(hosting.status == 'expire') {
              html += '<td class="text-info">Đã hết hạn</td>';
          }
          else {
              html += '<td class="text-secondary">Chưa tạo</td>';
          }
          // hành động
          html += '<td class="button-action">';
          html += '<a href="/admin/hostings/detail/'+ hosting.id +'" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>'
          html += '<button class="btn btn-outline-warning button-action-hosting mr-1" data-type="suspend" data-id="'+ hosting.id +'" data-domain="'+ hosting.domain +'" data-toggle="tooltip" title="Suspend Hosting"><i class="fas fa-power-off"></i></button>';
          html += '<button class="btn btn-outline-info button-action-hosting mr-1" data-type="unsuspend" data-id="'+ hosting.id +'" data-domain="'+ hosting.domain +'" data-toggle="tooltip" title="Unsuspend Hosting"><i class="fas fa-toggle-on"></i></button>';
          html += '<button class="btn btn-danger button-action-hosting" data-type="delete" data-id="'+ hosting.id +'" data-domain="'+ hosting.domain +'" data-toggle="tooltip" title="Xóa Hosting"><i class="fas fa-trash-alt"></i></button>';
          html += '</td>';
          html += '</tr>';
      })
      $('tbody').html(html);
      // phân trang cho vps
      var total = data.total;
      var per_page = data.perPage;
      var current_page = data.current_page;
      var html_page = '';
      if (total > per_page) {
        if ( total / per_page > 11 ) {
          var page = parseInt(total/per_page + 1);
          html_page += '<td colspan="10" class="text-center link-right">';
          html_page += '<nav>';
          html_page += '<ul class="pagination pagination_list_hosting">';
          if (current_page != 1) {
            html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
          } else {
            html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
          }
          if (current_page < 7) {
            for (var i = 1; i < 9; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
            for (var i = page - 1; i <= page; i++) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
            }
          }
          else if (current_page >= 7 || current_page <= page - 7) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = current_page - 3; i <= current_page +3; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
          }
          else if (current_page >= page - 6) {
            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
            html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
            for (var i = page - 6; i < page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
          }

          if (current_page != page.toPrecision(1)) {
            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
          } else {
            html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
          }
          html_page += '</ul>';
          html_page += '<nav>';
          html_page += '</td>';
        } else {
          var page = total/per_page + 1;
          html_page += '<td colspan="10" class="text-center link-right">';
          html_page += '<nav>';
          html_page += '<ul class="pagination pagination_list_hosting">';
          if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
          } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
          }
          for (var i = 1; i < page; i++) {
              var active = '';
              if (i == current_page) {
                  active = 'active';
              }
              if (active == 'active') {
                html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }

          }
          if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
          } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
          }
          html_page += '</ul>';
          html_page += '<nav>';
          html_page += '</td>';
        }
      }
      $('tfoot').html(html_page);
      var html_page_top = '';
      if (total > per_page) {
        if ( total / per_page > 11 ) {
          var page = parseInt(total/per_page + 1);
          html_page_top += '<nav>';
          html_page_top += '<ul class="pagination pagination_list_hosting">';
          if (current_page != 1) {
            html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
          } else {
            html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
          }
          if (current_page < 7) {
            for (var i = 1; i < 9; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
            for (var i = page - 1; i <= page; i++) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
            }
          }
          else if (current_page >= 7 && current_page <= page - 6) {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = current_page - 3; i <= current_page + 3; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
          }
          else if (current_page > page - 6) {
            html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
            html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
            html_page_top += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
            for (var i = page - 6; i <= page; i++) {
              var active = '';
              if (i == current_page) {
                active = 'active';
              }
              if (active == 'active') {
                html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              } else {
                html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
          }

          if (current_page != page.toPrecision(1)) {
            html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
          } else {
            html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
          }
          html_page += '</ul>';
          html_page += '<nav>';
        } else {
          var page = total/per_page + 1;
          html_page_top += '<nav>';
          html_page_top += '<ul class="pagination pagination_list_hosting">';
          if (current_page != 1) {
            html_page_top += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
          } else {
            html_page_top += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
          }
          for (var i = 1; i < page; i++) {
            var active = '';
            if (i == current_page) {
              active = 'active';
            }
            if (active == 'active') {
              html_page_top += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
            } else {
              html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
            }

          }
          if (current_page != page.toPrecision(1)) {
            html_page_top += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
          } else {
            html_page_top += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
          }
          html_page_top += '</ul>';
          html_page_top += '<nav>';
        }
      }
      $('.paginate_top').html(html_page_top);
  }

  $(document).on('click', '.pagination_list_hosting a', function (event) {
      event.preventDefault();
      var type_user = $('#type_user_personal').val();
      var page = $(this).attr('data-page');
      var sort = $('.sort_type').val();
      if ( type_user == 'select_status_vps' ) {
        var status = $('#select_status_hosting').val();
        var q = $('#search').val();
        // console.log(status , q);
        $('#type_user_personal').val('select_status_vps');
        $.ajax({
            url: '/admin/hostings/select_status_hosting',
            data: { status: status, q: q,sort:sort, page:page },
            dataType: 'json',
            beforeSend: function(){
                var html = '<td  colspan="10" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data.data != '') {
                    screen_vps(data);
                } else {
                    html = '<td  colspan="10" class="text-center text-danger">Không có Hosting trong dữ liệu</td>';
                    $('tbody').html(html);
                }
            },
            error: function (e) {
                console.log(e);
                var html = '<td  colspan="10" class="text-center text-danger">Truy vấn Hosting lỗi!</td>';
                $('tbody').html(html);
            }
        });
      }
      else if ( type_user == 'search_hosting' ) {
        var q = $('#search').val();
        var status = $('#select_status_hosting').val();
        $.ajax({
            url: '/admin/hostings/search',
            data: { status: status, q: q,sort:sort, page:page },
            dataType: 'json',
            beforeSend: function(){
                var html = '<td  colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data.data != '') {
                    screen_vps(data);
                } else {
                    html = '<td  colspan="9" class="text-center text-danger">Không có dữ liệu Hosting</td>';
                    $('tbody').html(html);
                }
            },
            error: function (e) {
                console.log(e);
                var html = '<td  colspan="9" class="text-center text-danger">Truy vấn Hosting lỗi!</td>';
                $('tbody').html(html);
            }
        });
      }
      else {
        if ( type_user ==  'personal' ) {
            var link = '/admin/hostings/list_hosting_of_personal';
        } else {
            var link = '/admin/hostings/list_hosting_of_enterprise';
        }
        // console.log(link);
        $.ajax({
            url: link,
            data: {page: page},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data != '') {
                    screen_list_vps(data);
                } else {
                    var html = '';
                    html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Không có thanh toán trong dữ liệu!</span></td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                    $('.paginate_top').html('');
                }
            },
            error: function (e) {
              console.log(e);
              var html = '';
              html += '<td colspan="9" class="text-center"><span class="text-danger text-center">Lỗi truy vấn thanh toán!</span></td>';
              $('tbody').html(html);
              $('tfoot').html('');
              $('.paginate_top').html('');
            }
        });
      }
  })

  $('.sort_next_due_date').on('click', function () {
      var vps_q = $('#search').val();
      var status = $('#select_status_vps').val();
      $('#type_user_personal').val('search_vps');
      var vps_sort = $(this).attr('data-sort');
      if (vps_sort == 'ASC') {
        $(this).attr('data-sort', 'DESC');
        $('.sort_type').val('ASC');
        $('.sort_next_due_date i').removeClass('fa-sort-down');
        $('.sort_next_due_date i').removeClass('fa-sort');
        $('.sort_next_due_date i').addClass('fa-sort-up');
      } else {
        $(this).attr('data-sort', 'ASC');
        $('.sort_type').val('DESC');
        $('.sort_next_due_date i').addClass('fa-sort-down');
        $('.sort_next_due_date i').removeClass('fa-sort');
        $('.sort_next_due_date i').removeClass('fa-sort-up');
      }
      $.ajax({
          url: '/admin/hostings/search',
          data: { status: status, q: q, sort:vps_sort },
          dataType: 'json',
          beforeSend: function(){
              var html = '<td  colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              // console.log(data);
              var html = '';
              if (data.data != '') {
                  screen_vps(data);
              } else {
                  html = '<td  colspan="9" class="text-center text-danger">Không có dữ liệu Hosting</td>';
                  $('tbody').html(html);
              }
          },
          error: function (e) {
              console.log(e);
              var html = '<td  colspan="9" class="text-center text-danger">Truy vấn Hosting lỗi!</td>';
              $('tbody').html(html);
          }
      });
  })

  function addCommas(nStr)
  {
      nStr += '';
      x = nStr.split('.');
      x1 = x[0];
      x2 = x.length > 1 ? '.' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;
      while (rgx.test(x1)) {
          x1 = x1.replace(rgx, '$1' + '.' + '$2');
      }
      return x1 + x2;
  }
});
