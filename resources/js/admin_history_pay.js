$(document).ready(function () {
  $('#search').on('keyup', function () {
      var q = $(this).val();
      var id = $('#user_id').val();
      $.ajax({
        url: '/admin/users/tim-kiem-lich-su-giao-dich',
        data: {id: id, q: q},
        dataType: 'json',
        beforeSend: function(){
          var html = '<td  colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data);
          if (data.data != '') {
            screent_history_pay(data);
          } else {
            $('tbody').html('<td colspan="9" class="text-center text-danger">Thông tin thanh toán của khách hàng này không có trong dữ liệu!</td>');
          }
        },
        error: function (e) {
          console.log(e);
          $('tbody').html('<td colspan="9" class="text-center text-danger">Truy xuất đến thông tin thanh toán lỗi!</td>');
        }
      });
  })

  $(document).on('click', '.pagination_history_pay a', function () {
      var q  = $('#search').val();
      var id = $('#user_id').val();
      var page = $(this).attr('data-page');
      $.ajax({
        url: '/admin/users/tim-kiem-lich-su-giao-dich',
        data: {id: id, q: q, page: page},
        dataType: 'json',
        beforeSend: function(){
          var html = '<td  colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
          $('tbody').html(html);
        },
        success: function (data) {
          // console.log(data);
          if (data.data != '') {
            screent_history_pay(data);
          } else {
            $('tbody').html('<td colspan="9" class="text-center text-danger">Thông tin thanh toán của khách hàng này không có trong dữ liệu!</td>');
          }
        },
        error: function (e) {
          console.log(e);
          $('tbody').html('<td colspan="9" class="text-center text-danger">Truy xuất đến thông tin thanh toán lỗi!</td>');
        }
      });
  })

  function screent_history_pay(data) {
      var html = '';
      var classt = '';
      $.each(data.data , function (index, payment) {
        console.log(payment.text_method_gd_invoice);
        html += '<tr>';
        html += '<td>' + payment.ma_gd + '</td>';
        html += '<td>' + payment.text_method_gd_invoice + '</td>';
        html += '<td>' + payment.text_type_gd + '</td>';
        html += '<td>';
        if ( payment.type_gd != 1 || payment.type_gd != 98 || payment.type_gd != 99 ) {
          html += '<button type="button" class="btn btn-outline-success collapsed tooggle-plus" data-toggle="collapse" data-target="#service'+ payment.id +'" data-placement="top" title="" data-original-title="Chi tiết" aria-expanded="false">';
          html += '<i class="fas fa-plus"></i>';
          html += '</button>';
          html += '<div id="service'+ payment.id +'" class="mt-4 collapse" style="">';
          html += payment.text_list_service;
          html += '</div>';
        }
        html += '</td>';
        html += '<td>';
        if ( payment.type_gd == 1 || payment.type_gd == 98 ) {
          html += '<strong class=" text-success">+</strong>';
          classt = 'text-success';
        } else {
          html += '<strong class="text-danger">-</strong>';
          classt = 'text-danger';
        }
        html += '<span class="'+ classt +'">' + payment.money + '</span>';
        html += '</td>';
        html += '<td>' + payment.log_payment_before + '</td>';
        html += '<td>' + payment.log_payment_after + '</td>';
        html += '<td>' + payment.text_date_gd + '</td>';
        html += '<td>' + payment.text_status + '</td>';
        html += '</tr>';
      })
      $('tbody').html(html);
      var total = data.total;
      var per_page = data.perPage;
      var current_page = data.current_page;
      var html_page = '';
      if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_history_pay">';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 || current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '</nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_history_pay">';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '</nav>';
            html_page += '</td>';
          }
      }
      $('tfoot').html(html_page);
  }

})
