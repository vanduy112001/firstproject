$(document).ready(function () {

    loadHostingDa();
    function loadHostingDa() {
        var id = $('#server_hosting_id').val();
        $.ajax({
            url: '/admin/directAdmin/getHostingDaPortal',
            dataType: 'json',
            data: { id: id },
            beforeSend: function(){
                var html = '<td class="text-center" colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
             success: function (data) {
               // console.log(data);
               if (data != '') {
                 screent_loadHosting(data);
               } else {
                   var html = '<span class="text-center" colspan="6">Không có Hosting trong account này.</span>';
                   $('tbody').html(html);
               }
             },
             error: function (e) {
                 console.log(e);
                 var html = '<span class="text-center" colspan=""6>Lỗi truy xuất DA Portal.</span>';
                 $('tbody').html(html);
             }
        })
    }

    function screent_loadHosting(data) {
        var html = '';
        var server_id = $('#server_hosting_id').val();
        $.each(data, function (index, hosting) {
            html += '<tr>';
            html += '<td>'+ hosting.domain +'</td>';
            html += '<td>'+ hosting.creator +'</td>';
            html += '<td>'+ hosting.package +'</td>';
            if (typeof hosting.suspended_by_admin === "undefined") {
              html += '<td></td>';
            }
            else {
              html += '<td>'+ hosting.suspended_by_admin +'</td>';
            }
            // console.log(data);
            if (hosting.user_pt == 0) {
                html += '<td></td>';
            } else {
                html += '<td><a href="/admin/users/detail/'+ hosting.user_pt.id +'">'+ hosting.user_pt.name +'</a></td>';

            }
            html += '<td>';
            html += '<a href="/admin/directAdmin/addHostingWithPortal/'+ hosting.username +'/'+ server_id +'"  class="btn btn-warning"><i class="fas fa-edit"></i></a>';
            html += '</td>';
            html += '<tr>';
        })
        $('tbody').html(html);
    }

})
