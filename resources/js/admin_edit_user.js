$(document).ready(function() {
    // khởi tạo modal Toast
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    $(document).on('click', '.edit-credit', function () {
        var id = $(this).attr('data-id');
        var credit = $(this).attr('data-credit');
        var token = $('meta[name="csrf-token"]').attr('content');
        var html = '';
        html += '<form id="form_update_credit">';
        html += '<input type="hidden" name="_token" value="'+ token +'">';
        html += '<input type="hidden" name="user_id" id="user_id" value="'+ id +'">';
        html += '<div class="form-group row">';
        html += '<div class="col-md-8">';
        html += '<input type="text" name="user_credit" class="form-control" id="user_credit" value="'+ credit +'" placeholder="Nhập số tiền cần thay đổi">';
        html += '</div>';
        html += '<div class="col-md-4">';
        html += '<button type="button" name="button" class="btn btn-primary button_submit_update_credit">Thay đổi</button>';
        html += '</div>';
        html += '</div>';
        html += '</form>';
        $('.form-edit-credit').html(html);
    });

    $(document).on('click', '.edit_cloudzone_point', function () {
        var id = $(this).attr('data-id');
        var token = $('meta[name="csrf-token"]').attr('content');
        var html = '';
        html += '<form id="form_update_cloudzone_point">';
        html += '<input type="hidden" name="_token" value="'+ token +'">';
        html += '<input type="hidden" name="user_id" id="user_id" value="'+ id +'">';
        html += '<div class="form-group row">';
        html += '<div class="col-md-8">';
        html += '<input type="text" name="cloudzone_point" class="form-control" id="cloudzone_point" value="" placeholder="Nhập số điểm Cloudzone Point">';
        html += '</div>';
        html += '<div class="col-md-4">';
        html += '<button type="button" name="button" class="btn btn-primary button_submit_edit_cloudzone_point">Nâng điểm</button>';
        html += '</div>';
        html += '</div>';
        html += '</form>';
        $('#edit_cloudzone_point').html(html);
    });

    $(document).on('click', '.payment-credit-total', function () {
        var id = $(this).attr('data-id');
        var token = $('meta[name="csrf-token"]').attr('content');
        var html = '';
        html += '<form role="form" method="post" action="/admin/users/addPayment" id="form_select" class="mt-2">';
        html += '<input type="hidden" name="_token" value="'+ token +'">';
        html += '<input type="hidden" name="user_id" id="user_id" value="'+ id +'">';
        html += '<div class="form-group ">';
        html += '<input type="text" id="amount" class="form-control" name="amount" value="" placeholder="Nhập số tiền nạp">';
        html += '</div>';
        html += '<div class="form-group">';
        html += '<select class="form-control" name="method_pay" id="method_pay">';
        html += '<option value="" selected disabled>Chọn hình thức thanh toán</option>';
        html += '<option value="pay_in_office">Trực tiếp tại văn phòng</option>';
        html += '<option value="bank_transfer_vietcombank">Chuyển khoản ngân hàng Vietcombank</option>';
        html += '<option value="bank_transfer_techcombank">Chuyển khoản ngân hàng Techcombank</option>';
        html += '<option value="momo">Thanh toán qua MOMO</option>';
        html += '</select>';
        html += '</div>';
        html += '<div class="form-group text-right">';
        html += '<input class="btn btn-primary" type="submit" value="Tạo nạp tiền">';
        html += '</div>';
        html += '</form>';
        $('#payment_credit_total').html(html);
    });

    $(document).on('click', '.button_submit_update_credit', function () {
        var id = $('#user_id').val();
        var credit = $('#user_credit').val();
        var form = $('#form_update_credit').serialize();

        $.ajax({
           url: '/admin/users/updateCredit',
           type: 'post',
           dataType: 'json',
           data: form,
           beforeSend: function(){
               var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
               $('.form-edit-credit').html(html);
           },
           success: function (data) {
              if (data != '') {
                var html = '';
                html += addCommas(credit);
                html += '<small class="ml-2">VNĐ</small>';
                html += '<span class="ml-2">';
                html += '<a href="#" class="edit-credit text-secondary" data-credit="'+ credit +'" data-id="'+ id +'" data-toggle="tooltip" title="Thay đổi số dư tài khoản"> <i class="fas fa-edit"></i> </a>'
                html += '</span>'
                $('.form-edit-credit').html(html);
              } else {
                var html = '<span class="text-center">Thay đổi số dư tài khoản thất bại.</span>';
                html += '<span class="ml-2">';
                html += '<a href="#" class="edit-credit text-secondary" data-credit="'+ credit +'" data-id="'+ id +'" data-toggle="tooltip" title="Thay đổi số dư tài khoản"> <i class="fas fa-edit"></i> </a>'
                html += '</span>'
                $('.form-edit-credit').html(html);
              }
           },
           error: function (e) {
               console.log(e);
               var html = '<span class="text-center">Lỗi truy xuất user.</span>';
               html += '<span class="ml-2">';
               html += '<a href="#" class="edit-credit text-secondary" data-credit="'+ credit +'" data-id="'+ id +'" data-toggle="tooltip" title="Thay đổi số dư tài khoản"> <i class="fas fa-edit"></i> </a>'
               html += '</span>'
               $('.form-edit-credit').html(html);
           }
        })

    });

    $(document).on('click', '.button_submit_edit_cloudzone_point', function () {
        var id = $('#user_id').val();
        var form = $('#form_update_cloudzone_point').serialize();

        $.ajax({
           url: '/admin/users/updateCloudzonePoint',
           type: 'post',
           dataType: 'json',
           data: form,
           beforeSend: function(){
               var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
               $('#edit_cloudzone_point').html(html);
           },
           success: function (data) {
              if (data != '') {
                var html = '';
                html += addCommas(data);
                html += '<small class="ml-2">VNĐ</small>';
                html += '<span class="ml-2">';
                html += '<a href="#" class="edit_cloudzone_point text-secondary" data-id="'+ id +'" > <i class="fas fa-edit"></i> </a>'
                html += '</span>'
                $('#edit_cloudzone_point').html(html);
              } else {
                var html = '<span class="text-center">Thêm điểm Cloudzone Point thất bại.</span>';
                html += '<span class="ml-2">';
                html += '<a href="#" class="edit_cloudzone_point text-secondary" data-id="'+ id +'" > <i class="fas fa-edit"></i> </a>'
                html += '</span>'
                $('#edit_cloudzone_point').html(html);
              }
           },
           error: function (e) {
               console.log(e);
               var html = '<span class="text-center">Lỗi truy xuất user.</span>';
               html += '<span class="ml-2">';
               html += '<a href="#" class="edit_cloudzone_point text-secondary" data-id="'+ id +'"> <i class="fas fa-edit"></i> </a>'
               html += '</span>'
               $('#edit_cloudzone_point').html(html);
           }
        })

    });

    $('.button_active_user').on('click', function () {
          $('#active_user').modal('show');
          var type = $(this).attr('data-type');
          var id  = $(this).attr('data-id');
          if (type == 'active') {
              $('.modal-title-active').text('Kích hoạt tài khoản');
              $('#notication-user').html('<span class="text-danger">Bạn có muốn kích hoạt tài khoản này không?</span>');
              $('#button-active').val('Kích hoạt');
              $('#button-active').attr('data-type' , 'active');
              $('#button-active').attr('data-id' , id);
          } else {
              $('.modal-title-active').text('Tắt kích hoạt tài khoản');
              $('#notication-user').html('<span class="text-danger">Bạn có muốn tắt kích hoạt tài khoản này không?</span>');
              $('#button-active').val('Tắt kích hoạt');
              $('#button-active').attr('data-type' , 'inactive');
              $('#button-active').attr('data-id' , id);
          }
    })

    $('#button-active').on('click', function () {
        var type = $(this).attr('data-type');
        var id  = $(this).attr('data-id');
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/admin/users/actionUser',
            type: 'post',
            dataType: 'json',
            data: {'_token': token, id: id, type: type},
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#notication-user').html(html);
                $('#button-active').attr('disabled', true);
            },
            success: function (data) {
                if (data != '') {
                  if (type == 'active') {
                      $(document).Toasts('create', {
                          class: 'bg-success',
                          title: 'Tài khoản',
                          subtitle: 'kích hoạt',
                          body: "Kích hoạt tài khoản thành công",
                      })
                      $('.button_active_user').attr('data-type', 'inactive');
                      $('.button_active_user').text('Tắt kích hoạt');
                  } else {
                      $(document).Toasts('create', {
                          class: 'bg-success',
                          title: 'Tài khoản',
                          subtitle: 'tắt kích hoạt',
                          body: "Tắt kích hoạt tài khoản thành công",
                      })
                      $('.button_active_user').attr('data-type', 'active');
                      $('.button_active_user').text('kích hoạt');
                  }
                  $('#active_user').modal('hide');
                } else {
                   if (type == 'active') {
                       var html = '<span class="text-center">Kích hoạt tài khoản thất bại.</span>';
                       $('#notication-user').html(html);
                   } else {
                       var html = '<span class="text-center">Tắt kích hoạt tài khoản thất bại.</span>';
                       $('#notication-user').html(html);
                   }
                }
                $('#button-active').attr('disabled', false);
            },
            error: function (e) {
                console.log(e);
                var html = '<span class="text-center">Lỗi truy xuất user.</span>';
                $('#notication-user').html(html);
                $('#button-active').attr('disabled', false);
            }
        })
    })

    $(document).on('click', '.edit-credit-total', function () {
        var id = $(this).attr('data-id');
        var credit = $(this).attr('data-credit');
        var token = $('meta[name="csrf-token"]').attr('content');
        var html = '';
        html += '<form id="form_update_credit_total">';
        html += '<input type="hidden" name="_token" value="'+ token +'">';
        html += '<input type="hidden" name="user_id" id="user_id" value="'+ id +'">';
        html += '<div class="form-group row">';
        html += '<div class="col-md-8">';
        html += '<input type="text" name="user_credit" class="form-control" id="user_credit_total" value="'+ credit +'" placeholder="Nhập số tiền cần thay đổi">';
        html += '</div>';
        html += '<div class="col-md-4">';
        html += '<button type="button" name="button" class="btn btn-primary button_submit_update_credit_total">Thay đổi</button>';
        html += '</div>';
        html += '</div>';
        html += '</form>';
        $('.form-edit-credit-total').html(html);
    });


    $(document).on('click', '.button_submit_update_credit_total', function () {
        var id = $('#user_id').val();
        var credit = $('#user_credit_total').val();
        var form = $('#form_update_credit_total').serialize();

        $.ajax({
           url: '/admin/users/updateCreditTotal',
           type: 'post',
           dataType: 'json',
           data: form,
           beforeSend: function(){
               var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
               $('.form-edit-credit-total').html(html);
           },
           success: function (data) {
              if (data != '') {
                var html = '';
                html += addCommas(credit);
                html += '<small class="ml-2">VNĐ</small>';
                html += '<span class="ml-2">';
                html += '<a href="#" class="edit-credit-total text-secondary" data-credit="'+ credit +'" data-id="'+ id +'" data-toggle="tooltip" title="Thay đổi số dư tài khoản"> <i class="fas fa-edit"></i> </a>'
                html += '</span>'
                $('.form-edit-credit-total').html(html);
              } else {
                var html = '<span class="text-center">Thay đổi số dư tài khoản thất bại.</span>';
                html += '<span class="ml-2">';
                html += '<a href="#" class="edit-credit-total text-secondary" data-credit="'+ credit +'" data-id="'+ id +'" data-toggle="tooltip" title="Thay đổi số dư tài khoản"> <i class="fas fa-edit"></i> </a>'
                html += '</span>'
                $('.form-edit-credit-total').html(html);
              }
           },
           error: function (e) {
               console.log(e);
               var html = '<span class="text-center">Lỗi truy xuất user.</span>';
               html += '<span class="ml-2">';
               html += '<a href="#" class="edit-credit text-secondary" data-credit="'+ credit +'" data-id="'+ id +'" data-toggle="tooltip" title="Thay đổi số dư tài khoản"> <i class="fas fa-edit"></i> </a>'
               html += '</span>'
               $('.form-edit-credit-total').html(html);
           }
        })

    });

    list_hosting_by_user();
    list_vps_by_user();
    list_vps_us_by_user();
    // TODO: list cac dich vu hosting cua user
    function list_hosting_by_user() {
         var user_id = $('#user_id').val();
         $.ajax({
           url: '/admin/users/list_hosting_by_user',
           data: { user_id : user_id },
           dataType: 'json',
           beforeSend: function(){
               var html = '<td  colspan="8" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('#hosting_table body').html(html);
           },
           success: function (data) {
             if ( data.data != '' ) {
                screen_list_hostig(data)
             }
             else {
                $('#hosting_table body').html('<td colspan="8" class="text-danger text-center">Dữ liệu rỗng</td>');
             }
           },
           error: function (e) {
              console.log(e);
              $('#hosting_table body').html('<td  colspan="8" class="text-danger">Lỗi kết nối Hosting!</td>');
           }
       })
    }

    function screen_list_hostig(data) {
        var html = '';
        $.each(data.data, function (index, hosting) {
            html += '<tr>';
            html += '<td>' + hosting.id + '</td>';
            html += '<td>' + hosting.product_name + '</td>';
            html += '<td><a target="_blank" href="/admin/hostings/detail/' + hosting.id  + '">' + hosting.domain + '</a></td>';
            html += '<td>' + hosting.billing_cycle + '</td>';
            html += '<td>' + hosting.start_date + '</td>';
            html += '<td>' + hosting.end_date + '</td>';
            html += '<td>' + hosting.status_hosting + '</td>';
            html += '</tr>';
        });
        $('#hosting_table tbody').html(html);
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_list_hosting>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 || current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_list_hosting>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('#hosting_table tfoot').html(html_page);
    }

    $(document).on('click', '#hosting_table .pagination a', function(event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var user_id = $('#user_id').val();
        $.ajax({
            url: '/admin/users/list_hosting_by_user',
            data: { user_id : user_id, page: page },
            dataType: 'json',
            beforeSend: function(){
                var html = '<td  colspan="8" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('#hosting_table body').html(html);
            },
            success: function (data) {
              if ( data.data != '' ) {
                 screen_list_hostig(data)
              }
              else {
                 $('#hosting_table body').html('<td colspan="8" class="text-danger text-center">Dữ liệu rỗng</td>');
              }
            },
            error: function (e) {
               console.log(e);
               $('#hosting_table body').html('<td  colspan="8" class="text-danger">Lỗi kết nối Hosting!</td>');
            }
        })
    });

    // TODO: list cac dich vu vps cua user
    function list_vps_by_user() {
         var user_id = $('#user_id').val();
         var status_vps = '';
         $.ajax({
           url: '/admin/users/list_vps_by_user',
           data: { user_id : user_id, status_vps: status_vps },
           dataType: 'json',
           beforeSend: function(){
               var html = '<td  colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('#list_vps tbody').html(html);
           },
           success: function (data) {
             // console.log(data);
             if ( data.data.length ) {
                screen_list_vps(data)
             }
             else {
                $('#list_vps tbody').html('<td colspan="9" class="text-danger text-center">Dữ liệu rỗng</td>');
             }
           },
           error: function (e) {
              console.log(e);
              $('#list_vps tbody').html('<td  colspan="9" class="text-danger">Lỗi kết nối VPS!</td>');
           }
       })
    }

    function screen_list_vps(data) {
        var html = '';
        $.each(data.data, function (index, vps) {
            html += '<tr>';
            html += '<td>' + vps.id + '</td>';
            html += '<td>' + vps.product_name + '</td>';
            html += '<td><a target="_blank" href="/admin/vps/detail/' + vps.id  + '" target="_bank">' + vps.ip + '</a></td>';
            html += '<td>' + vps.text_type_vps + '</td>';
            html += '<td>' + vps.amount + ' VNĐ</td>';
            html += '<td>' + vps.config_vps + '</td>';
            html += '<td>' + vps.billing_cycle + '</td>';
            html += '<td>' + vps.start_date + '</td>';
            html += '<td>' + vps.end_date + '</td>';
            if(vps.status == 'Pending') {
                html += '<td class="text-danger">Chưa tạo</td>';
            }
            else if(vps.status_vps == 'progressing') {
                html += '<td class="text-info">Đang tạo</td>';
            }
            else if(vps.status_vps == 'off') {
                html += '<td class="text-danger">Đã tắt</td>';
            }
            else if(vps.status_vps == 'on') {
                html += '<td class="text-success">Đang bật</td>';
            }
            else if(vps.status_vps == 'admin_off') {
                html += '<td class="text-danger">Admin tắt</td>';
            }
            else if(vps.status_vps == 'cancel') {
                html += '<td class="text-danger">Đã hủy</td>';
            }
            else if(vps.status_vps == 'delete_vps') {
                html += '<td class="text-danger">Đã xóa</td>';
            }
            else if(vps.status_vps == 'change_user') {
                html += '<td class="text-danger">Đã chuyển</td>';
            }
            else if(vps.status_vps == 'rebuild') {
                html += '<td class="text-info">Đang cài lại</td>';
            }
            else if(vps.status_vps == 'change_ip') {
                html += '<td class="text-info">Đang đổi IP</td>';
            }
            else if(vps.status_vps == 'expire') {
                html += '<td class="text-danger">Đã hết hạn</td>';
            }
            else {
                html += '<td class="text-secondary">Chưa tạo</td>';
            }
            html += '</tr>';
        });
        $('#list_vps tbody').html(html);
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_list_hosting>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 || current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_list_hosting>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('#list_vps tfoot').html(html_page);
    }

    $(document).on('click', '#list_vps .pagination a', function(event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var user_id = $('#user_id').val();
        $.ajax({
            url: '/admin/users/list_vps_by_user',
            data: { user_id : user_id, page: page },
            dataType: 'json',
            beforeSend: function(){
                var html = '<td  colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('#list_vps tbody').html(html);
            },
            success: function (data) {
              if ( data.data.length ) {
                 screen_list_vps(data)
              }
              else {
                 $('#list_vps tbody').html('<td colspan="9" class="text-danger text-center">Dữ liệu rỗng</td>');
              }
            },
            error: function (e) {
               console.log(e);
               $('#list_vps tbody').html('<td  colspan="9" class="text-danger">Lỗi kết nối Hosting!</td>');
            }
        })
    });

    $('.filter_status').on('click', function () {
      // console.log($(this).attr('data-action'));
      if ( $(this).attr('data-action') == 'on' ) {
        $(this).attr('data-action', 'off');
        var html = '';
        html = `
        <select class="filter_status_vps">
          <option value="">Chọn trạng thái</option>
          <option value="on">Bật</option>
          <option value="off">Tắt</option>
          <option value="progressing">Đang tạo</option>
          <option value="rebuild">Đang cài lại</option>
          <option value="change_ip">Đang đổi IP</option>
          <option value="expire">Đã hết hạn</option>
          <option value="suspend">Đang bị khóa</option>
          <option value="delete_vps">Đã xóa</option>
          <option value="change_user">Đã chuyển</option>
        </select>`;
        $('.filter_status').html(html);
      }
    })

    $(document).on('change', '.filter_status_vps' , function () {
      var user_id = $('#user_id').val();
      var status_vps = $(this).val();
      $.ajax({
        url: '/admin/users/list_vps_by_user',
        data: { user_id : user_id, status_vps: status_vps },
        dataType: 'json',
        beforeSend: function(){
            var html = '<td  colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
            $('#list_vps tbody').html(html);
        },
        success: function (data) {
          // console.log(data.data.length);
          if ( data.data.length > 0 ) {
              // console.log('da den 1');
             screen_list_vps(data)
          }
          else {
              // console.log('da den 2');
             $('#list_vps tbody').html('<td colspan="9" class="text-danger text-center">Không có VPS trong dữ liệu</td>');
          }
        },
        error: function (e) {
           console.log(e);
           $('#list_vps tbody').html('<td  colspan="9" class="text-danger">Lỗi kết nối VPS!</td>');
        }
      })
    })

    $('.filter_status_us').on('click', function () {
      // console.log($(this).attr('data-action'));
      if ( $(this).attr('data-action') == 'on' ) {
        $(this).attr('data-action', 'off');
        var html = '';
        html = `
        <select class="filter_status_vps_us">
          <option value="">Chọn trạng thái</option>
          <option value="on">Bật</option>
          <option value="off">Tắt</option>
          <option value="progressing">Đang tạo</option>
          <option value="rebuild">Đang cài lại</option>
          <option value="change_ip">Đang đổi IP</option>
          <option value="expire">Đã hết hạn</option>
          <option value="suspend">Đang bị khóa</option>
          <option value="delete_vps">Đã xóa</option>
          <option value="change_user">Đã chuyển</option>
        </select>`;
        $('.filter_status_us').html(html);
      }
    })

    $(document).on('change', '.filter_status_vps_us' , function () {
      var user_id = $('#user_id').val();
      var status_vps = $(this).val();
      $.ajax({
        url: '/admin/users/list_vps_us_by_user',
        data: { user_id : user_id, status_vps: status_vps },
        dataType: 'json',
        beforeSend: function(){
            var html = '<td  colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
            $('#list_vps_us tbody').html(html);
        },
        success: function (data) {
          // console.log(data);
          if ( data.data.length ) {
             screen_list_vps_us(data)
          }
          else {
             $('#list_vps_us tbody').html('<td colspan="9" class="text-danger text-center">Không có VPS trong dữ liệu</td>');
          }
        },
        error: function (e) {
           console.log(e);
           $('#list_vps_us tbody').html('<td  colspan="9" class="text-danger">Lỗi kết nối VPS!</td>');
        }
      })
    })

    function list_vps_us_by_user() {
         var user_id = $('#user_id').val();
         var status_vps = '';
         $.ajax({
           url: '/admin/users/list_vps_us_by_user',
           data: { user_id : user_id, status_vps: status_vps },
           dataType: 'json',
           beforeSend: function(){
               var html = '<td  colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('#list_vps_us tbody').html(html);
           },
           success: function (data) {
             if ( data.data.length ) {
                screen_list_vps_us(data)
             }
             else {
                $('#list_vps_us tbody').html('<td colspan="9" class="text-danger text-center">Không có VPS trong dữ liệu</td>');
             }
           },
           error: function (e) {
              console.log(e);
              $('#list_vps_us tbody').html('<td  colspan="9" class="text-danger">Lỗi kết nối VPS!</td>');
           }
       })
    }

    function screen_list_vps_us(data) {
        var html = '';
        $.each(data.data, function (index, vps) {
            html += '<tr>';
            html += '<td>' + vps.id + '</td>';
            html += '<td>' + vps.product_name + '</td>';
            html += '<td><a target="_blank" href="/admin/vps/detail/' + vps.id  + '" target="_bank">' + vps.ip + '</a></td>';
            html += '<td>' + vps.text_type_vps + '</td>';
            html += '<td>' + vps.amount + ' VNĐ</td>';
            html += '<td>' + vps.config_vps + '</td>';
            html += '<td>' + vps.billing_cycle + '</td>';
            html += '<td>' + vps.start_date + '</td>';
            html += '<td>' + vps.end_date + '</td>';
            if(vps.status == 'Pending') {
                html += '<td class="text-danger">Chưa tạo</td>';
            }
            else if(vps.status_vps == 'progressing') {
                html += '<td class="text-info">Đang tạo</td>';
            }
            else if(vps.status_vps == 'off') {
                html += '<td class="text-danger">Đã tắt</td>';
            }
            else if(vps.status_vps == 'on') {
                html += '<td class="text-success">Đang bật</td>';
            }
            else if(vps.status_vps == 'admin_off') {
                html += '<td class="text-danger">Admin tắt</td>';
            }
            else if(vps.status_vps == 'cancel') {
                html += '<td class="text-danger">Đã hủy</td>';
            }
            else if(vps.status_vps == 'delete_vps') {
                html += '<td class="text-danger">Đã xóa</td>';
            }
            else if(vps.status_vps == 'change_user') {
                html += '<td class="text-danger">Đã chuyển</td>';
            }
            else if(vps.status_vps == 'rebuild') {
                html += '<td class="text-info">Đang cài lại</td>';
            }
            else if(vps.status_vps == 'change_ip') {
                html += '<td class="text-info">Đang đổi IP</td>';
            }
            else if(vps.status_vps == 'expire') {
                html += '<td class="text-danger">Đã hết hạn</td>';
            }
            else {
                html += '<td class="text-secondary">Chưa tạo</td>';
            }
            html += '</tr>';
        });
        $('#list_vps_us tbody').html(html);
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_list_hosting>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 || current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_list_hosting>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
        }
        $('#list_vps_us tfoot').html(html_page);
    }

    $(document).on('click', '#list_vps_us .pagination a', function(event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var user_id = $('#user_id').val();
        $.ajax({
            url: '/admin/users/list_vps_by_user',
            data: { user_id : user_id, page: page },
            dataType: 'json',
            beforeSend: function(){
                var html = '<td  colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('#list_vps_us tbody').html(html);
            },
            success: function (data) {
              if ( data.data.length ) {
                 screen_list_vps_us(data)
              }
              else {
                 $('#list_vps_us tbody').html('<td colspan="9" class="text-danger text-center">Không có VPS trong dữ liệu</td>');
              }
            },
            error: function (e) {
               console.log(e);
               $('#list_vps_us tbody').html('<td  colspan="9" class="text-danger">Lỗi kết nối!</td>');
            }
        })
    });

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

});
