$(document).ready(function () {
    load_hosting();

    function load_hosting() {
        $.ajax({
            url: '/dich-vu/email_hosting/list_email_hosting_nearly_and_expired',
            dataType: 'json',
            beforeSend: function(){
                var html = '<td class="text-center"  colspan="10"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data.data);
                if ( data.data.length > 0 ) {
                  list_hosting_nearly_and_expire_screent(data);
                  $("#data-hosting-table").DataTable({
                      "columnDefs": [
                           { "targets": [  1, 3 , 4, 6, 7, 8, 9 ], "orderable": false }
                      ],
                      "pageLength": 30,
                      "lengthMenu": [[ 25, 50, 100, -1], [25, 50, 100, "All"]],
                      "iDisplayLength": 25
                  });
                } else {
                  $('tfoot').html('');
                  $('tbody').html('<td class="text-center text-danger"  colspan="10">Không có dịch vụ hosting nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function (e) {
                console.log(e);
                $('tfoot').html('');
                $('tbody').html('<td class="text-center text-danger"  colspan="10">Lỗi truy vấn dữ liệu dịch vụ hosting!</td>');
            }
        });
    }

    function list_hosting_nearly_and_expire_screent(data) {
       // console.log(data);
       var html = '';
       $.each(data.data, function (index , hosting) {
          html += '<tr>';
          // checkbox
          html += '<td><a href="/service/detail/'+ hosting.id +'?type=hosting">'+ hosting.product_name +'</a></td>'
          // ip
          html += '<td class="ip_hosting" data-ip="'+ hosting.domain +'"><a href="/service/detail/'+ hosting.id +'?type=hosting">'+ hosting.domain +'</a>';
          if (hosting.expired == true) {
            html += ' - <span class="text-danger">Hết hạn</span>';
          } else if (hosting.isExpire == true) {
            html += ' - <span class="text-danger">Gần hết hạn</span>';
          }
          html += '</td>';
          // loai

          // khach hang
          // ngay tao
          html += '<td><span>' + hosting.date_create + '</span></td>';
          // ngay ket thuc
          html += '<td class="next_due_date">';
          if (hosting.isExpire == true || hosting.expired == true) {
            html += '<span class="text-danger">' + hosting.next_due_date + '<span>';
          } else {
            html += '<span>' + hosting.next_due_date + '<span>';
          }
          html += '</td>';
          // thoi gian thue
          html += '<td>' + hosting.text_billing_cycle +  '</td>';
          // html += '<td>' + hosting.amount + ' VNĐ</td>';
          html += '<td class="hosting-status">' + hosting.text_status_vps + '</td>';
          html += '<td class="page-service-action">';
          if (hosting.status_hosting != 'delete_hosting' ) {
            if (hosting.status_hosting != 'cancel') {
              if (hosting.status_hosting != 'suspend') {
                if (hosting.isExpire == true || hosting.expired == true) {
                  html += '<button type="button" class="btn btn-warning button-action-email_hosting expired" data-toggle="tooltip" data-placement="top" title="Gia hạn Email Hosting" data-action="expired" data-id="' + hosting.id + '" data-ip="' + hosting.domain + '"><i class="fas fa-plus-circle"></i></button>';
                } else {
                  if (hosting.status_hosting != 'admin_off') {
                    if (hosting.status_hosting != 'cancel') {
                      html += '<button class="btn btn-outline-danger button-action-email_hosting terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ Email Hosting" data-action="terminated" data-id="' + hosting.id + '" data-ip="' + hosting.domain + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                    }
                  }
                }
              }
            }
          }
          html += '</td>';
          html += '</tr>';
       })
       $('tbody').html(html);
       $('[data-toggle="tooltip"]').tooltip();
    }

    $(document).on('click','.button-action-email_hosting', function () {
        $('tr').removeClass('action-row-email_hosting');
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var domain = $(this).attr('data-ip');
        $('#button_email_hosting_service').val(id);
        $('#button_email_hosting_service').fadeIn();
        $('#button-finish').fadeOut();
        $('#modal-service').modal('show');
        switch (action) {
          case 'terminated':
                $('#modal-service .modal-title').text('Yêu cầu hủy dịch vụ Email Hosting');
                $('#notication-service').html('<span>Bạn có muốn hủy dịch vụ Email Hosting <b class="text-danger">(domain: '+ domain +')</b> này không ?</span>');
            break;
          case 'expired':
              $('#modal-service .modal-title').text('Yêu cầu gia hạn Email Hosting');
              $('#notication-service').html('<span>Bạn có muốn gia hạn Email Hosting <b class="text-danger">(domain: '+ domain +')</b> này không </span>');
              $.ajax({
                  url: '/service/request_expired_email_hosting',
                  data: {id: id},
                  dataType: 'json',
                  beforeSend: function(){
                      var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                      $('#button_email_hosting_service').attr('disabled', true);
                      $('#notication-service').html(html);
                  },
                  success: function (data) {
                      // console.log(data);
                      var html = '';
                      if (data.error == 1) {
                          $('#notication-service').html('<span class="text-center text-danger">Dịch vụ thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại dịch vụ được chọn.</span>');
                          $('#button_email_hosting_service').attr('disabled', false);
                      } else {
                          // console.log(data);
                          html += '<div class="form-group">';
                          html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                          html += '<div class="mt-3 mb-3">';
                          html += '<select id="select_billing_cycle" class="form-control select2 text-center" style="width:100%;">';
                          html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                          if ( data.total['monthly'] != "0 VNĐ" ) {
                            html += '<option value="monthly">1 Tháng / '+ data.total['monthly'] +'</option>';
                          }
                          if ( data.total['twomonthly'] != "0 VNĐ" ) {
                            html += '<option value="twomonthly">2 Tháng / '+ data.total['twomonthly'] +'</option>';
                          }
                          if ( data.total['quarterly'] != "0 VNĐ" ) {
                            html += '<option value="quarterly">3 Tháng / '+ data.total['quarterly'] +'</option>';
                          }
                          if ( data.total['semi_annually'] != "0 VNĐ" ) {
                            html += '<option value="semi_annually">6 Tháng / '+ data.total['semi_annually'] +'</option>';
                          }
                          if ( data.total['annually'] != "0 VNĐ" ) {
                            html += '<option value="annually">1 Năm / '+ data.total['annually'] +'</option>';
                          }
                          if ( data.total['biennially'] != "0 VNĐ" ) {
                            html += '<option value="biennially">2 Năm / '+ data.total['biennially'] +'</option>';
                          }
                          if ( data.total['triennially'] != "0 VNĐ" ) {
                            html += '<option value="triennially">3 Năm / '+ data.total['triennially'] +'</option>';
                          }
                          html += '</select>';
                          html += '</div>';
                          html += '</div>';
                          $('#notication-service').html(html);
                          $('#button_email_hosting_service').attr('disabled', false);
                          $('.select2').select2();
                      }
                  },
                  error: function (e) {
                    console.log(e);
                    $('#notication-service').html('<span class="text-center">Lỗi truy xuất dữ liệu khách hàng.</span>');
                    $('#button_email_hosting_service').attr('disabled', false);
                  },
                });
            break;

        }
        $('#button_email_hosting_service').attr('data-id', id);
        $('#button_email_hosting_service').attr('data-action', action);
        $('#button_email_hosting_service').attr('data-ip', domain);
        $(this).closest('tr').addClass('action-row-email_hosting');
    })

    $('#button_email_hosting_service').on('click', function () {
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var billing_cycle = '';
        var token = $('meta[name="csrf-token"]').attr('content');
        var ip = $(this).attr('data-ip');
        if (action == 'expired') {
            billing_cycle = $('#select_billing_cycle').val();
        }
        $.ajax({
            url: '/services/email_hosting/action_email_hosting',
            type: 'post',
            dataType: 'json',
            data: {'_token': token, id: id, action: action, billing_cycle: billing_cycle},
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button_email_hosting_service').attr('disabled', true);
                $('#notication-service').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data) {
                  if (data.error == 1) {
                    $('#notication-service').html('<span class="text-danger">Dịch vụ Email Hosting này không thuộc quyền quản lý của bạn</span>');
                    $('#button_email_hosting_service').attr('disabled', false);
                  }
                  else if (data.error == 4) {
                    $('#notication-service').html('<span class="text-danger">Hủy dịch vụ Email Hosting thất bại!</span>');
                    $('#button_email_hosting_service').attr('disabled', false);
                  }
                  else if (data.error == 3) {
                    $('#notication-service').html('<span class="text-danger">Gia hạn dịch vụ Email Hosting thất bại!</span>');
                    $('#button_email_hosting_service').attr('disabled', false);
                  }
                  else {
                    if (data.type == 4) {
                        $('.action-row-email_hosting .terminated').fadeOut();
                        $('.action-row-email_hosting .email_hosting_status').html('<span class="text-danger">Đã hủy</span>');
                        $('#button_server_service').attr('disabled', false);
                        $('tr').removeClass('action-row-email_hosting');
                        $('#notication-service').html('<span class="text-danger">Yêu cầu hủy Email Hosting thành công</span>');
                        $('#button_email_hosting_service').fadeOut();
                        $('#button-finish').fadeIn();
                    }
                    else if ( data.type == 3 ) {
                        var html = '<span class="text-center">Yêu cầu gia hạn Email Hosting thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/'+data.invoice_id+'">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                        $('#button_server_service').attr('disabled', false);
                        $('tr').removeClass('action-row-colocation');
                        $('#notication-service').html(html);
                        $('#button_email_hosting_service').fadeOut();
                        $('#button-finish').fadeIn();
                    }
                  }
                  // $('#modal-service').modal('hide');
                  // $('#button-service').attr('disabled', false);
                } else {
                  $('#notication-service').html('<span class="text-danger">Hành động truy vấn đến quản lý dịch vụ thất bại!</span>');
                  $('#button_email_hosting_service').attr('disabled', false);
                }
            },
            error: function (e) {
                console.log(e);
                $('#notication-service').html('<span class="text-danger">Truy vấn đến quản lý dịch vụ lỗi. Quý khách vui lòng liên hệ với chúng tôi để được giúp đỡ.</span>');
                $('#button_email_hosting_service').attr('disabled', false);
            }
        });
    });

})
