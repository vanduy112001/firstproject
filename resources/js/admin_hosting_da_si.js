$(document).ready(function () {

    loadHostingDa();
    function loadHostingDa() {
        var id = $('#server_hosting_id').val();
        $.ajax({
            url: '/admin/directAdmin/getHostingDaSingapore',
            dataType: 'json',
            data: { id: id },
            beforeSend: function(){
                var html = '<td class="text-center" colspan="4"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
             success: function (data) {
               // console.log(data);
               if (data != '') {
                 screent_loadHosting(data);
               } else {
                   var html = '<span class="text-center" colspan="3">Không có Hosting trong account này.</span>';
                   $('tbody').html(html);
               }
             },
             error: function (e) {
                 console.log(e);
                 var html = '<span class="text-center" colspan="3">Lỗi truy xuất DA Portal.</span>';
                 $('tbody').html(html);
             }
        })
    }

    function screent_loadHosting(data) {
        var html = '';
        var server_id = $('#server_hosting_id').val();
        $.each(data, function (index, hosting) {
            html += '<tr>';
            html += '<td>'+ hosting.domain +'</td>';
            html += '<td>'+ hosting.package +'</td>';
            // console.log(data);
            if (hosting.user_pt == 0) {
                html += '<td></td>';
            } else {
                html += '<td><a href="/admin/users/detail/'+ hosting.user_pt.id +'">'+ hosting.user_pt.name +'</a></td>';

            }
            html += '<td>';
            html += '<a href="/admin/directAdmin/addHostingSingaporeWithPortal/'+ hosting.username +'/'+ server_id +'"  class="btn btn-warning"><i class="fas fa-edit"></i></a>';
            html += '</td>';
            html += '<tr>';
        })
        $('tbody').html(html);
    }

})
