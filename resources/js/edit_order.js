$(document).ready(function () {
    $('#type_products').on('change', function() {
        var type_product = $(this).val();
        if (type_product ==  'Hosting') {
            $.ajax({
                type: "get",
                url: "/admin/orders/list_product",
                data: {action: 'Hosting'},
                dataType: "json",
                beforeSend: function(){
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('.order_product').html(html);
                },
                success: function (data) {
                    var html = '';
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '<label for="type_products">Sản phẩm</label>';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right">';
                    html += '<select name="product_id" id="product_id" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Hosting">';
                    html += '<option disabled selected>Chọn sản phẩm</option>';
                    if (data.product != '') {
                        $.each(data.product, function (index, product) { 
                            html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                        });   
                    } else {
                        html += '<option disabled class="text-danger">Không có sản phẩm Hosting</option>';
                    }
                    html += '</select>';
                    html += '</div>';
                    html += '</div>';
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '<label for="domain">Domain</label>';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right domain">';
                    html += '<input id="domain" name="domain" class="form-control" type="text" placeholder="Domain">';
                    html += '</div>';
                    html += '</div>';
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '<label for="billing_cycle">Thời gian</label>';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right">';
                    html += '<select name="billing_cycle" id="billing_cycle" class="select2 form-control"  style="width: 100%;">';
                    $.each(data.billing, function (index2, billing) { 
                        html += '<option value="'+ index2 +'">'+ billing +'</option>';
                    });
                    html += '</select>';
                    html += '</div>';
                    html += '</div>';
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '<label for="quantity_hosting">Số lượng</label>';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right">';
                    html += '<input id="quantity_hosting" name="quantity" class="form-control" type="text" placeholder="Số lượng" value="1">';
                    html += '</div>';
                    html += '</div>';
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '<label for="invoices">Thanh toán</label>';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right">';
                    html += '<select name="invoices" id="invoices" class="select2 form-control"  style="width: 100%;">';
                    $.each(data.invoices, function (index_invoices, invoice) { 
                        html += '<option value="'+ index_invoices +'">'+ invoice +'</option>';
                    });
                    html += '</select>';
                    html += '</div>';
                    html += '</div>';

                    $('.order_product').html(html);
                },
                error: function(e) {
                    console.log(e);
                    var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                    $('.order_product').html(html);
                }
            });
        } else if (type_product ==  'Server') {
            $.ajax({
                type: "get",
                url: "/admin/orders/list_product",
                data: {action: 'Server'},
                dataType: "json",
                beforeSend: function(){
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('.order_product').html(html);
                },
                success: function (data) {
                    var html = '';
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '<label for="type_products">Sản phẩm</label>';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right">';
                    html += '<select name="product_id" id="product_id" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Server">';
                    html += '<option disabled selected>Chọn sản phẩm</option>';
                    if (data.product != '') {
                        $.each(data.product, function (index, product) { 
                            html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                        });   
                    } else {
                        html += '<option disabled class="text-danger">Không có sản phẩm Server</option>';
                    }
                    html += '</select>';
                    html += '</div>';
                    html += '</div>';
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '<label for="billing_cycle">Thời gian</label>';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right">';
                    html += '<select name="billing_cycle" id="billing_cycle" class="select2 form-control"  style="width: 100%;">';
                    $.each(data.billing, function (index2, billing) { 
                        html += '<option value="'+ index2 +'">'+ billing +'</option>';
                    });
                    html += '</select>';
                    html += '</div>';
                    html += '</div>';
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '<label for="quantity">Số lượng</label>';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right">';
                    html += '<input id="quantity" name="quantity" class="form-control" type="text" placeholder="Số lượng" value="1">';
                    html += '</div>';
                    html += '</div>';
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '<label for="os">Hệ điều hành</label>';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right">';
                    html += '<input id="os" name="os" class="form-control" type="text" placeholder="Hệ điều hành" value="">';
                    html += '</div>';
                    html += '</div>';
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '<label for="invoices">Thanh toán</label>';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right">';
                    html += '<select name="invoices" id="invoices" class="select2 form-control"  style="width: 100%;">';
                    $.each(data.invoices, function (index_invoices, invoice) { 
                        html += '<option value="'+ index_invoices +'">'+ invoice +'</option>';
                    });
                    html += '</select>';
                    html += '</div>';
                    html += '</div>';
                    
                    $('.order_product').html(html);
                },
                error: function(e) {
                    console.log(e);
                    var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                    $('.order_product').html(html);
                }
            });     
        } else if (type_product ==  'VPS') {
            $.ajax({
                type: "get",
                url: "/admin/orders/list_product",
                data: {action: 'VPS'},
                dataType: "json",
                beforeSend: function(){
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('.order_product').html(html);
                },
                success: function (data) {
                    var html = '';
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '<label for="type_products">Sản phẩm</label>';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right">';
                    html += '<select name="product_id" id="product_id" data-value="Vps" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;">';
                    html += '<option disabled selected>Chọn sản phẩm</option>';
                    if (data.product != null) {
                        $.each(data.product, function (index, product) { 
                            html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                        });   
                    } else {
                        html += '<option disabled class="text-danger">Không có sản phẩm VPS</option>';
                    }
                    html += '</select>';
                    html += '</div>';
                    html += '</div>';
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right">';
                    html += '<button class="button_vps btn btn-default"><i class="far fa-plus-square"></i> Thêm cấu hình</button>';
                    html += '</div>';
                    html += '</div>';
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '<label for="billing_cycle">Thời gian</label>';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right">';
                    html += '<select name="billing_cycle" id="billing_cycle" class="select2 form-control"  style="width: 100%;">';
                    $.each(data.billing, function (index2, billing) { 
                        html += '<option value="'+ index2 +'">'+ billing +'</option>';
                    });
                    html += '</select>';
                    html += '</div>';
                    html += '</div>';
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '<label for="quantity">Số lượng</label>';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right">';
                    html += '<input id="quantity" name="quantity" class="form-control" type="text" placeholder="Số lượng" value="1">';
                    html += '</div>';
                    html += '</div>';
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '<label for="os">Hệ điều hành</label>';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right">';
                    html += '<input id="os" name="os" class="form-control" type="text" placeholder="Hệ điều hành" value="">';
                    html += '</div>';
                    html += '</div>';
                    // 1 form-group
                    html += '<div class="row form-group">';
                    html += '<div class="col col-md-3 text-right ">';
                    html += '<label for="invoices">Thanh toán</label>';
                    html += '</div>';
                    html += '<div class="col col-md-8 order-right">';
                    html += '<select name="invoices" id="invoices" class="select2 form-control"  style="width: 100%;">';
                    $.each(data.invoices, function (index_invoices, invoice) { 
                        html += '<option value="'+ index_invoices +'">'+ invoice +'</option>';
                    });
                    html += '</select>';
                    html += '</div>';
                    html += '</div>';

                    $('.order_product').html(html);
                },
                error: function(e) {
                    console.log(e);
                    var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                    $('.order_product').html(html);
                }
            });     
        }
    });
    $(document).on('change', '#product_id', function() {
        var id = $(this).val();
        var type_products = $('#type_products').val();
        if (type_products == 'Hosting') {
            var qtt = $('#quantity_hosting').val();
        } else {
            var qtt = $('#quantity').val();
        }
        var billing = $('#billing_cycle').val();
        $.ajax({
            type: "get",
            url: "/admin/orders/get_product",
            data: { id: id },
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.ordersummary_product').html(html);
            },
            success: function (data) {
                var html = '';
                if(data.pricing.type) {
                    if(data.pricing.type == 'free') {
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">0 VNĐ</div>';
                            html += '</div></div>';
                        }
                        $('#form_total').val(0);
                        $('#form_sub_total').val(0);
                    } else if(data.pricing.type == 'recurring') {
                        var price = 0;
                        var sub_total = 0;
                        var total = 0;
                        $.each(data.pricing, function (index_pricing, pricing) { 
                            if(index_pricing == billing) {
                                if (pricing == 0) {
                                    if(data.pricing.monthly > 0) {
                                        price = data.pricing.monthly;
                                        billing = 'monthly';
                                    } 
                                    else if (data.pricing.quarterly > 0) {
                                        price = data.pricing.quarterly;
                                        billing = 'quarterly';
                                    }
                                    else if (data.pricing.semi_annually > 0) {
                                        price = data.pricing.semi_annually;
                                        billing = 'semi_annually';
                                    }
                                    else if (data.pricing.annually > 0) {
                                        price = data.pricing.annually;
                                        billing = 'annually';
                                    }
                                    else if (data.pricing.biennially > 0) {
                                        price = data.pricing.biennially;
                                        billing = 'biennially';
                                    } else {
                                        price = data.pricing.triennially;
                                        billing = 'triennially';
                                    }
                                } else {
                                    price = pricing;
                                }
                            }
                        });
                        for (let i = 0; i < qtt; i++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ price +' VNĐ</div>';
                            html += '</div></div>';
                        }
                        total = price * qtt;
                        sub_total = price * qtt;
                        $('#form_total').val(total);
                        $('#form_sub_total').val(sub_total);
                        $('.sub_total_pricing').html( sub_total + ' VNĐ');
                        $('.total_pricing').html( total + ' VNĐ');
                    } else {
                        var sub_total = 0;
                        var total = 0;
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">Vĩnh viễn</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ data.pricing.one_time_pay +' VNĐ</div>';
                            html += '</div></div>';
                        }
                        total = data.pricing.one_time_pay * qtt;
                        sub_total = data.pricing.one_time_pay * qtt;
                        $('#form_total').val(total);
                        $('#form_sub_total').val(sub_total);
                        $('.sub_total_pricing').html( sub_total + ' VNĐ');
                        $('.total_pricing').html( total + ' VNĐ');
                    }
                    $('.ordersummary_product').html(html);
                } 
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

    $(document).on('change', '#billing_cycle', function() {
        var id = $('#product_id').val();
        var type_products = $('#type_products').val();
        if (type_products == 'Hosting') {
            var qtt = $('#quantity_hosting').val();
        } else {
            var qtt = $('#quantity').val();
        }
        var billing = $(this).val();
        $.ajax({
            type: "get",
            url: "/admin/orders/get_product",
            data: { id: id },
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.ordersummary_product').html(html);
            },
            success: function (data) {
                var html = '';
                if(data.pricing.type) {
                    if(data.pricing.type == 'free') {
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">0 VNĐ</div>';
                            html += '</div></div>';
                        }
                        $('#form_total').val(0);
                        $('#form_sub_total').val(0);
                    } else if(data.pricing.type == 'recurring') {
                        var price = 0;
                        var sub_total = 0;
                        var total = 0;
                        $.each(data.pricing, function (index_pricing, pricing) { 
                            if(index_pricing == billing) {
                                if (pricing == 0) {
                                    if(data.pricing.monthly > 0) {
                                        price = data.pricing.monthly;
                                        billing = 'monthly';
                                    } 
                                    else if (data.pricing.quarterly > 0) {
                                        price = data.pricing.quarterly;
                                        billing = 'quarterly';
                                    }
                                    else if (data.pricing.semi_annually > 0) {
                                        price = data.pricing.semi_annually;
                                        billing = 'semi_annually';
                                    }
                                    else if (data.pricing.annually > 0) {
                                        price = data.pricing.annually;
                                        billing = 'annually';
                                    }
                                    else if (data.pricing.biennially > 0) {
                                        price = data.pricing.biennially;
                                        billing = 'biennially';
                                    } else {
                                        price = data.pricing.triennially;
                                        billing = 'triennially';
                                    }
                                } else {
                                    price = pricing;
                                }
                            }
                        });
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ price +' VNĐ</div>';
                            html += '</div></div>';
                        }
                        total = price * qtt;
                        sub_total = price * qtt;
                        $('#form_total').val(total);
                        $('#form_sub_total').val(sub_total);
                        $('.sub_total_pricing').html( sub_total + ' VNĐ');
                        $('.total_pricing').html( total + ' VNĐ');
                    } else {
                        var sub_total = 0;
                        var total = 0;
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">Vĩnh viễn</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ data.pricing.one_time_pay +' VNĐ</div>';
                            html += '</div></div>';
                        }
                        total = data.pricing.one_time_pay * qtt;
                        sub_total = data.pricing.one_time_pay * qtt;
                        $('#form_total').val(total);
                        $('#form_sub_total').val(sub_total);
                        $('.sub_total_pricing').html( sub_total + ' VNĐ');
                        $('.total_pricing').html( total + ' VNĐ');
                    }
                    $('.ordersummary_product').html(html);
                } 
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

    $(document).on('keyup', '#quantity', function() {
        var id = $('#product_id').val();
        var type_products = $('#type_products').val();
        if (type_products == 'Hosting') {
            var qtt = $('#quantity_hosting').val();
        } else {
            var qtt = $('#quantity').val();
        }
        var billing = $('#billing_cycle').val();
        $.ajax({
            type: "get",
            url: "/admin/orders/get_product",
            data: { id: id },
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.ordersummary_product').html(html);
            },
            success: function (data) {
                var html = '';
                if(data.pricing.type) {
                    if(data.pricing.type == 'free') {
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">0 VNĐ</div>';
                            html += '</div></div>';
                        }
                        $('#form_total').val(0);
                        $('#form_sub_total').val(0);
                    } else if(data.pricing.type == 'recurring') {
                        var price = 0;
                        var sub_total = 0;
                        var total = 0;
                        $.each(data.pricing, function (index_pricing, pricing) { 
                            if(index_pricing == billing) {
                                if (pricing == 0) {
                                    if(data.pricing.monthly > 0) {
                                        price = data.pricing.monthly;
                                        billing = 'monthly';
                                    } 
                                    else if (data.pricing.quarterly > 0) {
                                        price = data.pricing.quarterly;
                                        billing = 'quarterly';
                                    }
                                    else if (data.pricing.semi_annually > 0) {
                                        price = data.pricing.semi_annually;
                                        billing = 'semi_annually';
                                    }
                                    else if (data.pricing.annually > 0) {
                                        price = data.pricing.annually;
                                        billing = 'annually';
                                    }
                                    else if (data.pricing.biennially > 0) {
                                        price = data.pricing.biennially;
                                        billing = 'biennially';
                                    } else {
                                        price = data.pricing.triennially;
                                        billing = 'triennially';
                                    }
                                } else {
                                    price = pricing;
                                }
                            }
                        });
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ price +' VNĐ</div>';
                            html += '</div></div>';
                        }
                        total = price * qtt;
                        sub_total = price * qtt;
                        $('#form_total').val(total);
                        $('#form_sub_total').val(sub_total);
                        $('.sub_total_pricing').html( sub_total + ' VNĐ');
                        $('.total_pricing').html( total + ' VNĐ');
                    } else {
                        var sub_total = 0;
                        var total = 0;
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">Vĩnh viễn</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ data.pricing.one_time_pay +' VNĐ</div>';
                            html += '</div></div>';
                        }
                        total = data.pricing.one_time_pay * qtt;
                        sub_total = data.pricing.one_time_pay * qtt;
                        $('#form_total').val(total);
                        $('#form_sub_total').val(sub_total);
                        $('.sub_total_pricing').html( sub_total + ' VNĐ');
                        $('.total_pricing').html( total + ' VNĐ');
                    }
                    $('.ordersummary_product').html(html);
                } 
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

    $(document).on('keyup', '#quantity_hosting', function() {
        var id = $('#product_id').val();
        var qtt = $('#quantity_hosting').val();
        var billing = $('#billing_cycle').val();
        $.ajax({
            type: "get",
            url: "/admin/orders/get_product",
            data: { id: id },
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.ordersummary_product').html(html);
            },
            success: function (data) {
                var html = '';
                var html_domain = '';
                if(data.pricing.type) {
                    if(data.pricing.type == 'free') {
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">0 VNĐ</div>';
                            html += '</div></div>';
                        }
                        $('#form_total').val(0);
                        $('#form_sub_total').val(0);
                    } else if(data.pricing.type == 'recurring') {
                        var price = 0;
                        var sub_total = 0;
                        var total = 0;
                        $.each(data.pricing, function (index_pricing, pricing) { 
                            if(index_pricing == billing) {
                                if (pricing == 0) {
                                    if(data.pricing.monthly > 0) {
                                        price = data.pricing.monthly;
                                        billing = 'monthly';
                                    } 
                                    else if (data.pricing.quarterly > 0) {
                                        price = data.pricing.quarterly;
                                        billing = 'quarterly';
                                    }
                                    else if (data.pricing.semi_annually > 0) {
                                        price = data.pricing.semi_annually;
                                        billing = 'semi_annually';
                                    }
                                    else if (data.pricing.annually > 0) {
                                        price = data.pricing.annually;
                                        billing = 'annually';
                                    }
                                    else if (data.pricing.biennially > 0) {
                                        price = data.pricing.biennially;
                                        billing = 'biennially';
                                    } else {
                                        price = data.pricing.triennially;
                                        billing = 'triennially';
                                    }
                                } else {
                                    price = pricing;
                                }
                            }
                        });
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ price +' VNĐ</div>';
                            html += '</div></div>';
                            // them the input de nhap domain
                            var n = index;
                            if (n > 0) {
                                html_domain += '<input name="domain[]" class="form-control" type="text" placeholder="Domain '+ (n + 1) +'">';
                            }
                        }
                        total = price * qtt;
                        sub_total = price * qtt;
                        $('#form_total').val(total);
                        $('#form_sub_total').val(sub_total);
                        $('.sub_total_pricing').html( sub_total + ' VNĐ');
                        $('.total_pricing').html( total + ' VNĐ');
                    } else {
                        var sub_total = 0;
                        var total = 0;
                        for (let index = 0; index < qtt; index++) {
                            
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">Vĩnh viễn</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ data.pricing.one_time_pay +' VNĐ</div>';
                            html += '</div></div>';
                        }
                        total = data.pricing.one_time_pay * qtt;
                        sub_total = data.pricing.one_time_pay * qtt;
                        $('#form_total').val(total);
                        $('#form_sub_total').val(sub_total);
                        $('.sub_total_pricing').html( sub_total + ' VNĐ');
                        $('.total_pricing').html( total + ' VNĐ');
                    }
                    $('.ordersummary_product').html(html);
                    $('#domain').attr('name', 'domain[]');
                    $('.domain').append(html_domain);
                } 
            },
            error: function(e) {
                console.log(e);
            }
        });
    });

    
    // page edit
    var id = $('#product_id').val();
    var billing = $('#billing_cycle').val();
    var type_products = $('#type_products').val();
    if (type_products == 'Hosting') {
        var qtt = $('#quantity_hosting').val();
    } else {
        var qtt = $('#quantity').val();
    }
    console.log(type_products,id, qtt, billing);
    edit(id, qtt, billing);
    function edit(id, qtt, billing) {
        $.ajax({
            type: "get",
            url: "/admin/orders/get_product",
            data: { id: id },
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.ordersummary_product').html(html);
            },
            success: function (data) {
                var html = '';
                var html_domain = '';
                if(data.pricing.type) {
                    if(data.pricing.type == 'free') {
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">0 VNĐ</div>';
                            html += '</div></div>';
                        }
                    } else if(data.pricing.type == 'recurring') {
                        var price = 0;
                        var sub_total = 0;
                        var total = 0;
                        $.each(data.pricing, function (index_pricing, pricing) { 
                            if(index_pricing == billing) {
                                if (pricing == 0) {
                                    if(data.pricing.monthly > 0) {
                                        price = data.pricing.monthly;
                                        billing = 'monthly';
                                    } 
                                    else if (data.pricing.quarterly > 0) {
                                        price = data.pricing.quarterly;
                                        billing = 'quarterly';
                                    }
                                    else if (data.pricing.semi_annually > 0) {
                                        price = data.pricing.semi_annually;
                                        billing = 'semi_annually';
                                    }
                                    else if (data.pricing.annually > 0) {
                                        price = data.pricing.annually;
                                        billing = 'annually';
                                    }
                                    else if (data.pricing.biennially > 0) {
                                        price = data.pricing.biennially;
                                        billing = 'biennially';
                                    } else {
                                        price = data.pricing.triennially;
                                        billing = 'triennially';
                                    }
                                } else {
                                    price = pricing;
                                }
                            }
                        });
                        for (let index = 0; index < qtt; index++) {
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">'+data.billing[billing]+'</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ price +' VNĐ</div>';
                            html += '</div></div>';
                            // them the input de nhap domain
                            var n = index;
                            if (n > 0) {
                                html_domain += '<input name="domain[]" class="form-control" type="text" placeholder="Domain '+ (n + 1) +'">';
                            }
                        }
                    } else {
                        var sub_total = 0;
                        var total = 0;
                        for (let index = 0; index < qtt; index++) {
                            
                            html += '<div class="item">';
                            html += '<div class="itemtitle">'+ data.group_product.name + ' - ' + data.product.name +'</div>';
                            html += '<div class="row">';
                            html += '<div class="col col-md-6 item_billing">Vĩnh viễn</div>';
                            html += '<div class="col col-md-6 item_pricing text-right">'+ data.pricing.one_time_pay +' VNĐ</div>';
                            html += '</div></div>';
                        }
                    }
                    $('.ordersummary_product').html(html);
                } 
            },
            error: function(e) {
                console.log(e);
            }
        });
    }
});