$(document).ready(function() {
    check_amount_pavietnam();

    function check_amount_pavietnam() {
        $('.alert-pavietnam').hide();
        $.ajax({
            url: '/admin/domain/check-account-still',
            type: 'get',
            dataType: 'json',
            success: function(data) {
                if (data.status == true) {
                    var money = Number(data.money.replace(/\,/g, ''));
                    if (money < 750000) {
                        $('.alert-pavietnam').show();
                        $('.show-money-pavietnam').text(data.money);
                    }
                }
            }
        });
    }
});