$(document).ready(function () {

    $('#test').on('click', function () {
        var host = $('#host').val();
        var post = $('#post').val();
        var user_name = $('#user_name').val();
        var password = $('#password').val();
        var type_server_hosting = $('#type_server_hosting').val();
        $.ajax({
            url: '/admin/server_hosting/check_connection',
            typeData: 'json',
            data: { host: host, post: post, user_name: user_name, password: password, type: type_server_hosting },
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#notication-test').html(html);
                $('#test').attr('disabled', true);
            },
            success: function (data) {
                console.log(data);
                if (data == 1) {
                  $('#notication-test').html('<span class="text-danger">Kết nối không thành công.</span>');
                  $('#test').attr('disabled', false);
                } else {
                  $('#notication-test').html('<span class="text-success">Kết nối thành công.</span>');
                  $('#test').attr('disabled', false);
                }
            },
            error: function (e) {
                console.log(e);
                $('#notication-test').html('<span class="text-danger">Truy vấn server hosting lỗi!</span>');
                $('#test').attr('disabled', false);
            }
        })
    })

});
