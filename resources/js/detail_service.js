$(document).ready(function() {
    const Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });

    $('#button_terminated').on('click', function (event) {
        event.preventDefault();
        var id = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        var domain = $(this).attr('data-domain');
        // var token = $('meta[name="csrf-token"]').attr('content');
        $('#terminated-services').modal('show');
        $('#button-terminated').attr('data-type', type);
        $('#button-terminated').attr('data-id', id);
        if (type == 'hosting') {
            $('#notication-invoice').html('Bạn có muốn hủy dịch vụ Hosting (Domain: <b class="text-danger">'+domain+'</b>) không?');
        }
    });

    $('#button-terminated').on('click', function () {
        var id = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        // console.log(type);
        var token = $('meta[name="csrf-token"]').attr('content');
        $.ajax({
            url: '/service/cancel_sevices',
            type: 'post',
            data: {'_token': token, id: id, type: type },
            dataType: 'json',
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-terminated').attr('disabled', true);
                $('#notication-invoice').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data) {
                    $(document).Toasts('create', {
                        class: 'bg-info',
                        title: 'Hủy dịch vụ',
                        subtitle: '',
                        body: "Hủy dịch vụ thành công.",
                    })
                    $('#button_terminated').fadeOut();
                    location.reload();
                } else {
                    $('#notication-invoice').html('<span class="text-danger">VPS này không có hoặc không thuộc quyền quản lý của quý khách!</span>');
                }
                $('#terminated-services').modal('hide');
            },
            error: function (e) {
                console.log(e);
                $('#button-terminated').attr('disabled', false);
                $('#notication-invoice').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
            }
        });
    });

    $('.button_add_config').on('click', 'button', function (event) {
        event.preventDefault();
        $('html,body').animate({ scrollTop: $('#card_config').offset().top - 50 }, 500);
        var billing = 'monthly';
        $.ajax({
            url: '/loadAddon',
            data: {type: 'addon-vps'},
            dataType: 'json',
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#card_config').html(html);
            },
            success: function (data) {
                // console.log(data);
                var html = '';
                if (data != '' || data != null) {
                    html += '<div class="card-header">'
                    html += '<h3 class="card-title">Thêm cấu hình</h3>';
                    html += '</div>';
                    html += '<div class="card-body row mb-4">';
                    html += '<div class="form-group addon-product text-center mt-4 mb-4">';
                    html += '<div class="notication">';
                    html += '</div>';
                    html += '<form id="addon_upgrade">';
                    html += '<input name="addon" value="1" id="addon-hidden" type="hidden">';
                    html += '<label for="addon" class="text-danger mb-4">Chọn gói Addon</label>';
                    html += '<div class="row">';
                    $.each(data, function(index, product) {
                        var type = '';
                        var label = '';
                        // console.log(product, product.meta_product.type_addon);
                        if (product.meta_product.type_addon == 'addon_cpu') {
                            type = 'cpu';
                            label = 'CPU';
                            html += '<input name="id-addon-cpu" value="'+product.id+'" id="addon-hidden" type="hidden">';
                        }
                        if (product.meta_product.type_addon == 'addon_ram') {
                            type = 'ram';
                            label = 'Ram';
                            html += '<input name="id-addon-ram" value="'+product.id+'" id="addon-hidden" type="hidden">';
                        }
                        if (product.meta_product.type_addon == 'addon_disk') {
                            type = 'disk';
                            label = 'Disk';
                            html += '<input name="id-addon-disk" value="'+product.id+'" id="addon-hidden" type="hidden">';
                        }
                        // if (product.meta_product.type_addon != '') {
                        //     type = 'storage';
                        //     label = 'Storage';
                        //       html += '<input name="id-addon-storage" value="'+product.id+'" id="addon-hidden" type="hidden">';
                        // }
                        // if (product.meta_product.database != '') {
                        //     type = 'database';
                        //     label = 'Database';
                        //       html += '<input name="id-addon-database" value="'+product.id+'" id="addon-hidden" type="hidden">';
                        // }
                        if (type == 'disk') {
                            html += '<div class="col-md-4 text-center addon-'+type+'">';
                            html += '<label>' + label + ' ('+ addCommas(product.pricing[billing]) + ' VNĐ / Tháng)' + '</lable> <br>';
                            html += '<input name="addon_'+type+'" type="number" value="0" min="0" max="200" step="10" data-name="'+ product.name +'" data-pricing="'+ product.pricing[billing] +'" />';
                            html += '</div>';
                        } else {
                            html += '<div class="col-md-4 text-center addon-'+type+'">';
                            html += '<label>' + label + ' ('+ addCommas(product.pricing[billing]) + ' VNĐ / Tháng)' + '</lable> <br>';
                            html += '<input name="addon_'+type+'" type="number" value="0" min="0" max="16" step="1" data-name="'+ product.name +'" data-pricing="'+ product.pricing[billing] +'" />';
                            html += '</div>';
                        }
                    });
                    html += '</div>';
                    html += '<div class="amount mt-4 text-center">';
                    html += '</div>';
                    html += '<div class="col-md-12 mt-4 text-center">';
                    html += '<button class="btn btn-info" id="upgrade">Nâng cấp</button>';
                    html += '</div>';
                    html += '</form>';
                    html += '</div>';
                    html += '</div>';
                    $('#card_config').html(html);
                    $('.select3').select2();
                    $("input[type='number']").inputSpinner()
                } else {
                    $('#card_config').html('Không có gói Addon Product cho gói sản phẩm này.');
                }
            },
            error: function (e) {
                console.log(e);
                $('#card_config').html('Truy vấn Addon Product thất bại.');
             }
        });
    })

    // // khi click vào chọn gói addon sẽ tăng set lại sub_total
    // $(document).on('click', '.addon-cpu' , function() {
    //     // console.log('da click');
    //     var qtt_addon = parseInt($(".addon-cpu input").val());
    //     var pricing = parseInt($(".addon-cpu input").attr('data-pricing'));
    //     // lay cac addon con lai
    //     var qtt_addon_ram = parseInt($(".addon-ram input").val());
    //     var pricing_ram = parseInt($(".addon-ram input").attr('data-pricing'));
    //     var qtt_addon_disk = parseInt($(".addon-disk input").val());
    //     var pricing_disk = parseInt($(".addon-disk input").attr('data-pricing'));
    //     total =   (qtt_addon * pricing) + (qtt_addon_ram * pricing_ram) + (qtt_addon_disk * pricing_disk/10);
    //     var html = '';
    //     html += '<div class="row mt-4 mb-4">';
    //     html += '<div class="col-md-6 text-right">';
    //     html += '<b>Thành tiền</b>';
    //     html += '</div>';
    //     html += '<div class="col-md-6 text-left">';
    //     html +=  ;
    //     html += '</div>';
    //     html += '</div>';
    //     $('.amount').html(html);
    //     // console.log(total);
    // });
    // // khi click vào chọn gói addon sẽ tăng set lại sub_total
    // $(document).on('click', '.addon-ram' , function() {
    //     var qtt_addon = parseInt($('.addon-ram input').val());
    //     var pricing = parseInt($('.addon-ram input').attr('data-pricing'));
    //     // lay cac addon con load
    //     var qtt_addon_cpu = parseInt($(".addon-cpu input").val());
    //     var pricing_cpu = parseInt($(".addon-cpu input").attr('data-pricing'));
    //     var qtt_addon_disk = parseInt($(".addon-disk input").val());
    //     var pricing_disk = parseInt($(".addon-disk input").attr('data-pricing'));
    //
    //     total =  (qtt_addon * pricing )  + (qtt_addon_disk * pricing_disk/10 )  + (qtt_addon_cpu * pricing_cpu );
    //     var html = '';
    //     html += '<div class="row mt-4 mb-4">';
    //     html += '<div class="col-md-6 text-right">';
    //     html += '<b>Thành tiền</b>';
    //     html += '</div>';
    //     html += '<div class="col-md-6 text-left">';
    //     html +=  addCommas(total) + ' VNĐ';
    //     html += '</div>';
    //     html += '</div>';
    //     $('.amount').html(html);
    // });
    // // khi click vào chọn gói addon sẽ tăng set lại sub_total
    // $(document).on('click', '.addon-disk' , function() {
    //     var qtt_addon = parseInt($('.addon-disk input').val()) / 10;
    //     var pricing = parseInt($('.addon-disk input').attr('data-pricing'));
    //     // lay cac addon con lai
    //     var qtt_addon_cpu = parseInt($(".addon-cpu input").val());
    //     var pricing_cpu = parseInt($(".addon-cpu input").attr('data-pricing'));
    //     var qtt_addon_ram = parseInt($(".addon-ram input").val());
    //     var pricing_ram = parseInt($(".addon-ram input").attr('data-pricing'));
    //
    //     total =  (qtt_addon * pricing ) + (qtt_addon_cpu * pricing_cpu ) + (qtt_addon_ram * pricing_ram );
    //     var html = '';
    //     html += '<div class="row mt-4 mb-4">';
    //     html += '<div class="col-md-6 text-right">';
    //     html += '<b>Thành tiền</b>';
    //     html += '</div>';
    //     html += '<div class="col-md-6 text-left">';
    //     html +=  addCommas(total) + ' VNĐ';
    //     html += '</div>';
    //     html += '</div>';
    //     $('.amount').html(html);
    //     // console.log(total);
    // });

    $(document).on('click', '#upgrade' ,function (event) {
        event.preventDefault();
        var form = $("#addon_upgrade").serialize();
        var billing = $('#billing').val();
        var token = $('meta[name="csrf-token"]').attr('content');
        // console.log(form);
        var vps_id = $('#id').val();
        form += '&_token=' + token + '&vps_id=' + vps_id + '&billing=' + billing;
        // console.log(form);
        $.ajax({
            url: '/services/upgrade_vps',
            type: 'post',
            dataType: 'json',
            data: form,
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.notication').html(html);
                $('#addon_upgrade').css('opacity', 0.5);
                $('#addon_upgrade').attr('disabled', true);
                $('#upgrade').attr('disabled', true);
            },
            success: function (data) {
              // console.log(data);
              if (data != '') {
                  $('#card_config .addon-product').html('<span class="text-danger">Yêu cầu nâng cấp VPS thành công.Quý khách vui lòng bấm <a href="/order/check-invoices/'+data+'">vào đây</a> thanh toán để hoàn thành quá trình nâng cấp VPS. </span>');
                  $('#upgrade').attr('disabled', false);
              } else {
                  $('.notication').html('<span class="text-danger">Nâng cấp VPS thất bại.Quý khách vui lòng kiểm tra lại cấu hình.</span>');
                  $('#addon_upgrade').attr('disabled', false);
                  $('#upgrade').attr('disabled', false);
                  $('#addon_upgrade').css('opacity', 1);
              }
            },
            error: function (e) {
                console.log(e);
                $('.notication').html('<span class="text-danger">Truy vấn Addon Product lỗi.</span>');
                $('#addon_upgrade').attr('disabled', false);
                $('#upgrade').attr('disabled', false);
                $('#addon_upgrade').css('opacity', 1);
            }
        });
    })

    $('.button_upgrade').on('click', function (event) {
        event.preventDefault();
        $('html,body').animate({ scrollTop: $('#card_config').offset().top - 50 }, 500);
        var id = $(this).attr('data-id');
        var hosting_id = $(this).attr('data-hosting');
        var billing = $(this).attr('data-billing');
        var time = '';
        if (billing == 'monthly') {
          time = '1 Tháng';
        }
        else if (billing == 'twomonthly') {
          time = '2 Tháng';
        }
        else if (billing == 'twomonthly') {
          time = '2 Tháng';
        }
        else if (billing == 'quarterly') {
          time = '3 Tháng';
        }
        else if (billing == 'semi_annually') {
          time = '6 Tháng';
        }
        else if (billing == 'annually') {
          time = '1 Năm';
        }
        else if (billing == 'biennially') {
          time = '2 Năm';
        }
        else if (billing == 'triennially') {
          time = '3 Năm';
        }
        else if (billing == 'one_time_pay') {
          time = 'Vĩnh viễn';
        }
        else {
          time = 'Miễn phí';
        }
        $.ajax({
          url: '/services/check-upgrade/' + id,
          dataType: 'json',
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#card_config').html(html);
          },
          success: function (data) {
             // console.log(data);
             var html = '';
             if (data != '') {
               html += '<div class="card-header">'
               html += '<h3 class="card-title">Nâng cấp Hosting</h3>';
               html += '</div>';
               html += '<div class="card-body row mb-4">';
               html += '<div class="form-group addon-product text-center mt-4 mb-40">';
               html += '<div class="notication">';
               html += '</div>';
               html += '<form id="upgrade_hosting">';
               html += '<input name="hosting_id" value="' + hosting_id + '" id="hosting_id" type="hidden">';
               html += '<label for="upgrate_hosting" class="text-danger mb-4">Chọn gói sản phẩm / dịch vụ</label>';
               html += '<div class="form-group" id="select_product_form" multiple data-placeholder="Chọn gói sản phẩm / dịch vụ" style="width:70%;margin:auto;">';
               html += '<select class="form-control select2" name="product_upgrade_id">';
               $.each(data , function (index, upgrade) {
                 html += '<option value="' + upgrade.product.id + '">'+ upgrade.product.name + ' - ' + addCommas( upgrade.pricing[billing] ) +' VNĐ / '+time+' </option>';
               });
               html += '</select>';
               html += '</div>';
               html += '</form>';
               html += '<div class="col-md-12 mt-4 text-center">';
               html += '<button class="btn btn-info" id="upgrade_hosting_submit">Nâng cấp</button>';
               html += '</div>';
               html += '</div>';
               html += '</div>';
             } else {
               html += '<div class="text-center text-danger mt-4 mb-4">Không có gói nâng cấp sản phẩm Hosting này.</div>';
             }
             $('#card_config').html(html);
             $('.select2').select2();
          },
          error: function (e) {
              console.log(e);
              $('#card_config').html('<div class="text-center text-danger mt-4 mb-4">Truy vấn nâng cấp Hosting thất bại.</div>');
          }
        });
    })

    $(document).on('click', '#upgrade_hosting_submit' , function (event) {
        event.preventDefault();
        var form = $("#upgrade_hosting").serialize();
        var token = $('meta[name="csrf-token"]').attr('content');
        form += '&_token=' + token;
        $.ajax({
            url: '/services/upgrade_hosting',
            type: 'post',
            dataType: 'json',
            data: form,
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.notication').html(html);
                $('#upgrade_hosting').css('opacity', 0.5);
                $('#upgrade_hosting').attr('disabled', true);
                $('#upgrade_hosting_submit').attr('disabled', true);
            },
            success: function (data) {
                console.log(data);
                if (data != '') {
                    $('#card_config .addon-product').html('<span class="text-danger">Yêu cầu nâng cấp Hosting thành công.Quý khách vui lòng bấm <a href="/order/check-invoices/'+data+'">vào đây</a> thanh toán để hoàn thành quá trình nâng cấp Hosting. </span>');
                    $('#upgrade_hosting_submit').attr('disabled', false);
                } else {
                    $('.notication').html('<span class="text-danger">Yêu cầu nâng cấp Hosting thất bại. Quý khách vui lòng kiểm tra lại thông tin nâng cấp.</span>');
                    $('#upgrade_hosting_submit').attr('disabled', false);
                    $('#upgrade_hosting').attr('disabled', false);
                    $('#upgrade_hosting').css('opacity', 1);
                }
            },
            error: function (e) {
              console.log(e);
              $('.notication').html('<span class="text-danger">Truy vấn nâng cấp Hosting lỗi.</span>');
              $('#upgrade_hosting').attr('disabled', false);
              $('#upgrade_hosting_submit').attr('disabled', false);
              $('#upgrade_hosting').css('opacity', 1);
            }
        })
    })

    $('.button_service_cancel').on('click', function (event) {
        event.preventDefault();
        var id = $(this).attr('data-id');
        var type = $(this).attr('data-type');
        var name = $(this).attr('data-name');
        if (type == 'vps') {
          $('#terminated_services').modal('show');
          $('.modal-title').text('Hủy dịch vụ VPS');
          $('#notication-invoice').html('<span class="text-danger">Bạn có muốn hủy dịch vụ VPS <b>'+ name +'</b> này không?</span>');
          $('#button-terminated').attr('data-id', id);
          $('#button-terminated').attr('data-type', type);
        }
        else if ( type == 'proxy' ) {
          $('#terminated_services').modal('show');
          $('.modal-title').text('Hủy dịch vụ Proxy');
          $('#notication-invoice').html('<span class="text-danger">Bạn có muốn hủy dịch vụ Proxy <b>'+ name +'</b> này không?</span>');
          $('#button-terminated').attr('data-id', id);
          $('#button-terminated').attr('data-type', type);
        }
        else if ( type == 'server' ) {
          $('#terminated_services').modal('show');
          $('.modal-title').text('Hủy dịch vụ Server');
          $('#notication-invoice').html('<span class="text-danger">Bạn có muốn hủy dịch vụ Server <b>'+ name +'</b> này không?</span>');
          $('#button-terminated').attr('data-id', id);
          $('#button-terminated').attr('data-type', type);
        }
    })

    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

});
