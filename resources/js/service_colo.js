$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    // khởi tạo modal Toast
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000
    });
    var $vpsCheckbox = $('.checkbox');
    var lastChecked = null;

    $(document).on('click', '.checkbox_all', function() {
        if ($(this).is(':checked')) {
            $('.checkbox').prop('checked', this.checked);
            $('tbody tr').addClass('action');
        } else {
            $('.checkbox').prop('checked', this.checked);
            $('tbody tr').removeClass('action');
        }
        var checked = $('.checkbox:checked');
        $('.qtt-checkbox').text( checked.length );
    })

    // Lưu các checkbox vps
    $(document).on('click', '.checkbox', function(e) {
        var checked = $('.checkbox:checked');
        $('.qtt-checkbox').text( checked.length );
        if ($(this).is(':checked')) {
            $(this).closest('tr').addClass('action');
        } else {
            $(this).closest('tr').removeClass('action');
        }
        if (!lastChecked) {
            lastChecked = this;
            return;
        }
        if (e.shiftKey) {
            // console.log('da click 2');
            var start = $vpsCheckbox.index(this);
            var end = $vpsCheckbox.index(lastChecked);
            $vpsCheckbox.slice(Math.min(start, end), Math.max(start, end) + 1).prop('checked', lastChecked.checked);
            $.each($vpsCheckbox, function(index, value) {
                if ($(this).is(':checked')) {
                    $(this).closest('tr').addClass('action');
                } else {
                    $(this).closest('tr').removeClass('action');
                }
            })
        }
        lastChecked = this;
        var checked = $('.checkbox:checked');
        $('.qtt-checkbox').text( checked.length );
    });
    // Click vào các button action của Vps
    $('.list_action').on('change', function() {
        var action = $('.list_action').val();
        if (action == "") {
            alert('Vui lòng chọn hành động.');
        } else if (action == 'change_user') {
            var checked = $('.vps_checkbox:checked');
            var list_vps = [];
            if (checked.length > 0) {
                $.each(checked, function(index, value) {
                    list_vps.push($(this).val());
                })
                window.location.href = '/dich-vu/vps/chuyen-doi-khach-hang?list_vps=' + list_vps;
                // console.log('/dich-vu/vps/chuyen-doi-khach-hang?list_vps=' + list_vps);
            } else {
                alert('Vui lòng chọn một VPS.');
            }
        } else if (action == 'export_vps') {
            var checked = $('.vps_checkbox:checked');
            var list_vps = [];
            if (checked.length > 0) {
                $.each(checked, function(index, value) {
                    list_vps.push($(this).val());
                })
                window.location.href = '/dich-vu/vps/xuat-file-excel?list_vps=' + list_vps;
            } else {
                alert('Vui lòng chọn một VPS.');
            }
        } else {
            var checked = $('.checkbox:checked');
            $('#button-terminated').fadeIn();
            $('#button-rebuil').fadeOut();
            $('#button-multi-finish').fadeOut();
            $('#button-multi-finish').attr('data-type', 'action');
            $('#button-multi-finish').text('Hoàn thành');
            if (checked.length > 0) {
                $('#modal-services').modal('show');
                switch (action) {
                    case 'on':
                        $('.modal-title').text('Bật VPS');
                        $('#notication-invoice').html('<span class="text-danger">Bạn có muốn bật các VPS này không?</span>');
                        $('#button-terminated').attr('data-type', 'vps');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'off':
                        $('.modal-title').text('Tắt VPS');
                        $('#notication-invoice').html('<span class="text-danger">Bạn có muốn tắt các VPS này không?</span>');
                        $('#button-terminated').attr('data-type', 'vps');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'delete':
                        $('.modal-title').text('Hủy dịch vụ Colocation');
                        $('#notication-invoice').html('<span class="text-danger">Bạn có muốn hủy các dịch vụ Colocation này không?</span>');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'auto_refurn':
                        $('.modal-title').text('Cài đặt tự động gia hạn VPS');
                        var html_auto = '<span>Bạn có muốn cài đặt tự động gia hạn các dịch vụ VPS này không?</span><br><br>';
                        html_auto += '<span class="luu-y"><b>* Lưu ý: </b>Tự động gia hạn VPS sẽ tự động gia hạn khi VPS hết hạn và sẽ tự động trừ tiền trong số dư tài khoản của quý khách. ';
                        html_auto += 'Trong khi số dư tài khoản của quý khách không đủ để gia hạn VPS thì hệ thống sẽ thông báo cho quý khách qua thông báo và Email.</span>';
                        $('#notication-invoice').html(html_auto);
                        $('#button-terminated').attr('data-type', 'vps');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'off_auto_refurn':
                        $('.modal-title').text('Tắt tự động gia hạn VPS');
                        var html_auto = '<span class="text-danger">Bạn có muốn tắt tự động gia hạn các dịch vụ VPS này không?</span>';
                        $('#notication-invoice').html(html_auto);
                        $('#button-terminated').attr('data-type', 'vps');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'restart':
                        $('.modal-title').text('Khởi động lại VPS');
                        $('#notication-invoice').html('<span class="text-danger">Bạn có muốn khởi động lại các VPS này không?</span>');
                        $('#button-terminated').attr('data-type', 'vps');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'expired':
                        $('.modal-title').text('Yêu cầu gia hạn dịch vụ Proxy');
                        var list_vps = [];
                        $.each(checked, function(index, value) {
                                list_vps.push($(this).val());
                            })
                            // console.log(list_vps);
                        $.ajax({
                            url: '/service/request_expired_list_colocation',
                            data: { list_id: list_vps },
                            dataType: 'json',
                            beforeSend: function() {
                                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                                $('#button-terminated').attr('disabled', true);
                                $('#notication-invoice').html(html);
                            },
                            success: function(data) {
                                // console.log(data);
                                var html = '';
                                if (data.error == 9998) {
                                    $('#notication-invoice').html('<span class="text-center text-danger">Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.</span>');
                                    $('#button-terminated').attr('disabled', false);
                                } else if (data.error == 1) {
                                    $('#notication-invoice').html('<span class="text-center text-danger">Yêu cầu gia hạn thất bại. Quý khách vui lòng liên hệ với chúng tôi qua Fanpage để được giúp đỡ. Bấm <a href="https://www.facebook.com/cloudzone.vn" target="_blank">vào đây</a> để đến Fanpage Cloudzone.vn.</span>');
                                    $('#button-terminated').attr('disabled', false);
                                } else {
                                    if (data.price_override != '') {
                                        if (data.price_override.error == 0) {
                                            html += '<div class="form-group">';
                                            html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                            html += '<div class="mt-3 mb-3">';
                                            html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                                            html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                            html += '<option value="' + data.price_override.billing_cycle + '">' + data.price_override.text_billing_cycle + ' / ' + data.price_override.total + '</option>';
                                            html += '</select>';
                                            html += '</div>';
                                            html += '</div>';
                                            $('#notication-invoice').html(html);
                                            $('#button-terminated').attr('data-action', action);
                                            $('#button-terminated').attr('data-type', 'vps');
                                            $('#button-terminated').attr('disabled', false);
                                            $('.select_expired').select2();
                                        } else {
                                            $('#notication-invoice').html('<span class="text-center text-danger">Lỗi chọn VPS không đồng bộ thời gian. Quý khách vui lòng chọn VPS trùng với thời gian.</span>');
                                            $('#button-terminated').attr('disabled', false);
                                        }
                                    } else {
                                        html += '<div class="form-group">';
                                        html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                        html += '<div class="mt-3 mb-3">';
                                        html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                                        html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                        if (data.total['monthly'] != '0 VNĐ') {
                                            html += '<option value="monthly">1 Tháng / ' + data.total['monthly'] + '</option>';
                                        }
                                        if (data.total['twomonthly'] != '0 VNĐ') {
                                            html += '<option value="twomonthly">2 Tháng / ' + data.total['twomonthly'] + '</option>';
                                        }
                                        if (data.total['quarterly'] != '0 VNĐ') {
                                            html += '<option value="quarterly">3 Tháng / ' + data.total['quarterly'] + '</option>';
                                        }
                                        if (data.total['semi_annually'] != '0 VNĐ') {
                                            html += '<option value="semi_annually">6 Tháng / ' + data.total['semi_annually'] + '</option>';
                                        }
                                        if (data.total['annually'] != '0 VNĐ') {
                                            html += '<option value="annually">1 Năm / ' + data.total['annually'] + '</option>';
                                        }
                                        if (data.total['biennially'] != '0 VNĐ') {
                                            html += '<option value="biennially">2 Năm / ' + data.total['biennially'] + '</option>';
                                        }
                                        if (data.total['triennially'] != '0 VNĐ') {
                                            html += '<option value="triennially">3 Năm / ' + data.total['triennially'] + '</option>';
                                        }
                                        html += '</select>';
                                        html += '</div>';
                                        html += '</div>';
                                        $('#notication-invoice').html(html);
                                        $('#button-terminated').attr('data-action', action);
                                        $('#button-terminated').attr('data-type', 'vps');
                                        $('#button-terminated').attr('disabled', false);
                                        $('.select_expired').select2();
                                    }
                                }
                            },
                            error: function(e) {
                                console.log(e);
                                $('#notication-invoice').html('<span class="text-center text-danger">Lỗi truy xuất dữ liệu khách hàng.</span>');
                                $('#button-terminated').attr('disabled', false);
                            },
                        })
                        break;
                }
            } else {
                alert('Vui lòng chọn một Proxy.');
            }
        }
    });

    $('.vp_classic').on('change', function() {
        list_proxy_use();
    })

    $('.sort_next_due_date').on('click', function() {
        var vps_sort = $('.sort_next_due_date').attr('data-sort');
        if (vps_sort == 'ASC') {
            $('.sort_next_due_date').attr('data-sort', 'DESC');
            $('.sort_type').val('ASC');
            $('.sort_next_due_date i').removeClass('fa-sort-down');
            $('.sort_next_due_date i').removeClass('fa-sort');
            $('.sort_next_due_date i').addClass('fa-sort-up');
        } else {
            $('.sort_next_due_date').attr('data-sort', 'ASC');
            $('.sort_type').val('DESC');
            $('.sort_next_due_date i').addClass('fa-sort-down');
            $('.sort_next_due_date i').removeClass('fa-sort');
            $('.sort_next_due_date i').removeClass('fa-sort-up');
        }
        list_proxy_use();
    })

    $(document).on("click", '#btn_use_search_multi', function() {
        list_proxy_use();
    })

    function list_proxy_use() {
        var vps_sort = $('.sort_next_due_date').attr('data-sort');
        var sl = $('.vp_classic').val();
        // console.log(sl);
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if ( text != "" ) {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        $.ajax({
            url: '/services/colocation/select_colocation',
            dataType: 'json',
            data: { sort: vps_sort, sl: sl, multi_q: multi_q, action: 'use' },
            beforeSend: function() {
                var html = '<td class="text-center" colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function(data) {
                // console.log(data.data);
                if (data.data.length > 0) {
                    list_proxy_use_screent(data);
                } else {
                    $('tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ Proxy nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    $('tfoot').html('');
                }
            },
            error: function(e) {
                console.log(e);
                $('.tfoot').html('');
                $('.tbody').html('<td class="text-center text-danger" colspan="15">Lỗi truy vấn dữ liệu dịch vụ Proxy!</td>');
            }
        })
    }

    function list_proxy_use_screent(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function(index, colo) {
            // console.log(vps);
            html += '<tr>';
            if (colo) {
                // checkbox
                html += '<td><input type="checkbox" value="' + colo.id + '" data-ip="' + colo.ip + '" class="checkbox"></td>';
                // ip
                html += '<td class="ip_vps" data-ip="' + colo.ip + '"><a href="/service/colocation/detail/' + colo.id + '">' + colo.ip + '</a>';
                if (colo.expired == true) {
                    html += ' - <span class="text-danger">Hết hạn</span>';
                } else if (colo.isExpire == true) {
                    html += ' - <span class="text-danger">Gần hết hạn</span>';
                }
                html += '</td>';
                //port
                html += '<td>' + colo.type_colo + '</td>';
                // addon
                html += '<td>';
                html += colo.qtt_addon_ip;
                html += colo.add_on_ip;
                html += '</td>';
                // ngay tao
                html += '<td><span>' + colo.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                if (colo.isExpire == true || colo.expired == true) {
                    html += '<span class="text-danger">' + colo.next_due_date + ' - ' + colo.text_day + '<span>';
                } else {
                    html += '<span>' + colo.next_due_date + ' - ' + colo.text_day + '<span>';
                }
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + colo.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + colo.text_billing_cycle + '</td>';
                // giá
                html += '<td>' + colo.amount + '</td>';
                // html += '<td>';
                // if (colo.description != null) {
                //     html += '<span class="text-description">';
                //     html += colo.description.slice(0, 40);
                //     html += '</span>';
                // }
                // html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + proxy.id + '" data-toggle="tooltip" title="Ghi chú"><i class="fas fa-edit"></i></a></span>';
                // html += '</td>';
                // // auto gia han
                // html += '<td class="auto_refurn text-center">';
                // if (vps.auto_refurn) {
                //     html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Bật">';
                // } else {
                //     html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Tắt">';
                // }
                // html += '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">';
                // if (vps.auto_refurn) {
                //     html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps" checked>';
                // } else {
                //     html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps">';
                // }
                // html += '<label class="custom-control-label" for="customSwitch' + vps.id + '"></label>';
                // html += '</div>';
                // html += '</div>';
                // html += '</td>';
                // trang thai
                html += '<td class="vps-status">' + colo.text_status_server + '</td>';
                // console
                // if (vps.status_vps != 'suspend' && vps.status_vps != 'progressing' && vps.status_vps != 'rebuild') {
                //     html += '<td class="vps-console"><a href="/services/vps/console/' + vps.id + '" class="btn btn-sm btn-success" target="_blank"><i class="fas fa-desktop"></i></a></td>';
                // } else {
                //     html += '<td></td>';
                // }
                html += '<td class="page-service-action text-left">';
                html += '<button type="button" class="btn mr-1 btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn Colocation" data-action="expired" data-id="' + colo.id + '" data-ip="' + colo.ip + '"><i class="fas fa-plus-circle"></i></button>';
                if (colo.status != 'suspend' && colo.status != 'progressing' && colo.status != 'rebuild' && colo.status != 'Pending') {
                    if (!colo.expired) {
                        // if (vps.status_vps == 'off' || vps.status_vps == 'cancel') {
                        //     html += '<button class="btn mr-1 btn-outline-warning button-action-vps on" disabled data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                        //     html += '<button class="btn mr-1 btn-outline-success button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                        // } else {
                        //     html += '<button class="btn mr-1 btn-outline-warning button-action-vps on" data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                        //     html += '<button class="btn mr-1 btn-outline-success button-action-vps off" data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                        // }
                        // if (vps.status_vps == 'on' || vps.status_vps == 'cancel') {
                        //     html += '<button class="btn mr-1 btn-outline-info button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                        // } else {
                        //     html += '<button class="btn mr-1 btn-outline-info button-action-vps off" data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                        // }
                        // html += '<button class="btn mr-1 btn-default button-action-vps" data-toggle="tooltip" data-placement="top" title="Rebuild VPS" data-action="rebuild" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-history"></i></button>';
                        // html += '<button class="btn mr-1 bg-purple button-action-vps" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-action="change_user" data-id="' + vps.id + '"><i class="fas fa-exchange-alt"></i></button>';
                    }
                }
                html += '<button class="btn mr-1 btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ Colocation" data-action="terminated" data-id="' + colo.id + '" data-ip="' + colo.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                html += '</td>';
            } else {
                html += '<td class="text-danger text-center" colspan="15">Colocation này không có hoặc không thuộc quyền sở hữu của quý khách.</td>';
            }
            html += '</tr>';
        })
        $('tbody').html(html);
        $('.total-item').text(data.data.length);
        $('[data-toggle="tooltip"]').tooltip();
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_use">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_use">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
        $vpsCheckbox = $('.checkbox');
    }

    // ajax phân trang vps
    $(document).on('click', '.pagination_vps_use a', function(event) {
        event.preventDefault();
        console.log("da click");
        var page = $(this).attr('data-page');
        var vps_sort = $('.sort_next_due_date').attr('data-sort');
        var sl = $('.vp_classic').val();
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if ( text != "" ) {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        $.ajax({
            url: '/services/colocation/select_colocation?page=' + page,
            dataType: 'json',
            data: { sort: vps_sort, sl: sl, multi_q: multi_q, action: 'use' },
            beforeSend: function() {
                var html = '<td class="text-center" colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_proxy_use_screent(data);
                } else {
                    $('tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ Proxy nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    $('tfoot').html('');
                }
            },
            error: function(e) {
                console.log(e);
                $('.tfoot').html('');
                $('.tbody').html('<td class="text-center text-danger" colspan="15">Lỗi truy vấn dữ liệu dịch vụ Proxy!</td>');
            }
        })
    });

    $(document).on('click', '.button-action-vps', function() {
        $('tr').removeClass('action-row-vps');
        var action = $(this).attr('data-action');
        if (action == 'change_user') {
            var id = $(this).attr('data-id');
            var list_vps = [];
            list_vps.push(id);
            window.location.href = '/dich-vu/vps/chuyen-doi-khach-hang?list_vps=' + list_vps;
        } else {
            $('#button-service').attr('data-rebuild', '0');
            var id = $(this).attr('data-id');
            var ip = $(this).attr('data-ip');
            $('#button-service').fadeIn();
            $('#modal-service').modal('show');
            switch (action) {
                case 'expired':
                    $('#modal-service .modal-title').text('Yêu cầu gia hạn Colocation');
                    $('#notication-service').html('<span>Bạn có muốn gia hạn Colocation <b class="text-danger">' + ip + '</b> này không </span>');
                    $.ajax({
                        url: '/service/request_expired_colocation',
                        data: { id: id },
                        dataType: 'json',
                        beforeSend: function() {
                            var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                            $('#button-service').attr('disabled', true);
                            $('#notication-service').html(html);
                        },
                        success: function(data) {
                            var html = '';
                            if (data.error == 9998) {
                                $('#notication-service').html('<span class="text-center text-danger">Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.</span>');
                                $('#button-service').attr('disabled', false);
                            } else if (data.error == 1) {
                                $('#notication-service').html('<span class="text-center text-danger">Yêu cầu gia hạn thất bại. Quý khách vui lòng liên hệ với chúng tôi qua Fanpage để được giúp đỡ. Bấm <a href="https://www.facebook.com/cloudzone.vn" target="_blank">vào đây</a> để đến Fanpage Cloudzone.vn.</span>');
                                $('#button-service').attr('disabled', false);
                            } else {
                                // console.log(data);
                                if (data.expire_billing_cycle == true) {
                                    html += '<div class="form-group">';
                                    html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                    html += '<div class="mt-3 mb-3">';
                                    html += '<select id="select_billing_cycle" class="form-control select2 text-center" style="width:100%;">';
                                    html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                    html += '<option value="' + data.price_override.billing_cycle + '">' + data.price_override.text_billing_cycle + ' / ' + data.price_override.total + '</option>';
                                    html += '</select>';
                                    html += '</div>';
                                    html += '</div>';
                                    $('#notication-service').html(html);
                                    $('#button-service').attr('disabled', false);
                                    $('.select2').select2();
                                } else {
                                    html += '<div class="form-group">';
                                    html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                    html += '<div class="mt-3 mb-3">';
                                    html += '<select id="select_billing_cycle" class="form-control select2 text-center" style="width:100%;">';
                                    html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                    if (data.total['monthly'] != '0 VNĐ') {
                                        html += '<option value="monthly">1 Tháng / ' + data.total['monthly'] + '</option>';
                                    }
                                    if (data.total['twomonthly'] != '0 VNĐ') {
                                        html += '<option value="twomonthly">2 Tháng / ' + data.total['twomonthly'] + '</option>';
                                    }
                                    if (data.total['quarterly'] != '0 VNĐ') {
                                        html += '<option value="quarterly">3 Tháng / ' + data.total['quarterly'] + '</option>';
                                    }
                                    if (data.total['semi_annually'] != '0 VNĐ') {
                                        html += '<option value="semi_annually">6 Tháng / ' + data.total['semi_annually'] + '</option>';
                                    }
                                    if (data.total['annually'] != '0 VNĐ') {
                                        html += '<option value="annually">1 Năm / ' + data.total['annually'] + '</option>';
                                    }
                                    if (data.total['biennially'] != '0 VNĐ') {
                                        html += '<option value="biennially">2 Năm / ' + data.total['biennially'] + '</option>';
                                    }
                                    if (data.total['triennially'] != '0 VNĐ') {
                                        html += '<option value="triennially">3 Năm / ' + data.total['triennially'] + '</option>';
                                    }
                                    html += '</select>';
                                    html += '</div>';
                                    html += '</div>';
                                    $('#notication-service').html(html);
                                    $('#button-service').attr('disabled', false);
                                    $('.select2').select2();
                                }
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            $('#notication-service').html('<span class="text-center">Lỗi truy xuất dữ liệu khách hàng.</span>');
                            $('#button-terminated').attr('disabled', false);
                        },
                    });
                    break;
                case 'off':
                    $('#modal-service .modal-title').text('Yêu cầu tắt dịch vụ VPS');
                    $('#notication-service').html('<span>Bạn có muốn tắt dịch vụ VPS <b class="text-danger">(ip: ' + ip + ')</b> này không ?</span>');
                    break;
                case 'on':
                    $('#modal-service .modal-title').text('Yêu cầu bật lại dịch vụ VPS');
                    $('#notication-service').html('<span>Bạn có muốn bật lại dịch vụ VPS <b class="text-danger">(ip: ' + ip + ')</b> này không ?</span>');
                    break;
                case 'restart':
                    $('#modal-service .modal-title').text('Yêu cầu khởi động lại dịch vụ VPS');
                    $('#notication-service').html('<span>Bạn có muốn khởi động lại dịch vụ VPS <b class="text-danger">(ip: ' + ip + ')</b> này không ?</span>');
                    break;
                case 'terminated':
                    $('#modal-service .modal-title').text('Yêu cầu hủy dịch vụ Colocation');
                    $('#notication-service').html('<span>Bạn có muốn hủy dịch vụ Colocation <b class="text-danger">' + ip + '</b> này không ?</span>');
                    break;
                case 'rebuild':
                    $('#modal-service .modal-title').text('Yêu cầu cài đặt lại VPS');
                    $('#notication-service').html('<span>Bạn có muốn cài đặt lại VPS <b class="text-danger">(ip: ' + ip + ')</b> này không ?</span>');
                    $('#button-service').attr('data-rebuild', '1');
                    break;
            }
            $('#button-service').attr('data-id', id);
            $('#button-service').attr('data-action', action);
            $('#button-service').attr('data-ip', ip);
            $(this).closest('tr').addClass('action-row-vps');
            $('#button-service').fadeIn();
            $('#button-finish').fadeOut();
            $('#button-finish').text('Hoàn thành');
            $('#button-finish').attr('data-type', 'action');
        }
    });

    $('#button-service').on('click', function() {
        var action_rebuild = $(this).attr('data-rebuild');
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        if (action_rebuild == 1) {
            var id_services = [];
            id_services.push(id);
            var html = '';
            $.ajax({
                url: '/service/check-os',
                dataType: 'json',
                data: { list_id: id_services },
                beforeSend: function() {
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#button-rebuil').attr('disabled', true);
                    $('#form-rebuild').html(html);
                },
                success: function(data) {
                    //called when successful
                    // console.log(data);
                    var html = '';
                    if (!data.error) {
                        html += '<div class="form-group mt-4">';
                        html += '<label for="os_select">Chọn hệ điều hành</label>';
                        html += '<select class="form-control select2 os_select" id="os_select" style="width=100%">';
                        if (data.type) {
                            $.each(data.os, function(index, value) {
                                html += '<option value="' + value.id + '">' + value.os + '</option>';
                            })
                        } else {
                            // html += '<option value="1">Windows 7</option>';
                            html += '<option value="1">Windows 10 64bit</option>';
                            html += '<option value="2">Windows Server 2012 R2</option>';
                            html += '<option value="3">Windows Server 2016</option>';
                            html += '<option value="4">Linux CentOS 7 64bit</option>';
                            html += '<option value="15">Windows Server 2019</option>';
                            html += '<option value="31">Linux Ubuntu-20.04</option>';
                        }
                        html += '</select>';
                        html += '</div>';
                        $('#notication-service').html(html);
                        $('.select2').select2();
                        $('#button-service').attr('data-rebuild', '2');
                    } else {
                        html += 'VPS ' + data.ip + ' bị giới hạn hệ điều hành khác với các VPS khác. Quý khách vui lòng bỏ chọn VPS này và thực hiện tiếp quy trình cài lại hệ điều hành.';
                        $('.form-rebuild').html(html);
                    }
                },
                error: function(e) {
                    //called when there is an error
                    console.log(e);
                    $('#form-rebuild').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
                }
            });
        } else if (action_rebuild == 2) {
            var os = $('#os_select option:selected').text();
            var os_id = $('#os_select option:selected').val();
            var html = '';
            html += '<div class="text-left text-bold mauden mb-2">';
            html += 'Bạn sẽ cài đặt lại các VPS ở trên với thông tin như sau: <br>';
            html += 'Hệ điều hành: ' + os;
            // html += '<br> Security: ' + security_text;
            html += '<input type="hidden" name="os" id="os" value="' + os_id + '">';
            html += '</div>';
            html += '<div class="text-danger text-left mt-2 mb-2">';
            html += 'Lưu ý: Hành động này rất nguy hiểm, nó có thể xóa VPS và cài đặt lại. Quý khách vui lòng kiểm tra lại các VPS cần cài đặt lại.';
            html += '</div>';
            $('#notication-service').html(html);
            $('#button-service').attr('data-rebuild', '3');
        } else if (action_rebuild == 3) {
            var token = $('meta[name="csrf-token"]').attr('content');
            var id_services = [];
            id_services.push(id);
            var os = $('#os').val();
            $.ajax({
                url: '/services/rebuild_vps',
                type: 'post',
                data: { '_token': token, id: id_services, os: os, security: 0 },
                dataType: 'json',
                beforeSend: function() {
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#button-service').attr('disabled', true);
                    $('#notication-service').html(html);
                },
                success: function(data) {
                    // console.log(data);
                    if (data.success != '') {
                        $(document).Toasts('create', {
                            class: 'bg-info',
                            title: 'VPS',
                            subtitle: 'cài đặt lại vps',
                            body: "Thao tác cài đặt lại các dịch vụ vps thành công.",
                        })
                        $('#button-service').attr('disabled', false);
                        $('#modal-service').modal('hide');
                        $('.action-row-vps .vps-status span').addClass('vps-progressing');
                        $('.action-row-vps .vps-status span').text('Đang cài lại ...');
                        $('.action-row-vps .vps-status span').removeClass('text-success');
                        $('.action-row-vps .vps-status span').removeClass('text-danger');
                        $(".action-row-vps .page-service-action").html('');
                        $(".vps-console").html('');
                        $('tr').removeClass('action-row-vps');
                    } else {
                        $('#notication-service').html('<span class="text-danger">' + data.error + '</span>');
                    }
                },
                error: function(e) {
                    console.log(e);
                    $('#button-service').attr('disabled', false);
                    $('#notication-service').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
                }
            })
        } else {
            var billing_cycle = '';
            var type = 'proxy';
            var token = $('meta[name="csrf-token"]').attr('content');
            if (action == 'expired') {
                billing_cycle = $('#select_billing_cycle').val();
            }
            $.ajax({
                url: '/services/colocation/action-server',
                type: 'post',
                dataType: 'json',
                data: { '_token': token, id: id, type: type, action: action, billing_cycle: billing_cycle },
                beforeSend: function() {
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#button-service').attr('disabled', true);
                    $('#notication-service').html(html);
                },
                success: function(data) {
                    // console.log(data);
                    var html = '';
                    if (data) {
                        if (data.error) {
                            if (data.error == 1) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'trạng thái vps',
                                    body: "Có 1 VPS chưa được tạo trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                                })
                            } else if (data.error == 2) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'trạng thái vps',
                                    body: "Có 1 VPS đang ở trạng thái bật trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                                })
                            } else if (data.error == 3) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'trạng thái vps',
                                    body: "Có 1 VPS đang ở trạng thái tắt trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                                })
                            } else if (data.error == 4) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'VPS',
                                    subtitle: 'Không thành công',
                                    body: "Có 1 VPS do Admin off. Vui lòng kiểm tra lại trạng thái của VPS",
                                })
                            } else if (data.error == 5) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'VPS',
                                    subtitle: 'Không thành công',
                                    body: "Có 1 VPS đang ở trạng thái hủy trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                                })
                            } else if (data.error == 400) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'trạng thái vps',
                                    body: "Cập nhật trạng thái VPS thất bại. Vui lòng fresh lại website và thử lại",
                                })
                            } else if (data.error == 2223) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'mở lại hosting',
                                    body: "Mở lại hosting thất bại. Hosting này đã bị Admin khóa lại, quý khách vui lòng liên hệ lại với chúng tôi.",
                                })
                            } else if (data.error == 2224) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'mở lại hosting',
                                    body: "Mở lại hosting thất bại. Hosting đã hủy, quý khách vui lòng liên hệ lại với chúng tôi.",
                                })
                            } else if (data.error == 9998) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'Không thành công',
                                    body: "Yêu cầu thất bại. Dịch vụ được chọn không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại dịch vụ được chọn.",
                                })
                            }
                            $('#modal-service').modal('hide');
                            $('#button-service').attr('disabled', false);
                        } else {
                            if (action == 'expired') {
                                $(document).Toasts('create', {
                                    class: 'bg-success',
                                    title: 'Colocation',
                                    subtitle: 'gia hạn',
                                    body: "Yêu cầu gia hạn colocation thành công",
                                });
                                $('.action-row-vps .expired').fadeOut();
                                $('#button-service').attr('disabled', false);
                                html += '<span class="text-center">Yêu cầu gia hạn Colocation thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/' + data.invoice_id + '">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                                $('#notication-service').html(html);
                                $('#button-service').fadeOut();
                                $('#button-finish').fadeIn();
                                $('#button-finish').attr('data-type', 'expired');
                                $('#button-finish').html('Thanh toán');
                                $('#button-finish').attr('data-link', '/order/check-invoices/' + data.invoice_id);
                                $('tr').removeClass('action-row-vps');
                            } else if (action == 'off') {
                                $(document).Toasts('create', {
                                    class: 'bg-success',
                                    title: 'Proxy',
                                    subtitle: 'off',
                                    body: "Yêu cầu tắt Proxy thành công",
                                });
                                $('.action-row-vps .off').attr('disabled', false);
                                $('.action-row-vps .on').attr('disabled', true);
                                $('.action-row-vps .vps-status').html('<span class="text-danger">Đã tắt</span>');
                                $('#button-service').attr('disabled', false);
                                $('#modal-service').modal('hide');
                                $('tr').removeClass('action-row-vps');
                            } else if (action == 'on') {
                                $(document).Toasts('create', {
                                    class: 'bg-success',
                                    title: 'Proxy',
                                    subtitle: 'on',
                                    body: "Yêu cầu bật Proxy thành công",
                                });
                                $('.action-row-vps .off').attr('disabled', true);
                                $('.action-row-vps .on').attr('disabled', false);
                                $('.action-row-vps .vps-status').html('<span class="text-success">Đã bật</span>');
                                $('#button-service').attr('disabled', false);
                                $('#modal-service').modal('hide');
                                $('tr').removeClass('action-row-vps');
                            } else if (action == 'restart') {
                                $(document).Toasts('create', {
                                    class: 'bg-success',
                                    title: 'Proxy',
                                    subtitle: 'on',
                                    body: "Yêu cầu khởi động lại Proxy thành công",
                                });
                                $('.action-row-vps .off').attr('disabled', false);
                                $('.action-row-vps .on').attr('disabled', true);
                                $('.action-row-vps .vps-status').html('<span class="text-success">Đang bật</span>');
                                $('#button-service').attr('disabled', false);
                                $('#modal-service').modal('hide');
                                $('tr').removeClass('action-row-vps');
                                setTimeout(function() {
                                    html = '<span class="text-danger">Đã bật</span>';
                                    $('.action-row-vps .vps-status').html(html);
                                }, 40000);
                            } else if (action == 'terminated') {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Colocation',
                                    subtitle: 'terminated',
                                    body: "Hủy dịch vụ Colocation thành công",
                                });
                                $('.action-row-vps .terminated').fadeOut();
                                $('.action-row-vps .vps-status').html('<span class="text-danger">Đã hủy</span>');
                                $('#button-service').attr('disabled', false);
                                $('#modal-service').modal('hide');
                                $('tr').removeClass('action-row-vps');
                            }
                            // $('#modal-service').modal('hide');
                            // $('#button-service').attr('disabled', false);
                        }
                    } else {
                        $('#notication-service').html('<span class="text-danger">Hành động truy vấn đến quản lý dịch vụ thất bại!</span>');
                        $('#button-service').attr('disabled', false);
                    }
                },
                error: function(e) {
                    console.log(e);
                    $('#notication-service').html('<span class="text-danger">Truy vấn đến quản lý dịch vụ lỗi. Quý khách vui lòng liên hệ với chúng tôi để được giúp đỡ.</span>');
                    $('#button-service').attr('disabled', false);
                }
            });
        }
    });

    $('#button-finish').on('click', function() {
        if ($(this).attr('data-type') == 'expired') {
            let link = $(this).attr("data-link");
            window.open(link, '_blank').focus();
        } else {
            $('#modal-service').modal('hide');
        }
    })

    $('#button-terminated').on('click', function() {
        var type = 'proxy';
        var action = $(this).attr('data-action');
        var billing_cycle = '';
        var token = $('meta[name="csrf-token"]').attr('content');
        var id_services = [];
        if (action == 'expired') {
            billing_cycle = $('#select_billing_cycle').val();
        }
        var checked = $('.checkbox:checked');
        $.each(checked, function(index, value) {
            id_services.push($(this).val());
        })
        // console.log(id_services);
        $.ajax({
            url: '/services/action_colocation_services',
            type: 'post',
            data: { '_token': token, type: type, id: id_services, action: action, billing_cycle: billing_cycle },
            dataType: 'json',
            beforeSend: function() {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-terminated').attr('disabled', true);
                $('#notication-invoice').html(html);
            },
            success: function(data) {
                // console.log(data);
                var html = '';
                if (data) {
                    if (data.error) {
                        // console.log(data,data.error,'da den error');

                        if (data.error == 1) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách chưa được tạo trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 2) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái bật trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 3) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái tắt trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 4) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách do Admin tắt. Quý khách vui lòng lên hệ lại với chúng tôi để mở lại VPS",
                            })
                        } else if (data.error == 5) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái hủy trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 6) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang bị khóa. Quý khách vui lòng lên hệ lại với chúng tôi để mở lại VPS",
                            })
                        } else if (data.error == 400) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Cập nhật trạng thái VPS thất bại. Vui lòng fresh lại website và thử lại",
                            })
                        } else if (data.error == 2223) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái hosting',
                                body: "Mở lại hosting thất bại. Hosting này đã bị Admin khóa lại, quý khách vui lòng liên hệ lại với chúng tôi.",
                            })
                        } else if (data.error == 2224) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái hosting',
                                body: "Mở lại hosting thất bại. Hosting đã hủy, quý khách vui lòng liên hệ lại với chúng tôi.",
                            })
                        } else if (data.error == 9999) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'lỗi',
                                body: "Yêu cầu thất bại. Quý khách vui lòng chọn 1 dịch vụ để thực hiện yêu cầu.",
                            })
                        } else if (data.error == 9998) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'Không thành công',
                                body: "Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.",
                            })
                        }
                        $('#modal-services').modal('hide');
                        $('#button-terminated').attr('disabled', false);
                    } else {
                        // console.log(data,data.error,'da den success');
                        if (action == 'on') {
                            html = '<span class="text-success">Đang bật</span>';
                            $('#vps .action-vps .vps-status').html(html);
                            $(document).Toasts('create', {
                                class: 'bg-success',
                                title: 'VPS',
                                subtitle: 'bật vps',
                                body: "Thao tác bật các dịch vụ vps thành công.",
                            })
                            $('#modal-services').modal('hide');
                            $('#button-terminated').attr('disabled', false);
                            // console.log('da den on');
                            $('.action-vps .off').attr('disabled', true);
                            $('.action-vps .on').attr('disabled', false);
                            $('#button-multi-finish').fadeIn();
                        } else if (action == 'off') {
                            html = '<span class="text-danger">Đã tắt</span>';
                            $('#vps .action-vps .vps-status').html(html);
                            $(document).Toasts('create', {
                                class: 'bg-success',
                                title: 'VPS',
                                subtitle: 'tắt vps',
                                body: "Thao tác tắt các dịch vụ vps thành công.",
                            })
                            $('#modal-services').modal('hide');
                            $('#button-terminated').attr('disabled', false);
                            // console.log('da den off');
                            $('.action-vps .off').attr('disabled', false);
                            $('.action-vps .on').attr('disabled', true);
                            $('#button-multi-finish').fadeIn();
                        } else if (action == 'delete') {
                            html = '<span class="text-danger">Đã hủy</span>';
                            $('.action .vps-status').html(html);
                            $(document).Toasts('create', {
                                class: 'bg-success',
                                title: 'Colocation',
                                subtitle: 'hủy dịch vụ',
                                body: "Thao tác hủy các dịch vụ Colocation thành công.",
                            })
                            $('#modal-services').modal('hide');
                            $('#button-terminated').attr('disabled', false);
                            $('.action').html('');
                            $('#button-multi-finish').fadeIn();
                        } else if (action == 'auto_refurn') {
                            // html = '<span class="text-danger">Đã hủy</span>';
                            // $('#vps .action-vps .vps-status').html(html);
                            $(document).Toasts('create', {
                                class: 'bg-success',
                                title: 'VPS',
                                subtitle: 'tự động gia hạn',
                                body: "Thao tác cài đặt tự động gia hạn các dịch vụ VPS thành công.",
                            })
                            $('#vps .action-vps .auto_refurn span').removeClass('text-danger');
                            $('#vps .action-vps .auto_refurn span').addClass('text-success');
                            $('#modal-services').modal('hide');
                            $('#button-terminated').attr('disabled', false);
                            $('#button-multi-finish').fadeIn();
                        } else if (action == 'off_auto_refurn') {
                            // html = '<span class="text-danger">Đã hủy</span>';
                            // $('#vps .action-vps .vps-status').html(html);
                            $(document).Toasts('create', {
                                class: 'bg-success',
                                title: 'VPS',
                                subtitle: 'tắt tự động gia hạn',
                                body: "Thao tác tắt tự động gia hạn các dịch vụ VPS thành công.",
                            })
                            $('#vps .action-vps .auto_refurn span').removeClass('text-success');
                            $('#vps .action-vps .auto_refurn span').addClass('text-danger');
                            $('#modal-services').modal('hide');
                            $('#button-terminated').attr('disabled', false);
                            $('#button-multi-finish').fadeIn();
                        } else if (action == 'restart') {
                            html = '<span class="text-danger">Đang khởi động lại</span>';
                            $('.action .vps-status').html(html);
                            $(document).Toasts('create', {
                                class: 'bg-success',
                                title: 'Proxy',
                                subtitle: 'khởi động lại',
                                body: "Thao tác khởi động lại các dịch vụ Proxy thành công.",
                            })
                            $('#modal-services').modal('hide');
                            $('#button-terminated').attr('disabled', false);
                            $('.action .off').attr('disabled', false);
                            $('.action .on').attr('disabled', true);
                            $('#button-multi-finish').fadeIn();
                            setTimeout(function() {
                                html = '<span class="text-success">Đã bật</span>';
                                $('#vps .action-vps .vps-status').html(html);
                            }, 40000);

                        } else if (action == 'expired') {
                            // dd(data);
                            $(document).Toasts('create', {
                                class: 'bg-success',
                                title: 'Colocation',
                                subtitle: 'gia hạn',
                                body: "Thao tác yêu cầu gia hạn các dịch vụ Colocation thành công.",
                            })
                            $('.action .expired').fadeOut();
                            $('#button-terminated').attr('disabled', false);
                            html += '<span class="text-center">Yêu cầu gia hạn Colocation thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/' + data.invoice_id + '">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                            $('#notication-invoice').html(html);
                            $('#button-terminated').fadeOut();
                            $('#button-multi-finish').fadeIn();
                            $('#button-multi-finish').text('Thanh toán');
                            $('#button-multi-finish').attr('data-type', 'expired');
                            $('#button-multi-finish').attr('data-link', '/order/check-invoices/' + data.invoice_id);
                        }
                    }
                    $('tr').removeClass('action');
                } else {
                    toastr.warning('Thao tác với dịch vụ Proxy thất bại.');
                }
                // console.log(html);
                // $('#terminated-services').modal('hide');
                $('.list_action').val('');
            },
            error: function(e) {
                console.log(e);
                $('#button-terminated').attr('disabled', false);
                $('#notication-invoice').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
                $('#button-service').fadeOut();
                $('#button-finish').fadeIn();
            }
        })
    });

    // bấm vào thanh toán để chuyển link
    $('#button-multi-finish').on('click', function() {
        if ($(this).attr('data-type') == 'expired') {
            let link = $(this).attr('data-link');
            window.open(link, '_blank').focus();
        }
    })

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
    /**ON */
    $('.vp_on_classic').on('change', function() {
        list_proxy_on();
    })

    $('.sort_next_due_date_on').on('click', function() {
        var sl = $('.vp_classic').val();
        var vps_sort = $('.sort_next_due_date_on').attr('data-sort');
        if (vps_sort == 'ASC') {
            $('.sort_next_due_date_on').attr('data-sort', 'DESC');
            $('.sort_type').val('ASC');
            $('.sort_next_due_date_on i').removeClass('fa-sort-down');
            $('.sort_next_due_date_on i').removeClass('fa-sort');
            $('.sort_next_due_date_on i').addClass('fa-sort-up');
        } else {
            $('.sort_next_due_date_on').attr('data-sort', 'ASC');
            $('.sort_type').val('DESC');
            $('.sort_next_due_date_on i').addClass('fa-sort-down');
            $('.sort_next_due_date_on i').removeClass('fa-sort');
            $('.sort_next_due_date_on i').removeClass('fa-sort-up');
        }
        list_proxy_on();
    })

    $(document).on("click", '#btn_on_search_multi_on', function() {
        list_proxy_on();
    })

    function list_proxy_on() {
        var vps_sort = $('.sort_next_due_date_on').attr('data-sort');
        var sl = $('.vp_on_classic').val();
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if ( text != "" ) {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        $.ajax({
            url: '/services/colocation/select_colocation',
            dataType: 'json',
            data: { sort: vps_sort, sl: sl, multi_q: multi_q, action: 'on'  },
            beforeSend: function() {
                var html = '<td class="text-center" colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_proxy_on_screent(data);
                } else {
                    $('tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ Proxy nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    $('tfoot').html('');
                }
            },
            error: function(e) {
                console.log(e);
                $('.tfoot').html('');
                $('.tbody').html('<td class="text-center text-danger" colspan="15">Lỗi truy vấn dữ liệu dịch vụ Proxy!</td>');
            }
        })
    }

    function list_proxy_on_screent(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function(index, colo) {
            // console.log(vps);
            html += '<tr>';
            if (colo) {
                // checkbox
                html += '<td><input type="checkbox" value="' + colo.id + '" data-ip="' + colo.ip + '" class="checkbox"></td>';
                // ip
                html += '<td class="ip_vps" data-ip="' + colo.ip + '"><a href="/service/colocation/detail/' + colo.id + '">' + colo.ip + '</a>';
                if (colo.expired == true) {
                    html += ' - <span class="text-danger">Hết hạn</span>';
                } else if (colo.isExpire == true) {
                    html += ' - <span class="text-danger">Gần hết hạn</span>';
                }
                html += '</td>';
                //port
                html += '<td>' + colo.type_colo + '</td>';
                // addon
                html += '<td>';
                html += colo.qtt_addon_ip;
                html += colo.add_on_ip;
                html += '</td>';
                // ngay tao
                html += '<td><span>' + colo.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                if (colo.isExpire == true || colo.expired == true) {
                    html += '<span class="text-danger">' + colo.next_due_date + ' - ' + colo.text_day + '<span>';
                } else {
                    html += '<span>' + colo.next_due_date + ' - ' + colo.text_day + '<span>';
                }
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + colo.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + colo.text_billing_cycle + '</td>';
                // giá
                html += '<td>' + colo.amount + '</td>';
                // html += '<td>';
                // if (colo.description != null) {
                //     html += '<span class="text-description">';
                //     html += colo.description.slice(0, 40);
                //     html += '</span>';
                // }
                // html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + proxy.id + '" data-toggle="tooltip" title="Ghi chú"><i class="fas fa-edit"></i></a></span>';
                // html += '</td>';
                // // auto gia han
                // html += '<td class="auto_refurn text-center">';
                // if (vps.auto_refurn) {
                //     html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Bật">';
                // } else {
                //     html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Tắt">';
                // }
                // html += '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">';
                // if (vps.auto_refurn) {
                //     html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps" checked>';
                // } else {
                //     html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps">';
                // }
                // html += '<label class="custom-control-label" for="customSwitch' + vps.id + '"></label>';
                // html += '</div>';
                // html += '</div>';
                // html += '</td>';
                // trang thai
                html += '<td class="vps-status">' + colo.text_status_server + '</td>';
                // console
                // if (vps.status_vps != 'suspend' && vps.status_vps != 'progressing' && vps.status_vps != 'rebuild') {
                //     html += '<td class="vps-console"><a href="/services/vps/console/' + vps.id + '" class="btn btn-sm btn-success" target="_blank"><i class="fas fa-desktop"></i></a></td>';
                // } else {
                //     html += '<td></td>';
                // }
                html += '<td class="page-service-action text-left">';
                html += '<button type="button" class="btn mr-1 btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn Colocation" data-action="expired" data-id="' + colo.id + '" data-ip="' + colo.ip + '"><i class="fas fa-plus-circle"></i></button>';
                if (colo.status != 'suspend' && colo.status != 'progressing' && colo.status != 'rebuild' && colo.status != 'Pending') {
                    if (!colo.expired) {
                        // if (vps.status_vps == 'off' || vps.status_vps == 'cancel') {
                        //     html += '<button class="btn mr-1 btn-outline-warning button-action-vps on" disabled data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                        //     html += '<button class="btn mr-1 btn-outline-success button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                        // } else {
                        //     html += '<button class="btn mr-1 btn-outline-warning button-action-vps on" data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                        //     html += '<button class="btn mr-1 btn-outline-success button-action-vps off" data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                        // }
                        // if (vps.status_vps == 'on' || vps.status_vps == 'cancel') {
                        //     html += '<button class="btn mr-1 btn-outline-info button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                        // } else {
                        //     html += '<button class="btn mr-1 btn-outline-info button-action-vps off" data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                        // }
                        // html += '<button class="btn mr-1 btn-default button-action-vps" data-toggle="tooltip" data-placement="top" title="Rebuild VPS" data-action="rebuild" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-history"></i></button>';
                        // html += '<button class="btn mr-1 bg-purple button-action-vps" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-action="change_user" data-id="' + vps.id + '"><i class="fas fa-exchange-alt"></i></button>';
                    }
                }
                html += '<button class="btn mr-1 btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ Colocation" data-action="terminated" data-id="' + colo.id + '" data-ip="' + colo.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                html += '</td>';
            } else {
                html += '<td class="text-danger text-center" colspan="15">Colocation này không có hoặc không thuộc quyền sở hữu của quý khách.</td>';
            }
            html += '</tr>';
        })
        $('tbody').html(html);
        $('.total-item').text(data.data.length);
        $('[data-toggle="tooltip"]').tooltip();
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_on">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_on">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
        $vpsCheckbox = $('.checkbox');
    }

    // ajax phân trang vps
    $(document).on('click', '.pagination_vps_on a', function(event) {
        event.preventDefault();
        // console.log("da click");
        var page = $(this).attr('data-page');
        var vps_sort = $('.sort_next_due_date_on').attr('data-sort');
        var sl = $('.vp_on_classic').val();
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if ( text != "" ) {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        $.ajax({
            url: '/services/colocation/select_colocation?page=' + page,
            dataType: 'json',
            data: { sort: vps_sort, sl: sl, multi_q: multi_q, action: 'on'  },
            beforeSend: function() {
                var html = '<td class="text-center" colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_proxy_on_screent(data);
                } else {
                    $('tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ Colocation nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    $('tfoot').html('');
                }
            },
            error: function(e) {
                console.log(e);
                $('.tfoot').html('');
                $('.tbody').html('<td class="text-center text-danger" colspan="15">Lỗi truy vấn dữ liệu dịch vụ!</td>');
            }
        })
    });
    /**EXPIRE */
    $('.vps_expire_classic').on('change', function() {
        list_proxy_expire();
    })

    $('.sort_next_due_date_expire').on('click', function() {
        var sl = $('.vps_expire_classic').val();
        var vps_sort = $('.sort_next_due_date_expire').attr('data-sort');
        if (vps_sort == 'ASC') {
            $('.sort_next_due_date_expire').attr('data-sort', 'DESC');
            $('.sort_type').val('ASC');
            $('.sort_next_due_date_expire i').removeClass('fa-sort-down');
            $('.sort_next_due_date_expire i').removeClass('fa-sort');
            $('.sort_next_due_date_expire i').addClass('fa-sort-up');
        } else {
            $('.sort_next_due_date_expire').attr('data-sort', 'ASC');
            $('.sort_type').val('DESC');
            $('.sort_next_due_date_expire i').addClass('fa-sort-down');
            $('.sort_next_due_date_expire i').removeClass('fa-sort');
            $('.sort_next_due_date_expire i').removeClass('fa-sort-up');
        }
        list_proxy_expire();
    })

    $(document).on("click", '#btn_on_search_multi_expire', function() {
        list_proxy_expire();
    })

    function list_proxy_expire() {
        var vps_sort = $('.sort_next_due_date_expire').attr('data-sort');
        var sl = $('.vps_expire_classic').val();
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if ( text != "" ) {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        $.ajax({
            url: '/services/colocation/select_colocation',
            dataType: 'json',
            data: { sort: vps_sort, sl: sl, multi_q: multi_q, action: 'nearly' },
            beforeSend: function() {
                var html = '<td class="text-center" colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_proxy_expire_screent(data);
                } else {
                    $('tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ Proxy nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    $('tfoot').html('');
                }
            },
            error: function(e) {
                console.log(e);
                $('.tfoot').html('');
                $('.tbody').html('<td class="text-center text-danger" colspan="15">Lỗi truy vấn dữ liệu dịch vụ Proxy!</td>');
            }
        })
    }

    function list_proxy_expire_screent(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function(index, colo) {
            // console.log(vps);
            html += '<tr>';
            if (colo) {
                // checkbox
                html += '<td><input type="checkbox" value="' + colo.id + '" data-ip="' + colo.ip + '" class="checkbox"></td>';
                // ip
                html += '<td class="ip_vps" data-ip="' + colo.ip + '"><a href="/service/colocation/detail/' + colo.id + '">' + colo.ip + '</a>';
                if (colo.expired == true) {
                    html += ' - <span class="text-danger">Hết hạn</span>';
                } else if (colo.isExpire == true) {
                    html += ' - <span class="text-danger">Gần hết hạn</span>';
                }
                html += '</td>';
                //port
                html += '<td>' + colo.type_colo + '</td>';
                // addon
                html += '<td>';
                html += colo.qtt_addon_ip;
                html += colo.add_on_ip;
                html += '</td>';
                // ngay tao
                html += '<td><span>' + colo.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                if (colo.isExpire == true || colo.expired == true) {
                    html += '<span class="text-danger">' + colo.next_due_date + ' - ' + colo.text_day + '<span>';
                } else {
                    html += '<span>' + colo.next_due_date + ' - ' + colo.text_day + '<span>';
                }
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + colo.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + colo.text_billing_cycle + '</td>';
                // giá
                html += '<td>' + colo.amount + '</td>';
                // html += '<td>';
                // if (colo.description != null) {
                //     html += '<span class="text-description">';
                //     html += colo.description.slice(0, 40);
                //     html += '</span>';
                // }
                // html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + proxy.id + '" data-toggle="tooltip" title="Ghi chú"><i class="fas fa-edit"></i></a></span>';
                // html += '</td>';
                // // auto gia han
                // html += '<td class="auto_refurn text-center">';
                // if (vps.auto_refurn) {
                //     html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Bật">';
                // } else {
                //     html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Tắt">';
                // }
                // html += '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">';
                // if (vps.auto_refurn) {
                //     html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps" checked>';
                // } else {
                //     html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps">';
                // }
                // html += '<label class="custom-control-label" for="customSwitch' + vps.id + '"></label>';
                // html += '</div>';
                // html += '</div>';
                // html += '</td>';
                // trang thai
                html += '<td class="vps-status">' + colo.text_status_server + '</td>';
                // console
                // if (vps.status_vps != 'suspend' && vps.status_vps != 'progressing' && vps.status_vps != 'rebuild') {
                //     html += '<td class="vps-console"><a href="/services/vps/console/' + vps.id + '" class="btn btn-sm btn-success" target="_blank"><i class="fas fa-desktop"></i></a></td>';
                // } else {
                //     html += '<td></td>';
                // }
                html += '<td class="page-service-action text-left">';
                html += '<button type="button" class="btn mr-1 btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn Colocation" data-action="expired" data-id="' + colo.id + '" data-ip="' + colo.ip + '"><i class="fas fa-plus-circle"></i></button>';
                if (colo.status != 'suspend' && colo.status != 'progressing' && colo.status != 'rebuild' && colo.status != 'Pending') {
                    if (!colo.expired) {
                        // if (vps.status_vps == 'off' || vps.status_vps == 'cancel') {
                        //     html += '<button class="btn mr-1 btn-outline-warning button-action-vps on" disabled data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                        //     html += '<button class="btn mr-1 btn-outline-success button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                        // } else {
                        //     html += '<button class="btn mr-1 btn-outline-warning button-action-vps on" data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                        //     html += '<button class="btn mr-1 btn-outline-success button-action-vps off" data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                        // }
                        // if (vps.status_vps == 'on' || vps.status_vps == 'cancel') {
                        //     html += '<button class="btn mr-1 btn-outline-info button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                        // } else {
                        //     html += '<button class="btn mr-1 btn-outline-info button-action-vps off" data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                        // }
                        // html += '<button class="btn mr-1 btn-default button-action-vps" data-toggle="tooltip" data-placement="top" title="Rebuild VPS" data-action="rebuild" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-history"></i></button>';
                        // html += '<button class="btn mr-1 bg-purple button-action-vps" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-action="change_user" data-id="' + vps.id + '"><i class="fas fa-exchange-alt"></i></button>';
                    }
                }
                html += '<button class="btn mr-1 btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ Colocation" data-action="terminated" data-id="' + colo.id + '" data-ip="' + colo.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                html += '</td>';
            } else {
                html += '<td class="text-danger text-center" colspan="15">Colocation này không có hoặc không thuộc quyền sở hữu của quý khách.</td>';
            }
            html += '</tr>';
        })
        $('tbody').html(html);
        $('.total-item').text(data.data.length);
        $('[data-toggle="tooltip"]').tooltip();
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_expire_on">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_expire_on">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
        $vpsCheckbox = $('.checkbox');
    }

    // ajax phân trang vps
    $(document).on('click', '.pagination_expire_on a', function(event) {
        event.preventDefault();
        // console.log("da click");
        var page = $(this).attr('data-page');
        var vps_sort = $('.sort_next_due_date_expire').attr('data-sort');
        var sl = $('.vps_expire_classic').val();
        let text = $("#form_search_multi").val();
        let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
        let ips, ip = '';
        while (text.match(r) != null) {
            ip = text.match(r)[0];
            ips += ip + ',';
            text = text.replace(ip, '');
        }
        ips = ips.slice(0, -1);
        ips = ips.replace('undefined', '');
        var multi_q = ips;
        $.ajax({
            url: '/services/colocation/select_colocation?page=' + page,
            dataType: 'json',
            data: { sort: vps_sort, sl: sl, multi_q: multi_q, action: 'nearly' },
            beforeSend: function() {
                var html = '<td class="text-center" colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_proxy_expire_screent(data);
                } else {
                    $('tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ Proxy nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    $('tfoot').html('');
                }
            },
            error: function(e) {
                console.log(e);
                $('.tfoot').html('');
                $('.tbody').html('<td class="text-center text-danger" colspan="15">Lỗi truy vấn dữ liệu dịch vụ Proxy!</td>');
            }
        })
    });

    /**CANCEL */
    $('.vps_cancel_classic').on('change', function() {
        list_proxy_cancel();
    })

    $('.sort_next_due_date_cancel').on('click', function() {
        var sl = $('.vps_cancel_classic').val();
        var vps_sort = $('.sort_next_due_date_cancel').attr('data-sort');
        if (vps_sort == 'ASC') {
            $('.sort_next_due_date_cancel').attr('data-sort', 'DESC');
            $('.sort_type').val('ASC');
            $('.sort_next_due_date_cancel i').removeClass('fa-sort-down');
            $('.sort_next_due_date_cancel i').removeClass('fa-sort');
            $('.sort_next_due_date_cancel i').addClass('fa-sort-up');
        } else {
            $('.sort_next_due_date_cancel').attr('data-sort', 'ASC');
            $('.sort_type').val('DESC');
            $('.sort_next_due_date_cancel i').addClass('fa-sort-down');
            $('.sort_next_due_date_cancel i').removeClass('fa-sort');
            $('.sort_next_due_date_cancel i').removeClass('fa-sort-up');
        }
        list_proxy_cancel();
    })

    $(document).on("click", '#btn_cancel_search_multi', function() {
        list_proxy_cancel();
    })

    function list_proxy_cancel() {
        var vps_sort = $('.sort_next_due_date_cancel').attr('data-sort');
        var sl = $('.vps_cancel_classic').val();
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if ( text != "" ) {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        $.ajax({
            url: '/services/colocation/select_colocation',
            dataType: 'json',
            data: { sort: vps_sort, sl: sl, multi_q: multi_q, action: 'cancel' },
            beforeSend: function() {
                var html = '<td class="text-center" colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_proxy_cancel_screent(data);
                } else {
                    $('tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ Proxy nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    $('tfoot').html('');
                }
            },
            error: function(e) {
                console.log(e);
                $('.tfoot').html('');
                $('.tbody').html('<td class="text-center text-danger" colspan="15">Lỗi truy vấn dữ liệu dịch vụ Proxy!</td>');
            }
        })
    }

    function list_proxy_cancel_screent(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function(index, colo) {
            // console.log(vps);
            html += '<tr>';
            if (colo) {
                // ip
                html += '<td class="ip_vps" data-ip="' + colo.ip + '"><a href="/service/colocation/detail/' + colo.id + '">' + colo.ip + '</a>';
                if (colo.expired == true) {
                    html += ' - <span class="text-danger">Hết hạn</span>';
                } else if (colo.isExpire == true) {
                    html += ' - <span class="text-danger">Gần hết hạn</span>';
                }
                html += '</td>';
                //port
                html += '<td>' + colo.type_colo + '</td>';
                // addon
                html += '<td>';
                html += colo.qtt_addon_ip;
                html += colo.add_on_ip;
                html += '</td>';
                // ngay tao
                html += '<td><span>' + colo.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                if (colo.isExpire == true || colo.expired == true) {
                    html += '<span class="text-danger">' + colo.next_due_date + ' - ' + colo.text_day + '<span>';
                } else {
                    html += '<span>' + colo.next_due_date + '<span>';
                }
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + colo.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + colo.text_billing_cycle + '</td>';
                // giá
                html += '<td>' + colo.amount + '</td>';
                // html += '<td>';
                // if (colo.description != null) {
                //     html += '<span class="text-description">';
                //     html += colo.description.slice(0, 40);
                //     html += '</span>';
                // }
                // html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + proxy.id + '" data-toggle="tooltip" title="Ghi chú"><i class="fas fa-edit"></i></a></span>';
                // html += '</td>';
                // // auto gia han
                // html += '<td class="auto_refurn text-center">';
                // if (vps.auto_refurn) {
                //     html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Bật">';
                // } else {
                //     html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Tắt">';
                // }
                // html += '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">';
                // if (vps.auto_refurn) {
                //     html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps" checked>';
                // } else {
                //     html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps">';
                // }
                // html += '<label class="custom-control-label" for="customSwitch' + vps.id + '"></label>';
                // html += '</div>';
                // html += '</div>';
                // html += '</td>';
                // trang thai
                html += '<td class="vps-status">' + colo.text_status_server + '</td>';
                // console
                // if (vps.status_vps != 'suspend' && vps.status_vps != 'progressing' && vps.status_vps != 'rebuild') {
                //     html += '<td class="vps-console"><a href="/services/vps/console/' + vps.id + '" class="btn btn-sm btn-success" target="_blank"><i class="fas fa-desktop"></i></a></td>';
                // } else {
                //     html += '<td></td>';
                // }
            } else {
                html += '<td class="text-danger text-center" colspan="15">Colocation này không có hoặc không thuộc quyền sở hữu của quý khách.</td>';
            }
            html += '</tr>';
        })
        $('tbody').html(html);
        $('.total-item').text(data.data.length);
        $('[data-toggle="tooltip"]').tooltip();
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_cancel">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_cancel">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
        $vpsCheckbox = $('.checkbox');
    }

    // ajax phân trang vps
    $(document).on('click', '.pagination_vps_cancel a', function(event) {
        event.preventDefault();
        // console.log("da click");
        var page = $(this).attr('data-page');
        var vps_sort = $('.sort_next_due_date_cancel').attr('data-sort');
        var sl = $('.vps_cancel_classic').val();
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if ( text != "" ) {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        $.ajax({
            url: '/services/colocation/select_colocation?page=' + page,
            dataType: 'json',
            data: { sort: vps_sort, sl: sl, multi_q: multi_q, action: 'cancel' },
            beforeSend: function() {
                var html = '<td class="text-center" colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_proxy_cancel_screent(data);
                } else {
                    $('tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ Proxy nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    $('tfoot').html('');
                }
            },
            error: function(e) {
                console.log(e);
                $('.tfoot').html('');
                $('.tbody').html('<td class="text-center text-danger" colspan="15">Lỗi truy vấn dữ liệu dịch vụ Proxy!</td>');
            }
        })
    });
    /**ALL */
    $('.vps_all_classic').on('change', function() {
        list_proxy_all();
    })

    $('.sort_next_due_date_all').on('click', function() {
        var sl = $('.vps_all_classic').val();
        var vps_sort = $('.sort_next_due_date_all').attr('data-sort');
        if (vps_sort == 'ASC') {
            $('.sort_next_due_date_all').attr('data-sort', 'DESC');
            $('.sort_type').val('ASC');
            $('.sort_next_due_date_all i').removeClass('fa-sort-down');
            $('.sort_next_due_date_all i').removeClass('fa-sort');
            $('.sort_next_due_date_all i').addClass('fa-sort-up');
        } else {
            $('.sort_next_due_date_all').attr('data-sort', 'ASC');
            $('.sort_type').val('DESC');
            $('.sort_next_due_date_all i').addClass('fa-sort-down');
            $('.sort_next_due_date_all i').removeClass('fa-sort');
            $('.sort_next_due_date_all i').removeClass('fa-sort-up');
        }
        list_proxy_all();
    })

    $(document).on("click", '#btn_all_search_multi', function() {
        // console.log('da den');
        list_proxy_all();
    })

    function list_proxy_all() {
        var vps_sort = $('.sort_next_due_date_all').attr('data-sort');
        var sl = $('.vps_all_classic').val();
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if ( text != "" ) {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        $.ajax({
            url: '/services/colocation/select_colocation',
            dataType: 'json',
            data: { sort: vps_sort, sl: sl, multi_q: multi_q, action: 'all' },
            beforeSend: function() {
                var html = '<td class="text-center" colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_proxy_all_screent(data);
                } else {
                    $('tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ Proxy nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    $('tfoot').html('');
                }
            },
            error: function(e) {
                console.log(e);
                $('.tfoot').html('');
                $('.tbody').html('<td class="text-center text-danger" colspan="15">Lỗi truy vấn dữ liệu dịch vụ Proxy!</td>');
            }
        })
    }

    function list_proxy_all_screent(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function(index, colo) {
            // console.log(vps);
            html += '<tr>';
            if (colo) {
                // ip
                html += '<td class="ip_vps" data-ip="' + colo.ip + '"><a href="/service/colocation/detail/' + colo.id + '">' + colo.ip + '</a>';
                if (colo.expired == true) {
                    html += ' - <span class="text-danger">Hết hạn</span>';
                } else if (colo.isExpire == true) {
                    html += ' - <span class="text-danger">Gần hết hạn</span>';
                }
                html += '</td>';
                //port
                html += '<td>' + colo.type_colo + '</td>';
                // addon
                html += '<td>';
                html += colo.qtt_addon_ip;
                html += colo.add_on_ip;
                html += '</td>';
                // ngay tao
                html += '<td><span>' + colo.date_create + '</span></td>';
                // tổng thời gian thuê
                html += '<td>' + colo.total_time + '</td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                if (colo.isExpire == true || colo.expired == true) {
                    html += '<span class="text-danger">' + colo.next_due_date + '<span>';
                } else {
                    html += '<span>' + colo.next_due_date + '<span>';
                }
                html += '</td>';
                // thoi gian thue
                html += '<td>' + colo.text_billing_cycle + '</td>';
                // giá
                html += '<td>' + colo.amount + '</td>';
                // html += '<td>';
                // if (colo.description != null) {
                //     html += '<span class="text-description">';
                //     html += colo.description.slice(0, 40);
                //     html += '</span>';
                // }
                // html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + proxy.id + '" data-toggle="tooltip" title="Ghi chú"><i class="fas fa-edit"></i></a></span>';
                // html += '</td>';
                // // auto gia han
                // html += '<td class="auto_refurn text-center">';
                // if (vps.auto_refurn) {
                //     html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Bật">';
                // } else {
                //     html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Tắt">';
                // }
                // html += '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">';
                // if (vps.auto_refurn) {
                //     html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps" checked>';
                // } else {
                //     html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps">';
                // }
                // html += '<label class="custom-control-label" for="customSwitch' + vps.id + '"></label>';
                // html += '</div>';
                // html += '</div>';
                // html += '</td>';
                // trang thai
                html += '<td class="vps-status">' + colo.text_status_server + '</td>';
                // console
                // if (vps.status_vps != 'suspend' && vps.status_vps != 'progressing' && vps.status_vps != 'rebuild') {
                //     html += '<td class="vps-console"><a href="/services/vps/console/' + vps.id + '" class="btn btn-sm btn-success" target="_blank"><i class="fas fa-desktop"></i></a></td>';
                // } else {
                //     html += '<td></td>';
                // }
            } else {
                html += '<td class="text-danger text-center" colspan="15">Colocation này không có hoặc không thuộc quyền sở hữu của quý khách.</td>';
            }
            html += '</tr>';
        })
        $('tbody').html(html);
        $('.total-item').text(data.data.length);
        $('[data-toggle="tooltip"]').tooltip();
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_all">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_all">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
        $vpsCheckbox = $('.checkbox');
    }

    // ajax phân trang vps
    $(document).on('click', '.pagination_vps_all a', function(event) {
        event.preventDefault();
        // console.log("da click");
        var page = $(this).attr('data-page');
        var vps_sort = $('.sort_next_due_date_all').attr('data-sort');
        var sl = $('.vps_all_classic').val();
        let text = $("#form_search_multi").val();
        let ips, ip = '';
        if ( text != "" ) {
            let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', '');
        }
        var multi_q = ips;
        $.ajax({
            url: '/services/colocation/select_colocation?page=' + page,
            dataType: 'json',
            data: { sort: vps_sort, sl: sl, multi_q: multi_q, action: 'all' },
            beforeSend: function() {
                var html = '<td class="text-center" colspan="15"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_proxy_all_screent(data);
                } else {
                    $('tbody').html('<td class="text-center text-danger"  colspan="15">Không có dịch vụ Proxy nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    $('tfoot').html('');
                }
            },
            error: function(e) {
                console.log(e);
                $('.tfoot').html('');
                $('.tbody').html('<td class="text-center text-danger" colspan="15">Lỗi truy vấn dữ liệu dịch vụ Proxy!</td>');
            }
        })
    });

    // update description
    $(document).on('click', '.button_edit_description', function functionName(event) {
        event.preventDefault();
        $('td').removeClass('choose-update-description');
        $(this).closest('td').addClass('choose-update-description');
        var id = $(this).attr('data-id');
        var token = $('meta[name="csrf-token"]').attr('content');
        var text = $('.choose-update-description .text-description').text();
        // console.log(text);
        var html = '';
        html += '<form id="form_update_description">';
        html += '<input type="hidden" name="_token" value="' + token + '">';
        html += '<input type="hidden" name="vps_id" value="' + id + '">';
        html += '<div class="form-group">';
        html += '<textarea rows="5" cols="12" maxlength="40" class="form-control description_area" name="description">' + text.trim() + '</textarea>'
        html += '</div>';
        html += '<div class="text-right mt-2">';
        html += '<button type="button" name="button" class="btn mr-1 btn-primary button_submit_update_description">Thay đổi</button>';
        html += '</div>';
        html += '</form>';
        $('.choose-update-description').html(html);
    });

    $(document).on('click', '.button_submit_update_description', function() {
        var form = $(this).closest('#form_update_description').serialize();
        var description = $(".description_area").val();
        $.ajax({
            url: '/services/proxy/updateDescriptionProxy',
            data: form,
            type: 'post',
            dataType: 'json',
            beforeSend: function() {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.choose-update-description').html(html);
            },
            success: function(data) {
                if (data != '') {
                    var html = '';
                    html += '<span class="text-description">';
                    html += description;
                    html += '</span>';
                    html += '<span>';
                    html += '<a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + data.id + '" data-toggle="tooltip" title="Ghi chú"><i class="fas fa-edit"></i></a>';
                    html += '</span>';
                    $('.choose-update-description').html(html);
                } else {
                    $('.choose-update-description').html('<span class="text-center">Cập nhật ghi chú không thành công.</span>');
                }
                $('td').removeClass('choose-update-description');
            },
            error: function(e) {
                console.log(e);
                $('.choose-update-description').html('<span class="text-center">Lỗi truy xuất VPS.</span>');
                $('td').removeClass('choose-update-description');
            }
        });
    });

});
