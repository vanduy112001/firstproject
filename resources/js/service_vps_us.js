$(document).ready(function() {

    // VPS
    $('.vps_checkbox').removeAttr('checked');
    $('tr').removeClass('action-vps');

    var $vpsCheckbox = $('.vps_checkbox');
    var lastChecked = null;
    var $vpsCheckboxNearly = $('.vps_nearly_checkbox');
    // $vpsCheckbox.click(function (e) {
    //     if (!lastChecked) {
    //         lastChecked = this;
    //         return;
    //     }
    //     if ( e.shiftKey ) {
    //         var start = $vpsCheckbox.index(this);
    //         var end = $vpsCheckbox.index(lastChecked);
    //         $vpsCheckbox.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
    //         $.each( $vpsCheckbox , function (index , value) {
    //             if( $(this).is(':checked') ) {
    //                 $(this).closest('tr').addClass('action-vps');
    //             } else {
    //                 $(this).closest('tr').removeClass('action-vps');
    //             }
    //         })
    //     }
    //     lastChecked = this;
    // })

    // Lưu các checkbox vps
    $(document).on('click', '.vps_checkbox', function(e) {
        var checked = $('.vps_checkbox:checked');
        $('.qtt-checkbox').text( checked.length );
        if ($(this).is(':checked')) {
            $(this).closest('tr').addClass('action-vps');
        } else {
            $(this).closest('tr').removeClass('action-vps');
        }
        // console.log($vpsCheckbox);
        if (!lastChecked) {
            lastChecked = this;
            return;
        }
        if (e.shiftKey) {
            // console.log('da click 2');
            var start = $vpsCheckbox.index(this);
            var end = $vpsCheckbox.index(lastChecked);
            $vpsCheckbox.slice(Math.min(start, end), Math.max(start, end) + 1).prop('checked', lastChecked.checked);
            $.each($vpsCheckbox, function(index, value) {
                if ($(this).is(':checked')) {
                    $(this).closest('tr').addClass('action-vps');
                } else {
                    $(this).closest('tr').removeClass('action-vps');
                }
            })
        }
        lastChecked = this;
        var checked = $('.vps_checkbox:checked');
        $('.qtt-checkbox').text( checked.length );
    });
    // Lưu các checkbox vps
    $(document).on('click', '.vps_nearly_checkbox', function(e) {
        var checked = $('.vps_nearly_checkbox:checked');
        $('.qtt-checkbox').text( checked.length );
        if ($(this).is(':checked')) {
            $(this).closest('tr').addClass('action-vps');
        } else {
            $(this).closest('tr').removeClass('action-vps');
        }
        // console.log($vpsCheckbox);
        if (!lastChecked) {
            lastChecked = this;
            return;
        }
        if (e.shiftKey) {
            // console.log('da click 2');
            var start = $vpsCheckboxNearly.index(this);
            var end = $vpsCheckboxNearly.index(lastChecked);
            $vpsCheckboxNearly.slice(Math.min(start, end), Math.max(start, end) + 1).prop('checked', lastChecked.checked);
            $.each($vpsCheckbox, function(index, value) {
                if ($(this).is(':checked')) {
                    $(this).closest('tr').addClass('action-vps');
                } else {
                    $(this).closest('tr').removeClass('action-vps');
                }
            })
        }
        lastChecked = this;
        var checked = $('.vps_nearly_checkbox:checked');
        $('.qtt-checkbox').text( checked.length );
    });

    $('.vps_checkbox_all').on('click', function() {
        if ($(this).is(':checked')) {
            $('.vps_checkbox').prop('checked', this.checked);
            $('.list_vps tbody tr').addClass('action-vps');
        } else {
            $('.vps_checkbox').prop('checked', this.checked);
            $('.list_vps tbody tr').removeClass('action-vps');
        }
        var checked = $('.vps_checkbox:checked');
        $('.qtt-checkbox').text( checked.length );
    })

    $('.btn_export_vps').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        var checked = $('.vps_checkbox:checked');
        var list_vps = [];
        if (checked.length > 0) {
            $.each(checked, function(index, value) {
                list_vps.push($(this).val());
            })
            window.location.href = '/dich-vu/vps_us/xuat-file-excel?list_vps=' + list_vps;
        } else {
            alert('Vui lòng chọn một VPS.');
        }
    });

    $('.btn_nearly_export_vps').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        var checked = $('.vps_nearly_checkbox:checked');
        var list_vps = [];
        if (checked.length > 0) {
            $.each(checked, function(index, value) {
                list_vps.push($(this).val());
            })
            window.location.href = '/dich-vu/vps_us/xuat-file-excel?list_vps=' + list_vps;
        } else {
            alert('Vui lòng chọn một VPS.');
        }
    });

    $('.vps_nearly_checkbox_all').on('click', function() {
        if ($(this).is(':checked')) {
            $('.vps_nearly_checkbox').prop('checked', this.checked);
            $('.list_vps tbody tr').addClass('action-vps');
        } else {
            $('.vps_nearly_checkbox').prop('checked', this.checked);
            $('.list_vps tbody tr').removeClass('action-vps');
        }
        var checked = $('.vps_nearly_checkbox:checked');
        $('.qtt-checkbox').text( checked.length );
    })

    $(document).on('click', '.button-action-vps', function() {
        $('tr').removeClass('action-row-vps');
        var action = $(this).attr('data-action');
        if (action == 'change_user') {
            var id = $(this).attr('data-id');
            var list_vps = [];
            list_vps.push(id);
            window.location.href = '/dich-vu/vps_us/chuyen-doi-khach-hang?list_vps=' + list_vps;
        } else {
            var id = $(this).attr('data-id');
            var ip = $(this).attr('data-ip');
            $('#button-service').fadeIn();
            $('#modal-service').modal('show');
            switch (action) {
                case 'expired':
                    $('#modal-service .modal-title').text('Yêu cầu gia hạn VPS');
                    $('#notication-service').html('<span>Bạn có muốn gia hạn vps <b class="text-danger">(ip: ' + ip + ')</b> này không </span>');
                    $.ajax({
                        url: '/service/request_expired_vps',
                        data: { list_vps: id },
                        dataType: 'json',
                        beforeSend: function() {
                            var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                            $('#button-service').attr('disabled', true);
                            $('#notication-service').html(html);
                        },
                        success: function(data) {
                            var html = '';
                            if (data.error == 9998) {
                                $('#notication-service').html('<span class="text-center text-danger">Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.</span>');
                                $('#button-service').attr('disabled', false);
                            } else if (data.error == 1) {
                                $('#notication-service').html('<span class="text-center text-danger">Yêu cầu gia hạn thất bại. Quý khách vui lòng liên hệ với chúng tôi qua Fanpage để được giúp đỡ. Bấm <a href="https://www.facebook.com/cloudzone.vn" target="_blank">vào đây</a> để đến Fanpage Cloudzone.vn.</span>');
                                $('#button-service').attr('disabled', false);
                            } else {
                                // console.log(data);
                                if (data.expire_billing_cycle == true) {
                                    html += '<div class="form-group">';
                                    html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                    html += '<div class="mt-3 mb-3">';
                                    html += '<select id="select_billing_cycle" class="form-control select2 text-center" style="width:100%;">';
                                    html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                    html += '<option value="' + data.price_override.billing_cycle + '">' + data.price_override.text_billing_cycle + ' / ' + data.price_override.total + '</option>';
                                    html += '</select>';
                                    html += '</div>';
                                    html += '</div>';
                                    $('#notication-service').html(html);
                                    $('#button-service').attr('disabled', false);
                                    $('.select2').select2();
                                } else {
                                    html += '<div class="form-group">';
                                    html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                    html += '<div class="mt-3 mb-3">';
                                    html += '<select id="select_billing_cycle" class="form-control select2 text-center" style="width:100%;">';
                                    html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                    if (data.total['monthly'] != '0 VNĐ') {
                                        html += '<option value="monthly">1 Tháng / ' + data.total['monthly'] + '</option>';
                                    }
                                    if (data.total['twomonthly'] != '0 VNĐ') {
                                        html += '<option value="twomonthly">2 Tháng / ' + data.total['twomonthly'] + '</option>';
                                    }
                                    if (data.total['quarterly'] != '0 VNĐ') {
                                        html += '<option value="quarterly">3 Tháng / ' + data.total['quarterly'] + '</option>';
                                    }
                                    if (data.total['semi_annually'] != '0 VNĐ') {
                                        html += '<option value="semi_annually">6 Tháng / ' + data.total['semi_annually'] + '</option>';
                                    }
                                    if (data.total['annually'] != '0 VNĐ') {
                                        html += '<option value="annually">1 Năm / ' + data.total['annually'] + '</option>';
                                    }
                                    if (data.total['biennially'] != '0 VNĐ') {
                                        html += '<option value="biennially">2 Năm / ' + data.total['biennially'] + '</option>';
                                    }
                                    if (data.total['triennially'] != '0 VNĐ') {
                                        html += '<option value="triennially">3 Năm / ' + data.total['triennially'] + '</option>';
                                    }
                                    html += '</select>';
                                    html += '</div>';
                                    html += '</div>';
                                    $('#notication-service').html(html);
                                    $('#button-service').attr('disabled', false);
                                    $('.select2').select2();
                                }
                            }
                        },
                        error: function(e) {
                            console.log(e);
                            $('#notication-service').html('<span class="text-center">Lỗi truy xuất dữ liệu khách hàng.</span>');
                            $('#button-terminated').attr('disabled', false);
                        },
                    });
                    break;
                case 'off':
                    $('#modal-service .modal-title').text('Yêu cầu tắt dịch vụ VPS');
                    $('#notication-service').html('<span>Bạn có muốn tắt dịch vụ VPS <b class="text-danger">(ip: ' + ip + ')</b> này không ?</span>');
                    break;
                case 'on':
                    $('#modal-service .modal-title').text('Yêu cầu bật lại dịch vụ VPS');
                    $('#notication-service').html('<span>Bạn có muốn bật lại dịch vụ VPS <b class="text-danger">(ip: ' + ip + ')</b> này không ?</span>');
                    break;
                case 'restart':
                    $('#modal-service .modal-title').text('Yêu cầu khởi động lại dịch vụ VPS');
                    $('#notication-service').html('<span>Bạn có muốn khởi động lại dịch vụ VPS <b class="text-danger">(ip: ' + ip + ')</b> này không ?</span>');
                    break;
                case 'terminated':
                    $('#modal-service .modal-title').text('Yêu cầu hủy dịch vụ VPS');
                    $('#notication-service').html('<span>Bạn có muốn hủy dịch vụ VPS <b class="text-danger">(ip: ' + ip + ')</b> này không ?</span>');
                    break;
                case 'rebuild':
                    $('#modal-service .modal-title').text('Yêu cầu cài đặt lại VPS');
                    $('#notication-service').html('<span>Bạn có muốn cài đặt lại VPS <b class="text-danger">(ip: ' + ip + ')</b> này không ?</span>');
                    $('#button-service').attr('data-rebuild', '1');
                    break;
            }

            $('#button-service').attr('data-id', id);
            $('#button-service').attr('data-action', action);
            $('#button-service').attr('data-ip', ip);
            $('#button-service').attr('data-type', 'vps_us');
            $(this).closest('tr').addClass('action-row-vps');
            $('#button-service').fadeIn();
            $('#button-finish').fadeOut();
            $('#button-finish').text('Hoàn thành');
            $('#button-finish').attr('data-type', 'action');
        }

    });

    $('#button-finish').on('click', function() {
        if ($(this).attr('data-type') == 'expired') {
            let link = $(this).attr("data-link");
            window.open(link, '_blank').focus();
        } else {
            $('#modal-service').modal('hide');
        }
    })

    $('#button-service').on('click', function() {
        var action = $(this).attr('data-action');
        var id = $(this).attr('data-id');
        var action_rebuild = $(this).attr('data-rebuild');
        if (action_rebuild == 1) {
            var id_services = [];
            id_services.push(id);
            var html = '';
            html += '<div class="form-group mt-4">';
            html += '<label for="os_select">Chọn hệ điều hành</label>';
            html += '<select class="form-control select2 os_select" id="os_select" style="width=100%">';
            html += '<option value="2">Windows Server 2012 R2</option>';
            html += '</select>';
            html += '</div>';
            $('#notication-service').html(html);
            $('.select2').select2();
            $('#button-service').attr('data-rebuild', '2');
        } else if (action_rebuild == 2) {
            var os = $('#os_select option:selected').text();
            var os_id = $('#os_select option:selected').val();
            var html = '';
            html += '<div class="text-left text-bold mauden mb-2">';
            html += 'Bạn sẽ cài đặt lại các VPS ở trên với thông tin như sau: <br>';
            html += 'Hệ điều hành: ' + os;
            // html += '<br> Security: ' + security_text;
            html += '<input type="hidden" name="os" id="os" value="' + os_id + '">';
            html += '</div>';
            html += '<div class="text-danger text-left mt-2 mb-2">';
            html += 'Lưu ý: Hành động này rất nguy hiểm, nó có thể xóa VPS và cài đặt lại. Quý khách vui lòng kiểm tra lại các VPS cần cài đặt lại.';
            html += '</div>';
            $('#notication-service').html(html);
            $('#button-service').attr('data-rebuild', '3');
        } else if (action_rebuild == 3) {
            var token = $('meta[name="csrf-token"]').attr('content');
            var id_services = [];
            id_services.push(id);
            var os = $('#os').val();
            $.ajax({
                url: '/services/rebuild_vps_us',
                type: 'post',
                data: { '_token': token, id: id_services },
                dataType: 'json',
                beforeSend: function() {
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#button-service').attr('disabled', true);
                    $('#notication-service').html(html);
                },
                success: function(data) {
                    // console.log(data);
                    if (data.success != '') {
                        $(document).Toasts('create', {
                            class: 'bg-info',
                            title: 'VPS',
                            subtitle: 'cài đặt lại vps',
                            body: "Thao tác cài đặt lại các dịch vụ vps thành công.",
                        })
                        $('#button-service').attr('disabled', false);
                        $('#modal-service').modal('hide');
                        $('.action-row-vps .vps-status span').addClass('vps-progressing');
                        $('.action-row-vps .vps-status span').text('Đang cài lại ...');
                        $('.action-row-vps .vps-status span').removeClass('text-success');
                        $('.action-row-vps .vps-status span').removeClass('text-danger');
                        $(".action-row-vps .page-service-action").html('');
                        $(".vps-console").html('');
                        $('tr').removeClass('action-row-vps');
                    } else {
                        $('#notication-service').html('<span class="text-danger">' + data.error + '</span>');
                    }
                },
                error: function(e) {
                    console.log(e);
                    $('#button-service').attr('disabled', false);
                    $('#notication-service').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
                }
            })
        } else {
            var id_services = [];
            var billing_cycle = '';
            var type = $(this).attr('data-type');
            var token = $('meta[name="csrf-token"]').attr('content');
            var ip = $(this).attr('data-ip');
            if (action == 'expired') {
                billing_cycle = $('#select_billing_cycle').val();
            }
            $.ajax({
                url: '/services/action',
                type: 'post',
                dataType: 'json',
                data: { '_token': token, id: id, type: type, action: action, billing_cycle: billing_cycle },
                beforeSend: function() {
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#button-service').attr('disabled', true);
                    $('#notication-service').html(html);
                },
                success: function(data) {
                    // console.log(data, action);
                    var html = '';
                    if (data) {
                        if (data.error) {
                            if (data.error == 1) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'trạng thái vps',
                                    body: "Có 1 VPS chưa được tạo trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                                })
                            } else if (data.error == 2) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'trạng thái vps',
                                    body: "Có 1 VPS đang ở trạng thái bật trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                                })
                            } else if (data.error == 3) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'trạng thái vps',
                                    body: "Có 1 VPS đang ở trạng thái tắt trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                                })
                            } else if (data.error == 4) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'VPS',
                                    subtitle: 'Không thành công',
                                    body: "Có 1 VPS do Admin off. Vui lòng kiểm tra lại trạng thái của VPS",
                                })
                            } else if (data.error == 5) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'VPS',
                                    subtitle: 'Không thành công',
                                    body: "Có 1 VPS đang ở trạng thái hủy trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                                })
                            } else if (data.error == 400) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'trạng thái vps',
                                    body: "Cập nhật trạng thái VPS thất bại. Vui lòng fresh lại website và thử lại",
                                })
                            } else if (data.error == 2223) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'mở lại hosting',
                                    body: "Mở lại hosting thất bại. Hosting này đã bị Admin khóa lại, quý khách vui lòng liên hệ lại với chúng tôi.",
                                })
                            } else if (data.error == 2224) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'mở lại hosting',
                                    body: "Mở lại hosting thất bại. Hosting đã hủy, quý khách vui lòng liên hệ lại với chúng tôi.",
                                })
                            } else if (data.error == 9998) {
                                $(document).Toasts('create', {
                                    class: 'bg-danger',
                                    title: 'Cloudzone',
                                    subtitle: 'Không thành công',
                                    body: "Yêu cầu thất bại. Dịch vụ được chọn không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại dịch vụ được chọn.",
                                })
                            }
                            $('#modal-service').modal('hide');
                            $('#button-service').attr('disabled', false);
                        } else {
                            switch (type) {
                                case 'vps_us':
                                    if (action == 'expired') {
                                        $(document).Toasts('create', {
                                            class: 'bg-success',
                                            title: 'VPS US',
                                            subtitle: 'gia hạn',
                                            body: "Yêu cầu gia hạn vps thành công",
                                        });
                                        $('.action-row-vps .expired').fadeOut();
                                        $('#button-service').attr('disabled', false);
                                        html += '<span class="text-center">Yêu cầu gia hạn VPS thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/' + data + '">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                                        $('#notication-service').html(html);
                                        $('#button-service').fadeOut();
                                        $('#button-finish').fadeIn();
                                        $('tr').removeClass('action-row-vps');
                                        $('#button-finish').attr('data-type', 'expired');
                                        $('#button-finish').html('Thanh toán');
                                        $('#button-finish').attr('data-link', '/order/check-invoices/' + data);
                                    } else if (action == 'off') {
                                        $(document).Toasts('create', {
                                            class: 'bg-success',
                                            title: 'VPS US',
                                            subtitle: 'off',
                                            body: "Yêu cầu tắt VPS thành công",
                                        });
                                        $('.action-row-vps .off').attr('disabled', true);
                                        $('.action-row-vps .on').attr('disabled', false);
                                        $('.action-row-vps .vps-status').html('<span class="text-danger">Đã tắt</span>');
                                        $('#button-service').attr('disabled', false);
                                        $('#modal-service').modal('hide');
                                        $('tr').removeClass('action-row-vps');
                                    } else if (action == 'on') {
                                        $(document).Toasts('create', {
                                            class: 'bg-success',
                                            title: 'VPS US',
                                            subtitle: 'on',
                                            body: "Yêu cầu bật VPS thành công",
                                        });
                                        $('.action-row-vps .off').attr('disabled', false);
                                        $('.action-row-vps .on').attr('disabled', true);
                                        $('.action-row-vps .vps-status').html('<span class="text-success">Đã bật</span>');
                                        $('#button-service').attr('disabled', false);
                                        $('#modal-service').modal('hide');
                                        $('tr').removeClass('action-row-vps');
                                    } else if (action == 'restart') {
                                        $(document).Toasts('create', {
                                            class: 'bg-success',
                                            title: 'VPS US',
                                            subtitle: 'on',
                                            body: "Yêu cầu khởi động lại VPS thành công",
                                        });
                                        $('.action-row-vps .off').attr('disabled', false);
                                        $('.action-row-vps .on').attr('disabled', true);
                                        $('.action-row-vps .vps-status').html('<span class="text-success">Đang bật</span>');
                                        $('#button-service').attr('disabled', false);
                                        $('#modal-service').modal('hide');
                                        $('tr').removeClass('action-row-vps');
                                        setTimeout(function() {
                                            html = '<span class="text-danger">Đã bật</span>';
                                            $('.action-row-vps .vps-status').html(html);
                                        }, 40000);
                                    } else if (action == 'terminated') {
                                        $(document).Toasts('create', {
                                            class: 'bg-danger',
                                            title: 'VPS',
                                            subtitle: 'terminated',
                                            body: "Hủy dịch vụ VPS thành công",
                                        });
                                        $('.action-row-vps .terminated').fadeOut();
                                        $('.action-row-vps .vps-status').html('<span class="text-danger">Đã hủy</span>');
                                        $('#button-service').attr('disabled', false);
                                        $('#modal-service').modal('hide');
                                        $('tr').removeClass('action-row-vps');
                                    }
                                    break;
                            }
                            // $('#modal-service').modal('hide');
                            // $('#button-service').attr('disabled', false);
                        }
                    } else {
                        $('#notication-service').html('<span class="text-danger">Hành động truy vấn đến quản lý dịch vụ thất bại!</span>');
                        $('#button-service').attr('disabled', false);
                    }
                },
                error: function(e) {
                    console.log(e);
                    $('#notication-service').html('<span class="text-danger">Truy vấn đến quản lý dịch vụ lỗi. Quý khách vui lòng liên hệ với chúng tôi để được giúp đỡ.</span>');
                    $('#button-service').attr('disabled', false);
                }
            });
        }
    });

    // Click vào các button action của Vps
    $('#vps .list_action').on('change', function() {
        var action = $('.list_action').val();
        var checked = $('.vps_checkbox:checked');
        if (action == "") {
            alert('Vui lòng chọn hành động.');
        } else if (action == 'change_user') {
            /* Act on the event */
            var checked = $('.vps_checkbox:checked');
            var list_vps = [];
            if (checked.length > 0) {
                $.each(checked, function(index, value) {
                    list_vps.push($(this).val());
                })
                window.location.href = '/dich-vu/vps_us/chuyen-doi-khach-hang?list_vps=' + list_vps;
                // console.log('/dich-vu/vps/chuyen-doi-khach-hang?list_vps=' + list_vps);
            } else {
                alert('Vui lòng chọn một VPS.');
            }
        } else if (action == 'export_vps') {
            var checked = $('.vps_checkbox:checked');
            var list_vps = [];
            if (checked.length > 0) {
                $.each(checked, function(index, value) {
                    list_vps.push($(this).val());
                })
                window.location.href = '/dich-vu/vps_us/xuat-file-excel?list_vps=' + list_vps;
            } else {
                alert('Vui lòng chọn một VPS.');
            }
        } else {
            $('#button-terminated').fadeIn();
            $('#button-rebuil').fadeOut();
            $('#button-multi-finish').fadeOut();
            $('#button-multi-finish').attr('data-type', 'action');
            $('#button-multi-finish').text('Hoàn thành');
            if (checked.length > 0) {
                $('#modal-services').modal('show');
                switch (action) {
                    case 'on':
                        $('.modal-title').text('Bật VPS');
                        $('#notication-invoice').html('<span class="text-danger">Bạn có muốn bật các VPS này không?</span>');
                        $('#button-terminated').attr('data-type', 'vps_us');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'off':
                        $('.modal-title').text('Tắt VPS');
                        $('#notication-invoice').html('<span class="text-danger">Bạn có muốn tắt các VPS này không?</span>');
                        $('#button-terminated').attr('data-type', 'vps_us');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'reset_password':
                        $('.modal-title').text('Đặt lại mật khẩu');
                        $('#notication-invoice').html('<span class="text-danger">Bạn có muốn đặt lại mật khẩu cho các VPS này không?</span>');
                        $('#button-terminated').attr('data-type', 'vps_us');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'delete':
                        $('.modal-title').text('Hủy dịch vụ VPS');
                        $('#notication-invoice').html('<span class="text-danger">Bạn có muốn hủy các dịch vụ VPS này không?</span>');
                        $('#button-terminated').attr('data-type', 'vps_us');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'auto_refurn':
                        $('.modal-title').text('Cài đặt tự động gia hạn VPS');
                        var html_auto = '<span>Bạn có muốn cài đặt tự động gia hạn các dịch vụ VPS này không?</span><br><br>';
                        html_auto += '<span class="luu-y"><b>* Lưu ý: </b>Tự động gia hạn VPS sẽ tự động gia hạn khi VPS hết hạn và sẽ tự động trừ tiền trong số dư tài khoản của quý khách. ';
                        html_auto += 'Trong khi số dư tài khoản của quý khách không đủ để gia hạn VPS thì hệ thống sẽ thông báo cho quý khách qua thông báo và Email.</span>';
                        $('#notication-invoice').html(html_auto);
                        $('#button-terminated').attr('data-type', 'vps_us');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'off_auto_refurn':
                        $('.modal-title').text('Tắt tự động gia hạn VPS');
                        var html_auto = '<span class="text-danger">Bạn có muốn tắt tự động gia hạn các dịch vụ VPS này không?</span>';
                        $('#notication-invoice').html(html_auto);
                        $('#button-terminated').attr('data-type', 'vps_us');
                        $('#button-terminated').attr('data-action', action);
                        break;
                    case 'expired':
                        $('.modal-title').text('Yêu cầu gia hạn dịch vụ VPS');
                        var list_vps = [];
                        $.each(checked, function(index, value) {
                            list_vps.push($(this).val());
                        })
                        $.ajax({
                            url: '/service/request_expired_vps',
                            data: { list_vps: list_vps },
                            dataType: 'json',
                            beforeSend: function() {
                                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                                $('#button-terminated').attr('disabled', true);
                                $('#notication-invoice').html(html);
                            },
                            success: function(data) {
                                var html = '';
                                if (data.error == 9998) {
                                    $('#notication-invoice').html('<span class="text-center text-danger">Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.</span>');
                                    $('#button-terminated').attr('disabled', false);
                                } else {
                                    if (data.price_override != '') {
                                        if (data.price_override.error == 0) {
                                            html += '<div class="form-group">';
                                            html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                            html += '<div class="mt-3 mb-3">';
                                            html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                                            html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                            html += '<option value="' + data.price_override.billing_cycle + '">' + data.price_override.text_billing_cycle + ' / ' + data.price_override.total + '</option>';
                                            html += '</select>';
                                            html += '</div>';
                                            html += '</div>';
                                            $('#notication-invoice').html(html);
                                            $('#button-terminated').attr('data-action', action);
                                            $('#button-terminated').attr('data-type', 'vps_us');
                                            $('#button-terminated').attr('disabled', false);
                                            $('.select_expired').select2();
                                        } else {
                                            $('#notication-invoice').html('<span class="text-center text-danger">Lỗi chọn VPS không đồng bộ thời gian. Quý khách vui lòng chọn VPS trùng với thời gian.</span>');
                                            $('#button-terminated').attr('disabled', false);
                                        }
                                    } else {
                                        html += '<div class="form-group">';
                                        html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                        html += '<div class="mt-3 mb-3">';
                                        html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                                        html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                        // console.log(data.total);
                                        if (data.total['monthly'] != '0 VNĐ') {
                                            html += '<option value="monthly">1 Tháng / ' + data.total['monthly'] + '</option>';
                                        }
                                        if (data.total['twomonthly'] != '0 VNĐ') {
                                            html += '<option value="twomonthly">2 Tháng / ' + data.total['twomonthly'] + '</option>';
                                        }
                                        if (data.total['quarterly'] != '0 VNĐ') {
                                            html += '<option value="quarterly">3 Tháng / ' + data.total['quarterly'] + '</option>';
                                        }
                                        if (data.total['semi_annually'] != '0 VNĐ') {
                                            html += '<option value="semi_annually">6 Tháng / ' + data.total['semi_annually'] + '</option>';
                                        }
                                        if (data.total['annually'] != '0 VNĐ') {
                                            html += '<option value="annually">1 Năm / ' + data.total['annually'] + '</option>';
                                        }
                                        if (data.total['biennially'] != '0 VNĐ') {
                                            html += '<option value="biennially">2 Năm / ' + data.total['biennially'] + '</option>';
                                        }
                                        if (data.total['triennially'] != '0 VNĐ') {
                                            html += '<option value="triennially">3 Năm / ' + data.total['triennially'] + '</option>';
                                        }
                                        html += '</select>';
                                        html += '</div>';
                                        html += '</div>';
                                        $('#notication-invoice').html(html);
                                        $('#button-terminated').attr('data-action', action);
                                        $('#button-terminated').attr('data-type', 'vps_us');
                                        $('#button-terminated').attr('disabled', false);
                                        $('.select_expired').select2();
                                    }
                                }
                            },
                            error: function(e) {
                                console.log(e);
                                $('#notication-invoice').html('<span class="text-center text-danger">Lỗi truy xuất dữ liệu khách hàng.</span>');
                                $('#button-terminated').attr('disabled', false);
                            },
                        })
                        break;
                    case 'rebuild':
                        $('.modal-title').text('Cài đặt lại VPS');
                        $('#button-rebuil').attr('disabled', false);
                        var html = '';
                        html += '<div class="text-notication">';
                        html += '</div>';
                        html += '<div class="text-left ml-4">';
                        html += 'Danh sách VPS yêu cầu cài đặt lại: <br>';
                        $.each(checked, function(index, value) {
                            html += $(this).attr('data-ip') + '<br>';
                        })
                        html += '</div>';
                        html += '<div class="text-center text-danger mt-2 form-rebuild">';
                        html += 'Bạn có muốn cài đặt lại tất cả VPS này không';
                        html += '</div>';
                        html += '<div class="text-luuy">';
                        html += '</div>';
                        $('#notication-invoice').html(html);
                        $('#button-rebuil').attr('data-action', 'confirm');
                        $('#button-rebuil').attr('data-type', 'vps');
                        $('#button-rebuil').fadeIn();
                        $('#button-terminated').fadeOut();
                        break;
                    case 'change_ip':
                        $('.modal-title').text('Yêu cầu thay đổi IP VPS US');
                        var html = '';
                        var term = true;
                        if (term == true) {
                            html += '<div class="text-notication">';
                            html += '</div>';
                            html += '<div class="text-left ml-4">';
                            html += 'Danh sách VPS yêu cầu đổi IP: <br>';
                            $.each(checked, function(index, value) {
                                html += $(this).attr('data-ip') + '<br>';
                            })
                            html += '</div>';
                            html += '<div class="text-left mt-2 ml-4">';
                            html += 'Tổng số VPS: ' + checked.length + ' </br>';
                            html += 'Thành tiền: ' + addCommas(checked.length * 20000) + ' VNĐ </br>';
                            html += 'Số dư tài khoản: ' + addCommas($('#credit').val()) + ' VNĐ </br>';
                            if (checked.length * 20000 > parseInt($('#credit').val())) {
                                html += '<span class="text-danger">Số dư tài khoản hiện tại của Quý khách không đủ để thanh toán dịch vụ này. Quý khách vui lòng <a href="/pay-in-online" target="_blank">nạp tiền</a> vào tài khoản để hoàn tất thanh toán dịch vụ.</span>';
                            }
                            html += '</div>';
                            html += '<div class="text-danger text-left m-2">';
                            html += 'Lưu ý: Hành động này rất nguy hiểm, nó có thể xóa VPS và cài đặt lại. Quý khách vui lòng kiểm tra lại các VPS cần đổi IP.';
                            html += '</div>';
                            $('#notication-invoice').html(html);
                            $('#button-terminated').attr('data-action', action);
                            $('#button-terminated').attr('data-type', 'vps_us');
                        } else {
                            var html = '';
                            $('#notication-invoice').html('<span class="text-danger">NAT VPS không thể đổi IP. Quý khách vui lòng kiểm tra lại loại VPS của các VPS được chọn.</span>');
                            $('#button-terminated').fadeOut();
                        }
                        break;
                }
            } else {
                alert('Vui lòng chọn một VPS.');
            }
        }
    });
    // bấm vào thanh toán để chuyển link
    $('#button-multi-finish').on('click', function() {
            if ($(this).attr('data-type') == 'expired') {
                let link = $(this).attr('data-link');
                window.open(link, '_blank').focus();
            }
        })
        // rebuild VPS
    $('#button-rebuil').on('click', function() {
            var action = $(this).attr('data-action');
            if (action == 'form') {
                var html = '';
                html += '<div class="form-group mt-4">';
                html += '<label for="os_select">Chọn hệ điều hành</label>';
                html += '<select class="form-control select2 os_select" id="os_select" style="width=100%">';
                html += '<option value="2">Windows Server 2012 R2</option>';
                html += '</select>';
                html += '</div>';
                $('.form-rebuild').html(html);
                $('.select2').select2();
                $('#button-rebuil').attr('data-action', 'confirm');
                $('#button-rebuil').attr('disabled', false);
            } else if (action == 'confirm') {
                var os = $('#os_select option:selected').text();
                var os_id = $('#os_select option:selected').val();
                var html = '';
                html += '<div class="text-left text-bold mauden mb-2">';
                html += 'Bạn sẽ cài đặt lại các VPS US ở trên với thông tin như sau: <br>';
                html += 'Hệ điều hành: Windows Server 2012 R2';
                // html += '<input type="hidden" name="os" id="os" value="'+ os_id +'">';
                html += '</div>';
                html += '<div class="text-danger text-left mt-2 mb-2">';
                html += 'Lưu ý: Hành động này rất nguy hiểm, nó có thể xóa VPS và cài đặt lại. Quý khách vui lòng kiểm tra lại các VPS cần cài đặt lại.';
                html += '</div>';
                $('.form-rebuild').html(html);
                $('#button-rebuil').attr('data-action', 'submit');
            } else if (action == 'submit') {
                var token = $('meta[name="csrf-token"]').attr('content');
                var id_services = [];
                var checked = $('.vps_checkbox:checked');
                $.each(checked, function(index, value) {
                    id_services.push($(this).val());
                })
                $.ajax({
                    url: '/services/rebuild_vps_us',
                    type: 'post',
                    data: { '_token': token, id: id_services },
                    dataType: 'json',
                    beforeSend: function() {
                        var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                        $('#button-rebuil').attr('disabled', true);
                        $('#notication-invoice').html(html);
                    },
                    success: function(data) {
                        // console.log(data);
                        if (data.success != '') {
                            $(document).Toasts('create', {
                                class: 'bg-info',
                                title: 'VPS',
                                subtitle: 'cài đặt lại vps',
                                body: "Thao tác cài đặt lại các dịch vụ vps thành công.",
                            })
                            $('#button-rebuil').attr('disabled', false);
                            $('#modal-services').modal('hide');
                            $('.action-vps .vps-status').html('<span class="vps-progressing" data-id="{{ $vps->id }}">Đang cài lại ...</span>');
                            $(".page-service-action").html('');
                            $('tr').removeClass('action-vps');
                        } else {
                            $('#notication-invoice').html('<span class="text-danger">' + data.error + '</span>');
                        }
                        $('.list_action').val('');
                    },
                    error: function(e) {
                        console.log(e);
                        $('#button-rebuil').attr('disabled', false);
                        $('#notication-invoice').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
                    }
                })
            }
        })
        // modal action của hosting
    $('#button-terminated').on('click', function() {
        var type = $(this).attr('data-type');
        var action = $(this).attr('data-action');
        var billing_cycle = '';
        var token = $('meta[name="csrf-token"]').attr('content');
        var id_services = [];
        var checked = $('.vps_checkbox:checked');
        if (action == 'expired') {
            billing_cycle = $('#select_billing_cycle').val();
        }
        $.each(checked, function(index, value) {
                id_services.push($(this).val());
            })
            // console.log(id_services);
        $.ajax({
            url: '/services/action_services',
            type: 'post',
            data: { '_token': token, type: type, id: id_services, action: action, billing_cycle: billing_cycle },
            dataType: 'json',
            beforeSend: function() {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-terminated').attr('disabled', true);
                $('#notication-invoice').html(html);
            },
            success: function(data) {
                // console.log(data);
                var html = '';
                if (data) {
                    if (data.error) {
                        // console.log(data,data.error,'da den error');

                        if (data.error == 1) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách chưa được tạo trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 2) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái bật trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 3) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái tắt trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 4) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách do Admin tắt. Quý khách vui lòng lên hệ lại với chúng tôi để mở lại VPS",
                            })
                        } else if (data.error == 5) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái hủy, xóa hoặc chuyển trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 6) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang bị khóa. Quý khách vui lòng lên hệ lại với chúng tôi để mở lại VPS",
                            })
                        }
                        else if (data.error == 7) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đã hết hạn. Quý khách vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                        else if (data.error == 8) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang đổi IP. Quý khách vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                        else if (data.error == 400) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Cập nhật trạng thái VPS thất bại. Vui lòng fresh lại website và thử lại",
                            })
                        } else if (data.error == 2223) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái hosting',
                                body: "Mở lại hosting thất bại. Hosting này đã bị Admin khóa lại, quý khách vui lòng liên hệ lại với chúng tôi.",
                            })
                        } else if (data.error == 2224) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái hosting',
                                body: "Mở lại hosting thất bại. Hosting đã hủy, quý khách vui lòng liên hệ lại với chúng tôi.",
                            })
                        } else if (data.error == 9999) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'lỗi',
                                body: "Yêu cầu thất bại. Quý khách vui lòng chọn 1 dịch vụ để thực hiện yêu cầu.",
                            })
                        }
                        else if (data.error == 9998) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'Không thành công',
                                body: "Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.",
                            })
                        }
                        else if (data.error == 1111) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'Không thành công',
                                body: "Yêu cầu đổi IP thất bại. Tài khoản của quý khách không đủ để thực hiện giao dịch này.",
                            })
                        }
                        else {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'Không thành công',
                                body: data.error,
                            })
                        }
                        $('#modal-services').modal('hide');
                        $('#button-terminated').attr('disabled', false);
                    } else {
                        // console.log(data,'da den success', type, action);
                        switch (type) {
                            case 'vps_us':
                                if (action == 'on') {
                                    html = '<span class="text-success">Đang bật</span>';
                                    $('#vps .action-vps .vps-status').html(html);
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'VPS',
                                        subtitle: 'bật vps',
                                        body: "Thao tác bật các dịch vụ vps thành công.",
                                    })
                                    $('#modal-services').modal('hide');
                                    $('#button-terminated').attr('disabled', false);
                                    $('#vps .action-vps .off').attr('disabled', false);
                                    $('#vps .action-vps .on').attr('disabled', true);
                                    $('.action-vps .vps-status').html('<span class="text-success">Đã bật</span>');
                                } else if (action == 'off') {
                                    html = '<span class="text-danger">Đã tắt</span>';
                                    $('#vps .action-vps .vps-status').html(html);
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'VPS',
                                        subtitle: 'tắt vps',
                                        body: "Thao tác tắt các dịch vụ vps thành công.",
                                    })
                                    $('#modal-services').modal('hide');
                                    $('#button-terminated').attr('disabled', false);
                                    $('#vps .action-vps .off').attr('disabled', true);
                                    $('#vps .action-vps .on').attr('disabled', false);
                                    $('.action-vps .vps-status').html('<span class="text-danger">Đã tắt</span>');
                                } else if (action == 'reset_password') {
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'VPS',
                                        subtitle: 'đặt lại mật khẩu',
                                        body: "Yêu cầu đặt lại mật khẩu cho các dịch vụ vps thành công.",
                                    })
                                    $('#button-terminated').attr('disabled', false);
                                    html += '<span class="text-center">Yêu cầu đặt lại mật khẩu cho các dịch vụ vps thành công.</span>';
                                    $('#notication-invoice').html(html);
                                    $('#button-multi-finish').fadeIn();
                                    $('#button-terminated').fadeOut();
                                    $('.action-vps .vps-status').html('<span class="vps-progressing" data-id="{{ $vps->id }}">Đang đặt lại mật khẩu...</span>');
                                    $(".page-service-action").html('');
                                } else if (action == 'delete') {
                                    html = '<span class="text-danger">Đã hủy</span>';
                                    $('#vps .action-vps .vps-status').html(html);
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'VPS',
                                        subtitle: 'hủy dịch vụ',
                                        body: "Thao tác hủy các dịch vụ vps thành công.",
                                    })
                                    $('#modal-services').modal('hide');
                                    $('#button-terminated').attr('disabled', false);
                                    $('#vps .action-vps .off').attr('disabled', false);
                                    $('#vps .action-vps .on').attr('disabled', true);
                                    $('.action-vps .vps-status').html('<span class="text-danger">Đã hủy</span>');
                                } else if (action == 'auto_refurn') {
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'VPS US',
                                        subtitle: 'tự động gia hạn',
                                        body: "Thao tác cài đặt tự động gia hạn các dịch vụ VPS US thành công.",
                                    })
                                    $('#vps .action-vps .auto_refurn span').removeClass('text-danger');
                                    $('#vps .action-vps .auto_refurn span').addClass('text-success');
                                    $('#modal-services').modal('hide');
                                    $('#button-terminated').attr('disabled', false);
                                } else if (action == 'off_auto_refurn') {
                                    // html = '<span class="text-danger">Đã hủy</span>';
                                    // $('#vps .action-vps .vps-status').html(html);
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'VPS',
                                        subtitle: 'tắt tự động gia hạn',
                                        body: "Thao tác tắt tự động gia hạn các dịch vụ VPS thành công.",
                                    })
                                    $('#vps .action-vps .auto_refurn span').removeClass('text-success');
                                    $('#vps .action-vps .auto_refurn span').addClass('text-danger');
                                    $('#modal-services').modal('hide');
                                    $('#button-terminated').attr('disabled', false);
                                } else if (action == 'restart') {
                                    html = '<span class="text-danger">Đang khởi động lại</span>';
                                    $('#vps .action-vps .vps-status').html(html);
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'VPS',
                                        subtitle: 'khởi động lại',
                                        body: "Thao tác khởi động lại các dịch vụ vps thành công.",
                                    })
                                    $('#modal-services').modal('hide');
                                    $('#button-terminated').attr('disabled', false);
                                    $('#vps .action-vps .off').attr('disabled', false);
                                    $('#vps .action-vps .on').attr('disabled', true);
                                    setTimeout(function() {
                                        html = '<span class="text-success">Đã bật</span>';
                                        $('#vps .action-vps .vps-status').html(html);
                                    }, 40000);

                                } else if (action == 'expired') {
                                    // dd(data);
                                    $(document).Toasts('create', {
                                        class: 'bg-success',
                                        title: 'VPS',
                                        subtitle: 'gia hạn',
                                        body: "Thao tác yêu cầu gia hạn các dịch vụ VPS thành công.",
                                    })
                                    $('#vps .action-vps .expired').fadeOut();
                                    $('#button-terminated').attr('disabled', false);
                                    html += '<span class="text-center">Yêu cầu gia hạn VPS thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/' + data + '">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                                    $('#notication-invoice').html(html);
                                    $('#button-terminated').fadeOut();
                                    $('#button-multi-finish').fadeIn();
                                    $('#button-multi-finish').text('Thanh toán');
                                    $('#button-multi-finish').attr('data-type', 'expired');
                                    $('#button-multi-finish').attr('data-link', '/order/check-invoices/' + data);
                                } else if (action == 'change_ip') {
                                    // console.log('da den');
                                    if (data.check == 1) {
                                        $('#button-terminated').attr('disabled', false);
                                        html += '<span class="text-danger">Số dư tài khoản hiện tại của Quý khách không đủ để thanh toán dịch vụ này. Quý khách vui lòng <a href="/pay-in-online" target="_blank">nạp tiền</a> vào tài khoản để hoàn tất thanh toán dịch vụ.</span>';
                                        $('#notication-invoice').html(html);
                                    } else {
                                        $(document).Toasts('create', {
                                            class: 'bg-success',
                                            title: 'VPS',
                                            subtitle: 'thay đổi IP',
                                            body: "Yêu cầu thay đổi IP các dịch vụ VPS thành công.",
                                        })
                                        $('#button-terminated').attr('disabled', false);
                                        html += '<span class="text-center">Yêu cầu thay đổi IP VPS thành công.</span>';
                                        $('#notication-invoice').html(html);
                                        $('#button-multi-finish').fadeIn();
                                        $('#button-terminated').fadeOut();
                                        $('.action-vps .vps-status').html('<span class="vps-progressing" data-id="{{ $vps->id }}">Đang đổi IP ...</span>');
                                        $(".page-service-action").html('');
                                    }
                                }
                                break;
                        }
                    }
                    $('tr').removeClass('action-services');
                } else {
                    toastr.warning('Thao tác với dịch vụ VPS thất bại.');
                }
                $('.list_action').val('');
                // console.log(html);
                // $('#terminated-services').modal('hide');
            },
            error: function(e) {
                console.log(e);
                $('#button-terminated').attr('disabled', false);
                $('#notication-invoice').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
                $('#button-service').fadeOut();
                $('#button-finish').fadeIn();
            }
        })
    });

    // auto gia hạn
    $(document).on('click', '.btnAutoRefurn', function() {
        var type = $(this).attr('data-type');
        var billing_cycle = '';
        var token = $('meta[name="csrf-token"]').attr('content');
        var id_services = [];
        if (this.checked) {
            var action = 'auto_refurn';
            var typeCheked = 'checked';
        } else {
            var action = 'off_auto_refurn';
            var typeCheked = 'unchecked';
        }
        id_services.push($(this).attr('data-id'));
        $.ajax({
            url: '/services/action_services',
            type: 'post',
            data: { '_token': token, type: type, id: id_services, action: action, billing_cycle: billing_cycle },
            dataType: 'json',
            success: function(data) {
                if (data) {
                    if (data.error) {
                        if (typeCheked == 'checked') {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'tự động gia hạn',
                                body: "Bật tự động gia hạn thất bại",
                            })
                        } else {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'tự động gia hạn',
                                body: "Tắt tự động gia hạn thất bại",
                            })
                        }
                    } else {
                        if (typeCheked == 'checked') {
                            $(document).Toasts('create', {
                                class: 'bg-success',
                                title: 'VPS',
                                subtitle: 'tự động gia hạn',
                                body: "Bật tự động gia hạn thành công",
                            })
                        } else {
                            $(document).Toasts('create', {
                                class: 'bg-success',
                                title: 'VPS',
                                subtitle: 'tự động gia hạn',
                                body: "Tắt tự động gia hạn thành công",
                            })
                        }
                    }
                } else {
                    toastr.warning('Thao tác với dịch vụ VPS thất bại.');
                }
            },
            error: function(e) {
                console.log(e);
                toastr.warning('Thao tác với dịch vụ VPS thất bại.');
            }
        })
    })

    $('.vp_classic').on('change', function() {
        var sl = $(this).val();
        $.ajax({
            url: '/services/list_vps_us_with_sl',
            data: { sl: sl },
            dataType: 'json',
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_vps_screent(data);
                } else {
                    $('.list_vps tfoot').html('');
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_vps tfoot').html('');
                $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        });
    })

    $('#vps_search').on("keyup", function() {
        var vps_q = $(this).val();
        var vps_sort = $('.sort_type').val();
        var sl = $('.vp_classic').val();
        $.ajax({
            url: '/services/search_vps_us',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl },
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_vps_screent(data);
                } else {
                    $('.list_vps tfoot').html('');
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_vps tfoot').html('');
                $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    $('.sort_next_due_date').on('click', function() {
        var vps_q = $('#vps_search').val();
        var sl = $('.vp_classic').val();
        var vps_sort = $(this).attr('data-sort');
        if (vps_sort == 'ASC') {
            $(this).attr('data-sort', 'DESC');
            $('.sort_type').val('ASC');
            $('.sort_next_due_date i').removeClass('fa-sort-down');
            $('.sort_next_due_date i').removeClass('fa-sort');
            $('.sort_next_due_date i').addClass('fa-sort-up');
        } else {
            $(this).attr('data-sort', 'ASC');
            $('.sort_type').val('DESC');
            $('.sort_next_due_date i').addClass('fa-sort-down');
            $('.sort_next_due_date i').removeClass('fa-sort');
            $('.sort_next_due_date i').removeClass('fa-sort-up');
        }
        $.ajax({
            url: '/services/search_vps_us',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl },
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_vps_screent(data);
                } else {
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    $('.list_vps tfoot').html('');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_vps tfoot').html('');
                $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    $('.vp_classic_use').on('change', function() {
        var sl = $(this).val();
        $.ajax({
            url: '/services/list_vps_us_use_with_sl',
            data: { sl: sl },
            dataType: 'json',
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_vps_use_screent(data);
                } else {
                    $('tfoot').html('');
                    $('tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('tfoot').html('');
                $('tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        });
    })

    $('#vps_search_use').on("keyup", function() {
        var vps_q = $(this).val();
        var vps_sort = $('.sort_type_use').val();
        var sl = $('.vp_classic_use').val();
        $.ajax({
            url: '/services/search_vps_us_use',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl },
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps_use tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_vps_use_screent(data);
                } else {
                    $('.list_vps_use tfoot').html('');
                    $('.list_vps_use tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_vps_use tfoot').html('');
                $('.list_vps_use tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    $('.sort_next_due_date_use').on('click', function() {
        var vps_q = $('#vps_search_use').val();
        var sl = $('.vp_classic_use').val();
        var vps_sort = $(this).attr('data-sort');
        if (vps_sort == 'ASC') {
            $(this).attr('data-sort', 'DESC');
            $('.sort_type_use').val('ASC');
            $('.sort_next_due_date_use i').removeClass('fa-sort-down');
            $('.sort_next_due_date_use i').removeClass('fa-sort');
            $('.sort_next_due_date_use i').addClass('fa-sort-up');
        } else {
            $(this).attr('data-sort', 'ASC');
            $('.sort_type_use').val('DESC');
            $('.sort_next_due_date_use i').addClass('fa-sort-down');
            $('.sort_next_due_date_use i').removeClass('fa-sort');
            $('.sort_next_due_date_use i').removeClass('fa-sort-up');
        }
        $.ajax({
            url: '/services/search_vps_us_use',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl },
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps_use tbody').html(html);
            },
            success: function(data) {
                // console.log(data);
                if (data.data.length > 0) {
                    list_vps_use_screent(data);
                } else {
                    $('.list_vps_use tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    $('.list_vps_use tfoot').html('');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_vps_use tfoot').html('');
                $('.list_vps_use tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    $('#btn_use_multi').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        var html = '';
        html += '<div class="input-group">';
        html += '<textarea class="form-control" aria-label="With textarea" id="form_search_multi" placeholder="Tìm kiếm nhiều VPS theo IP (Ví dụ: 192.168.1.1, 192.168.1.2)"></textarea>';
        html += '<div class="input-group-prepend" style="align-items: initial;" id="btn_use_search_multi">';
        html += '<span class="input-group-text btnOutlinePrimary">Tìm kiếm</span>';
        html += '</div>';
        html += '</div>';
        $('#on_search_multi').html(html);
    });

    $(document).on("click", '#btn_use_search_multi', function() {
        let text = $("#form_search_multi").val();
        let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
        let r2 = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}\b/;
        let ips, ip = '';
        if ( text.match(r2) ) {
            while (text.match(r2) != null) {
                ip = text.match(r2)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }
        if ( text.match(r) ) {
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }

        if (typeof(ips) !== 'undefined') {
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', ''); 
            console.log(ips);

            var vps_q = ips;
            ips = ips.replace(',', ' ');
            $("#form_search_multi").val(ips);
            $.ajax({
                url: '/services/search_multi_vps_us_use',
                dataType: 'json',
                data: { q: vps_q },
                beforeSend: function() {
                    var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_vps_use tbody').html(html);
                },
                success: function(data) {
                    // console.log(data);
                    if (data.data.length > 0) {
                        list_vps_use_screent(data);
                    } else {
                        $('.list_vps_use tfoot').html('');
                        $('.list_vps_use tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    }
                },
                error: function(e) {
                    console.log(e);
                    $('.list_vps_use tfoot').html('');
                    $('.list_vps_use tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
        else {
            var vps_q = $("#form_search_multi").val();
            var vps_sort = $('.sort_type_use').val();
            var sl = $('.vp_classic_use').val();
            $.ajax({
                url: '/services/search_vps_us_use',
                dataType: 'json',
                data: { q: vps_q, sort: vps_sort, sl: sl },
                beforeSend: function() {
                    var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_vps_use tbody').html(html);
                },
                success: function(data) {
                    if (data.data.length > 0) {
                        list_vps_use_screent(data);
                    } else {
                        $('.list_vps_use tfoot').html('');
                        $('.list_vps_use tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    }
                },
                error: function(e) {
                    console.log(e);
                    $('.list_vps_use tfoot').html('');
                    $('.list_vps_use tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
    })
        // Click vào các button action của Vps
    $('.list_action_nearly').on('change', function() {
        var action = $('.list_action_nearly').val();
        if (action == "") {
            alert('Vui lòng chọn hành động.');
        } else if (action == 'change_user') {
            var checked = $('.vps_nearly_checkbox:checked');
            var list_vps = [];
            if (checked.length > 0) {
                $.each(checked, function(index, value) {
                    list_vps.push($(this).val());
                })
                window.location.href = '/dich-vu/vps_us/chuyen-doi-khach-hang?list_vps=' + list_vps;
                // console.log('/dich-vu/vps/chuyen-doi-khach-hang?list_vps=' + list_vps);
            } else {
                alert('Vui lòng chọn một VPS.');
            }
        } else if (action == 'export_vps') {
            var checked = $('.vps_nearly_checkbox:checked');
            var list_vps = [];
            if (checked.length > 0) {
                $.each(checked, function(index, value) {
                    list_vps.push($(this).val());
                })
                window.location.href = '/dich-vu/vps_us/xuat-file-excel?list_vps=' + list_vps;
            } else {
                alert('Vui lòng chọn một VPS.');
            }
        } else {
            var checked = $('.vps_nearly_checkbox:checked');
            $('#button-terminated-nearly').fadeIn();
            $('#button-rebuil-nearly').fadeOut();
            $('#button-multi-finish').fadeOut();
            $('#button-multi-finish').attr('data-type', 'action');
            $('#button-multi-finish').text('Hoàn thành');
            if (checked.length > 0) {
                $("#modal-services-nearly").modal("show");
                $('.modal-title').text('Yêu cầu gia hạn dịch vụ VPS');
                var list_vps = [];
                $.each(checked, function(index, value) {
                    list_vps.push($(this).val());
                })
                $.ajax({
                    url: '/service/request_expired_vps',
                    data: { list_vps: list_vps },
                    dataType: 'json',
                    beforeSend: function() {
                        var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                        $('#button-terminated-nearly').attr('disabled', true);
                        $('#notication-invoice-nearly').html(html);
                    },
                    success: function(data) {
                        var html = '';
                        if (data.error == 9998) {
                            $('#notication-invoice-nearly').html('<span class="text-center text-danger">Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.</span>');
                            $('#button-terminated-nearly').attr('disabled', false);
                        } else if (data.error == 1) {
                            $('#notication-invoice-nearly').html('<span class="text-center text-danger">Yêu cầu gia hạn thất bại. Quý khách vui lòng liên hệ với chúng tôi qua Fanpage để được giúp đỡ. Bấm <a href="https://www.facebook.com/cloudzone.vn" target="_blank">vào đây</a> để đến Fanpage Cloudzone.vn.</span>');
                            $('#button-terminated-nearly').attr('disabled', false);
                        } else {
                            if (data.price_override != '') {
                                if (data.price_override.error == 0) {
                                    html += '<div class="form-group">';
                                    html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                    html += '<div class="mt-3 mb-3">';
                                    html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                                    html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                    html += '<option value="' + data.price_override.billing_cycle + '">' + data.price_override.text_billing_cycle + ' / ' + data.price_override.total + '</option>';
                                    html += '</select>';
                                    html += '</div>';
                                    html += '</div>';
                                    $('#notication-invoice-nearly').html(html);
                                    $('#button-terminated-nearly').attr('data-action', action);
                                    $('#button-terminated-nearly').attr('data-type', 'vps_us');
                                    $('#button-terminated-nearly').attr('disabled', false);
                                    $('.select_expired').select2();
                                } else {
                                    $('#notication-invoice-nearly').html('<span class="text-center text-danger">Lỗi chọn VPS không đồng bộ thời gian. Quý khách vui lòng chọn VPS trùng với thời gian.</span>');
                                    $('#button-terminated-nearly').attr('disabled', false);
                                }
                            } else {
                                html += '<div class="form-group">';
                                html += '<label for="select_billing_cycle">Chọn thời gian gia hạn</label>';
                                html += '<div class="mt-3 mb-3">';
                                html += '<select id="select_billing_cycle" class="form-control select_expired text-center" style="width:100%;">';
                                html += '<option disabled>--Chọn thời gian gia hạn---</option>';
                                html += '<option value="monthly">1 Tháng / ' + data.total['monthly'] + '</option>';
                                html += '<option value="twomonthly">2 Tháng / ' + data.total['twomonthly'] + '</option>';
                                html += '<option value="quarterly">3 Tháng / ' + data.total['quarterly'] + '</option>';
                                html += '<option value="semi_annually">6 Tháng / ' + data.total['semi_annually'] + '</option>';
                                html += '<option value="annually">1 Năm / ' + data.total['annually'] + '</option>';
                                html += '<option value="biennially">2 Năm / ' + data.total['biennially'] + '</option>';
                                html += '<option value="triennially">3 Năm / ' + data.total['triennially'] + '</option>';
                                html += '</select>';
                                html += '</div>';
                                html += '</div>';
                                $('#notication-invoice-nearly').html(html);
                                $('#button-terminated-nearly').attr('data-action', action);
                                $('#button-terminated-nearly').attr('data-type', 'vps_us');
                                $('#button-terminated-nearly').attr('disabled', false);
                                $('.select_expired').select2();
                            }
                        }
                    },
                    error: function(e) {
                        console.log(e);
                        $('#notication-invoice-nearly').html('<span class="text-center text-danger">Lỗi truy xuất dữ liệu khách hàng.</span>');
                        $('#button-terminated-nearly').attr('disabled', false);
                    },
                })
            } else {
                alert('Vui lòng chọn một VPS.');
            }
        }
    });

    // modal action của hosting
    $('#button-terminated-nearly').on('click', function() {
        var type = $(this).attr('data-type');
        var action = $(this).attr('data-action');
        var billing_cycle = '';
        var token = $('meta[name="csrf-token"]').attr('content');
        var id_services = [];
        var checked = $('.vps_nearly_checkbox:checked');
        if (action == 'expired') {
            billing_cycle = $('#select_billing_cycle').val();
        }
        $.each(checked, function(index, value) {
                id_services.push($(this).val());
            })
            // console.log(id_services);
        $.ajax({
            url: '/services/action_services',
            type: 'post',
            data: { '_token': token, type: type, id: id_services, action: action, billing_cycle: billing_cycle },
            dataType: 'json',
            beforeSend: function() {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-terminated-nearly').attr('disabled', true);
                $('#notication-invoice-nearly').html(html);
            },
            success: function(data) {
                // console.log(data);
                var html = '';
                if (data) {
                    if (data.error) {
                        // console.log(data,data.error,'da den error');

                        if (data.error == 1) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách chưa được tạo trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 2) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái bật trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 3) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái tắt trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 4) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách do Admin tắt. Quý khách vui lòng lên hệ lại với chúng tôi để mở lại VPS",
                            })
                        } else if (data.error == 5) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang ở trạng thái hủy trong các VPS được chọn. Vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        } else if (data.error == 6) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang bị khóa. Quý khách vui lòng lên hệ lại với chúng tôi để mở lại VPS",
                            })
                        }
                        else if (data.error == 7) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đã hết hạn. Quý khách vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                        else if (data.error == 8) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Có 1 VPS của quý khách đang đổi IP. Quý khách vui lòng kiểm tra lại trạng thái của VPS",
                            })
                        }
                        else if (data.error == 400) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'VPS',
                                subtitle: 'Không thành công',
                                body: "Cập nhật trạng thái VPS thất bại. Vui lòng fresh lại website và thử lại",
                            })
                        } else if (data.error == 2223) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái hosting',
                                body: "Mở lại hosting thất bại. Hosting này đã bị Admin khóa lại, quý khách vui lòng liên hệ lại với chúng tôi.",
                            })
                        } else if (data.error == 2224) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'trạng thái hosting',
                                body: "Mở lại hosting thất bại. Hosting đã hủy, quý khách vui lòng liên hệ lại với chúng tôi.",
                            })
                        } else if (data.error == 9999) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'lỗi',
                                body: "Yêu cầu thất bại. Quý khách vui lòng chọn 1 dịch vụ để thực hiện yêu cầu.",
                            })
                        } else if (data.error == 9998) {
                            $(document).Toasts('create', {
                                class: 'bg-danger',
                                title: 'Cloudzone',
                                subtitle: 'Không thành công',
                                body: "Yêu cầu thất bại. Trong các dịch vụ được chọn, có 1 hoặc nhiều dịch vụ không có hoặc không thuộc quyền quản lý của quý khách. Vui lòng kiểm tra lại các dịch vụ được chọn.",
                            })
                        }
                        $('#modal-services').modal('hide');
                        $('#button-terminated').attr('disabled', false);
                    } else {
                        // console.log(data,data.error,'da den success');
                        // dd(data);
                        $(document).Toasts('create', {
                            class: 'bg-success',
                            title: 'VPS',
                            subtitle: 'gia hạn',
                            body: "Thao tác yêu cầu gia hạn các dịch vụ VPS thành công.",
                        })
                        $('.action-vps .expired').fadeOut();
                        $('#button-terminated-nearly').attr('disabled', false);
                        html += '<span class="text-center">Yêu cầu gia hạn VPS thành công. Quý khách vui lòng bấm <a href="/order/check-invoices/' + data + '">vào đây</a> để thanh toán hoàn thành quá trình gia hạn.</span>';
                        $('#notication-invoice-nearly').html(html);
                        $('#button-terminated-nearly').fadeOut();
                        $('#button-multi-finish').fadeIn();
                        $('#button-multi-finish').attr('data-type', 'expired');
                        $('#button-multi-finish').attr('data-link', '/order/check-invoices/' + data);
                        $('#button-multi-finish').text('Thanh toán');
                    }
                    $('tr').removeClass('action-vps');
                } else {
                    toastr.warning('Thao tác với dịch vụ VPS thất bại.');
                }
                // console.log(html);
                // $('#terminated-services').modal('hide');
                $('.list_action_nearly').val('');
            },
            error: function(e) {
                console.log(e);
                $('#button-terminated-nearly').attr('disabled', false);
                $('#notication-invoice-nearly').html('<span class="text-danger">Truy vấn dịch vụ lỗi!</span>');
                $('#button-service-nearly').fadeOut();
            }
        })
    });

    $('#btn_on_multi').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        var html = '';
        html += '<div class="input-group">';
        html += '<textarea class="form-control" aria-label="With textarea" id="form_search_multi" placeholder="Tìm kiếm nhiều VPS theo IP (Ví dụ: 192.168.1.1, 192.168.1.2)"></textarea>';
        html += '<div class="input-group-prepend" style="align-items: initial;" id="btn_on_search_multi">';
        html += '<span class="input-group-text btnOutlinePrimary">Tìm kiếm</span>';
        html += '</div>';
        html += '</div>';
        $('#on_search_multi').html(html);
    });

    $(document).on("click", '#btn_on_search_multi', function() {
        let text = $("#form_search_multi").val();
        let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
        let r2 = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}\b/;
        let ips, ip = '';
        if ( text.match(r2) ) {
            while (text.match(r2) != null) {
                ip = text.match(r2)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }
        if ( text.match(r) ) {
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }

        if (typeof(ips) !== 'undefined') {
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', ''); 
            console.log(ips);

            var vps_q = ips;
            ips = ips.replace(',', ' ');
            $("#form_search_multi").val(ips);
            $.ajax({
                url: '/services/search_multi_vps_us_on',
                dataType: 'json',
                data: { q: vps_q },
                beforeSend: function() {
                    var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_vps tbody').html(html);
                },
                success: function(data) {
                    // console.log(data);
                    if (data.data.length > 0) {
                        list_vps_screent(data);
                    } else {
                        $('.list_vps tfoot').html('');
                        $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    }
                },
                error: function(e) {
                    console.log(e);
                    $('.list_vps tfoot').html('');
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
        else {
            var vps_q = $("#form_search_multi").val();
            var vps_sort = $('.sort_type').val();
            var sl = $('.vp_classic').val();
            $.ajax({
                url: '/services/search_vps_us',
                dataType: 'json',
                data: { q: vps_q, sort: vps_sort, sl: sl },
                beforeSend: function() {
                    var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_vps_use tbody').html(html);
                },
                success: function(data) {
                    if (data.data.length > 0) {
                        list_vps_screent(data);
                    } else {
                        $('.list_vps_use tfoot').html('');
                        $('.list_vps_use tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    }
                },
                error: function(e) {
                    console.log(e);
                    $('.list_vps_use tfoot').html('');
                    $('.list_vps_use tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
        // console.log(ips);
    })

    function list_vps_screent(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function(index, vps) {
            html += '<tr>';
            if (vps) {
                // checkbox
                html += '<td><input type="checkbox" value="' + vps.id + '" data-ip="' + vps.ip + '" class="vps_checkbox"></td>';
                // ip
                html += '<td class="ip_vps text-left" data-ip="' + vps.ip + '"><a href="/service/detail/' + vps.id + '?type=vps">' + vps.ip + '</a>';
                if (vps.expired == true) {
                    html += ' - <span class="text-danger">Hết hạn</span>';
                } else if (vps.isExpire == true) {
                    html += ' - <span class="text-danger">Gần hết hạn</span>';
                }
                html += '</td>';
                // Mật khẩu
                if (vps.password) {
                    html += '<td>' + vps.password + '</td>';
                } else {
                    html += '<td></td>';
                }
                // cau hinh
                html += '<td>' + vps.text_vps_config + '</td>';
                // loai
                if (vps.state != null) {
                    html += '<td>' + vps.state + '</td>';
                } else {
                    html += '<td>VPS US</td>';
                }
                // khach hang
                // html += '<td>' + vps.link_customer;
                // html += '<span><a href="#" class="text-secondary ml-2 button_edit_customer" data-id="'+ vps.id +'" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></a></span>';
                // html += '</td>';
                // ngay tao
                html += '<td><span>' + vps.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                if (vps.isExpire == true || vps.expired == true) {
                    html += '<span class="text-danger">' + vps.next_due_date + ' - ' + vps.text_day + '<span>';
                } else {
                    html += '<span>' + vps.next_due_date + ' - ' + vps.text_day + '<span>';
                }
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + vps.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + vps.text_billing_cycle + '</td>';
                // giá
                html += '<td>' + addCommas(vps.price_vps) + '</td>';
                html += '<td>';
                if (vps.description != null) {
                    html += '<span class="text-description">';
                    html += vps.description.slice(0, 40);
                    html += '</span>';
                }
                html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + vps.id + '"><i class="fas fa-edit"></i></a></span>';
                html += '</td>';
                // auto gia han
                html += '<td class="auto_refurn text-center">';
                if (vps.auto_refurn) {
                    html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Bật">';
                } else {
                    html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Tắt">';
                }
                html += '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">';
                if (vps.auto_refurn) {
                    html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps" checked>';
                } else {
                    html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps">';
                }
                html += '<label class="custom-control-label" for="customSwitch' + vps.id + '"></label>';
                html += '</div>';
                html += '</div>';
                html += '</td>';
                // trang thai
                html += '<td class="vps-status">' + vps.text_status_vps + '</td>';

                html += '<td class="page-service-action text-left">';
                if (vps.status_vps != 'suspend' && vps.status_vps != 'progressing' && vps.status_vps != 'rebuild' && vps.status_vps != 'change_ip' && vps.status_vps != 'reset_password') {
                    if (vps.expired == true) {
                        html += '<button type="button" class="btn mr-1 btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn VPS" data-action="expired" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-plus-circle"></i></button>';
                        html += '<button class="btn mr-1 bg-purple button-action-vps" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-action="change_user" data-id="' + vps.id + '"><i class="fas fa-exchange-alt"></i></button>';
                        if (vps.status_vps != 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ VPS" data-action="terminated" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                        }
                    } else if (vps.status_vps == 'expire') {
                        html += '<button type="button" class="btn mr-1 btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn VPS" data-action="expired" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-plus-circle"></i></button>';
                        html += '<button class="btn mr-1 bg-purple button-action-vps" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-action="change_user" data-id="' + vps.id + '"><i class="fas fa-exchange-alt"></i></button>';
                        if (vps.status_vps != 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ VPS" data-action="terminated" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                        }
                    } else {
                        html += '<button type="button" class="btn mr-1 btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn VPS" data-action="expired" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-plus-circle"></i></button>';
                        if (vps.status_vps == 'off' || vps.status_vps == 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-warning button-action-vps on" disabled data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                            html += '<button class="btn mr-1 btn-outline-success button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                        } else {
                            html += '<button class="btn mr-1 btn-outline-warning button-action-vps on" data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                            html += '<button class="btn mr-1 btn-outline-success button-action-vps off" data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                        }
                        if (vps.status_vps == 'on' || vps.status_vps == 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-info button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                        } else {
                            html += '<button class="btn mr-1 btn-outline-info button-action-vps off" data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                        }
                        html += '<button class="btn mr-1 btn-default button-action-vps off" data-toggle="tooltip" data-placement="top" title="Rebuild Vps" data-action="rebuild" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-history"></i></button>';
                        html += '<button class="btn mr-1 bg-purple button-action-vps" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-action="change_user" data-id="' + vps.id + '"><i class="fas fa-exchange-alt"></i></button>';
                        if (vps.status_vps != 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ VPS" data-action="terminated" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                        }
                        if (vps.status_vps == 'on' && vps.vm_id < 1000000000) {
                            html += '<a href="/services/vps/console/'+ vps.id +'" class="btn-sm btn btn-success btn-console" target="_blank" data-toggle="tooltip" data-placement="top" title="console"><i class="fas fa-desktop"></i></a>';
                        }
                    }
                }
                html += '</td>';
            } else {
                html += '<td class="text-danger text-center" colspan="14">VPS này không có hoặc không thuộc quyền sở hữu của quý khách.</td>';
            }
            html += '</tr>';
        })
        $('.list_vps tbody').html(html);
        $('.total-item').text(data.data.length);
        $('[data-toggle="tooltip"]').tooltip();
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="14" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_on">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="14" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_on">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('.list_vps tfoot').html(html_page);
        $vpsCheckbox = $('.vps_checkbox');
    }

    // ajax phân trang vps
    $(document).on('click', '.list_vps .pagination_vps_on a', function(event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var vps_q = $('#vps_search').val();
        var vps_sort = $('.sort_type').val();
        var sl = $('.vp_classic').val();
        $.ajax({
            url: '/services/search_vps_us',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl, page: page },
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_vps_screent(data);
                } else {
                    $('.list_vps tfoot').html('');
                    $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_vps tfoot').html('');
                $('.list_vps tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    });

    function list_vps_use_screent(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function(index, vps) {
            html += '<tr>';
            if (vps) {
                // checkbox
                html += '<td><input type="checkbox" value="' + vps.id + '" data-ip="' + vps.ip + '" class="vps_checkbox"></td>';
                // ip
                html += '<td class="ip_vps text-left" data-ip="' + vps.ip + '"><a href="/service/detail/' + vps.id + '?type=vps">' + vps.ip + '</a>';
                if (vps.expired == true) {
                    html += ' - <span class="text-danger">Hết hạn</span>';
                } else if (vps.isExpire == true) {
                    html += ' - <span class="text-danger">Gần hết hạn</span>';
                }
                html += '</td>';
                // Mật khẩu
                if (vps.password) {
                    html += '<td>' + vps.password + '</td>';
                } else {
                    html += '<td></td>';
                }
                // cau hinh
                html += '<td>' + vps.text_vps_config + '</td>';
                // loai
                if (vps.state != null) {
                    html += '<td>' + vps.state + '</td>';
                } else {
                    html += '<td>VPS US</td>';
                }
                // khach hang
                // html += '<td>' + vps.link_customer;
                // html += '<span><a href="#" class="text-secondary ml-2 button_edit_customer" data-id="'+ vps.id +'" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></a></span>';
                // html += '</td>';
                // ngay tao
                html += '<td><span>' + vps.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                if (vps.isExpire == true || vps.expired == true) {
                    html += '<span class="text-danger">' + vps.next_due_date + ' - ' + vps.text_day + '<span>';
                } else {
                    html += '<span>' + vps.next_due_date + ' - ' + vps.text_day + '<span>';
                }
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + vps.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + vps.text_billing_cycle + '</td>';
                // giá
                html += '<td>' + addCommas(vps.price_vps) + '</td>';
                html += '<td>';
                if (vps.description != null) {
                    html += '<span class="text-description">';
                    html += vps.description.slice(0, 40);
                    html += '</span>';
                }
                html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + vps.id + '"><i class="fas fa-edit"></i></a></span>';
                html += '</td>';
                // auto gia han
                html += '<td class="auto_refurn text-center">';
                if (vps.auto_refurn) {
                    html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Bật">';
                } else {
                    html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Tắt">';
                }
                html += '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">';
                if (vps.auto_refurn) {
                    html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps" checked>';
                } else {
                    html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps">';
                }
                html += '<label class="custom-control-label" for="customSwitch' + vps.id + '"></label>';
                html += '</div>';
                html += '</div>';
                html += '</td>';
                // trang thai
                html += '<td class="vps-status">' + vps.text_status_vps + '</td>';

                html += '<td class="page-service-action text-left">';
                if (vps.status_vps != 'suspend' && vps.status_vps != 'progressing' && vps.status_vps != 'rebuild' && vps.status_vps != 'change_ip' && vps.status_vps != 'reset_password') {
                    if (vps.expired == true) {
                        html += '<button type="button" class="btn mr-1 btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn VPS" data-action="expired" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-plus-circle"></i></button>';
                        html += '<button class="btn mr-1 bg-purple button-action-vps" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-action="change_user" data-id="' + vps.id + '"><i class="fas fa-exchange-alt"></i></button>';
                        if (vps.status_vps != 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ VPS" data-action="terminated" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                        }
                    } else {
                        html += '<button type="button" class="btn mr-1 btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn VPS" data-action="expired" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-plus-circle"></i></button>';
                        if (vps.status_vps == 'off' || vps.status_vps == 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-warning button-action-vps on" disabled data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                            html += '<button class="btn mr-1 btn-outline-success button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                        } else {
                            html += '<button class="btn mr-1 btn-outline-warning button-action-vps on" data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-power-off"></i></button>';
                            html += '<button class="btn mr-1 btn-outline-success button-action-vps off" data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-sync-alt"></i></button>';
                        }
                        if (vps.status_vps == 'on' || vps.status_vps == 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-info button-action-vps off" disabled data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                        } else {
                            html += '<button class="btn mr-1 btn-outline-info button-action-vps off" data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="far fa-stop-circle"></i></button>';
                        }
                        html += '<button class="btn mr-1 btn-default button-action-vps off" data-toggle="tooltip" data-placement="top" title="Rebuild Vps" data-action="rebuild" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-history"></i></button>';
                        html += '<button class="btn mr-1 bg-purple button-action-vps" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-action="change_user" data-id="' + vps.id + '"><i class="fas fa-exchange-alt"></i></button>';
                        if (vps.status_vps != 'cancel') {
                            html += '<button class="btn mr-1 btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ VPS" data-action="terminated" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                        }
                        if (vps.status_vps == 'on' && vps.vm_id < 1000000000) {
                            html += '<a href="/services/vps/console/'+ vps.id +'" class="btn-sm btn btn-success btn-console" target="_blank" data-toggle="tooltip" data-placement="top" title="console"><i class="fas fa-desktop"></i></a>';
                        }
                    }
                }
                html += '</td>';
            } else {
                html += '<td class="text-danger text-center" colspan="14">VPS này không có hoặc không thuộc quyền sở hữu của quý khách.</td>';
            }
            html += '</tr>';
        })
        $('.list_vps_use tbody').html(html);
        $('.total-item').text(data.data.length);
        $('[data-toggle="tooltip"]').tooltip();
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="14" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_on">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="14" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_on">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('.list_vps_use tfoot').html(html_page);
        $vpsCheckbox = $('.vps_checkbox');
    }

    // ajax phân trang vps
    $(document).on('click', '.list_vps_use .pagination_vps_on a', function(event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var vps_q = $('#vps_search_use').val();
        var vps_sort = $('.sort_type_use').val();
        var sl = $('.vp_classic_use').val();
        $.ajax({
            url: '/services/search_vps_us_use',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl, page: page },
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps_use tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_vps_screent(data);
                } else {
                    $('.list_vps_use tfoot').html('');
                    $('.list_vps_use tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_vps_use tfoot').html('');
                $('.list_vps_use tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    });
    //VPS HẾT HẠN
    // chọn sl
    $('.vp_nearly_classic').on('change', function() {
        var sl = $(this).val();
        $.ajax({
            url: '/services/list_vps_us_nearly_with_sl',
            data: { sl: sl },
            dataType: 'json',
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps_nearly tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_vps_nearly_screent(data);
                } else {
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_vps_nearly tfoot').html('');
                $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        });
    })

    $('#vps_nearly_search').on("keyup", function() {
        var vps_q = $(this).val();
        var sl = $('.vp_nearly_classic').val();
        var vps_sort = $('.sort_type').val();
        $.ajax({
            url: '/services/search_vps_us_nearly',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl },
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps_nearly tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_vps_nearly_screent(data);
                } else {
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_vps_nearly tfoot').html('');
                $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    $('.sort_next_due_date_nearly').on('click', function() {
        var vps_q = $('#vps_nearly_search').val();
        var sl = $('.vp_nearly_classic').val();
        var vps_sort = $(this).attr('data-sort');
        if (vps_sort == 'ASC') {
            $(this).attr('data-sort', 'DESC');
            $('.sort_type_nearly').val('ASC');
            $('.sort_next_due_date_nearly i').removeClass('fa-sort-down');
            $('.sort_next_due_date_nearly i').removeClass('fa-sort');
            $('.sort_next_due_date_nearly i').addClass('fa-sort-up');
        } else {
            $(this).attr('data-sort', 'ASC');
            $('.sort_type_nearly').val('DESC');
            $('.sort_next_due_date_nearly i').addClass('fa-sort-down');
            $('.sort_next_due_date_nearly i').removeClass('fa-sort');
            $('.sort_next_due_date_nearly i').removeClass('fa-sort-up');
        }
        $.ajax({
            url: '/services/search_vps_us_nearly',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl },
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps_nearly tbody').html(html);
            },
            success: function(data) {
                // console.log(data);
                if (data.data.length > 0) {
                    list_vps_nearly_screent(data);
                } else {
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    $('.list_vps_nearly tfoot').html('');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_vps_nearly tfoot').html('');
                $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    $('#btn_nearly_multi').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        var html = '';
        html += '<div class="input-group">';
        html += '<textarea class="form-control" aria-label="With textarea" id="form_search_multi" placeholder="Tìm kiếm nhiều VPS theo IP (Ví dụ: 192.168.1.1, 192.168.1.2)"></textarea>';
        html += '<div class="input-group-prepend" style="align-items: initial;" id="btn_nearly_search_multi">';
        html += '<span class="input-group-text btnOutlinePrimary">Tìm kiếm</span>';
        html += '</div>';
        html += '</div>';
        $('#nearly_search_multi').html(html);
    });

    $(document).on("click", '#btn_nearly_search_multi', function() {
        let text = $("#form_search_multi").val();
        let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
        let r2 = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}\b/;
        let ips, ip = '';
        if ( text.match(r2) ) {
            while (text.match(r2) != null) {
                ip = text.match(r2)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }
        if ( text.match(r) ) {
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }

        if (typeof(ips) !== 'undefined') {
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', ''); 
            console.log(ips);

            var vps_q = ips;
            ips = ips.replace(',', ' ');
            $("#form_search_multi").val(ips);
            $.ajax({
                url: '/services/search_multi_vps_us_nearly',
                dataType: 'json',
                data: { q: vps_q },
                beforeSend: function() {
                    var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_vps_nearly tbody').html(html);
                },
                success: function(data) {
                    // console.log(data);
                    if (data.data.length > 0) {
                        list_vps_nearly_screent(data);
                    } else {
                        $('.list_vps_nearly tfoot').html('');
                        $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    }
                },
                error: function(e) {
                    console.log(e);
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
        else {
            var vps_q = $("#form_search_multi").val();
            var sl = $('.vp_nearly_classic').val();
            var vps_sort = $('.sort_type').val();
            $.ajax({
                url: '/services/search_vps_us_nearly',
                dataType: 'json',
                data: { q: vps_q, sort: vps_sort, sl: sl },
                beforeSend: function() {
                    var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_vps_nearly tbody').html(html);
                },
                success: function(data) {
                    if (data.data.length > 0) {
                        list_vps_nearly_screent(data);
                    } else {
                        $('.list_vps_nearly tfoot').html('');
                        $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có VPS này trong dữ liệu.</td>');
                    }
                },
                error: function(e) {
                    console.log(e);
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
    })

    function list_vps_nearly_screent(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function(index, vps) {
            html += '<tr>';
            if (vps) {
                // checkbox
                html += '<td><input type="checkbox" value="' + vps.id + '" data-ip="' + vps.ip + '" class="vps_nearly_checkbox"></td>'
                    // ip
                html += '<td class="ip_vps text-left" data-ip="' + vps.ip + '"><a href="/service/detail/' + vps.id + '?type=vps">' + vps.ip + '</a>';
                html += '</td>';
                // Mật khẩu
                if (vps.password) {
                    html += '<td>' + vps.password + '</td>';
                } else {
                    html += '<td></td>';
                }
                // cau hinh
                html += '<td>' + vps.text_vps_config + '</td>';
                // loai
                if (vps.state != null) {
                    html += '<td>' + vps.state + '</td>';
                } else {
                    html += '<td>VPS US</td>';
                }
                // khach hang
                // html += '<td>' + vps.link_customer;
                // html += '<span><a href="#" class="text-secondary ml-2 button_edit_customer" data-id="'+ vps.id +'" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></a></span>';
                // html += '</td>';
                // ngay tao
                html += '<td><span>' + vps.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                html += '<span class="text-danger">' + vps.next_due_date + ' - ' + vps.text_day + '<span>';
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + vps.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + vps.text_billing_cycle + '</td>';
                // giá
                html += '<td>' + addCommas(vps.price_vps) + '</td>';
                // description
                html += '<td>';
                if (vps.description != null) {
                    html += '<span class="text-description">';
                    html += vps.description.slice(0, 40);
                    html += '</span>';
                }
                html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + vps.id + '"><i class="fas fa-edit"></i></a></span>';
                html += '</td>';
                // auto gia han
                html += '<td class="auto_refurn text-center">';
                if (vps.auto_refurn) {
                    html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Bật">';
                } else {
                    html += '<div class="form-group" style="padding-left: 10px;position: relative;" data-toggle="tooltip" data-placement="top" title="Tắt">';
                }
                html += '<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">';
                if (vps.auto_refurn) {
                    html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps" checked>';
                } else {
                    html += '<input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch' + vps.id + '" data-id="' + vps.id + '" data-type="vps">';
                }
                html += '<label class="custom-control-label" for="customSwitch' + vps.id + '"></label>';
                html += '</div>';
                html += '</div>';
                html += '</td>';
                html += '<td class="vps-status">' + vps.text_status_vps + '</td>';
                html += '<td class="page-service-action text-left">';
                if (vps.status_vps != 'delete_vps') {
                    if (vps.status_vps != 'cancel') {
                        if (vps.status_vps != 'suspend') {
                            html += '<button type="button" class="btn mr-1 btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn VPS" data-action="expired" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fas fa-plus-circle"></i></button>';
                            html += '<button class="btn mr-1 bg-purple button-action-vps" data-toggle="tooltip" data-placement="top" title="Chuyển khách hàng" data-action="change_user" data-id="' + vps.id + '"><i class="fas fa-exchange-alt"></i></button>';
                            html += '<button class="btn mr-1 btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ VPS" data-action="terminated" data-id="' + vps.id + '" data-ip="' + vps.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                        }
                    }
                }
                html += '</td>';
            } else {
                html += '<td class="text-danger text-center" colspan="14">VPS này không có hoặc không thuộc quyền sở hữu của quý khách.</td>';
            }
            html += '</tr>';
        })
        $('.list_vps_nearly tbody').html(html);
        $('.total-item').text(data.data.length);
        $('[data-toggle="tooltip"]').tooltip();
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="14" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_nearly">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="14" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_nearly">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('.list_vps_nearly tfoot').html(html_page);
        $vpsCheckboxNearly = $('.vps_nearly_checkbox');
    }

    // ajax phân trang vps
    $(document).on('click', '.pagination_vps_nearly a', function(event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var vps_q = $('#vps_nearly_search').val();
        var vps_sort = $('.sort_type').val();
        var sl = $('.vp_nearly_classic').val();
        $.ajax({
            url: '/services/search_vps_us_nearly',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl, page: page },
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps_nearly tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_vps_nearly_screent(data);
                } else {
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_vps_nearly tfoot').html('');
                $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    });

    // VPS HỦY
    // chọn sl
    $('.vp_cancel_classic').on('change', function() {
        var sl = $(this).val();
        var vps_q = $('#vps_cancel_search').val();
        $.ajax({
            url: '/services/search_vps_us_cancel',
            data: { sl: sl },
            dataType: 'json',
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps_nearly tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_vps_cancel_screent(data);
                } else {
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_vps_nearly tfoot').html('');
                $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        });
    })

    $('#vps_cancel_search').on("keyup", function() {
        var vps_q = $(this).val();
        var sl = $('.vp_cancel_classic').val();
        $.ajax({
            url: '/services/search_vps_us_cancel',
            dataType: 'json',
            data: { q: vps_q, sl: sl },
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps_nearly tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_vps_cancel_screent(data);
                } else {
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_vps_nearly tfoot').html('');
                $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })


    $('#btn_cancel_multi').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        var html = '';
        html += '<div class="input-group">';
        html += '<textarea class="form-control" aria-label="With textarea" id="form_search_multi" placeholder="Tìm kiếm nhiều VPS theo IP (Ví dụ: 192.168.1.1, 192.168.1.2)"></textarea>';
        html += '<div class="input-group-prepend" style="align-items: initial;" id="btn_cancel_search_multi">';
        html += '<span class="input-group-text btnOutlinePrimary">Tìm kiếm</span>';
        html += '</div>';
        html += '</div>';
        $('#cancel_search_multi').html(html);
    });

    $(document).on("click", '#btn_cancel_search_multi', function() {
        let text = $("#form_search_multi").val();
        let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/;
        let r2 = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}\b/;
        let ips, ip = '';
        if ( text.match(r2) ) {
            while (text.match(r2) != null) {
                ip = text.match(r2)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }
        if ( text.match(r) ) {
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }

        if (typeof(ips) !== 'undefined') {
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', ''); 
            console.log(ips);

            var vps_q = ips;
            ips = ips.replace(',', ' ');
            $("#form_search_multi").val(ips);
            $.ajax({
                url: '/services/search_multi_vps_us_cancel',
                dataType: 'json',
                data: { q: vps_q },
                beforeSend: function() {
                    var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_vps_nearly tbody').html(html);
                },
                success: function(data) {
                    // console.log(data);
                    if (data.data.length > 0) {
                        list_vps_cancel_screent(data);
                    } else {
                        $('.list_vps_nearly tfoot').html('');
                        $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    }
                },
                error: function(e) {
                    console.log(e);
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
        else {
            var vps_q = $("#form_search_multi").val();
            var sl = $('.vp_cancel_classic').val();
            $.ajax({
                url: '/services/search_vps_us_cancel',
                dataType: 'json',
                data: { q: vps_q, sl: sl },
                beforeSend: function() {
                    var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_vps_nearly tbody').html(html);
                },
                success: function(data) {
                    if (data.data.length > 0) {
                        list_vps_cancel_screent(data);
                    } else {
                        $('.list_vps_nearly tfoot').html('');
                        $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có VPS này trong dữ liệu.</td>');
                    }
                },
                error: function(e) {
                    console.log(e);
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
    })

    function list_vps_cancel_screent(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function(index, vps) {
            html += '<tr>';
            if (vps) {
                // checkbox
                html += '<td><input type="checkbox" value="' + vps.id + '" data-ip="' + vps.ip + '" class="vps_nearly_checkbox"></td>';
                // ip
                html += '<td class="ip_vps" data-ip="' + vps.ip + '"><a href="/service/detail/' + vps.id + '?type=vps">' + vps.ip + '</a>';
                html += '</td>';
                // cau hinh
                html += '<td>' + vps.text_vps_config + '</td>';
                // loai
                if (vps.state != null) {
                    html += '<td>' + vps.state + '</td>';
                } else {
                    html += '<td>VPS US</td>';
                }
                // khach hang
                // html += '<td>' + vps.link_customer;
                // html += '<span><a href="#" class="text-secondary ml-2 button_edit_customer" data-id="'+ vps.id +'" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></a></span>';
                // html += '</td>';
                // ngay tao
                html += '<td><span>' + vps.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                if (vps.isExpire == true || vps.expired == true) {
                    html += '<span class="text-danger">' + vps.next_due_date + '<span>';
                } else {
                    html += '<span>' + vps.next_due_date + '<span>';
                }
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + vps.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + vps.text_billing_cycle + '</td>';
                // giá
                html += '<td>' + addCommas(vps.price_vps) + ' VNĐ</td>';
                html += '<td>';
                if (vps.description != null) {
                    html += '<span class="text-description">';
                    html += vps.description.slice(0, 40);
                    html += '</span>';
                }
                html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + vps.id + '"><i class="fas fa-edit"></i></a></span>';
                html += '</td>';
                html += '<td class="vps-status">' + vps.text_status_vps + '</td>';
            } else {
                html += '<td class="text-danger text-center" colspan="14">VPS này không có hoặc không thuộc quyền sở hữu của quý khách.</td>';
            }
            html += '</tr>';
        })
        $('.list_vps_nearly tbody').html(html);
        $('[data-toggle="tooltip"]').tooltip();
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="14" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_cancel">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="14" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_vps_cancel">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('.list_vps_nearly tfoot').html(html_page);
    }

    // ajax phân trang vps
    $(document).on('click', '.pagination_vps_cancel a', function(event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var vps_q = $('#vps_cancel_search').val();
        var sl = $('.vp_cancel_classic').val();
        $.ajax({
            url: '/services/search_vps_us_cancel',
            dataType: 'json',
            data: { q: vps_q, sl: sl, page: page },
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_vps_nearly tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_vps_cancel_screent(data);
                } else {
                    $('.list_vps_nearly tfoot').html('');
                    $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_vps_nearly tfoot').html('');
                $('.list_vps_nearly tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    });

    $('.vps_all_classic').on('change', function() {
        var sl = $(this).val();
        $.ajax({
            url: '/services/list_all_vps_us_with_sl',
            data: { sl: sl },
            dataType: 'json',
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_all_vps tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_all_vps_screent(data);
                } else {
                    $('.list_all_vps tfoot').html('');
                    $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="14">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_all_vps tfoot').html('');
                $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        });
    })

    $('#all_vps_search').on("keyup", function() {
        var vps_q = $(this).val();
        var vps_sort = $('.sort_type').val();
        var sl = $('.vps_all_classic').val();
        // console.log(vps_q, vps_sort);
        $.ajax({
            url: '/services/all_vps_us_search',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl },
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_all_vps tbody').html(html);
            },
            success: function(data) {
                // console.log(data);
                if (data.data.length > 0) {
                    list_all_vps_screent(data);
                } else {
                    $('.list_all_vps tfoot').html('');
                    $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="14">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_all_vps tfoot').html('');
                $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    $('.sort_next_due_date_all_vps').on('click', function() {
        var vps_q = $('#all_vps_search').val();
        var sl = $('.vps_all_classic').val();
        var vps_sort = $(this).attr('data-sort');
        if (vps_sort == 'ASC') {
            $(this).attr('data-sort', 'DESC');
            $('.sort_type').val('ASC');
            $('.sort_next_due_date_all_vps i').removeClass('fa-sort-down');
            $('.sort_next_due_date_all_vps i').removeClass('fa-sort');
            $('.sort_next_due_date_all_vps i').addClass('fa-sort-up');
        } else {
            $(this).attr('data-sort', 'ASC');
            $('.sort_type').val('DESC');
            $('.sort_next_due_date_all_vps i').addClass('fa-sort-down');
            $('.sort_next_due_date_all_vps i').removeClass('fa-sort');
            $('.sort_next_due_date_all_vps i').removeClass('fa-sort-up');
        }
        $.ajax({
            url: '/services/all_vps_us_search',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl },
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="9"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_all_vps tbody').html(html);
            },
            success: function(data) {
                if (data.data.length > 0) {
                    list_all_vps_screent(data);
                } else {
                    $('.list_all_vps tfoot').html('');
                    $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="9">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_all_vps tfoot').html('');
                $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="9">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    })

    $('#btn_all_multi').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        var html = '';
        html += '<div class="input-group">';
        html += '<textarea class="form-control" aria-label="With textarea" id="form_search_multi" placeholder="Tìm kiếm nhiều VPS theo IP (Ví dụ: 192.168.1.1, 192.168.1.2)"></textarea>';
        html += '<div class="input-group-prepend" style="align-items: initial;" id="btn_all_search_multi">';
        html += '<span class="input-group-text btnOutlinePrimary">Tìm kiếm</span>';
        html += '</div>';
        html += '</div>';
        $('#all_search_multi').html(html);
    });

    $(document).on("click", '#btn_all_search_multi', function() {
        let text = $("#form_search_multi").val();
        let r = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\b/; //192.168.1.1
        let r2 = /\b\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\:\d{1,5}\b/;
        let ips, ip = '';
        if ( text.match(r2) ) {
            while (text.match(r2) != null) {
                ip = text.match(r2)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }
        if ( text.match(r) ) {
            while (text.match(r) != null) {
                ip = text.match(r)[0];
                ips += ip + ',';
                text = text.replace(ip, '');
            }
        }

        if (typeof(ips) !== 'undefined') {
            ips = ips.slice(0, -1);
            ips = ips.replace('undefined', ''); 
            console.log(ips);

            var vps_q = ips;
            ips = ips.replace(',', ' ');
            $("#form_search_multi").val(ips);
            $.ajax({
                url: '/services/search_multi_vps_us_all',
                dataType: 'json',
                data: { q: vps_q },
                beforeSend: function() {
                    var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_all_vps tbody').html(html);
                },
                success: function(data) {
                    // console.log(data);
                    if (data.data.length > 0) {
                        list_all_vps_screent(data);
                    } else {
                        $('.list_all_vps tfoot').html('');
                        $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="14">Không có dịch vụ VPS nào hết hạn hoặc hủy trong dữ liệu.</td>');
                    }
                },
                error: function(e) {
                    console.log(e);
                    $('.list_all_vps tfoot').html('');
                    $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
        else {
            var vps_q = $("#form_search_multi").val();
            var vps_sort = $('.sort_type').val();
            var sl = $('.vps_all_classic').val();
            // console.log(vps_q, vps_sort);
            $.ajax({
                url: '/services/all_vps_us_search',
                dataType: 'json',
                data: { q: vps_q, sort: vps_sort, sl: sl },
                beforeSend: function() {
                    var html = '<td class="text-center"  colspan="14"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                    $('.list_all_vps tbody').html(html);
                },
                success: function(data) {
                    // console.log(data);
                    if (data.data.length > 0) {
                        list_all_vps_screent(data);
                    } else {
                        $('.list_all_vps tfoot').html('');
                        $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="14">Không có VPS này trong dữ liệu.</td>');
                    }
                },
                error: function(e) {
                    console.log(e);
                    $('.list_all_vps tfoot').html('');
                    $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="14">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
                }
            })
        }
    })

    function list_all_vps_screent(data) {
        // console.log(data);
        var html = '';
        $.each(data.data, function(index, vps) {
            html += '<tr>';
            // checkbox
            if (vps) {
                // ip
                html += '<td class="ip_vps" data-ip="' + vps.ip + '"><a href="/service/detail/' + vps.id + '?type=vps">' + vps.ip + '</a>';
                html += '</td>';
                // cau hinh
                html += '<td>' + vps.text_vps_config + '</td>';
                // loai
                if (vps.state != null) {
                    html += '<td>' + vps.state + '</td>';
                } else {
                    html += '<td>VPS US</td>';
                }
                // khach hang
                // html += '<td>' + vps.link_customer;
                // html += '<span><a href="#" class="text-secondary ml-2 button_edit_customer" data-id="'+ vps.id +'" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></a></span>';
                // html += '</td>';
                // ngay tao
                html += '<td><span>' + vps.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                if (vps.isExpire == true || vps.expired == true) {
                    html += '<span class="text-danger">' + vps.next_due_date + '<span>';
                } else {
                    html += '<span>' + vps.next_due_date + '<span>';
                }
                html += '</td>';
                // tổng thời gian thuê
                html += '<td>' + vps.total_time + '</td>';
                // thoi gian thue
                html += '<td>' + vps.text_billing_cycle + '</td>';
                // giá
                // html += '<td>' + vps.price_vps + ' VNĐ</td>';
                html += '<td>';
                if (vps.description != null) {
                    html += '<span class="text-description">';
                    html += vps.description.slice(0, 40);
                    html += '</span>';
                }
                html += '<span><a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + vps.id + '"><i class="fas fa-edit"></i></a></span>';
                html += '</td>';
                html += '<td class="vps-status">' + vps.text_status_vps + '</td>';
            } else {
                html += '<td class="text-danger text-center" colspan="14">VPS này không có hoặc không thuộc quyền sở hữu của quý khách.</td>';
            }
            html += '</tr>';
        })
        $('.list_all_vps tbody').html(html);
        $('[data-toggle="tooltip"]').tooltip();
        // phân trang cho vps
        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="14" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_all">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="14" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_all">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            }
        }
        $('.list_all_vps tfoot').html(html_page);
    }

    // ajax phân trang vps
    $(document).on('click', '.pagination_all a', function(event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var vps_q = $('#vps_nearly_search').val();
        var vps_sort = $('.sort_type').val();
        var sl = $('.vp_nearly_classic').val();
        $.ajax({
            url: '/services/all_vps_us_search',
            dataType: 'json',
            data: { q: vps_q, sort: vps_sort, sl: sl, page: page },
            beforeSend: function() {
                var html = '<td class="text-center"  colspan="9"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('.list_all_vps tbody').html(html);
            },
            success: function(data) {
                // console.log(data);
                if (data.data.length > 0) {
                    list_all_vps_screent(data);
                } else {
                    $('.list_all_vps tfoot').html('');
                    $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="9">Không có VPS này trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('.list_all_vps tfoot').html('');
                $('.list_all_vps tbody').html('<td class="text-center text-danger"  colspan="9">Lỗi truy vấn dữ liệu dịch vụ VPS!</td>');
            }
        })
    });

    // update description
    $(document).on('click', '.button_edit_description', function functionName(event) {
        event.preventDefault();
        $('td').removeClass('choose-update-description');
        $(this).closest('td').addClass('choose-update-description');
        var id = $(this).attr('data-id');
        var token = $('meta[name="csrf-token"]').attr('content');
        var text = $('.choose-update-description .text-description').text();
        // console.log(text);
        var html = '';
        html += '<form id="form_update_description">';
        html += '<input type="hidden" name="_token" value="' + token + '">';
        html += '<input type="hidden" name="vps_id" value="' + id + '">';
        html += '<div class="form-group">';
        html += '<textarea rows="5" cols="12" maxlength="40" class="form-control description_area" name="description">' + text.trim() + '</textarea>'
        html += '</div>';
        html += '<div class="text-right mt-2">';
        html += '<button type="button" name="button" class="btn mr-1 btn-primary button_submit_update_description">Thay đổi</button>';
        html += '</div>';
        html += '</form>';
        $('.choose-update-description').html(html);
    });

    $(document).on('click', '.button_submit_update_description', function() {
        var form = $(this).closest('#form_update_description').serialize();
        var description = $(".description_area").val();
        $.ajax({
            url: '/services/updateDescription',
            data: form,
            type: 'post',
            dataType: 'json',
            beforeSend: function() {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.choose-update-description').html(html);
            },
            success: function(data) {
                if (data != '') {
                    var html = '';
                    html += '<span class="text-description">';
                    html += description;
                    html += '</span>';
                    html += '<span>';
                    html += '<a href="#" class="text-secondary ml-2 button_edit_description" data-id="' + data.id + '"><i class="fas fa-edit"></i></a>';
                    html += '</span>';
                    $('.choose-update-description').html(html);
                } else {
                    $('.choose-update-description').html('<span class="text-center">Cập nhật ghi chú không thành công.</span>');
                }
                $('td').removeClass('choose-update-description');
            },
            error: function(e) {
                console.log(e);
                $('.choose-update-description').html('<span class="text-center">Lỗi truy xuất VPS.</span>');
                $('td').removeClass('choose-update-description');
            }
        });
    });

    $('.btn_change_user').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        var checked = $('.vps_checkbox:checked');
        var list_vps = [];
        if (checked.length > 0) {
            $.each(checked, function(index, value) {
                list_vps.push($(this).val());
            })
            window.location.href = '/dich-vu/vps_us/chuyen-doi-khach-hang?list_vps=' + list_vps;
            // console.log('/dich-vu/vps/chuyen-doi-khach-hang?list_vps=' + list_vps);
        } else {
            alert('Vui lòng chọn một VPS.');
        }
    });


    $('.btn_nearly_change_user').on('click', function(event) {
        event.preventDefault();
        /* Act on the event */
        var checked = $('.vps_nearly_checkbox:checked');
        var list_vps = [];
        if (checked.length > 0) {
            $.each(checked, function(index, value) {
                list_vps.push($(this).val());
            })
            window.location.href = '/dich-vu/vps_us/chuyen-doi-khach-hang?list_vps=' + list_vps;
            // console.log('/dich-vu/vps/chuyen-doi-khach-hang?list_vps=' + list_vps);
        } else {
            alert('Vui lòng chọn một VPS.');
        }
    });

    function addCommas(nStr) {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

})
