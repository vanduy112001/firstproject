$(document).ready(function () {
    $('.button-confirm').on('click', function () {
        $('#confirm-order').modal('show');
        $('#button-confirm-order').fadeIn();
        $('#button-confirm-finish').hide();
        var id = $(this).attr('data-id');
        // console.log(id);
        $.ajax({
            type: "get",
            url: "/admin/orders/get_order_confirm",
            data: {id: id},
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-invoice').attr('disabled', true);
                $('#notication-confirm-order').html(html);
            },
            success: function (data) {
                // console.log(data);
                if(data != '' || data != null) {
                    var html = '';
                    html += '<form id="form_confirm">';
                    if(data.type == 'VPS') {
                        html += '<div class="form-group">';
                        html += '<label>Datacenter</label>';
                        html += '<input type="hidden" name="type" value="VPS">';
                        html += '<select id="data_center" name="data_center" class="form-control select2">';
                        html += '<option value="IID">IID</option>';
                        html += '<option value="CMC">CMC</option>';
                        html += '<option value="ODS">ODS</option>';
                        html += '</select>';
                        html += '</div>';
                        html += '<div class="form-group clearfix">';
                        html += '<div class="icheck-primary d-inline">';
                        html += '<label for="checkboxPrimary2" class="label-dropship">Dropship</label>';
                        html += '<input type="checkbox" id="checkboxPrimary2" name="dropship">';
                        html += '</div>';
                        html += '</div>';
                        html += '<div class="advanced">';
                        html += '<button class="btn btn-outline-success button-advanced" type="button">Nâng cao</button>';
                        html += '</div>';
                    } else if (data.type == 'Server') {
                        html += '<div class="text-center text-danger">';
                        html += 'Bạn có muốn xác nhận đơn hàng server vật lý này không?';
                        html += '<input type="hidden" name="type" value="Server">';
                        html += '</div>';
                    } else {
                        html += '<div class="text-center text-danger">';
                        html += 'Bạn có muốn xác nhận đơn hàng hosting này không?';
                        html += '<input type="hidden" name="type" value="Hosting">';
                        html += '</div>';
                    }
                    html += '<input type="hidden" name="id" id="modal-order-id" value="'+data.id+'">';
                    html += '</form>';
                    $('#content-confirm-order').html(html);
                } else {
                    $('#content-confirm-order').html('<div class="text-center text-danger">Không có dữ liệu order.</p>');
                }
                $('#notication-confirm-order').html('');
            }
        });
    });

    $(document).on('click', '.button-advanced' ,function () {
        var order_id = $('#modal-order-id').val();
        var dropship = $('#checkboxPrimary2').val();
        var data_center = $('#data_center').val();
        $.ajax({
            url: '/admin/vps/loadIPVPS',
            dataType: 'json',
            data: {id: order_id, dropship: dropship,data_center: data_center},
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.advanced').html(html);
            },
            success: function (data) {
                var html = '';
                var m = 0;
                if (data.ip != '' || data.ip.length > 0  ) {
                    // sau nay chỉnh sửa ip;
                } else {
                    for (var i = 0; i < data.detail_order.quantity; i++) {
                        m = i + 1;
                        html += '<div class="form-group">';
                        html += '<label>Địa chị IP VPS ';
                        if (data.detail_order.quantity > 1) {
                            html += m;
                        }
                        html +=  '</label>';
                        html += '<input class="form-control" name="ip[]" type="text" placeholder="Ip Address">';
                        html += '</div>';
                    }
                }
                $('.advanced').html(html);
            },
            error: function (e) {
                console.log(e);
                var html = '<p class="text-danger">Truy vấn nâng cao cho VPS thất bại</p>';
                $('.advanced').html(html);
            }
        });
    });

    $('#button-confirm-order').on('click', function (event) {
        event.preventDefault();
        var form_data = $('#form_confirm').serialize();
        $.ajax({
            type: "get",
            url: "/admin/orders/form_order_confirm",
            data: form_data,
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-confirm-order').attr('disabled', true);
                $('#notication-confirm-order').html(html);
            },
            success: function (data) {
                var html =  '';
                if (data) {
                    html += '<div class="text-center text-success">Xác nhận đơn hàng thành công.</div>';
                    $('#button-confirm-order').hide();
                    $('#button-confirm-finish').fadeIn();
                } else {
                    html += '<div class="text-center text-danger">Xác nhận đơn hàng không thành công.</div>';
                }
                $('#content-confirm-order').html(html);
                $('#button-confirm-order').attr('disabled', false);
                $('#notication-confirm-order').html('');
            },
            error: function (e) {
                console.log(e);
                var html = '<div class="text-center text-danger">Truy vấn đơn hàng thất bại</div>';
                $('#button-confirm-order').attr('disabled', false);
                $('#notication-confirm-order').html(html);
            }
        });
    });

    $(document).on('click', '.button_edit_total_order', function (event) {
       event.preventDefault();
       $('td').removeClass('edit_total');
       var token = $('meta[name="csrf-token"]').attr('content');
       var order_id = $(this).attr('data-id');
       $(this).closest('td').addClass('edit_total');
       var html = '';
       html += '<form method="post" action="/admin/orders/edit_total_order" id="form_edit_total">';
       html += '<input type="hidden" name="_token" value="'+ token +'">';
       html += '<input type="hidden" name="order_id" class="edit_order_id" value="'+ order_id +'">';
       html += '<input type="text" name="total" value="" class="total_order"  placeholder="Giá của đơn hàng">';
       html += '<input type="submit" value="Chỉnh sửa" class="btn btn-primary submit_form_edit_total ml-2">';
       html += '</form>';
       $('.edit_total').html(html);
    })

    $(document).on("submit", '#form_edit_total', function (event) {
        event.preventDefault();
        var form = $(this).serialize();
        var total = $('.total_order').val();
        var order_id = $('.edit_order_id').val();
        $.ajax({
          url: '/admin/orders/edit_total_order',
          type: 'post',
          dataType: 'json',
          data: form,
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('.submit_form_edit_total').attr('disabled', true);
              $('.edit_total').html(html);
          },
          success: function (data) {
              console.log(data);
              var html = '';
              if (data == true) {
                 html += addCommas(total) + ' VNĐ';
              } else {
                  html += '<span class="text-danger">Chỉnh sửa giá đơn đặt hàng thất bại</span>';
              }
              html += '<span class="ml-2">';
              html += '<a href="#" class="text-secondary ml-2 button_edit_total_order" data-id="'+ order_id +'" data-toggle="tooltip" title="Chỉnh sửa giá đơn đặt hàng"><i class="fas fa-edit"></i></a>';
              html += '</span>';
              $('.edit_total').html(html);
              $('[data-toggle="tooltip"]').tooltip();
              $('td').removeClass('edit_total');
          },
          error: function (e) {
            console.log(e);
            $('.edit_total').html('<span class="text-danger">Lỗi truy vấn đơn đặt hàng!</span>');
          }
        });
    })

    $(document).on('click', '.detail_invoice', function (event) {
        event.preventDefault();
        $('#detail_invoice').modal('show');
        var id = $(this).attr('data-id');
        $.ajax({
          url: '/admin/invoices/detail_invoice?id=' + id,
          dataType: 'json',
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#notication-detail').html(html);
          },
          success: function (data) {
              // console.log(data);
              var html = '';
              // Thông tin paid
              html += '<div class="detail_order text-center">';
              html += '<div class="paid">';
              if (data.status == 'paid') {
                html += '<span class="text-success">Đã thanh toán</span>';
              } else if (data.status == 'unpaid') {
                html += '<span class="text-danger">Chưa thanh toán</span>';
              } else {
                html += '<span class="text-default">Hủy</span>';
              }
              html += '</div>';
              html += '<div class="date_paid">';
              if (data.paid_date != '') {
                html += '<b class="text-success">' + data.paid_date + '</b>';
              } else {
                html += '<b class="text-danger">Chưa thanh toán</b>';
              }
              html += '</div>';
              html += '<div class="date_due">';
              html += 'Ngày đặt hàng: <b>' + data.date_create + '</b>';
              html += '</div>';
              html += '<div class="date_due">';
              if (data.status == 'unpaid') {
                html += 'Ngày hết hạn: <b>' + data.due_date + '</b>';
              }
              html += '</div>';
              html += '</div>';
              // Thông tin đơn đặt hàng
              html += '<div class="detail_order">';
              html += '<div>';
              html += '<h4>Thông tin dịch vụ / sản phẩm</h4>';
              html += '</div>';
              html += '<div class="m-3">';
              html += '<table class="table table-hover table-bordered">';
              if (data.type_order == 'expired') {
                html += '<thead class="primary">';
                html += '<th>Tên khách hàng</th><th>Dịch vụ</th><th>Cấu hình</th><th>Ngày đặt hàng</th><th>Thời gian</th><th>Giá</th>';
                html += '</thead>';
                html += '<tbody>';
                $.each(data.services , function(index, service) {
                    html += '<tr>';
                    html += '<td><a href="/admin/users/detail/'+ data.user_id +'">'+ data.username +'</a></td>';
                    html += '<td>';
                    if (data.type == 'VPS') {
                      html += 'Gia hạn VPS - ';
                      html += '<a href="/admin/vps/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    }
                    else if (data.type == 'VPS US' || data.type == ' US') {
                      html += 'Gia hạn VPS US - ';
                      html += '<a href="/admin/vps/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    }
                    else if (data.type == 'hosting') {
                      html += 'Gia hạn Hosting - ';
                      html += '<a href="/admin/hostings/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    }
                    else if (data.type == 'Email Hosting') {
                      html += 'Gia hạn Email Hosting - ';
                      html += '<a href="/admin/email_hosting/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    }
                    else if (data.type == 'Domain') {
                      html += 'Gia hạn Domain - ';
                      html += '<a href="/admin/domain/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    }
                    else if (data.type == 'Server') {
                      html += 'Gia hạn Server - ';
                      html += '<a href="/admin/server/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    }
                    else if (data.type == 'Colocation') {
                      html += 'Gia hạn Colocation - ';
                      html += '<a href="/admin/colocation/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    }
                    html += '</td>';
                    html += '<td>';
                    html += service.config;
                    html += '</td>';
                    html += '<td>';
                    html += data.date_create;
                    html += '</td>';
                    html += '<td>';
                    html += service.billing_cycle;
                    html += '</td>';
                    html += '<td>';
                    html += service.amount;
                    html += '</td>';
                    html += '</tr>';
                });
                html += '</tbody>';
              }
              else if (data.type_order == 'addon_vps') {
                html += '<thead class="primary">';
                html += '<th>Tên khách hàng</th><th>IP</th><th>CPU</th><th>RAM</th><th>DISK</th><th>Ngày đặt hàng</th><th>Thời gian</th><th>Giá</th>';
                html += '</thead>';
                html += '<tbody>';
                $.each(data.services , function(index, service) {
                  html += '<tr>';
                  html += '<td><a href="/admin/users/detail/'+ data.user_id +'">'+ data.username +'</a></td>';
                  html += '<td>';
                  html += 'Addon VPS - ';
                  html += '<a href="/admin/vps/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                  html += '</td>';
                  html += '<td>' + service.cpu + '</td>';
                  html += '<td>' + service.ram + '</td>';
                  html += '<td>' + service.disk + '</td>';
                  html += '<td>';
                  html += data.date_create;
                  html += '</td>';
                  html += '<td>';
                  html += service.time;
                  html += '</td>';
                  html += '<td>';
                  html += service.amount;
                  html += '</td>';
                  html += '</tr>';
                });
                html += '</tbody>';
              }
              else if (data.type_order == 'addon_server') {
                html += '<thead class="primary">';
                html += '<th>Tên khách hàng</th><th>IP</th><th>CPU</th><th>RAM</th><th>DISK</th><th>Thời gian</th><th>Giá</th>';
                html += '</thead>';
                html += '<tbody>';
                $.each(data.services , function(index, service) {
                  html += '<tr>';
                  html += '<td><a href="/admin/users/detail/'+ data.user_id +'">'+ data.username +'</a></td>';
                  html += '<td>';
                  html += 'Addon VPS - ';
                  html += '<a href="/admin/server/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                  html += '</td>';
                  html += '<td>' + service.ip + '</td>';
                  html += '<td>' + service.ram + '</td>';
                  html += '<td>' + service.disk + '</td>';
                  html += '<td>';
                  html += data.date_create;
                  html += '</td>';
                  html += '<td>';
                  html += service.amount;
                  html += '</td>';
                  html += '</tr>';
                });
                html += '</tbody>';
              }
              else if (data.type_order == 'change_ip') {
                html += '<thead class="primary">';
                html += '<th>Tên khách hàng</th><th>IP</th><th>Ngày đặt hàng</th><th>Giá</th>';
                html += '</thead>';
                html += '<tbody>';
                $.each(data.services , function(index, service) {
                  html += '<tr>';
                  html += '<td><a href="/admin/users/detail/'+ data.user_id +'">'+ data.username +'</a></td>';
                  html += '<td>';
                  html += 'Đổi IP VPS - ';
                  html += '<a href="/admin/vps/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                  html += '</td>';
                  html += '<td>';
                  html += data.date_create;
                  html += '</td>';
                  html += '<td>';
                  html += service.amount;
                  html += '</td>';
                  html += '</tr>';
                });
                html += '</tbody>';
              }
              else if (data.type_order == 'upgrade_hosting') {
                html += '<thead class="primary">';
                html += '<th>Tên khách hàng</th><th>Hosting</th><th>Nâng cấp gói Hosting</th><th>Ngày đặt hàng</th><th>Giá</th>';
                html += '</thead>';
                html += '<tbody>';
                html += '<tr>';
                html += '<td><a href="/admin/users/detail/'+ data.user_id +'">'+ data.username +'</a></td>';
                html += '<td>';
                html += 'Nâng cấp Hosting - ';
                html += '<a href="/admin/vps/detail/'+ data.services.sevice_id +'">'+ data.services.sevice_name +'</a>';
                html += '</td>';
                html += '<td>' + data.services.upgrade + '</td>';
                html += '<td>';
                html += data.date_create;
                html += '</td>';
                html += '<td>';
                html += data.services.amount;
                html += '</td>';
                html += '</tr>';
                html += '</tbody>';
              }
              else {
                html += '<thead class="primary">';
                html += '<th>Tên khách hàng</th><th>Dịch vụ</th><th>Cấu hình</th><th>Ngày đặt hàng</th><th>Thời gian</th><th>Giá</th>';
                html += '</thead>';
                html += '<tbody>';
                $.each(data.services , function(index, service) {
                    html += '<tr>';
                    html += '<td><a href="/admin/users/detail/'+ data.user_id +'">'+ data.username +'</a></td>';
                    html += '<td>';
                    if (data.type == 'VPS' || data.type == 'NAT-VPS' || data.type == 'VPS-US' || data.type == 'VPS US'  || data.type == ' US') {
                      html += 'Tạo '+ data.type +' - ';
                      html += '<a href="/admin/vps/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    }
                    else if (data.type == 'Hosting' || data.type == 'Hosting-Singapore') {
                      html += 'Tạo '+ data.type +' - ';
                      html += '<a href="/admin/hostings/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    }
                    else if (data.type == 'Email Hosting') {
                      html += 'Tạo '+ data.type +' - ';
                      html += '<a href="/admin/hostings/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    }
                    else if (data.type == 'Domain') {
                      html += 'Tạo Domain - ';
                      html += '<a href="/admin/domain/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    }
                    else if (data.type == 'Server') {
                      html += 'Tạo Server - ';
                      html += '<a href="/admin/servers/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    }
                    else if (data.type == 'Colocation') {
                      html += 'Tạo Colocation - ';
                      html += '<a href="/admin/colocation/detail/'+ service.sevice_id +'">'+ service.sevice_name +'</a>';
                    }
                    html += '</td>';
                    html += '<td>';
                    html += service.config;
                    html += '</td>';
                    html += '<td>';
                    html += data.date_create;
                    html += '</td>';
                    html += '<td>';
                    html += service.billing_cycle;
                    html += '</td>';
                    html += '<td>';
                    html += service.amount;
                    html += '</td>';
                    html += '</tr>';
                });
                html += '</tbody>';
              }
              html += '</table>';
              html += '</div>';
              html += '</div>';
              $('#notication-detail').html(html);
          },
          error: function (e) {
            console.log(e);
            $('#notication-detail').html('<div  class="text-center"><span>Lỗi truy vấn Detail Order!</span></div>');
          },
        });
    });

    $(document).on('click', '#user_invoice span' ,function () {
        $.ajax({
          url: '/admin/users/list_all_user',
          dataType: 'json',
          success: function (data) {
              var html = '';
              html += '<select style="width: 100%;">';
              html += '<option value="" selected disabled>Chọn khách hàng</option>';
              $.each(data, function (index, user) {
                html += '<option value="'+ user.id +'">'+ user.name +'</option>';
              })
              html += '</select>';
              $('#user_invoice').html(html);
          },
          error: function (e) {
            console.log(e);
            var html = '';
            html += 'Lỗi truy vấn';
            html += '<span class="float-right">';
            html += '<i class="fas fa-filter"></i>';
            html += '<input type="hidden" id="input_filter_user" value="">';
            html += '</span>';
            $('#user_invoice span').html(html);
          }
        })
    });

    $(document).on('change', '#user_invoice select', function () {
        // console.log( 'da click', $('#type_invoice select :selected').val(), $('#type_invoice select :selected').text() );
        var user_id = $('#user_invoice select :selected').val();
        var user_name = $('#user_invoice select :selected').text();
        var status = $('#input_filter_status').val();
        $.ajax({
          url: '/admin/orders/filter_user',
          data: {user_id: user_id, status: status},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              // console.log(data);
              if (data.data != '') {
                  var html = '';
                  $.each(data.data, function(index , invoice) {
                      html += '<tr>';
                      if (invoice.deeta != 0) {
                        html += '<td><a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="detail_invoice" data-id="'+ invoice.id_invoice +'">'+ invoice.id +'</a></td>';
                      } else {
                        html += '<td>'+ invoice.id +'</td>';
                      }
                      html += '<td>'+ invoice.date_create +'</td>';
                      html += '<td><a href="/admin/users/detail/'+ invoice.user_id +'">'+ invoice.username +'</a></td>';
                      html += '<td>'+ invoice.total +'</td>';
                      html += '<td>' + invoice.text_type_order + '</td>';
                      html += '<td>';
                      if (invoice.status == 'Finish') {
                        html += '<span class="text-success">Đã thanh toán</span>';
                      }
                      else if (invoice.status == 'Pending') {
                        html += '<span class="text-danger">Chưa thanh toán</span>';
                      }
                      else if (invoice.status == 'Active') {
                        html += '<span class="text-info">Đang chờ tạo</span>';
                      }
                      else {
                        html += '<span class="text-default">Đã hủy</span>';
                      }
                      html += '</td>';
                      html += '<td class="button-action">';
                      if (invoice.deeta != 0) {
                        html += '<a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="btn btn-warning text-white detail_invoice" data-id="'+ invoice.id_invoice +'"><i class="fas fa-edit"></i></a>';
                      }
                      html += '<button class="btn btn-danger btn-delete-invoices" data-id="' + invoice.id + '"><i class="fas fa-trash-alt"></i></button>';
                      html += '</td>';
                      html += '</tr>';
                  })
                  $('tbody').html(html);
                  $('tfoot').html('');
                  var html_th_type  = '';
                  html_th_type +=  'Khách hàng ' + user_name;
                  html_th_type += '<span class="float-right"><i class="fas fa-filter"></i>';
                  html_th_type += '<input type="hidden" id="input_filter_user" value="' + user_id + '">';
                  html_th_type += '</span>';
                  $('#user_invoice').html(html_th_type);
                  // phân trang cho vps
                  var total = data.total;
                  var per_page = data.perPage;
                  var current_page = data.current_page;
                  var html_page = '';
                    if (total > per_page) {
                      if ( total / per_page > 11 ) {
                        var page = parseInt(total/per_page + 1);
                        html_page += '<td colspan="11" class="text-center link-right">';
                        html_page += '<nav>';
                        html_page += '<ul class="pagination user_invoice_paginate">';
                        if (current_page != 1) {
                          html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                        } else {
                          html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                        }
                        if (current_page < 7) {
                          for (var i = 1; i < 9; i++) {
                            var active = '';
                            if (i == current_page) {
                              active = 'active';
                            }
                            if (active == 'active') {
                              html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            } else {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }
                          }
                          html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                          for (var i = page - 1; i <= page; i++) {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                          }
                        }
                        else if (current_page >= 7 || current_page <= page - 7) {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                            html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                            for (var i = current_page - 3; i <= current_page +3; i++) {
                              var active = '';
                              if (i == current_page) {
                                active = 'active';
                              }
                              if (active == 'active') {
                                html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                              } else {
                                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                              }
                            }
                            html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                            for (var i = page - 1; i <= page; i++) {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }
                        }
                        else if (current_page >= page - 6) {
                          html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                          html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                          html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                          for (var i = page - 6; i < page; i++) {
                            var active = '';
                            if (i == current_page) {
                              active = 'active';
                            }
                            if (active == 'active') {
                              html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            } else {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }
                          }
                        }

                        if (current_page != page.toPrecision(1)) {
                          html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                        } else {
                          html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                        }
                        html_page += '</ul>';
                        html_page += '</nav>';
                        html_page += '</td>';
                      } else {
                        var page = total/per_page + 1;
                        html_page += '<td colspan="11" class="text-center link-right">';
                        html_page += '<nav>';
                        html_page += '<ul class="pagination user_invoice_paginate">';
                        if (current_page != 1) {
                            html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                        } else {
                            html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                        }
                        for (var i = 1; i < page; i++) {
                            var active = '';
                            if (i == current_page) {
                                active = 'active';
                            }
                            if (active == 'active') {
                              html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            } else {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }

                        }
                        if (current_page != page.toPrecision(1)) {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                        } else {
                            html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                        }
                        html_page += '</ul>';
                        html_page += '</nav>';
                        html_page += '</td>';
                      }
                  }
                  $('tfoot').html(html_page);
              } else {
                  var html = '<td colspan="9" class="text-center">Không có đơn hàng của khách hàng này trong dữ liệu!</td>';
                  $('tbody').html(html);
                  $('tfoot').html('');
                  var html_th_type  = '';
                  html_th_type +=  'Khách hàng ' + user_name;
                  html_th_type += '<span class="float-right"><i class="fas fa-filter"></i>';
                  html_th_type += '<input type="hidden" id="input_filter_user" value="' + user_id + '">';
                  html_th_type += '</span>';
                  $('#user_invoice').html(html_th_type);
              }
          },
          error: function (e) {
              console.log(e);
              var html = '<td colspan="10" class="text-center">Truy vấn đơn hàng lỗi!</td>';
              $('tbody').html(html);
          }
        })
    });

    $(document).on('click', '#status_invoice span' ,function () {
      var html = '';
      html += '<select style="width: 100%;">';
      html += '<option value="" selected disabled>Chọn trạng thái</option>';
      html += '<option value="Finish">Đã thanh toán</option>';
      html += '<option value="Active">Đang chờ tạo</option>';
      html += '<option value="Pending">Chưa thanh toán</option>';
      html += '<option value="cancel">Hủy</option>';
      html += '</select>';
      $('#status_invoice').html(html);
    });

    $(document).on('change', '#status_invoice select', function () {
        // console.log( 'da click', $('#type_invoice select :selected').val(), $('#type_invoice select :selected').text() );
        var status = $('#status_invoice select :selected').val();
        var status_text = $('#status_invoice select :selected').text();
        var user_id = $('#input_filter_user').val();
        $.ajax({
          url: '/admin/orders/filter_status',
          data: {user_id: user_id, status: status},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              // console.log(data);
              if (data.data != '') {
                  var html = '';
                  $.each(data.data, function(index , invoice) {
                      html += '<tr>';
                      if (invoice.deeta != 0) {
                        html += '<td><a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="detail_invoice" data-id="'+ invoice.id_invoice +'">'+ invoice.id +'</a></td>';
                      } else {
                        html += '<td>'+ invoice.id +'</td>';
                      }
                      html += '<td>'+ invoice.date_create +'</td>';
                      html += '<td><a href="/admin/users/detail/'+ invoice.user_id +'">'+ invoice.username +'</a></td>';
                      html += '<td>'+ invoice.total +'</td>';
                      html += '<td>' + invoice.text_type_order + '</td>';
                      html += '<td>';
                      if (invoice.status == 'Finish') {
                        html += '<span class="text-success">Đã thanh toán</span>';
                      }
                      else if (invoice.status == 'Pending') {
                        html += '<span class="text-danger">Chưa thanh toán</span>';
                      }
                      else if (invoice.status == 'Active') {
                        html += '<span class="text-info">Đang chờ tạo</span>';
                      }
                      else {
                        html += '<span class="text-default">Đã hủy</span>';
                      }
                      html += '</td>';
                      html += '<td class="button-action">';
                      if (invoice.deeta != 0) {
                        html += '<a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="btn btn-warning text-white detail_invoice" data-id="'+ invoice.id_invoice +'"><i class="fas fa-edit"></i></a>';
                      }
                      html += '<button class="btn btn-danger btn-delete-invoices" data-id="' + invoice.id + '"><i class="fas fa-trash-alt"></i></button>';
                      html += '</td>';
                      html += '</tr>';
                  })
                  $('tbody').html(html);
                  $('tfoot').html('');
                  var html_th_type  = '';
                  html_th_type +=  'Trạng thái ' + status_text;
                  html_th_type += '<span class="float-right"><i class="fas fa-filter"></i>';
                  html_th_type += '<input type="hidden" id="input_filter_status" value="' + status + '">';
                  html_th_type += '</span>';
                  $('#status_invoice').html(html_th_type);
                  // phân trang cho vps
                  var total = data.total;
                  var per_page = data.perPage;
                  var current_page = data.current_page;
                  var html_page = '';
                  if (total > per_page) {
                    if ( total / per_page > 11 ) {
                      var page = parseInt(total/per_page + 1);
                      html_page += '<td colspan="11" class="text-center link-right">';
                      html_page += '<nav>';
                      html_page += '<ul class="pagination status_invoice_paginate">';
                      if (current_page != 1) {
                        html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                      } else {
                        html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                      }
                      if (current_page < 7) {
                        for (var i = 1; i < 9; i++) {
                          var active = '';
                          if (i == current_page) {
                            active = 'active';
                          }
                          if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                          } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                          }
                        }
                        html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                        for (var i = page - 1; i <= page; i++) {
                          html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                        }
                      }
                      else if (current_page >= 7 || current_page <= page - 7) {
                          html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                          html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                          html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                          for (var i = current_page - 3; i <= current_page +3; i++) {
                            var active = '';
                            if (i == current_page) {
                              active = 'active';
                            }
                            if (active == 'active') {
                              html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            } else {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }
                          }
                          html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                          for (var i = page - 1; i <= page; i++) {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                          }
                      }
                      else if (current_page >= page - 6) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                        html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                        for (var i = page - 6; i < page; i++) {
                          var active = '';
                          if (i == current_page) {
                            active = 'active';
                          }
                          if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                          } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                          }
                        }
                      }

                      if (current_page != page.toPrecision(1)) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                      } else {
                        html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                      }
                      html_page += '</ul>';
                      html_page += '</nav>';
                      html_page += '</td>';
                    } else {
                      var page = total/per_page + 1;
                      html_page += '<td colspan="11" class="text-center link-right">';
                      html_page += '<nav>';
                      html_page += '<ul class="pagination status_invoice_paginate">';
                      if (current_page != 1) {
                          html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                      } else {
                          html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                      }
                      for (var i = 1; i < page; i++) {
                          var active = '';
                          if (i == current_page) {
                              active = 'active';
                          }
                          if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                          } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                          }

                      }
                      if (current_page != page.toPrecision(1)) {
                          html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                      } else {
                          html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                      }
                      html_page += '</ul>';
                      html_page += '</nav>';
                      html_page += '</td>';
                    }
                  }
                  $('tfoot').html(html_page);
              } else {
                  var html = '<td colspan="9" class="text-center">Không có đơn hàng của khách hàng này trong dữ liệu!</td>';
                  $('tbody').html(html);
                  $('tfoot').html('');
                  var html_th_type  = '';
                  html_th_type +=  'Trạng thái ' + status_text;
                  html_th_type += '<span class="float-right"><i class="fas fa-filter"></i>';
                  html_th_type += '<input type="hidden" id="input_filter_status" value="' + status + '">';
                  html_th_type += '</span>';
                  $('#status_invoice').html(html_th_type);
              }
          },
          error: function (e) {
              console.log(e);
              var html = '<td colspan="10" class="text-center">Truy vấn đơn hàng lỗi!</td>';
              $('tbody').html(html);
          }
        })
    });

    // ajax phân trang vps
    $(document).on('click', '#status_invoice_paginate a', function(event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        $.ajax({
            url: '/admin/orders/filter_status',
            data: {page: page},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td class="text-center"  colspan="9"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data.length > 0) {
                   // console.log('da den 1');
                    var html = '';
                    $.each(data.data, function(index , invoice) {
                        html += '<tr>';
                        if (invoice.deeta != 0) {
                          html += '<td><a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="detail_invoice" data-id="'+ invoice.id_invoice +'">'+ invoice.id +'</a></td>';
                        } else {
                          html += '<td>'+ invoice.id +'</td>';
                        }
                        html += '<td>'+ invoice.date_create +'</td>';
                        html += '<td><a href="/admin/users/detail/'+ invoice.user_id +'">'+ invoice.username +'</a></td>';
                        html += '<td>'+ invoice.total +'</td>';
                        html += '<td>' + invoice.text_type_order + '</td>';
                        html += '<td>';
                        if (invoice.status == 'Finish') {
                          html += '<span class="text-success">Đã thanh toán</span>';
                        }
                        else if (invoice.status == 'Pending') {
                          html += '<span class="text-danger">Chưa thanh toán</span>';
                        }
                        else if (invoice.status == 'Active') {
                          html += '<span class="text-info">Đang chờ tạo</span>';
                        }
                        else {
                          html += '<span class="text-default">Đã hủy</span>';
                        }
                        html += '</td>';
                        html += '<td class="button-action">';
                        if (invoice.deeta != 0) {
                          html += '<a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="btn btn-warning text-white detail_invoice" data-id="'+ invoice.id_invoice +'"><i class="fas fa-edit"></i></a>';
                        }
                        html += '<button class="btn btn-danger btn-delete-invoices" data-id="' + invoice.id + '"><i class="fas fa-trash-alt"></i></button>';
                        html += '</td>';
                        html += '</tr>';
                    })
                    $('tbody').html(html);
                    $('tfoot').html('');
                    // phân trang cho vps
                    var total = data.total;
                    var per_page = data.perPage;
                    var current_page = data.current_page;
                    var html_page = '';
                    if (total > per_page) {
                        var page = total/per_page + 1;
                        html_page += '<td colspan="7" class="text-center">';
                        html_page += '<ul class="pagination" id="status_invoice_paginate">';
                        if (current_page != 1) {
                            html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                        } else {
                            html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                        }
                        for (var i = 1; i < page; i++) {
                            var active = '';
                            if (i == current_page) {
                                active = 'active';
                            }
                            if (active == 'active') {
                              html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            } else {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }

                        }
                        if (current_page != page.toPrecision(1)) {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                        } else {
                            html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                        }
                        html_page += '</ul>';
                        html_page += '</td>';
                    }
                    $('tfoot').html(html_page);
                } else {
                   // console.log('da den 2');
                    var html = '<td colspan="9" class="text-center">Không có đơn hàng của khách hàng này trong dữ liệu!</td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                }
            },
            error: function (e) {
               console.log(e);
               var html = '<td colspan="9" class="text-center">Truy vấn đơn hàng lỗi!</td>';
               $('tbody').html(html);
           }
        });
    });

    // ajax phân trang vps
    $(document).on('click', '#user_invoice_paginate a', function(event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        $.ajax({
            url: '/admin/orders/filter_user',
            data: {page: page},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td class="text-center"  colspan="9"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                if (data.data.length > 0) {
                   // console.log('da den 1');
                    var html = '';
                    $.each(data.data, function(index , invoice) {
                        html += '<tr>';
                        if (invoice.deeta != 0) {
                          html += '<td><a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="detail_invoice" data-id="'+ invoice.id_invoice +'">'+ invoice.id +'</a></td>';
                        } else {
                          html += '<td>'+ invoice.id +'</td>';
                        }
                        html += '<td>'+ invoice.date_create +'</td>';
                        html += '<td><a href="/admin/users/detail/'+ invoice.user_id +'">'+ invoice.username +'</a></td>';
                        html += '<td>'+ invoice.total +'</td>';
                        html += '<td>' + invoice.text_type_order + '</td>';
                        html += '<td>';
                        if (invoice.status == 'Finish') {
                          html += '<span class="text-success">Đã thanh toán</span>';
                        }
                        else if (invoice.status == 'Pending') {
                          html += '<span class="text-danger">Chưa thanh toán</span>';
                        }
                        else if (invoice.status == 'Active') {
                          html += '<span class="text-info">Đang chờ tạo</span>';
                        }
                        else {
                          html += '<span class="text-default">Đã hủy</span>';
                        }
                        html += '</td>';
                        html += '<td class="button-action">';
                        if (invoice.deeta != 0) {
                          html += '<a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="btn btn-warning text-white detail_invoice" data-id="'+ invoice.id_invoice +'"><i class="fas fa-edit"></i></a>';
                        }
                        html += '<button class="btn btn-danger btn-delete-invoices" data-id="' + invoice.id + '"><i class="fas fa-trash-alt"></i></button>';
                        html += '</td>';
                        html += '</tr>';
                    })
                    $('tbody').html(html);
                    $('tfoot').html('');
                    // phân trang cho vps
                    var total = data.total;
                    var per_page = data.perPage;
                    var current_page = data.current_page;
                    var html_page = '';
                    if (total > per_page) {
                        var page = total/per_page + 1;
                        html_page += '<td colspan="7" class="text-center">';
                        html_page += '<ul class="pagination" id="user_invoice_paginate">';
                        if (current_page != 1) {
                            html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                        } else {
                            html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                        }
                        for (var i = 1; i < page; i++) {
                            var active = '';
                            if (i == current_page) {
                                active = 'active';
                            }
                            if (active == 'active') {
                              html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            } else {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }

                        }
                        if (current_page != page.toPrecision(1)) {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                        } else {
                            html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                        }
                        html_page += '</ul>';
                        html_page += '</td>';
                    }
                    $('tfoot').html(html_page);
                } else {
                   // console.log('da den 2');
                    var html = '<td colspan="9" class="text-center">Không có đơn hàng của khách hàng này trong dữ liệu!</td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                }
            },
            error: function (e) {
               console.log(e);
               var html = '<td colspan="9" class="text-center">Truy vấn đơn hàng lỗi!</td>';
               $('tbody').html(html);
           }
        });
    });


    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

    $('#type_user').on('click', function () {
        $.ajax({
           url: '/admin/orders/list_order_of_personal',
           dataType: 'json',
           beforeSend: function(){
               var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('tbody').html(html);
           },
           success: function (data) {
                console.log(data.data);
               if (data.data.length > 0) {
                  // console.log('da den 1');
                   var html = '';
                   $.each(data.data, function(index , invoice) {
                       html += '<tr>';
                       if (invoice.deeta != 0) {
                         html += '<td><a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="detail_invoice" data-id="'+ invoice.id_invoice +'">'+ invoice.id +'</a></td>';
                       } else {
                         html += '<td>'+ invoice.id +'</td>';
                       }
                       html += '<td>'+ invoice.date_create +'</td>';
                       html += '<td><a href="/admin/users/detail/'+ invoice.user_id +'">'+ invoice.username +'</a></td>';
                       html += '<td>'+ invoice.total +'</td>';
                       html += '<td>' + invoice.text_type_order + '</td>';
                       html += '<td>';
                       if (invoice.status == 'Finish') {
                         html += '<span class="text-success">Đã thanh toán</span>';
                       }
                       else if (invoice.status == 'Pending') {
                         html += '<span class="text-danger">Chưa thanh toán</span>';
                       }
                       else if (invoice.status == 'Active') {
                         html += '<span class="text-info">Đang chờ tạo</span>';
                       }
                       else {
                         html += '<span class="text-default">Đã hủy</span>';
                       }
                       html += '</td>';
                       html += '<td class="button-action">';
                       if (invoice.deeta != 0) {
                         html += '<a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="btn btn-warning text-white detail_invoice" data-id="'+ invoice.id_invoice +'"><i class="fas fa-edit"></i></a>';
                       }
                       html += '<button class="btn btn-danger btn-delete-invoices" data-id="' + invoice.id + '"><i class="fas fa-trash-alt"></i></button>';
                       html += '</td>';
                       html += '</tr>';
                   })
                   $('tbody').html(html);
                   $('tfoot').html('');
                   // phân trang cho vps
                   var total = data.total;
                   var per_page = data.perPage;
                   var current_page = data.current_page;
                   var html_page = '';
                   if (total > per_page) {
                       var page = total/per_page + 1;
                       html_page += '<td colspan="7" class="text-center">';
                       html_page += '<ul class="pagination" id="pagination_personal_order">';
                       if (current_page != 1) {
                           html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                       } else {
                           html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                       }
                       for (var i = 1; i < page; i++) {
                           var active = '';
                           if (i == current_page) {
                               active = 'active';
                           }
                           if (active == 'active') {
                             html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                           } else {
                             html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                           }

                       }
                       if (current_page != page.toPrecision(1)) {
                           html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                       } else {
                           html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                       }
                       html_page += '</ul>';
                       html_page += '</td>';
                   }
                   $('tfoot').html(html_page);
               } else {
                  // console.log('da den 2');
                   var html = '<td colspan="9" class="text-center">Không có đơn hàng của khách hàng này trong dữ liệu!</td>';
                   $('tbody').html(html);
                   $('tfoot').html('');
               }
           },
           error: function (e) {
                console.log(e);
               var html = '<td colspan="9" class="text-center">Truy vấn đơn hàng lỗi!</td>';
               $('tbody').html(html);
           }
        })
    })

    // ajax phân trang vps
    $(document).on('click', '#pagination_personal_order a', function(event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        $.ajax({
            url: '/admin/orders/list_order_of_personal',
            data: {page: page},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td class="text-center"  colspan="9"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                   // console.log(data);
                if (data.data.length > 0) {
                    var html = '';
                    $.each(data.data, function(index , invoice) {
                        html += '<tr>';
                        if (invoice.deeta != 0) {
                          html += '<td><a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="detail_invoice" data-id="'+ invoice.id_invoice +'">'+ invoice.id +'</a></td>';
                        } else {
                          html += '<td>'+ invoice.id +'</td>';
                        }
                        html += '<td>'+ invoice.date_create +'</td>';
                        html += '<td><a href="/admin/users/detail/'+ invoice.user_id +'">'+ invoice.username +'</a></td>';
                        html += '<td>'+ invoice.total +'</td>';
                        html += '<td>' + invoice.text_type_order + '</td>';
                        html += '<td>';
                        if (invoice.status == 'Finish') {
                          html += '<span class="text-success">Đã thanh toán</span>';
                        }
                        else if (invoice.status == 'Pending') {
                          html += '<span class="text-danger">Chưa thanh toán</span>';
                        }
                        else if (invoice.status == 'Active') {
                          html += '<span class="text-info">Đang chờ tạo</span>';
                        }
                        else {
                          html += '<span class="text-default">Đã hủy</span>';
                        }
                        html += '</td>';
                        html += '<td class="button-action">';
                        if (invoice.deeta != 0) {
                          html += '<a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="btn btn-warning text-white detail_invoice" data-id="'+ invoice.id_invoice +'"><i class="fas fa-edit"></i></a>';
                        }
                        html += '<button class="btn btn-danger btn-delete-invoices" data-id="' + invoice.id + '"><i class="fas fa-trash-alt"></i></button>';
                        html += '</td>';
                        html += '</tr>';
                    })
                    $('tbody').html(html);
                    $('tfoot').html('');
                    // phân trang cho vps
                    var total = data.total;
                    var per_page = data.perPage;
                    var current_page = data.current_page;
                    var html_page = '';
                    if (total > per_page) {
                        var page = total/per_page + 1;
                        html_page += '<td colspan="7" class="text-center">';
                        html_page += '<ul class="pagination" id="pagination_personal_order">';
                        if (current_page != 1) {
                            html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                        } else {
                            html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                        }
                        for (var i = 1; i < page; i++) {
                            var active = '';
                            if (i == current_page) {
                                active = 'active';
                            }
                            if (active == 'active') {
                              html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            } else {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }

                        }
                        if (current_page != page.toPrecision(1)) {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                        } else {
                            html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                        }
                        html_page += '</ul>';
                        html_page += '</td>';
                    }
                    $('tfoot').html(html_page);
                } else {
                   // console.log('da den 2');
                    var html = '<td colspan="9" class="text-center">Không có đơn hàng của khách hàng này trong dữ liệu!</td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                }
            },
            error: function (e) {
               console.log(e);
               var html = '<td colspan="9" class="text-center">Truy vấn đơn hàng lỗi!</td>';
               $('tbody').html(html);
           }
        });
    });

    // ajax phân trang vps
    $(document).on('click', '#pagination_enterprice_order a', function(event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        $.ajax({
            url: '/admin/orders/list_order_of_enterprise',
            data: {page: page},
            dataType: 'json',
            beforeSend: function(){
                var html = '<td class="text-center"  colspan="9"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                   // console.log(data);
                if (data.data.length > 0) {
                    var html = '';
                    $.each(data.data, function(index , invoice) {
                        html += '<tr>';
                        if (invoice.deeta != 0) {
                          html += '<td><a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="detail_invoice" data-id="'+ invoice.id_invoice +'">'+ invoice.id +'</a></td>';
                        } else {
                          html += '<td>'+ invoice.id +'</td>';
                        }
                        html += '<td>'+ invoice.date_create +'</td>';
                        html += '<td><a href="/admin/users/detail/'+ invoice.user_id +'">'+ invoice.username +'</a></td>';
                        html += '<td>'+ invoice.total +'</td>';
                        html += '<td>' + invoice.text_type_order + '</td>';
                        html += '<td>';
                        if (invoice.status == 'Finish') {
                          html += '<span class="text-success">Đã thanh toán</span>';
                        }
                        else if (invoice.status == 'Pending') {
                          html += '<span class="text-danger">Chưa thanh toán</span>';
                        }
                        else if (invoice.status == 'Active') {
                          html += '<span class="text-info">Đang chờ tạo</span>';
                        }
                        else {
                          html += '<span class="text-default">Đã hủy</span>';
                        }
                        html += '</td>';
                        html += '<td class="button-action">';
                        if (invoice.deeta != 0) {
                          html += '<a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="btn btn-warning text-white detail_invoice" data-id="'+ invoice.id_invoice +'"><i class="fas fa-edit"></i></a>';
                        }
                        html += '<button class="btn btn-danger btn-delete-invoices" data-id="' + invoice.id + '"><i class="fas fa-trash-alt"></i></button>';
                        html += '</td>';
                        html += '</tr>';
                    })
                    $('tbody').html(html);
                    $('tfoot').html('');
                    // phân trang cho vps
                    var total = data.total;
                    var per_page = data.perPage;
                    var current_page = data.current_page;
                    var html_page = '';
                    if (total > per_page) {
                        var page = total/per_page + 1;
                        html_page += '<td colspan="7" class="text-center">';
                        html_page += '<ul class="pagination" id="pagination_enterprice_order">';
                        if (current_page != 1) {
                            html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                        } else {
                            html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                        }
                        for (var i = 1; i < page; i++) {
                            var active = '';
                            if (i == current_page) {
                                active = 'active';
                            }
                            if (active == 'active') {
                              html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            } else {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }

                        }
                        if (current_page != page.toPrecision(1)) {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                        } else {
                            html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                        }
                        html_page += '</ul>';
                        html_page += '</td>';
                    }
                    $('tfoot').html(html_page);
                } else {
                   // console.log('da den 2');
                    var html = '<td colspan="9" class="text-center">Không có đơn hàng của khách hàng này trong dữ liệu!</td>';
                    $('tbody').html(html);
                    $('tfoot').html('');
                }
            },
            error: function (e) {
               console.log(e);
               var html = '<td colspan="9" class="text-center">Truy vấn đơn hàng lỗi!</td>';
               $('tbody').html(html);
           }
        });
    });

    $('#type_enterprise').on('click', function () {
        $.ajax({
           url: '/admin/orders/list_order_of_enterprise',
           dataType: 'json',
           beforeSend: function(){
               var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
               $('tbody').html(html);
           },
           success: function (data) {
                // console.log(data);
               if (data.data.length > 0) {
                   var html = '';
                   $.each(data.data, function(index , invoice) {
                       html += '<tr>';
                       if (invoice.deeta != 0) {
                         html += '<td><a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="detail_invoice" data-id="'+ invoice.id_invoice +'">'+ invoice.id +'</a></td>';
                       } else {
                         html += '<td>'+ invoice.id +'</td>';
                       }
                       html += '<td>'+ invoice.date_create +'</td>';
                       html += '<td><a href="/admin/users/detail/'+ invoice.user_id +'">'+ invoice.username +'</a></td>';
                       html += '<td>'+ invoice.total +'</td>';
                       html += '<td>' + invoice.text_type_order + '</td>';
                       html += '<td>';
                       if (invoice.status == 'Finish') {
                         html += '<span class="text-success">Đã thanh toán</span>';
                       }
                       else if (invoice.status == 'Pending') {
                         html += '<span class="text-danger">Chưa thanh toán</span>';
                       }
                       else if (invoice.status == 'Active') {
                         html += '<span class="text-info">Đang chờ tạo</span>';
                       }
                       else {
                         html += '<span class="text-default">Đã hủy</span>';
                       }
                       html += '</td>';
                       html += '<td class="button-action">';
                       if (invoice.deeta != 0) {
                         html += '<a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="btn btn-warning text-white detail_invoice" data-id="'+ invoice.id_invoice +'"><i class="fas fa-edit"></i></a>';
                       }
                       html += '<button class="btn btn-danger btn-delete-invoices" data-id="' + invoice.id + '"><i class="fas fa-trash-alt"></i></button>';
                       html += '</td>';
                       html += '</tr>';
                   })
                   $('tbody').html(html);
                   $('tfoot').html('');
                   // phân trang cho vps
                   var total = data.total;
                   var per_page = data.perPage;
                   var current_page = data.current_page;
                   var html_page = '';
                   if (total > per_page) {
                       var page = total/per_page + 1;
                       html_page += '<td colspan="7" class="text-center">';
                       html_page += '<ul class="pagination" id="pagination_enterprice_order">';
                       if (current_page != 1) {
                           html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                       } else {
                           html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                       }
                       for (var i = 1; i < page; i++) {
                           var active = '';
                           if (i == current_page) {
                               active = 'active';
                           }
                           if (active == 'active') {
                             html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                           } else {
                             html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                           }

                       }
                       if (current_page != page.toPrecision(1)) {
                           html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                       } else {
                           html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                       }
                       html_page += '</ul>';
                       html_page += '</td>';
                   }
                   $('tfoot').html(html_page);
               } else {
                   var html = '<td colspan="9" class="text-center">Không có đơn hàng của khách hàng này trong dữ liệu!</td>';
                   $('tbody').html(html);
                   $('tfoot').html('');
               }
           },
           error: function (e) {
               var html = '<td colspan="9" class="text-center">Truy vấn đơn hàng lỗi!</td>';
               $('tbody').html(html);
           }
        })
    })

});
