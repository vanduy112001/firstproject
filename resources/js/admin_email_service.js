$(document).ready(function () {

	listEmail();

	function listEmail() {
		$.ajax({
          	type: "get",
          	url: "/admin/email-service/list-email-service",
          	dataType: "json",
          	beforeSend: function(){
              	var html = '<td class="text-center" colspan="6"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              	$('tbody').html(html);
          	},
          	success: function (data) {
              	// 1 form-group
              	if (data != '') {
                  	screent_email(data);
                  	$('[data-toggle="tooltip"]').tooltip();
              	} else {
                  	var html = '<td class="text-center text-danger" colspan="6">Không có email trong dữ liệu!</td>';
                  	$('tbody').html(html);
              	}
          	},
          	error: function(e) {
              	console.log(e);
              	var html = '<td class="text-center text-danger" colspan="6">Truy vấn email lỗi!</td>';
              	$('tbody').html(html);
          	}
    })
	}

	function screent_email(data) {
		var html = '';
		$.each(data.data, function (index, value) {
			html += '<tr>';
			html += '<td><a href="/admin/email-service/edit-email/'+ value.id +'">' + value.name  + '</a></td>';
			html += '<td>' + value.meta_email.subject + '</td>';
      if ( value.type_service == 'request_payment' ) {
        html += '<td>Yêu cầu nạp tiền</td>';
      }
      else if ( value.type_service == 'finish_payment' ) {
        html += '<td>Hoàn thành nạp tiền</td>';
      }
      else if ( value.type_service == 'finish_change_ip' ) {
        html += '<td>Hoàn thành đổi IP US</td>';
      }
      else if ( value.type_service == 'finish_change_ip_vn' ) {
        html += '<td>Hoàn thành đổi IP VN</td>';
      }
      else if ( value.type_service == 'finish_addon_vps' ) {
        html += '<td>Hoàn thành thanh toán addon VPS</td>';
      }
      else if ( value.type_service == 'error_auto_refurn' ) {
        html += '<td>Lỗi tự động gia hạn VPS (Số dư tài khoản)</td>';
      }
      else if ( value.type_service == 'create_user' ) {
        html += '<td>Đăng ký tài khoản</td>';
      }
      else if ( value.type_service == 'verify_user' ) {
        html += '<td>Kích hoạt tài khoản</td>';
      }
      else if ( value.type_service == 'forgot_password' ) {
        html += '<td>Yêu cầu đổi mật khẩu tài khoản</td>';
      }
      else {
        html += '<td></td>';
      }
			html += '<td>';
      html += '<a class="btn btn-warning btn-sm text-light mr-1" data-toggle="tooltip" href="/admin/email-service/edit-email/'+ value.id +'" data-placement="top" title="Chỉnh sửa"><i class="far fa-edit"></i></a>';
      html += '<button type="button" class="btn btn-danger delete_event" data-name="'+ value.name +'" data-id="'+ value.id +'" data-toggle="tooltip" data-placement="top" title="Xóa"><i class="far fa-trash-alt"></i></button>';
      html += '</td>';
			html += '<tr>';
    })
    $('tbody').html(html);
    // phân trang cho vps
    var total = data.total;
    var per_page = data.perPage;
    var current_page = data.current_page;
    var html_page = '';
    if (total > per_page) {
          if ( total / per_page > 11 ) {
            var page = parseInt(total/per_page + 1);
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_list_vps>';
            if (current_page != 1) {
              html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
              html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            if (current_page < 7) {
              for (var i = 1; i < 9; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 1; i <= page; i++) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
              }
            }
            else if (current_page >= 7 && current_page <= page - 7) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = current_page - 3; i <= current_page +3; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  if (active == 'active') {
                    html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  } else {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                  }
                }
                html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                for (var i = page - 1; i <= page; i++) {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
            }
            else if (current_page >= page - 6) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
              html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
              for (var i = page - 6; i < page; i++) {
                var active = '';
                if (i == current_page) {
                  active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }
              }
            }

            if (current_page != page.toPrecision(1)) {
              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
              html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          } else {
            var page = total/per_page + 1;
            html_page += '<td colspan="10" class="text-center link-right">';
            html_page += '<nav>';
            html_page += '<ul class="pagination pagination_list_vps>';
            if (current_page != 1) {
                html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
            } else {
                html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
            }
            for (var i = 1; i < page; i++) {
                var active = '';
                if (i == current_page) {
                    active = 'active';
                }
                if (active == 'active') {
                  html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                } else {
                  html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                }

            }
            if (current_page != page.toPrecision(1)) {
                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
            } else {
                html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
            }
            html_page += '</ul>';
            html_page += '<nav>';
            html_page += '</td>';
          }
    }
    $('tfoot').html(html_page);
	}

	$(document).on('click', '.delete_event', function(event) {
		event.preventDefault();
		/* Act on the event */
		$('#delete-product').modal('show');
		$('#button-product').fadeIn();
		$('.modal-title').text('Xóa email');
		var html = '<span>Bạn có muốn xóa email <b class="text-danger">'+ $(this).attr('data-name') +'</b> này không?</span>';
		$('#notication-product').html(html);
		$('#button-product').attr('data-id', $(this).attr('data-id'));
		$('#button-product').attr('data-action', 'delete');
	});

	$('#button-product').on('click', function(event) {
		var id = $(this).attr('data-id');
		var action = $(this).attr('data-action');
		var token = $('meta[name="csrf-token"]').attr('content');
		$.ajax({
          	type: "post",
          	url: "/admin/email-service/action-email-service",
          	data: { _token: token, id: id, action: action },
          	dataType: "json",
          	beforeSend: function(){
              	var html = '<div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>';
              	$('#notication-product').html(html);
              	$('#button-product').attr("disabled", true);
          	},
          	success: function (data) {
          		// console.log(data);
              	// 1 form-group
              	if ( action == 'delete' ) {
              		if (data.error == 0) {
              			$('#notication-product').html('<span class="text-success">Xóa email thành công</span>');
              			$('#button-product').fadeOut();
              			listEmail();
              		} else {
              			$('#notication-product').html('<span class="text-danger">Xóa email thất bại</span>');
              		}
              	}
              	$('#button-product').attr("disabled", false);
          	},
          	error: function(e) {
              	console.log(e);
              	var html = '<span class="text-center text-danger" colspan="6">Truy vấn email marketing lỗi!</span>';
              	$('#notication-product').html(html);
              	$('#button-product').attr("disabled", false);
          	}
       	})
	});

});