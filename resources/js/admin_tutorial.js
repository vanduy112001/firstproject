$(document).ready(function () {
  $('.select2').select2();
  var select_product = $('#select_product').val();
  // list_event(select_product);

   $('#create_tutorial').on('click', function () {
      $('#delete_vps').modal('show');
      $('.modal-title').text('Tạo hướng dẫn');
      $('#button-submit').fadeIn();
      $('#notication_invoice').html("");
      $('#form_group_tutorial')[0].reset();
      $('#create').val('create');
   });

   $('#button-submit').on('click', function () {
      var form = $('#form_group_tutorial').serialize();
      var link = $('#link').val();
      var title = $('#title').val();
      var validate = group_product_validate(link, title);

      if (validate == false) {
          $.ajax({
             url: '/admin/huong-dan/hanh-dong',
             type: 'post',
             data: form,
             dataType: 'json',
             beforeSend: function(){
               var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
               $('#notication_invoice').html(html);
               $('#form_group_tutorial').css('opacity', 0.5);
               $('#button-submit').attr('disabled', true);
               $('#form_group_tutorial').attr('disabled', true);
             },
             success: function (data) {
               // console.log(data);
               if (data) {
                 var html = '<p class="text-success">'+data+'</p>';
                 $('#notication_invoice').html(html);
                 $('#form_group_tutorial')[0].reset();
                 $('.modal-title').text('Tạo hướng dẫn');
                 $('#form_group_tutorial').css('opacity', 1);
                 $('#button-submit').attr('disabled', false);
                 $('#form_group_tutorial').attr('disabled', false);
               } else {
                 var html = '<p class="text-danger">Tạo hướng dẫn thất bại</p>';
                 $('#notication_invoice').html(html);
                 $('#form_group_tutorial').css('opacity', 1);
                 $('#button-submit').attr('disabled', false);
                 $('#form_group_tutorial').attr('disabled', false)
               }
               list_tutorial('');
             },
             error: function (e) {
                console.log(e);
                $('#notication_invoice').html('<span class="text-danger">Tạo hướng dẫn lỗi.</span>');
             }
          })
      }

   })

   function list_tutorial() {
       $.ajax({
          url: '/admin/huong-dan/danh-sach-huong-dan',
          dataType: 'json',
          beforeSend: function(){
            var html = '<td  colspan="2" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
            $('tbody').html(html);
          },
          success: function (data) {
            // console.log(data);
            if (data.data != '') {
              var html = '';
              $.each(data.data, function (index, event) {
                  // console.log(event);
                  html += '<tr>';
                  html += '<td>' + event.title + '</td>';
                  html += '<td>';
                  html += '<button type="button" class="btn btn-warning text-light edit_event mr-2" data-toggle="tooltip" data-id="'+ event.id +'" data-placement="top" title="Chỉnh sửa hướng dẫn"><i class="far fa-edit"></i></button>';
                  html += '<button type="button" class="btn btn-danger delete_tutorial"><i class="far fa-trash-alt" data-id="'+ event.id +'" data-toggle="tooltip" data-placement="top" title="Xóa hướng dẫn"></i></button>';
                  html += '</td>';
                  html += '</tr>';
              })
              var total = data.total;
              var per_page = data.per_page;
              var current_page = data.current_page;
              var html_page = '';
              if (total > per_page) {
                var page = total/per_page + 1;
                html_page += '<td colspan="4" class="text-center">';
                html_page += '<ul class="pagination">';
                if (current_page != 1) {
                  html_page += '<li><a href="#" data-page="'+(current_page-1)+'" class="prev">&laquo</a></li>';
                } else {
                  html_page += '<li><a href="#" class="prev disabled">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                  var active = '';
                  if (i == current_page) {
                    active = 'active';
                  }
                  html_page += '<li><a href="#" data-page="'+i+'" class="'+active+'">'+i+'</a></li>';
                }
                if (current_page != page.toPrecision(1)) {
                  html_page += '<li><a href="#" data-page="'+current_page+'" class="prev">&raquo;</a></li>';
                } else {
                  html_page += '<li><a href="#" class="prev disabled">&raquo;</a></li>';
                }
              }
              html_page += '</ul>';
              html_page += '</td>';
              $('tfoot').html(html_page);
              $('tbody').html(html);
            } else {
               $('tbody').html('<td colspan="4" class="text-center text-danger">Không có khuyến mãi nào trong cơ sở dữ liệu.</td>');
            }
          },
          error: function (e) {
             console.log(e);
             $('tbody').html('<td colspan="4" class="text-center text-danger">Truy vấn khuyến mãi thất bại</td>');
          }
       })
   }

   //ajax phân trang
   $(document).on('click', '.pagination a', function(event) {
       event.preventDefault();
       var page = $(this).attr('data-page');
       $.ajax({
           url: '/admin/huong-dan/danh-sach-huong-dan',
           type: 'get',
           dataType: 'json',
           data: {page: page},
           beforeSend: function(){
             var html = '<td  colspan="2" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
             $('tbody').html(html);
           },
           success: function (data) {
             // console.log(data);
             if (data.data != '') {
               var html = '';
               $.each(data.data, function (index, event) {
                   // console.log(event);
                   html += '<tr>';
                   html += '<td>' + event.title + '</td>';
                   html += '<td>';
                   html += '<button type="button" class="btn btn-warning text-light edit_tutorial mr-2" data-toggle="tooltip" data-id="'+ event.id +'" data-placement="top" title="Chỉnh sửa hướng dẫn"><i class="far fa-edit"></i></button>';
                   html += '<button type="button" class="btn btn-danger delete_tutorial"><i class="far fa-trash-alt" data-id="'+ event.id +'" data-toggle="tooltip" data-placement="top" title="Xóa hướng dẫn"></i></button>';
                   html += '</td>';
                   html += '</tr>';
               })
               var total = data.total;
               var per_page = data.per_page;
               var current_page = data.current_page;
               var html_page = '';
               if (total > per_page) {
                 var page = total/per_page + 1;
                 html_page += '<td colspan="4" class="text-center">';
                 html_page += '<ul class="pagination">';
                 if (current_page != 1) {
                   html_page += '<li><a href="#" data-page="'+(current_page-1)+'" class="prev">&laquo</a></li>';
                 } else {
                   html_page += '<li><a href="#" class="prev disabled">&laquo</a></li>';
                 }
                 for (var i = 1; i < page; i++) {
                   var active = '';
                   if (i == current_page) {
                     active = 'active';
                   }
                   html_page += '<li><a href="#" data-page="'+i+'" class="'+active+'">'+i+'</a></li>';
                 }
                 if (current_page != page.toPrecision(1)) {
                   html_page += '<li><a href="#" data-page="'+current_page+'" class="prev">&raquo;</a></li>';
                 } else {
                   html_page += '<li><a href="#" class="prev disabled">&raquo;</a></li>';
                 }
               }
               html_page += '</ul>';
               html_page += '</td>';
               $('tfoot').html(html_page);
               $('tbody').html(html);
             } else {
                $('tbody').html('<td colspan="4" class="text-center text-danger">Không có khuyến mãi nào trong cơ sở dữ liệu.</td>');
             }
           },
           error: function (e) {
              console.log(e);
              $('tbody').html('<td colspan="4" class="text-center text-danger">Truy vấn khuyến mãi thất bại</td>');
           }
       });
   });

   $(document).on('click', '.edit_tutorial', function () {
      var id = $(this).attr('data-id');
      $('#delete_vps').modal('show');
      $('.modal-title').text('Sửa hướng dẫn');
      $('#notication_invoice').html("");
      $('#form_group_tutorial')[0].reset();
      $('#create').val('update');
      $('#tutorial_id').val(id);
      $('#button-submit').fadeIn();

      $.ajax({
         url: '/admin/huong-dan/chi-tiet-huong-dan',
         data: {id:id},
         dataType: 'json',
         beforeSend: function(){
           var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
           $('#notication_invoice').html(html);
           $('#form_group_tutorial').css('opacity', 0.5);
           $('#button-submit').attr('disabled', true);
           $('#form_group_tutorial').attr('disabled', true);
         },
         success: function (data) {
           // console.log(data);
           if (data) {
              var html = '';
              $('#title').val(data.title);
              $('#link').val(data.link);
              $('#description').val(data.description);
           }
           $('#notication_invoice').html('');
           $('#form_group_tutorial').css('opacity', 1);
           $('#button-submit').attr('disabled', false);
           $('#form_group_tutorial').attr('disabled', false);
         },
         error: function (e) {
            console.log(e);
            $('#notication_invoice').html('<span class="text-danger">Truy vấn khuyến mãi thất bại.</span>');
         }
      })
   })

   $(document).on('click', '.delete_tutorial', function () {
      var id = $(this).attr('data-id');
      $('#button-delete').fadeIn();
      $('#button-finish').fadeOut();
      $('#delete_tutorial').modal('show');
      $('#notication_tutorial').html('<span class="text-danger">Bạn có muốn xóa hướng dẫn này không?</span>');
      $('#button-delete').attr('data-id', id);
   })

   $(document).on('click', '#button-delete', function () {
      var id = $(this).attr('data-id');
      var token = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
         url: '/admin/huong-dan/xoa-huong-dan',
         type: 'post',
         data: {_token:token , id:id},
         dataType: 'json',
         beforeSend: function(){
           var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
           $('#notication_tutorial').html(html);
           $('#button-delete').attr('disabled', true);
         },
         success: function (data) {
           // console.log(data);
           if (data) {
             var html = '<p class="text-success">Xóa hướng dẫn thành công.</p>';
             $('#notication_tutorial').html(html);
             $('#button-delete').fadeOut();
             $('#button-finish').fadeIn();
           } else {
             var html = '<p class="text-danger">Xóa hướng dẫn thất bại.</p>';
             $('#notication_tutorial').html(html);
             $('#button-delete').fadeOut();
             $('#button-finish').fadeIn();
           }
           list_tutorial('');
         },
         error: function (e) {
            console.log(e);
            $('#notication_tutorial').html('<span class="text-danger">Truy vấn hướng dẫn lỗi.</span>');
         }
      })
   })

   // validate group product
   function group_product_validate(link, title) {

     if (link == null || link == '') {
       $('#notication_invoice').html('<span class="text-danger">Tiêu đề không được để trống.</span>');
       return true;
     } else if(title == null || title == '') {
       $('#notication_invoice').html('<span class="text-danger">Liên kết hướng dẫn không được để trống.</span>');
       return true;
     }
     else {
       return false;
     }
   }

})
