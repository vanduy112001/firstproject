$(document).ready(function () {
    
    loadProxy();

    $('#qtt').on('change', function () {
        loadProxy();
    })

    $('#status').on('change', function () {
        loadProxy();
    })

    $('#q').on('keyup', function () {
        loadProxy();
    })

    function loadProxy() {
        var qtt = $('#qtt').val();
        var status = $('#status').val();
        var q = $('#q').val();
        $.ajax({
            url: '/admin/proxy/getProxy',
            data: { qtt: qtt, status: status, q: q },
            dataType: 'json',
            beforeSend: function() {
                var html = '<td colspan="15" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function(data) {
                // console.log(data);
                if (data.data != '') {
                    screent_proxy(data);
                } else {
                    $('tbody').html('<td colspan="15" class="text-center">Không có dịch vụ Proxy trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('tbody').html('<td colspan="15" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    }

    function screent_proxy(data) {
        var html = '';
        $.each(data.data, function(index, proxy) {
            if ( proxy ) {
                html += '<tr>';
                // checkbox
                html += '<td><input type="checkbox" value="' + proxy.id + '" data-ip="' + proxy.ip + '" class="checkbox"></td>';
                // khách hàng
                html += '<td><a href="/admin/users/detail/'+ proxy.user_id +'">'+ proxy.text_user_name +'</a></td>';
                // sản phẩm
                html += '<td>' + proxy.text_product + '</td>';
                // ip
                html += '<td><a href="/admin/proxy/detail/'+ proxy.id +'">'+ proxy.ip +'</a>';
                // port
                html += '<br>' + proxy.port + '</td>';
                // user/password
                html += '<td>';
                html += proxy.username + '<br>';
                html += proxy.password;
                html += '</td>';
                // bang
                html += '<td>' + proxy.state + '</td>';
                // ngay tao
                html += '<td><span>' + proxy.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                html += '<span>' + proxy.text_next_due_date + '<span>';
                html += '</td>';
                // tổng thoi gian thue
                html += '<td>' + proxy.total_time + '</td>';
                // chu kỳ thanh toán
                html += '<td>' + proxy.text_billing_cycle + '</td>';
                // giá
                html += '<td><b>' + proxy.amount + '</b></td>';
                // trang thai
                html += '<td class="server-status">' + proxy.text_status + '</td>';
                // hành động
                html += '<td class="page-service-action">';
                html += '<a href="/admin/proxy/detail/'+ proxy.id +'" data-toggle="tooltip" class="btn btn-sm btn-warning text-light mr-1" data-placement="top" title="Chỉnh sửa" style="font-size: 10px;"><i class="fas fa-edit"></i></a>';
                html += '<button class="btn btn-sm mr-1 btn-outline-danger button-action terminated" data-toggle="tooltip" data-placement="top" title="Xóa dịch vụ" data-action="terminated" data-id="' + proxy.id + '" data-ip="' + proxy.ip + '"><i class="fa fa-ban" aria-hidden="true"></i></button>';
                html += '</td>';
                html += '</tr>';
            } else {
                html += '<tr>';
                html += '<td colspan="15" class="text-center text-danger">Proxy này không tồn tại.</td>';
                html += '</tr>';
            }
        })
        $('tbody').html(html);
        $('.total-item').text(data.data.length)
        $('[data-toggle="tooltip"]').tooltip();

        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_server_use">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_server_use">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '<nav>';
                html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
        $vpsCheckbox = $('.checkbox');
    }

    $(document).on('click', '.pagination a', function(event) {
        event.preventDefault();
        var page = $(this).attr('data-page');
        var qtt = $('#qtt').val();
        var status = $('#status').val();
        var q = $('#q').val();
        $.ajax({
            url: '/admin/proxy/getProxy',
            data: { qtt: qtt, status: status, q: q, page: page },
            dataType: 'json',
            beforeSend: function() {
                var html = '<td colspan="15" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function(data) {
                // console.log(data);
                if (data.data != '') {
                    screent_proxy(data);
                } else {
                    $('tbody').html('<td colspan="15" class="text-center">Không có dịch vụ Proxy trong dữ liệu.</td>');
                }
            },
            error: function(e) {
                console.log(e);
                $('tbody').html('<td colspan="15" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    })

    $(document).on('click', '.button-action', function () {
        var action = $(this).attr('data-action');
        $('tr').removeClass('remove-vps');
        var id = $(this).attr('data-id');
        var ip = $(this).attr('data-ip');
        $(this).closest('tr').addClass('remove-vps');
        switch (action) {
            case 'terminated':
                $('#delete-order').modal('show');
                $('.modal-title').text('Xóa Proxy');
                $('#notication-order').html('Bạn có muốn xóa Proxy <b class="text-danger"> IP:' + ip + '</b> này không?');
                $('#button-multi-vps').attr('disabled', false);
                $('#button-order').val('Xác nhận');
                $('#button-order').attr('data-id', id);
                $('#button-order').attr('data-action', "delete");
                $('#button-order').fadeIn();
                $('#button-finish').fadeOut();
                break;
            default:
                alert('Chọn hành động thất bại');
                break;
        }
    });

    $('#button-order').on('click', function () {
        id = $(this).attr('data-id');
        action = $(this).attr('data-action');
        $.ajax({
            type: "get",
            url: "/admin/proxy/action_proxy",
            data: {id:  id, action: action},
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-order').attr('disabled', true);
                $('#notication-order').html(html);
            },
            success: function (data) {
                // console.log(data);
                if ( action == 'delete' ) {
                    if (data) {
                        var html = '<p class="text-danger">Xóa Proxy thành công</p>';
                        $('#notication-order').html(html);
                        $('#button-order').attr('disabled', false);
                        $('.remove-vps').fadeOut(1500);
                        $('#button-order').fadeOut();
                        $('#button-finish').fadeIn();
                    } else {
                        var html = '<p class="text-danger">Xóa Proxy thất bại</p>';
                        $('#notication-order').html(html);
                        $('#button-order').attr('disabled', false);
                    }
                }
                else if ( action == 'off' ) {
                    if (data) {
                        $('.remove-vps .off').attr('disabled', false);
                        $('.remove-vps .on').attr('disabled', true);
                        $('.remove-vps .vps-status').html('<span class="text-danger">Đã tắt</span>');
                        $('#notication-order').html('<span class="text-success">Tắt VPS thành công</span>');
                        $('#button-order').fadeOut();
                        $('#button-finish').fadeIn();
                        $('tr').removeClass('remove-vps');
                    } else {
                        $('#notication-order').html('<span class="text-danger">Tắt VPS thất bại</span>');
                    }
                    $('#button-order').attr('disabled', false);
                }
                else if ( action == 'on' ) {
                    if (data) {
                        $('.remove-vps .off').attr('disabled', true);
                        $('.remove-vps .on').attr('disabled', false);
                        $('.remove-vps .vps-status').html('<span class="text-success">Đang bật</span>');
                        $('#notication-order').html('<span class="text-success">Bật VPS thành công</span>');
                        $('#button-order').fadeOut();
                        $('#button-finish').fadeIn();
                        $('tr').removeClass('remove-vps');
                    } else {
                        $('#notication-order').html('<span class="text-danger">Bật VPS thất bại</span>');
                    }
                    $('#button-order').attr('disabled', false);
                }
                else if ( action == 'restart' ) {
                    if (data) {
                        $('.remove-vps .off').attr('disabled', false);
                        $('.remove-vps .on').attr('disabled', true);
                        $('.remove-vps .vps-status').html('<span class="text-success">Đang bật</span>');
                        $('#notication-order').html('<span class="text-success">Khởi động lại VPS thành công</span>');
                        $('#button-order').fadeOut();
                        $('#button-finish').fadeIn();
                        $('tr').removeClass('remove-vps');
                    } else {
                        $('#notication-order').html('<span class="text-danger">Khởi động lại VPS thất bại</span>');
                    }
                    $('#button-order').attr('disabled', false);
                }
                
            },
            error: function (e) {
                console.log(e);
                var html = '<p class="text-danger">Truy vấn Proxy bị lỗi!</p>';
                $('#nnotication-order').html(html);
                $('#button-order').attr('disabled', false);
                $('.remove-vps').removeClass('remove-vps');
            }
        });
    });

    var $vpsCheckbox = $('.checkbox');
    var lastChecked = null;

    $(document).on('click','.checkbox', function () {
        var checked = $('.checkbox:checked');
        $('.last-item').text( checked.length );
        if ($(this).is(':checked')) {
            $(this).closest('tr').addClass('action');
        } else {
            $(this).closest('tr').removeClass('action');
        }
        if (!lastChecked) {
            lastChecked = this;
            return;
        }
        $vpsCheckbox.click(function (e) {
            if (!lastChecked) {
                lastChecked = this;
                return;
            }
            if ( e.shiftKey ) {
                var start = $vpsCheckbox.index(this);
                var end = $vpsCheckbox.index(lastChecked);
                $vpsCheckbox.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
                $.each( $vpsCheckbox , function (index , value) {
                    if( $(this).is(':checked') ) {
                        $(this).closest('tr').addClass('action');
                    } else {
                        $(this).closest('tr').removeClass('action');
                    }
                })
            }
            lastChecked = this;
        })

        var checked = $('.checkbox:checked');
        $('.last-item').text( checked.length );
    });

    $(document).on('click', '.checkbox_all', function() {
        if ($(this).is(':checked')) {
            $('.checkbox').prop('checked', this.checked);
            $('tbody tr').addClass('action');
        } else {
            $('.checkbox').prop('checked', this.checked);
            $('tbody tr').removeClass('action');
        }
        var checked = $('.checkbox:checked');
        $('.last-item').text( checked.length );
    })

});