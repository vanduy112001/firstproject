$(document).ready(function() {
    loadCustomer();

    function loadCustomer() {
        $.ajax({
            type: 'get',
            url: '/users/listCustomer',
            dataType: 'json',
            beforeSend: function() {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.home-service-customer').html(html);
            },
            success: function(data) {
                // console.log(data);
                var html = '';
                if (data != '') {
                    $.each(data.data, function(index, customer) {
                        html += '<div class="row service_info">';
                        html += '<div class="col-sm-3 home-mobile-repon-3">';
                        html += '<a href="/khach-hang/thong-tin-khach-hang/' + customer.id + '">' + customer.ma_customer + '</a>';
                        html += '</div>';
                        html += '<div class="col-sm-6 home-customer-name home-mobile-repon-6">';
                        if (customer.type_customer == 0) {
                            html += customer.customer_name;
                        } else {
                            html += customer.customer_tc_name;
                        }
                        html += '</div>';
                        html += '<div class="col-sm-3 text-right home-mobile-repon-3">';
                        html += '<a href="/khach-hang/thong-tin-khach-hang/' + customer.id + '" class="btn btn-secondary btn-sm">Chi tiết</a>';
                        html += '</div>';
                        html += '</div>';
                        // console.log(html);
                    });
                    var current_page = data.current_page;
                    var page = data.total / data.per_page + 1;
                    if (page > 2) {
                        html += '<div class="customer-pagi text-center">';
                        html += '<ul class="pagination">';
                        if (current_page != 1) {
                            html += '<li><a href="#" data-page="' + (current_page - 1) + '" class="prev">&laquo</a></li>';
                        } else {
                            html += '<li><a href="#" class="prev disabled">&laquo</a></li>';
                        }
                        for (var i = 1; i < page; i++) {
                            var active = '';
                            if (i == current_page) {
                                active = 'active';
                            }
                            html += '<li><a href="#" data-page="' + i + '" class="' + active + '">' + i + '</a></li>';
                        }
                        if (current_page != page.toPrecision(1)) {
                            html += '<li><a href="#" data-page="' + current_page + '" class="prev">&raquo;</a></li>';
                        } else {
                            html += '<li><a href="#" class="prev disabled">&raquo;</a></li>';
                        }
                        html += '</ul>';
                        html += '</div>';
                    }
                } else {
                    html += '<div class="text-center text-danger">Quý khách chưa có thông tin quản lý khách hàng!</div>';
                }
                $('.home-service-customer').html(html);
            },
            error: function(e) {
                console.log(e);
                var html = '';
                html += '<div class="text-center text-danger">Truy vấn quản lý khách hàng không thành công</div>';
                $('.home-service-customer').html(html);
            },
        });
    }

    loadTotalSerivces();

    function loadTotalSerivces() {
        $.ajax({
            type: 'get',
            url: '/users/load_siderbar_top',
            dataType: 'json',
            beforeSend: function() {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.text_siderbar_top').html(html);
            },
            success: function(data) {
                // console.log(data);
                $('.total_services .text_siderbar_top').html(data.total_service);
                $('.total_notifica .text_siderbar_top').html(data.detail_order_progressing);
                $('.total_ticket .text_siderbar_top').html(data.total_ticket);
            },
            error: function(e) {
                console.log(e);
            },
        });
    }

    loadContentCenter();

    function loadContentCenter() {
        $.ajax({
            type: 'get',
            url: '/users/load_content_center',
            dataType: 'json',
            beforeSend: function() {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.content_center .card-body').html(html);
            },
            success: function(data) {
                // console.log( data);
                // tổng số các dịch vụ vps đã hết hạn
                var html_total_vps_nearly = '';
                if (data.total_vps_nearly.qtt > 0) {
                    html_total_vps_nearly += '<div class="" style="margin: 10px 10px;text-align: justify;font-size:14px">';
                    html_total_vps_nearly += 'Quý khách có ' + data.total_vps_nearly.qtt + ' dịch vụ VPS gần hết hạn hoặc đã quá hạn. Dịch vụ sẽ bị xóa trong vòng 2-4 ngày tính từ ngày hết hạn, Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ!';
                    html_total_vps_nearly += '<br>Danh sách dịch vụ gần hết hạn và hết hạn';
                    html_total_vps_nearly += '<div class="info-expire">';
                    html_total_vps_nearly += data.total_vps_nearly.text;
                    html_total_vps_nearly += '</div>';
                    html_total_vps_nearly += '<a href="/dich-vu/vps/gan-het-han-va-qua-han">Xem chi tiết</a>';
                    html_total_vps_nearly += '</div>';
                    $('#total_vps_service_nearly .card-body').html(html_total_vps_nearly);
                    $('#total_vps_service_nearly').css('display', 'block');
                }
                // tổng số các dịch vụ vps us đã hết hạn
                var html_total_vps_us_nearly = '';
                if (data.total_vps_us_nearly.qtt > 0) {
                    html_total_vps_us_nearly += '<div class="" style="margin: 10px 10px;text-align: justify;font-size:14px">';
                    html_total_vps_us_nearly += 'Quý khách có ' + data.total_vps_us_nearly.qtt + ' dịch vụ VPS US gần hết hạn hoặc đã quá hạn. Dịch vụ sẽ bị xóa trong vòng 2-4 ngày tính từ ngày hết hạn, Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ! ';
                    html_total_vps_us_nearly += '<br>Danh sách dịch vụ gần hết hạn và hết hạn';
                    html_total_vps_us_nearly += '<div class="info-expire">';
                    html_total_vps_us_nearly += data.total_vps_us_nearly.text;
                    html_total_vps_us_nearly += '</div>';
                    html_total_vps_us_nearly += '<a href="/dich-vu/vps_us/gan-het-han-va-qua-han">Xem chi tiết</a>';
                    html_total_vps_us_nearly += '</div>';
                    $('#total_vps_us_service_nearly .card-body').html(html_total_vps_us_nearly);
                    $('#total_vps_us_service_nearly').css('display', 'block');
                }
                // tổng số hosting gần hết hạn mới
                var html_hosting_service_nearly = '';
                if (data.total_hosting_nearly > 0) {
                    html_hosting_service_nearly += '<div class="" style="margin: 10px 10px;text-align: justify;">';
                    html_hosting_service_nearly += 'Quý khách có ' + data.total_hosting_nearly + ' dịch vụ Hosting gần hết hạn hoặc đã quá hạn. Khi hết hạn chúng tôi sẽ tạm dừng sản phẩm/dịch vụ đó và xóa trong vòng 7-15 ngày tính từ ngày hết hạn. Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ! ';
                    html_hosting_service_nearly += ' <a href="/dich-vu/hosting/gan-het-han-va-qua-han">Xem chi tiết</a>';
                    html_hosting_service_nearly += '</div>';
                    $('#total_hosting_service_nearly .card-body').html(html_hosting_service_nearly);
                    $('#total_hosting_service_nearly').css('display', 'block');
                }
                // tổng số thông báo mới
                var html_notication = '';
                var html_header_notication = '<h4 class="info-header card-title"><i class="fas fa-newspaper"></i> Thông báo mới</h4>';
                if (data.total_notication > 0) {
                    html_header_notication = '<h4 class="info-header card-title"><i class="fas fa-newspaper"></i> Có ' + data.total_notication + ' thông báo mới  <a href="/notifications"><span class="d-inline badge badge-primary ml-2">Xem tất cả</span></a></h4>';
                    html_notication += '<div class="box-body chat" id="scroll-notication">';
                    html_notication += '<div class="row">';
                    html_notication += '<div class="col-md-12">';
                    html_notication += '<ul class="all-inverted timeline">';
                    $.each(data.notifications_reads, function(index, notication_read) {
                        // console.log(notication_read);
                        html_notication += '<a href="/notifications/detail/' + notication_read.notification.id + '">';
                        html_notication += '<li class="timeline-inverted">';
                        html_notication += '<div class="timeline-panel">';
                        html_notication += '<div class="timeline-title">';
                        html_notication += notication_read.notification.name + '<br>';
                        html_notication += '<span class="text-domain">Ngày tạo: ' + notication_read.date_create + '</span>';
                        html_notication += '</div>';
                        html_notication += '</div>';
                        html_notication += '</li>';
                        html_notication += '</a>';
                    });
                    html_notication += '</ul>';
                    html_notication += '</div>';
                    html_notication += '</div>';
                    html_notication += '</div>';
                }
                $('#home-notication .card-header').html(html_header_notication);
                $('#home-notication .card-body').html(html_notication);
                $('#scroll-notication').slimscroll({
                    // height: 'auto'
                });
                // tổng số server gần hết hạn mới
                var html_server_nearly = '';
                if (data.total_server_nearly > 0) {
                    html_server_nearly += '<div class="" style="margin: 20px 20px;text-align: justify;">';
                    html_server_nearly += 'Quý khách có ' + data.total_server_nearly + ' dịch vụ Server gần hết hạn hoặc đã quá hạn. Khi hết hạn chúng tôi sẽ tạm dừng sản phẩm/dịch vụ đó và xóa trong vòng 3-7 ngày tính từ ngày hết hạn. Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ! ';
                    html_server_nearly += '<a href="/dich-vu/server/gan-het-han-huy-hoac-qua-han">Xem chi tiết</a>';
                    html_server_nearly += '</div>';
                    $('#home-service .card-body').html(html_server_nearly);
                    $('#home-service').css('display', 'block');
                }
                // tổng số hướng dẫn
                var html_tutorial = '';
                if (data.total_tutorials > 0) {
                    html_tutorial += '<div class="box-body chat" id="scroll-tutorials">';
                    html_tutorial += '<div class="row">';
                    html_tutorial += '<div class="col-md-12">';
                    html_tutorial += '<ul class="all-inverted timeline">';
                    $.each(data.tutorials.data, function(index, tutorial) {
                        html_tutorial += '<a href="' + tutorial.link + '" target="_blank">';
                        html_tutorial += '<li class="timeline-inverted">';
                        html_tutorial += '<div class="timeline-panel">';
                        html_tutorial += '<div class="timeline-title">';
                        html_tutorial += '<span class="text-tutorials">' + tutorial.title + '</span>';
                        html_tutorial += '</div>';
                        html_tutorial += '</div>';
                        html_tutorial += '</li>';
                        html_tutorial += '</a>';
                    });
                    html_tutorial += '</ul>';
                    html_tutorial += '</div>';
                    html_tutorial += '</div>';
                    html_tutorial += '</div>';
                }
                $('#home-tutorials .card-body').html(html_tutorial);
                $('#scroll-tutorials').slimscroll({
                    // height: 'auto'
                });
                // tổng số colocation gần hết hạn mới
                var html_colocation_nearly = '';
                if (data.total_colocation_nearly > 0) {
                    html_colocation_nearly += '<div class="" style="margin: 20px 20px;text-align: justify;">';
                    html_colocation_nearly += 'Quý khách có ' + data.total_colocation_nearly + ' dịch vụ Colocation gần hết hạn hoặc đã quá hạn. Khi hết hạn chúng tôi sẽ tạm dừng sản phẩm/dịch vụ đó và xóa trong vòng 3-7 ngày tính từ ngày hết hạn. Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ! ';
                    html_colocation_nearly += '<a href="/dich-vu/colocation/gan-het-han-va-qua-han">Xem chi tiết</a>';
                    html_colocation_nearly += '</div>';
                    $('#home-colocations .card-body').html(html_colocation_nearly);
                    $('#home-colocations').css('display', 'block');
                }
                // tổng số email_hosting gần hết hạn mới
                var html_email_hosting_nearly = '';
                if (data.total_email_hosting_nearly > 0) {
                    html_email_hosting_nearly += '<div class="" style="margin: 20px 20px;text-align: justify;">';
                    html_email_hosting_nearly += 'Quý khách có ' + data.total_email_hosting_nearly + ' dịch vụ Email Hosting gần hết hạn hoặc đã quá hạn. Khi hết hạn chúng tôi sẽ tạm dừng sản phẩm/dịch vụ đó và xóa trong vòng 10-15 ngày tính từ ngày hết hạn. Quý khách vui lòng gia hạn để không bị gián đoạn dịch vụ! ';
                    html_email_hosting_nearly += '<a href="/dich-vu/email_hosting/gan-het-han-va-qua-han">Xem chi tiết</a>';
                    html_email_hosting_nearly += '</div>';
                    $('#home-email-hosting .card-body').html(html_email_hosting_nearly);
                    $('#home-email-hosting').css('display', 'block');

                }
            },
            error: function(e) {
                console.log(e);
            },
        });
    }

    loadSiderbarRight();

    function loadSiderbarRight() {
        $.ajax({
            type: 'get',
            url: '/users/load_siderbar_right',
            dataType: 'json',
            beforeSend: function() {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.home-service-chart').html(html);
            },
            success: function(data) {
                // console.log(data);
                var html_total_services = '';
                var services_right = data.services_right;
                if (services_right != '') {
                    // VPS VN
                    if (services_right.total_vps > 0) {
                        html_total_services += '<div class="row service_info mb-2">';
                        html_total_services += '<div class="col-12 home-mobile-repon mb-2"><b>VPS VN</b></div>';
                        // html_total_services += '<div class="col-sm-4 home-mobile-repon"></div>';
                        // html_total_services += '<div class="col-sm-4 text-right home-mobile-repon">';
                        // html_total_services += '</div>';

                        html_total_services += '<div class="col-sm-4 home-mobile-repon">Tổng</div>';
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">' + services_right.total_vps + '</div>';
                        html_total_services += '<div class="col-sm-4 text-right home-mobile-repon mb-1">';
                        html_total_services += '<a href="/dich-vu/vps/dang-su-dung" class="btn btn-secondary btn-sm">Chi tiết</a>';
                        html_total_services += '</div>';
                        // đang sử dụng
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">Còn hạn</div>';
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">' + services_right.total_vps_use + '</div>';
                        html_total_services += '<div class="col-sm-4 text-right home-mobile-repon mb-1">';
                        html_total_services += '<a href="/dich-vu/vps/con-han" class="btn btn-secondary btn-sm">Chi tiết</a>';
                        html_total_services += '</div>';
                        // hết hạn
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">Hết hạn</div>';
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">' + services_right.total_vps_expire + '</div>';
                        html_total_services += '<div class="col-sm-4 text-right home-mobile-repon mb-1">';
                        html_total_services += '<a href="/dich-vu/vps/da-het-han" class="btn btn-secondary btn-sm">Chi tiết</a>';
                        html_total_services += '</div>';
                        html_total_services += '</div>';
                    }
                    // VPS US
                    if (services_right.total_vps_us > 0) {
                        html_total_services += '<div class="row service_info mb-2">';
                        html_total_services += '<div class="col-12 home-mobile-repon mb-2"><b>VPS US</b></div>';
                        // html_total_services += '<div class="col-sm-4 home-mobile-repon"></div>';
                        // html_total_services += '<div class="col-sm-4 text-right home-mobile-repon">';
                        // html_total_services += '</div>';
                        // tổng cộng
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">Tổng</div>';
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">' + services_right.total_vps_us + '</div>';
                        html_total_services += '<div class="col-sm-4 text-right home-mobile-repon mb-1">';
                        html_total_services += '<a href="/dich-vu/vps_us/dang-su-dung" class="btn btn-secondary btn-sm">Chi tiết</a>';
                        html_total_services += '</div>';
                        // còn hạn
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">Còn hạn</div>';
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">' + services_right.total_vps_us_use + '</div>';
                        html_total_services += '<div class="col-sm-4 text-right home-mobile-repon mb-1">';
                        html_total_services += '<a href="/dich-vu/vps_us/con-han" class="btn btn-secondary btn-sm">Chi tiết</a>';
                        html_total_services += '</div>';
                        // hết hạn
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">Hết hạn</div>';
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">' + services_right.total_vps_us_expire + '</div>';
                        html_total_services += '<div class="col-sm-4 text-right home-mobile-repon mb-1">';
                        html_total_services += '<a href="/dich-vu/vps_us/da-het-han" class="btn btn-secondary btn-sm">Chi tiết</a>';
                        html_total_services += '</div>';
                        html_total_services += '</div>';
                    }
                    // Proxy
                    if (services_right.total_proxy > 0) {
                        html_total_services += '<div class="row service_info mb-2">';
                        html_total_services += '<div class="col-12 home-mobile-repon mb-2"><b>Proxy</b></div>';
                        // html_total_services += '<div class="col-sm-4 home-mobile-repon"></div>';
                        // html_total_services += '<div class="col-sm-4 text-right home-mobile-repon">';
                        // html_total_services += '</div>';
                        // tổng cộng
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">Tổng</div>';
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">' + services_right.total_proxy + '</div>';
                        html_total_services += '<div class="col-sm-4 text-right home-mobile-repon mb-1">';
                        html_total_services += '<a href="/dich-vu/proxy/dang-su-dung" class="btn btn-secondary btn-sm">Chi tiết</a>';
                        html_total_services += '</div>';
                        // còn hạn
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">Còn hạn</div>';
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">' + services_right.total_proxy_use + '</div>';
                        html_total_services += '<div class="col-sm-4 text-right home-mobile-repon mb-1">';
                        html_total_services += '<a href="/dich-vu/proxy/con-han" class="btn btn-secondary btn-sm">Chi tiết</a>';
                        html_total_services += '</div>';
                        // hết hạn
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">Hết hạn</div>';
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">' + services_right.total_proxy_expire + '</div>';
                        html_total_services += '<div class="col-sm-4 text-right home-mobile-repon mb-1">';
                        html_total_services += '<a href="/dich-vu/proxy/het-han" class="btn btn-secondary btn-sm">Chi tiết</a>';
                        html_total_services += '</div>';
                        html_total_services += '</div>';
                    }
                    if (services_right.total_hosting > 0) {
                        html_total_services += '<div class="row service_info">';
                        html_total_services += '<div class="col-12 home-mobile-repon mb-2"><b>Hosting</b></div>';
                        // html_total_services += '<div class="col-sm-4 home-mobile-repon"></div>';
                        // html_total_services += '<div class="col-sm-4 text-right home-mobile-repon">';
                        // html_total_services += '</div>';

                        // Tổng cộng
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">Tổng</div>';
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">' + services_right.total_hosting + '</div>';
                        html_total_services += '<div class="col-sm-4 text-right home-mobile-repon">';
                        html_total_services += '<a href="/dich-vu/hosting/dang-su-dung" class="btn btn-secondary btn-sm">Chi tiết</a>';
                        html_total_services += '</div></div>';
                    }

                    if (services_right.total_server) {
                        html_total_services += '<div class="row service_info">';
                        html_total_services += '<div class="col-12 home-mobile-repon mb-2"><b>Server</b></div>';
                        // html_total_services += '<div class="col-sm-4 home-mobile-repon"></div>';
                        // html_total_services += '<div class="col-sm-4 text-right home-mobile-repon">';
                        // html_total_services += '</div>';

                        html_total_services += '<div class="col-sm-4 home-mobile-repon">Tổng</div>';
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">' + services_right.total_server + '</div>';
                        html_total_services += '<div class="col-sm-4 text-right home-mobile-repon">';
                        html_total_services += '<a href="/dich-vu/server/dang-su-dung" class="btn btn-secondary btn-sm">Chi tiết</a>';
                        html_total_services += '</div></div>';
                    }

                    if (services_right.total_colocation > 0) {
                        html_total_services += '<div class="row service_info">';
                        html_total_services += '<div class="col-12 home-mobile-repon mb-2"><b>Colocation</b></div>';
                        // html_total_services += '<div class="col-sm-4 home-mobile-repon"></div>';
                        // html_total_services += '<div class="col-sm-4 text-right home-mobile-repon">';
                        // html_total_services += '</div>';

                        html_total_services += '<div class="col-sm-4 home-mobile-repon">Tổng</div>';
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">' + services_right.total_colocation + '</div>';
                        html_total_services += '<div class="col-sm-4 text-right home-mobile-repon">';
                        html_total_services += '<a href="/dich-vu/colocation/dang-su-dung" class="btn btn-secondary btn-sm">Chi tiết</a>';
                        html_total_services += '</div></div>';
                    }

                    if (services_right.total_email_hosting > 0) {
                        html_total_services += '<div class="row service_info">';
                        html_total_services += '<div class="col-12 home-mobile-repon mb-2"><b>Email Hosting</b></div>';
                        // html_total_services += '<div class="col-sm-4 home-mobile-repon"></div>';
                        // html_total_services += '<div class="col-sm-4 text-right home-mobile-repon">';
                        // html_total_services += '</div>';

                        html_total_services += '<div class="col-sm-4 home-mobile-repon">Tổng</div>';
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">' + services_right.total_email_hosting + '</div>';
                        html_total_services += '<div class="col-sm-4 text-right home-mobile-repon">';
                        html_total_services += '<a href="/dich-vu/email_hosting/dang-su-dung" class="btn btn-secondary btn-sm">Chi tiết</a>';
                        html_total_services += '</div></div>';
                    }

                    if (services_right.total_domain > 0) {
                        html_total_services += '<div class="row service_info">';
                        html_total_services += '<div class="col-12 home-mobile-repon mb-2"><b>Domain</b></div>';
                        // html_total_services += '<div class="col-sm-4 home-mobile-repon"></div>';
                        // html_total_services += '<div class="col-sm-4 text-right home-mobile-repon">';
                        // html_total_services += '</div>';

                        html_total_services += '<div class="col-sm-4 home-mobile-repon">Tổng</div>';
                        html_total_services += '<div class="col-sm-4 home-mobile-repon">' + services_right.total_domain + '</div>';
                        html_total_services += '<div class="col-sm-4 text-right home-mobile-repon">';
                        html_total_services += '<a href="/service/domain" class="btn btn-secondary btn-sm">Chi tiết</a>';
                        html_total_services += '</div></div>';
                    }
                    $('.home-service-chart').html(html_total_services);
                } else {
                    html_total_services += '<div class="row service_info">';
                    html_total_services += '<div class="col-sm-12 home-mobile-repon text-danger">Quý khách không có dịch vụ đang sử dụng!</div>';
                    html_total_services += '</div></div>';
                }
            },
            error: function(e) {
                console.log(e);
            },
        });
    }

});