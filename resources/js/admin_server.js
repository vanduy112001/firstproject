const { ajax } = require("jquery");

$(document).ready(function () {
    $(document).on('click', '.btn-delete-vps', function () {
        $('tr').removeClass('remove-vps');
        var id = $(this).attr('data-id');
        var ip = $(this).attr('data-ip');
        $(this).closest('tr').addClass('remove-vps');
        $('#delete-order').modal('show');
        $('.modal-title').text('Xóa Server');
        $('#notication-order').html('Bạn có muốn xóa Server <b class="text-danger"> (ID:' + id + ' - IP:' + ip + ' )</b> này không?');
        $('#button-multi-vps').attr('disabled', false);
        $('#button-order').val('Xác nhận');
        $('#button-order').attr('data-id', id);
        $('#button-order').attr('data-action', "delete");
        $('#button-order').fadeIn();
        $('#button-finish').fadeOut();
    });

    $('#button-order').on('click', function () {
        id = $(this).attr('data-id');
        action = $(this).attr('data-action');
        $.ajax({
            type: "get",
            url: "/admin/servers/delete_ajax_server",
            data: { id: id, action: action },
            dataType: "json",
            beforeSend: function () {
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-order').attr('disabled', true);
                $('#notication-order').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.error == 0) {
                    if (action == 'delete') {
                        if (data.action) {
                            var html = '<p class="text-danger">Xóa Server thành công</p>';
                            $('#notication-order').html(html);
                            $('#button-order').attr('disabled', false);
                            $('.remove-vps').fadeOut(1500);
                            $('#button-order').fadeOut();
                            $('#button-finish').fadeIn();
                        } else {
                            var html = '<p class="text-danger">Xóa Server thất bại</p>';
                            $('#notication-order').html(html);
                            $('#button-order').attr('disabled', false);
                        }
                    }
                    else if (action == 'off') {
                        if (data) {
                            $('.remove-vps .off').attr('disabled', false);
                            $('.remove-vps .on').attr('disabled', true);
                            $('.remove-vps .vps-status').html('<span class="text-danger">Đã tắt</span>');
                            $('#notication-order').html('<span class="text-success">Tắt VPS thành công</span>');
                            $('#button-order').fadeOut();
                            $('#button-finish').fadeIn();
                            $('tr').removeClass('remove-vps');
                        } else {
                            $('#notication-order').html('<span class="text-danger">Tắt VPS thất bại</span>');
                        }
                        $('#button-order').attr('disabled', false);
                    }
                    else if (action == 'on') {
                        if (data) {
                            $('.remove-vps .off').attr('disabled', true);
                            $('.remove-vps .on').attr('disabled', false);
                            $('.remove-vps .vps-status').html('<span class="text-success">Đang bật</span>');
                            $('#notication-order').html('<span class="text-success">Bật VPS thành công</span>');
                            $('#button-order').fadeOut();
                            $('#button-finish').fadeIn();
                            $('tr').removeClass('remove-vps');
                        } else {
                            $('#notication-order').html('<span class="text-danger">Bật VPS thất bại</span>');
                        }
                        $('#button-order').attr('disabled', false);
                    }
                    else if (action == 'restart') {
                        if (data) {
                            $('.remove-vps .off').attr('disabled', false);
                            $('.remove-vps .on').attr('disabled', true);
                            $('.remove-vps .vps-status').html('<span class="text-success">Đang bật</span>');
                            $('#notication-order').html('<span class="text-success">Khởi động lại VPS thành công</span>');
                            $('#button-order').fadeOut();
                            $('#button-finish').fadeIn();
                            $('tr').removeClass('remove-vps');
                        } else {
                            $('#notication-order').html('<span class="text-danger">Khởi động lại VPS thất bại</span>');
                        }
                        $('#button-order').attr('disabled', false);
                    }
                } else {
                    $('#notication-order').html('<span class="text-danger">Lỗi, không có hạnh động.</span>');
                }

            },
            error: function (e) {
                console.log(e);
                var html = '<p class="text-danger">Truy vấn VPS bị lỗi!</p>';
                $('#nnotication-order').html(html);
                $('#button-order').attr('disabled', false);
                $('.remove-vps').removeClass('remove-vps');
            }
        });
    });

    $('#qtt').on('change', function () {
        loadServer();
    })
    $('#q').on('keyup', function () {
        loadServer();
    })

    $("#status").on('change', function () {
        loadServer();
    })




    function loadServer() {
        var qtt = $('#qtt').val();
        var status = $('#status').val();
        var q = $('#q').val();
        $.ajax({
            url: '/admin/servers/getServer',
            data: { qtt: qtt, status: status, q: q },
            dataType: 'json',
            beforeSend: function () {
                var html = '<td colspan="15" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
                $('tbody').html(html);
            },
            success: function (data) {
                // console.log(data);
                if (data.data != '') {
                    screent_server(data);
                } else {
                    $('tbody').html('<td colspan="15" class="text-center">Không có dịch vụ server trong dữ liệu.</td>');
                }
                count = $('.checkbox:checked').length;
            },
            error: function (e) {
                console.log(e);
                $('tbody').html('<td colspan="15" class="text-center">Truy vấn đến dịch vụ thất bại.</td>');
            }
        });
    }
    function screent_server(data) {
        var html = '';
        $.each(data.data, function (index, server) {
            if (server) {
                html += '<tr>';
                // checkbox
                html += '<td><input type="checkbox" value="' + server.id + '" data-ip="' + server.ip + '" class="checkbox"></td>';
                // khách hàng
                html += '<td><a href="/admin/users/detail/' + server.user_id + '">' + server.text_user_name + '</a></td>';
                // sản phẩm
                html += '<td>' + server.text_product + '</td>';
                // ip
                if (server.ip) {
                    html += '<td><a href="/admin/server/detail/' + server.id + '">' + server.ip + '</a>';
                } else {
                    html += '<td class="text-danger">Chưa tạo</td>';
                }
                // cấu hình
                html += '<td>' + server.specs + '</td>';
                // datacenter
                html += '<td>';
                html += server.location + '<br>';
                if (server.rack == null) {
                    rack = '';
                } else {
                    rack = server.rack;
                }
                html += rack;
                html += '</td>';
                // ngay tao
                html += '<td><span>' + server.date_create + '</span></td>';
                // ngay ket thuc
                html += '<td class="next_due_date">';
                html += '<span>' + server.text_next_due_date + '<span>';
                html += '</td>';
                // tổng thoi gian thue
                html += '<td>' + server.total_time + '</td>';
                // chu kỳ thanh toán
                html += '<td>' + server.text_billing_cycle + '</td>';
                // giá
                if (server.amount) {
                    html += '<td><b class="text-danger">' + server.total + '</b></td>';
                } else {
                    html += '<td>' + server.total + '</td>';
                }
                // trang thai
                html += '<td class="server-status">' + server.text_status + '</td>';
                // hành động
                html += '<td class="button-action">';
                html += ' <a href="/admin/servers/detail/' + server.id + '" class="btn btn-sm btn-warning text-white"><i class="fas fa-edit"></i></a>';
                if (server.send_mail_create == '') {
                    html += '<a class="btn btn-success btn-sm text-light" data-toggle="tooltip" href="/admin/servers/gui-mail-hoan-thanh-cai-dat/' + server.id + '" data-placement="top" title="Gửi email hoàn thành cài đặt"><i class="fas fa-envelope-open-text"></i></a>';
                }
                html += ' <button class="btn btn-sm btn-danger btn-delete-vps" data-id="' + server.id + '" data-ip="' + server.ip + '" ><i class="fas fa-trash-alt"></i></button>';
                html += '</td>';
                html += '</tr>';
            } else {
                html += '<tr>';
                html += '<td colspan="15" class="text-center text-danger">server này không tồn tại.</td>';
                html += '</tr>';
            }
        })

        $('tbody').html(html);
        $('.total-item').text(data.data.length)
        $('[data-toggle="tooltip"]').tooltip();

        var total = data.total;
        var per_page = data.perPage;
        var current_page = data.current_page;
        var html_page = '';
        if (total > per_page) {
            if (total / per_page > 11) {
                var page = parseInt(total / per_page + 1);
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_server_use">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                if (current_page < 7) {
                    for (var i = 1; i < 9; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= 7 || current_page <= page - 7) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = current_page - 3; i <= current_page + 3; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 1; i <= page; i++) {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }
                } else if (current_page >= page - 6) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                    html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                    for (var i = page - 6; i < page; i++) {
                        var active = '';
                        if (i == current_page) {
                            active = 'active';
                        }
                        if (active == 'active') {
                            html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        } else {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                        }
                    }
                }

                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '</nav>';
                html_page += '</td>';
            } else {
                var page = total / per_page + 1;
                html_page += '<td colspan="15" class="text-center link-right">';
                html_page += '<nav>';
                html_page += '<ul class="pagination pagination_server_use">';
                if (current_page != 1) {
                    html_page += '<li class="prev"><a href="#" data-page="' + (current_page - 1) + '" class="page-link">&laquo</a></li>';
                } else {
                    html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                }
                for (var i = 1; i < page; i++) {
                    var active = '';
                    if (i == current_page) {
                        active = 'active';
                    }
                    if (active == 'active') {
                        html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    } else {
                        html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + i + '" >' + i + '</a></li>';
                    }

                }
                if (current_page != page.toPrecision(1)) {
                    html_page += '<li class="page-item"><a href="#" class="page-link" data-page="' + current_page + '">&raquo;</a></li>';
                } else {
                    html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                }
                html_page += '</ul>';
                html_page += '<nav>';
                html_page += '</td>';
            }
        }
        $('tfoot').html(html_page);
        $vpsCheckbox = $('.checkbox');
    }

    count = 0;
    $(".checkbox").on('change', function () {
        if ($(this).is(':checked')) {
            count = $('.checkbox:checked').length;
            $(".last-item").text(count);

        } else {
            count = $('.checkbox:checked').length;
            $(".last-item").text(count);
        }
        if (count > 0) {
            $("#btn-hide-group").css('display', 'block');
        } else {
            $("#btn-hide-group").css('display', 'none');
        }
    });
    $("#deleteServerBtn").on('click', function (event) {
        if (confirm('Bạn có chắc muốn thực hiện hành động này không?')) {
            data_ids = [];
            action = 'delete';
            $(".checkbox:checked").each(function () {
                data_ids.push($(this).attr('value') + ',');
            })
            $.ajax({
                type: "GET",
                url: "/admin/servers/delete_ajax_server",
                data: {data_ids : data_ids, action:action},
                dataType: "json",
                success: function (data) {
                    // console.log(data);
                    if (data.error == 0) {
                        if (action == 'delete') {
                            if (data.action) {
                                var html = '<p class="text-danger">Xóa Server thành công</p>';
                                $('#notication-order').html(html);
                                $('#button-order').attr('disabled', false);
                                $('.remove-vps').fadeOut(1500);
                                $('#button-order').fadeOut();
                                $('#button-finish').fadeIn();
                            } else {
                                var html = '<p class="text-danger">Xóa Server thất bại</p>';
                                $('#notication-order').html(html);
                                $('#button-order').attr('disabled', false);
                            }
                        }
                        else if (action == 'off') {
                            if (data) {
                                $('.remove-vps .off').attr('disabled', false);
                                $('.remove-vps .on').attr('disabled', true);
                                $('.remove-vps .vps-status').html('<span class="text-danger">Đã tắt</span>');
                                $('#notication-order').html('<span class="text-success">Tắt VPS thành công</span>');
                                $('#button-order').fadeOut();
                                $('#button-finish').fadeIn();
                                $('tr').removeClass('remove-vps');
                            } else {
                                $('#notication-order').html('<span class="text-danger">Tắt VPS thất bại</span>');
                            }
                            $('#button-order').attr('disabled', false);
                        }
                        else if (action == 'on') {
                            if (data) {
                                $('.remove-vps .off').attr('disabled', true);
                                $('.remove-vps .on').attr('disabled', false);
                                $('.remove-vps .vps-status').html('<span class="text-success">Đang bật</span>');
                                $('#notication-order').html('<span class="text-success">Bật VPS thành công</span>');
                                $('#button-order').fadeOut();
                                $('#button-finish').fadeIn();
                                $('tr').removeClass('remove-vps');
                            } else {
                                $('#notication-order').html('<span class="text-danger">Bật VPS thất bại</span>');
                            }
                            $('#button-order').attr('disabled', false);
                        }
                        else if (action == 'restart') {
                            if (data) {
                                $('.remove-vps .off').attr('disabled', false);
                                $('.remove-vps .on').attr('disabled', true);
                                $('.remove-vps .vps-status').html('<span class="text-success">Đang bật</span>');
                                $('#notication-order').html('<span class="text-success">Khởi động lại VPS thành công</span>');
                                $('#button-order').fadeOut();
                                $('#button-finish').fadeIn();
                                $('tr').removeClass('remove-vps');
                            } else {
                                $('#notication-order').html('<span class="text-danger">Khởi động lại VPS thất bại</span>');
                            }
                            $('#button-order').attr('disabled', false);
                        }
                    } else {
                        $('#notication-order').html('<span class="text-danger">Lỗi, không có hạnh động.</span>');
                    }
                    loadServer();
                },
                error: function (e) {
                    console.log(e);
                    var html = '<p class="text-danger">Truy vấn VPS bị lỗi!</p>';
                    $('#nnotication-order').html(html);
                    $('#button-order').attr('disabled', false);
                    $('.remove-vps').removeClass('remove-vps');
                }
            });
        } else {
            event.preventDefault();
        }
    })

});

$(document).on('click', '.checkbox_all', function () {
    if ($(this).is(':checked')) {
        $('.checkbox').prop('checked', this.checked);
        $('tbody tr').addClass('action');
    } else {
        $('.checkbox').prop('checked', this.checked);
        $('tbody tr').removeClass('action');
    }
    var checked = $('.checkbox:checked');
    $('.last-item').text(checked.length);
    if ($('.checkbox:checked').length > 0) {
        $("#btn-hide-group").css('display', 'block');
    } else {
        $("#btn-hide-group").css('display', 'none');
    }
})

