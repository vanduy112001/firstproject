$(document).ready(function () {
  $('#datepicker').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
  });

  $('#date_create span').on('click', function () {
      var html = '';
      html += '<div class="input-group input-group-sm">';
      html += '<input type="text" name="date_create" class="form-control mr-2 rounded create_date_create" id="datepicker" placeholder="Cập nhập ngày tạo" autocomplete="off">';
      html += '</div>';
      $('#form_date_create').html(html);
      $('#datepicker').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy'
      });
  })

  $('#next_due_date span').on('click', function () {
      var html = '';
      html += '<div class="input-group input-group-sm">';
      html += '<input type="text" name="next_due_date" class="form-control mr-2 rounded create_next_due_date" id="datepicker_next_due_date" placeholder="Cập nhập ngày kết thúc" autocomplete="off">';
      html += '</div>';
      $('#form_next_due_date').html(html);
      $('#datepicker_next_due_date').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy'
      });
  })

  $('#billing_cycle span').on('click', function () {
      var html = '';
      html += '<select class="form-control" name="billing_cycle">';
      html += '<option value="">Chọn thời gian thuê</option>';
      html += '<option value="monthly">1 Tháng</option>';
      html += '<option value="twomonthly">2 Tháng</option>';
      html += '<option value="quarterly">3 Tháng</option>';
      html += '<option value="semi_annually">6 Tháng</option>';
      html += '<option value="annually">1 Năm</option>';
      html += '<option value="biennially">2 Năm</option>';
      html += '<option value="triennially">3 Năm</option>';
      html += '</select>';
      $('#form_billing_cycle').html(html);
      // var html_amount = '<input type="text" name="sub_total" value="" class="form-control" placeholder="Thành tiền">';
      // $('.sub_total_hidden').html(html_amount);
  })

  $('#sub_total .ml-4').on('click', function () {
    var html_amount = '<input type="text" name="sub_total" value="" class="form-control" placeholder="Thành tiền">';
    $('.sub_total_hidden').html(html_amount);
  })

  $(document).on('click', '.btn-delete-colo', function () {
      var id = $(this).attr('data-id');
      var domain = $(this).attr('data-ip');
      $('tr').removeClass('action_email');
      $(this).closest('tr').addClass('action_email');
      $('#delete_vps').modal('show');
      $('.modal-title').text('Xoá Email Hosting');
      $('#notication-invoice').html('<span>Bạn có muốn xóa Email Hosting <b class="text-danger">' + domain + '</b> này không?</span>');
      $('#button-vps').attr('data-id', id);
      $('#button-vps').fadeIn();
      $('#button-vps-finish').fadeOut();
  })

  $('#button-vps').on('click', function () {
      var id = $(this).attr('data-id');
      var token = $('meta[name="csrf-token"]').attr('content');
      $.ajax({
         url: '/admin/email_hosting/delete',
         type: 'post',
         data: {id: id, _token: token},
         dataType: 'json',
         beforeSend: function(){
             var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
             $('#button-vps').attr('disabled', true);
             $('#notication-invoice').html(html);
         },
         success: function (data) {
             // console.log(data);
             if (data) {
                 var html = '<p class="text-danger">Xóa Email Hosting thành công</p>';
                 $('#notication-invoice').html(html);
                 $('#button-vps').attr('disabled', false);
                 $('.action_email').fadeOut(1500);
                 $('#button-vps').fadeOut();
                 $('#button-vps-finish').fadeIn();
             } else {
                 var html = '<p class="text-danger">Xóa Email Hosting thất bại</p>';
                 $('#notication-invoice').html(html);
                 $('#button-vps').attr('disabled', false);
             }
         },
         error: function (e) {
            console.log(e);
            $('#notication-invoice').html('<span class="text-danger">Truy vấn Colocation thất bại!</span>');
         }
      })
  })

})
