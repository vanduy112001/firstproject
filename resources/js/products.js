$(document).ready(function() {

  $('#createGroup').on('click', function()
  {
    $('#modal-product').modal('show');
    $('.modal-title').text('Tạo nhóm sản phẩm');
    $('#button-submit').val('Tạo nhóm sản phẩm');
    $('#notication').html("");
    $('#form-group-product')[0].reset();
    $('#action').val('create');
  })

  $('#button-submit').on('click', function() {

    var form_data = $('#form-group-product').serialize();
    var name = $('#name').val();
    var title = $('#title').val();
    var type = $('#type').val();
    var validate = group_product_validate(name, title, type);
    // console.log(form_data);
    if (validate == false) {
      $.ajax({
        type: "post",
        url: "/admin/products/create-product",
        data: form_data,
        dataType: "json",
        beforeSend: function(){
          var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
          $('#notication').html(html);
          $('#form-group-product').css('opacity', 0.5);
          $('#button-submit').attr('disabled', true);
          $('#form-group-product').attr('disabled', true);
        },
        success: function (response) {
          // console.log(response);
          if (response) {
            var html = '<p class="text-success">'+response+'</p>';
            $('#notication').html(html);
            $('#form-group-product')[0].reset();
            $('#button-submit').val('Tạo nhóm sản phẩm');
            $('.modal-title').text('Tạo nhóm sản phẩm');
            $('#form-group-product').css('opacity', 1);
            $('#button-submit').attr('disabled', false);
            $('#form-group-product').attr('disabled', false);
          } else {
            var html = '<p class="text-danger">Tạo nhóm sản phẩm thất bại</p>';
            $('#notication').html(html);
            $('#form-group-product').css('opacity', 1);
            $('#button-submit').attr('disabled', false);
            $('#form-group-product').attr('disabled', false)
          }
          list_product();
        },
        error: function(e) {
          console.log(e);
          var html = '<p class="text-danger">Tạo / Chỉnh sửa nhóm sản phẩm thất bại</p>';
          $('#notication').html(html);
          $('#form-group-product').css('opacity', 1);
          $('#button-submit').attr('disabled', false);
          $('#form-group-product').attr('disabled', false);
        }
      });
    }

  });
  // sua nhom san pham
  $(document).on('click', '.editGroup', function() {
    var id = $(this).attr('data-id');
    $('#form-group-product')[0].reset();
    $.ajax({
      url: "/admin/products/editGroup",
      data: {id: id},
      dataType: "json",
      success: function (data) {
        $('#modal-product').modal('show');
        $('.modal-title').text('Sửa nhóm sản phẩm');
        $('#button-submit').val('Sửa nhóm sản phẩm');
        $('#notication').html("");
        $('#name').val(data.name);
        $('#title').val(data.title);
        $('#id-group').val(data.id);
        $('#stt').val(data.stt);
        $('#action').val('update');
        var html = '';
        var selected = '';
        var selected_vps = '';
        var selected_hosting = '';
        var selected_vps_us = '';
        var selected_vps_uk = '';
        var selected_vps_si = '';
        var selected_vps_hl = '';
        var selected_vps_uc = '';
        var selected_vps_ca = '';
        var selected_email_hosting = '';
        var selected_server = '';
        var selected_addon_server = '';
        var selected_proxy = '';
        var selected_colocation = '';
        var selected_addon_colocation = '';
        if (data.type == 'vps') {
            selected_vps = 'selected';
        }
        else if (data.type == 'vps_us') {
            selected_vps_us = 'selected';
        }
        else if (data.type == 'vps_uk') {
            selected_vps_uk = 'selected';
        }
        else if (data.type == 'vps_sing') {
            selected_vps_si = 'selected';
        }
        else if (data.type == 'vps_hl') {
            selected_vps_hl = 'selected';
        }
        else if (data.type == 'vps_uc') {
            selected_vps_uc = 'selected';
        }
        else if (data.type == 'vps_ca') {
            selected_vps_ca = 'selected';
        }
        else if (data.type == 'hosting') {
            selected_hosting = 'selected';
        }
        else if ( data.type == 'email_hosting' ) {
            selected_email_hosting = 'selected';
        }
        else if ( data.type == 'server' ) {
            selected_server = 'selected';
        }
        else if ( data.type == 'addon_server' ) {
            selected_addon_server = 'selected';
        }
        else if ( data.type == 'proxy' ) {
            selected_proxy = 'selected';
        }
        else if ( data.type == 'colocation' ) {
            selected_colocation = 'selected';
        }
        else if ( data.type == 'addon_colo' ) {
          selected_addon_colocation = 'selected';
        }
        else {
            selected = 'selected';
        }
        html += '<option value="" disabled '+ selected +' >Chọn loại sản phẩm</option>';
        html += '<option value="vps" '+ selected_vps +'>VPS</option>';
        html += '<option value="vps_us" '+ selected_vps_us +'>VPS US</option>';
        html += '<option value="vps_uk" '+ selected_vps_uk +'>VPS UK</option>';
        html += '<option value="vps_sing" '+ selected_vps_si +'>VPS Singapore</option>';
        html += '<option value="vps_hl" '+ selected_vps_hl +'>VPS Hà Lan</option>';
        html += '<option value="vps_uc" '+ selected_vps_uc +'>VPS Úc</option>';
        html += '<option value="vps_ca" '+ selected_vps_ca +'>VPS Canada</option>';
        html += '<option value="hosting" '+ selected_hosting +'>Hosting</option>';
        html += '<option value="email_hosting" '+ selected_email_hosting +'>Email Hosting</option>';
        html += '<option value="server" '+ selected_server +'>Server</option>';
        html += '<option value="addon_server" '+ selected_addon_server +'>Addon Server</option>';
        html += '<option value="proxy" '+ selected_proxy +'>Proxy</option>';
        html += '<option value="colocation" '+ selected_colocation +'>Colocation</option>';
        html += '<option value="addon_colo" '+ selected_addon_colocation +'>Addon Colocation</option>';
        $('#type').html(html);
        $('#link').val(data.link);
        if (data.hidden == 1) {
            $('#hidden').attr('checked', true);
        } else {
            $('#hidden').attr('checked', false);
        }
      }
    });
  });

  list_product();
  // list group_product va product
  function list_product() {
    $.ajax({
      url: "/admin/products/list-product",
      dataType: "json",
      beforeSend: function(){
        var html = '<td colspan="6" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
        $('tbody').html(html);
      },
      success: function (data) {
        // console.log(data);
        var html = '';
        if (data == '') {
          html += '<td colspan="6" class="text-danger">Không có nhóm sản phẩm và sản phẩm</td>';
          $('tbody').html(html);
        } else {
          $.each(data.group_products, function (index, group_product) {
            var num = index+1;
            html += '<tr class="table-group-product">';
            html += '<td><button type="button" class="button-minus btn" style="padding: 0;" data-id="'+group_product.id+'"><span class="num">'+romanize(num)+'</span></button></td>';
            html += '<td><span class="title-table-group">Nhóm sản phẩm: </span>' + group_product.name + ' - ' + group_product.title + '</td>';
            html += '<td></td>';
            html += '<td></td>';
            html += '<td></td>';
            if ( group_product.hidden != 0 ) {
              html += '<td>Ẩn</td>';
            } else {
              html += '<td>Hiện</td>';
            }
            html += '<td></td>';
            html += '<td>';
            html += '<button class="btn btn-sm btn-warning editGroup"  data-id='+group_product.id+'><i class="fas fa-edit text-white"></i></button>';
            html += '<button class="btn btn-sm btn-danger deleteGroup" data-name=';
            html += '\''+group_product.name + '\'';
            html += ' data-id='+ group_product.id +'><i class="fas fa-trash-alt"></i></button>';
            html += '</td>';
            html += '</tr>';
            if (group_product.products != null) {
              $.each(group_product.products, function (indexProduct, product) {
                // console.log(product.meta_product);
                var stt = indexProduct+1;
                html += '<tr class="tager'+group_product.id+'">';
                html += '<td><span class="child-num">'+ stt +'</span></td>';
                html += '<td>' + product.name + '</td>';
                html += '<td>' + product.type_product + '</td>';
                html += '<td style="font-size: 13px;">'+ product.info +'</td>';
                html += '<td>'+ screen_pricing(data.pricings , product.id) +'<br>'+ product.duplicate +'</td>';
                if ( product.hidden != 0 ) {
                  html += '<td>Ẩn</td>';
                } else {
                  html += '<td></td>';
                }
                html += '<td class="description">';
                if (product.description) {
                  html +=  product.description.substr(0, 50);
                }
                html += '</td>';
                html += '<td class="button-action">';
                html += '<a href="/admin/products/edit/'+ product.id +'" class="btn btn-sm btn-warning"><i class="fas fa-edit text-white"></i></a>';
                html += '<button class="btn btn-sm btn-danger deleteProduct" data-id="'+ product.id +'" data-name=\''+ product.name +'\'><i class="fas fa-trash-alt"></i></button>';
                html += '</td>';
                html += '</tr>';
                // console.log(html);
              });
            }
          });
        }
        // //console.log(html);
        $('tbody').html(html);
      },
      error: function(e) {
        //console.log(e);
      }
    });
  }
  function romanize(num) {
    var lookup = {M:1000,CM:900,D:500,CD:400,C:100,XC:90,L:50,XL:40,X:10,IX:9,V:5,IV:4,I:1},
        roman = '',
        i;
    for ( i in lookup ) {
      while ( num >= lookup[i] ) {
        roman += i;
        num -= lookup[i];
      }
    }
    return roman;
  }
  // screen pricing
  function screen_pricing(pricings , product_id) {
      var html = '';
      $.each(pricings, function (index, pricing) {
        if (pricing.product_id == product_id) {
            if(pricing.type == 0) {
              html = "0 / Free";
            }
            else if(pricing.type == 'one_time') {
              html = addCommas(pricing.one_time_pay) + ' / Vĩnh viễn';
            }
            else {
              if (pricing.monthly > 0) {
                html = addCommas(pricing.monthly) + ' / 1 Tháng';
              }
              else if (pricing.twomonthly > 0) {
                html = addCommas(pricing.twomonthly) + ' / 2 Tháng';
              }
              else if (pricing.quarterly > 0) {
                html = addCommas(pricing.quarterly) + ' / 3 Tháng';
              }
              else if (pricing.semi_annually > 0) {
                html = addCommas(pricing.semi_annually) + ' / 6 Tháng';
              }
              else if (pricing.annually > 0) {
                html = addCommas(pricing.annually) + ' / 1 Năm';
              }
              else if (pricing.biennially > 0) {
                html = addCommas(pricing.biennially) + ' / 2 Năm';
              }
              else if (pricing.triennially > 0) {
                html = addCommas(pricing.triennially) + ' / 3 Năm';
              }
              else {
                html = '0 / 0';
              }
            }
        }
      });
      return html;
  }
  function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
  // delete product
  $(document).on('click', '.deleteGroup', function() {
    $('#delete-product').modal('show');
    var name = $(this).attr('data-name');
    var id = $(this).attr('data-id');
    var html = 'Bạn có muốn xóa nhóm sản phẩm <b class="text-danger">'+ name +'</b> này không?';
    $('#button-product').attr('data-id', id);
    $('#button-product').attr('data-action', 'group');
    $('#button-product').val('Xóa nhóm sản phẩm');
    $('#notication-product').html(html);
  });


  // delete group product
  $(document).on('click', '.deleteProduct', function() {
    $('#delete-product').modal('show');
    var name = $(this).attr('data-name');
    var id = $(this).attr('data-id');
    var html = 'Bạn có muốn xóa sản phẩm <b class="text-danger">'+ name +'</b> này không?';
    $('#button-product').attr('data-id', id);
    $('#button-product').attr('data-action', 'product');
    $('#button-product').val('Xóa sản phẩm');
    $('#notication-product').html(html);
  });
  // click vao button-product
  $('#button-product').on('click', function () {
    var id = $(this).attr('data-id');
    var action = $(this).attr('data-action');
    // //console.log(id, action);
    $.ajax({
      type: "get",
      url: "/admin/products/delete",
      data:  { id: id, action: action },
      dataType: "json",
      beforeSend: function(){
        var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
        $('#notication-product').html(html);
        $('#button-product').attr('disabled', true);
        $('#delete-product').css('opacity', 0.5);
      },
      success: function (data) {
        //console.log(data);
        var html = '<div class="text-center text-danger">' + data + '</div>';
        $('#notication-product').html(html);
        $('#button-product').attr('disabled', false);
        $('#delete-product').css('opacity', 1);
        list_product();
      },
      error: function(e) {
        console.log(e);
        var html = '<div class="text-center text-danger">Lỗi khi xóa sản phẩm.</div>';
        $('#notication-product').html(html);
        $('#button-product').attr('disabled', false);
        $('#delete-product').css('opacity', 1);
      }
    });
  });
  // Thu gọn
  $(document).on('click', '.button-minus', function() {
      $(this).removeClass('button-minus');
      $(this).addClass('button-add');
      var id = $(this).attr('data-id');
      var class_minus = '.tager' + id;
      // console.log('da click', class_minus);
      $(class_minus).fadeOut();
      $(this).children('.fa-minus').removeClass('fa-minus');
      $(this).children('i').addClass('fa-plus');
  });
  // Mở rộng button-minus
  $(document).on('click', '.button-add', function() {
      $(this).removeClass('button-add');
      $(this).addClass('button-minus');
      var id = $(this).attr('data-id');
      var class_minus = '.tager' + id;
      // console.log('da click', class_minus);
      $(class_minus).fadeIn();
      $(this).children('.fa-plus').removeClass('fa-plus');
      $(this).children('i').addClass('fa-minus');
  });
  // validate group product
  function group_product_validate(name, title, type) {

    if (name == null || name == '') {
      $('#notication').text('Tên nhóm sản phẩm không được để trống.')
      return true;
    } else if(name.length < 5) {
      $('#notication').text('Số ký tự của tên nhóm sán phẩm không được nhỏ hơn 5 ký tự.')
      return true;
    } else if(name.length > 100) {
      $('#notication').text('Số ký tự của tên nhóm sán phẩm không được lớn hơn 100 ký tự.')
      return true;
    } else if(title.length < 5) {
      $('#notication').text('Số ký tự của tiều đề hiển thị không được nhỏ hơn 5 ký tự.')
      return true;
    } else if(title.length > 200) {
      $('#notication').text('Số ký tự của tiều đề hiển thị không được lớn hơn 200 ký tự.')
      return true;
    } else if( title == null || title == '' ) {
      $('#notication').text('Tiều đề hiển thị không được để trống.')
      return true;
    } else if(type == null || type == '') {
      $('#notication').text('Loại sản phẩm không được để trống.')
      return true;
    } else {
      return false;
    }



  }
})
