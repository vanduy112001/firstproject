$(document).ready(function() {
    $('#reset_form').on('click', function() {
        $('#form_customer')[0].reset();
        $('.radio_personal').fadeIn();
        $('.radio_organization').hide();
    });
    $('#radioPrimary1').on('click', function() {
        $('.radio_personal').fadeIn();
        $('.radio_organization').hide();
    });
    $('#radioPrimary2').on('click', function() {
        $('.radio_personal').hide();
        $('.radio_organization').fadeIn();
    });

    // kiếm tra type_checked
    var type = $('.type_checked:checked').val();
    if (type == 1) {
      $('.radio_personal').hide();
      $('.radio_organization').fadeIn();
    } else {
      $('.radio_personal').fadeIn();
      $('.radio_organization').hide();
    }
    // sao chép thông tin người đăng ký cho 2 phần dướid customer_dk_name customer_dk_gender customer_dk_cv customer_dk_date  customer_dk_cmnd  customer_dk_phone
    $('.info_copy').on('click', function() {
        // Người đại diện
        $('.customer_dd_name').val($('.customer_dk_name').val());
        $('.customer_dd_gender').val($('.customer_dk_gender').val());
        $('.customer_dd_cv').val($('.customer_dk_cv').val());
        $('#customer_dd_date').val($('.customer_dk_date').val());
        $('.customer_dd_cmnd').val($('.customer_dk_cmnd').val());
        // người đại diện làm thủ tục
        $('.customer_tt_name').val($('.customer_dk_name').val());
        $('.customer_tt_gender').val($('.customer_dk_gender').val());
        $('#customer_tt_date').val($('.customer_dk_date').val());
        $('.customer_tt_cmnd').val($('.customer_dk_cmnd').val());
    })
    // sao chép thông tin người đại diện cho người đại diện dăng ký
    $('.info_dd').on('click', function() {
        $('.customer_tt_name').val($('.customer_dd_name').val());
        $('.customer_tt_gender').val($('.customer_dd_gender').val());
        $('#customer_tt_date').val($('#customer_dd_date').val());
        $('.customer_tt_cmnd').val($('.customer_dd_cmnd').val());
        $('.customer_tt_email').val($('.customer_dd_email').val());
    })
    // Xóa customer
    $('.btn-delete-customer').on('click', function(e) {
        e.preventDefault();
        $('tr').removeClass('delete-row');
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        $('#delete-customer').modal('show');
        $('#notication-customer').html('<p>Bạn có muốn xóa khách hàng <b class="text-danger">'+name+'</b> này không?</p>');
        $('#button-customer').attr('data-id', id);
        $('#button-customer').fadeIn();
        $('#button-finish').hide();
        $(this).closest('tr').addClass('delete-row');
    });

    $('#button-customer').on('click', function() {
        var id = $(this).attr('data-id');
        $.ajax({
            type: 'get',
            url: '/khach-hang/xoa-khach-hang',
            dataType: 'json',
            data: {id: id},
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button-customer').attr('disabled', true);
                $('#notication-customer').html(html);
            },
            success: function(data) {
                if (data) {
                    $('#notication-customer').html('<p class="text-danger">Xóa hồ sơ khách hàng thành công.</p>');
                    $('.delete-row').fadeOut(1500);
                } else {
                    $('#notication-customer').html('<p class="text-danger">Xóa hồ sơ khách hàng thất bại.</p>');
                    $('tr').removeClass('delete-row');
                }
                $('#button-customer').attr('disabled', false);
                $('#button-customer').hide();
                $('#button-finish').fadeIn();
            },
            error: function(error) {
                console.error(error);
                $('#notication-customer').html('<p class="text-danger">Truy vấn quản lý khách hàng không thành công.</p>');
                $('#button-customer').attr('disabled', false);
                $('#button-customer').hide();
                $('#button-finish').fadeIn();
            }
        });
    });

    $('.btn-detail-delete-customer').on('click', function(e) {
        e.preventDefault();
        var id = $(this).attr('data-id');
        var name = $(this).attr('data-name');
        $('#delete-customer').modal('show');
        $('#notication-customer').html('<p>Bạn có muốn xóa khách hàng <b class="text-danger">'+name+'</b> này không?</p>');
        $('#button-detail-customer').attr('data-id', id);
    });

    $('#button-detail-customer').on('click', function() {
      var id = $(this).attr('data-id');
      $.ajax({
          type: 'get',
          url: '/khach-hang/xoa-khach-hang',
          dataType: 'json',
          data: {id: id},
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#button-customer').attr('disabled', true);
              $('#notication-customer').html(html);
          },
          success: function(data) {
              if (data) {
                  $('#notication-customer').html('<p class="text-danger">Xóa hồ sơ khách hàng thành công.</p>');
                  window.location = '/khach-hang';
              } else {
                  $('#notication-customer').html('<p class="text-danger">Xóa hồ sơ khách hàng thất bại.</p>');
              }
              $('#button-customer').attr('disabled', false);
          },
          error: function(error) {
              console.error(error);
              $('#notication-customer').html('<p class="text-danger">Truy vấn quản lý khách hàng không thành công.</p>');
              $('#button-customer').attr('disabled', false);
              $('#button-customer').hide();
              $('#button-finish').fadeIn();
          }
      });
    })

});
