$(document).ready(function () {

  $('#product_id').on('change', function () {
      var price = $('#product_id :selected').attr('data-price');
      $('#total').val(price);
  })

  $('#price_override').on('click', function () {
      if( $(this).is(':checked') ) {
          $('#total').attr('disabled', false);
      } else {
          $('#total').attr('disabled', true);
      }
  })

  $('#user_id').on('change', function() {
      var user_id = $(this).val();
      var type_vps = $('#type_vps').val();
      $.ajax({
          type: "get",
          url: "/admin/orders/list_product",
          data: {action: type_vps, user_id: user_id},
          dataType: "json",
          beforeSend: function(){
              var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
              $('#product').html(html);
          },
          success: function (data) {
              var html = '';
              // 1 form-group
              html += '<select name="product_id" id="product_id" data-value="Vps" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;">';
              html += '<option disabled selected>Chọn sản phẩm</option>';
              if (data.product != null) {
                  $.each(data.product, function (index, product) {
                      html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                  });
              } else {
                  html += '<option disabled class="text-danger">Không có sản phẩm VPS</option>';
              }
              html += '</select>';
              $('#product').html(html);
              $('.select2').select2();
          },
          error: function(e) {
              console.log(e);
              var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
              $('#product').html(html);
          }
      });
  });

})
