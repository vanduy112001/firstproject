$(document).ready(function () {
    $('#company').on('click', function(event) {
        event.preventDefault();
        var invoice_id  = $('#invoice_id').val();
        $.ajax({
            url: '/pay-in-online/check-invoices?id=' + invoice_id,
            dataType: 'json',
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.screent-payment-live').html(html);
            },
            success: function(data) {
              //console.log(data);
                var html = '';
                if(data != '' || data != null) {
                    html += '<div class="company-name">Công ty TNHH MTV Đại Việt Số</div>';
                    html += '<div class="info-company">';
                    html += '<b>Địa chỉ văn phòng:</b> 67 - Nguyễn Thị Định - quận Sơn Trà - Thành phố Đà Nẵng <br>';
                    html += '<b>Điện thoại hỗ trợ:</b> 0236 4455 789';
                    html += '<br><b>Mã giao dịch</b>: ' + data.ma_gd;
                    html += '</div>';
                    $('.screent-payment-live').html(html);
                } else {
                    var html = '<div class="text-center text-danger">Hóa đơn này không có thật. Quy khách vui lòng kiểm tra lại hóa đơn.</div>';
                    $('.screent-payment-live').html(html);
                }
            },
            error: function(e) {
                console.log(e);
                var html = '<div class="text-center text-danger">Truy vấn hóa đơn không thành công.</div>';
                $('.screent-payment-live').html(html);
            }
        });
    });

    $('#atm').on('click', function(event) {
        event.preventDefault();
        var invoice_id  = $('#invoice_id').val();
        $.ajax({
            url: '/pay-in-online/check-invoices?id=' + invoice_id,
            dataType: 'json',
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.screent-payment-live').html(html);
            },
            success: function(data) {
              //console.log(data);
                var html = '';
                if(data != '' || data != null) {
                    html += '<div class="info-company">';
                    html += '<div class="info-bank">';
                    html += '<b>Bank Name:</b> Vietcombank <br>';
                    html +=  '<b>Account Name:</b> Nguyen Thi Ai Hoa <br>';
                    html += '<b>Account Number:</b> 0041000830880 <br>';
                    html += '</div>';
                    // html += '<div class="info-bank">';
                    // html += '<b>Bank Name:</b> Techcombank <br>';
                    // html +=  '<b>Account Name:</b> Nguyen Thi Ai Hoa <br>';
                    // html += '<b>Account Number:</b> 19033827040017 <br>';
                    // html += '</div>';
                    html += '<br><b>Nội dung</b>: <br>- '+data.discription +'<br>- Mã giao dịch ' + data.ma_gd;
                    html += '</div>';
                    $('.screent-payment-live').html(html);
                } else {
                    var html = '<div class="text-center text-danger">Hóa đơn này không có thật. Quy khách vui lòng kiểm tra lại hóa đơn.</div>';
                    $('.screent-payment-live').html(html);
                }
            },
            error: function(e) {
                console.log(e);
                var html = '<div class="text-center text-danger">Truy vấn hóa đơn không thành công.</div>';
                $('.screent-payment-live').html(html);
            }
        });
    });

    $('#momo').on('click', function(event) {
        event.preventDefault();
        var invoice_id  = $('#invoice_id').val();
        $.ajax({
            url: '/pay-in-online/check-invoices?id=' + invoice_id,
            dataType: 'json',
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.screent-payment-live').html(html);
            },
            success: function(data) {
              //console.log(data);
                var html = '';
                if(data != '' || data != null) {
                    html += '<div class="info-company">';
                    html += '<a href="/pay-in-online/thanh-toan-momo?invoice_id='+ invoice_id +'&ma_gd='+ data.ma_gd +'" class="btn btn-success" target="_blank">Tiến hành thanh toán MOMO</a>';
                    html += '</div>';
                    $('.screent-payment-live').html(html);
                } else {
                    var html = '<div class="text-center text-danger">Hóa đơn này không có thật. Quy khách vui lòng kiểm tra lại hóa đơn.</div>';
                    $('.screent-payment-live').html(html);
                }
            },
            error: function(e) {
                console.log(e);
                var html = '<div class="text-center text-danger">Truy vấn hóa đơn không thành công.</div>';
                $('.screent-payment-live').html(html);
            }
        });
    });

});
