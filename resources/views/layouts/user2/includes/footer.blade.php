<footer class="main-footer" style="margin-left: 0">

  <div class="float-right d-none d-sm-block" style="margin-right: 50px;">
      <a href="https://facebook.com/cloudzone.vn/" target="_blank"><img src="https://portal.cloudzone.vn/images/icon-facebook.png" alt=""></a>
      <a href="tel:02364455789"><img src="https://portal.cloudzone.vn/images/call.png" alt=""></a>
      <a href="mailto:support@cloudzone.vn"><img src="https://portal.cloudzone.vn/images/mail.png" alt=""></a>
  </div>
  <strong>Copyright © 2020 @php $year = 2020;$current_year = date('Y'); if ( $current_year > $year) { echo " - " . $current_year;} @endphp
  <a href="https://cloudzone.vn" target="_blank">Cloudzone</a>.</strong> All rights reserved.

  <!-- <button class="chatbox-open" data-status="false"><i class="fa fa-comment fa-2x" aria-hidden="true"></i></button>
  <button class="chatbox-close"><i class="fas fa-times fa-2x" aria-hidden="true"></i></button> -->

  @php
    $user = Auth::user();
  @endphp
  <div class="chatbox">
    <section class="chatbox-popup">
      <header class="chatbox-popup__header">
        <aside style="flex:3">
          @if( !empty($user->user_meta->avatar) )
            <img class="fa-3x chatbox-popup__avatar" style="width: 58px;height: 58px; object-fit: cover;" src="{{ $user->user_meta->avatar }}">
          @else
            <i class="fa fa-user-circle fa-3x chatbox-popup__avatar" aria-hidden="true"></i>
          @endif
        </aside>
        <aside style="flex:8">
          <h1 class="chatbox-title">{{ $user->name }}</h1>
        </aside>
        <aside style="flex:1">
          <button class="chatbox-maximize"><i class="fa fa-window-maximize" aria-hidden="true"></i></button>
        </aside>
      </header>
      <main class="chatbox-popup__main chatbox-main">
        Xin chào, chào mừng bạn đến với Cloudzone!
        <br> Hãy tiếp tục và gửi cho chúng tôi tin nhắn.
      </main>
      <form class="chat__form">
        <footer class="chatbox-popup__footer">
          <aside style="flex:1;color:#888;text-align:center;">
            <i class="fa fa-camera" aria-hidden="true"></i>
          </aside>
          <aside style="flex:10">
            <input type="text" class="chatbox-textarea" placeholder="Nhập tin nhắn tại đây..." autofocus>
          </aside>
          <aside style="flex:1;color:#888;text-align:center;">
            <button type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
          </aside>
        </footer>
      </form>
    </section>

    <section class="chatbox-panel">
      <header class="chatbox-panel__header">
        <aside style="flex:3">
          @if( !empty($user->user_meta->avatar) )
            <img class="fa-3x chatbox-popup__avatar" style="width: 58px;height: 58px; object-fit: cover;" src="{{ $user->user_meta->avatar }}">
          @else
            <i class="fa fa-user-circle fa-3x chatbox-popup__avatar" aria-hidden="true"></i>
          @endif
        </aside>
        <aside style="flex:6">
          <h1 class="chatbox-title">{{ $user->name }}</h1>
        </aside>
        <aside style="flex:3;text-align:right;">
          <button class="chatbox-minimize"><i class="fa fa-window-restore" aria-hidden="true"></i></button>
          <button class="chatbox-panel-close"><i class="fa fa-close" aria-hidden="true"></i></button>
        </aside>
      </header>
      <main class="chatbox-panel__main chatbox-main" style="flex:1">

      </main>
      <form class="chat__form">
        <footer class="chatbox-panel__footer">
          <aside style="flex:1;color:#888;text-align:center;">
            <i class="fa fa-camera" aria-hidden="true"></i>
          </aside>
          <aside style="flex:10">
            <input type="text" class="chatbox-textarea" placeholder="Nhập tin nhắn tại đây..." autofocus>
          </aside>
          <aside style="flex:1;color:#888;text-align:center;">
            <button type="submit"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
          </aside>
        </footer>
      </form>
    </section>
  </div>

  <script type="text/javascript">
    $(() => {

        const userName = "{{ $user->name }}";
        const userId = {{ $user->id }};
        const msgerChat = $(".chatbox-main");
        const token = $('meta[name="csrf-token"]').attr('content');
        let scroll = 0;
        let sm = 0;

        $('.chat__form').on('submit', function(event) {
          event.preventDefault();
          /* Act on the event */
          const msgText = $(".chatbox-textarea").val();
          // console.log(msgText);
          if (!msgText) return;
          appendMessage("right", msgText, sm);
          $(".chatbox-textarea").val('');

          $.ajax({
            url: '/messages/sendMessage',
            type: 'POST',
            dataType: 'json',
            data: {'_token': token,msgText: msgText},
            success: function (data) {
              // console.log(data);
              if (!data.error) {
                $('.status-msg-'+ sm +' i').removeClass('fa-circle');
                $('.status-msg-'+ sm +' i').addClass('fa-check-circle');
              }
              else if ( data.error == 1 ) {
                $('.status-msg-'+ sm +' i').removeClass('fa-circle');
                $('.status-msg-'+ sm +' i').addClass('fa-times-circle');
                $('.status-msg-'+ sm).addClass('text-danger');
              }
            },
            error: function (e) {
              console.log(e);
              $('.status-msg-'+ sm +' i').removeClass('fa-circle');
              $('.status-msg-'+ sm +' i').addClass('fa-times-circle');
              $('.status-msg-'+ sm).addClass('text-danger');
            }
          })
        });

        function appendMessage(side, text, sm) {
          //   Simple solution for small apps
          const msgHTML = `
            <div class="msg ${side}-msg">
              <div class="ml-1 status-msg-${sm}">
                <i class="far fa-circle"></i>
              </div>
              <div class="msg-bubble">
                <div class="msg-info">
                  <div class="msg-info-name">${userName}</div>
                  <div class="msg-info-time">${formatDate(new Date())}</div>
                </div>

                <div class="msg-text text-left">${text}</div>
              </div>
            </div>`;
          scroll += 150;
          msgerChat.append(msgHTML);
          msgerChat.scrollTop( scroll );
        }

        function formatDate(date) {
          const h = "0" + date.getHours();
          const m = "0" + date.getMinutes();

          return `${h.slice(-2)}:${m.slice(-2)}`;
        }

        loadChat();

        function loadChat() {
          var status = $(this).attr('data-status');
          if (!status) {
            $.ajax({
              type: 'get',
              url: '/messages/loadChat',
              dataType: 'json',
              beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('.chatbox-main').html(html);
              },
              success: function(data) {
                  $('.chatbox-open').attr('data-status', true);
                  screent_chat(data);
              },
              error: function(e) {
                console.log(e);
                var html = '';
                // html += '<div class="text-center text-danger">Truy vấn quản lý khách hàng không thành công</div>';
                // $('.home-service-customer').html(html);
              },
            });
          }
        }

        function screent_chat(data) {
          var html = '';
          if ( data.error == 2) {
            html += '<div class="msg text-left">';
            html += '<div class="msg-bubble">';
            html += '<div class="msg-info">';
            html += '<div class="msg-info-name">Admin</div>';
            html += '<div class="msg-info-time">00:00</div>';
            html += '</div>';
            html += '<div class="msg-text">';
            html += 'Xin chào, chào mừng bạn đến với Cloudzone! Hãy tiếp tục và tin nhắn gửi cho chúng tôi .';
            html += '</div>';
            html += '</div>';
            html += '</div>';
          }
          else if ( data.error == 0 ) {
            // console.log(data);
            let content = data.content.data.reverse();
            let qttPending = 0;
            $.each(content, function(index, val) {
              if ( val.user_id == userId ) {
                html += `
                  <div class="msg right-msg">
                    <div class="msg-bubble">
                      <div class="msg-info">
                        <div class="msg-info-name">${userName}</div>
                        <div class="msg-info-time">${val.dateSend}</div>
                      </div>

                      <div class="msg-text text-left">${val.content}</div>
                    </div>
                  </div>`;
              } else {
                html += '<div class="msg text-left">';
                html += '<div class="msg-bubble">';
                html += '<div class="msg-info">';
                html += '<div class="msg-info-name">Admin</div>';
                html += '<div class="msg-info-time">' + val.dateSend + '</div>';
                html += '</div>';
                html += '<div class="msg-text">';
                html += val.content;
                html += '</div>';
                html += '</div>';
                html += '</div>';
              }
              scroll += 250;
            });
          }
          msgerChat.html(html);
        }

        $(".chatbox-open").click(() => {
          $(".chatbox-popup, .chatbox-close").fadeIn();
          msgerChat.scrollTop( scroll );
        });

        $(".chatbox-close").click(() =>
          $(".chatbox-popup, .chatbox-close").fadeOut()
        );

        $(".chatbox-maximize").click(() => {
          $(".chatbox-popup, .chatbox-open, .chatbox-close").fadeOut();
          $(".chatbox-panel").fadeIn();
          $(".chatbox-panel").css({ display: "flex" });
          msgerChat.scrollTop( scroll );
        });

        $(".chatbox-minimize").click(() => {
          $(".chatbox-panel").fadeOut();
          $(".chatbox-popup, .chatbox-open, .chatbox-close").fadeIn();
        });

        $(".chatbox-panel-close").click(() => {
          $(".chatbox-panel").fadeOut();
          $(".chatbox-open").fadeIn();
        });
      });
  </script>
</footer>
