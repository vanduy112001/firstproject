<div class="giuseart-nav ui-widget-content" id="draggable-contact">
    <div class="drag-icon text-center">
        <i class="fa fa-arrows-alt"></i>
    </div>
    <ul>
        <li><a href="https://m.me/cloudzone.vn" rel="nofollow" target="_blank"><i
                    class="ticon-messenger"></i>Messenger</a></li>
        <li><a href="http://zalo.me/0905091805" rel="nofollow" target="_blank"><i class="ticon-zalo-circle2"></i>Chat
                Zalo</a></li>
        <li class="phone-mobile">
            <a href="tel:0888880043" rel="nofollow" class="button">
                <span class="phone_animation animation-shadow">
                    <i class="icon-phone-w" aria-hidden="true"></i>
                </span>
                <span class="btn_phone_txt">Call</span>
            </a>
        </li>
        <li><a href="sms:0888880043" class="chat_animation">
                <i class="ticon-chat-sms" aria-hidden="true" title="Nhắn tin sms"></i> Texting SMS</a>
        </li>
        <!-- <li><a href="" rel="nofollow" target="_blank"><i class="ticon-heart"></i>Scan code</a></li> -->
        {{-- <li class="to-top-pc">
        <a href="#top" rel="nofollow" id="top-link" style="">
          <i class="fa fa-angle-up"></i> 
        </a>
        <span class="len-tren">To top</span>
      </li> --}}
    </ul>
</div>

<script src="{{ asset('libraries/user2/jquery.ui.touch-punch.min.js')}}"></script>
<script>
    jQuery(document).ready(function($){
        $('#draggable-contact').draggable();
      });
</script>