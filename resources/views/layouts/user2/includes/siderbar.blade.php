@php
    $currentRouteName = \Illuminate\Support\Facades\Route::currentRouteName();
    $group_products_vps = GroupProduct::get_group_product_vps_private(Auth::user()->id);
    $group_products_vps_us = GroupProduct::get_group_product_vps_us_private(Auth::user()->id);
    $list_group_products_vps_us = GroupProduct::list_group_products_vps_us(Auth::user()->id);
    $group_product_server = GroupProduct::get_group_product_server(Auth::user()->id);
    $group_product_proxy = GroupProduct::get_group_product_proxy(Auth::user()->id);
    // dd($group_product_server);
@endphp
<nav class="pcoded-navbar navbar-collapsed">
  <div class="nav-list">
      <div class="pcoded-inner-navbar main-menu">
        <!-- Đăng ký dịch vụ -->
        <div class="pcoded-navigation-label">
            <i class="fa fa-shopping-basket" aria-hidden="true"></i> Đăng ký dịch vụ
            <span class="label-register-service"><i class="fas fa-th"></i></span>
            <span class="collapse-menu"><i class="fa fa-minus"></i></span>
        </div>
        <ul class="pcoded-item pcoded-left-item">
          @php
              $group_products = GroupProduct::get_group_product_private(Auth::user()->id);
          @endphp
          <li class="pcoded-hasmenu {{ $currentRouteName == 'user.product.vps' ? 'active pcoded-trigger' : '' }}">
            <a href="javascript:void(0)" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-cloud"></i></span>
              <span class="pcoded-mtext">Cloud VPS VN</span>
            </a>
            <ul class="pcoded-submenu">
              @foreach ($group_products_vps as $group_product_vps)
                @if($group_product_vps->type == 'vps')
                  @if (!empty($group_product_vps->products[0]->type_product))
                    @if ($group_product_vps->products[0]->type_product != 'Addon-VPS')
                      <li class="">
                        <a href="{{ route('user.product.vps', $group_product_vps->id ) }}" class="waves-effect waves-dark">
                          <span class="pcoded-mtext">{{ $group_product_vps->title }}</span>
                        </a>
                      </li>
                    @endif
                  @endif
                @endif
              @endforeach
            </ul>
          </li>
          <li class="pcoded-hasmenu {{ $currentRouteName == 'user.product.vps_us' ? 'active pcoded-trigger' : '' }}">
            <a href="javascript:void(0)" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-cloud"></i></span>
              <span class="pcoded-mtext">Cloud VPS NN</span>
            </a>
            <ul class="pcoded-submenu">
              @foreach ($list_group_products_vps_us as $group_product_vps_us)
                @if($group_product_vps_us->type == 'vps_us' || $group_product_vps_us->type == 'vps_uk'
                || $group_product_vps_us->type == 'vps_sing' || $group_product_vps_us->type == 'vps_hl'
                || $group_product_vps_us->type == 'vps_ca'
                )
                  <li class="">
                    <a href="{{ route('user.product.vps_us', $group_product_vps_us->id ) }}" class="waves-effect waves-dark">
                      <span class="pcoded-mtext">{{ $group_product_vps_us->title }}</span>
                    </a>
                  </li>
                @endif
              @endforeach
            </ul>
          </li>
          @if ( isset($group_product_proxy) )
            <li class="{{ $currentRouteName == 'user.product.proxy' ? 'active pcoded-trigger' : '' }}">
              <a href="{{ route('user.product.proxy', $group_product_proxy->id) }}" class="waves-effect waves-dark">
                  <span class="pcoded-micon"><i class="fas fa-cloud"></i></span>
                  <span class="pcoded-mtext">Cloud Proxy</span>
              </a>
            </li>
          @endif
          @if ( isset($group_product_server) )
            <li class="{{ $currentRouteName == 'user.product.server' ? 'active pcoded-trigger' : '' }}">
              <a href="{{ route('user.product.server', $group_product_server->id) }}" class="waves-effect waves-dark">
                  <span class="pcoded-micon"><i class="fas fa-server"></i></span>
                  <span class="pcoded-mtext">Dedicated Server</span>
              </a>
            </li>
          @endif
          <li class="pcoded-hasmenu  {{ $currentRouteName == 'user.product.hosting' ? 'active pcoded-trigger' : '' }}">
            <a href="javascript:void(0)" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-database"></i></span>
              <span class="pcoded-mtext">Cloud Hosting</span>
            </a>
            <ul class="pcoded-submenu">
              @php
                  $group_products_vps = GroupProduct::get_group_product_hosting_private(Auth::user()->id);
              @endphp
              @foreach ($group_products as $group_product)
                @if($group_product->type == 'hosting')
                  <li class="">
                    <a href="{{ route('user.product.hosting', $group_product->id ) }}" class="waves-effect waves-dark">
                      <span class="pcoded-mtext">{{ $group_product->title }}</span>
                    </a>
                  </li>
                @endif
              @endforeach
            </ul>
          </li>
          <li class="{{ $currentRouteName == 'user.domain.search' ? 'active pcoded-trigger' : '' }}">
            <a href="{{ route('user.domain.search') }}" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-globe"></i></span>
              <span class="pcoded-mtext">Đăng ký tên miền</span>
            </a>
          </li>
        </ul>
        <!-- ./ Đăng ký dịch vụ -->
        @php
            $qtt_services_on = GroupProduct::get_qtt_services_on(Auth::user()->id);
            $total = 0;
            if ( !empty($qtt_services_on['vps']) ) {
              $total += $qtt_services_on['vps'];
            }
            if ( !empty($qtt_services_on['hosting']) ) {
              $total += $qtt_services_on['hosting'];
            }
            if ( !empty($qtt_services_on['domain']) ) {
              $total += $qtt_services_on['domain'];
            }
        @endphp
        <!--  Quán lý dịch vụ -->
        <div class="pcoded-navigation-label"><i class="fas fa-tachometer-alt"></i> Dịch vụ của tôi <span class="collapse-menu"><i class="fa fa-minus"></i></span></div>
        <ul class="pcoded-item pcoded-left-item">
          <li class="pcoded-hasmenu {{ $currentRouteName == 'service.vps.on' || $currentRouteName == 'service.vps.all'
            || $currentRouteName == 'service.vps.nearly' || $currentRouteName == 'service.vps.vps_nearly_and_expire' ? 'pcoded-trigger active' : '' }}">
            <a href="javascript:void(0)" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-cloud"></i></span>
              <span class="pcoded-mtext">Cloud VPS VN (
                @php
                    if ( !empty($qtt_services_on['vps_on']) && !empty($qtt_services_on['nearly_vps']) ) {
                      echo $qtt_services_on['vps_on'] + $qtt_services_on['nearly_vps'];
                    }
                    elseif ( !empty($qtt_services_on['vps_on']) ) {
                      echo $qtt_services_on['vps_on'];
                    }
                    elseif ( !empty($qtt_services_on['nearly_vps']) ) {
                      echo $qtt_services_on['nearly_vps'];
                    }
                    else {
                      echo "0";
                    }

                @endphp
                )
              </span>
            </a>
            <ul class="pcoded-submenu">
              <li class="{{ $currentRouteName == 'service.vps.on' ? 'active' : '' }}">
                <a href="{{ route('service.vps.on') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Đang sử dụng ({{ !empty($qtt_services_on['vps_on']) ? $qtt_services_on['vps_on'] + $qtt_services_on['nearly_vps'] : 0 }})</span>
                </a>
              </li>
              <li class="ml-3 {{ $currentRouteName == 'service.vps.use' ? 'active' : '' }}">
                <a href="{{ route('service.vps.use') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext icon-lv3-menu">Còn hạn ({{ !empty($qtt_services_on['vps_on']) ? $qtt_services_on['vps_on'] : 0 }})</span>
                </a>
              </li>
              <li class="ml-3 {{ $currentRouteName == 'service.vps.nearly' ? 'active' : '' }}">
                <a href="{{ route('service.vps.nearly') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext icon-lv3-menu">Đã hết hạn ({{ !empty($qtt_services_on['nearly_vps']) ? $qtt_services_on['nearly_vps'] : 0 }})</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'service.vps.cancel' ? 'active' : '' }}">
                <a href="{{ route('service.vps.cancel') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Đã hủy/xóa/chuyển ({{ !empty($qtt_services_on['cancel_vps']) ? $qtt_services_on['cancel_vps'] : 0 }})</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'service.vps.all' ? 'active' : '' }}">
                <a href="{{ route('service.vps.all') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Tất cả dịch vụ ({{ !empty($qtt_services_on['total_vps']) ? $qtt_services_on['total_vps'] : 0 }})</span>
                </a>
              </li>
            </ul>
          </li>
          <li class="pcoded-hasmenu {{ $currentRouteName == 'service.vps_us.on' || $currentRouteName == 'service.vps_us.all'
            || $currentRouteName == 'service.vps_us.nearly' || $currentRouteName == 'service.vps_us.vps_us_nearly_and_expire'
            || $currentRouteName == 'service.vps_us.cancel' || $currentRouteName == 'service.vps_us.use'
            ? 'pcoded-trigger active' : '' }}">
            <a href="javascript:void(0)" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-cloud"></i></span>
              <span class="pcoded-mtext">Dịch vụ VPS NN (
                @php
                    if ( !empty($qtt_services_on['vps_us_on']) && !empty($qtt_services_on['nearly_vps_us']) ) {
                      echo $qtt_services_on['vps_us_on'] + $qtt_services_on['nearly_vps_us'];
                    }
                    elseif ( !empty($qtt_services_on['vps_us_on']) ) {
                      echo $qtt_services_on['vps_us_on'];
                    }
                    elseif ( !empty($qtt_services_on['nearly_vps_us']) ) {
                      echo $qtt_services_on['nearly_vps_us'];
                    }
                    else {
                      echo "0";
                    }

                @endphp
                )
              </span>
            </a>
            <ul class="pcoded-submenu">
              <li class="{{ $currentRouteName == 'service.vps_us.on' ? 'active' : '' }}">
                <a href="{{ route('service.vps_us.on') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Đang sử dụng ({{ !empty($qtt_services_on['vps_us_on']) ? $qtt_services_on['vps_us_on'] + $qtt_services_on['nearly_vps_us'] : 0 }})</span>
                </a>
              </li>
              <li class="ml-3 {{ $currentRouteName == 'service.vps_us.use' ? 'active' : '' }}">
                <a href="{{ route('service.vps_us.use') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext icon-lv3-menu">Còn hạn ({{ !empty($qtt_services_on['vps_us_on']) ? $qtt_services_on['vps_us_on'] : 0 }})</span>
                </a>
              </li>
              <li class="ml-3 {{ $currentRouteName == 'service.vps_us.nearly' ? 'active' : '' }}">
                <a href="{{ route('service.vps_us.nearly') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext icon-lv3-menu">Đã hết hạn ({{ !empty($qtt_services_on['nearly_vps_us']) ? $qtt_services_on['nearly_vps_us'] : 0 }})</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'service.vps_us.cancel' ? 'active' : '' }}">
                <a href="{{ route('service.vps_us.cancel') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Đã hủy/xóa/chuyển ({{ !empty($qtt_services_on['cancel_vps_us']) ? $qtt_services_on['cancel_vps_us'] : 0 }})</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'service.vps_us.all' ? 'active' : '' }}">
                <a href="{{ route('service.vps_us.all') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Tất cả dịch vụ ({{ !empty($qtt_services_on['total_vps_us']) ? $qtt_services_on['total_vps_us'] : 0 }})</span>
                </a>
              </li>
            </ul>
          </li>
          <li class="pcoded-hasmenu {{ $currentRouteName == 'service.proxy.use' || $currentRouteName == 'service.proxy.on'
            || $currentRouteName == 'service.proxy.expire' || $currentRouteName == 'service.proxy.cancel'
            || $currentRouteName == 'service.proxy.all'
            ? 'pcoded-trigger active' : '' }}">
            <a href="javascript:void(0)" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-cloud"></i></span>
              <span class="pcoded-mtext">Dịch vụ Proxy (
                @php
                    if ( !empty($qtt_services_on['proxy_on']) && !empty($qtt_services_on['proxy_expire']) ) {
                      echo $qtt_services_on['proxy_on'] + $qtt_services_on['proxy_expire'];
                    }
                    elseif ( !empty($qtt_services_on['proxy_on']) ) {
                      echo $qtt_services_on['proxy_on'];
                    }
                    elseif ( !empty($qtt_services_on['proxy_expire']) ) {
                      echo $qtt_services_on['proxy_expire'];
                    }
                    else {
                      echo "0";
                    }
                @endphp
                )
              </span>
            </a>
            <ul class="pcoded-submenu">
              <li class="{{ $currentRouteName == 'service.proxy.use' ? 'active' : '' }}">
                <a href="{{ route('service.proxy.use') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Đang sử dụng ({{ !empty($qtt_services_on['proxy_on']) ? $qtt_services_on['proxy_on'] + $qtt_services_on['proxy_expire'] : 0 }})</span>
                </a>
              </li>
              <li class="ml-3 {{ $currentRouteName == 'service.proxy.on' ? 'active' : '' }}">
                <a href="{{ route('service.proxy.on') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext icon-lv3-menu">Còn hạn ({{ !empty($qtt_services_on['proxy_on']) ? $qtt_services_on['proxy_on'] : 0 }})</span>
                </a>
              </li>
              <li class="ml-3 {{ $currentRouteName == 'service.proxy.expire' ? 'active' : '' }}">
                <a href="{{ route('service.proxy.expire') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext icon-lv3-menu">Đã hết hạn ({{ !empty($qtt_services_on['proxy_expire']) ? $qtt_services_on['proxy_expire'] : 0 }})</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'service.proxy.cancel' ? 'active' : '' }}">
                <a href="{{ route('service.proxy.cancel') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Đã hủy/xóa/chuyển ({{ !empty($qtt_services_on['proxy_cancel']) ? $qtt_services_on['proxy_cancel'] : 0 }})</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'service.proxy.all' ? 'active' : '' }}">
                <a href="{{ route('service.proxy.all') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Tất cả dịch vụ ({{ !empty($qtt_services_on['proxy_total']) ? $qtt_services_on['proxy_total'] : 0 }})</span>
                </a>
              </li>
            </ul>
          </li>
          <li class="pcoded-hasmenu {{ $currentRouteName == 'service.hosting.on' || $currentRouteName == 'service.hosting.nearly' 
            || $currentRouteName == 'service.hosting.all' || $currentRouteName == 'service.hosting.use'
            || $currentRouteName == 'service.hosting.cancel' ? 'pcoded-trigger active' : '' }}">
            <a href="javascript:void(0)" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-database"></i></span>
              <span class="pcoded-mtext">Cloud Hosting ({{ $qtt_services_on['hosting_on'] + $qtt_services_on['nearly_hosting'] }})</span>
            </a>
            <ul class="pcoded-submenu">
              <li class="{{ $currentRouteName == 'service.hosting.use' ? 'active' : '' }}">
                <a href="{{ route('service.hosting.use') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Đang sử dụng ({{ $qtt_services_on['hosting_on'] + $qtt_services_on['nearly_hosting'] }})</span>
                </a>
              </li>
              <li class="ml-3 {{ $currentRouteName == 'service.hosting.on' ? 'active' : '' }}">
                <a href="{{ route('service.hosting.on') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Còn hạn ({{ !empty($qtt_services_on['hosting_on']) ? $qtt_services_on['hosting_on'] : 0 }})</span>
                </a>
              </li>
              <li class="ml-3 {{ $currentRouteName == 'service.hosting.nearly' ? 'active' : '' }}">
                <a href="{{ route('service.hosting.nearly') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Đã hết hạn ({{ !empty($qtt_services_on['nearly_hosting']) ? $qtt_services_on['nearly_hosting'] : 0 }})</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'service.hosting.cancel' ? 'active' : '' }}">
                <a href="{{ route('service.hosting.cancel') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Hủy/xóa ({{ !empty($qtt_services_on['cancel_hosting']) ? $qtt_services_on['cancel_hosting'] : 0 }})</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'service.hosting.all' ? 'active' : '' }}">
                <a href="{{ route('service.hosting.all') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Tất cả dịch vụ ({{ !empty($qtt_services_on['total_hosting']) ? $qtt_services_on['total_hosting'] : 0 }})</span>
                </a>
              </li>
            </ul>
          </li>
          <li class="pcoded-hasmenu {{ $currentRouteName == 'service.server.on' || $currentRouteName == 'service.server.nearly'
          || $currentRouteName == 'service.server.cancel' || $currentRouteName == 'service.server.all'
          || $currentRouteName == 'service.server.use'
          ? 'pcoded-trigger active' : '' }}">
            <a href="javascript:void(0)" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-server"></i></span>
              <span class="pcoded-mtext">Server ({{ $qtt_services_on['server_on'] + $qtt_services_on['nearly_server'] }})</span>
            </a>
            <ul class="pcoded-submenu">
              <li class="{{ $currentRouteName == 'service.server.use' ? 'active' : '' }}">
                <a href="{{ route('service.server.use') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Đang sử dụng ({{ $qtt_services_on['nearly_server'] + $qtt_services_on['server_on'] }})</span>
                </a>
              </li>
              <li class="ml-3 {{ $currentRouteName == 'service.server.on' ? 'active' : '' }}">
                <a href="{{ route('service.server.on') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Còn hạn ({{ !empty($qtt_services_on['server_on']) ? $qtt_services_on['server_on'] : 0 }})</span>
                </a>
              </li>
              <li class="ml-3 {{ $currentRouteName == 'service.server.nearly' ? 'active' : '' }}">
                <a href="{{ route('service.server.nearly') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Đã hết hạn ({{ !empty($qtt_services_on['nearly_server']) ? $qtt_services_on['nearly_server'] : 0 }})</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'service.server.cancel' ? 'active' : '' }}">
                <a href="{{ route('service.server.cancel') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Đã xóa/hủy/chuyển ({{ !empty($qtt_services_on['server_cancel']) ? $qtt_services_on['server_cancel'] : 0 }})</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'service.server.all' ? 'active' : '' }}">
                <a href="{{ route('service.server.all') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Tất cả dịch vụ ({{ !empty($qtt_services_on['total_server']) ? $qtt_services_on['total_server'] : 0 }})</span>
                </a>
              </li>
            </ul>
          </li>
          <li class="pcoded-hasmenu {{ $currentRouteName == 'service.colocation.use' || $currentRouteName == 'service.colocation.on'
          || $currentRouteName == 'service.colocation_detail' ||  $currentRouteName == 'service.colocation.nearly'
          || $currentRouteName == 'service.colocation.all' || $currentRouteName == 'service.colocation.on'
          ? 'pcoded-trigger active' : '' }}">
            <a href="javascript:void(0)" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-globe-asia"></i></span>
              <span class="pcoded-mtext">Colocation ({{ !empty($qtt_services_on['colocation_use']) ? $qtt_services_on['colocation_use'] : 0 }})</span>
            </a>
            <ul class="pcoded-submenu">
              <li class="{{ $currentRouteName == 'service.colocation.use' ? 'active' : '' }}">
                <a href="{{ route('service.colocation.use') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Đang sử dụng ({{ !empty($qtt_services_on['colocation_use']) ? $qtt_services_on['colocation_use'] : 0 }})</span>
                </a>
              </li>
              <li class="ml-3 {{ $currentRouteName == 'service.colocation.on' ? 'active' : '' }}">
                <a href="{{ route('service.colocation.on') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Còn hạn ({{ !empty($qtt_services_on['colocation_on']) ? $qtt_services_on['colocation_on'] : 0 }})</span>
                </a>
              </li>
              <li class="ml-3 {{ $currentRouteName == 'service.colocation.nearly' ? 'active' : '' }}">
                <a href="{{ route('service.colocation.nearly') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Đã hết hạn ({{ !empty($qtt_services_on['nearly_colocation']) ? $qtt_services_on['nearly_colocation'] : 0 }})</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'service.colocation.cancel' ? 'active' : '' }}">
                <a href="{{ route('service.colocation.cancel') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Hết hạn / Hủy ({{ !empty($qtt_services_on['cancel_colocation']) ? $qtt_services_on['cancel_colocation'] : 0 }})</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'service.colocation.all' ? 'active' : '' }}">
                <a href="{{ route('service.colocation.all') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Tất cả dịch vụ ({{ !empty($qtt_services_on['total_colocation']) ? $qtt_services_on['total_colocation'] : 0 }})</span>
                </a>
              </li>
            </ul>
          </li>
          <li class="pcoded-hasmenu {{ $currentRouteName == 'service.email_hosting.on' || $currentRouteName == 'service.email_hosting.nearly' || $currentRouteName == 'service.email_hosting.all' ? 'pcoded-trigger active' : '' }}">
            <a href="javascript:void(0)" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-envelope-open-text"></i></span>
              <span class="pcoded-mtext">Email Hosting ({{ !empty($qtt_services_on['total_email_hosting']) ? $qtt_services_on['total_email_hosting'] : 0 }})</span>
            </a>
            <ul class="pcoded-submenu">
              <li class="{{ $currentRouteName == 'service.email_hosting.on' ? 'active' : '' }}">
                <a href="{{ route('service.email_hosting.on') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Đang sử dụng ({{ !empty($qtt_services_on['email_hosting_on']) ? $qtt_services_on['email_hosting_on'] : 0 }})</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'service.email_hosting.nearly' ? 'active' : '' }}">
                <a href="{{ route('service.email_hosting.nearly') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Hết hạn / Hủy ({{ !empty($qtt_services_on['nearly_email_hosting']) ? $qtt_services_on['nearly_email_hosting'] : 0 }})</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'service.email_hosting.all' ? 'active' : '' }}">
                    <a href="{{ route('service.email_hosting.all') }}" class="waves-effect waves-dark">
                      <span class="pcoded-mtext">Tất cả dịch vụ ({{ !empty($qtt_services_on['total_email_hosting']) ? $qtt_services_on['total_email_hosting'] : 0 }})</span>
                    </a>
              </li>
            </ul>
          </li>
          <li class="{{ $currentRouteName == 'user.domain.index' ? 'active' : '' }}">
            <a href="{{ route('user.domain.index') }}" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-globe"></i></span>
              <span class="pcoded-mtext">Tên miền ({{ !empty($qtt_services_on['domain']) ? $qtt_services_on['domain'] : 0 }})</span>
            </a>
          </li>
        </ul>
        <!-- ./ Quán lý dịch vụ -->
        <!-- thanh toán -->
        <div class="pcoded-navigation-label"><i class="fa fa-credit-card" aria-hidden="true"></i> Quản lý thanh toán <span class="collapse-menu"><i class="fa fa-minus"></i></span></div>
        <ul class="pcoded-item pcoded-left-item">
          <li class="{{ $currentRouteName == 'user.order.list' ? 'active' : '' }}">
            <a href="{{ route('user.order.list') }}" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-shopping-cart"></i></span>
              <span class="pcoded-mtext">Đơn đặt hàng</span>
            </a>
          </li>
          <li class="pcoded-hasmenu {{ ( $currentRouteName == 'order.check_invoices'
            || $currentRouteName == 'order.list_invoices' || $currentRouteName == 'order.list_invoices.paid'
            || $currentRouteName == 'invoice.checkout_payment_off' || $currentRouteName == 'invoice.checkout_payment_expired_off'
            || $currentRouteName == 'invoice.checkout_payment_addon_off'
            || $currentRouteName == 'order.list_invoices.unpaid' ) ? 'active pcoded-trigger' : '' }}">
            <a href="javascript:void(0)" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-layer-group"></i></span>
              <span class="pcoded-mtext">Hóa đơn</span>
            </a>
            <ul class="pcoded-submenu">
              <li class="{{ $currentRouteName == 'order.list_invoices.paid' ? 'active' : '' }}">
                <a href="{{ route('order.list_invoices.paid') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Đã thanh toán</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'order.list_invoices.unpaid' ? 'active' : '' }}">
                <a href="{{ route('order.list_invoices.unpaid') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Chưa thanh toán</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'order.list_invoices' ? 'active' : '' }}">
                <a href="{{ route('order.list_invoices') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Tất cả thanh toán</span>
                </a>
              </li>
            </ul>
          </li>
          {{-- <li class="{{ ($currentRouteName == 'user.contract.index' || $currentRouteName == 'user.contract.create' || $currentRouteName == 'user.contract.detail') ? 'active' : '' }}">
            <a href="{{ route('user.contract.index') }}" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-paste"></i></span>
              <span class="pcoded-mtext">Hợp đồng</span>
            </a>
          </li> --}}
        </ul>
        <!-- ./ Thanh toán -->
        <!-- Tài khoản đại lý -->
        <!-- <div class="pcoded-navigation-label">Tài khoản đại lý</div>
        <ul class="pcoded-item pcoded-left-item">
          <li class="pcoded-hasmenu {{ ( $currentRouteName == 'user.customer.index' || $currentRouteName == 'user.customer.create'
                  || $currentRouteName == 'user.customer.detail' || $currentRouteName == 'user.customer.edit' || $currentRouteName == 'user.customer.list_order_with_customer' ) ? 'active pcoded-trigger' : '' }}">
            <a href="javascript:void(0)" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-users"></i></span>
              <span class="pcoded-mtext">Quản lý khách hàng</span>
            </a>
            <ul class="pcoded-submenu">
              <li class="{{ $currentRouteName == 'user.customer.index' ? 'active' : '' }}">
                <a href="{{ route('user.customer.index') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Hồ sơ khách hàng</span>
                </a>
              </li>
              <li class="{{ $currentRouteName == 'user.customer.create' ? 'active' : '' }}">
                <a href="{{ route('user.customer.create') }}" class="waves-effect waves-dark">
                  <span class="pcoded-mtext">Tạo khách hàng mới</span>
                </a>
              </li>
            </ul>
          </li>
        </ul> -->
        <!-- ./ Tài khoản đại lý -->
        <!-- activity log -->
        <div class="pcoded-navigation-label"><i class="fa fa-history" aria-hidden="true"></i> Nhật ký hoạt động <span class="collapse-menu"><i class="fa fa-minus"></i></span></div>
        <ul class="pcoded-item pcoded-left-item">
          <li class="{{ ( $currentRouteName == 'user.activity_log' ) ? 'active' : '' }}">
            <a href="{{ route('user.activity_log') }}" class="waves-effect waves-dark">
              <span class="pcoded-micon"><i class="fas fa-chart-line"></i></span>
              <span class="pcoded-mtext">Danh sách hoạt động</span>
            </a>
          </li>
        </ul>
        <!-- ./activity log -->
    </div>

    <!-- Show menu fix order -->
    @php
        $group_products_vps = GroupProduct::get_group_product_vps_private(Auth::user()->id);
        $group_products_vps_us = GroupProduct::get_group_product_vps_us_private(Auth::user()->id);
        $list_group_products_vps_us = GroupProduct::list_group_products_vps_us(Auth::user()->id);
    @endphp
    <div class="row shadow row-register-service" style="display: none">
        <div class="bg-danger close-row-register-service">x</div>
        <div class="col col-12">
            <div class="block-group-product">
                <div class="title-service">Cloud VPS Việt Nam</div>
                <ul>
                    @foreach ($group_products_vps as $group_product_vps)
                        @if($group_product_vps->type == 'vps')
                          @if (!empty($group_product_vps->products[0]->type_product))
                            @if ($group_product_vps->products[0]->type_product != 'Addon-VPS')
                              <li>
                                  <div class="icon-product">
                                      <i class="fas fa-server"></i>
                                  </div>
                                  <div class="link-product">
                                      <a href="{{ route('user.product.vps', $group_product_vps->id ) }}">{{ $group_product_vps->title }}</span></a>
                                  </div>
                              </li>
                            @endif
                          @endif
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col col-12">
            <div class="title-service">Cloud VPS nước ngoài</div>
            <ul>
                @foreach ($list_group_products_vps_us as $group_product_vps_us)
                    @if($group_product_vps_us->type == 'vps_us' || $group_product_vps_us->type == 'vps_uk'
                      || $group_product_vps_us->type == 'vps_sing' || $group_product_vps_us->type == 'vps_hl'
                      || $group_product_vps_us->type == 'vps_ca'
                    )
                        <li>
                            <div class="icon-product">
                                <i class="fas fa-server"></i>
                            </div>
                            <div class="link-product">
                                <a href="{{ route('user.product.vps_us', $group_product_vps_us->id ) }}">{{ $group_product_vps_us->title }}</a>
                            </div>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
        <div class="col col-12">
            <div class="title-service">Cloud Hosting</div>
            <ul>
                @foreach ($group_products as $group_product)
                    @if($group_product->type == 'hosting')
                        <li>
                            <div class="icon-product">
                                <i class="fa fa-globe"></i>
                            </div>
                            <div class="link-product">
                                <a href="{{ route('user.product.hosting', $group_product->id ) }}">{{ $group_product->title }}</a>
                            </div>
                        </li>
                    @endif
                @endforeach
            </ul>
        </div>
        <div class="col col-12">
            <div class="title-service">Tên miền</div>
            <ul>
                <li>
                    <div class="icon-product">
                        <i class="fa fa-globe"></i>
                    </div>
                    <div class="link-product">
                        <a href="{{ route('user.domain.search') }}">Đăng ký tên miền</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>

  </div>
</nav>

<script type="text/javascript">
  jQuery(document).ready(function($) {

    $('li.pcoded-hasmenu').each(function(n) {
      $(this).attr('id', "link-" + n);
    });

    $('.pcoded-navigation-label').each(function(n) {
      $(this).attr('id', "collslapse-menu-" + n);
    });

    $('.pcoded-hasmenu').on('click', function(){
        $(this).children('ul.pcoded-submenu').toggleClass('show');
        var arr = [];
        var show = $('ul.pcoded-submenu.show');
        if (show.length > 0) {
            $(show).each(function (index) {
            arr.push($(this).parent().attr('id'));
            });
        }
        localStorage.setItem('chooseId', JSON.stringify(arr));
    });

    if (localStorage['chooseId']) {
      var activeIndex = JSON.parse(localStorage['chooseId']);
      $(activeIndex).each(function (key, index) {
        $('#'+index).children('ul.pcoded-submenu').addClass('show');
      });
    }

    $('.collapse-menu').on('click', function(){
        $(this).find('i').toggleClass('fa-plus');
        $(this).closest('.pcoded-navigation-label').next().toggleClass('hidden');
        var arr_collslapse_menu = [];
        var show_collslapse_menu = $('.pcoded-item.pcoded-left-item.hidden');
        if (show_collslapse_menu.length > 0) {
            $(show_collslapse_menu).each(function (index) {
                arr_collslapse_menu.push($(this).prev().attr('id'));
            });
        }
        localStorage.setItem('collslapseId', JSON.stringify(arr_collslapse_menu));
    });

    if (localStorage['collslapseId']) {
        var activeIndex = JSON.parse(localStorage['collslapseId']);
        $(activeIndex).each(function (key, index) {
            $('#'+index).next('.pcoded-item.pcoded-left-item').addClass('hidden');
            $('#'+index).find('.collapse-menu i').toggleClass('fa-plus');
        });
    }

    $('.label-register-service').on('click', function(){
        $('.row-register-service').toggle();
    });

    $('.close-row-register-service').on('click', function(){
        $('.row-register-service').hide();
    });

  });

</script>
<style type="text/css">
  ul.pcoded-submenu {
    display: none !important;
  }
  ul.pcoded-submenu.show {
    display: block !important;
  }
</style>
