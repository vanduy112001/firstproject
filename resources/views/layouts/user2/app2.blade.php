<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="author" content="">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="icon" href="{{ asset('images/favicon.ico') }}">
  <title>{{ 'Trang quản lý dịch vụ - Cloudzone Portal' }}</title>
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- css -->
  <link href="{{ asset('css/user2/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="{{ asset('css/user2/waves.min.css') }}" type="text/css" media="all">
  <link href="{{ asset('libraries/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/user2/font-awesome-n.min.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/user2/feather.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/user2/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/user2/widget.css') }}">
  <!-- Scripts -->
  <link rel="stylesheet" href="{{ asset('css/adminlte.min.css')}}">
  {{---------------------------------- Template AdminLTE --------------------------}}
  <link href="{{ asset('css/usercss.css') }}" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('/libraries/toastr/toastr.min.css')}}">
  @yield('style')
  <!-- Google Tag Manager -->
  <script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NHDFXLJ');
  </script>
<!-- End Google Tag Manager -->

</head>
<body  id="page-top">
  <div class="loader-bg">
    <div class="loader-bar"></div>
  </div>
  <div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <!-- header -->
    @include('layouts/user2/includes/header')
    <!-- content -->
    <div class="pcoded-container navbar-wrapper">
      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
            <!-- siderbar -->
            <!-- content -->
            <div class="pcoded-content">
                <!-- breadcrumb -->
                <div class="page-header card">
                  <div class="row align-items-end">
                    <div class="col-lg-8">
                      <div class="page-header-title">
                        <i class="feather icon-home bg-c-blue"></i>
                        <div class="d-inline">
                          @yield('title')
                        </div>
                      </div>
                    </div>
                    <div class="col-lg-4">
                      <div class="page-header-breadcrumb">
                        <ul class=" breadcrumb breadcrumb-title">
                          @yield('breadcrumb')
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- content -->
                <div class="pcoded-inner-content">
                  <div class="main-body">
                    <div class="page-wrapper">
                      <div class="page-body">
                        <div class="row">
                          @yield('content')
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- End of Main Content -->
                <!-- Footer -->
                @include('layouts/user/includes/footer')
                <!-- End of Footer -->
                <!-- Scroll to Top Button-->
                <a class="scroll-to-top rounded" href="#page-top">
                    <i class="fas fa-angle-up"></i>
                </a>
                <!-- Nút liên lạc hỗ trợ-->
                <div class="float-contact">
                  <button class="chat-face">
                    <a href="https://m.me/cloudzone.vn" target="_blank"><i class="fab fa-facebook-messenger"></i> Chat Facebook</a>
                  </button>
                  <button class="hotline">
                    <a href="tel:02364455789"><i class="fa fa-phone" aria-hidden="true"></i> 0236-4455-789</a>
                  </button>
                </div>
            </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <!-- <script src="{{ asset('libraries/jquery/jquery.min.js') }}"></script> -->
    <script data-cfasync="false"  src="{{ asset('libraries/user2/email-decode.min.js')}}"></script>
    <script src="{{ asset('libraries/user2/jquery.min.js')}}"></script>
    <script src="{{ asset('libraries/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- <script src="{{ asset('libraries/js/adminlte.js')}}"></script> -->


    <script src="{{ asset('libraries/user2/jquery-ui.min.js')}}"></script>
    <script src="{{ asset('libraries/user2/popper.min.js')}}"></script>
    <script src="{{ asset('libraries/user2/bootstrap.min.js')}}"></script>
    <script src="{{ asset('libraries/user2/waves.min.js')}}"></script>

    <script src="{{ url('libraries/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>

    <script src="{{ asset('libraries/user2/jquery.flot.js')}}"></script>
    <script src="{{ asset('libraries/user2/jquery.flot.categories.js')}}"></script>
    <script src="{{ asset('libraries/user2/curvedlines.js')}}"></script>
    <script src="{{ asset('libraries/user2/jquery.flot.tooltip.min.js')}}"></script>

    <script src="{{ asset('libraries/user2/chartist.js')}}"></script>

    <script src="{{ asset('libraries/user2/amcharts.js')}}"></script>
    <script src="{{ asset('libraries/user2/serial.js')}}"></script>
    <script src="{{ asset('libraries/user2/light.js')}}"></script>

    <script src="{{ asset('libraries/user2/pcoded.min.js')}}"></script>
    <script src="{{ asset('libraries/user2/vertical-layout.min.js')}}"></script>
    <script src="{{ asset('libraries/user2/custom-dashboard.min.js')}}"></script>
    <script src="{{ asset('libraries/user2/script.min.js')}}"></script>

    <script src="{{ asset('libraries/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/user.min.js') }}"></script>
    <script src="{{ asset('js/all_page.js') }}"></script>
    <script src="{{ asset('/libraries/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/libraries/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('/libraries/toastr/toastr.min.js') }}"></script>
    <script src="{{ asset('/libraries/bootstrap/js/bootstrap-show-password.min.js') }}"></script>



    @yield('scripts')


    <script type="text/javascript">
      $(document).ready(function () {
          // khởi tạo modal Toast
          const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000
          });
          // Enable pusher logging - don't include this in production
          // Pusher.logToConsole = true;
          //
          // var pusher = new Pusher('bf06d29e8d774078d193', {
          //   cluster: 'ap1',
          //   forceTLS: true
          // });
          //
          // var channel = pusher.subscribe('new-vps');
          // channel.bind('vps-event', function(data) {
          //   var user_id = {{ Auth::user()->id }}
          //   if (user_id == data.user_id) {
          //     var vps = data.vps_ip;
          //     $(document).Toasts('create', {
          //         class: 'bg-success',
          //         title: 'Action VPS',
          //         subtitle: 'tạo vps',
          //         body: "Tạo VPS " + vps +" thành công ",
          //     });
          //   }
          // });

          //Ajax cập nhập thời gian đọc thông báo
          $('.unread').click( function () {
            var id = $(this).attr('data-id');
            // console.log(id);
            $.ajax({
              type: "get",
              url: "{{ route('user.notification.read_notification') }}",
              dataType: 'json',
              data: {
                  "_token": "{{ csrf_token() }}",
                  "id": id
              },
            });
          });
      })
    </script>
  </div>
</body>
</html>
