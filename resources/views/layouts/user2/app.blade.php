<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <title>{{ 'Trang quản lý dịch vụ - Cloudzone Portal' }}</title>

  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=0, minimal-ui">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="author" content="cloudzone">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="icon" href="{{ asset('images/favicon.ico') }}">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Quicksand:500,700" rel="stylesheet">

  <!-- Scripts -->
  <link rel="stylesheet" href="{{ asset('css/template.css')}}">
  {{---------------------------------- Template AdminLTE --------------------------}}
  <link href="{{ asset('css/css.css') }}?token={{ date('YmdH') }}" rel="stylesheet">

  <link href="{{ asset('css/user2/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="{{ asset('css/user2/waves.min.css') }}" type="text/css" media="all">

  <link rel="stylesheet" type="text/css" href="{{ asset('css/user2/feather.css') }}">
  <link href="{{ asset('libraries/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/user2/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/user2/pages.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/user2/widget.css') }}">

  <link href="{{ asset('css/usercss.css') }}?token={{ date('YmdH') }}" rel="stylesheet">
  <link href="{{ asset('css/usercss2.css') }}?token={{ date('YmdH') }}" rel="stylesheet">

  <link rel="stylesheet" href="{{ asset('/libraries/toastr/toastr.min.css')}}">
  @yield('style')
  <!-- Google Tag Manager -->
  <script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NHDFXLJ');
  </script>
  <!-- End Google Tag Manager -->
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-SSN2BLFKBW"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-SSN2BLFKBW');
  </script>

</head>
<body>

  <div class="loader-bg">
    <div class="loader-bar"></div>
  </div>

  <div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

      <!-- Header -->
      @include('layouts/user2/includes/header')
      <!-- end header -->

      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <!-- menu -->
          @include('layouts/user2/includes/siderbar')
          <!-- end menu -->
        <div class="pcoded-content">

          <div class="page-header card">
            <div class="row align-items-end">
              <div class="col-lg-8">
                <div class="page-header-title">
                  <span class="pcoded-mtext"><h1 class="h3 mb-0 title-page">@yield('title')</h1></span>
                </div>
              </div>
              <div class="col-lg-4" >
                <div class="page-header-breadcrumb">
                  <ul class=" breadcrumb breadcrumb-title" style="height: 1.5em;overflow: hidden;">
                    @yield('breadcrumb')
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <div class="pcoded-inner-content">
            <div class="main-body">
              <div class="page-wrapper">
                <div class="page-body">
                  @yield('content')
                </div>
              </div>
            </div>
          </div>
          @include('layouts/user2/includes/footer')
        <div id="styleSelector">
        </div>

      </div>
    </div>
  </div>
</div>


  <script data-cfasync="false"  src="{{ asset('libraries/user2/email-decode.min.js')}}"></script>
  <script src="{{ asset('libraries/user2/jquery.min.js')}}"></script>

  <script src="{{ asset('libraries/user2/jquery-ui.min.js')}}"></script>
  <script src="{{ asset('libraries/user2/popper.min.js')}}"></script>
  <script src="{{ asset('libraries/user2/bootstrap.min.js')}}"></script>

  <script src="{{ asset('libraries/user2/waves.min.js')}}"></script>


  <script src="{{ asset('libraries/user2/jquery.slimscroll.js')}}"></script>

  <script src="{{ asset('libraries/user2/jquery.flot.js')}}"></script>
  <script src="{{ asset('libraries/user2/jquery.flot.categories.js')}}"></script>
  <script src="{{ asset('libraries/user2/curvedlines.js')}}"></script>
  <script src="{{ asset('libraries/user2/jquery.flot.tooltip.min.js')}}"></script>

  <script src="{{ asset('libraries/user2/chartist.js')}}"></script>

  <script src="{{ asset('libraries/user2/amcharts.js')}}"></script>
  <script src="{{ asset('libraries/user2/serial.js')}}"></script>
  <script src="{{ asset('libraries/user2/light.js')}}"></script>

  <script src="{{ asset('libraries/user2/pcoded.min.js')}}"></script>
  <script src="{{ asset('libraries/user2/vertical-layout.min.js')}}"></script>
  <script src="{{ asset('libraries/user2/script.min.js')}}"></script>

  <script src="{{ asset('libraries/user2/rocket-loader.min.js')}}" defer=""></script>
  <!-- Core plugin JavaScript-->
  <script src="{{ asset('libraries/js/template.js')}}"></script>
  <script src="{{ asset('libraries/jquery-easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('js/user.min.js') }}"></script>
  <script src="{{ asset('js/all_page.js') }}"></script>
  <script src="{{ asset('/libraries/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('/libraries/sweetalert2/sweetalert2.min.js') }}"></script>
  <script src="{{ asset('/libraries/toastr/toastr.min.js') }}"></script>
  <script src="{{ asset('/libraries/bootstrap/js/bootstrap-show-password.min.js') }}"></script>


  @yield('scripts')
  <script type="text/javascript">
    $(document).ready(function () {
        // khởi tạo modal Toast
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
        });
        //Ajax cập nhập thời gian đọc thông báo
        $('.unread').click( function () {
          var id = $(this).attr('data-id');
          // console.log(id);
          $.ajax({
            type: "get",
            url: "{{ route('user.notification.read_notification') }}",
            dataType: 'json',
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
            },
          });
        });

        // xem số lượng VPS đang tạo
        loadVpsPros();
        function loadVpsPros() {
          setInterval(function(){
            var vpsPros = $('#vpsPros').attr('data-pros');
            // console.log(vpsPros);
            if ( vpsPros > 0 ) {
              $.ajax({
                url: '/loadVpsPros',
                success: function (data) {
                  // console.log(data);
                  if ( vpsPros >  data ) {
                    $('#vpsPros').text( data );
                    $('#vpsPros').attr('data-pros', data);
                    if ( data == 0 ) {
                      $('#vpsPros').removeClass('qttVpsPros');
                    }
                    $(document).Toasts('create', {
                      class: 'bg-success',
                      title: 'Cloudzone',
                      subtitle: 'VPS',
                      body: "Tạo thành công " + ( vpsPros - data) + " VPS",
                    })
                  }
                },
                error: function (e) {
                  // body...
                  console.log(e);
                },
              })
            }
          }, 5000);

        }
    })
  </script>
</body>

<!-- Mirrored from colorlib.com/polygon/admindek/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 12 Dec 2019 16:08:25 GMT -->
</html>
@include('layouts.user2.includes.contact-fixed')
{{-- Hiển thị chân trang theo độ cao tối thiểu của trình duyệt --}}
<script>
    jQuery(document).ready(function($) {
        $(function() {
            var timer;
            $(window).resize(function() {
                clearTimeout(timer);
                var x = $('.pcoded-inner-content').offset();
                var y = $('.main-footer').height();
                var z = $(window).height();
                var res = z - x.top - y - 35;
                timer = setTimeout(function() {
                    $('.pcoded-inner-content').css("min-height", res + "px" );
                }, 40);
            }).resize();
        });
    });
</script>
