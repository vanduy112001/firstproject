<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <title>{{ 'Trang quản lý dịch vụ - Cloudzone Portal' }}</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=0, minimal-ui">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="description" content="">
  <meta name="author" content="colorlib">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="icon" href="{{ asset('images/favicon.ico') }}">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Quicksand:500,700" rel="stylesheet">

  <!-- Scripts -->
  <link rel="stylesheet" href="{{ asset('css/template.css')}}">
  {{---------------------------------- Template AdminLTE --------------------------}}
  <link href="{{ asset('css/css.css') }}" rel="stylesheet">

  <link href="{{ asset('css/user2/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="{{ asset('css/user2/waves.min.css') }}" type="text/css" media="all">

  <link rel="stylesheet" type="text/css" href="{{ asset('css/user2/feather.css') }}">
  <!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/user2/themify-icons.css') }}"> -->
  <!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/user2/icofont.css') }}"> -->
  <!-- <link rel="stylesheet" type="text/css" href="{{ asset('css/user2/simple-line-icons.css') }}"> -->
  <link href="{{ asset('libraries/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/user2/style.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/user2/pages.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('css/user2/widget.css') }}">

  <link href="{{ asset('css/usercss.css') }}" rel="stylesheet">
  <link href="{{ asset('css/usercss2.css') }}" rel="stylesheet">

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-SSN2BLFKBW"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-SSN2BLFKBW');
  </script>

</head>
<body>

  <div class="loader-bg">
    <div class="loader-bar"></div>
  </div>

  <div id="pcoded" class="pcoded">
    <div class="pcoded-overlay-box"></div>
    <div class="pcoded-container navbar-wrapper">

      <!-- Header -->
      @include('layouts/user2/includes/header')
      <!-- end header -->

      <div class="pcoded-main-container">
        <div class="pcoded-wrapper">
          <!-- menu -->
          @include('layouts/user2/includes/siderbar_console')
          <!-- end menu -->
        <div class="pcoded-content">

          <div class="page-header card">
            <div class="row align-items-end">
              <div class="col-lg-8">
                <div class="page-header-title">
                  <span class="pcoded-mtext"><h1 class="h3 mb-0 title-page">@yield('title')</h1></span>
                </div>
              </div>
              <div class="col-lg-4">
                <div class="page-header-breadcrumb">
                  <ul class=" breadcrumb breadcrumb-title">
                    @yield('breadcrumb')
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <div class="pcoded-inner-content">
            <div class="main-body">
              <div class="page-wrapper">
                <div class="page-body">
                  @yield('content')
                </div>
              </div>
            </div>
          </div>

        <div id="styleSelector">
        </div>

      </div>
    </div>
  </div>
</div>


  <script data-cfasync="false"  src="{{ asset('libraries/user2/email-decode.min.js')}}"></script>
  <script src="{{ asset('libraries/user2/jquery.min.js')}}"></script>

  <script src="{{ asset('libraries/user2/jquery-ui.min.js')}}"></script>
  <script src="{{ asset('libraries/user2/popper.min.js')}}"></script>
  <script src="{{ asset('libraries/user2/bootstrap.min.js')}}"></script>

  <script src="{{ asset('libraries/user2/waves.min.js')}}"></script>


  <script src="{{ asset('libraries/user2/jquery.slimscroll.js')}}"></script>

  <script src="{{ asset('libraries/user2/jquery.flot.js')}}"></script>
  <script src="{{ asset('libraries/user2/jquery.flot.categories.js')}}"></script>
  <script src="{{ asset('libraries/user2/curvedlines.js')}}"></script>
  <script src="{{ asset('libraries/user2/jquery.flot.tooltip.min.js')}}"></script>

  <script src="{{ asset('libraries/user2/chartist.js')}}"></script>

  <script src="{{ asset('libraries/user2/amcharts.js')}}"></script>
  <script src="{{ asset('libraries/user2/serial.js')}}"></script>
  <script src="{{ asset('libraries/user2/light.js')}}"></script>

  <script src="{{ asset('libraries/user2/pcoded.min.js')}}"></script>
  <script src="{{ asset('libraries/user2/vertical-layout.min.js')}}"></script>
  <script src="{{ asset('libraries/user2/script.min.js')}}"></script>

  <script src="{{ asset('libraries/user2/rocket-loader.min.js')}}" defer=""></script>
  <!-- Core plugin JavaScript-->
  <script src="{{ asset('libraries/js/template.js')}}"></script>
  <script src="{{ asset('libraries/jquery-easing/jquery.easing.min.js') }}"></script>
  <script src="{{ asset('js/user.min.js') }}"></script>
  <script src="{{ asset('js/all_page.js') }}"></script>
  <script src="{{ asset('/libraries/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('/libraries/sweetalert2/sweetalert2.min.js') }}"></script>
  <script src="{{ asset('/libraries/toastr/toastr.min.js') }}"></script>
  <script src="{{ asset('/libraries/bootstrap/js/bootstrap-show-password.min.js') }}"></script>


  @yield('scripts')
  <script type="text/javascript">
    $(document).ready(function () {
        // khởi tạo modal Toast
        const Toast = Swal.mixin({
          toast: true,
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000
        });
        //Ajax cập nhập thời gian đọc thông báo
        $('.unread').click( function () {
          var id = $(this).attr('data-id');
          // console.log(id);
          $.ajax({
            type: "get",
            url: "{{ route('user.notification.read_notification') }}",
            dataType: 'json',
            data: {
                "_token": "{{ csrf_token() }}",
                "id": id
            },
          });
        });

    })
  </script>
</body>

<!-- Mirrored from colorlib.com/polygon/admindek/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 12 Dec 2019 16:08:25 GMT -->
</html>
