<footer class="main-footer" style="margin-left: 0">
    <div class="float-right d-none d-sm-block" style="margin-right: 50px;">
        <a href="https://facebook.com/cloudzone.vn/" target="_blank"><img src="https://portal.cloudzone.vn/images/icon-facebook.png" alt=""></a>
        <a href="tel:02364455789"><img src="https://portal.cloudzone.vn/images/call.png" alt=""></a>
        <a href="mailto:support@cloudzone.vn"><img src="https://portal.cloudzone.vn/images/mail.png" alt=""></a>
      </div>
      <strong>Copyright © 2020 @php $year = 2020;$current_year = date('Y'); if ( $current_year > $year) { echo " - " . $current_year;} @endphp 
        <a href="https://cloudzone.vn" target="_blank">Cloudzone</a>.</strong> All rights reserved.
</footer>
