<!-- Main Sidebar Container -->
@php
    $user = UserHelper::get_user(Auth::user()->id);
    $currentRouteName = \Illuminate\Support\Facades\Route::currentRouteName();
    $cmnd_unactive_number = UserHelper::get_cmnd_unactive_number();
    $contract_pendings = UserHelper::get_contract_pending();
    $listGroupUser = UserHelper::getGroupUser();
@endphp
<aside class="main-sidebar sidebar-light-primary elevation-4">
        <!-- Brand Logo -->
        <a href="{{ route('admin.home') }}" class="brand-link">
          <img src="{{ asset('images/CloudzoneLogo-1.png')}}" alt="Cloudzone Logo" class="brand-image img-circle elevation-3 bg-white"
               style="opacity: .8">
          <span class="brand-text font-weight-light">Portal Cloudzone</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
          <!-- Sidebar user panel (optional) -->
          <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
              <img src="{{ asset('images/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
              <a href="{{ route('admin.home') }}" class="d-block">{{ $user->name }}</a>
            </div>
          </div>

          <!-- Sidebar Menu -->
          <nav class="mt-2" id="menu_sidebar">
            <!-- <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false"> -->
            <ul class="nav nav-pills nav-sidebar nav-child-indent flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <!-- Add icons to the links using the .nav-icon class
                   with font-awesome or any other icon font library -->
              {{-- user --}}
              <li id="menu_user" class="treeview item-active-menu nav-item {{ ( $currentRouteName == 'admin.user.list' || $currentRouteName == 'admin.user.create' || $currentRouteName == 'admin.user.detail' || $currentRouteName == 'admin.users.cmnd_verifies' || $currentRouteName == 'admin.user.quotation' || $currentRouteName == 'admin.contract.index' || $currentRouteName == 'admin.contract.print_view' || $currentRouteName == 'admin.user.groupUser' || $currentRouteName == 'admin.user.addPayment' ) ? 'menu-open' : '' }}">
                <a href="#" data-id="menu_user" class="nav-active-link nav-link {{ ( $currentRouteName == 'admin.user.list' || $currentRouteName == 'admin.user.create' || $currentRouteName == 'admin.user.detail' || $currentRouteName == 'admin.users.cmnd_verifies' || $currentRouteName == 'admin.user.quotation' || $currentRouteName == 'admin.contract.index' || $currentRouteName == 'admin.contract.print_view' ||  $currentRouteName == 'admin.user.groupUser' || $currentRouteName == 'admin.user.addPayment' ) ? 'active' : '' }}">
                  <i class="nav-icon fas fa-user"></i>
                  <p>
                    Quản lý người dùng
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview treeview-menu">
                  <li class="nav-item">
                    <a href="{{ route('admin.user.list') }}" class="nav-link {{ $currentRouteName == 'admin.user.list'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Danh sách người dùng</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.users.cmnd_verifies') }}" class="nav-link {{ $currentRouteName == 'admin.users.cmnd_verifies'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <span>Xác nhận CMND</span>
                      @if ($cmnd_unactive_number > 0)<span class="pull-right-container"><small class="label pull-right bg-red"> {{$cmnd_unactive_number}} </small></span> @endif
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.user.create') }}" class="nav-link {{ $currentRouteName == 'admin.user.create'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Tạo mới người dùng</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.user.groupUser') }}" class="nav-link {{ $currentRouteName == 'admin.user.groupUser'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Danh sách đại lý</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.user.quotation') }}" class="nav-link {{ $currentRouteName == 'admin.user.quotation'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Danh sách báo giá</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.contract.index') }}" class="nav-link {{ ($currentRouteName == 'admin.contract.index' || $currentRouteName == 'admin.contract.create_form' || $currentRouteName == 'admin.contract.update_form') ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Hợp đồng</p>
                      @if ($contract_pendings > 0)<span class="pull-right-container"><small class="label pull-right bg-warning"> {{$contract_pendings}} </small></span> @endif
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.user.addPayment') }}" class="nav-link {{ $currentRouteName == 'admin.user.addPayment'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Tạo nạp tiền tài khoản</p>
                    </a>
                  </li>
                </ul>
              </li>
              {{-- /.user --}}
              {{-- product --}}
              <li  id="menu_product" class="treeview item-active-menu nav-item {{ ( 
                $currentRouteName == 'admin.product.index' || $currentRouteName == 'admin.product_private.index'
                 || $currentRouteName == 'admin.product.state_vps_us' || $currentRouteName == 'admin.product.state_proxy'
                 || $currentRouteName == 'admin.product.state_cloudzone' || $currentRouteName == 'admin.product.state_proxy'
                ) ? 'menu-open' : '' }}">
                <a href="#" data-id="menu_product" class="nav-active-link nav-link {{ ( $currentRouteName == 'admin.product.index' || $currentRouteName == 'admin.product_private.index') ? 'active' : '' }}">
                  <i class="nav-icon fas fa-cubes"></i>
                  <p>
                    Quản lý sản phẩm
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview treeview-menu">
                  <li class="nav-item">
                    <a href="{{ route('admin.product.index') }}" class="nav-link {{ $currentRouteName == 'admin.product.index'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Sản phẩm cơ bản</p>
                    </a>
                  </li>
                  @foreach($listGroupUser as $groupuser)
                  <li class="nav-item">
                    <a href="{{ route('admin.product_private.index', $groupuser->id) }}" class="nav-link {{ $currentRouteName == 'admin.product_private.index'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>{{ $groupuser->name }}</p>
                    </a>
                  </li>
                  @endforeach
                  <li class="nav-item">
                    <a href="{{ route('admin.product.state_vps_us') }}" class="nav-link {{ $currentRouteName == 'admin.product.state_vps_us'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Bang VPS US</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.product.state_cloudzone') }}" class="nav-link {{ $currentRouteName == 'admin.product.state_cloudzone'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Bang VPS US Cloudzone</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.product.state_proxy') }}" class="nav-link {{ $currentRouteName == 'admin.product.state_proxy'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Bang Proxy</p>
                    </a>
                  </li>
                </ul>
              </li>
              {{-- /.produc --}}
              {{-- order --}}
              <li id="menu_order" class="treeview item-active-menu nav-item {{ ( $currentRouteName == 'admin.orders.index' || $currentRouteName == 'admin.invoices.index' || $currentRouteName == 'admin.orders.create') ? 'menu-open' : '' }}">
                <a href="#" data-id="menu_order" class="nav-active-link nav-link {{ ( $currentRouteName == 'admin.orders.index' || $currentRouteName == 'admin.invoices.index' || $currentRouteName == 'admin.orders.create') ? 'active' : '' }}">
                  <i class="nav-icon  fas fa-shopping-cart"></i>
                  <p>
                    Quản lý đơn đặt hàng
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview treeview-menu">
                  <li class="nav-item">
                    <a href="{{ route('admin.orders.index') }}" class="nav-link {{ $currentRouteName == 'admin.orders.index'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Đơn đặt hàng</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.invoices.index') }}" class="nav-link {{ $currentRouteName == 'admin.invoices.index'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Danh sách hóa đơn</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.orders.create') }}" class="nav-link {{ $currentRouteName == 'admin.orders.create'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Tạo đơn đặt hàng</p>
                    </a>
                  </li>
                </ul>
              </li>
              {{-- /.order --}}
                {{-- payment --}}
              <li id="menu_payment" class="treeview item-active-menu nav-item {{ $currentRouteName == 'admin.payments.index' || $currentRouteName == 'admin.payments.invoice_create' || $currentRouteName == 'admin.payments.invoice_addon' || $currentRouteName == 'admin.payments.invoice_upgrade_hosting' || $currentRouteName == 'admin.payments.invoice_change_ip' || $currentRouteName == 'admin.payments.invoice_expired' || $currentRouteName == 'admin.payments.invoice_domain' || $currentRouteName == 'admin.payments.invoice_domain_exp' || $currentRouteName == 'admin.payments.all' ? 'menu-open' : '' }}">
                <a href="#" data-id="menu_payment" class="nav-active-link nav-link {{ $currentRouteName == 'admin.payments.index' || $currentRouteName == 'admin.payments.invoice_create' || $currentRouteName == 'admin.payments.invoice_addon' || $currentRouteName == 'admin.payments.invoice_upgrade_hosting' || $currentRouteName == 'admin.payments.invoice_change_ip' || $currentRouteName == 'admin.payments.invoice_expired' || $currentRouteName == 'admin.payments.invoice_domain' || $currentRouteName == 'admin.payments.invoice_domain_exp' || $currentRouteName == 'admin.payments.all'  ? 'active' : '' }}">
                    <i class="fas fa-donate nav-icon"></i>
                  <p>
                    Quản lý thanh toán
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview treeview-menu">
                  <li class="nav-item">
                    <a href="{{ route('admin.payments.index') }}" class="nav-link {{ $currentRouteName == 'admin.payments.index'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Nạp tiền</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.payments.invoice_create') }}" class="nav-link {{ $currentRouteName == 'admin.payments.invoice_create'  ? 'active' : '' }} ">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Thanh toán tạo mới</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.payments.invoice_addon') }}" class="nav-link {{ $currentRouteName == 'admin.payments.invoice_addon'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Addon VPS</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.payments.invoice_upgrade_hosting') }}" class="nav-link {{ $currentRouteName == 'admin.payments.invoice_upgrade_hosting'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Nâng cấp Hosting</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.payments.invoice_change_ip') }}" class="nav-link {{ $currentRouteName == 'admin.payments.invoice_change_ip'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Đổi IP VPS</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.payments.invoice_expired') }}" class="nav-link {{ $currentRouteName == 'admin.payments.invoice_expired'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Gia hạn dịch vụ</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.payments.invoice_domain') }}" class="nav-link {{ $currentRouteName == 'admin.payments.invoice_domain'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Thanh toán tên miền</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.payments.invoice_domain_exp') }}" class="nav-link {{ $currentRouteName == 'admin.payments.invoice_domain_exp'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Gia hạn tên miền</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.payments.all') }}" class="nav-link {{ $currentRouteName == 'admin.payments.all'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Tất cả thanh toán</p>
                    </a>
                  </li>
                </ul>
              </li>
              {{-- /.payment --}}
              {{-- sevices --}}
              <li id="menu_services" class="treeview item-active-menu nav-item {{ ( $currentRouteName == 'admin.email_hosting.index' || $currentRouteName == 'admin.colo.index' || $currentRouteName == 'admin.vps.index' 
                || $currentRouteName == 'admin.hostings.index'  || $currentRouteName == 'admin.server.index' 
                || $currentRouteName == 'admin.vps_us.index' || $currentRouteName == 'admin.email_hosting.index'
                || $currentRouteName == 'admin.hosting_singapore.index' || $currentRouteName == 'admin.proxy.listProxy' 
                ) ? 'menu-open' : '' }}">
                <a href="#" data-id="menu_services" class="nav-active-link nav-link {{ ( $currentRouteName == 'admin.vps.index' || $currentRouteName == 'admin.hostings.index' 
                  || $currentRouteName == 'admin.server.index' || $currentRouteName == 'admin.hosting_singapore.index' 
                  || $currentRouteName == 'admin.vps_us.index' || $currentRouteName == 'admin.colo.index' 
                  || $currentRouteName == 'admin.email_hosting.index' || $currentRouteName == 'admin.proxy.listProxy' 
                  ) ? 'active' : '' }}">
                  <i class="fas fa-chart-pie nav-icon"></i>
                  <p>
                    Quản lý dịch vụ
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview treeview-menu">
                  <li class="nav-item">
                    <a href="{{ route('admin.vps.index') }}" class="nav-link {{  $currentRouteName == 'admin.vps.index'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Danh sách VPS</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.vps_us.index') }}" class="nav-link {{  $currentRouteName == 'admin.vps_us.index'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Danh sách VPS US</p>
                    </a>
                  </li>
                  <li class="nav-item {{ ( $currentRouteName == 'admin.hostings.index' || $currentRouteName == 'admin.hosting_singapore.index' ) ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ $currentRouteName == 'admin.hostings.index'  ? 'active' : '' }}">
                      <i class="fas fa-database nav-icon"></i>
                      <p>
                        Danh sách Hosting
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    <ul class="nav nav-treeview nav-3">
                        <li class="nav-item">
                          <a href="{{ route('admin.hostings.index') }}" class="nav-link {{ $currentRouteName == 'admin.hostings.index'  ? 'active' : '' }}">
                            <i class="fas fa-minus nav-icon"></i>
                            <p>Hosting Việt Nam</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="{{ route('admin.hosting_singapore.index') }}" class="nav-link {{ $currentRouteName == 'admin.hostings.index'  ? 'active' : '' }}">
                            <i class="fas fa-minus nav-icon"></i>
                            <p>Hosting Singapore</p>
                          </a>
                        </li>
                        <li class="nav-item">
                          <a href="#" class="nav-link {{ $currentRouteName == 'admin.hostings.index'  ? 'active' : '' }}">
                            <i class="fas fa-minus nav-icon"></i>
                            <p>Hosting US</p>
                          </a>
                        </li>
                    </ul>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.server.index') }}" class="nav-link {{ $currentRouteName == 'admin.server.index'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Danh sách Server</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.colo.index') }}" class="nav-link {{ $currentRouteName == 'admin.colo.index'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Colocation</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.proxy.listProxy') }}" class="nav-link {{ $currentRouteName == 'admin.proxy.listProxy'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Danh sách Proxy</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.email_hosting.index') }}" class="nav-link {{ $currentRouteName == 'admin.email_hosting.index'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Email Hosting</p>
                    </a>
                  </li>
                </ul>
              </li>
              {{-- /.sevices --}}
              {{-- Domain --}}
              <li id="menu_domain" class="treeview item-active-menu nav-item {{ ( $currentRouteName == 'admin.domain-products.index' || $currentRouteName == 'admin.domain.index') ? 'menu-open' : '' }}">
                <a href="#" data-id="menu_domain" class="nav-active-link nav-link {{ ( $currentRouteName == 'admin.domain-products.index' || $currentRouteName == 'admin.domain.index') ? 'active' : '' }}">
                  <i class="fa fa-globe nav-icon"></i>
                  <p>
                    Quản lý tên miền
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview treeview-menu">
                  <li class="nav-item">
                    <a href="{{ route('admin.domain-products.index') }}" class="nav-link {{ $currentRouteName == 'admin.domain-products.index'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Sản phẩm tên miền</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('admin.domain.index') }}" class="nav-link {{ $currentRouteName == 'admin.domain.index'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Danh sách tên miền</p>
                    </a>
                  </li>
                </ul>
              </li>
              {{-- /.sevices --}}
              {{-- message  --}}
              <li id="menu_message"  class="treeview item-active-menu nav-item">
                <a href="{{ route('admin.messages.index') }}" data-id="menu_message" class="nav-active-link nav-link {{ $currentRouteName == 'admin.messages.index'  ? 'active' : '' }}">
                  <i class="far fa-comment-dots nav-icon"></i>
                  <p>
                    Quản lý tin nhắn 
                    <span class="badge badge-danger right">10</span>
                  </p>
                </a>
              </li>
              {{-- /.message  --}}
              {{-- email --}}
              <li id="menu_email" class="treeview item-active-menu nav-item {{ $currentRouteName == 'email.index' || 
                $currentRouteName == 'email.email_marketing.list' || $currentRouteName == 'email.service.list' 
                ? 'menu-open' : '' }}">
                <a href="#" data-id="menu_email" class="nav-active-link nav-link {{ $currentRouteName == 'email.index' || $currentRouteName == 'email.email_marketing.list' || $currentRouteName == 'email.service.list' ? 'active' : '' }}">
                  <i class=" nav-icon  fas fa-envelope-open-text"></i>
                  <p>
                    Quản lý Email
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview treeview-menu">
                  <li class="nav-item">
                    <a href="{{ route('email.index') }}" class="nav-link {{ $currentRouteName == 'email.index'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Email Template</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('email.service.list') }}" class="nav-link {{ $currentRouteName == 'email.service.list'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Email Service</p>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a href="{{ route('email.email_marketing.list') }}" class="nav-link {{ $currentRouteName == 'email.email_marketing.list'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Email Marketing</p>
                    </a>
                  </li>
                </ul>
              </li>
              {{-- /.email --}}

              {{-- event --}}
              <li id="menu_event" class="treeview item-active-menu nav-item {{ $currentRouteName == 'admin.event.index'  ? 'menu-open' : '' }}">
                <a href="#" data-id="menu_event" class="nav-active-link nav-link {{ $currentRouteName == 'admin.event.index'  ? 'active' : '' }}">
                  <i class="nav-icon fas fa-gift"></i>
                  <p>
                    Quản lý khuyến mãi
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview treeview-menu">
                  <li class="nav-item">
                    <a href="{{ route('admin.event.index') }}" class="item-active-menu nav-link {{ $currentRouteName == 'admin.event.index'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Danh sách khuyến mãi</p>
                    </a>
                  </li>
                </ul>
              </li>
              {{-- /.event --}}
              {{-- tutorial --}}
              <li id="menu_tutorial" class="treeview item-active-menu nav-item {{ $currentRouteName == 'admin.tutorial.index'  ? 'menu-open' : '' }}">
                <a href="#" data-id="menu_event" class="nav-active-link nav-link {{ $currentRouteName == 'admin.tutorial.index'  ? 'active' : '' }}">
                  <i class="nav-icon fas fa-bookmark"></i>
                  <p>
                    Quản lý hướng dẫn
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview treeview-menu">
                  <li class="nav-item">
                    <a href="{{ route('admin.tutorial.index') }}" class="item-active-menu nav-link {{ $currentRouteName == 'admin.tutorial.index'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Danh sách hướng dẫn</p>
                    </a>
                  </li>
                </ul>
              </li>
              {{-- /.tutorial --}}
              {{-- notification --}}
              <li id="menu_notification" class="treeview item-active-menu nav-item {{ $currentRouteName == 'admin.notification.index'  ? 'menu-open' : '' }}">
                <a href="#" data-id="menu_notification" class="nav-active-link nav-link {{ $currentRouteName == 'admin.notification.index'  ? 'active' : '' }}">
                  <i class=" nav-icon fas fa-bell"></i>
                  <p>
                    Quản lý Thông báo
                    <i class="right fas fa-angle-left"></i>
                  </p>
                </a>
                <ul class="nav nav-treeview treeview-menu">
                  <li class="nav-item">
                    <a href="{{ route('admin.notification.index') }}" class="nav-link {{ $currentRouteName == 'admin.notification.index'  ? 'active' : '' }}">
                      <i class="far fa-circle nav-icon"></i>
                      <p>Danh sách thông báo</p>
                    </a>
                  </li>
                </ul>
              </li>
              {{-- /.notification --}}
                <li id="menu_ticket" class="item-active-menu nav-item">
                    <a href="{{ route('admin.ticket.getList') }}" data-id="menu_ticket"  class="nav-active-link nav-link {{ $currentRouteName == 'admin.ticket.getList'  ? 'active' : '' }}">
                        <i class="far fa-comments nav-icon"></i>
                        <p>
                            Quản lý Tickets
                        </p>
                    </a>
                </li>
              {{-- server_hosting  --}}
                <li id="menu_server"  class="treeview item-active-menu nav-item {{ $currentRouteName == 'admin.server_hosting.create_server_hosting' || $currentRouteName == 'admin.server_hosting.server_si' || $currentRouteName == 'admin.server_hosting.index' || $currentRouteName == 'admin.hostings.getHostingDaPortal'  ? 'menu-open' : '' }}">
                  <a href="#" data-id="menu_server" class="nav-active-link nav-link {{ $currentRouteName == 'admin.server_hosting.index' || $currentRouteName == 'admin.server_hosting.server_si' || $currentRouteName == 'admin.server_hosting.create_server_hosting' || $currentRouteName == 'admin.hostings.getHostingDaPortal'  ? 'active' : '' }}">
                    <i class="fas fa-database nav-icon"></i>
                    <p>
                      Quản lý Server Hosting
                      <i class="right fas fa-angle-left"></i>
                    </p>
                  </a>
                  <ul class="nav nav-treeview treeview-menu">
                    <li class="nav-item">
                      <a href="{{ route('admin.server_hosting.index') }}" class="nav-link {{ $currentRouteName == 'admin.server_hosting.index'  ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Hosting Việt Nam</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{ route('admin.server_hosting.server_si') }}" class="nav-link {{ $currentRouteName == 'admin.server_hosting.server_si'  ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Hosting Singapore</p>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="{{ route('admin.server_hosting.create_server_hosting') }}" class="nav-link {{ $currentRouteName == 'admin.server_hosting.create_server_hosting'  ? 'active' : '' }}">
                        <i class="far fa-circle nav-icon"></i>
                        <p>Thêm server hosting</p>
                      </a>
                    </li>
                  </ul>
                </li>
                {{-- /.server_hosting  --}}
                {{-- log  --}}
                <li id="menu_log"  class="treeview item-active-menu nav-item">
                  <a href="{{ route('admin.activity_log.index') }}" data-id="menu_log" class="nav-active-link nav-link {{ $currentRouteName == 'admin.activity_log.index'  ? 'active' : '' }}">
                    <i class="fas fa-chart-line nav-icon"></i>
                    <p>
                      Quản lý Activity Log
                    </p>
                  </a>
                </li>
                {{-- /.log  --}}
                {{-- promotion  --}}
                <li id="menu_promotion"  class="treeview item-active-menu nav-item">
                  <a href="{{ route('admin.promotion.index') }}" data-id="menu_promotion" class="nav-active-link nav-link {{ $currentRouteName == 'admin.promotion.index'  ? 'active' : '' }}">
                    <i class="fas fa-percent nav-icon"></i>
                    <p>
                      Quản lý mã khuyến mãi
                    </p>
                  </a>
                </li>
                {{-- /.promotion  --}}
                {{-- momo  --}}
                <li id="menu_send_mail"  class="treeview item-active-menu nav-item">
                  <a href="{{ route('admin.send_mail.index') }}" data-id="menu_send_mail" class="nav-active-link nav-link {{ $currentRouteName == 'admin.momo.index'  ? 'active' : '' }}">
                    <i class="far fa-envelope nav-icon"></i>
                    <p>
                      Quản lý gửi mail
                    </p>
                  </a>
                </li>
                {{-- /.momo 
                {{-- agency  --}}
                {{-- <li id="menu_agency_api"  class="treeview item-active-menu nav-item">
                  <a href="{{ route('admin.agency.index') }}" data-id="menu_agency" class="nav-active-link nav-link {{ $currentRouteName == 'admin.agency.index'  ? 'active' : '' }}">
                    <i class="fas fa-credit-card nav-icon"></i>
                    <p>
                      Quản lý tài khoản API
                    </p>
                  </a>
                </li> --}}
                {{-- /.agency  --}}
            </ul>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
      </aside>
