<!DOCTYPE html>
<html>
<head>
  <title>Cloudzone Customer System</title>
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
</head>
<body style="width: 100%;margin: auto;">
	<div class="send-mail" style="width: 90%;border: 1px solid #ddd;padding-top: 25px;padding-bottom: 10px;">
		<div class="mail-logo" style="text-align: center;border-bottom: 1px solid #ddd;padding-bottom: 30px;">
			<img src="{{ !empty($message->embed('images/logo-cloudzone-mail.png')) ? $message->embed('images/logo-cloudzone-mail.png') : $message->embed('images/logo-mail.png')  }}" alt="logo-mail" width="200px">
		</div>
		<div class="mail-content" style="font-size: 16px;font-family: roboto;padding-top: 35px;margin: 10px 10%;color: rgb(16, 55, 132);">
			@yield('content')
		</div>
	</div>
	<div class="mail-footer" style="width: 90%;border: 1px solid #ddd;background: #f2f2f2;">
		<div style="padding: 15px 4% 10px;text-align: center;font-size: 13px;font-family: roboto;">
			Công ty TNHH MTV Công nghệ Đại Việt Số <br>
            Địa chỉ: 257 Lê Duẩn , Đà Nẵng. <br>
            Văn phòng: 67 Nguyễn Thị Định, Đà Nẵng. <br>
            Điện thoại: 0236 4455 789 - Website: <a href="https://cloudzone.vn ">https://cloudzone.vn </a><br>
            Email: <a href="mailto:support@cloudzone.vn">support@cloudzone.vn</a> <br>
            <div style="padding-top: 5px;">
                <a href="https://m.me/cloudzone.vn" style="padding-right: 5px;"><img src="{{ $message->embed('images/icon-facebook.png') }}" width="30px"></a>
            </div>
			<div style="clear:both;"></div>
		</div>
	</div>
</body>
</html>
