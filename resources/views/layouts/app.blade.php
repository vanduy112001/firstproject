<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('images/favicon.ico') }}">
    <title>{{  'Cloudzone Portal Manager' }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('fonts/fontawesome-free/css/all.min.css') }}">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/css.css') }}" rel="stylesheet">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="{{ asset('/libraries/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
    <!-- Toastr -->
    <link rel="stylesheet" href="{{ asset('/libraries/toastr/toastr.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{ asset('css/OverlayScrollbars.min.css')}}">
    {{---------------------------------- Template AdminLTE --------------------------}}
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">

    @yield('style')
</head>
<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">
    @include('layouts/includes/header')
    @include('layouts/includes/siderbar')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">
                            @yield('title')
                        </h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            @yield('breadcrumb')
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- /.content-main -->
        <section class="content">
            <div class="container-fluid">
              <div id="app">
                  <main class="py-4">
                      @yield('content')
                  </main>
              </div>
            </div>
        </section>

        <!-- /.content-footer -->
        <section class="footer">
            <div class="container-fluid">
                @yield('footer')
            </div>
        </section>
      </div>
    <!-- jQuery -->
    <script src="{{ asset('libraries/jquery/jquery.min.js')}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('libraries/jquery-ui/jquery-ui.min.js')}}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('libraries/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('libraries/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- SweetAlert2 -->
    <script src="{{ asset('libraries/sweetalert2/sweetalert2.min.js')}}"></script>
    <script src="{{ asset('libraries/js/adminlte.js')}}"></script>
    <script src="{{ asset('libraries/js/jquery.overlayScrollbars.min.js')}}"></script>
    <script src="{{ url('libraries/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset('/libraries/toastr/toastr.min.js') }}" ></script>
    <script src="https://js.pusher.com/7.0/pusher.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
            const Toast = Swal.mixin({
                toast: true,
                showConfirmButton: false,
                timer: 3000
            });

            active_menu();

            function active_menu() {
               var session_menu = JSON.parse( sessionStorage.getItem('menu') );
               if (session_menu != null) {
                 if (session_menu.length > 0) {
                     $.each(session_menu , function (index , value) {
                        $('#' + value).addClass('menu-open');
                     })
                 }
               }
            }

            $('#menu_sidebar ul .item-active-menu .nav-active-link').on('click', function () {
                // console.log('da click');
               if ( typeof(Storage) !== 'undefined') {
                  var session_menu = JSON.parse( sessionStorage.getItem('menu') );
                  //nav-item menu-open
                  if (session_menu != null) {
                    if (session_menu.length > 0) {
                        var menu_li = $(this).attr('data-id');
                        var check = true;
                        $.each(session_menu , function (index , value) {
                            if (menu_li == value) {
                                session_menu.splice(index, 1);
                                check = false;
                            }
                        })
                        if (check == true) {
                          session_menu.push(menu_li);
                          // sessionStorage.setItem('menu', session_menu);
                          sessionStorage.setItem("menu", JSON.stringify(session_menu));
                        } else {
                          sessionStorage.setItem("menu", JSON.stringify(session_menu));
                        }
                    } else {
                        var menu_li = $(this).attr('id');
                        var session_menu = JSON.stringify([menu_li]);
                        sessionStorage.setItem('menu', session_menu);
                    }
                  } else {
                     // console.log('ko');
                    var menu_li = $(this).attr('id');
                    var session_menu = JSON.stringify([menu_li]);
                    sessionStorage.setItem('menu', session_menu);
                  }

               }
            })

            // $(document).on('click', '#toastsContainerBottomLeft', function() {
            //     // console.log('da click');
            //     window.location.href = '/admin/messages';
            // });

            // var pusher = new Pusher('bf06d29e8d774078d193', {
            //   cluster: 'ap1'
            // });
            
            // const page = $('#page').val();
            // const user_chat = {{ Auth::id() }}

            // var channel = pusher.subscribe('user-chat');
            // channel.bind('user-chat-for-admin', function(data) {
            //     let chat_message = JSON.stringify(data);
            //     $.ajax({
            //         url: '/admin/messages/userChat',
            //         data: {message: chat_message},
            //         dataType: 'json',
            //         success: function (data) {
            //             console.log(data.content);
            //             if (data.error == 0) {
            //                 let titleToasts = data.content.user.name;
            //                 let bodyToasts = '';
            //                 if ( data.content.content.length < 30 ) {
            //                     bodyToasts = data.content.content;
            //                 } else {
            //                     bodyToasts = data.content.content.substring(0, 30);
            //                     bodyToasts += ' ...';
            //                 }
            //                 $(document).Toasts('create', {
            //                     title: titleToasts,
            //                     position: 'bottomLeft',
            //                     autohide: true,
            //                     delay: 5000,
            //                     body: bodyToasts
            //                 })
            //                 // trong page chat
            //                 if (page == 'chat') {
            //                     let choose_user_id = $('#user').val();
            //                     let html = '';
            //                     let user_chat = '.user_chat_' + data.content.user_id;
            //                     // nếu đang mở đúng user đó thì thêm chat vào
            //                     if ( choose_user_id == data.content.user_id ) {
            //                         $(user_chat + ' h5').addClass('font-weight-bold');
            //                         $(user_chat + ' p').addClass('font-weight-bold');
            //                         $(user_chat + ' p').addClass('text-primary');
            //                         $(user_chat + ' .chat_date').text(data.content.diffForHumans);
            //                         $(user_chat + ' .chat_people_text').text(bodyToasts);

            //                         html += `
            //                             <div class="incoming_msg">
            //                               <div class="received_msg">
            //                                 <div class="received_withd_msg">
            //                                   <p>${data.content.content}</p>
            //                                   <span class="time_date"> ${data.content.timeSend}    |    ${data.content.dateSend}</span>
            //                                 </div>
            //                               </div>
            //                             </div>
            //                         `;
            //                         $('.msg_history').append(html);
            //                         $('.msg_history').scrollTop( $(".type_msg").offset().top );
            //                     }
            //                     // nếu ko phải user đang mở:
            //                     // xóa user đó trong khung chat, thêm mới vào
            //                     else {
            //                         $(user_chat).remove();
            //                         html = `
            //                             <div class="chat_list user_chat_${data.content.user_id}" data-id="${data.content.user_id}">
            //                                 <div class="chat_people">
            //                                     <div class="chat_img">
            //                                         <img class="img-circle elevation-2" src="${data.content.avatar}" alt="sunil"> 
            //                                     </div>
            //                                     <div class="chat_ib">
            //                                         <h5 class="font-weight-bold">
            //                                             ${data.content.user_name}
            //                                             <span class="chat_date">${data.content.diffForHumans}</span>
            //                                         </h5>
            //                                         <p class="chat_people_text text-primary font-weight-bold">${bodyToasts}</p>
            //                                     </div>
            //                                 </div>
            //                             </div>
            //                         `;
            //                         $('.inbox_chat').prepend(html);
            //                     }
            //                 }

            //             } else {
            //                 console.log(data);
            //             }
            //         },
            //         error: function (e) {
            //             console.log(e);
            //         }
            //     });

            // });
        })
    </script>
    @yield('scripts')
  </div>
</body>
</html>
