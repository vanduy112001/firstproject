<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ 'Cloudzone Customer Portal' }}</title>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" defer></script>

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('fonts/fontawesome-free/css/all.min.css') }}">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <!-- Styles -->
  <link href="{{ asset('css/login.css') }}" rel="stylesheet">
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">

  <link href="{{ asset('css/css.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('css/adminlte.min.css')}}">
  {{---------------------------------- Template AdminLTE --------------------------}}
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('css/ionicons.min.css') }}">

  <!-- Google Tag Manager -->
  <script>
    (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NHDFXLJ');
  </script>
  <!-- End Google Tag Manager -->
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-SSN2BLFKBW"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-SSN2BLFKBW');
  </script>

  @yield('style')
</head>
<body class="hold-transition" id="page-auth">
  <div class="wrapper">
    <!-- Content Wrapper. Contains page content -->
    <!-- /.content-header -->
    <!-- /.content-main -->
    @yield('content')

    <!-- jQuery -->
    <script src="{{ asset('libraries/jquery/jquery.min.js')}}"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('libraries/jquery-ui/jquery-ui.min.js')}}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('libraries/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{ asset('libraries/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{ asset('libraries/js/adminlte.js')}}"></script>
    <script src="{{ url('libraries/ckeditor/ckeditor.js') }}"></script>
    <!-- script vue -->
    <script src="{{ asset('libraries/vue/vue.min.js') }}"></script>

    @yield('scripts')
  </div>
</body>
</html>