@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item"><a href="{{ route('admin.vps.index') }}"><i class="fa fa-users" aria-hidden="true"></i> Danh sách VPS</a></li>
<li class="breadcrumb-item active"><i class="fa fa-user" aria-hidden="true"></i> Xuất File Excel</li>
@endsection
<div class="col-md-6" style="margin:auto">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title text-center">Xuất File Excel</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @if ($errors->any())
        <div class="alert alert-danger" style="margin-top:20px">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(session("success"))
            <div class="bg-success p-4">
                <p class="text-light">{{session("success")}}</p>
            </div>
        @elseif(session("fails"))
            <div class="bg-danger p-4">
                <p class="text-light">{{session("fails")}}</p>
            </div>
        @endif
        @if(session("error"))
        <div class="bg-success p-4">
            <p class="text-light">{{session("error")}}</p>
        </div>
        @endif
        <div id="notification">

        </div>
        <form role="form" method="post" action="{{ route('admin.vps_us.form_export_excel') }}" id="form_export">
            {{ csrf_field() }}
            <div class="card-body">
                <div class="form-group">
                    <label for="">Danh sách VPS</label>
                    <div class="p-3">
                        @foreach ($list_vps as $vps)
                            <div class="detail-vps">
                                {{ $vps->ip }}
                                <input type="hidden" name="id[]" class="id_vps" value="{{ $vps->id }}">
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <label for="">Tên file</label>
                    <input type="text" name="file_name" class="form-control">
                </div>
                <div class="form-group">
                    <label for="">Chọn thuộc tính xuất excel</label>
                    <div class="form-check">
                        <input class="form-check-input" name="ip" value="1" type="checkbox" checked>
                        <label class="form-check-label">Địa chỉ IP</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" name="state" value="1" type="checkbox" checked>
                        <label class="form-check-label">Bang</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" name="config" value="1" type="checkbox" checked>
                        <label class="form-check-label">Cấu hình</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" name="due_date" value="1" type="checkbox" checked>
                        <label class="form-check-label">Ngày kết thúc</label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" name="password" value="1" type="checkbox" checked>
                        <label class="form-check-label">Mật khẩu</label>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a class="btn btn-default" href="{{ route('admin.vps.index') }} ">Hủy</a>
                <button type="submit" class="btn btn-info float-right">Xác nhận</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
    <script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
    <script type="text/javascript">
          $(document).ready(function() {
                // select
                $('.select2').select2();

                $('#form_export').on('submit', function(event) {
                  event.preventDefault();
                  var form = $(this).serialize();
                  $.ajax({
                   url: '/admin/vps_us/form_export_excel',
                   type: 'post',
                   dataType: 'json',
                   data: form,
                   success: function (data) {
                        if (!data.error) {
                            $('#export_excel').attr('href', data.content);
                            var a = document.createElement('a');
                            a.href = data.content;
                            a.download = data.file_name;
                            document.body.append(a);
                            a.click();
                            a.remove();
                            
                        } else {
                            $('#notification').html('<span class="text-center">'+ data.content +'</span>');
                        }
                   },
                   error: function (e) {
                       console.log(e);
                       var html = '<span class="text-center">Lỗi truy xuất VPS US.</span>';
                       $('#notification').html(html);
                   }
                })
              })
          });
    </script>
@endsection
