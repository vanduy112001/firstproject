@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Quản lý VPS
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item "><a href="/admin/vps/">VPS</a></li>
    <li class="breadcrumb-item active">Chỉnh sửa VPS</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" style="margin-top:20px">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper dt-bootstrap">
            <h4>Thêm VPS vào Portal</h4>
            <div class="container">
                <form action="{{ route('admin.vps.create_sevices') }}" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Loại VPS:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="type_vps" class="form-control" id="type_vps">
                                                <option value="" disabled selected>Chọn loại VPS</option>
                                                <option value="VPS">VPS</option>
                                                <option value="NAT-VPS">NAT VPS</option>
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Tên khách hàng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select disabled name="user_id" class="form-control select2" id="user_id">
                                                <option value="" selected disabled>Chọn khách hàng</option>
                                                @foreach ($users as $user)
                                                    @php
                                                        $selected = '';
                                                        if ($user->id == old('user_id')) {
                                                            $selected = 'selected';
                                                        }
                                                    @endphp
                                                    <option value="{{$user->id}}" {{$selected}}>{{$user->name}} - {{$user->email}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Sản phẩm / Dịch vụ:</label>
                                        </div>
                                        <div class="col-md-8" id="product">
                                            <select name="product_id" id="product_id" disabled class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Hosting">
                                                <option disabled selected>Chọn sản phẩm</option>
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">IP:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="ip" value="{{ old('ip') }}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">User:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="sevices_username" value="{{ old('sevices_username') }}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Password:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="sevices_password" value="{{ old('sevices_password') }}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">VM_ID:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="vm_id" value="{{ old('vm_id') }}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Trạng thái:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="status" id="status" class="select2 form-control" style="width: 100%;" data-value="Hosting">
                                                <option value="" disabled>Chọn trạng thái</option>
                                                <option value="Active" selected>Đã tạo</option>
                                                <option value="Pending">Chưa tạo</option>
                                                <option value="Cancel">Hủy</option>
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice row">
                                        <div class="col-md-4 text-right">
                                            <label for="">Cấu hình thêm:</label>
                                        </div>
                                        <div class="col-md-8">
                                        </div>
                                        <div class="col-md-4 text-right mt-2">
                                            <label for="">CPU:</label>
                                        </div>
                                        <div class="col-md-8 mt-2">
                                            <input type="number" class="form-control" name="addon_cpu" value="{{ !empty(old('addon_cpu')) ? old('addon_cpu') : 0 }}">
                                        </div>
                                        <div class="col-md-4 text-right mt-2">
                                            <label for="">RAM:</label>
                                        </div>
                                        <div class="col-md-8 mt-2">
                                            <input type="number" class="form-control" name="addon_ram" value="{{ !empty(old('addon_ram')) ? old('addon_ram') : 0 }}">
                                        </div>
                                        <div class="col-md-4 text-right mt-2">
                                            <label for="">DISK:</label>
                                        </div>
                                        <div class="col-md-8 mt-2">
                                            <input type="number" class="form-control" name="addon_disk" value="{{ !empty(old('addon_disk')) ? old('addon_disk') : 0 }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">OS:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" name="os">
                                                <option value="" disabled>Chọn hệ điều hành</option>
                                                @foreach($list_os as $key =>  $os)
                                                    <option value="{{ $key }}">{{ $os }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Security :</label>
                                        </div>
                                        <div class="col-md-8 ml-2 icheck-primary d-inline">
                                            <input type="checkbox" class="custom-control-input" name="security" id="security" value="1" checked>
                                            <label for="security">
                                            </label>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Ngày đặt hàng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="date_create" value="{{  !empty(old('date_create')) ? old('date_create') : date('d-m-Y') }}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Ngày tạo :</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text"  name="created_at" value="{{ !empty(old('created_at')) ? old('created_at') : date('d-m-Y') }}" class="form-control">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Ngày hết hạn :</label>
                                        </div>
                                        <div class="col-md-8" id="next_due_date">
                                            <input disabled type="text"  name="next_due_date" value="{{ !empty(old('next_due_date')) ? old('next_due_date') : date('d-m-Y', strtotime('+1 month')) }}" class="form-control">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Thời gian:</label>
                                        </div>
                                        <div class="col-md-8" id="billing_cycle">
                                            <select class="form-control" name="billing_cycle">
                                                <option value="" disabled>Chọn thời gian</option>
                                                @foreach($billings as $key => $billing)
                                                    <option value="{{ $key }}">{{ $billing }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                      <div class="col-md-4 text-right" style="padding-top: 5px;">
                                          <label for="">GH theo chu kỳ :</label>
                                      </div>
                                      <div class="col-md-8 ml-2 icheck-primary d-inline">
                                          <input type="checkbox" class="custom-control-input" name="expire_billing_cycle" id="expire_billing_cycle" @if(!empty($detail->expire_billing_cycle)) checked @endif  value="1">
                                          <label for="expire_billing_cycle">
                                          </label>
                                      </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Giá áp tay :</label>
                                        </div>
                                        <div class="col-md-8 ml-2 icheck-primary d-inline">
                                            <input type="checkbox" class="custom-control-input" name="price_override" id="price_override" value="1">
                                            <label for="price_override">
                                            </label>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Thành tiền :</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text"  name="total" id="total" value="{{ old('total') }}" class="form-control" disabled>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Trừ tiền tài khoản :</label>
                                        </div>
                                        <div class="col-md-8 ml-2 icheck-primary d-inline">
                                            <input type="checkbox" class="custom-control-input" name="sub_value" id="sub_value" value="1">
                                            <label for="sub_value">
                                            </label>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="form-button pb-4">
                                        @csrf
                                        <input type="submit" value="Thêm VPS" class="btn btn-primary float-right">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- Modal xoa vps --}}
<div class="modal fade" id="delete-vps">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button-vps">Xóa VPS</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
@endsection
@section('scripts')
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('js/add_vps.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
		//Date picker
    $('#type_vps').on('change', function () {
      $('#user_id').attr('disabled', false);
    })
		$('#datepicker').datepicker({
			autoclose: true
		});
        // select
        $('.select2').select2();
	});
</script>
@endsection
