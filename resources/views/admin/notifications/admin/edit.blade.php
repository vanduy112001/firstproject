@extends('layouts.app')
@section('title')
Chỉnh sửa thông báo
@endsection
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item"><a href="{{ route('admin.notification.index') }}"><i class="fa fa-users" aria-hidden="true"></i> List notifications</a></li>
<li class="breadcrumb-item active"><i class="fa fa-user" aria-hidden="true"></i> Edit notification</li>
@endsection
<div class="col-md-12" style="margin:auto">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title text-center">Chỉnh sửa thông báo</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @if ($errors->any())
        <div class="alert alert-danger" style="margin-top:20px">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(session("error"))
        <div class="bg-success">
            <p class="text-light">{{session("error")}}</p>
        </div>
        @endif
        <form role="form" method="post" action="{{ route('admin.notification.update', ['id' => $notification->id]) }}">
            {{ csrf_field() }}
            <div class="card-body">
                <div class="form-group">
                    <label for="name">Tiêu đề</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Tên"
                        value="{{ old('name') ? old('name') : $notification->name }}">
                </div>
                <div class="form-group">
                    <label for="content">Nội dung</label>
                    <textarea id="content" class="form-control"
                        name="content">{{ old('content') ? old('content') : $notification->content }}</textarea>
                </div>
                <div class="form-group">
                    <label for="status">Trạng thái</label>
                    <select id="status" class="select2 form-control" name="status">
                        @foreach($statuses as $status)
                        <?php 
                        $selected = '';
                        if ($status == $notification->status) {
											$selected = 'selected';
										}
                        ?>
                        <option value="{{ $status }}" {{$selected}}>{{ $status }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <input type="hidden" name="notification_id" value="{{ $notification->id }}">
                <a class="btn btn-default" href="{{ route('admin.notification.index') }} ">Hủy</a>
                <button type="submit" class="btn btn-info float-right">Cập nhập</button>
            </div>
        </form>
    </div>
</div>
    <input type="hidden" id="page" value="edit_notication">
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        CKEDITOR.replace( 'content');
    })
</script>
@endsection