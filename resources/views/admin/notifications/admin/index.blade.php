@extends('layouts.app')
@section('title')
Danh sách thông báo
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> List notifications</li>
@endsection
@section('content')
<div class="row">
    <div data-v-a16ce4fa="" class="col-md-4">
        <a class="btn btn-primary" href="{{ route('admin.notification.create') }}">Tạo thông báo</a>
    </div>
</div>

@if(session("success"))
<div class="alert alert-success alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("success")}}</span>
</div>
@endif

<div class="card" style="margin-top: 20px">
    <div class="card-header">
        {{-- <h3 class="card-title">Bảng danh thông báo</h3> --}}
        <div class="card-tools float-left">
            <div class="input-group input-group-sm" style="width: 180px;">
                <button id="check_delete" type="submit" class="btn btn-sm btn-default check_delete"><i class="fa fa-trash" aria-hidden="true"></i> Xóa mục đã chọn</button>
            </div>
        </div>
        <div class="card-tools">
        <form method="get">
                <div class="input-group input-group-sm" style="width: 170px;">
                    <input type="search" name="keyword" class="form-control float-right"
                        placeholder="Tìm thông báo ..."
                        value="{{ !empty($_GET['keyword']) ? $_GET['keyword'] : '' }}">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-default "><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>
                        <div>
                            <input class="check_all" id="check_all" type="checkbox" value="1"> All
                        </div>
                    </th>
                    <th>ID</th>
                    <th>Thông báo</th>
                    <th>Thời gian</th>
                    <th>Trạng thái</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if ($notifications->count() > 0)
                    @foreach($notifications as $notification)
                    <tr data-row-id="{{ $notification->id }}">
                        <td>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="check_id"
                                    value="{{ $notification->id }}">
                            </div>
                        </td>
                        <td>{{ $notification->id }}</td>
                        <td>{{ $notification->name }}</td>
                        <td>{{ date('H:i d/m/Y', strtotime($notification->created_at)) }}</td>
                        <td>{{ $notification->status }}</td>
                        <td>
                            <a class="btn btn-primary" href="{{ route( 'admin.notification.edit', ['id' => $notification->id] ) }}" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
                            <a class="btn btn-danger delete-notification" href="javascript:void(0)" data-id="{{ $notification->id }}" data-toggle="tooltip" title="Delete"><i class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <td colspan="6" class="text-center text-danger">
                        Không tìm thấy từ khóa {{ $search }}
                    </td>
                @endif
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
            <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                {{ $notifications->links() }}
            </div>
        </ul>
    </div>
    <!-- /.card-footer -->
</div>
    <input type="hidden" id="page" value="list_notication">
@endsection

@section('scripts')
<script>
    //Ajax xóa đơn lẻ từng row
    $(function(){
         $('.delete-notification').click(function (e) {
            var r = confirm("Bạn muốn xóa thông báo này?");
            if (r == true) {
                var obj = $(this);
                var id = obj.attr('data-id');

                $.ajax({
                    type: "post",
                    url: "{{ route('admin.notification.delete') }}",
                    dataType: 'json',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": id
                    },
                    beforeSend: function () {

                    },
                    success: function (data) {
                        alert(data.message);
                        if (data.status) {
                            obj.closest('tr').remove();                          
                        }
                    }, 
                    error: function () {
                        alert(data.message);
                    }
                });
            }
        });

        //Click để ẩn và hiện các row 
        $("#check_all").click(function(){
            $('input:checkbox').prop('checked', this.checked);         
        });


        // Hiện button danger
        $('input:checkbox').change(function () {
            var check = $('.form-check-input:checked');
            if (check.length > 0) {
                $("#check_delete").addClass('btn-danger').removeClass('btn-default');
            } else if(check.length == 0){
                $("#check_delete").addClass('btn-default').removeClass('btn-danger');
            }
        });



        //Gửi ajax lên serverver để xóa các row được chọn
        $('.check_delete').on('click', function () {
            var r = confirm("Bạn muốn xóa các mục đã chọn?");
            if (r == true) {
                var array = [];
                var value = $('.form-check-input:checked');
                if (value.length > 0) {
                $(value).each(function () {
                    array.push($(this).val());
                })
                
                } else {
                alert('Chưa chọn mục cần xóa');
                }
                // console.log(array);
                $.ajax({
                    type: "post",
                    url: "{{ route('admin.notification.multidelete') }}",
                    dataType: 'json',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": array
                    },
                    success: function (data) {
                        alert(data.message);              
                    }, 
                    error: function () {
                        alert(data.message);
                    }
            });

            //Hiển thị lại bảng sau khi xóa row
            $.each(array, function( index, value ) {
				  $('table tr').filter("[data-row-id='" + value + "']").remove();
			    });
            }
        });

    });


</script>
@endSection