@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-credit-card"></i> Quản lý giao dịch MoMo
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
<li class="breadcrumb-item active">MoMo</li>
@endsection
@section('content')
<div class="col-md-12 detail-course-finish">
    @if(session("success"))
    <div class="bg-success">
        <p class="text-light">{{session("success")}}</p>
    </div>
    @elseif(session("fails"))
    <div class="bg-danger">
        <p class="text-light">{{session("fails")}}</p>
    </div>
    @endif
</div>
<div class="container-fluid">
  <!-- Timelime example  -->
  <div class="row">
      <div class="col-md-12">
          <div class="fillter_momo mt-4 mb-4">
              <button type="button" class="btn btn-primary btn_filter" name="button"><i class="fas fa-clipboard-list"></i> Lọc</button>
          </div>
          <div class="card">
             <div class="card-body table-responsive p-0">
               <table class="table table-hover">
                  <thead>
                    <th>Thời gian</th>
                    <th>Số điện thoại</th>
                    <th>Số tiền</th>
                    <th>Nội dung</th>
                    <th>Mã GD MoMo</th>
                    <th>Hành động</th>
                  </thead>
                  <tbody>
                    @if( $list_momos->count() > 0 )
                      @foreach ( $list_momos as $momo )
                        <tr>
                          <td>{{ date('H:m:s d-m-Y', strtotime($momo->created_at)) }}</td>
                          <td>{{ $momo->number_phone }}</td>
                          <td>{!! number_format( $momo->amount ,0,",",".") !!} VNĐ</td>
                          <td>{{ $momo->content }}</td>
                          <td>{{ $momo->id_momo }}</td>
                          <td><a href="{{ route('admin.momo.edit', $momo->id_momo) }}" class="btn btn-warning text-white" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa lại giao dịch MoMo và xác nhận lại thanh toán"><i class="fas fa-edit"></i></a></td>
                        </tr>
                      @endforeach
                    @else
                      <td class="text-center text-danger" colspan="6">Không có giao dịch momo trong dữ liệu!</td>
                    @endif
                  </tbody>
                  <tfoot>
                    <td class="text-center link-right" colspan="6">{{ $list_momos->links() }}</td>
                  </tfoot>
               </table>
             </div>
          </div>
      </div>
  </div>
</div>
<input type="hidden" id="type" data-type="">
<input type="hidden" id="page" value="list_momos">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('/js/admin_momo.js') }}"></script>
@endsection
