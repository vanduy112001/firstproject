@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-credit-card"></i> Quản lý giao dịch MoMo
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">MoMo</li>
    <li class="breadcrumb-item active">Đang nhập</li>
@endsection
@section('content')
<div class="col-md-12 detail-course-finish">
    @if(session("success"))
    <div class="bg-success">
        <p class="text-light">{{session("success")}}</p>
    </div>
    @elseif(session("fails"))
    <div class="bg-danger">
        <p class="text-light">{{session("fails")}}</p>
    </div>
    @endif
</div>
<div class="container-fluid">
  <!-- Timelime example  -->
  <div class="row">
      <div class="col-md-12">
        <div class="login-box text-center" style="margin: auto;">
          <div class="card">
            <div class="card-body login-card-body">
              <p class="login-box-msg">Đăng nhập để tiếp tục</p>
              @if(session("success"))
                  <p class="text-danger">{{session("success")}}</p>
              @elseif(session("fails"))
                  <p class="text-danger">{{session("fails")}}</p>
              @endif
              <form action="{{ route('admin.momo.login') }}" method="post">
                @csrf
                <div class="input-group mb-3">
                  <input type="user" name="user_login_momo" class="form-control" placeholder="Tài khoản">
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-envelope"></span>
                    </div>
                  </div>
                </div>
                <div class="input-group mb-3">
                  <input type="password" name="passwd_login_momo" class="form-control" placeholder="Mật khẩu">
                  <div class="input-group-append">
                    <div class="input-group-text">
                      <span class="fas fa-lock"></span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-12">
                    <button type="submit" class="btn btn-primary btn-block">Đăng nhập</button>
                  </div>
                  <!-- /.col -->
                </div>
              </form>
            </div>
            <!-- /.login-card-body -->
          </div>
        </div>
      </div>
  </div>
</div>
<input type="hidden" id="type" data-type="">
    <input type="hidden" id="page" value="login_momo">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('/js/admin_momo.js') }}"></script>
@endsection
