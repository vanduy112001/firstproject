@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Danh sách hướng dẫn
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Hướng dẫn</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
                <button  id="create_tutorial" class="btn btn-danger">Thêm hướng dẫn</button>
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-bordered">
                <thead class="primary">
                    <th width="80%">Tiêu đề</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                  @if ($tutorials->count() > 0)
                      @foreach($tutorials as $tutorial)
                      <tr>
                        <td>{{ $tutorial->title }}</td>
                        <td>
                          <button type="button" class="btn btn-warning text-light edit_tutorial mr-2" data-toggle="tooltip" data-id="{{ $tutorial->id }}" data-placement="top" title="Chỉnh sửa hướng dẫn"><i class="far fa-edit"></i></button>
                          <button type="button" class="btn btn-danger delete_tutorial" data-id="{{ $tutorial->id }}" data-toggle="tooltip" data-placement="top" title="Xóa hướng dẫn"><i class="far fa-trash-alt"></i></button>
                        </td>
                      </tr>
                      @endforeach
                  @else
                    <td colspan="2" class="text-danger text-center">Không có hướng dẫn nào trong dữ liệu!</td>
                  @endif
                </tbody>
                <tfoot class="card-footer clearfix" id="event_pagination">
                    <td colspan="2" class="text-right">{{ $tutorials->links() }}</td>
                </tfoot>
            </table>
        </div>
    </div>
</div>
{{-- Modal xoa vps --}}
<div class="modal fade" id="delete_vps">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication_invoice" class="text-center">

            </div>
            <form id="form_group_tutorial" action="" method="post">
                @csrf
                <div class="form-group">
                    <label for="title">Tiêu đề</label>
                    <input type="text" id="title" name="title" value="" class="form-control">
                </div>
                <div class="form-group">
                    <label for="link">Liên kết hướng dẫn</label>
                    <input type="text" id="link" name="link" value="" class="form-control">
                </div>
                <div class="form-group">
                    <label for="description">Ghi chú</label>
                    <textarea name="description" class="form-control" id="description"  rows="4" cols="20"></textarea>
                </div>
                <div class="hidden">
                    <input type="hidden" name="action" value="create" id="create">
                    <input type="hidden" name="tutorial_id" value="" id="tutorial_id">
                </div>
            </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button-submit">Xác nhận</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
{{-- Modal xoa vps --}}
<div class="modal fade" id="delete_tutorial">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa hướng dẫn</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication_tutorial" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button-delete">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
<input type="hidden" id="page" value="tutorial">
@endsection
@section('scripts')
<script src="{{ asset('/libraries/bootstrap/js/bootstrap.bundle.min.js') }}" defer></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('/libraries/sweetalert2/sweetalert2.min.js') }}" defer></script>
<script src="{{ asset('/libraries/toastr/toastr.min.js') }}" defer></script>
<script src="{{ asset('js/admin_tutorial.js') }}"></script>
@endsection
