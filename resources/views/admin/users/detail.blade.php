@extends('layouts.app')
@section('style')
<!-- <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"> -->
<link rel="stylesheet" href="{{ asset('/libraries/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
<link rel="stylesheet" href="{{ asset('/libraries/toastr/toastr.min.css')}}">
@endsection
@section('title')
Client Profile
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item"><a href="{{ route('admin.user.list') }}"><i class="fa fa-users" aria-hidden="true"></i>
        List users</a></li>
<li class="breadcrumb-item active"><i class="fa fa-user" aria-hidden="true"></i> User detail</li>
@endsection
@section('content')
<div class="col-md-12 detail-course-finish">
    @if(session("success"))
        <div class="bg-success">
            <p class="text-light">{{session("success")}}</p>
        </div>
    @elseif(session("fails"))
        <div class="bg-danger">
            <p class="text-light">{{session("fails")}}</p>
        </div>
    @endif
</div>
<div class="col-md-12" style="margin:auto">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title text-center">Thông tin khách hàng</h3>
        </div>
        <div class="card-body">

            <div class="tab-content">
                <div id="client_information" class="tab-pane in active">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <div class="card card-widget widget-user">
                                <!-- Add the bg color to the header using any of the bg-* classes -->
                                <div class="widget-user-header bg-info">
                                    <h3 class="widget-user-username">{{ $user->name }}</h3>
                                    <h5 class="widget-user-desc"><a class="text-white" href="tel:{{ $user->user_meta->phone }}">{{ $user->user_meta->phone }}</a></h5>
                                </div>
                                <div class="widget-user-image">
                                    <img class="img-circle elevation-2" src="{{ !empty($user->meta_user->avatar) ? $user->meta_user->avatar : url('/images/avatar5.png') }}"
                                        alt="User Avatar">
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                        <div class="col-12 border-bottom">
                                            <div class="description-block text-left">
                                                <h5 class="description-header">Giới tính</h5>
                                                <span class="description-text text-lowercase">{{ $user->user_meta->gender }}</span>
                                            </div>
                                            <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-12 border-bottom">
                                            <div class="description-block text-left">
                                                <h5 class="description-header">Email</h5>
                                                <span class="description-text text-lowercase"><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></span>
                                            </div>
                                            <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <div class="col-12  border-bottom">
                                            <div class="description-block text-left">
                                                <h5 class="description-header">Đại lý</h5>
                                                <span class="">{{ !empty( $user->group_user->name ) ? $user->group_user->name : 'Cơ bản'  }}</span>
                                            </div>
                                            <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <!-- /.col -->
                                        <div class="col-12  border-bottom">
                                            <div class="description-block text-left">
                                                <h5 class="description-header">Loại</h5>
                                                <span class="">{{ !empty( $user->enterprise ) ? 'Doanh nghiệp' : 'Cá nhân'  }}</span>
                                            </div>
                                            <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                        <!-- /.col -->
                                        <div class="col-12">
                                            <div class="description-block text-left">
                                                <h5 class="description-header">Địa chỉ</h5>
                                                <span class="description-text text-lowercase">{{ $user->user_meta->address }}</span>
                                            </div>
                                            <!-- /.description-block -->
                                        </div>
                                        <!-- /.col -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-8">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-maroon elevation-1"><i class="fas fa-dollar-sign"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Số dư tài khoản</span>
                                            <span class="info-box-number form-edit-credit">
                                                {!! !empty($user->credit->value) ? number_format($user->credit->value,0,",",".") : '0 ' !!}
                                                <small class="ml-2">VNĐ</small>
                                                <span class="ml-2">
                                                  <!-- edit-credit  -->
                                                  <a href="{{ route('admin.user.edit_credit', $user->id) }}" class="text-secondary" data-credit="{{ $user->credit->value }}" data-id="{{ $user->id }}" data-toggle="tooltip" title="Chỉnh sửa số dư tài khoản"> <i class="fas fa-edit"></i> </a>
                                                </span>
                                            </span>
                                        </div>
                                        <!-- /.Thông tin số dư -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-purple elevation-1"><i class="fas fa-donate nav-icon"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Tổng tiền đã nạp</span>
                                            <span class="info-box-number form-edit-credit-total">
                                                {!! !empty($user->credit) ? number_format($user->credit->total,0,",",".") : '0' !!}
                                                <small class="ml-2">VNĐ</small>
                                                <span class="ml-2">
                                                  <a href="#" class="edit-credit-total text-secondary" data-credit="{{ $user->credit->value }}" data-id="{{ $user->id }}" data-toggle="tooltip" title="Thay đổi tổng tiền đã nạp"> <i class="fas fa-edit"></i> </a>
                                                </span>
                                            </span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-info elevation-1"><i class="fas fa-cubes"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Tổng số dịch vụ/sản phẩm</span>
                                            <span class="info-box-number">
                                                {{ $total_hosting + $total_vps + $servers->total() }}
                                                <small></small>
                                            </span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-success elevation-1"><i class="fas fa-cubes"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Đang sử dụng</span>
                                            <span class="info-box-number">
                                                {{ $hostingsUsed + $vpsListUsed + $serversUsed }}
                                                <small></small>
                                            </span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-cubes"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Hết hạn</span>
                                            <span class="info-box-number">
                                                {{ $hostingsExpire + $vpsExpire }}
                                                <small></small>
                                            </span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-cubes"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Đã xóa/hủy/chuyển</span>
                                            <span class="info-box-number">
                                                {{ $vpsDelete + $hostingDelete }}
                                                <small></small>
                                            </span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-danger elevation-1"><i class="far fa-credit-card"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Tạo nạp tiền tài khoản</span>
                                            <span class="info-box-number" id="payment_credit_total">
                                                <span class="ml-2">
                                                  <a href="#" class="payment-credit-total text-secondary" data-id="{{ $user->id }}"> <i class="fas fa-edit"></i> </a>
                                                </span>
                                            </span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-orange elevation-1"><i class="fas fa-donate nav-icon"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Cloudzone Point</span>
                                            <span class="info-box-number" id="edit_cloudzone_point">
                                                {{ !empty($user->user_meta->point) ? $user->user_meta->point : '0' }}
                                                <small class="ml-2">Điểm</small>
                                                <span class="ml-2">
                                                  <a href="#" class="edit_cloudzone_point text-secondary" data-id="{{ $user->id }}" data-toggle="tooltip"> <i class="fas fa-edit"></i> </a>
                                                </span>
                                            </span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="info-box">
                                        <span class="info-box-icon bg-info elevation-1"><i class="far fa-id-card nav-icon"></i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">{{ !empty( $user->enterprise ) ? 'Mã số thuế' : 'Số CMND'  }}</span>
                                            <span class="info-box-number" id="edit_cloudzone_point">
                                                @if(!empty( $user->enterprise ))
                                                    {{ !empty($user->user_meta->mst) ? $user->user_meta->mst : 'Chưa cập nhật' }}
                                                @else
                                                    {{ !empty($user->user_meta->cmnd) ? $user->user_meta->cmnd : 'Chưa cập nhật' }}
                                                @endif
                                            </span>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                            </div>
                        </div>

                    </div>
                    <hr>
                    <a href="/admin/users/login-user/{{$user->id}}"  class="btn btn-outline-danger">Login as user</a>
                    <a href="{{ route('admin.user.edit',['id' => $user->id]) }}" class="btn btn-info">Thông tin khách hàng</a>
                    <!-- Button trigger modal -->
                    <a href="{{ route('admin.user.send_mail_with_user', $user->id) }}" class="btn btn-success">Gửi email</a>
                    <button type="button" name="button" class="btn btn-warning button_active_user" data-id="{{ $user->id }}" @if(!empty($user->email_verified_at)) data-type="inactive" @else data-type="active" @endif >
                        @if(!empty($user->email_verified_at))
                          Tắt kích hoạt
                        @else
                          Kích hoạt
                        @endif
                    </button>
                    <!-- lịch sử giao dịch -->
                    <a href="{{ route('admin.user.history_pay', $user->id) }}" class="btn btn-purple">Lịch sử giao dịch</a>
                    <!-- lịch sử order -->
                    <a href="{{ route('admin.user.history_order', $user->id) }}" class="btn btn-default">Lịch sử đặt hàng</a>
                    {{-- tạo báo giá --}}
                    <a href="{{ route('admin.user.history_quotation', $user->id) }}" class="btn btn-primary">Lịch sử báo giá</a>
                    {{-- Hợp đồng --}}
                    <a href="{{ route('admin.user_contract', ['id' => $user->id]) }}" class="btn btn-default">Hợp đồng</a>
                </div>
            </div>
        </div>
    </div>

    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title text-center">Services information</h3>
        </div>
        <div class="card-body">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="service2-tab" data-toggle="tab" href="#service2" role="tab"
                        aria-controls="profile" aria-selected="false">VPS VN</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="service4-tab" data-toggle="tab" href="#service4" role="tab"
                        aria-controls="vps_us" aria-selected="false">VPS US</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="service1-tab" data-toggle="tab" href="#service1" role="tab"
                        aria-controls="home" aria-selected="true">Hosting</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="service3-tab" data-toggle="tab" href="#service3" role="tab"
                        aria-controls="contact" aria-selected="false">Server</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="service2" role="tabpanel" aria-labelledby="service2-tab">
                    <table class="table table-hover" id="list_vps">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Product</th>
                                <th>IP</th>
                                <th>Loại</th>
                                <th>Chi phí</th>
                                <th>Cấu hình</th>
                                <th>Chu kỳ</th>
                                <th>Ngày tạo</th>
                                <th>Ngày hết hạn</th>
                                <th class="filter_status" data-action="on" data-toggle="tooltip" title="Lọc trạng thái VPS">Trạng thái</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
                <div class="tab-pane fade" id="service4" role="tabpanel" aria-labelledby="service2-tab">
                    <table class="table table-hover" id="list_vps_us">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Product</th>
                                <th>IP</th>
                                <th>Loại</th>
                                <th>Chi phí</th>
                                <th>Cấu hình</th>
                                <th>Chu kỳ</th>
                                <th>Ngày tạo</th>
                                <th>Ngày hết hạn</th>
                                <th class="filter_status_us" data-action="on" data-toggle="tooltip" title="Lọc trạng thái VPS US">
                                  Trạng thái
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
                <div class="tab-pane fade" id="service1" role="tabpanel" aria-labelledby="service1-tab">

                    <table class="table table-hover" id="hosting_table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Product</th>
                                <th>Domain</th>
                                <th>Chu kỳ</th>
                                <th>Ngày tạo</th>
                                <th>Ngày hết hạn</th>
                                <th>Trạng thái</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
                <div class="tab-pane fade" id="service3" role="tabpanel" aria-labelledby="service3-tab">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Order ID</th>
                                <th>Product</th>
                                <th>Kiểu</th>
                                <th>IP</th>
                                <th>OS</th>
                                <th>Billing cycle</th>
                                <th>Ngày hết hạn</th>
                                <th>Trạng thái thanh toán</th>
                                <th>Trạng thái</th>
                                <th>Ngày tạo</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($servers))
                            @foreach($servers as $server)
                            <tr data-row-id="{{ $user->id }}">
                                <td>{{$server->id}} </td>
                                <td>
                                    {{ $server->order_id }}
                                </td>
                                <td>{{$server->product_id}} </td>
                                <td>{{$server->type}} </td>
                                <td>{{$server->ip}} </td>
                                <td>{{$server->os}} </td>
                                <td>{{$server->billing_cycle}} </td>
                                <td>
                                    @if(!empty($server->next_due_date))
                                    {{date('h:i:s d-m-Y', strtotime($server->next_due_date))}}
                                    @else
                                    {{--<b class="maudo"></b>--}}
                                    @endif

                                </td>
                                <td>{{$server->paid}} </td>
                                <td>{{$server->status}} </td>

                                <td>@if(!empty($server->created_at))
                                    {{date('h:i:s d-m-Y', strtotime($server->created_at))}}
                                    @else
                                    <b class="maudo">Không có ngày tạo</b>
                                    @endif</td>
                                <td>
                            </tr>
                            @endforeach
                            @else
                            <td colspan="10" class="text-danger text-center">Dữ liệu rỗng</td>
                            @endif
                        </tbody>
                    </table>
                    <ul class="pagination pagination-sm m-0 float-right">
                        <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                            {{ $servers->links() }}
                        </div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!-- Modal -->
<div class="modal fade" id="modalSendEmailProfile" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="sendMailUser">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Send email</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Title email</label>
                        <input type="text" name="subject" class="form-control">
                    </div>

                    <div class="form-group">
                        <label>Content email</label>
                        <textarea class="form-control" name="content" rows="5"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close
                    </button>
                    <button type="submit" class="btn btn-primary">Send</button>
                </div>
            </div>

            {{ csrf_field() }}
            <input type="hidden" name="email" value="{{ $user->email }}">
        </form>
    </div>
</div>
{{-- Modal xoa invoice --}}
<div class="modal fade" id="active_user">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title-active">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-user" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-active" value="Xóa đơn hàng">
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="user_id" value="{{ $user->id }}">
<input type="hidden" id="page" value="detail_user">
@section('scripts')
<script src="{{ asset('/libraries/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('/libraries/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('js/admin_edit_user.js') }}"></script>
<script>
    $(function () {
            $('.sendMailUser').submit(function (e) {
                e.preventDefault();
                var obj = $(this);

                $.ajax({
                    type: "post",
                    url: "{{ route('admin.user.sendMail') }}",
                    dataType: 'json',
                    data: obj.serialize(),
                    beforeSend: function () {

                    },
                    success: function (data) {
                        if (data.status) {
                            alert(data.message);
                            $('#modalSendEmailProfile').modal('hide');
                        } else {
                            alert(data.message);
                        }
                    }, errors: function () {
                        alert('Có lỗi xảy ra, vui lòng reload và thử lại.');
                    }
                });
            });
        });
</script>
@endSection
