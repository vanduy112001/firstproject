@extends('layouts.app')
@section('style')
<link href="{{ asset('/libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('libraries/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('title')
    <i class="nav-icon  fas fa-shopping-cart"></i> Quản lý đơn đặt hàng
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Đơn đặt hàng</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-6">
                <a href="{{ route('admin.orders.create') }}" class="btn btn-primary">Tạo đơn đặt hàng</a>
            </div>
            <!-- form search -->
            <div class="col-md-6">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("error"))
                    <div class="bg-danger">
                        <p class="text-light">{!! session("error") !!}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table id="table_order" class="table table-hover table-bordered">
                <thead class="primary">
                    <th>ID</th>
                    <th>Ngày đặt hàng</th>
                    <th>Total</th>
                    <th>Chi tiết</th>
                    <th>Trạng thái</th>
                    {{-- <th>Hành động</th> --}}
                </thead>
                <tbody>
                </tbody>
                <tfoot class="card-footer clearfix">
                    <td colspan="7" class="text-center">
                    </td>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<input type="hidden" id="user_id" value="{{ $id }}">
<input type="hidden" id="page" value="history_order_user">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('js/order.js') }}"></script>
<script>
    $(document).ready(function () {
      loadOrder();
      function loadOrder() {
        var user_id = $('#user_id').val();
        $.ajax({
          url: '/admin/orders/filter_user',
          data: {user_id: user_id, status: 'all'},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              // console.log(data);
              if (data.data != '') {
                  var html = '';
                  $.each(data.data, function(index , invoice) {
                      html += '<tr>';
                      if (invoice.deeta != 0) {
                        html += '<td><a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="detail_invoice" data-id="'+ invoice.id_invoice +'">'+ invoice.id +'</a></td>';
                      } else {
                        html += '<td>'+ invoice.id +'</td>';
                      }
                      html += '<td>'+ invoice.date_create +'</td>';
                      html += '<td>'+ invoice.total +'</td>';
                      html += '<td>' + invoice.text_type_order + '</td>';
                      html += '<td>';
                      if (invoice.status == 'Finish') {
                        html += '<span class="text-success">Đã thanh toán</span>';
                      }
                      else if (invoice.status == 'Pending') {
                        html += '<span class="text-danger">Chưa thanh toán</span>';
                      }
                      else if (invoice.status == 'Active') {
                        html += '<span class="text-info">Đang chờ tạo</span>';
                      }
                      else {
                        html += '<span class="text-default">Đã hủy</span>';
                      }
                      html += '</td>';
                      // html += '<td class="button-action">';
                      // if (invoice.deeta != 0) {
                      //   html += '<a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="btn btn-warning text-white detail_invoice" data-id="'+ invoice.id_invoice +'"><i class="fas fa-edit"></i></a>';
                      // }
                      // html += '<button class="btn btn-danger btn-delete-invoices" data-id="' + invoice.id + '"><i class="fas fa-trash-alt"></i></button>';
                      // html += '</td>';
                      html += '</tr>';
                  })
                  $('tbody').html(html);
                  $('tfoot').html('');
                  // phân trang cho vps
                  var total = data.total;
                  var per_page = data.perPage;
                  var current_page = data.current_page;
                  var html_page = '';
                    if (total > per_page) {
                      if ( total / per_page > 11 ) {
                        var page = parseInt(total/per_page + 1);
                        html_page += '<td colspan="11" class="text-center link-right">';
                        html_page += '<nav>';
                        html_page += '<ul class="pagination user_invoice_paginate">';
                        if (current_page != 1) {
                          html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                        } else {
                          html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                        }
                        if (current_page < 7) {
                          for (var i = 1; i < 9; i++) {
                            var active = '';
                            if (i == current_page) {
                              active = 'active';
                            }
                            if (active == 'active') {
                              html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            } else {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }
                          }
                          html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                          for (var i = page - 1; i <= page; i++) {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                          }
                        }
                        else if (current_page >= 7 || current_page >= page - 7) {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                            html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                            for (var i = current_page - 3; i <= current_page +3; i++) {
                              var active = '';
                              if (i == current_page) {
                                active = 'active';
                              }
                              if (active == 'active') {
                                html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                              } else {
                                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                              }
                            }
                            html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                            for (var i = page - 1; i <= page; i++) {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }
                        }
                        else if (current_page >= page - 6) {
                          html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                          html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                          html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                          for (var i = page - 6; i < page; i++) {
                            var active = '';
                            if (i == current_page) {
                              active = 'active';
                            }
                            if (active == 'active') {
                              html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            } else {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }
                          }
                        }

                        if (current_page != page.toPrecision(1)) {
                          html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                        } else {
                          html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                        }
                        html_page += '</ul>';
                        html_page += '</nav>';
                        html_page += '</td>';
                      } else {
                        var page = total/per_page + 1;
                        html_page += '<td colspan="11" class="text-center link-right">';
                        html_page += '<nav>';
                        html_page += '<ul class="pagination user_invoice_paginate">';
                        if (current_page != 1) {
                            html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                        } else {
                            html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                        }
                        for (var i = 1; i < page; i++) {
                            var active = '';
                            if (i == current_page) {
                                active = 'active';
                            }
                            if (active == 'active') {
                              html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            } else {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }

                        }
                        if (current_page != page.toPrecision(1)) {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                        } else {
                            html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                        }
                        html_page += '</ul>';
                        html_page += '</nav>';
                        html_page += '</td>';
                      }
                  }
                  $('tfoot').html(html_page);
              } else {
                  var html = '<td colspan="9" class="text-center">Không có đơn hàng của khách hàng này trong dữ liệu!</td>';
                  $('tbody').html(html);
                  $('tfoot').html('');
              }
          },
          error: function (e) {
              console.log(e);
              var html = '<td colspan="10" class="text-center">Truy vấn đơn hàng lỗi!</td>';
              $('tbody').html(html);
          }
        })
      }

      $(document).on('click', '.user_invoice_paginate a', function(event) {
        event.preventDefault();
        /* Act on the event */
        var user_id = $('#user_id').val();
        var page = $(this).attr('data-page');
        $.ajax({
          url: '/admin/orders/filter_user',
          data: {user_id: user_id, status: 'all', page: page},
          dataType: 'json',
          beforeSend: function(){
              var html = '<td colspan="9" class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></td>';
              $('tbody').html(html);
          },
          success: function (data) {
              // console.log(data);
              if (data.data != '') {
                  var html = '';
                  $.each(data.data, function(index , invoice) {
                      html += '<tr>';
                      if (invoice.deeta != 0) {
                        html += '<td><a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="detail_invoice" data-id="'+ invoice.id_invoice +'">'+ invoice.id +'</a></td>';
                      } else {
                        html += '<td>'+ invoice.id +'</td>';
                      }
                      html += '<td>'+ invoice.date_create +'</td>';
                      html += '<td>'+ invoice.total +'</td>';
                      html += '<td>' + invoice.text_type_order + '</td>';
                      html += '<td>';
                      if (invoice.status == 'Finish') {
                        html += '<span class="text-success">Đã thanh toán</span>';
                      }
                      else if (invoice.status == 'Pending') {
                        html += '<span class="text-danger">Chưa thanh toán</span>';
                      }
                      else if (invoice.status == 'Active') {
                        html += '<span class="text-info">Đang chờ tạo</span>';
                      }
                      else {
                        html += '<span class="text-default">Đã hủy</span>';
                      }
                      html += '</td>';
                      // html += '<td class="button-action">';
                      // if (invoice.deeta != 0) {
                      //   html += '<a href="/admin/invoices/edit/' + invoice.id_invoice + '" class="btn btn-warning text-white detail_invoice" data-id="'+ invoice.id_invoice +'"><i class="fas fa-edit"></i></a>';
                      // }
                      // html += '<button class="btn btn-danger btn-delete-invoices" data-id="' + invoice.id + '"><i class="fas fa-trash-alt"></i></button>';
                      // html += '</td>';
                      html += '</tr>';
                  })
                  $('tbody').html(html);
                  $('tfoot').html('');
                  // phân trang cho vps
                  var total = data.total;
                  var per_page = data.perPage;
                  var current_page = data.current_page;
                  var html_page = '';
                    if (total > per_page) {
                      if ( total / per_page > 11 ) {
                        var page = parseInt(total/per_page + 1);
                        html_page += '<td colspan="11" class="text-center link-right">';
                        html_page += '<nav>';
                        html_page += '<ul class="pagination user_invoice_paginate">';
                        if (current_page != 1) {
                          html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                        } else {
                          html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                        }
                        if (current_page < 7) {
                          for (var i = 1; i < 9; i++) {
                            var active = '';
                            if (i == current_page) {
                              active = 'active';
                            }
                            if (active == 'active') {
                              html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            } else {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }
                          }
                          html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                          for (var i = page - 1; i <= page; i++) {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                          }
                        }
                        else if (current_page >= 7 || current_page >= page - 7) {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                            html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                            for (var i = current_page - 3; i <= current_page +3; i++) {
                              var active = '';
                              if (i == current_page) {
                                active = 'active';
                              }
                              if (active == 'active') {
                                html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                              } else {
                                html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                              }
                            }
                            html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                            for (var i = page - 1; i <= page; i++) {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }
                        }
                        else if (current_page >= page - 6) {
                          html_page += '<li class="page-item"><a href="#" class="page-link" data-page="1" >1</a></li>';
                          html_page += '<li class="page-item"><a href="#" class="page-link" data-page="2" >2</a></li>';
                          html_page += '<li class="page-item disabled" aria-disabled="true"><span class="page-link">...</span></li>';
                          for (var i = page - 6; i < page; i++) {
                            var active = '';
                            if (i == current_page) {
                              active = 'active';
                            }
                            if (active == 'active') {
                              html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            } else {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }
                          }
                        }

                        if (current_page != page.toPrecision(1)) {
                          html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                        } else {
                          html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                        }
                        html_page += '</ul>';
                        html_page += '</nav>';
                        html_page += '</td>';
                      } else {
                        var page = total/per_page + 1;
                        html_page += '<td colspan="11" class="text-center link-right">';
                        html_page += '<nav>';
                        html_page += '<ul class="pagination user_invoice_paginate">';
                        if (current_page != 1) {
                            html_page += '<li class="prev"><a href="#" data-page="'+(current_page-1)+'" class="page-link">&laquo</a></li>';
                        } else {
                            html_page += '<li class="page-item disabled"><a href="#" class="page-link">&laquo</a></li>';
                        }
                        for (var i = 1; i < page; i++) {
                            var active = '';
                            if (i == current_page) {
                                active = 'active';
                            }
                            if (active == 'active') {
                              html_page += '<li class="page-item active disabled"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            } else {
                              html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+i+'" >'+i+'</a></li>';
                            }

                        }
                        if (current_page != page.toPrecision(1)) {
                            html_page += '<li class="page-item"><a href="#" class="page-link" data-page="'+current_page+'">&raquo;</a></li>';
                        } else {
                            html_page += '<li  class="page-item disabled"><a href="#" class="page-link">&raquo;</a></li>';
                        }
                        html_page += '</ul>';
                        html_page += '</nav>';
                        html_page += '</td>';
                      }
                  }
                  $('tfoot').html(html_page);
              } else {
                  var html = '<td colspan="9" class="text-center">Không có đơn hàng của khách hàng này trong dữ liệu!</td>';
                  $('tbody').html(html);
                  $('tfoot').html('');
              }
          },
          error: function (e) {
              console.log(e);
              var html = '<td colspan="10" class="text-center">Truy vấn đơn hàng lỗi!</td>';
              $('tbody').html(html);
          }
        })
      });

    });
</script>
@endsection
