@extends('layouts.app')
@section('style')
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
Sửa thông tin người dùng
@endsection
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item"><a href="{{ route('admin.user.list') }}"><i class="fa fa-users" aria-hidden="true"></i> List users</a></li>
<li class="breadcrumb-item active"><i class="fa fa-user" aria-hidden="true"></i> Edit users</li>
@endsection
<div class="col-md-6" style="margin:auto">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title text-center">Sửa thông tin người dùng</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @if ($errors->any())
        <div class="alert alert-danger" style="margin-top:20px">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(session("error"))
        <div class="bg-success">
            <p class="text-light">{{session("error")}}</p>
        </div>
        @endif
        <form role="form" method="post" action="{{ route('admin.user.update', ['id' => $user->id]) }}" autocomplete="off">
            {{ csrf_field() }}
            <div class="card-body">
                <div class="form-group">
                    <label for="name">Tên</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Tên"
                        value="{{ old('name') ? old('name') : $user->name }}">
                </div>
                <div class="form-group">
                    <label for="gender">Giới tính</label>
                    <select class="form-control" name="gender">
                        <option value="nam" selected="">Nam</option>
                        <option value="nữ">Nữ</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="address">Địa chỉ</label>
                    <input type="text" name="address" class="form-control" id="address" placeholder="Địa chỉ"
                        value="{{ old('address') ? old('address') : $user->user_meta->address }}">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Email"
                        value="{{ old('email') ? old('email')  : $user->email }}">
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input type="text" name="phone" class="form-control" id="phone" placeholder="Phone"
                        value="{{ old('phone') ? old('phone') : $user->user_meta->phone }}">
                </div>
                <div class="form-group">
                    <label for="company">Công ty / Tổ chức</label>
                    <input type="text" name="company" class="form-control" id="company" placeholder="Công ty / Tổ chức" value="{{ $user->user_meta->company }}">
                </div>
                <div class="form-group">
                    <label for="abbreviation_name">Tên viết tắt</label>
                    <input type="text" name="abbreviation_name" class="form-control" id="abbreviation_name" placeholder="Tên viết tắt (cá nhân, công ty, tổ chức)" value="{{ old('abbreviation_name') ? old('abbreviation_name') : $user->user_meta->abbreviation_name }}">
                </div>
                <div class="form-group">
                    @php
                    $role = !empty(old('role')) ? old('role') : $user->user_meta->role;
                    @endphp
                    <label>User role</label>
                    <select class="form-control" name="role">
                        <option value="admin" {{ $role == "admin" ? "selected" : '' }}>Admin</option>
                        <option value="editor" {{ $role == "editor" ? "selected" : '' }}>Editor
                        </option>
                        <option value="user" {{ (empty($role) || $role == "user" ) ? "selected" : '' }}>
                            User
                        </option>
                    </select>
                </div>
                <div class="form-group">
                    <label>Nhóm đại lý</label>
                    <select class="form-control" name="group_user_id">
                        <option value="0">Cơ bản</option>
                        @foreach($group_users as $group_user)
                          @php
                              $selected = '';
                              if($group_user->id == $user->group_user_id) {
                                  $selected = 'selected';
                              }
                          @endphp
                          <option value="{{ $group_user->id }}" {{$selected}}>{{ $group_user->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                  <label for="enterprise">Doanh nghiệp</label>
                  <div class="col-md-8 ml-2 icheck-primary d-inline">
                      <input type="checkbox" class="custom-control-input" name="enterprise" id="enterprise" @if( !empty($user->enterprise) ) checked @endif  value="1">
                      <label for="enterprise">
                      </label>
                  </div>
                </div>
                <div class="form-group" id="mst">
                   @if( !empty($user->enterprise) )
                      <label for="imst">Mã số thuế</label>
                      <input type="text" name="mst" value="{{ $user->user_meta->mst }}" class="form-control" placeholder="Mã số thuế">
                   @endif
                </div>
                <div class="form-group">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="changePass" value="1" class="changePass">
                            Change password
                        </label>
                    </div>

                    <div class="ctn-change-pass" style="display: none">
                        <label>New Password (*)</label>
                        <input type="password" name="new_password" autocomplete="new-password"
                            class="form-control @error('new_password') is-invalid @enderror" {{ old('new_password') }}>
                    </div>
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <input type="hidden" name="user_id" value="{{ $user->id }}">
                <a class="btn btn-default" href="{{ route('admin.user.list') }} ">Hủy</a>
                <button type="submit" class="btn btn-info float-right">Sửa</button>
            </div>
        </form>
    </div>
</div>
<input type="hidden" id="page" value="edit_user">
@endsection

@section('scripts')
<script>
    $(function(){
        $('.changePass').click(function () {
            var obj = $(this);
            if (obj.is(':checked')) {
                $('.ctn-change-pass').show()
            } else {
                $('.ctn-change-pass').hide()
            }
        });
        $('#enterprise').on('click', function () {
            if ( $(this).is(':checked') ) {
              var html = '';
              html += '<label for="imst">Mã số thuế</label>';
              html += '<input type="text" name="mst" value="" class="form-control" placeholder="Mã số thuế">';
              $('#mst').html(html);
            } else {
              $('#mst').html('');
            }
        })
    });

</script>
@endSection
