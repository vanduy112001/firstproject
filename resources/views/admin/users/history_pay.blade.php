@extends('layouts.app')
@section('title')
Lịch sử thanh toán
@endsection
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> List users</li>
<li class="breadcrumb-item active"> Lịch sử đặt hàng</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                      <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Tìm kiếm mã giao dịch" id="search">
                        <div class="input-group-append">
                          <button type="submit" class="btn btn-danger"><i class="fas fa-search"></i>
                          </button>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-bordered">
                <thead class="primary">
                    <th>Mã giao dịch</th>
                    <th>Loại</th>
                    <th>Hình thức</th>
                    <th>Dịch vụ</th>
                    <th>Số tiền</th>
                    <th>Số dư trước</th>
                    <th>Số dư sau</th>
                    <th>Ngày thanh toán</th>
                    <th>Trạng thái</th>
                </thead>
                <tbody>
                @foreach ($payments as $payment)
                    <tr>
                        <td>{{ $payment->ma_gd }}</td>
                        <td>
                            @if ($payment->method_gd == 'invoice')
                              @if(!empty($pay_in[$payment->method_gd_invoice]))
                                {{$pay_in[$payment->method_gd_invoice]}}
                              @else
                                @if ($payment->status == 'Active')
                                  Admin xác nhận
                                @else
                                  <span class="text-danger">Chưa thanh toán</span>
                                @endif
                              @endif
                            @else
                              @if(!empty($pay_in[$payment->method_gd]))
                                {{-- {{$pay_in[$payment->method_gd]}} --}}
                                Vietcombank
                              @else
                                Thanh toán
                              @endif
                            @endif
                        </td>
                        <td>
                          @if ( $payment->type_gd == 2 )
                            Thanh toán {{ !empty($payment->detail_order->type) ? $payment->detail_order->type : "" }}
                          @elseif ( $payment->type_gd == 3 )
                            Gia hạn {{ !empty($payment->detail_order->type) ? $payment->detail_order->type : "" }}
                          @else
                            {{ $type_payin[$payment->type_gd] }}
                          @endif
                        </td>
                        <td>
                          {{-- List service --}}
                          @if ( $payment->type_gd != 1 && $payment->type_gd != 99 && $payment->type_gd != 98 )
                            <button type="button" class="btn btn-outline-success collapsed tooggle-plus" data-toggle="collapse" data-target="#service{{ $payment->id }}" data-placement="top" title="" data-original-title="Chi tiết" aria-expanded="false">
                              <i class="fas fa-plus"></i>
                            </button>
                            <div id="service{{ $payment->id }}" class="mt-4 collapse" style="">
                              @if ( $payment->type_gd == 2 || $payment->type_gd == 6 )
                                @if ( !empty($payment->detail_order->type) )
                                  @if ( 
                                    $payment->detail_order->type == 'VPS' || $payment->detail_order->type == 'VPS-US' ||
                                    $payment->detail_order->type == 'VPS US' || $payment->detail_order->type == 'NAT-VPS'
                                  )
                                    @foreach ( $payment->detail_order->vps as $vps )
                                      @if ( !empty($vps->ip) )
                                        {{ $vps->ip }} <br>
                                      @else
                                        <span class="text-danger">Đang chờ tạo</span> <br>
                                      @endif
                                    @endforeach
                                  @elseif ( $payment->detail_order->type == 'Proxy' )
                                    @foreach ( $payment->detail_order->proxies as $proxy )
                                      @if ( !empty($proxy->ip) )
                                        {{ $proxy->ip }} <br>
                                      @else
                                        <span class="text-danger">Đang chờ tạo</span> <br>
                                      @endif
                                    @endforeach
                                  @elseif ( $payment->detail_order->type == 'Server' )
                                    @foreach ( $payment->detail_order->servers as $server )
                                      @if ( !empty($server->ip) )
                                        {{ $server->ip }} <br>
                                      @else
                                        <span class="text-danger">Đang chờ tạo</span> <br>
                                      @endif
                                    @endforeach
                                  @elseif ( $payment->detail_order->type == 'Colocation' )
                                    @foreach ( $payment->detail_order->colocations as $colocation )
                                      @if ( !empty($colocation->ip) )
                                        {{ $colocation->ip }} <br>
                                      @else
                                        <span class="text-danger">Đang chờ tạo</span> <br>
                                      @endif
                                    @endforeach
                                  @elseif ( $payment->detail_order->type == 'Hosting' )
                                    @foreach ( $payment->detail_order->hostings as $hosting )
                                      @if( !empty($hosting->domain) )
                                        {{ $hosting->domain }} <br>
                                      @else
                                        <span class="text-danger">Đã xóa</span> <br>
                                      @endif
                                    @endforeach
                                  @elseif ( $payment->detail_order->type == 'Domain' )
                                    @foreach ( $payment->detail_order->domains as $domain )
                                      @if( !empty($domain->domain) )
                                        {{ $domain->domain }} <br>
                                      @else
                                        <span class="text-danger">Đã xóa</span> <br>
                                      @endif
                                    @endforeach
                                  @endif    
                                @endif
                              @elseif ( $payment->type_gd == 3 || $payment->type_gd == 7 )
                                @if ( !empty($payment->detail_order->type) )
                                  @if ( 
                                    $payment->detail_order->type == 'VPS' || $payment->detail_order->type == 'VPS-US' ||
                                    $payment->detail_order->type == 'VPS US' || $payment->detail_order->type == 'NAT-VPS'
                                  )
                                    @if ( !empty($payment->detail_order->order_expireds) )
                                      @foreach ( $payment->detail_order->order_expireds as $order_expired )
                                        @if ( !empty( $order_expired->vps->ip ) )
                                          {{ $order_expired->vps->ip }} <br>
                                        @else
                                          <span class="text-danger">Đã xóa</span> <br>
                                        @endif
                                      @endforeach
                                    @endif
                                  @elseif ( $payment->detail_order->type == 'Proxy' )
                                    @if ( !empty($payment->detail_order->order_expireds) )
                                      @foreach ( $payment->detail_order->order_expireds as $order_expired )
                                        @if ( !empty( $order_expired->proxy->ip ) )
                                          {{ $order_expired->proxy->ip }} <br>
                                        @else
                                          <span class="text-danger">Đã xóa</span> <br>
                                        @endif
                                      @endforeach
                                    @endif
                                  @elseif ( $payment->detail_order->type == 'Server' )
                                    @if ( !empty($payment->detail_order->order_expireds) )
                                      @foreach ( $payment->detail_order->order_expireds as $order_expired )
                                        @if ( !empty( $order_expired->server->ip ) )
                                          {{ $order_expired->server->ip }} <br>
                                        @else
                                          <span class="text-danger">Đã xóa</span> <br>
                                        @endif
                                      @endforeach
                                    @endif
                                  @elseif ( $payment->detail_order->type == 'Colocation' )
                                    @if ( !empty($payment->detail_order->order_expireds) )
                                      @foreach ( $payment->detail_order->order_expireds as $order_expired )
                                        @if ( !empty( $order_expired->colocation->ip ) )
                                          {{ $order_expired->colocation->ip }} <br>
                                        @else
                                          <span class="text-danger">Đã xóa</span> <br>
                                        @endif
                                      @endforeach
                                    @endif
                                  @elseif ( $payment->detail_order->type == 'hosting' )
                                    @if ( !empty($payment->detail_order->order_expireds) )
                                      @foreach ( $payment->detail_order->order_expireds as $order_expired )
                                        @if ( !empty( $order_expired->hosting->domain ) )
                                          {{ $order_expired->hosting->domain }}
                                        @else
                                          <span class="text-danger">Đã xóa</span> <br>
                                        @endif
                                      @endforeach
                                    @endif
                                  @elseif ( $payment->detail_order->type == 'Domain' )
                                    @if ( !empty($payment->detail_order->order_expireds) )
                                      @foreach ( $payment->detail_order->order_expireds as $order_expired )
                                        @if ( !empty( $order_expired->domain->domain ) )
                                          {{ $order_expired->domain->domain }}
                                        @else
                                          <span class="text-danger">Đã xóa</span> <br>
                                        @endif
                                      @endforeach
                                    @endif
                                  @endif
                                @endif
                              @elseif ( $payment->type_gd == 4 )
                                @if ( !empty($payment->detail_order) )
                                  @if ($payment->detail_order->order_addon_vps)
                                    @foreach ($payment->detail_order->order_addon_vps as $order_addon_vps)
                                      @if ( !empty($order_addon_vps->vps->ip) )
                                        {{ $order_addon_vps->vps->ip }} <br>
                                      @else
                                        <span class="text-danger">Đã xóa</span> <br> 
                                      @endif
                                    @endforeach
                                  @endif
                                @endif
                              @elseif ( $payment->type_gd == 5 )
                                @if ( !empty($payment->detail_order) )
                                  @if ( !empty($payment->detail_order->order_change_vps) )
                                    @foreach ($payment->detail_order->order_change_vps as $order_change_vps)
                                      @if ( !empty($order_change_vps->vps->ip) )
                                        {{ $order_change_vps->vps->ip }} <br>
                                      @else
                                        <span class="text-danger">Đã xóa</span> <br> 
                                      @endif
                                    @endforeach
                                  @endif
                                @endif
                              @elseif ( $payment->type_gd == 8 )
                                @if ( !empty($payment->detail_order) )
                                  @if ( !empty($payment->detail_order->order_upgrade_hosting->hosting) )
                                    {{ $payment->detail_order->order_upgrade_hosting->hosting }}
                                  @else
                                    <span class="text-danger">Đã xóa</span>
                                  @endif
                                @endif
                              @endif
                            </div>
                          @endif
                        </td>
                        <td>
                            @php $class = '';  @endphp
                            @if( $payment->type_gd == 1 || $payment->type_gd == 98 )
                                <strong class=" text-success">+</strong>
                                @php $class = 'text-success'; @endphp
                            @else
                                <strong class="text-danger">-</strong>
                                @php $class = 'text-danger'; @endphp
                            @endif
                            <span class="{{$class}}">{!! !empty($payment->money) ? number_format($payment->money,0,",",".") : 0 !!} VNĐ</span>
                        </td>
                        <td>
                            @if ($payment->status == 'Active')
                              @if ( $payment->type_gd == 1 || $payment->type_gd == 98 || $payment->type_gd == 99 )
                                @if ( !empty($payment->log_payment->before) )
                                  {!!number_format($payment->log_payment->before,0,",",".") . ' VNĐ'!!}
                                @else
                                  0 VNĐ
                                @endif
                              @else
                                @if ( !empty($payment->log_payment->before) )
                                  {!!number_format($payment->log_payment->before,0,",",".") . ' VNĐ'!!}
                                @else
                                  0 VNĐ
                                @endif
                              @endif
                            @endif
                        </td>
                        <td>
                            @if ($payment->status == 'Active')
                              @if ( $payment->type_gd == 1 || $payment->type_gd == 98 || $payment->type_gd == 99 )
                                @if ( !empty($payment->log_payment->after) )
                                  {!!number_format($payment->log_payment->after,0,",",".") . ' VNĐ'!!}
                                @else
                                  0 VNĐ
                                @endif
                              @else
                                @if ( !empty($payment->log_payment->after) )
                                  {!!number_format($payment->log_payment->after,0,",",".") . ' VNĐ'!!}
                                @else
                                  0 VNĐ
                                @endif
                              @endif
                            @endif
                        </td>
                        <td>{{ date('d-m-Y', strtotime($payment->date_gd)) }}</td>
                        <td>
                            @if($payment->status == 'Active')
                                <span class="text-success">Đã thanh toán</span>
                            @elseif ($payment->status == 'Pending')
                                <span class="text-danger">Chưa thanh toán</span>
                            @elseif ($payment->status == 'confirm')
                                <span class="text-info">Đã yêu cầu</span>
                            @else
                                @php
                                  $content = '';
                                  if(!empty($payment->content)) {
                                      $content = unserialize($payment->content);
                                  }
                                @endphp
                                @if(!empty($payment->content))
                                  <span class="text-danger">Thanh toán lỗi - <a href="#{{ !empty($content['errorCode'])?$content['errorCode'] : ''  }}" class="text-danger"><b>{{ !empty($content['errorCode'])?$content['errorCode'] : '' }}</b></a></span>
                                @else
                                  <span class="text-danger">Chưa thanh toán</span>
                                @endif
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot class="card-footer clearfix">
                <td colspan="9" class="text-center">
                    {{ $payments->links()  }}
                </td>
                </tfoot>
            </table>
        </div>
    </div>
</div>
<input type="hidden" id="user_id" value="{{ $id }}">
<input type="hidden" id="page" value="history_pay_user">
@endSection
@section('scripts')
<script src="{{ asset('js/admin_history_pay.js') }}"></script>
@endSection
