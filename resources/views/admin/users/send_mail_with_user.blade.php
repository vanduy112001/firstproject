@extends('layouts.app')
@section('style')
<!-- <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet"> -->
<link rel="stylesheet" href="{{ asset('/libraries/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
<link rel="stylesheet" href="{{ asset('/libraries/toastr/toastr.min.css')}}">
@endsection
@section('title')
Client Profile
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item"><a href="{{ route('admin.user.list') }}"><i class="fa fa-users" aria-hidden="true"></i>
        List users</a></li>
<li class="breadcrumb-item active"><i class="fa fa-user" aria-hidden="true"></i> Send mail user</li>
@endsection
@section('content')
<div class="col-md-12 detail-course-finish">
    @if(session("success"))
        <div class="bg-success">
            <p class="text-light">{{session("success")}}</p>
        </div>
    @elseif(session("fails"))
        <div class="bg-danger">
            <p class="text-light">{{session("fails")}}</p>
        </div>
    @endif
</div>
<div class="col-md-12" style="margin:auto">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title text-center">Gửi mail cho khách hàng</h3>
        </div>
        <div class="card-body">
            <form action="{{ route('admin.user.form_send_mail') }}" method="post">
                @csrf
                <input type="hidden" name="user_id" value="{{ $user->id }}">
                <div class="form-group">
                    <input class="form-control" value="To: {{ $user->email }}" disabled>
                </div>
                <div class="form-group">
                    <input class="form-control" name="subject" value="{{ old('subject') }}" required placeholder="Subject:">
                </div>
                <div class="form-group">
                    <textarea name="content" id="content" cols="30" rows="10" class="form-control">{{ old('content') }}</textarea>
                </div>
                <div class="card-footer">
                <div class="float-right">
                    <button type="submit" class="btn btn-primary"><i class="far fa-envelope"></i> Send</button>
                </div>
                <a href="{{ route('admin.user.detail', $user->id) }}" class="btn btn-default"><i class="fas fa-times"></i> Hủy</a>
              </div>
            </form>
        </div>
    </div>
</div>
<input type="hidden" id="page" value="send_mail_with_user">
@endsection

@section('scripts')
<script src="{{ asset('/libraries/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('/libraries/toastr/toastr.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        CKEDITOR.replace( 'content');
    })
</script>
@endSection
