@extends('layouts.app_mail')
@section('content')
    {{ $content }}
@endsection
@section('footer')
    <div style="text-align: center;line-height: 20px; font-size: 16px;font-family: Aria;color: rgb(16, 55, 132);">
        Email này được gửi đến {{ $name }} bởi vì bạn đã đăng ký tài khoản với Cloudzone. <br>
        Các đường link trong email này đều dẫn đến trang <a href="http://cloudzone.vn" style="text-decoration: none;">cloudzone.vn</a>.
    </div>
@endsection
