@extends('layouts.app')
@section('style')
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item"><a href="{{ route('admin.user.list') }}"><i class="fa fa-users" aria-hidden="true"></i> List users</a></li>
<li class="breadcrumb-item active"><i class="fa fa-user" aria-hidden="true"></i> Chỉnh sửa số dư tài khoản</li>
@endsection
<div class="col-md-6" style="margin:auto">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title text-center">Chỉnh sửa số dư tài khoản</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @if ($errors->any())
        <div class="alert alert-danger" style="margin-top:20px">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
        @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
        @endif
        @if(session("error"))
        <div class="bg-success">
            <p class="text-light">{{session("error")}}</p>
        </div>
        @endif
        <form role="form" method="post" action="{{ route('admin.user.update_credit', $user->id) }}" id="form_select">
            {{ csrf_field() }}
            <div class="card-body">
                <div class="form-group">
                    <label for="user_id">Tên khách hàng</label>
                    <input type="text" id="user_id" class="form-control" value="{{ $user->name . ' - ' . $user->email }}" disabled="">
                </div>
                <div class="form-group">
                    <label for="amount">Số dư tài khoản</label>
                    @php
                        $credit = !empty($user->credit->value) ? number_format($user->credit->value,0,",",".") : '0 ';
                    @endphp
                    <input type="text" class="form-control" value="{{ $credit }} VNĐ" disabled="">
                </div>
                <div class="form-group">
                    <label for="method_gd">Hình thức</label>
                    <select class="form-control" name="method_pay" id="method_pay">
                        <option value="plus" selected>Cộng</option>
                        <option value="minus">Trừ</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="amount">Số tiền</label>
                    <input type="text" id="amount" class="form-control" name="amount">
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <a class="btn btn-default" href="{{ route('admin.user.list') }} ">Hủy</a>
                <button type="submit" class="btn btn-info float-right">Tạo</button>
            </div>
        </form>
    </div>
</div>
<input type="hidden" id="page" value="edit_credit_user">
@endsection
@section('scripts')
    <script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
    <script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
    <script type="text/javascript">
      $(document).ready(function() {
        //Date picker
        $('#datepicker').datepicker({
          autoclose: true
        });
            // select
            $('.select2').select2();
        $('#amount').on('keyup', function () {
            var amount = $(this).val();
            amount = amount.replace('.', '');
            amount = amount.replace('.', '');
            amount = amount.replace(',', '');
            $(this).val(  addCommas(amount) );
        })
        function addCommas(nStr)
        {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        }
      });
    </script>
@endsection
