@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
<i class="nav-icon fas fa-cubes"></i> Thông tin đại lý {{ $detail->name }}
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item"><a href="{{ route('admin.admin_roles.index') }}">Quản lý quản trị viên</a></li>
    <li class="breadcrumb-item active">{{ $detail->name }}</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<!-- nhóm khách hàng -->
<button type="button" name="button" id="add_user" class="btn btn-info mb-4">Thêm tài khoản</button>
<div class="card card-primary mb-4">
    <div class="card-header">
        <h3 class="card-title">Thông tin khách hàng trong {{ $detail->name }}</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
          </button>
        </div>
    </div>
    <div class="card-body" id="users" style="display: block;">
        <table class="table table-hover table-bordered">
            <thead class="">
                <th>Tên khách hàng</th>
                <th>Email</th>
                <th>Hành động</th>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>
<!-- nhóm khách hàng -->
<button type="button" name="button" id="add_group_product" class="btn btn-warning mt-4 mb-4">Đặc quyền</button>
<div class="card card-success">
    <div class="card-header">
        <h3 class="card-title">Đặc quyền trong nhóm {{ $detail->name }}</h3>
        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
          </button>
        </div>
    </div>
    <div class="card-body" id="group_products" style="display: block;">
          <div class="form-group row"> 
              <div class="col-md-2 text-right">
                <label>Tìm kiếm</label>
              </div>
              <div class="col-md-10">
                <select class="selectPage" id="selectPage" style="width: 50%" placeholder="Chọn trang cần tìm kiếm">
                  <option value="" selected>Chọn trang cần tìm kiếm</option>
                  @foreach( $role_config as $key => $role )
                    <option value="{{ $key }}">{{ $role }}</option>
                  @endforeach
                </select>
              </div>
          </div>
          <table class="table table-hover table-bordered">
              <thead class="">
                  <th>Trang</th>
                  <th>Hành động</th>
              </thead>
              <tbody>
              </tbody>
          </table>
    </div>
</div>
{{-- Modal --}}
<div class="modal fade" id="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa tài khoản khách hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication_modal">
            </div>
            <form id="form-group-product">
                @csrf
                <div class="form-group" id="change_group">
                </div>
                <div class="hidden">
                    <input type="hidden" id="id-group" name="id" value="{{ $detail->id }}">
                    <input type="text" id="type" name="type" value="">
                </div>
            </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="buttonSubmit">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="buttonFinish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
{{-- Modal --}}
<div class="modal fade" id="modalAll">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa nhóm tài khoản khách hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="noticationModalAll">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="buttonSubmitAll">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="buttonFinishAll" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<input type="hidden" id="page" value="detail_role">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('/js/detailAdminRole.js') }}"></script>
@endsection