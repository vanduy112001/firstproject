@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    <i class="nav-icon fas fa-tachometer-alt"></i> Quản lý quản trị viên
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Trang chủ</a></li>
    <li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Quản lý quản trị viên</li>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <button class="btn btn-primary" id="createGroup">Tạo nhóm quản trị</button>
    </div>
</div>
<div class="title">
    <div class="title-body">
        <div class="row">
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light p-4" style="font-size: 20px;">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light p-4" style="font-size: 20px;">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="card" style="margin-top: 20px">
    <!-- /.card-header -->
    <div class="card-body table-responsive">
        <div class="row ml-3">
            <div class="col-md-2">
                <div class="form-group">
                    <span class="mt-1">Số lượng</span>
                    <select id="qtt">
                        <option value="20">20</option>
                        <option value="30">30</option>
                        <option value="40">40</option>
                        <option value="50">50</option>
                        <option value="60">100</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6"></div>
            <div class="col-md-4">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Tìm kiếm tên" id="search">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-danger"><i class="fas fa-search"></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table id="data-user-table" class="table table-bordered table-hover">
            <thead class="primary">
                <tr>
                    <th> ID</th>
                    <th>Tên nhóm</th>
                    <th>Mô tả</th>
                    <th>Hành động</th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>

            </tfoot>
        </table>
    </div>
</div>
{{-- Modal --}}
<div class="modal fade" id="modal">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa tài khoản khách hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication_modal">
            </div>
            <form id="formAction">
                @csrf
                <div class="form-group">
                    <label for="nameGroup">Tên nhóm khách hàng</label>
                    <input type="text" name="name" value="" id="nameGroup" class="form-control" placeholder="Tên nhóm khách hàng">
                    <input type="hidden" name="id" id="idGroup" value="">
                    <input type="hidden" name="action" id="action" value="">
                </div>
                <div class="form-group">
                    <label for="description">Mô tả</label>
                    <textarea name="description" id="description" class="form-control" rows="3"></textarea>
                </div>
            </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="buttonSubmit">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="buttonFinish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
{{-- Modal --}}
<div class="modal fade" id="modalAll">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa nhóm tài khoản khách hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="noticationModalAll">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="buttonSubmitAll">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="buttonFinishAll" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<input type="hidden" id="type" value="id">
<input type="hidden" id="sort" value="desc">
<input type="hidden" id="page" value="list_role">
@endsection
@section('scripts')
    <script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
    <script src="{{ asset('/js/admin_role.js') }}"></script>
@endsection