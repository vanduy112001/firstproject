@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Danh sách mã khuyến mãi
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Mã khuyến mãi</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
                <a href="{{ route('admin.promotions.create') }}" class="btn btn-danger">Thêm mã khuyến mãi</a>
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-bordered">
                <thead class="primary">
                    <th>Tiêu đề</th>
                    <th>Mã</th>
                    <th>Loại</th>
                    <th>Giá trị</th>
                    <th>Ngày bắt đầu</th>
                    <th>Ngày kết thúc</th>
                    <th>Số lượng</th>
                    <th>Đã sữ dụng</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                  @if ($promotions->count() > 0)
                      @foreach($promotions as $promotion)
                      <tr>
                        <td>{{ $promotion->name }}</td>
                        <td>{{ $promotion->code }}</td>
                        <td>{{ !empty($type_promotion[$promotion->type]) ? $type_promotion[$promotion->type] : 'Không có' }}</td>
                        <td>{{ $promotion->value }}</td>
                        <td>{{ date('d-m-Y', strtotime($promotion->start_date)) }}</td>
                        <td>{{ date('d-m-Y', strtotime($promotion->end_date)) }}</td>
                        <td>{{ $promotion->max_uses }}</td>
                        <td>{{ !empty($promotion->uses) ? $promotion->uses : 0 }}</td>
                        <td class="button-action">
                          <a href="{{ route('admin.promotion.edit', $promotion->id) }}" class="btn btn-warning text-light mr-2" data-toggle="tooltip" data-placement="top" title="Chỉnh sửa hướng dẫn"><i class="far fa-edit"></i></a>
                          <button type="button" class="btn btn-danger delete_promotion" data-name="{{ $promotion->name }}" data-id="{{ $promotion->id }}" data-toggle="tooltip" data-placement="top" title="Xóa mã khuyến mãi"><i class="far fa-trash-alt"></i></button>
                        </td>
                      </tr>
                      @endforeach
                  @else
                    <td colspan="9" class="text-danger text-center">Không có mã khuyến mãi nào trong dữ liệu!</td>
                  @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
{{-- Modal xoa vps --}}
<div class="modal fade" id="delete_vps">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication_invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button_submit">Xác nhận</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal" id="button_finish">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
<input type="hidden" id="page" value="list_promotion">
@endsection
@section('scripts')
  <script type="text/javascript">
    $(document).ready(function () {
      $('.delete_promotion').on('click', function () {
          $('tr').removeClass('choose-row');
          var id = $(this).attr('data-id');
          var name = $(this).attr('data-name');
          $(this).closest('tr').addClass('choose-row');
          $('#button_submit').fadeIn();
          $('#button_finish').fadeOut();
          $('#delete_vps').modal('show');
          var html = '';
          html = '<span>Bạn có muốn xóa mã khuyến mãi <b class="text-danger">' + name + ' (ID: '+ id +')</b> này không?</span>';
          $('#notication_invoice').html(html);
          $('.modal-title').text('Xóa mã khuyến mãi');
          $('#button_submit').attr('data-id', id);
      })
      $('#button_submit').on('click', function () {
          var id = $(this).attr('data-id');
          $.ajax({
            url: '/admin/khuyen-mai/xoa-khuyen-mai',
            data: { id: id },
            dataType: 'json',
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#button_submit').attr('disabled', true);
                $('#notication_invoice').html(html);
            },
            success: function (data) {
                if (data) {
                  $('#notication_invoice').html('<span class="text-success">Xóa mã khuyến mãi thành công!</span>');
                  $('#button_submit').attr('disabled', false);
                  $('#button_submit').fadeOut();
                  $('#button_finish').fadeIn();
                  $('.choose-row').fadeOut();
                } else {
                  $('#notication_invoice').html('<span class="text-danger">Xóa mã khuyến mãi thất bại!</span>');
                  $('#button_submit').attr('disabled', false);
                }
            },
            error: function (e) {
                console.log(e);
                $('#notication_invoice').html('<span class="text-danger">Truy vấn mã khuyến mãi lỗi!</span>');
                $('#button_submit').attr('disabled', false);
            }
          })
      })
    })
  </script>
@endsection
