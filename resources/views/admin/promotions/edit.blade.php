@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('libraries/lou-multi-select/css/multi-select.css') }}" />
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Quản lý Server
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item "><a href="/admin/vps/">Server</a></li>
    <li class="breadcrumb-item active">Chỉnh sửa Server</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" style="margin-top:20px">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Tạo mã khuyến mãi</h3>
      <!-- /.card-tools -->
    </div>
    <form action="{{ route('admin.promotion.update') }}" method="post">
        @csrf
        <input type="hidden" name="id" value="{{ $detail->id }}">
        <div class="card-body">
              <div class="form-group">
                  <label for="name">Tiêu đề</label>
                  <input type="text" name="name" class="form-control" placeholder="Tiêu đề" value="{{ $detail->name }}">
              </div>
              <div class="form-group ">
                  <label for="code">Mã khuyến mãi</label>
                  <div class="row">
                    <div class="col-9">
                      <input type="text" name="code" id="code" class="form-control" placeholder="Mã khuyến mãi" value="{{ $detail->code }}">
                    </div>
                    <div class="col-3">
                      <button type="button" class="btn btn-success btn_auto_promotion">Tạo mã khuyến mãi</button>
                    </div>
                  </div>
              </div>
              <div class="form-group ">
                  <label for="type">Loại mã khuyến mãi</label>
                  <select class="form-control" name="type">
                      @foreach ( $type_promotion as $key => $type )
                        <?php
                            $selected = '';
                            if ($key == $detail->type ) {
                              $selected = 'selected';
                            }
                        ?>
                        <option value="{{ $key }}" {{ $selected }}>{{ $type }}</option>
                      @endforeach
                  </select>
              </div>
              <div class="form-group">
                  <label for="value">Giá trị</label>
                  <input type="text" name="value" class="form-control" placeholder="Giá trị" value="{{ $detail->value }}">
              </div>
              <div class="form-group">
                  <label for="product">Sản phẩm</label>
                  <select class="select2" name="product[]" multiple="multiple" data-placeholder="Chọn sản phẩm" style="width: 100%;">
                      @foreach ( $group_products as $group_product )
                        <optgroup label="{{ $group_product->name }}">
                            @foreach ( $group_product->products as $product )
                              @php
                                $selected = '';
                                if ( !empty($detail->product_promotions) ) {
                                    foreach ($detail->product_promotions as $product_promotion) {
                                        if ( $product_promotion->product_id == $product->id ) {
                                          $selected = 'selected';
                                        }
                                    }
                                }
                              @endphp
                              <option value="{{ $product->id }}" {{$selected}}>{{ $product->name }}</option>
                            @endforeach
                        </optgroup>
                      @endforeach
                  </select>
              </div>
              <div class="form-group">
                  <label for="billing">Thời gian</label>
                  <select class="form-control" name="billing[]" multiple="multiple" size="10" data-placeholder="Chọn thời gian" style="width: 100%;height: 100%;">
                    @foreach ( $billings as $key => $billing )
                      @php
                        $selected = '';
                        if ( !empty($detail->billing_promotions) ) {
                            foreach ($detail->billing_promotions as $billing_promotion) {
                                if ( $billing_promotion->billing == $key ) {
                                  $selected = 'selected';
                                }
                            }
                        }
                      @endphp
                      <option value="{{ $key }}" {{$selected}}>{{ $billing }}</option>
                    @endforeach
                  </select>
              </div>
              <div class="form-group">
                  <label for="start_date">Ngày bắt đầu</label>
                  <input type="date" name="start_date" class="form-control" placeholder="Ngày bắt đầu" value="{{ $detail->start_date }}">
              </div>
              <div class="form-group">
                  <label for="end_date">Ngày kết thúc</label>
                  <input type="date" name="end_date" class="form-control" placeholder="Ngày kết thúc" value="{{ $detail->start_date }}">
              </div>
              <div class="form-group">
                  <label for="max_uses">Số lần sử dụng</label>
                  <input type="text" name="max_uses" class="form-control" placeholder="Số lượng sử dụng tối đa" value="{{ $detail->max_uses }}">
              </div>
              <div class="form-group row">
                 <div class="col-md-2">
                    <label for="life_time">Lifetime Promotion</label>
                 </div>
                 <div class="col-md-10">
                    <?php
                      $checked = '';
                      if ( $detail->life_time ) {
                        $checked == 'checked';
                      }
                    ?>
                    <input type="checkbox" name="life_time" value="1" {{ $checked }}>
                    <span>Giá khuyến mãi được áp dụng ngay cả khi nâng cấp trong tương lai, tạo mới , gia hạn, v.v;</span>
                 </div>
              </div>
              <div class="form-group row">
                 <div class="col-md-2">
                    <label for="apply_one">Apply Once</label>
                 </div>
                 <div class="col-md-10">
                     <?php
                       $checked = '';
                       if ( $detail->apply_one ) {
                         $checked == 'checked';
                       }
                     ?>
                    <input type="checkbox" name="apply_one" value="1" {{ $checked }}>
                    <span>Chỉ áp dụng mã khuyến mãi cho mỗi khách hàng một lần</span>
                 </div>
              </div>
              <div class="form-group row">
                 <div class="col-md-2">
                    <label for="upgrade">Nâng cấp</label>
                 </div>
                 <div class="col-md-10">
                     <?php
                       $checked = '';
                       if ( $detail->upgrade ) {
                         $checked == 'checked';
                       }
                     ?>
                    <input type="checkbox" name="upgrade" value="1" {{ $checked }}>
                    <span>Áp dụng mã khuyến mãi cho cả đơn hàng nâng cấp</span>
                 </div>
              </div>
              <div class="form-group row">
                 <div class="col-md-2">
                    <label for="type_order">Loại đơn hàng</label>
                 </div>
                 <div class="col-md-10">
                    <select class="form-control" name="type_order">
                      <option value="order" @if($detail->type_order == 'order') selected  @endif>Order</option>
                      <option value="expire" @if($detail->type_order == 'expire') selected  @endif>Gia hạn</option>
                    </select>
                 </div>
              </div>
        </div>
        <div class="card-footer">
          <div class="text-right">
            <input type="submit" value="Chỉnh sửa mã khuyến mãi" class="btn btn-primary">
          </div>
        </div>
    </form>
</div>
<input type="hidden" id="page" value="edit_promotion">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('libraries/lou-multi-select/js/jquery.multi-select.js') }}" defer></script>
<script type="text/javascript">
  $(document).ready(function () {
    $('.select2').select2();
    $('.select3').select2();
    $('.btn_auto_promotion').on('click', function () {
        var length = 12;
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
          retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        $('#code').val(retVal);
    })
  })
</script>
@endsection
