@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('title')
    <i class="fas fa-donate nav-icon"></i> Quản lý tất cả thanh toán
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Thanh toán</li>
@endsection
@section('content')
    <div class="title">
        <div class="title-body">
            <div class="row">
                <!-- button tạo -->
                <div class="col-md-8">
                  <a target="_blank" href="{{ route('admin.orders.create') }}" class="btn btn-primary">Tạo đơn đặt hàng</a>
                  <button type="button" name="button" class="btn btn-info" id="all_type_user">Khách hàng cá nhân</button>
                  <button type="button" name="button" class="btn btn-danger" id="all_type_enterprise">Khách hàng doanh nghiệp</button>
                </div>
                <div class="col-md-4 text-right">
                    <span class="ml-3">Số lượng: </span>
                    <select class="payment_all_classic">
                      <option value="20" selected>20 thanh toán</option>
                      <option value="30">30 thanh toán</option>
                      <option value="40">40 thanh toán</option>
                      <option value="50">50 thanh toán</option>
                      <option value="100">100 thanh toán</option>
                      <option value="200">200 thanh toán</option>
                    </select>
                </div>
                <!-- form search -->
                <div class="col-md-4 mt-4 text-primary text-center">
                    <b>Trang thanh toán {{ $payments->firstItem() }} - {{ $payments->lastItem() }} / {{ $payments->total() }}</b>
                </div>
                <div class="col-md-4"></div>
                <div class="col-md-4 mt-4 link-right paginate_top">
                    {{ $payments->links()  }}
                </div>
                <div class="col-md-12 detail-course-finish">
                    @if(session("success"))
                        <div class="bg-success">
                            <p class="text-light">{{session("success")}}</p>
                        </div>
                    @elseif(session("fails"))
                        <div class="bg-danger">
                            <p class="text-light">{{session("fails")}}</p>
                        </div>
                    @endif
                </div>
                <div class="col-md-12 detail-course-finish">
                    @if(session("success"))
                        <div class="bg-success">
                            <p class="text-light">{{session("success")}}</p>
                        </div>
                    @elseif(session("fails"))
                        <div class="bg-danger">
                            <p class="text-light">{{session("fails")}}</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-body table-responsive">
            <div class="dataTables_wrapper form-inline dt-bootstrap">
                <table class="table table-bordered" id="payment_credit">
                    <thead class="primary">
                        <th>ID</th>
                        <th>Mã giao dịch</th>
                        <th id="user_payment_all">
                            Tên khách hàng
                            <span class="float-right user">
                              <i class="fas fa-filter"></i>
                              <input type="hidden" id="input_filter_user" value="">
                            </span>
                        </th>
                        <th id="type_payment_all">
                            Loại
                            <span class="float-right">
                              <i class="fas fa-filter"></i>
                              <input type="hidden" id="input_filter_type" value="">
                            </span>
                        </th>
                        <th id="method_payment_all">
                            Hình thức
                            <span class="float-right">
                              <i class="fas fa-filter"></i>
                              <input type="hidden" id="input_filter_method" value="">
                            </span>
                        </th>
                        <th>Số tiền</th>
                        <th>Ngày GD</th>
                        <th>Ngày thanh toán</th>
                        <th id="status_payment_all">
                            Trạng thái
                            <span class="float-right">
                              <i class="fas fa-filter"></i>
                              <input type="hidden" id="input_filter_status" value="">
                            </span>
                        </th>
                        <th>Hành động</th>
                    </thead>
                    <tbody>
                    @foreach ($payments as $payment)
                        <tr>
                              <td>{{ $payment->id }}</td>
                              <td>{{ $payment->ma_gd }}</td>
                              <td>
                                  @if(!empty($payment->user->name))
                                    <a href="{{ route('admin.user.detail', $payment->user->id) }}">{{ $payment->user->name }}</a>
                                  @endif
                              </td>
                              <td>
                                  @if ($payment->type_gd == 1)
                                    Nạp tiền vào tài khoản
                                  @else
                                    @if ($payment->type_gd == 2)
                                      Tạo {{ !empty($payment->detail_order->type) ? $payment->detail_order->type : '' }}
                                    @elseif ($payment->type_gd == 3)
                                      Gia hạn {{ !empty($payment->detail_order->type) ? $payment->detail_order->type : '' }}
                                    @elseif ($payment->type_gd == 4)
                                      Addon VPS
                                    @elseif ($payment->type_gd == 5)
                                      Đổi IP
                                    @elseif ($payment->type_gd == 6)
                                      Tạo Domain
                                    @elseif ($payment->type_gd == 7)
                                      Gia hạn Domain
                                    @elseif ($payment->type_gd == 8)
                                      Nâng cấp Hosting
                                    @elseif ($payment->type_gd == 9)
                                      Addon Server
                                    @elseif ($payment->type_gd == 98)
                                      Cộng tiền vào tài khoản
                                    @elseif ($payment->type_gd == 99)
                                      Trừ tiền tài khoản
                                    @endif
                                  @endif
                              </td>
                              <td>
                                @if ($payment->type_gd == 1)
                                  {{ !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd }}
                                @else
                                  @if($payment->status == 'confirm' || $payment->status == 'Active')
                                    {{
                                      !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản'
                                    }}
                                  @else
                                      <span class="text-danger">Chưa thanh toán</span>
                                  @endif
                                @endif
                              </td>
                              <td>{!!number_format($payment->money,0,",",".") . ' VNĐ'!!}</td>
                              <td>{{ date('H:i:s d-m-Y', strtotime($payment->created_at)) }}</td>
                              <td>{{ date('d-m-Y', strtotime($payment->date_gd)) }}</td>
                              <td class="payment-status">
                                  @if($payment->status == 'Active')
                                      <span class="text-success">Đã thanh toán</span>
                                  @elseif($payment->status == 'confirm')
                                      <span class="text-info">Đã xác nhận</span>
                                  @elseif ($payment->status == 'Pending')
                                      <span class="text-danger">Chưa thanh toán</span>
                                  @else
                                      <span class="text-danger">Thanh toán lỗi</span>
                                  @endif
                              </td>
                              <td class="table-button">
                                  @if($payment->status == 'Pending')
                                      <button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="{{ $payment->id }}" data-name="{{ !empty($payment->user->name) ?$payment->user->name : '' }}" data-magd="{{ $payment->ma_gd }}"><i class="fas fa-check"></i></button>
                                  @endif
                                  @if ($payment->type_gd == 1)
                                    @if($payment->status == 'confirm')
                                      <button type="button" data-toggle="tooltip" title="Xác nhận giao dịch" class="btn btn-success confirm-payment" data-id="{{ $payment->id }}" data-name="{{ !empty($payment->user->name) ?$payment->user->name : '' }}" data-magd="{{ $payment->ma_gd }}"><i class="fas fa-check"></i></button>
                                    @endif
                                  @endif
                                  @if($payment->type_gd != 1)
                                    <a href="{{ route('admin.payment.detail', $payment->id) }}" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>
                                  @endif
                                  <a href="" class="btn btn-danger delete-payment" data-id="{{ $payment->id }}" data-name="{{ !empty($payment->user->name) ?$payment->user->name : '' }}"><i class="fas fa-trash-alt"></i></a>
                              </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot class="card-footer clearfix">
                        <td colspan="10" class="text-center link-right">
                            {{ $payments->links()  }}
                        </td>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    {{-- Modal xoa invoice --}}
    <div class="modal fade" id="delete-invoice">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Xóa đơn đặt hàng</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="notication-invoice" class="text-center">

                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" id="button-cancel" data-dismiss="modal">Hủy</button>
                    <input type="submit" class="btn btn-primary" id="button-invoice" value="Xóa giao dịch">
                    <input type="submit" class="btn btn-primary" id="button-confirm" value="Xóa giao dịch">
                    <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <input type="hidden" id="type_user_personal" value="">
    <input type="hidden" id="page" value="list_all_invoice">
@endsection
@section('scripts')
    <script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
    <script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
    <script src="{{ asset('libraries/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('libraries/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/payments.js') }}"></script>
    <script type="text/javascript">
    	$(document).ready(function() {
    		//Date picker
    		$('#datepicker').datepicker({
    			autoclose: true
    		});
            // select
            $('.select2').select2();
    	});
    </script>
@endsection
