@extends('layouts.app')
@section('title')
    <i class="fas fa-donate nav-icon"></i> Quản lý thanh toán
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Thanh toán</li>
@endsection
@section('content')
    <div class="title">
        <div class="title-body">
            <div class="row">
                <!-- button tạo -->
                <div class="col-md-4">
                </div>
                <div class="col-md-4"></div>
                <!-- form search -->
                <div class="col-md-4">
                    <div class="row">
                        <div class="form-group col-md-12">
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 detail-course-finish">
                    @if(session("success"))
                        <div class="bg-success">
                            <p class="text-light">{{session("success")}}</p>
                        </div>
                    @elseif(session("fails"))
                        <div class="bg-danger">
                            <p class="text-light">{{session("fails")}}</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="box-body table-responsive col-md-9" style="margin: auto;">
        <div class="card card-info">
            <div class="card-header">
                <h3 class="card-title payment-detail-title">Thông tin thanh toán của người dùng</h3>
            </div>
            <div class="card-body payment-detail-body">
                <div class="row payment-detail">
                    <div class="col-md-4">
                        Họ và tên
                    </div>
                    <div class="col-md-8">
                        <a href="">{{ $payment->user->name }}</a>
                    </div>
                </div>
                <div class="row payment-detail">
                    <div class="col-md-4">
                        Mã giao dịch
                    </div>
                    <div class="col-md-8">
                        {{ $payment->ma_gd }}
                    </div>
                </div>
                <div class="row payment-detail">
                    <div class="col-md-4">
                        Ngày giao dịch
                    </div>
                    <div class="col-md-8">
                        {{ date('d-m-Y', strtotime($payment->date_gd)) }}
                    </div>
                </div>
                <div class="row payment-detail">
                    <div class="col-md-4">
                        Số tiền giao dịch
                    </div>
                    <div class="col-md-8">
                        {!!number_format($payment->money,0,",",".") . ' VNĐ'!!}
                    </div>
                </div>
                <div class="row payment-detail">
                    <div class="col-md-4">
                        Loại giao dịch
                    </div>
                    <div class="col-md-8">
                        {{ $pay_in[$payment->method_gd] }}
                    </div>
                </div>
                <div class="row payment-detail">
                    <div class="col-md-4">
                        Hình thức giao dịch
                    </div>
                    <div class="col-md-8">
                        {{ $type_payin[$payment->type_gd] }}
                    </div>
                </div>
                <div class="row payment-detail">
                    <div class="col-md-4">
                        Trạng thái
                    </div>
                    <div class="col-md-8">
                        @if($payment->status == 'Active')
                            <span class="text-success">Đã thanh toán</span>
                        @elseif ($payment->status == 'Pending')
                            <span class="text-danger">Chưa thanh toán</span>
                        @else
                            <div class="text-danger">Thanh toán lỗi</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="card card-success">
            <div class="card-header">
                <h3 class="card-title payment-detail-title">Thông tin sản phẩm</h3>
            </div>
            <div class="card-body">
                @php
                  $detail_order = $payment->detail_order;
                @endphp
                @if($detail_order->order->type == 'expired')
                  <table class="table table-hover table-bordered">
                      <thead class="primary">
                          <th>Dịch vụ</th>
                          <th>Ngày đặt hàng</th>
                          <th>Thời gian</th>
                          <th>Giá</th>
                      </thead>
                      <tbody>
                          @if($detail_order->type == 'VPS' || $detail_order->type == 'NAT-VPS' || $detail_order->type == 'VPS US')
                              @php
                                  $order_expireds = $detail_order->order_expireds;
                              @endphp
                              @foreach ($order_expireds as $order_expired)
                                <tr>
                                    <td>
                                        Gia hạn VPS - <a href="{{ route('admin.vps.detail', $order_expired->vps->id) }}">{{$order_expired->vps->ip}}</a>
                                    </td>
                                    <td>{{ date('d-m-Y' , strtotime($order_expired->created_at)) }}</td>
                                    <td>{{ !empty($order_expired->billing_cycle) ? $billings[$order_expired->billing_cycle] : '1 Tháng' }}</td>
                                    <td>{{ $detail_order->sub_total / $detail_order->quantity }}</td>
                              </tr>
                              @endforeach
                          @elseif($detail_order->type == 'Server')
                              @php
                                  $order_expireds = $detail_order->order_expireds;
                              @endphp
                              @foreach ($order_expireds as $order_expired)
                                <tr>
                                    <td>
                                        Gia hạn Server - <a href="{{ route('admin.server.detail', $order_expired->server->id) }}">{{$order_expired->server->ip}}</a>
                                    </td>
                                    <td>{{ date('d-m-Y' , strtotime($order_expired->created_at)) }}</td>
                                    <td>{{ !empty($order_expired->billing_cycle) ? $billings[$order_expired->billing_cycle] : '1 Tháng' }}</td>
                                    <td>{{ $detail_order->sub_total / $detail_order->quantity }}</td>
                                </tr>
                              @endforeach
                          @else
                              @php
                                  $order_expireds = $detail_order->order_expireds;
                              @endphp
                              @foreach ($order_expireds as $order_expired)
                                <tr>
                                    <td>
                                      Gia hạn Hosting - <a href="{{ route('admin.hosting.detail', $order_expired->hosting->id) }}">{{$order_expired->hosting->domain}}</a>
                                    </td>
                                    <td>{{ date('d-m-Y' , strtotime($order_expired->created_at)) }}</td>
                                    <td>{{ !empty($order_expired->billing_cycle) ? $billings[$order_expired->billing_cycle] : '1 Tháng' }}</td>
                                    <td>{{ $detail_order->sub_total / $detail_order->quantity }}</td>
                                </tr>
                              @endforeach
                          @endif
                      </tbody>
                  </table>
                @elseif($detail_order->type == 'addon_vps')
                    <table class="table table-hover table-bordered">
                        <thead class="primary">
                            <th>IP</th>
                            <th>CPU</th>
                            <th>RAM</th>
                            <th>DISK</th>
                            <th>Ngày đặt hàng</th>
                            <th>Thời gian</th>
                            <th>Giá</th>
                        </thead>
                        <tbody>
                            @php
                                $order_addon_vps = $detail_order->order_addon_vps;
                                $add_on_products = GroupProduct::get_addon_product_private($detail_order->user_id);
                                $billing_cycle = 'monthly';
                            @endphp
                            @foreach($order_addon_vps as $addon_vps)
                                <tr>
                                    <td>
                                        Addon VPS - <a href="{{ route('admin.vps.detail', $addon_vps->vps->id) }}">{{$addon_vps->vps->ip}}</a>
                                    </td>
                                    <td>
                                        {{ $addon_vps->cpu }}
                                    </td>
                                    <td>
                                        {{ $addon_vps->ram }}
                                    </td>
                                    <td>
                                        {{ $addon_vps->disk }}
                                    </td>
                                    <td>{{ date('d-m-Y' , strtotime($addon_vps->created_at)) }}</td>
                                    <td>
                                        {{ !empty($addon_vps->month) ? $addon_vps->month . ' Tháng' : '' }} {{ !empty($addon_vps->day) ? $addon_vps->day .' Ngày' : '' }}
                                    </td>
                                    <td>
                                        <?php
                                            $pricing_addon = 0;
                                            foreach ($add_on_products as $key => $add_on_product) {
                                                if (!empty($add_on_product->meta_product->type_addon)) {
                                                    if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                      $pricing_addon += $addon_vps->month * $addon_vps->cpu * $add_on_product->pricing[$billing_cycle] + (int) round($addon_vps->day  * $addon_vps->cpu * $add_on_product->pricing[$billing_cycle] / 30, -3);
                                                    }
                                                }
                                            }
                                            foreach ($add_on_products as $key => $add_on_product) {
                                                if (!empty($add_on_product->meta_product->type_addon)) {
                                                    if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                      $pricing_addon += $addon_vps->month * $addon_vps->ram * $add_on_product->pricing[$billing_cycle] + (int) round($addon_vps->day  * $addon_vps->ram * $add_on_product->pricing[$billing_cycle] / 30, -3);;
                                                    }
                                                }
                                            }
                                            foreach ($add_on_products as $key => $add_on_product) {
                                                if (!empty($add_on_product->meta_product->type_addon)) {
                                                    if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                      $pricing_addon += $addon_vps->month * $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10 + (int) round($addon_vps->day  * $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 30, -3)/10;
                                                    }
                                                }
                                            }
                                        ?>
                                        {!!number_format( $pricing_addon,0,",",".") . ' VNĐ'!!}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                @elseif($detail_order->type == 'Domain')
                    <table class="table table-hover table-bordered">
                    </table>
                @else
                    <table class="table table-hover table-bordered">
                        <thead class="primary">
                            <th>Dịch vụ</th>
                            <th>Ngày bắt đầu</th>
                            <th>Ngày kết thúc</th>
                            <th>Giá</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @if($detail_order->type == 'VPS' || $detail_order->type == 'NAT-VPS')
                                @foreach ($detail_order->vps as $vps)
                                <tr>
                                    <td>{{ $detail_order->type }} - {{$vps->ip}}</td>
                                    <td>{{ date('d-m-Y' , strtotime($vps->created_at)) }}</td>
                                    <td>{{ !empty($vps->next_due_date) ? date('d-m-Y' , strtotime($vps->next_due_date)) : '#' }}</td>
                                    <td>{{ $detail_order->sub_total / $detail_order->quantity }}</td>
                                    <td><button class="btn btn-danger delete-item-invoice" data-id="{{ $vps->id }}" data-name="{{$vps->ip}}" data-type="VPS" data-parent="{{$detail_order->id}}"><i class="fas fa-trash-alt"></i></button></td>
                                </tr>
                                @endforeach
                            @elseif($detail_order->type == 'Server')
                                @foreach ($detail_order->servers as $server)
                                    <tr>
                                        <td>{{ $detail_order->type }} - {{$server->ip}}</td>
                                        <td>{{ date('d-m-Y' , strtotime($server->created_at)) }}</td>
                                        <td>{{ !empty($server->next_due_date) ? date('d-m-Y' , strtotime($server->next_due_date)) : '#' }}</td>
                                        <td>{{ $detail_order->sub_total / $detail_order->quantity }}</td>
                                        <td><button class="btn btn-danger delete-item-invoice" data-name="{{$server->ip}}" data-id="{{ $server->id }}" data-parent="{{$detail_order->id}}" data-type="Server"><i class="fas fa-trash-alt" ></i></button></td>
                                    </tr>
                                @endforeach
                            @else
                                @foreach ($detail_order->hostings as $hosting)
                                    <tr>
                                        <td>{{ $detail_order->type }} - {{$hosting->domain}}</td>
                                        <td>{{ date('d-m-Y' , strtotime($hosting->created_at)) }}</td>
                                        <td>{{ !empty($hosting->next_due_date) ? date('d-m-Y' , strtotime($hosting->next_due_date)) : '#' }}</td>
                                        <td>{{ $detail_order->sub_total / $detail_order->quantity }}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                @endif
            </div>
        </div>
    </div>
    <div class="col col-md-12 text-center">
        <a href="{{ route('admin.home') }}"><i class="fas fa-arrow-left"></i> Trở về trang chủ</a>
    </div>
    <input type="hidden" id="page" value="detail_invoice">
@endsection
@section('scripts')
{{--    <script src="{{ asset('js/payments.js') }}"></script>--}}
@endsection
