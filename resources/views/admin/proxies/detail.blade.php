@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Quản lý Proxy
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item "><a href="/admin/proxy/">Proxy</a></li>
    <li class="breadcrumb-item active">Chỉnh sửa Proxy</li>
@endsection
@section('content')
@php
  $detail_order = $detail->detail_order;
@endphp
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper dt-bootstrap">
            <h4 class="text-center">Thông tin Proxy {{$detail->ip}}</h4>
            <div class="container">
                <form action="{{ route('admin.proxy.updateProxy') }}" method="post">
                  <div class="row">
                    <div class="col col-md-6">
                      <div class="order">
                        <div class="detail_order">
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Tên khách hàng:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="user_id" class="form-control select2">
                                <option value="" disabled>Chọn khách hàng</option>
                                @foreach ($users as $user)
                                  @php
                                    $selected = '';
                                    if ($user->id == $detail->user_id) {
                                      $selected = 'selected';
                                    }
                                  @endphp
                                  <option value="{{$user->id}}" {{$selected}}>{{$user->name}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">IP:</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="ip" class="form-control" value="{{$detail->ip}}">
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Port:</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="port" class="form-control" value="{{$detail->port}}">
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Username:</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="user_name" class="form-control" value="{{$detail->username}}">
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Password:</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="password" class="form-control" value="{{$detail->password}}">
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">VMID:</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="vm_id" class="form-control" value="{{$detail->vm_id}}">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col col-md-6">
                      <div class="order">
                        <div class="detail_order">
                          {{-- status --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Trạng thái của proxy:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="status" id="status" class="select2 form-control" style="width: 100%;" data-value="Hosting">
                                <option value="" disabled>Chọn trạng thái của Server</option>
                                <option value="on" @if( $detail->status == 'on' ) selected @endif>Đang bật</option>
                                <option value="off" @if( $detail->status == 'reset_password' ) selected @endif>Đang đổi mật khẩu</option>
                                <option value="progressing" @if( $detail->status == 'progressing' ) selected @endif>Đang cài đặt</option>
                                <option value="cancel" @if( $detail->status == 'cancel' ) selected @endif>Hủy</option>
                                <option value="expire" @if( $detail->status == 'expire' ) selected @endif>Hết hạn</option>
                                <option value="delete_server" @if( $detail->status == 'delete_server' ) selected @endif>Đã xóa</option>
                                <option value="Pending" @if( $detail->status == 'Pending' ) selected @endif>Chưa tạo</option>
                              </select>
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Ngày tạo :</label>
                            </div>
                            <div class="col col-md-8">
                              @if (!empty($detail->created_at))
                                {{date('d/m/Y', strtotime($detail->created_at))}}
                              @else
                                <p class="text-danger">Chưa tạo</p>
                              @endif
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Ngày hết hạn :</label>
                            </div>
                            <div class="col col-md-8">
                              @if (!empty($detail->next_due_date))
                                {{date('d/m/Y', strtotime($detail->next_due_date))}}
                              @else
                                <p class="text-danger">Chưa tạo</p>
                              @endif
                              <span class="ml-4">
                                <a href="#" class="btn btn-outline-default next_due_date" data-date='{{date('d-m-Y', strtotime($detail->next_due_date))}}'><i class="fas fa-edit"></i></a>
                              </span>
                              <div class="input_next_due_date">
                              </div>
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Ngày thanh toán :</label>
                            </div>
                            <div class="col col-md-8">
                              @if(!empty($detail->paid))
                                @if($detail->paid == 'paid')
                                  <span class="text-success">{{date('m/d/Y', strtotime($detail_order->paid_date))}}</span>
                                @elseif($detail->paid == 'cancel')
                                  <span class="text-secondary">Hủy</span>
                                @else
                                  <span class="text-danger">Chưa thanh toán</span>
                                @endif
                              @else
                                <span class="text-danger">Chưa thanh toán</span>
                              @endif
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Thời gian:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="billing_cycle" id="billing_cycle" class="select2 form-control"  style="width: 100%;">
                                <option value="" disabled>Chọn thời gian</option>
                                @foreach ($billings as $key => $billing)
                                  @php
                                    $selected = '';
                                    if ($key == $detail->billing_cycle) {
                                      $selected = 'selected';
                                    }
                                  @endphp
                                  <option value="{{ $key }}" {{$selected}}>{{ $billing }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Thành tiền :</label>
                            </div>
                            <div class="col col-md-8">
                              @php
                                 $total = 0;
                                 if ( !empty($product->price_override) ) {
                                    $total = $product->price_override;
                                 } else {
                                    $product = $detail->product;
                                    $total = !empty( $product->pricing[$detail->billing_cycle] ) ? $product->pricing[$detail->billing_cycle] : 0;
                                 }
                                @endphp
                              {!!number_format(($total),0,",",".")!!} VNĐ
                              <span class="ml-4">
                                <a href="#" class="btn btn-outline-default price" data-date="{{ $total }}"><i class="fas fa-edit"></i></a>
                              </span>
                              <div class="input_price">
                              </div>
                            </div>
                          </div>
                          <div class="form-button mt-4 mr-4">
                            @csrf
                            <input type="hidden" name="id" value="{{ $detail->id }}">
                            <input type="hidden" name="invoice_id" value="{{ $detail->detail_order_id }}">
                            <input type="submit" value="Cập nhật" class="btn btn-primary float-right">
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- Modal xoa order --}}
<div class="modal fade" id="delete-order">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-order" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-order" value="Xóa đơn hàng">
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="page" value="edit_server">
@endsection
@section('scripts')
<script src="{{ asset('js/edit_invoices.js') }}"></script>
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script type="text/javascript">
	$(document).ready(function() {
		//Date picker
        $('.datepicker').datepicker({
			autoclose: true,
            format: 'dd-mm-yyyy'
		});
        // select
        $('.select2').select2();

        $('.price').on('click', function () {
        var amount = $(this).attr('data-date');
        var html = '<input type="text" value="'+ amount +'" class="form-control" name="price">';
        $('.input_price').html(html);
        })

        $('.next_due_date').on('click', function () {
            var date = $(this).attr('data-date');
            var html = '<input type="text" id="datepicker" name="next_due_date" value="'+ date +'" class="form-control">';
            $('.input_next_due_date').html(html);
            $('#datepicker').datepicker({
                autoclose: true,
                format: 'dd-mm-yyyy'
            });
        })

        $('.date_create').on('click', function () {
            var date = $(this).attr('data-date');
            var html = '<input type="text" id="datepicker2" name="date_create" value="'+ date +'" class="form-control">';
            $('.input_date_create').html(html);
            $('#datepicker2').datepicker({
                autoclose: true,
                format: 'dd-mm-yyyy'
            });
        })

	});
</script>
@endsection
