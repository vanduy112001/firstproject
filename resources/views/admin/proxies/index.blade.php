@extends('layouts.app')
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Danh sách Proxy
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Proxy</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
                <div class="row">
                    <select class="form-control" id="status">
                      <option value="" disabled selected>Chọn trạng thái của Proxy</option>
                      <option value="on">Trạng thái đang bật</option>
                      <option value="off">Trạng thái đã tắt</option>
                      <option value="progressing">Trạng thái đang tạo</option>
                      <option value="rebuild">Trạng thái đang cài lại</option>
                      <option value="change_ip">Trạng thái đang đổi IP</option>
                      <option value="reset_password">Trạng thái đặt lại mật khẩu</option>
                      <option value="expire">Trạng thái hết hạn</option>
                      {{-- <option value="admin_off">Trạng thái Admin tắt</option>
                      <option value="suspend">Trạng thái đã khóa</option> --}}
                      <option value="change_user">Trạng thái đã chuyển khách hàng</option>
                      <option value="delete_vps">Trạng thái đã xóa</option>
                      <option value="cancel">Trạng thái đã hủy</option>
                    </select>
                </div>
            </div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                      <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Tìm kiếm Proxy" id="q">
                        <div class="input-group-append">
                          <button type="submit" class="btn btn-danger"><i class="fas fa-search"></i></button>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="mt-4 pl-4 col-md-12" style="display: inherit;">
                <div class="form-group">
                    <span class="mt-1">Số lượng</span> 
                    <select id="qtt">
                        <option value="10">10</option>
                        <option value="30" selected>30</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="200">200</option>
                        <option value="500">500</option>
                    </select>
                </div>
                <div class="form-group text-primary ml-4">
                    <b>Đã chọn: <span class="last-item">0</span> / <span class="total-item">0</span></b>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-bordered text-center tbl-list">
                <thead class="primary">
                    <th>
                        <input type="checkbox"  class="checkbox_all">
                    </th>
                    <th>Tên khách hàng</th>
                    <th>Sản phẩm</th>
                    <th>IP <br> Port</th>
                    <th>User <br> Password</th>
                    <th>Bang</th>
                    <th>Ngày tạo</th>
                    <th>Ngày kết thúc</th>
                    <th>Tổng thời gian thuê</th>
                    <th>Chu kỳ thanh toán</th>
                    <th>Chi phí</th>
                    <th>Trạng thái</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                    
                </tbody>
                <tfoot class="card-footer clearfix">
                </tfoot>
            </table>
        </div>
    </div>
    <!-- <div class="container" id="group_button">
        <div class="table-responsive" style="display: block;">
            <button class="btn btn-success btn-action-vps" data-type="mark_paid">Tạo VPS</button>
            <button class="btn btn-outline-secondary btn-action-vps" data-type="cancel">Hủy</button>
            <button class="btn btn-danger btn-action-vps" data-type="delete">Xóa</button>
        </div>
    </div> -->
</div>
{{-- Modal xoa order --}}
<div class="modal fade" id="delete-order">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-order" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-order" value="Xóa đơn hàng">
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
{{-- Modal xoa vps --}}
<div class="modal fade" id="delete-vps">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-vps" value="Xóa đơn hàng">
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="page" value="list_server">
@endsection
@section('scripts')
<script src="{{ asset('js/admin_proxy.js') }}"></script>
@endsection