@extends('layouts.app')
@section('title')
<i class="fa fa-globe" aria-hidden="true"></i> Danh sách sản phẩm domain
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item active"><i class="fa fa-globe" aria-hidden="true"></i> List domain products</li>
@endsection
@section('content')
<div class="row">
    <div data-v-a16ce4fa="" class="col-md-4">
        <a class="btn btn-primary" href="{{ route('admin.domain-products.create') }}">Tạo sản phẩm</a>
    </div>
</div>

@if(session("success"))
<div class="alert alert-success alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("success")}}</span>
</div>
@endif

<div class="card" style="margin-top: 20px">
    <div class="card-header">
        {{-- <h3 class="card-title">Bảng danh thông báo</h3> --}}
        <div class="card-tools float-left">
            <div class="input-group input-group-sm" style="width: 180px;">
                <button id="check_delete" type="submit" class="btn btn-sm btn-default check_delete"><i
                        class="fa fa-trash" aria-hidden="true"></i> Xóa
                    mục đã chọn</button>
            </div>
        </div>
        <div class="card-tools">
        <form method="get">
                <div class="input-group input-group-sm" style="width: 170px;">
                    <input type="search" name="keyword" class="form-control float-right"
                        placeholder="Tìm thông báo ..."
                        value="{{ !empty($_GET['keyword']) ? $_GET['keyword'] : '' }}">
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-default "><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive p-0">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th>
                        <div>
                            <input class="check_all" id="check_all" type="checkbox" value="1"> All
                        </div>
                    </th>
                    <th>ID</th>
                    <th>Loại</th>
                    <th>Vùng</th>
                    <th>1 năm</th>
                    <th>2 năm</th>
                    <th>3 năm</th>
                    <th>GH 1 năm</th>
                    <th>GH 2 năm</th>
                    <th>GH 3 năm</th>
                    <th>Khuyến mãi</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if ($domainproducts->count() > 0)
                    @foreach($domainproducts as $domainproduct)
                    <tr data-row-id="{{ $domainproduct->id }}">
                        <td>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="check_id"
                                    value="{{ $domainproduct->id }}">
                            </div>
                        </td>
                        <td>{{ $domainproduct->id }}</td>
                        <td>{{ $domainproduct->type }}</td>
                        <td>{{ $domainproduct->local_type }}</td>
                        <td>{{ $domainproduct->annually }}</td>
                        <td>{{ $domainproduct->biennially }}</td>
                        <td>{{ $domainproduct->triennially }}</td>
                        <td>{{ $domainproduct->annually_exp }}</td>
                        <td>{{ $domainproduct->biennially_exp }}</td>
                        <td>{{ $domainproduct->triennially_exp }}</td>
                        <td>
                          @if(!empty($domainproduct->promotion))
                            <span class="text-danger">Có</span>
                          @else
                            <span class="text-danger">Không</span>
                          @endif
                        </td>
                        <td>
                            <a class="btn btn-primary btn-sm"
                                href="{{ route( 'admin.domain-products.edit', ['id' => $domainproduct->id] ) }}"
                                data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
                            <a class="btn btn-danger btn-sm delete-domain-product" href="javascript:void(0)"
                                data-id="{{ $domainproduct->id }}" data-toggle="tooltip" title="Xóa sản phẩm này"><i
                                    class="fas fa-trash-alt"></i></a>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <td colspan="9" class="text-center text-danger">
                        Chưa có sản phẩm {{ $search }}
                    </td>
                @endif
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer clearfix">
        <ul class="pagination pagination-sm m-0 float-right">
            <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                {{ $domainproducts->links() }}
            </div>
        </ul>
    </div>
    <!-- /.card-footer -->
</div>
<input type="hidden" id="page" value="list_domain_2">
@endsection

@section('scripts')
<script>
    //Ajax xóa đơn lẻ từng row
    $(function(){
         $('.delete-domain-product').click(function (e) {
            var r = confirm("Bạn muốn xóa sản phẩm domain này này?");
            if (r == true) {
                var obj = $(this);
                var id = obj.attr('data-id');

                $.ajax({
                    type: "post",
                    url: "{{ route('admin.domain-products.delete') }}",
                    dataType: 'json',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": id
                    },
                    beforeSend: function () {

                    },
                    success: function (data) {
                        alert(data.message);
                        if (data.status) {
                            obj.closest('tr').remove();
                        }
                    },
                    error: function () {
                        alert(data.message);
                    }
                });
            }
        });

        //Click để ẩn và hiện các row
        $("#check_all").click(function(){
            $('input:checkbox').prop('checked', this.checked);
        });


        // Hiện button danger
        $('input:checkbox').change(function () {
            var check = $('.form-check-input:checked');
            if (check.length > 0) {
                $("#check_delete").addClass('btn-danger').removeClass('btn-default');
            } else if(check.length == 0){
                $("#check_delete").addClass('btn-default').removeClass('btn-danger');
            }
        });



        //Gửi ajax lên serverver để xóa các row được chọn
        $('.check_delete').on('click', function () {
            var r = confirm("Bạn muốn xóa các mục đã chọn?");
            if (r == true) {
                var array = [];
                var value = $('.form-check-input:checked');
                if (value.length > 0) {
                    $(value).each(function () {
                        array.push($(this).val());
                    })
                } else {
                    alert('Chưa chọn mục cần xóa');
                }
                $.ajax({
                    type: "post",
                    url: "{{ route('admin.domain-products.multidelete') }}",
                    dataType: 'json',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": array
                    },
                    success: function (data) {
                        //Hiển thị lại bảng sau khi xóa row
                        $.each(array, function( index, value ) {
				            $('table tr').filter("[data-row-id='" + value + "']").remove();
			            });
                        alert(data.message);
                    },
                    error: function () {
                        alert(data.message);
                    }
                });
            }
        });

    });


</script>
@endSection
