@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('title')

@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="/admin">Home</a></li>
<li class="breadcrumb-item "><a href="/admin/domain/">List domain</a></li>
<li class="breadcrumb-item active">Tạo tên miền</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                <div class="bg-success">
                    <p class="text-light">{{session("success")}}</p>
                </div>
                @elseif(session("fails"))
                <div class="bg-danger">
                    <p class="text-light">{{session("fails")}}</p>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
<form action="{{ route('admin.domain.create_domain') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="box box-primary">
        <div class="box-body table-responsive">
            <div class="dataTables_wrapper dt-bootstrap">
                <div class="row container mb-3">
                    <div class="col">
                        <h4>Tạo tên miền {{$domain}}</h4>
                    </div>
                </div>
                <div class="row mb-2 mr-2">
                    <div class="col">
                        <a href="{{ route('admin.domain.index') }}" class="btn btn-default btn-sm mb-2 ml-2">Danh sách tên miền</a>
                        <input type="submit" class="btn btn-info btn-sm float-right" value="Tạo tên miền">
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Khách hàng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="user_id" class="form-control select2">
                                                <option value="" disabled>Chọn khách hàng</option>
                                                @foreach ($users as $user)
                                                @php
                                                $selected = '';
                                                if ($user->id == old('user_id')) {
                                                $selected = 'selected';
                                                }
                                                @endphp
                                                <option value="{{$user->id}}" {{$selected}}>{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Domain:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control domain" name="domain" value="{{ $domain }}" required>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Vùng:</label>
                                        </div>
                                        <div class="col-md-8 printf_domain">
                                            {{ $product->local_type }}
                                        </div>
                                    </div>
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Trạng thái:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="status" id="status" class="select2 form-control"
                                                style="width: 100%;" data-value="Hosting" required>
                                                <option value="" disabled>Chọn trạng thái</option>
                                                <option value="Pending">Pending</option>
                                                <option value="Active">Active</option>
                                                <option value="Cancel">Cancel</option>
                                            </select>
                                            <div class="luu-y">
                                                Khi chọn trạng thái "Active", tên miền sẽ được đăng ký trên hệ thống PA Việt Nam
                                            </div>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Password:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="password_domain" value="" placeholder="8-15 kí tự gồm số và chữ" required>
                                        </div>
                                    </div>
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Khách hàng sử dụng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" name="customer_domain" required>
                                                <option value="canhan">Cá nhân</option>
                                                <option value="congty">Công ty</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Ngày đặt hàng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" name="" class="form-control" value="{{date('d/m/Y')}}" disabled>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right" style="padding-top: 5px;">
                                            <label for="">Thời gian đăng ký :</label>
                                        </div>
                                        <div class="col-md-8" id="next_due_date">
                                            <select name="billing_cycle" class="form-control select2" id="billing">
                                                <option value="" disabled>Chọn thời gian</option>
                                                <option value="annually" @if ($billing_cycle == 'annually') selected @endif>1 Năm</option>
                                                <option value="biennially" @if ($billing_cycle == 'biennially') selected @endif>2 Năm</option>
                                                <option value="triennially" @if ($billing_cycle == 'triennially') selected @endif>3 Năm</option>
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Ngày thanh toán :</label>
                                        </div>
                                        <div class="col-md-8" id="paid_date">
                                            <input type="text" name="date_paid" value="{{date('d/m/Y')}}" class="form-control">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Thành tiền :</label>
                                        </div>
                                        <div class="col-md-8 printf_billing" id="paid_date">
                                            <p>{{ $product->$billing_cycle }} VND</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input get-profile-info" type="checkbox" id="customCheckbox2">
                                    <label for="customCheckbox2" class="custom-control-label">Lấy thông tin cá nhân khách hàng để tự động điền vào form bên dưới</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 owner-info">
                            <div class="order">
                                {{-- 1 form group --}}
                                <h5 class="font-weight-bold text-info text-center">Thông tin khách hàng (owner)</h5>
                                <div class="form-group">
                                    <label for="owner-name" class="required">Tên (cá nhân hoặc công ty)</label>
                                    <input type="text" name="owner-name" id="owner-name" placeholder="Nhập tên"
                                        class="form-control"
                                        value="{{ old('owner-name') ? old('owner-name') : '' }}"
                                        title="Tên của khách hàng" required>
                                </div>
                                <div class="form-group input_ownerid_number">
                                    <label for="ownerid-number" class="required">Chứng minh thư</label>
                                        <input type="text" name="ownerid-number" id="ownerid-number" placeholder="Nhập chứng minh thư" class="form-control" value="{{ old('ownerid-number') ? old('ownerid-number') : '' }}"
                                    title="Tên của khách hàng" required>
                                </div>
                                <div class="form-group input_owner_taxcode">
                                    <label for="owner-taxcode" class="required">Mã số thuế (bắt buộc nếu khách hàng là công ty) </label>
                                        <input type="text" name="owner-taxcode" id="owner-taxcode" value="{{ old('owner-taxcode') ? old('owner-taxcode') : '' }}"
                                    placeholder="Bắt buộc đối với khách hàng công ty" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="owner-address" class="required">Địa chỉ</label>
                                    <input type="text" name="owner-address" id="owner-address"
                                        placeholder="Nhập địa chỉ" class="form-control"
                                        value="{{ old('owner-address') ? old('owner-address') : '' }}"
                                        required>
                                </div>
                                <div class="form-group">
                                    <label for="owner-email" class="required">Email </label>
                                    <input type="email" name="owner-email" id="owner-email" placeholder="Nhập email"
                                        class="form-control" pattern="[^ @]*@[^ @]*"
                                        value="{{ old('owner-email') ? old('owner-email') : '' }}"
                                        required>
                                </div>
                                <div class="form-group">
                                    <label for="owner-phone" class="required">Phone </label>
                                    <input type="text" name="owner-phone" id="owner-phone" placeholder="Nhập sđt"
                                        class="form-control"
                                        value="{{ old('owner-phone') ? old('owner-phone') : '' }}"
                                        required>
                                </div>
                                {{-- 1 form group --}}
                            </div>
                        </div>

                        <div class="col-12 ui-info">
                            <div class="order">
                                <h5 class="font-weight-bold text-info text-center">Thông tin chủ thể (ui)</h5>
                                <div class="form-group">
                                    <label for="">Tên chủ thể(cá nhân hoặc công ty):</label>
                                    <input type="text" name="ui_name" value="{{ old('ui_name') ? old('ui_name') : '' }}"
                                        class="form-control">
                                </div>
                                <div class="form-group input_ui_taxcode">
                                </div>

                                <div class="form-group">
                                    <label for="">Chứng minh nhân dân:</label>
                                    <input type="text" name="uiid_number"
                                        value="{{ old('uiid_number') ? old('uiid_number') : '' }}"
                                        class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Số điện thoại:</label>
                                    <input type="text" name="ui_phone" value="{{ old('ui_phone') ? old('ui_phone') : '' }}"
                                        class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Email:</label>
                                    <input type="text" name="ui_email" value="{{ old('ui_email') ? old('ui_email') : '' }}"
                                        class="form-control">
                                </div>
                                @if ($product->local_type === 'Việt Nam')
                                    <div class="form-group">
                                        <label for="">Giới tính:</label>
                                        <select class="form-control" name="ui_gender">
                                            <option value="Nam" @if (old('ui_gender') == 'Nam') selected @endif >Nam</option>
                                            <option value="Nữ" @if (old('ui_gender') == 'Nữ') selected @endif>Nữ</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Ngày sinh:</label>
                                        <input type="text" name="ui_birthdate" value="{{ old('ui_birthdate') ? old('ui_birthdate') : '' }}" class="form-control">
                                        <div class="luu-y">
                                            Lưu ý: Nhập theo đúng định dạng Ngày / Tháng / Năm. Ví dụ: 16/06/1992
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="">Địa chỉ:</label>
                                    <input type="text" name="ui_address" value="" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Tỉnh/Thành phố:</label>
                                    <select class="form-control" name="ui_province" required>
                                        <option value="" disabled selected>---Tỉnh/Thành phố ---</option>
                                        @foreach($provinces as $province)
                                        <?php
                                            $selected = '';
                                            if ($province == old('ui_province')) {
                                              $selected = 'selected';
                                            }
                                          ?>
                                        <option value="{{ $province }}" {{ $selected }}>{{$province}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="row form-group mb-4">
                                    <div class="col-md-6 text-center mt-4 show-ownerid-scan-before">
                                        <p>
                                            <b>Ảnh CMND mặt trước</b>
                                        </p>
                                        <div class="custom-file mt-3" style="width: 200px">
                                            <input type="file" class="custom-file-input ownerid-scan-before" name="cmnd_before"
                                                id="ownerid-scan-before" title="Chọn ảnh chứng minh nhân dân mặc trước">
                                            <label class="custom-file-label text-left overflow-hidden"
                                                for="ownerid-scan-before">Chọn ảnh</label>
                                        </div>
                                        {{-- <input type="file" name="cmnd_before" id="ownerid-scan-before" title="Chọn ảnh chứng minh nhân dân mặc trước" required=""> --}}
                                    </div>
                                    <div class="col-md-6 text-center mt-4 show-ownerid-scan-after">
                                        <p>
                                            <b>Ảnh CMND mặt sau</b>
                                        </p>
                                        <div class="custom-file mt-3" style="width: 200px">
                                            <input type="file" class="custom-file-input ownerid-scan-after" name="cmnd_after"
                                                id="ownerid-scan-after" title="Chọn ảnh chứng minh nhân dân mặc sau">
                                            <label class="custom-file-label text-left overflow-hidden"
                                                for="ownerid-scan-after">Chọn ảnh</label>
                                        </div>
                                        {{-- <input type="file" name="cmnd_after" id="ownerid-scan-after" title="Chọn ảnh chứng minh nhân dân mặc sau" required=""> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($product->local_type === 'Việt Nam')
                        <div class="col-12 admin-info">
                            <div class="order">
                                <h5 class="font-weight-bold text-info text-center">Thông tin người quản lý (admin)</h5>
                                <div class="form-group">
                                    <label for="admin-name" class="required">Tên</label>
                                    <div class="admin-name">
                                        <input type="text" name="admin-name" id="admin-name"
                                            placeholder="Nhập tên người quản lý" class="form-control"
                                            value="{{ old('admin-name') ? old('admin-name') : '' }}"
                                            required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="adminid-number" class="required">Chứng minh thư</label>
                                    <div class="adminid-number">
                                        <input type="text" name="adminid-number" id="adminid-number"
                                            value="{{ old('adminid-number') ? old('adminid-number') : '' }}"
                                            placeholder="Nhập CMT người quản lý" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="admin-gender" class="required">Giới tính</label>
                                    <select class="form-control @error('admin-gender') is-invalid @enderror"
                                        name="admin-gender" required>
                                        <option value="Nam" @if( old('admin-gender') == 'Nam') selected @endif>Nam</option>
                                        <option value="Nữ" @if( old('admin-gender') == 'Nữ') selected @endif>Nữ</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="admin-birthdate" class="required">Ngày sinh</label>
                                    <input type="text" name="admin-birthdate" id="admin-birthdate"
                                        value="{{ old('admin-birthdate') ? old('admin-birthdate') : '' }}" class="form-control  @error('admin-birthdate') is-invalid @enderror"
                                        placeholder="Ngày sinh" required>
                                    <div class="luu-y">
                                        Lưu ý: Nhập theo đúng định dạng Ngày / Tháng / Năm. Ví dụ: 16/06/1992
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="admin-address" class="required">Địa chỉ</label>
                                    <div class="admin-address">
                                        <input type="text" name="admin-address" id="admin-address" placeholder="Nhập địa chỉ" class="form-control"
                                            value="{{ old('admin-address') ? old('admin-address') : '' }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="admin-province" class="required">Tỉnh/Thành phố</label>
                                    <select class="form-control @error('admin-province') is-invalid @enderror" name="admin-province" required>
                                        <option value="" disabled selected>---Tỉnh/Thành phố ---</option>
                                        @foreach($provinces as $province)
                                            <?php
                                                $selected = '';
                                                if (old('admin-province') == $province) {
                                                    $selected = 'selected';
                                                }
                                            ?>
                                            <option value="{{$province}}" {{$selected}}>{{$province}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="admin-email" class="required">Email </label>
                                    <div class="admin-email">
                                        <input type="email" name="admin-email" id="admin-email" placeholder="Nhập email" class="form-control" pattern="[^ @]*@[^ @]*"
                                            value="{{ old('admin-email') ? old('admin-email') : '' }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="admin-phone" class="required">Phone</label>
                                    <div class="admin-phone">
                                        <input type="text" name="admin-phone" id="admin-phone" placeholder="Nhập sđt" class="form-control" value="{{ old('admin-phone') ? old('admin-phone') : '' }}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <a href="{{ route('admin.domain.index') }}" class="btn btn-default btn-sm mt-2 ml-2">Danh sách tên miền</a>
                    <input type="submit" value="Tạo tên miền" class="btn btn-info btn-sm float-right mt-2 mr-2">
                </div>
            </div>
        </div>
    </div>
</form>
<input type="hidden" id="page" value="create_domain">

@endsection
@section('scripts')
<!-- <script src="{{ asset('js/vps.js') }}"></script> -->
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script type="text/javascript">
    $(document).ready(function() {
		//Date picker
		$('#datepicker').datepicker({
			autoclose: true
		});
        // select
        $('.select2').select2();
        
        //Chọn file ảnh
        $(".custom-file-input").on("change", function() {
            var fileName = $(this).val().split("\\").pop();
            $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        });
        $('input[name="domain"]').keyup(function(){
            var domain = $(this).val();
            var html_domain = '';
            $.ajax({
                url: "{{ route('admin.domain.get_product_domain') }}",
                type: 'GET',
                dataType: 'JSON',
                data: {'domain': domain},
                beforeSend: function() {
                        $('.printf_domain').html('<div class="spinner-border spinner-border-sm text-info"></div>');
                },
                success: function(data) {
                    html_domain += ' <p>'+ data.local_type +'</p>';
                    $('.printf_domain').html(html_domain);
                },
                error: function(e) {
                    $('.printf_domain').html('<span class="text-danger">Loading ...</span>');
                }
            });
        });
        $('select[name=billing_cycle]').on('change', function(e) {
            e.preventDefault();
            var billing_cycle = $(this).val();
            var domain = $('input[name="domain"]').val();
            var html = '';
            $('.printf_billing').html(html);
            $.ajax({
                url: "{{ route('admin.domain.get_product_domain') }}",
                type: 'GET',
                dataType: 'JSON',
                data: {'domain': domain},
                beforeSend: function() {
                        $('.printf_billing').html('<div class="spinner-border spinner-border-sm text-info"></div>');
                    },
                success: function(data) {
                    html += ' <p>'+ data[billing_cycle]+' VND</p>';
                    $('.printf_billing').html(html);
                },
                error: function(e) {
                        $('.printf_billing').html('<span class="text-danger">Truy vấn lỗi</span>');
                }
            });
        });
        $('select[name="user_id"]').on('change', function(){
            var data_user_id = $(this).val();
            $('.get-profile-info').attr('data-user-id', data_user_id);
        });
        $('.get-profile-info').on('change', function(){
            if (this.checked) {
                var data_user_id = $('.get-profile-info').attr('data-user-id');
                if (data_user_id == '' || data_user_id == null) {
                    alert ('Vui lòng chọn người dùng');
                } else {
                    $.ajax({
                        type: 'get',
                        url:  "{{ route('admin.domain.auto_fill_domain_form') }}",
                        data: {id: data_user_id},
                        datatype: 'json',
                        success: function (data) {
                            console.log(data.user_meta);
                            $('input[name="owner-name"]').val(data.name);
                            $('input[name="ui_name"]').val(data.name);
                            $('input[name="admin-name"]').val(data.name);
                            $('input[name="ownerid-number"]').val(data.user_meta.cmnd);
                            $('input[name="uiid_number"]').val(data.user_meta.cmnd);
                            $('input[name="adminid-number"]').val(data.user_meta.cmnd);
                            $('input[name="owner-email"]').val(data.email);
                            $('input[name="ui_email"]').val(data.email);
                            $('input[name="admin-email"]').val(data.email);
                            $('input[name="owner-phone"]').val(data.user_meta.phone);
                            $('input[name="ui_phone"]').val(data.user_meta.phone);
                            $('input[name="admin-phone"]').val(data.user_meta.phone);
                            $('input[name="owner-address"]').val(data.user_meta.address);
                            $('input[name="ui_address"]').val(data.user_meta.address);
                            $('input[name="admin-address"]').val(data.user_meta.address);
                            $('input[name="ui_birthdate"]').val(data.user_meta.date);
                            $('input[name="admin-birthdate"]').val(data.user_meta.date);
                            $('input[name="owner-taxcode"]').val(data.user_meta.mst);
                            if(data.user_meta.gender) {
                                $('select[name="ui_gender"] option[value='+data.user_meta.gender+'').attr('selected','selected');
                                $('select[name="admin-gender"] option[value='+data.user_meta.gender+'').attr('selected','selected');
                            }
                            if(data.user_meta.cmnd_before) {
                                $('.show-ownerid-scan-before').append('<div style="margin: 10px 0" class="ownerid-scan-before-img"><img style="height:100px" src="/'+data.user_meta.cmnd_before+'"><input type="hidden" name="cmnd_before_in_profile" value="'+data.user_meta.cmnd_before+'"></div>');
                            }
                            if(data.user_meta.cmnd_after) {
                                $('.show-ownerid-scan-after').append('<div style="margin: 10px 0" class="ownerid-scan-after-img"><img style="height:100px" src="/'+data.user_meta.cmnd_after+'"><input type="hidden" name="cmnd_after_in_profile" value="'+data.user_meta.cmnd_after+'"></div>');
                            }
                            
                        },
                        error: function(e) {
                            console.error(e);
                        }
                    });
                }
            } else {
                $('.owner-info input').val('');
                $('.ui-info input').val('');
                $('.admin-info input').val('');
                $('select[name="ui_gender"]').find('option:selected').removeAttr('selected');
                $('select[name="admin-gender"]').find('option:selected').removeAttr('selected');
                $('.ownerid-scan-before-img').remove();
                $('.ownerid-scan-after-img').remove();
            } 
        });
    });
    // var customer = $('select[name=customer_domain] option').filter(':selected').val();
    // console.log(customer);
    // printf_taxcode(customer);
    // function printf_taxcode(customer) {
    //     var html_ui_taxcode = '';
    //     var html_owner_taxcode = '';
    //     var html_ownerid_number = '';
    //     if(customer == 'congty') {
    //         html_ui_taxcode += '<label for="">Mã số thuế:</label>';
    //         html_ui_taxcode += '<input type="text" name="ui_taxcode" value="" class="form-control">';
    //         html_owner_taxcode += '<label for="owner-taxcode" class="required">Mã số thuế </label>';
    //         html_owner_taxcode += '<input type="text" name="owner-taxcode" id="owner-taxcode" value="{{ old('owner-taxcode') ? old('owner-taxcode') : '' }}" placeholder="Bắt buộc đối với khách hàng công ty" class="form-control" required>';
    //         $('.input_ui_taxcode').html(html_ui_taxcode);
    //         $('.input_owner_taxcode').html(html_owner_taxcode);
    //     } else if (customer == 'canhan') {
    //         html_ownerid_number += '<label for="ownerid-number" class="required">Chứng minh thư</label>';
    //         html_ownerid_number += '<input type="text" name="ownerid-number" id="ownerid-number" placeholder="Nhập chứng minh thư" class="form-control" value="{{ old('ownerid-number') ? old('ownerid-number') : '' }}" title="Tên của khách hàng" required>';
    //         $('.input_ownerid_number').html(html_ownerid_number);
    //     }
    // }
    // $('select[name=customer_domain]').on('change', function() {
    //     var customer = $(this).val();
    //     var html_ui_taxcode = '';
    //     if(customer == 'congty') {
    //         html_ui_taxcode += '<label for="">Mã số thuế:</label>';
    //         html_ui_taxcode += '<input type="text" name="ui_taxcode" value="" class="form-control">';
    //         $('.input_ui_taxcode').html(html_ui_taxcode);
    //     } else {
    //         $('.input_ui_taxcode').html(html_ui_taxcode);
    //     }
    // });
</script>
@endsection