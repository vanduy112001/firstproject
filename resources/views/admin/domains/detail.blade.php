@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('title')
<i class="fas fa-file-invoice nav-icon "></i> Quản lý tên miền
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="/admin">Home</a></li>
<li class="breadcrumb-item "><a href="/admin/domain/">List domain</a></li>
<li class="breadcrumb-item active">Thông tin Domain</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>  
                @endif
            </div>
        </div>
    </div>
</div>
<form action="{{ route('admin.domain.update_sevices') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="box box-primary">
        <div class="box-body table-responsive">
            <div class="dataTables_wrapper dt-bootstrap">
                <div class="row container mb-3">
                    <div class="col">
                        <h4>Thông tin tên miền {{$detail->domain}}</h4>
                    </div>
                </div>
                <div class="row mb-2 mr-2">
                    <div class="col">
                        <a href="{{ route('admin.domain.index') }}" class="btn btn-default btn-sm mb-2 ml-2"><i class="fas fa-list"></i> Danh sách tên miền</a>
                        <button class="btn btn-secondary btn-sm mb-2 ml-2 re_order" data-id="{{ $detail->id }}" data-domain="{{ $detail->domain }}" data-toggle="modal" data-target="#confirm-reorder"><i class="fas fa-edit"></i> Đăng ký</button>
                        <button class="btn bg-olive btn-sm mb-2 ml-2 expired-domain" data-id="{{ $detail->id }}" data-domain="{{ $detail->domain }}" data-toggle="modal" data-target="#modal-exp"><i class="fas fa-plus-circle"></i> Gia hạn</button>
                        <input type="submit" class="btn btn-info btn-sm float-right" value="Cập nhập">
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Khách hàng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="user_id" class="form-control select2">
                                                <option value="" disabled>Chọn khách hàng</option>
                                                @foreach ($users as $user)
                                                @php
                                                $selected = '';
                                                if ($user->id == $detail->user_id) {
                                                $selected = 'selected';
                                                }
                                                @endphp
                                                <option value="{{$user->id}}" {{$selected}}>{{$user->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Vùng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            {{ $detail->domain_product->local_type }}
                                        </div>
                                    </div>
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Trạng thái:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="status" id="status" class="select2 form-control" style="width: 100%;" data-value="Hosting">
                                                <option value="" disabled>Chọn trạng thái</option>
                                                <option value="Active" @if ( $detail->status == 'Active' || old('status') == 'Active') selected @endif>Active</option>
                                                <option value="Pending" @if ( $detail->status == 'Pending' || old('status') == 'Pending') selected @endif>Pending</option>
                                                <option value="Cancel" @if ( $detail->status == 'Cancel' || old('status') == 'Cancel') selected @endif>Cancel</option>
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Domain:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="domain" value="{{$detail->domain}}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Password:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="password_domain" value="{{$detail->password_domain}}">
                                        </div>
                                    </div>
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Khách hàng sử dụng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select class="form-control" name="customer_domain">
                                                <option value="canhan" @if($detail->customer_domain == 'canhan') selected @endif>Cá nhân</option>
                                                <option value="congty" @if($detail->customer_domain == 'congty') selected @endif>Công ty</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Ngày đặt hàng:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <p class="text-success">{{date('d-m-Y', strtotime($detail->created_at))}}
                                            </p>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Ngày hết hạn :</label>
                                        </div>
                                        <div class="col-md-8" id="next_due_date">
                                            @if (!empty($detail->next_due_date))
                                                <span class="text-success show_next_due_date">{{date('d-m-Y', strtotime($detail->next_due_date))}}</span>
                                            @else
                                                <span class="text-danger show_next_due_date">Chưa tạo</span>
                                            @endif
                                            <span class="ml-4">
                                                <a href="javascript:void(0)" class="btn btn-danger badge badge-pill ml-1 show_create_next_due_date" data-date="{{date('d-m-Y', strtotime($detail->next_due_date))}}"> <i class="fas fa-edit"></i> Sửa </a>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="group-invoice create_next_due_date_form col-md-8 mx-auto" style="display: none">
                                        <div class="input-group input-group-sm">
                                            <input type="text" name="next_due_date" class="form-control mr-2 rounded create_next_due_date" id="datepicker" placeholder="Cập nhập ngày hết hạn" autocomplete="off">
                                            <button class="btn btn-primary btn-sm create_next_due_date_confirm" data-id="{{ $detail->id }}" type="submit">Cập nhập</button>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Ngày thanh toán :</label>
                                        </div>
                                        <div class="col-md-8" id="paid_date">
                                            @if(!empty($detail->detail_order->status))
                                                @if($detail->detail_order->status == 'paid')
                                                    <span class="text-success">{{date('d-m-Y', strtotime($detail->detail_order->updated_at))}}</span>
                                                    {{-- <span class="ml-4">
                                                                <a href="javascript:void(0)" class="btn btn-outline-default paid_date"
                                                                    data-date="{{ date('d-m-Y', strtotime($detail->detail_order->updated_at)) }}"><i
                                                        class="fas fa-edit"></i></a>
                                                    </span> --}}
                                                @else
                                                    <span class="text-danger">Chưa thanh toán</span>
                                                @endif
                                            @else
                                                <span class="text-danger">Chưa thanh toán</span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Thời gian:</label>
                                        </div>
                                        <div class="col-md-8" id="billing_cycle">
                                            {{ $billings[$detail->billing_cycle] }}
                                            {{-- <span class="ml-4">
                                                <a href="javascript:void(0)" class="btn btn-outline-default billing_cycle"
                                                    data-name="{{$detail->billing_cycle}}"><i
                                                class="fas fa-edit"></i></a>
                                            </span> --}}
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Thành tiền :</label>
                                        </div>
                                        <div class="col-md-8" id="sub_total">
                                            @if(!empty($detail->price_override))
                                                <span>
                                                    <b>{!!number_format( $detail->price_override ,0,",",".")!!} VNĐ</b>
                                                </span>
                                                <span class="pl-2 text-danger">- giá ghi đè </span>
                                                {{-- <span class="ml-4">
                                                        <a href="javascript:void(0)" class="btn btn-outline-default sub_total"
                                                            data-price="{{$detail->price_override}}"><i
                                                    class="fas fa-edit"></i></a>
                                                </span> --}}
                                            @else
                                                <span>
                                                    @if (!empty($detail->detail_order->sub_total))
                                                        <b>{!!number_format(($detail->detail_order->sub_total),0,",",".")!!} VNĐ</b>
                                                    @else
                                                        <b></b>
                                                    @endif
                                                </span>
                                                {{-- <span class="ml-4">
                                                        <a href="javascript:void(0)" class="btn btn-outline-default sub_total"
                                                            data-price="{{!empty($detail->detail_order->sub_total) ? $detail->detail_order->sub_total : ''}}"><i
                                                    class="fas fa-edit"></i></a>
                                                </span> --}}
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="order">
                                {{-- 1 form group --}}
                                <h5 class="font-weight-bold text-info text-center">Thông tin khách hàng (owner)</h5>
                                <div class="form-group">
                                    <label for="owner-name" class="required">Tên (cá nhân hoặc công ty)</label>
                                    <input type="text" name="owner-name" id="owner-name" placeholder="Nhập tên" class="form-control" value="{{ old('owner-name') ? old('owner-name') : $detail->owner_name }}" title="Tên của khách hàng" required>
                                </div>
                                <div class="form-group input_ownerid_number">
                                    {{-- <label for="ownerid-number" class="required">Chứng minh thư</label>
                                        <input type="text" name="ownerid-number" id="ownerid-number" placeholder="Nhập chứng minh thư" class="form-control" value="{{ old('ownerid-number') ? old('ownerid-number') : $detail->ownerid_number }}"
                                    title="Tên của khách hàng" required> --}}
                                </div>
                                <div class="form-group input_owner_taxcode">
                                    {{-- <label for="owner-taxcode" class="required">Mã số thuế </label>
                                        <input type="text" name="owner-taxcode" id="owner-taxcode" value="{{ old('owner-taxcode') ? old('owner-taxcode') : $detail->owner_taxcode }}"
                                    placeholder="Bắt buộc đối với khách hàng công ty" class="form-control" required>
                                    --}}
                                </div>
                                <div class="form-group">
                                    <label for="owner-address" class="required">Địa chỉ</label>
                                    <input type="text" name="owner-address" id="owner-address" placeholder="Nhập địa chỉ" class="form-control" value="{{ old('owner-address') ? old('owner-address') : $detail->owner_address }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="owner-email" class="required">Email </label>
                                    <input type="email" name="owner-email" id="owner-email" placeholder="Nhập email" class="form-control" pattern="[^ @]*@[^ @]*" value="{{ old('owner-email') ? old('owner-email') : $detail->owner_email }}" required>
                                </div>
                                <div class="form-group">
                                    <label for="owner-phone" class="required">Phone </label>
                                    <input type="text" name="owner-phone" id="owner-phone" placeholder="Nhập sđt" class="form-control" value="{{ old('owner-phone') ? old('owner-phone') : $detail->owner_phone }}" required>
                                </div>
                                {{-- 1 form group --}}
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="order">
                                <h5 class="font-weight-bold text-info text-center">Thông tin chủ thể (ui)</h5>
                                <div class="form-group">
                                    <label for="">Tên chủ thể(cá nhân hoặc công ty):</label>
                                    <input type="text" name="ui_name" value="{{ $detail->ui_name }}" class="form-control">
                                </div>
                                <div class="form-group input_ui_taxcode">
                                </div>
                                <div class="form-group">
                                    <label for="">Chứng minh nhân dân:</label>
                                    <input type="text" name="uiid_number" value="{{ old('uiid_number') ? old('uiid_number') : $detail->uiid_number }}" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Số điện thoại:</label>
                                    <input type="text" name="ui_phone" value="{{ $detail->ui_phone }}" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Email:</label>
                                    <input type="text" name="ui_email" value="{{ $detail->ui_email }}" class="form-control">
                                </div>
                                @if ($detail->domain_product->local_type === 'Việt Nam')
                                    <div class="form-group">
                                        <label for="">Giới tính:</label>
                                        <select class="form-control" name="ui_gender">
                                            <option value="Nam" @if($detail->ui_gender == 'Nam') selected @endif>Nam
                                            </option>
                                            <option value="Nữ" @if($detail->ui_gender == 'Nữ') selected @endif>Nữ</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="">Ngày sinh:</label>
                                        <input type="text" name="ui_birthdate" value="{{ old('ui_birthdate') ? old('ui_birthdate') : date("d/m/Y", strtotime($detail->admin_birthdate)) }}" class="form-control">
                                        <div class="luu-y">
                                            Lưu ý: Nhập theo đúng định dạng Ngày / Tháng / Năm. Ví dụ: 16/06/1992
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="">Địa chỉ:</label>
                                    <input type="text" name="ui_address" value="{{ $detail->ui_address }}" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="">Tỉnh/Thành phố:</label>
                                    <select class="form-control" name="ui_province">
                                        @foreach($provinces as $province)
                                            <?php
                                                $selected = '';
                                                if ($province == $detail->ui_province) {
                                                $selected = 'selected';
                                                }
                                            ?>
                                            <option value="{{ $province }}" {{ $selected }}>{{$province}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="row form-group mb-4">
                                    <div class="col-md-6 text-center mt-4">
                                        <p>
                                            <b>Ảnh CMND mặt trước</b>
                                        </p>
                                        @if (!empty($detail->cmnd_before))
                                            <a href="{{ asset($detail->cmnd_before) }}" download="">
                                                <img src="{{ asset($detail->cmnd_before) }}" alt="Scan CMND mặt trước" width="200" height="200">
                                            </a> <br>
                                        @endif
                                        <div class="custom-file mt-3" style="width: 200px">
                                            <input type="file" class="custom-file-input" name="cmnd_before" id="ownerid-scan-before" title="Chọn ảnh chứng minh nhân dân mặc trước">
                                            <label class="custom-file-label text-left overflow-hidden" for="ownerid-scan-before">Chọn ảnh</label>
                                        </div>
                                        {{-- <input type="file" name="cmnd_before" id="ownerid-scan-before" title="Chọn ảnh chứng minh nhân dân mặc trước" required=""> --}}
                                    </div>
                                    <div class="col-md-6 text-center mt-4">
                                        <p>
                                            <b>Ảnh CMND mặt sau</b>
                                        </p>
                                        @if (!empty($detail->cmnd_after))
                                            <a href="{{ url($detail->cmnd_after) }}" download="">
                                                <img src="{{ asset($detail->cmnd_after) }}" alt="Scan CMND mặt trước" width="200" height="200">
                                            </a> <br>
                                        @endif
                                        <div class="custom-file mt-3" style="width: 200px">
                                            <input type="file" class="custom-file-input" name="cmnd_after" id="ownerid-scan-after" title="Chọn ảnh chứng minh nhân dân mặc sau">
                                            <label class="custom-file-label text-left overflow-hidden" for="ownerid-scan-before">Chọn ảnh</label>
                                        </div>
                                        {{-- <input type="file" name="cmnd_after" id="ownerid-scan-after" title="Chọn ảnh chứng minh nhân dân mặc sau" required=""> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        @if ($detail->domain_product->local_type === 'Việt Nam')
                        <div class="col-12">
                            <div class="order">
                                <h5 class="font-weight-bold text-info text-center">Thông tin người quản lý (admin)</h5>
                                <div class="form-group">
                                    <label for="admin-name" class="required">Tên</label>
                                    <div class="admin-name">
                                        <input type="text" name="admin-name" id="admin-name" placeholder="Nhập tên người quản lý" class="form-control" value="{{ old('admin-name') ? old('admin-name') : $detail->admin_name }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="adminid-number" class="required">Chứng minh thư</label>
                                    <div class="adminid-number">
                                        <input type="text" name="adminid-number" id="adminid-number" value="{{ old('adminid-number') ? old('adminid-number') : $detail->adminid_number }}" placeholder="Nhập CMT người quản lý" class="form-control" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="admin-gender" class="required">Giới tính</label>
                                    <select class="form-control @error('admin-gender') is-invalid @enderror" name="admin-gender" required>
                                        <option value="Nam" @if( old('admin-gender')=='Nam' || $detail->admin_gender == 'Nam' ) selected @endif>Nam</option>
                                        <option value="Nữ" @if( old('admin-gender')=='Nữ' || $detail->admin_gender == 'Nữ') selected @endif>Nữ</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="admin-birthdate" class="required">Ngày sinh</label>
                                    <input type="text" name="admin-birthdate" id="admin-birthdate" value="{{ old('admin-birthdate') ? old('admin-birthdate') : date("d/m/Y", strtotime($detail->admin_birthdate))}}" class="form-control  @error('admin-birthdate') is-invalid @enderror" placeholder="Ngày sinh" required>
                                    <div class="luu-y">
                                        Lưu ý: Nhập theo đúng định dạng Ngày / Tháng / Năm. Ví dụ: 16/06/1992
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="admin-address" class="required">Địa chỉ</label>
                                    <div class="admin-address">
                                        <input type="text" name="admin-address" id="admin-address" placeholder="Nhập địa chỉ" class="form-control" value="{{ old('admin-address') ? old('admin-address') : $detail->admin_address }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="admin-province" class="required">Tỉnh/Thành phố</label>
                                    <select class="form-control @error('admin-province') is-invalid @enderror" name="admin-province" required>
                                        <option value="" disabled selected>---Tỉnh/Thành phố ---</option>
                                        @foreach($provinces as $province)
                                            <?php
                                            $selected = '';
                                            if (old('admin-province') == $province || $detail->admin_province == $province) {
                                                $selected = 'selected';
                                            }
                                            ?>
                                            <option value="{{$province}}" {{$selected}}>{{$province}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="admin-email" class="required">Email </label>
                                    <div class="admin-email">
                                        <input type="email" name="admin-email" id="admin-email" placeholder="Nhập email" class="form-control" pattern="[^ @]*@[^ @]*" value="{{ old('admin-email') ? old('admin-email') : $detail->admin_email }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="admin-phone" class="required">Phone</label>
                                    <div class="admin-phone">
                                        <input type="text" name="admin-phone" id="admin-phone" placeholder="Nhập sđt" class="form-control" value="{{ old('admin-phone') ? old('admin-phone') : $detail->admin_phone }}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <input type="hidden" name="id" value="{{ $detail->id }}">
                    <input type="hidden" name="invoice_id" value="{{ $detail->detail_order_id }}">
                    <a href="{{ route('admin.domain.index') }}" class="btn btn-default btn-sm mt-2 ml-2"><i class="fas fa-list"></i> Danh sách tên miền</a>
                    <button class="btn btn-secondary btn-sm mt-2 ml-2 re_order" data-id="{{ $detail->id }}" data-domain="{{ $detail->domain }}" data-toggle="modal" data-target="#confirm-reorder"><i class="fas fa-edit"></i> Đăng ký</button>
                    <button class="btn bg-olive btn-sm mt-2 ml-2 expired-domain" data-id="{{ $detail->id }}" data-domain="{{ $detail->domain }}" data-toggle="modal" data-target="#modal-exp"><i class="fas fa-plus-circle"></i> Gia hạn</button>
                    <input type="submit" value="Cập nhật" class="btn btn-info btn-sm float-right mt-2 mr-2">
                </div>
            </div>
        </div>
    </div>
</form>
{{-- Modal confirm submit Re-Order --}}
<div class="modal fade" id="confirm-reorder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Đăng ký tên miền <span class="this-domain text-primary"></span> trên PA Việt Nam</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" class="confirm-reorder" method="GET">
                <div class="modal-body">
                    <div class="loading text-center"></div>
                    Truy vấn đăng ký trong trường hợp: <br>
                    - PA Việt Nam gửi sai kết quả (lỗi do hệ thống PA). <br>
                    - Đăng ký mới.
                    <p class="text-danger">Lưu ý: Cập nhập lại các thông tin tên miền này (nếu có chỉnh sửa) trước khi đăng ký. </p>
                </div>
                <div class="reorder"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-success" value="Đăng ký">
                </div>
            </form>
        </div>
    </div>
</div>

{{-- Modal gia hạn --}}
<div class="modal fade" id="modal-exp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Gia hạn tên miền</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="notication-invoice" class="text-center">
                    <form action="{{ route('admin.domain.extend_domain_expired') }}" method="post">
                        <div class="form-group">
                            <label for="billing">Chọn thời gian gia hạn</label>
                            <div class="mt-3 mb-3">
                                <select name="billing" class="form-control time" id="billing">
                                </select>
                            </div>

                            <div class="card">
                                <div class="card-header text-center">
                                    <h3 class="card-title m-0">Bảng giá gia hạn</h3>
                                </div>
                                <div class="loading"></div>
                                <div class="card-body p-0 show-price">
                                    <table class="table table-sm">
                                        <thead>
                                            <tr class="add-domain"></tr>
                                        </thead>
                                        <tbody class="add-billing"></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                @csrf
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                <input type="hidden" name="domain" value="">
                <input type="submit" class="btn btn-block btn-primary" style="width:auto" value="Xác nhận">
            </div>
            </form>
        </div>
    </div>
</div>
<input type="hidden" id="page" value="detail_domain">


@endsection
@section('scripts')
<!-- <script src="{{ asset('js/vps.js') }}"></script> -->
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script type="text/javascript">
    $(document).ready(function() {
		//Date picker
		$('#datepicker').datepicker({
            autoclose: true,
            format: 'dd-mm-yyyy'
		});
        // select
        $('.select2').select2();
    });
    //Chọn file ảnh
    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });
    var customer = $('select[name=customer_domain] option').filter(':selected').val();
    printf_taxcode(customer);
    function printf_taxcode(customer) {
        var html_ui_taxcode = '';
        var html_owner_taxcode = '';
        var html_ownerid_number = '';
        if(customer == 'congty') {
            html_ui_taxcode += '<label for="">Mã số thuế:</label>';
            html_ui_taxcode += '<input type="text" name="ui_taxcode" value="{{ $detail->ui_taxcode }}" class="form-control">';
            html_owner_taxcode += '<label for="owner-taxcode" class="required">Mã số thuế </label>';
            html_owner_taxcode += '<input type="text" name="owner-taxcode" id="owner-taxcode" value="{{ old('owner-taxcode') ? old('owner-taxcode') : $detail->owner_taxcode }}" placeholder="Bắt buộc đối với khách hàng công ty" class="form-control" required>';
            $('.input_ui_taxcode').html(html_ui_taxcode);
            $('.input_owner_taxcode').html(html_owner_taxcode);
        } else if (customer == 'canhan') {
            html_ownerid_number += '<label for="ownerid-number" class="required">Chứng minh thư</label>';
            html_ownerid_number += '<input type="text" name="ownerid-number" id="ownerid-number" placeholder="Nhập chứng minh thư" class="form-control" value="{{ old('ownerid-number') ? old('ownerid-number') : $detail->ownerid_number }}" title="Tên của khách hàng" required>';
            $('.input_ownerid_number').html(html_ownerid_number);
        }
    }
    $('select[name=customer_domain]').on('change', function() {
        var customer = $(this).val();
        var html_ui_taxcode = '';
        if(customer == 'congty') {
            html_ui_taxcode += '<label for="">Mã số thuế:</label>';
            html_ui_taxcode += '<input type="text" name="ui_taxcode" value="{{ $detail->ui_taxcode }}" class="form-control">';
            $('.input_ui_taxcode').html(html_ui_taxcode);
        } else {
            $('.input_ui_taxcode').html(html_ui_taxcode);
        }
    });
    $('.re_order').on('click', function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        var domain = $(this).attr('data-domain');
        $('.this-domain').text(domain);
        $('.reorder').html('<input type="hidden" name="id" value="'+ id +'" >');
    });
    $('.confirm-reorder').submit(function (e) {
        e.preventDefault();
        var obj = $(this);
        var html = '';
        $.ajax({
            type: "GET",
            url: "{{ route('admin.domain.get_domain') }}",
            dataType: 'json',
            data: obj.serialize(),
            beforeSend: function () {
                $('.loading').html('<div class="spinner-border spinner-border-sm text-info"></div>');
            },
            success: function (data) {
                if(data.status == true) {
                    html += '<div class="alert alert-success alert-dismissible">';
                    html += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                    html += '<span class="text-light">'+ data.message +'</span>';
                    html += '</div>';
                    $('.loading').html(html);
                } else {
                    html += '<div class="alert alert-danger alert-dismissible">';
                    html += '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
                    html += '<span class="text-light">'+ data.message +'</span>';
                    html += '</div>';
                    $('.loading').html(html);
                }
            },
            errors: function () {
                alert('Có lỗi xảy ra, vui lòng reload và thử lại.');
            }
        });
    });

    //Hiển thị cập nhập ngày hết hạn
    $('.show_create_next_due_date').on('click', function(){
        $('.create_next_due_date_form').toggle(500);
    });
    //Cập nhập ngày hết hạn
    $('.create_next_due_date_confirm').on('click', function(e){
        e.preventDefault();

        var next_due_date = $('input[name="next_due_date"]').val();
        var id = $(this).attr('data-id');
        var html = '';
        if (next_due_date === "") {
            var r = confirm("Dữ liệu nhập vào bị rỗng. Có phải bạn muốn xóa ngày hết hạn hiện tại trong data?");
            if (r === true) {
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.domain.detail.update_next_due_date') }}",
                    dataType: 'JSON',
                    data: { 'next_due_date': next_due_date , 'id': id, '_token': '{{ csrf_token() }}' },
                    beforeSend: function () {
                        $('.show_next_due_date').html('<div class="spinner-border spinner-border-sm text-info"></div>');
                    },
                    success: function (data) {
                        if(data.status === true) {
                            $('.show_next_due_date').html(data.next_due_date);
                        } else {
                            $('.show_next_due_date').html(data.message);
                        }
                    },
                    errors: function () {
                        alert('Có lỗi xảy ra, vui lòng reload và thử lại.');
                    }
                });
            }
        } else {
            $.ajax({
                type: "POST",
                url: "{{ route('admin.domain.detail.update_next_due_date') }}",
                dataType: 'JSON',
                data: { 'next_due_date': next_due_date , 'id': id, '_token': '{{ csrf_token() }}' },
                beforeSend: function () {
                    $('.show_next_due_date').html('<div class="spinner-border spinner-border-sm text-info"></div>');
                },
                success: function (data) {
                    if(data.status === true) {
                        $('.show_next_due_date').html(data.next_due_date);
                    } else {
                        $('.show_next_due_date').html(data.message);
                    }
                },
                errors: function () {
                    alert('Có lỗi xảy ra, vui lòng reload và thử lại.');
                }
            });
        }
    });

    //Gia hạn domain
    $('.expired-domain').on('click', function(e) {
        e.preventDefault();
        var get_id = $(this).attr('data-id');
        $.ajax({
            type: 'get',
            url: '/domain/domain-product-expired-price',
            data: {id: get_id},
            dataType: 'json',
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                 $('.loading').html(html);
                    $('.show-price').hide();
                },
            success: function(data) {
                var html = '';
                var html_billing = '';
                var html_time = '<option value="" disabled>Chọn thời gian</option>';
                html += '<th>domain</th>';
                html += '<th class="text-danger">'+data.domain.domain+'</th>';
                $.each(data.billings, function(index, value){
                    if(data.domain_product[index] != null) {
                        html_billing += '<tr>';
                        html_billing += '<td>'+ value +' Năm</td>';
                        html_billing += '<td>'+ addCommas(data.domain_product[index]) +' VNĐ</td>';
                        html_billing += '</tr>';
                        html_time +='<option value="'+index+'" data-year="'+value+'" data-domain="'+data.domain.domain+'">'+ value +' Năm</option>';
                    }
                });
                $('input[name="domain"]').val(data.domain.domain);
                $('.add-billing').html(html_billing);
                $('.add-domain').html(html);
                $('.time').html(html_time);
                $('.show-price').show();
                $('.loading').html('');
            },
            error: function(e) {
                $('.loading').css('opacity', '1');
                $('.loading').html('Không hiển thị được giá sản phẩm');
            },

        });
    });
    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

</script>
@endsection
