@extends('layouts.app')
@section('style')
<link rel="stylesheet" type="text/css" href="{{ url('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('title')
    <i class="nav-icon fas fa-tachometer-alt"></i>Dashboard
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="alert alert-dismissible alert-pavietnam" style="display: none; color: #721c24; background-color: #f8d7da; border-color: #f5c6cb;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-exclamation-triangle"></i> Chú ý!</h5>
            Tài khoản ký quĩ trên PA còn <span class="text-danger  show-money-pavietnam"></span> đ. Nạp thêm tiền để tránh gián đoạn giao dịch
        </div>
    </div>
</div>
<div class="title">
    <div class="title-body">
        <div class="row">
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-info">
        <div class="inner siderbar_top" id="total_order_pending"></div>
        <div class="icon">
          <i class="fas fa-shopping-cart"></i>
        </div>
        <a href="{{ route('admin.orders.index')  }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-default">
        <div class="inner siderbar_top" id="total_order_confirm"></div>
        <div class="icon">
          <i class="fas fa-shopping-cart"></i>
        </div>
        <a href="{{ route('admin.orders.index')  }}" class="small-box-footer text-dark">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-success" >
        <div class="inner siderbar_top" id="total_vps_before_terminatio"></div>
        <div class="icon">
          <i class="fas fa-calendar fa-2x text-gray-300"></i>
        </div>
        <a href="{{ route('admin.orders.index')  }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-danger">
        <div class="inner siderbar_top" id="total_vps_us_before_termination"></div>
        <div class="icon">
          <i class="fas fa-times"></i>
        </div>
        <a href="{{ route('admin.orders.index')  }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-warning">
        <div class="inner siderbar_top" id="total_server_before_termination">
        </div>
        <div class="icon">
          <i class="fas fa-dollar-sign"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-primary">
        <div class="inner siderbar_top" id="total_colo_before_termination">
        </div>
        <div class="icon">
          <i class="fas fa-comments"></i>
        </div>
        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-success">
        <div class="inner siderbar_top" id="total_hosting_before_termination">
        </div>
        <div class="icon">
          <i class="fas fa-calendar fa-2x text-gray-300"></i>
        </div>
        <a href="{{ route('admin.orders.index')  }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <!-- ./col -->
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-danger">
        <div class="inner siderbar_top" id="total_email_hosting_termination">
        </div>
        <div class="icon">
          <i class="fas fa-times"></i>
        </div>
        <a href="{{ route('admin.orders.index')  }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
</div>
<div class="row" id="chart-dashboard">
    <div class="col-md-12">
      <!-- PIE CHART -->
      <div class="card card-danger">
        <div class="card-header">
          <h3 class="card-title">Thông tin dịch vụ</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6 mb-4">
              <canvas id="pieChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
            </div>
            <div class="col-md-6">
              <table class="table table table-hover table-bordered"  id="list_services_on">
                  <thead class="primary">
                    <th>Loại</th>
                    <th>Tổng cộng</th>
                    <th>Đang sử dụng</th>
                    <th>Hết hạn</th>
                  </thead>
                  <tbody>
                  </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    <div class="col-md-12 mb-4">
      <!-- PIE CHART -->
      <div class="card card-info">
        <div class="card-header">
          <h3 class="card-title">Tổng thu</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-6 mb-4">
              <canvas id="pieChartTotal" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
            </div>
            <div class="col-md-6">
              <table class="table table table-hover table-bordered" id="tong_thu">
                  <thead class="primary">
                    <th>Loại</th>
                    <th>Tổng cộng</th>
                    <th>Phần trăm</th>
                  </thead>
                  <tbody>

                  </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
      <!-- đồ thị user -->
      <div class="card card-success">
        <div class="card-header">
          <h3 class="card-title">Số lượng người đăng ký mới</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
               <button type="button" class="btn btn-danger" data-toggle="collapse" id="last_week">Tuần trước</button>
               <button type="button" class="btn btn-danger" id="last_month">Tháng trước</button>
               <button type="button" class="btn btn-primary" id="this_week" disabled>Tuần này</button>
               <button type="button" class="btn btn-primary" id="this_month">Tháng này</button>
               <button type="button" class="btn btn-info" id="sort">Xắp xếp</button>
               <button type="button" class="btn btn-success" id="custom">Tùy chọn</button>
            </div>
            <div class="col-md-12 row mt-4" id="set_sort">

            </div>
          </div>
          <div class="row">
            <div class="col-md-12 mb-4">
                <div class="print_chart_user">

                </div>
                <div class="errorPieChartUser">

                </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
      <!-- đồ thị VPS tạo mới -->
      <div class="card card-danger">
        <div class="card-header">
          <h3 class="card-title">Số lượng VPS đăng ký mới</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
               <button type="button" class="btn btn-danger" data-toggle="collapse" id="last_week_vps">Tuần trước</button>
               <button type="button" class="btn btn-danger" id="last_month_vps">Tháng trước</button>
               <button type="button" class="btn btn-primary" id="this_week_vps" disabled>Tuần này</button>
               <button type="button" class="btn btn-primary" id="this_month_vps">Tháng này</button>
               <button type="button" class="btn btn-info" id="sort_vps">Xắp xếp</button>
               <button type="button" class="btn btn-success" id="custom_vps">Tùy chọn</button>
            </div>
            <div class="col-md-12 row mt-4" id="set_sort_vps">

            </div>
          </div>
          <div class="row">
            <div class="col-md-12 mb-4">
                <div class="print_chart_vps">

                </div>
                <div class="errorPieChartVps">

                </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
      <!-- đồ thị Hosting tạo mới -->
      <div class="card card-danger">
        <div class="card-header">
          <h3 class="card-title">Số lượng Hosting đăng ký mới</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
               <button type="button" class="btn btn-danger" data-toggle="collapse" id="last_week_hosting">Tuần trước</button>
               <button type="button" class="btn btn-danger" id="last_month_hosting">Tháng trước</button>
               <button type="button" class="btn btn-primary" id="this_week_hosting" disabled>Tuần này</button>
               <button type="button" class="btn btn-primary" id="this_month_hosting">Tháng này</button>
               <button type="button" class="btn btn-info" id="sort_hosting">Xắp xếp</button>
               <button type="button" class="btn btn-success" id="custom_hosting">Tùy chọn</button>
            </div>
            <div class="col-md-12 row mt-4" id="set_sort_hosting">

            </div>
          </div>
          <div class="row">
            <div class="col-md-12 mb-4">
                <div class="print_chart_hosting">

                </div>
                <div class="errorPieChartHosting">

                </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
      <!-- đồ thị Hosting tạo mới -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Biểu đồ thông kê khách hàng nạp tiền</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
          </div>
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
               <button type="button" class="btn btn-danger" data-toggle="collapse" id="last_week_payment">Tuần trước</button>
               <button type="button" class="btn btn-danger" id="last_month_payment">Tháng trước</button>
               <button type="button" class="btn btn-primary" id="this_week_payment" disabled>Tuần này</button>
               <button type="button" class="btn btn-primary" id="this_month_payment">Tháng này</button>
               <button type="button" class="btn btn-info" id="sort_payment">Xắp xếp</button>
               <button type="button" class="btn btn-success" id="custom_payment">Tùy chọn</button>
            </div>
            <div class="col-md-12 row mt-4" id="set_sort_payment">

            </div>
          </div>
          <div class="row">
            <div class="col-md-12 mb-4">
                <div class="print_chart_payment">

                </div>
                <div class="errorPieChartPayment">

                </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
</div>
<input type="hidden" id="page" value="home">
@endsection
@section('scripts')
<script src="{{ asset('/libraries/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('/libraries/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('/libraries/chart.js/Chart.min.js') }}"></script>
<script src="{{ url('libraries/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('/js/home_admin.js') }}"></script>
<script src="{{ asset('/js/alert-pavietnam.js') }}"></script>
@endsection
