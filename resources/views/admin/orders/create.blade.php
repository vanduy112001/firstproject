@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
    <i class="nav-icon  fas fa-shopping-cart"></i> Quản lý đơn đặt hàng
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Đơn đặt hàng</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" style="margin-top:20px">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper dt-bootstrap">
            <form action="{{ route('admin.orders.store') }}" method="post">
                <div class="hidden">
                    @csrf
                    <input type="hidden" name="total" id="form_total">
                    <input type="hidden" name="sub_total" id="form_sub_total">
                </div>
                <div class="row">
                    <div class="col col-md-8">
                        <h3 class="order-title">Tạo đơn đặt hàng</h3>
                        <div class="create_order">
                            <div class="notification mt-4 text-center">

                            </div>
                            <div class="row form-group mt-4 ">
                                <div class="col col-md-3 text-right">
                                    <label for="user">Chọn khách hàng</label>
                                </div>
                                <div class="col col-md-8 order-right">
                                    <select name="user_id" id="user" class="select2 form-control" data-placeholder="Chọn khách hàng" style="width: 100%;">
                                        <option disabled selected>Chọn khách hàng</option>
                                        @foreach ($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3 text-right">
                                    <label for="promotion">Mã giảm giá</label>
                                </div>
                                <div class="col col-md-8 order-right">
                                    <select name="promotion_id" id="promotion" class="select2 form-control" data-placeholder="Chọn mã giảm giá" style="width: 100%;">
                                        <option value="0">None</option>
                                        @foreach ($promotions as $promotion)
                                            <option value="{{ $promotion->id }}">{{ $promotion->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3 text-right">
                                    <label for="order_status">Trạng thái</label>
                                </div>
                                <div class="col col-md-8 order-right">
                                    <select name="status" id="order_status" class="select2 form-control" data-placeholder="Chọn trạng thái" style="width: 100%;">
                                        @foreach ($type_orders as $type_order => $value)
                                            <option value="{{ $type_order }}">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3 text-right ">
                                    <label for="type_products">Loại sản phẩm / Dịch vụ</label>
                                </div>
                                <div class="col col-md-8 order-right">
                                    <select name="type_products" id="type_products" class="select2 form-control" style="width: 100%;">
                                        <option disabled selected>Chọn loại sản phẩm</option>
                                        @foreach ($type_products as $type_product)
                                            <option value="{{ $type_product }}">{{ $type_product }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h3 class="order-product-service">Sản phẩm / Dịch vụ</h3>
                        <div class="order order_product">

                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div id="ordersumm">
                            <div class="ordersummarytitle">
                                Tóm tắt đơn hàng
                            </div>
                            <div id="ordersummary">
                                <div class="ordersummary_product">
                                    <div class="text-none">
                                        Không có sản phẩm được chọn
                                    </div>
                                </div>
                                <div class="addon-item-cpu">

                                </div>
                                <div class="addon-item-ram">

                                </div>
                                <div class="addon-item-disk">

                                </div>
                                <div class="addon-item-storage">

                                </div>
                                <div class="addon-item-database">

                                </div>
                                <div class="sub_total">
                                    <div class="row">
                                        <div class="col col-md-3">
                                            Sub Total
                                            <input type="hidden" id="sub_total" name="sub_total" value="0">
                                        </div>
                                        <div class="col col-md-9 sub_total_pricing">
                                            0 VNĐ
                                        </div>
                                    </div>
                                </div>
                                <div class="total">
                                    <div class="row">
                                        <div class="col col-md-3">
                                            Total
                                        </div>
                                        <div class="col col-md-9 total_pricing">
                                            0 VNĐ
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="button_submit text-center">
                            <input type="submit" class="btn btn-primary" value="Tạo đơn hàng">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
    <input type="hidden" id="page" value="create_order">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}" defer></script>
<script src="{{ asset('js/create_order.js') }}"></script>
@endsection
