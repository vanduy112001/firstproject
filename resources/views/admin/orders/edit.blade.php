@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    <i class="nav-icon  fas fa-shopping-cart"></i> Quản lý đơn đặt hàng
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Đơn đặt hàng</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger" style="margin-top:20px">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper dt-bootstrap">
            <form action="{{ route('admin.orders.update') }}" method="post">
                <div class="hidden">
                    @csrf
                    <input type="hidden" name="total" id="form_total" value="{{$order->total}}">
                    <input type="hidden" name="sub_total" id="form_sub_total" value="{{$order_detail->sub_total}}">
                    <input type="hidden" name="order_id" value="{{$order->id}}">
                    <input type="hidden" name="detail_order_id" value="{{$order_detail->id}}">
                </div>
                <div class="row">
                    <div class="col col-md-8">
                        <h3 class="order-title">Chỉnh sửa đơn đặt hàng</h3>
                        <div class="order">
                            <div class="row form-group">
                                <div class="col col-md-3 text-right">
                                    <label for="user">Tên khách hàng</label>
                                </div>
                                <div class="col col-md-8 order-right">
                                    <select name="user_id" id="user" class="select2 form-control" data-placeholder="Chọn khách hàng" style="width: 100%;">
                                        @foreach ($users as $user)
                                            @php
                                                $selected = '';
                                                if ($user->id == $order->user_id) {
                                                    $selected = 'selected';
                                                }
                                            @endphp
                                            <option value="{{ $user->id }}" {{$selected}}>{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3 text-right">
                                    <label for="promotion">Mã giảm giá</label>
                                </div>
                                <div class="col col-md-8 order-right">
                                    <select name="promotion_id" id="promotion" class="select2 form-control" data-placeholder="Chọn mã giảm giá" style="width: 100%;">
                                        <option value="0">None</option>
                                        @foreach ($promotions as $promotion)
                                            @php
                                                $selected = '';
                                                if ($promotion->id == $order->promotion_id) {
                                                    $selected = 'selected';
                                                }
                                            @endphp
                                            <option value="{{ $promotion->id }}" {{$selected}}>{{ $promotion->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3 text-right">
                                    <label for="order_status">Trạng thái</label>
                                </div>
                                <div class="col col-md-8 order-right">
                                    <select name="status" id="order_status" class="select2 form-control" data-placeholder="Chọn trạng thái" style="width: 100%;">
                                        @foreach ($type_orders as $type_order => $value)
                                            @php
                                                $selected = '';
                                                if ($type_order == $order->status) {
                                                    $selected = 'selected';
                                                }
                                            @endphp
                                            <option value="{{ $type_order }}" {{$selected}}>{{ $type_order }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3 text-right ">
                                    <label for="type_products">Loại sản phẩm</label>
                                </div>
                                <div class="col col-md-8 order-right">
                                    <select name="type_products" id="type_products" class="select2 form-control" data-placeholder="Chọn loại sản phẩm" style="width: 100%;">
                                        <option disabled selected>Chọn loại sản phẩm</option>
                                        @foreach ($type_products as $type_product)
                                            @php
                                                $selected = '';
                                                if ($type_product == $order_detail->type) {
                                                    $selected = 'selected';
                                                }
                                            @endphp
                                            <option value="{{ $type_product }}" {{$selected}}>{{ $type_product }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <h3 class="order-product-service">Sản phẩm / Dịch vụ</h3>
                        <div class="order order_product">
                           <div class="row form-group">
                                <div class="col col-md-3 text-right ">
                                    <label for="type_products">Sản phẩm</label>
                                </div>
                                <div class="col col-md-8 order-right">
                                    <select name="product_id" id="product_id" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Hosting">
                                        <option disabled selected>Chọn sản phẩm</option>
                                        @foreach ($products as $product)
                                            @php
                                                $selected = '';
                                                if( $order_detail->type == 'VPS' ) {
                                                    if ($product->id == $order_detail->vps[0]->product_id) {
                                                        $selected = 'selected';
                                                    }
                                                } else if ($order_detail->type == 'Server') {
                                                    if ($product->id == $order_detail->servers[0]->product_id) {
                                                        $selected = 'selected';
                                                    }
                                                } else {
                                                    if ($product->id == $order_detail->hostings[0]->product_id) {
                                                        $selected = 'selected';
                                                    }
                                                }

                                            @endphp
                                            <option value="{{$product->id}}" {{$selected}}>{{$product->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                           </div>
                            @if ($order_detail->type == 'Hosting')
                                <div class="row form-group">
                                    <div class="col col-md-3 text-right ">
                                        <label for="domain">Domain</label>
                                    </div>
                                    <div class="col col-md-8 order-right domain">
                                        @foreach ($order_detail->hostings as $key => $hosting)
                                            <input id="domain" name="domain[]" class="form-control" type="text" placeholder="Domain {{$key + 1}}" value="{{$hosting->domain}}">
                                            <input type="hidden" name="hosting_id[]" value="{{$hosting->id}}">
                                        @endforeach
                                    </div>
                                </div>
                            @endif
                            <div class="row form-group">
                                <div class="col col-md-3 text-right ">
                                    <label for="billing_cycle">Thời gian</label>
                                </div>
                                <div class="col col-md-8 order-right">
                                    <select name="billing_cycle" id="billing_cycle" class="select2 form-control"  style="width: 100%;">
                                        <option value="" disabled>Chọn thời gian</option>
                                        @foreach ($billings as $key => $billing)
                                            @php
                                                $selected = '';
                                                if ($key == $order_detail->billing_cycle) {
                                                    $selected = 'selected';
                                                }
                                            @endphp
                                            <option value="{{ $key }}" {{$selected}}>{{ $billing }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col col-md-3 text-right ">
                                    <label for="quantity_hosting">Số lượng</label>
                                </div>
                                <div class="col col-md-8 order-right">
                                    @if($order_detail->type == 'Hosting')
                                        <input id="quantity_hosting" name="quantity" class="form-control" type="text" placeholder="Số lượng" value="{{$order_detail->quantity}}">
                                    @else
                                        <input id="quantity" name="quantity" class="form-control" type="text" placeholder="Số lượng" value="{{$order_detail->quantity}}">
                                    @endif
                                </div>
                            </div>
                            @if ( $order_detail->type == 'VPS' )
                                <div class="row form-group">
                                    <div class="col col-md-3 text-right ">
                                        <label for="os">Hệ điều hành</label>
                                    </div>
                                    <div class="col col-md-8 order-right">
                                        <input id="os" name="os" class="form-control" type="text" placeholder="Hệ điều hành" value="{{$order_detail->vps[0]->os}}">
                                    </div>
                                </div>
                            @endif
                            @if ( $order_detail->type == 'Server' )
                                <div class="row form-group">
                                    <div class="col col-md-3 text-right ">
                                        <label for="os">Hệ điều hành</label>
                                    </div>
                                    <div class="col col-md-8 order-right">
                                        <input id="os" name="os" class="form-control" type="text" placeholder="Hệ điều hành" value="{{$order_detail->servers[0]->os}}">
                                    </div>
                                </div>
                            @endif
                            @if ($order_detail->type == 'Hosting')
                                @foreach ($order_detail->hostings as $key => $hosting)
                                <div class="row form-group">
                                    <div class="col col-md-3 text-right ">
                                        <label>Tài khoản hosting {{ $key + 1 }}</label>
                                    </div>
                                    <div class="col col-md-8 order-right">
                                        <input name="user[]" class="form-control" type="text" placeholder="Mật khẩu" value="{{$hosting->user}}">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-3 text-right ">
                                        <label>Mật khẩu</label>
                                    </div>
                                    <div class="col col-md-8 order-right">
                                        <input name="password[]" class="form-control" type="text" placeholder="Mật khẩu" value="{{$hosting->password}}">
                                    </div>
                                </div>
                                @endforeach
                            @elseif ( $order_detail->type == 'VPS' )
                                @foreach ($order_detail->vps as $key => $vps)
                                    <div class="row form-group">
                                        <div class="col col-md-3 text-right ">
                                            <label>Tài khoản VPS {{ $key + 1 }}</label>
                                        </div>
                                        <div class="col col-md-8 order-right">
                                            <input name="user[]" class="form-control" type="text" placeholder="Mật khẩu" value="{{$vps->user}}">
                                            <input type="hidden" name="vps_id[]" value="{{ $vps->id }}">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3 text-right ">
                                            <label>Mật khẩu</label>
                                        </div>
                                        <div class="col col-md-8 order-right">
                                            <input name="password[]" class="form-control" type="text" placeholder="Mật khẩu" value="{{$vps->password}}">
                                        </div>
                                    </div>
                                @endforeach
                            @else
                                @foreach ($order_detail->servers as $key => $server)
                                    <div class="row form-group">
                                        <div class="col col-md-3 text-right ">
                                            <label>Tài khoản server {{ $key + 1 }}</label>
                                        </div>
                                        <div class="col col-md-8 order-right">
                                            <input name="user[]" class="form-control" type="text" placeholder="Mật khẩu" value="{{$server->user}}">
                                            <input type="hidden" name="server_id[]" value="{{ $server->id }}">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-3 text-right ">
                                            <label>Mật khẩu</label>
                                        </div>
                                        <div class="col col-md-8 order-right">
                                            <input name="password[]" class="form-control" type="text" placeholder="Mật khẩu" value="{{$server->password}}">
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="col col-md-4">
                        <div id="ordersumm">
                            <div class="ordersummarytitle">
                                Tóm tắt đơn hàng
                            </div>
                            <div id="ordersummary">
                                <div class="ordersummary_product">
                                    <div class="text-none">
                                        Không có sản phẩm được chọn
                                    </div>
                                </div>
                                <div class="sub_total">
                                    <div class="row">
                                        <div class="col col-md-3">
                                            Sub Total
                                        </div>
                                        <div class="col col-md-9 sub_total_pricing">
                                            {!!number_format($order_detail->sub_total,0,",",".") . ' VNĐ'!!}
                                        </div>
                                    </div>
                                </div>
                                <div class="total">
                                    <div class="row">
                                        <div class="col col-md-3">
                                            Total
                                        </div>
                                        <div class="col col-md-9 total_pricing">
                                            {!!number_format($order->total,0,",",".") . ' VNĐ'!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="button_submit text-center">
                            <input type="submit" class="btn btn-primary" value="Chỉnh sửa đơn hàng">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('js/edit_order.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
    });


</script>
@endsection
