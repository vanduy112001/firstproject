@extends('layouts.app')
@section('style')
<link href="{{ asset('/libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('libraries/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('title')
    <i class="nav-icon  fas fa-shopping-cart"></i> Quản lý đơn đặt hàng
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Đơn đặt hàng</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-6">
                <a href="{{ route('admin.orders.create') }}" class="btn btn-primary">Tạo đơn đặt hàng</a>
                <button type="button" name="button" class="btn btn-info" id="type_user">Khách hàng cá nhân</button>
                <button type="button" name="button" class="btn btn-danger" id="type_enterprise">Khách hàng doanh nghiệp</button>
            </div>
            <!-- form search -->
            <div class="col-md-6">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("error"))
                    <div class="bg-danger">
                        <p class="text-light">{!! session("error") !!}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table id="table_order" class="table table-hover table-bordered">
                <thead class="primary">
                    <th>ID</th>
                    <th>Ngày đặt hàng</th>
                    <th id="user_invoice">
                      Tên khách hàng
                      <span class="float-right">
                        <i class="fas fa-filter"></i>
                        <input type="hidden" id="input_filter_user" value="">
                      </span>
                    </th>
                    <th>Total</th>
                    <th>Chi tiết</th>
                    <th id="status_invoice">
                      Trạng thái
                      <span class="float-right">
                        <i class="fas fa-filter"></i>
                        <input type="hidden" id="input_filter_status" value="all">
                      </span>
                    </th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                    @if (isset($orders))
                        @foreach ($orders as $key => $order)
                            <tr>
                                <td>
                                  @if(!empty($order->detail_orders[0]->id))
                                    <a href="{{ route('admin.invoices.edit', $order->detail_orders[0]->id) }}" data-id="{{ $order->detail_orders[0]->id }}" class="detail_invoice">{{$order->id}}</a></td>
                                  @else
                                    {{$order->id}}
                                  @endif
                                <td>{{ date('H:i:s d-m-Y', strtotime($order->created_at)) }}</td>
                                <td>
                                    @if(!empty($order->user->name))
                                      <a href="{{ route('admin.user.detail', $order->user->id) }}">{{ $order->user->name }}</a>
                                    @else
                                      'Đã xóa'
                                    @endif
                                </td>
                                <td>
                                  {!!number_format($order->total,0,",",".") . ' đ'!!}
                                  <span class="ml-2">
                                    <!-- <button type="button" name="button" class="btn btn-secondary button_edit_customer" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></button> -->
                                    <a href="#" class="text-secondary ml-2 button_edit_total_order" data-id="{{ $order->id }}" data-toggle="tooltip" title="Chỉnh sửa giá đơn đặt hàng"><i class="fas fa-edit"></i></a>
                                  </span>
                                </td>
                                <td>
                                    @if ( $order->type == 'expired' )
                                      @if ( !empty($order->detail_orders) )
                                        @foreach ( $order->detail_orders as $detail_order )
                                            @if ( $detail_order->type == 'VPS' )
                                              @if ( !empty($detail_order->order_expireds) )
                                                @foreach ( $detail_order->order_expireds as $order_expired )
                                                    Gia hạn VPS - <a target="_blank" href="{{ route('admin.vps.detail' , $order_expired->expired_id ) }}">
                                                      @if ( !empty($order_expired->vps->ip) )
                                                        {{ $order_expired->vps->ip }}
                                                      @else
                                                        <b class="text-danger">đã xóa</b>
                                                      @endif
                                                    </a> <br>
                                                @endforeach
                                              @endif
                                            @elseif ( $detail_order->type == 'VPS US' || $detail_order->type == ' US' )
                                              {{-- dd($detail_order->type); --}}
                                              @if ( !empty($detail_order->order_expireds) )
                                                @foreach ( $detail_order->order_expireds as $order_expired )
                                                    Gia hạn VPS US - <a target="_blank" href="{{ route('admin.vps.detail' , $order_expired->expired_id ) }}">
                                                      @if ( !empty($order_expired->vps->ip) )
                                                        {{ $order_expired->vps->ip }}
                                                      @else
                                                        <b class="text-danger">đã xóa</b>
                                                      @endif
                                                    </a> <br>
                                                @endforeach
                                              @endif
                                            @elseif ( $detail_order->type == 'hosting' )
                                              @if ( !empty($detail_order->order_expireds) )
                                                @foreach ( $detail_order->order_expireds as $order_expired )
                                                    Gia hạn Hosting - <a target="_blank" href="{{ route('admin.hosting.detail' , $order_expired->expired_id ) }}">
                                                      @if ( !empty($order_expired->hosting->domain) )
                                                        {{ $order_expired->hosting->domain }}
                                                      @else
                                                        <b class="text-danger">đã xóa</b>
                                                      @endif
                                                    </a> <br>
                                                @endforeach
                                              @endif
                                            @elseif ( $detail_order->type == 'Email Hosting' )
                                              @if ( !empty($detail_order->order_expireds) )
                                                @foreach ( $detail_order->order_expireds as $order_expired )
                                                    Gia hạn Email Hosting - <a target="_blank" href="{{ route('admin.email_hosting.detail' , $order_expired->expired_id ) }}">
                                                      @if ( !empty($order_expired->email_hosting->domain) )
                                                        {{ $order_expired->email_hosting->domain }}
                                                      @else
                                                        <b class="text-danger">đã xóa</b>
                                                      @endif
                                                    </a> <br>
                                                @endforeach
                                              @endif
                                            @elseif ( $detail_order->type == 'Domain' )
                                              @if ( !empty($detail_order->order_expireds) )
                                                @foreach ( $detail_order->order_expireds as $order_expired )
                                                    Gia hạn Domain - <a target="_blank" href="{{ route('admin.domain.detail' , $order_expired->expired_id ) }}">
                                                      @if ( !empty($order_expired->domain->domain) )
                                                        {{ $order_expired->domain->domain }}
                                                      @else
                                                        <b class="text-danger">đã xóa</b>
                                                      @endif
                                                    </a> <br>
                                                @endforeach
                                              @endif
                                            @elseif ( $detail_order->type == 'Server' )
                                              @if ( !empty($detail_order->order_expireds) )
                                                @foreach ( $detail_order->order_expireds as $order_expired )
                                                    Gia hạn Server - <a target="_blank" href="{{ route('admin.server.detail' , $order_expired->expired_id ) }}">
                                                      {{ !empty($order_expired->server->ip) ? $order_expired->server->ip : 'không có' }}
                                                    </a> <br>
                                                @endforeach
                                              @endif
                                            @elseif ( $detail_order->type == 'Colocation' )
                                              @if ( !empty($detail_order->order_expireds) )
                                                @foreach ( $detail_order->order_expireds as $order_expired )
                                                    Gia hạn Colocation - <a target="_blank" href="{{ route('admin.colocation.detail' , $order_expired->expired_id ) }}">
                                                      {{ !empty($order_expired->colocation->ip) ? $order_expired->colocation->ip : 'không có' }}
                                                    </a> <br>
                                                @endforeach
                                              @endif
                                            @else
                                              {{-- dd($detail_order->type); --}}
                                            @endif
                                        @endforeach
                                      @endif
                                    @else
                                       @if ( !empty($order->detail_orders) )
                                          @foreach ( $order->detail_orders as $detail_order )
                                            @if ( $detail_order->type == 'addon_vps' )
                                                @if ( !empty($detail_order->order_addon_vps) )
                                                    @foreach ( $detail_order->order_addon_vps as $order_addon_vps )
                                                        Cấu hình thêm VPS - <a target="_blank" href="{{ route('admin.vps.detail' , $order_addon_vps->vps_id ) }}">
                                                          @if ( !empty($order_addon_vps->vps->ip) )
                                                            {{ $order_addon_vps->vps->ip }}
                                                          @else
                                                            Đã xóa
                                                          @endif
                                                        </a> <br>
                                                    @endforeach
                                                @endif
                                            @elseif ( $detail_order->type == 'addon_server' )
                                                @if ( !empty($detail_order->order_addon_servers) )
                                                    @foreach ( $detail_order->order_addon_servers as $order_addon_server )
                                                        Nâng cấp cấu hình Server - <a target="_blank" href="{{ route('admin.server.detail' , $order_addon_server->server_id ) }}">
                                                          @if ( !empty($order_addon_server->server->ip) )
                                                            {{ $order_addon_server->server->ip }}
                                                          @else
                                                            Đã xóa
                                                          @endif
                                                        </a> <br>
                                                    @endforeach
                                                @endif
                                            @elseif ( $detail_order->type == 'change_ip' )
                                                @if ( !empty($detail_order->order_change_vps) )
                                                    @foreach ( $detail_order->order_change_vps as $order_change_vps )
                                                        Đổi IP VPS - <a target="_blank" href="{{ route('admin.vps.detail' , $order_change_vps->vps_id ) }}">
                                                          @if ( !empty($order_change_vps->vps->ip ) )
                                                            {{ $order_change_vps->vps->ip }}
                                                          @else
                                                            Đã xóa
                                                          @endif
                                                        </a> <br>
                                                    @endforeach
                                                @endif
                                            @elseif ( $detail_order->type == 'VPS' )
                                                @if ( !empty($detail_order->vps) )
                                                    @foreach ( $detail_order->vps as $vps )
                                                        Tạo VPS - <a target="_blank" href="{{ route('admin.vps.detail' , $vps->id ) }}">{{ $vps->ip }}</a> <br>
                                                    @endforeach
                                                @endif
                                            @elseif ( $detail_order->type == 'VPS-US' )
                                                @if ( !empty($detail_order->vps) )
                                                    @foreach ( $detail_order->vps as $vps )
                                                        Tạo VPS US - <a target="_blank" href="{{ route('admin.vps.detail' , $vps->id ) }}">{{ $vps->ip }}</a> <br>
                                                    @endforeach
                                                @endif
                                            @elseif ( $detail_order->type == 'Email Hosting' )
                                                @if ( !empty($detail_order->email_hostings) )
                                                    @foreach ( $detail_order->email_hostings as $email_hosting )
                                                        Tạo Email Hosting - <a target="_blank" href="{{ route('admin.email_hosting.detail' , $email_hosting->id ) }}">{{ $email_hosting->domain }}</a> <br>
                                                    @endforeach
                                                @endif
                                            @elseif ( $detail_order->type == 'Server' )
                                                @if ( !empty($detail_order->servers) )
                                                    @foreach ( $detail_order->servers as $servers )
                                                        Tạo Server - <a target="_blank" href="{{ route('admin.vps.detail' , $servers->id ) }}">{{ $servers->ip }}</a> <br>
                                                    @endforeach
                                                @endif
                                            @elseif ( $detail_order->type == 'Colocation' )
                                                @if ( !empty($detail_order->colocations) )
                                                    @foreach ( $detail_order->colocations as $colocation )
                                                        Tạo Colocation - <a target="_blank" href="{{ route('admin.colocation.detail' , $colocation->id ) }}">{{ $colocation->ip }}</a> <br>
                                                    @endforeach
                                                @endif
                                            @elseif ( $detail_order->type == 'NAT-VPS' )
                                                @if ( !empty($detail_order->vps) )
                                                    @foreach ( $detail_order->vps as $vps )
                                                        Tạo NAT VPS - <a target="_blank" href="{{ route('admin.vps.detail' , $vps->id ) }}">{{ $vps->ip }}</a> <br>
                                                    @endforeach
                                                @endif
                                            @elseif ( $detail_order->type == 'Hosting' || $detail_order->type == 'Hosting-Singapore' )
                                                @if ( !empty($detail_order->hostings) )
                                                    @foreach ( $detail_order->hostings as $hosting )
                                                        Tạo Hosting - <a target="_blank" href="{{ route('admin.hosting.detail' , $hosting->id ) }}">{{ $hosting->domain }}</a> <br>
                                                    @endforeach
                                                @endif
                                            @elseif ( $detail_order->type == 'Domain' )
                                                @if ( !empty($detail_order->domains) )
                                                    @foreach ( $detail_order->domains as $domain )
                                                        Tạo Hosting - <a target="_blank" href="{{ route('admin.domain.detail' , $domain->id ) }}">{{ $domain->domain }}</a> <br>
                                                    @endforeach
                                                @endif
                                            @elseif ( $detail_order->type == 'upgrade_hosting' )
                                                @if ( !empty($detail_order->order_upgrade_hosting) )
                                                    Nâng cấp Hosting - <a target="_blank" href="{{ route('admin.hosting.detail' , $detail_order->order_upgrade_hosting->hosting_id ) }}">
                                                      @if ( !empty($detail_order->order_upgrade_hosting->hosting->domain ) )
                                                        {{ $detail_order->order_upgrade_hosting->hosting->domain }}
                                                      @else
                                                        Đã xóa
                                                      @endif
                                                    </a> <br>
                                                @endif
                                            @endif
                                          @endforeach
                                       @endif
                                    @endif
                                </td>
                                <td>
                                    @if ($order->status == 'Active')
                                        <span class="text-info">Đang chờ tạo</span>
                                    @elseif ($order->status == 'Pending')
                                        <span class="text-danger">Chưa thanh toán</span>
                                    @elseif ($order->status == 'Finish')
                                        <span class="text-success">Đã thanh toán</span>
                                    @else
                                        <span class="text-default">{{ $order->status }}</span>
                                    @endif
                                </td>
                                <td class="button-action">
                                    @if(!empty($order->detail_orders[0]->id))
                                        <a href="{{ route('admin.invoices.edit', $order->detail_orders[0]->id) }}" data-id="{{ $order->detail_orders[0]->id }}" class="detail_invoice btn btn-warning"><i class="fas fa-edit text-white"></i></a>
                                    @endif
                                    <button class="btn btn-danger delete-order" data-id="{{ $order->id }}" data-n="{{$key}}"><i class="fas fa-trash-alt"></i></button>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <td colspan="7" class="text-center text-danger">Không có đơn đặt hàng</td>
                    @endif
                </tbody>
                <tfoot class="card-footer clearfix">
                    <td colspan="7" class="text-center">
                        {{ $orders->links() }}
                    </td>
                </tfoot>
            </table>
        </div>
    </div>
</div>
{{-- Modal xoa order --}}
<div class="modal fade" id="delete-order">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-order" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-order" value="Xóa đơn hàng">
          <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
{{-- Modal confirm --}}
<div class="modal fade" id="confirm-order">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xác nhận đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-confirm-order" class="text-center">

            </div>
            <div id="content-confirm-order">
            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-confirm-order" value="Xác nhận">
          <button type="button" class="btn btn-danger" id="button-confirm-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
  {{-- Modal xoa invoice --}}
  <div class="modal fade" id="detail_invoice">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Chi tiết đơn đặt hàng</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div id="notication-detail">

              </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default float-right" data-dismiss="modal">Tắt</button>
          </div>
        </div>
        <!-- /.modal-content -->
    </div>
      <!-- /.modal-dialog -->
  </div>
    <!-- /.modal -->
    <input type="hidden" id="page" value="list_order">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('libraries/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('libraries/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('js/order.js') }}"></script>
<script>
    $(document).ready(function () {
      $('[data-toggle="tooltip"]').tooltip();
        $('.delete-order').on('click', function () {
            $('#delete-order').modal('show');
            var id = $(this).attr("data-id");
            var n = $(this).attr("data-n");
            var html = "Bạn có muốn xóa đơn hàng (<b class='text-danger'>ID: " + id + "</b>) này không?";
            $('#button-order').attr('data-id', id);
            $('#button-order').attr('data-n', n);
            $('#notication-order').html(html);
            $('#button-order').fadeIn();
            $('#button-finish').hide();
            $(this).closest('tr').addClass('removeRow');
        });

        $('#button-order').on('click', function() {
            var id = $(this).attr('data-id');
            var n = $(this).attr('data-n');
            $.ajax({
                type: "get",
                url: "{{ route('admin.orders.delete') }}",
                data: {id: id},
                dataType: "json",
                beforeSend: function(){
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#button-order').attr('disabled', true);
                    $('#notication-order').html(html);
                },
                success: function (data) {
                    if (data) {
                        var html = '<p class="text-danger">Xóa đơn đặt hàng thành công</p>';
                        $('#notication-order').html(html);
                        $('#button-order').attr('disabled', false);
                        $('.removeRow').fadeOut(1500);
                    } else {
                        var html = '<p class="text-danger>Xóa đơn đặt hàng thất bại</p>';
                        $('#notication-order').html(html);
                        $('#button-order').attr('disabled', false);
                        $('tr .removeRow').removeClass('removeRow');
                    }
                    $('#button-order').hide();
                    $('#button-finish').fadeIn();
                },
                error: function (e) {
                    console.log(e);
                    var html = '<p class="text-danger">Xóa đơn đặt hàng bị lỗi!</p>';
                    $('#notication-order').html(html);
                    $('#button-order').attr('disabled', false);
                    $('tr .removeRow').removeClass('removeRow');
                    $('#button-order').hide();
                    $('#button-finish').fadeIn();
                }
            });
        });

    });
</script>
@endsection
