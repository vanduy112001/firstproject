@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Quản lý Colocation
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item "><a href="/admin/colocation/">Colocation</a></li>
    <li class="breadcrumb-item active">Chỉnh sửa Colocation</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper dt-bootstrap">
            <h4>Thông tin Colocation {{$detail->ip}}</h4>
            <div class="container">
                <div class="row">
                    <div class="col col-md-6">
                        <form action="{{ route('admin.colocation.update') }}" method="post">
                          <div class="order">
                              <div class="detail_order">
                                  {{-- 1 form group --}}
                                  <div class="group-invoice">
                                      <div class="col col-md-4 text-right">
                                          <label for="">Tên khách hàng:</label>
                                      </div>
                                      <div class="col col-md-8">
                                          <select name="user_id" class="form-control select2">
                                              <option value="" disabled>Chọn khách hàng</option>
                                              @foreach ($users as $user)
                                                  @php
                                                      $selected = '';
                                                      if ($user->id == $detail->user_id) {
                                                          $selected = 'selected';
                                                      }
                                                  @endphp
                                                  <option value="{{$user->id}}" {{$selected}}>{{$user->name}}</option>
                                              @endforeach
                                          </select>
                                      </div>
                                  </div>
                                  {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Sản phẩm / Dịch vụ:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="product_id" id="product_id" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Hosting">
                                                <option disabled selected>Chọn sản phẩm</option>
                                                @foreach ($products as $product)
                                                    @php
                                                        $selected = '';
                                                        if ($product->id == $detail->product_id) {
                                                            $selected = 'selected';
                                                        }
                                                        $group_product = $product->group_product;
                                                        if ($group_product->private == 0) {
                                                            $group_user_name = 'Cơ bản';
                                                        } else {
                                                            $group_user_name = $group_product->group_user->name;
                                                        }
                                                    @endphp
                                                    <option value="{{$product->id}}" {{$selected}}>{{$product->name}} - {{ $group_user_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                  {{-- 1 form group --}}
                                  <div class="group-invoice">
                                      <div class="col col-md-4 text-right">
                                          <label for="">IP:</label>
                                      </div>
                                      <div class="col col-md-8">
                                          <input type="text" name="ip" class="form-control" value="{{$detail->ip}}">
                                          <button type="button" class="btn btn-outline-success collapsed tooggle-plus mt-1" id="btnAddIp" data-toggle="tooltip" data-placement="top" title="Thêm IP">
                                            <i class="fas fa-plus"></i>
                                          </button>
                                      </div>
                                  </div>
                                  <div class="group-invoice" id="addIp">
                                    <div class="col col-md-4 text-right"></div>
                                    <div class="col col-md-8">
                                        <div class="container-ip row mt-1">
                                            @if ( !empty($detail->colocation_ips) )
                                                @foreach ($detail->colocation_ips as $colocation_ip)
                                                    <div class="col-12 pl-0 inpAddIp">
                                                        <div class="form-group input-group">
                                                            <input type="text" name="addIp[]" value="{{ $colocation_ip->ip }}" placeholder="IP" class="form-control">
                                                            <span class="input-group-append">
                                                                <button type="button" class="btn btn-danger btnDeleteAddIp">
                                                                    <i class="fas fa-trash-alt"></i>
                                                                </button>
                                                            </span>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @endif
                                        </div>
                                    </div>
                                  </div>
                                  {{-- 1 form group --}}
                                  <div class="group-invoice">
                                      <div class="col col-md-4 text-right">
                                          <label for="">Loại:</label>
                                      </div>
                                      <div class="col col-md-8">
                                          <select class="form-control" name="type_colo">
                                            <option value="1 U" @if($detail->type_colo == '1 U') selected @endif>1 U</option>
                                            <option value="2 U" @if($detail->type_colo == '2 U') selected @endif>2 U</option>
                                            <option value="4 U" @if($detail->type_colo == '4 U') selected @endif>4 U</option>
                                          </select>
                                      </div>
                                  </div>
                                  {{-- 1 form group --}}
                                  <div class="group-invoice">
                                      <div class="col col-md-4 text-right">
                                          <label for="">Trạng thái:</label>
                                      </div>
                                      <div class="col col-md-8">
                                          <select name="status_colo" id="status_colo" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Hosting">
                                              <option value="on" @if($detail->status_colo == 'on') selected @endif>Đang bật</option>
                                              <option value="off" @if($detail->status_colo == 'off') selected @endif>Đã tắt</option>
                                              <option value="cancel" @if($detail->status_colo == 'cancel') selected @endif>Yêu cầu hủy</option>
                                              <option value="delete_colo" @if($detail->status_colo == 'delete_colo') selected @endif>Đã xóa</option>
                                          </select>
                                      </div>
                                  </div>
                                  {{-- 1 form group --}}
                                  <div class="group-invoice">
                                      <div class="col col-md-4 text-right">
                                          <label for="">Datacenter:</label>
                                      </div>
                                      <div class="col col-md-8">
                                        <select name="location" id="location" class="form-control"  style="width: 100%;">
                                          <option value="Đà Nẵng" @if($detail->location == 'Đà Nẵng') selected @endif>Đà Nẵng</option>
                                          <option value="Hồ Chí Minh" @if($detail->location == 'Hồ Chí Minh') selected @endif>Hồ Chí Minh</option>
                                          <option value="Hà Nội" @if($detail->location == 'Hà Nội') selected @endif>Hà Nội</option>
                                        </select>
                                      </div>
                                  </div>
                                  {{-- 1 form group --}}
                                  <div class="group-invoice">
                                    <div class="col col-md-4 text-right">
                                        <label for="">Vị trí:</label>
                                    </div>
                                    <div class="col col-md-8">
                                        <input type="text" name="rack" class="form-control" value="{{ $detail->rack }}">
                                    </div>
                                  </div>
                                  {{-- 1 form group --}}
                                  {{-- 1 form group --}}
                                  <div class="group-invoice">
                                    <div class="col col-md-4 text-right">
                                        <label for="">Công xuất nguồn:</label>
                                    </div>
                                    <div class="col col-md-8">
                                        <input type="text" name="power" class="form-control" value="{{ $detail->power }}">
                                    </div>
                                  </div>
                                  {{-- 1 form group --}}
                              </div>
                          </div>
                    </div>
                    <div class="col col-md-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Băng thông:</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <input type="text" name="bandwidth" class="form-control" value="{{ $detail->bandwidth }}">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Ngày đặt hàng:</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <input type="text" id="datepicker" name="created_at" value="{{date('d-m-Y', strtotime($detail->date_create))}}" class="form-control">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Ngày hết hạn :</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <input type="text" id="datepicker_next_due_date" name="next_due_date" value="{{date('d-m-Y', strtotime($detail->next_due_date))}}" class="form-control">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Ngày thanh toán :</label>
                                        </div>
                                        <div class="col col-md-8">
                                            @if(!empty($detail->paid))
                                                @if($detail->paid == 'paid')
                                                    <span class="text-success">{{date('m/d/Y', strtotime($detail->detail_order->paid_date))}}</span>
                                                @elseif($detail->paid == 'cancel')
                                                    <span class="text-secondary">Hủy</span>
                                                @else
                                                    <span class="text-danger">Chưa thanh toán</span>
                                                @endif
                                            @else
                                                <span class="text-danger">Chưa thanh toán</span>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Thời gian:</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <select name="billing_cycle" id="billing_cycle" class="select2 form-control"  style="width: 100%;">
                                                <option value="" disabled>Chọn thời gian</option>
                                                @foreach ($billings as $key => $billing)
                                                    @php
                                                        $selected = '';
                                                        if ($key == $detail->billing_cycle) {
                                                            $selected = 'selected';
                                                        }
                                                    @endphp
                                                    <option value="{{ $key }}" {{$selected}}>{{ $billing }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Thành tiền :</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <b>{!!number_format(($detail->amount),0,",",".")!!}</b> VNĐ<br>
                                            <input type="text" value="" class="form-control" name="amount" placeholder="Chỉnh sửa giá dịch vụ">
                                        </div>
                                    </div>
                                    <div class="form-button mt-4 mr-4">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $detail->id }}">
                                        <input type="hidden" name="invoice_id" value="{{ $detail->detail_order_id }}">
                                        <input type="submit" value="Chỉnh sửa" class="btn btn-primary float-right">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Modal xoa order --}}
<div class="modal fade" id="delete-order">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-order" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-order" value="Xóa đơn hàng">
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="page" value="detail_colo">
@endsection
@section('scripts')
<script src="{{ asset('js/edit_invoices.js') }}"></script>
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script type="text/javascript">
	$(document).ready(function() {
        // $('#addIp').fadeOut();
		//Date picker
		$('#datepicker').datepicker({
			autoclose: true,
            format: 'dd-mm-yyyy'
		});
		$('#datepicker_next_due_date').datepicker({
			autoclose: true,
            format: 'dd-mm-yyyy'
		});
        // select
        $('.select2').select2();

        $('#btnAddIp').on('click', function () {
            $('#addIp').fadeIn();
            var html = '';
            html += '<div class="col-12 pl-0 inpAddIp">';
            html += '<div class="form-group input-group">';
            html += '<input type="text" name="addIp[]" value="" placeholder="IP" class="form-control">';
            html += '<span class="input-group-append">';
            html += '<button type="button" class="btn btn-danger btnDeleteAddIp">';
            html += '<i class="fas fa-trash-alt"></i>';
            html += '</button>';
            html += '</span>';
            html += '</div>';
            html += '</div>';
            $('.container-ip').append(html);
        });

        $(document).on('click', '.btnDeleteAddIp', function () {
            $(this).closest('.inpAddIp').remove();
        })
	});
</script>
@endsection
