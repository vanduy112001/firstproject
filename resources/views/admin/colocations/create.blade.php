@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Tạo Colocation
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item "><a href="/admin/colocation/">Colocation</a></li>
    <li class="breadcrumb-item active">Tạo Colocation</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper dt-bootstrap">
            <h4>Thông tin Colocation</h4>
            <div class="container">
                <div class="row">
                    <div class="col col-md-6">
                        <form action="{{ route('admin.colocation.store') }}" method="post">
                          <div class="order">
                              <div class="detail_order">
                                  {{-- 1 form group --}}
                                  <div class="group-invoice">
                                      <div class="col col-md-4 text-right">
                                          <label for="">Tên khách hàng:</label>
                                      </div>
                                      <div class="col col-md-8">
                                          <select name="user_id" id="user_id" class="form-control select2">
                                              <option value="" selected disabled>Chọn khách hàng</option>
                                              @foreach ($users as $user)
                                                  @php
                                                      $selected = '';
                                                      if ($user->id == old('user_id')) {
                                                          $selected = 'selected';
                                                      }
                                                  @endphp
                                                  <option value="{{$user->id}}" {{$selected}}>{{$user->name}}</option>
                                              @endforeach
                                          </select>
                                      </div>
                                  </div>
                                  {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col-md-4 text-right">
                                            <label for="">Sản phẩm / Dịch vụ:</label>
                                        </div>
                                        <div class="col-md-8">
                                            <select name="product_id" id="product_id" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;" data-value="Hosting">
                                                <option disabled selected>Chọn sản phẩm</option>
                                            </select>
                                        </div>
                                    </div>
                                  {{-- 1 form group --}}
                                  <div class="group-invoice">
                                      <div class="col col-md-4 text-right">
                                          <label for="">IP:</label>
                                      </div>
                                      <div class="col col-md-8">
                                          <input type="text" name="ip" class="form-control" value="{{ old('ip') }}">
                                          <button type="button" class="btn btn-outline-success collapsed tooggle-plus mt-1" id="btnAddIp" data-toggle="tooltip" data-placement="top" title="Thêm IP">
                                            <i class="fas fa-plus"></i>
                                          </button>
                                      </div>
                                  </div>
                                  <div class="group-invoice" id="addIp" style="display: none;">
                                    <div class="col col-md-4 text-right"></div>
                                    <div class="col col-md-8">
                                        <div class="container-ip row mt-1">
                                            
                                        </div>
                                    </div>
                                  </div>
                                  {{-- 1 form group --}}
                                  <div class="group-invoice">
                                      <div class="col col-md-4 text-right">
                                          <label for="">Loại:</label>
                                      </div>
                                      <div class="col col-md-8">
                                          <select class="form-control" name="type_colo">
                                            <option value="1 U" @if( old('type_colo') == '1 U') selected @endif>1 U</option>
                                            <option value="2 U" @if( old('type_colo') == '2 U') selected @endif>2 U</option>
                                            <option value="4 U" @if( old('type_colo') == '4 U') selected @endif>4 U</option>
                                          </select>
                                      </div>
                                  </div>
                                  {{-- 1 form group --}}
                                  <div class="group-invoice">
                                      <div class="col col-md-4 text-right">
                                          <label for="">Datacenter:</label>
                                      </div>
                                      <div class="col col-md-8">
                                        <select name="location" id="location" class="form-control"  style="width: 100%;">
                                          <option value="DNG" @if( old('location') == 'DNG') selected @endif>DNG</option>
                                          <option value="DNG-CMC" @if( old('location') == 'DNG-CMC') selected @endif>DNG-CMC</option>
                                          <option value="HCM-Viettel HHT" @if( old('location') == 'HCM-Viettel HHT') selected @endif>HCM-Viettel HHT</option>
                                          <option value="HCM-ODS" @if( old('location') == 'HCM-ODS') selected @endif>HCM-ODS</option>
                                          <option value="HN-FPT"@if( old('location') == 'HN-FPT') selected @endif>HN-FPT</option>
                                        </select>
                                      </div>
                                  </div>
                                  {{-- 1 form group --}}
                                  <div class="group-invoice">
                                    <div class="col col-md-4 text-right">
                                        <label for="">Vị trí rack:</label>
                                    </div>
                                    <div class="col col-md-8">
                                        <input type="text" name="rack" class="form-control" value="{{ old('rack') }}">
                                    </div>
                                  </div>
                                  {{-- 1 form group --}}
                                  {{-- 1 form group --}}
                                  <div class="group-invoice">
                                    <div class="col col-md-4 text-right">
                                        <label for="">Công xuất nguồn:</label>
                                    </div>
                                    <div class="col col-md-8">
                                        <input type="text" name="power" class="form-control" value="{{ old('power') }}">
                                    </div>
                                  </div>
                                  {{-- 1 form group --}}

                              </div>
                          </div>
                    </div>
                    <div class="col col-md-6">
                            <div class="order">
                                <div class="detail_order">
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                      <div class="col col-md-4 text-right">
                                          <label for="">Băng thông:</label>
                                      </div>
                                      <div class="col col-md-8">
                                          <input type="text" name="bandwidth" class="form-control" value="{{ old('bandwidth') }}">
                                      </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Trạng thái Colo:</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <select name="status_colo" id="status_colo" class="select2 form-control" data-placeholder="Chọn trạng thái colocation" style="width: 100%;" data-value="Colocation">
                                                <option value="on" @if( old('status_colo') == 'on') selected @endif>Đang bật</option>
                                                <option value="off" @if( old('status_colo') == 'off') selected @endif>Đã tắt</option>
                                                <option value="expire" @if( old('status_colo') == 'expire') selected @endif>Đã hết hạn</option>
                                                <option value="cancel" @if( old('status_colo') == 'cancel') selected @endif>Yêu cầu hủy</option>
                                                <option value="delete_colo" @if( old('status_colo') == 'delete_colo') selected @endif>Đã xóa</option>
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Ngày đặt hàng:</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <input type="text" id="datepicker" name="created_at" value="{{date('d-m-Y', strtotime( old('date_create') ))}}" class="form-control">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Ngày hết hạn :</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <input type="text" id="datepicker_next_due_date" name="next_due_date" value="{{date('d-m-Y', strtotime( old('next_due_date') ))}}" class="form-control">
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Thời gian:</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <select name="billing_cycle" id="billing_cycle" class="select2 form-control"  style="width: 100%;">
                                                <option value="" disabled>Chọn thời gian</option>
                                                @foreach ($billings as $key => $billing)
                                                    @php
                                                        $selected = '';
                                                        if ($key == old('billing_cycle')) {
                                                            $selected = 'selected';
                                                        }
                                                    @endphp
                                                    <option value="{{ $key }}" {{$selected}}>{{ $billing }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    {{-- 1 form group --}}
                                    <div class="group-invoice">
                                        <div class="col col-md-4 text-right">
                                            <label for="">Thành tiền :</label>
                                        </div>
                                        <div class="col col-md-8">
                                            <input type="text" value="{{ old('amount') }}" class="form-control" name="amount" placeholder="Giá dịch vụ">
                                        </div>
                                    </div>
                                    <div class="form-button mt-4 mr-4">
                                        @csrf
                                        <input type="submit" value="Tạo Colocation" class="btn btn-primary float-right">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('js/edit_invoices.js') }}"></script>
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script type="text/javascript">
	$(document).ready(function() {
        $('#addIp').fadeOut();
		//Date picker
		$('#datepicker').datepicker({
			autoclose: true,
            format: 'dd-mm-yyyy'
		});
		$('#datepicker_next_due_date').datepicker({
			autoclose: true,
            format: 'dd-mm-yyyy'
		});
        // select
        $('.select2').select2();

        $('#btnAddIp').on('click', function () {
            $('#addIp').fadeIn();
            var html = '';
            html += '<div class="col-12 pl-0 inpAddIp">';
            html += '<div class="form-group input-group">';
            html += '<input type="text" name="addIp[]" value="" placeholder="IP" class="form-control">';
            html += '<span class="input-group-append">';
            html += '<button type="button" class="btn btn-danger btnDeleteAddIp">';
            html += '<i class="fas fa-trash-alt"></i>';
            html += '</button>';
            html += '</span>';
            html += '</div>';
            html += '</div>';
            $('.container-ip').append(html);
        });

        $(document).on('click', '.btnDeleteAddIp', function () {
            $(this).closest('.inpAddIp').remove();
        });

        $('#user_id').on('change', function() {
            var user_id = $(this).val();
            $.ajax({
                type: "get",
                url: "/admin/colocations/list_product",
                data: {user_id: user_id},
                dataType: "json",
                beforeSend: function(){
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('#product').html(html);
                },
                success: function (data) {
                    var html = '';
                    // 1 form-group
                    html += '<select name="product_id" id="product_id" data-value="Vps" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;">';
                    html += '<option disabled selected>Chọn sản phẩm</option>';
                    if (data.length) {
                        $.each(data, function (index, product) {
                            html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                        });
                    } else {
                        html += '<option disabled class="text-danger">Không có sản phẩm Server</option>';
                    }
                    html += '</select>';
                    $('#product_id').html(html);
                    $('.select2').select2();
                },
                error: function(e) {
                    console.log(e);
                    var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                    $('#product_id').html(html);
                }
            });
        });
	});
</script>
@endsection
