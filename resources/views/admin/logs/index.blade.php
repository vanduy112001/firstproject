@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-chart-line"></i> Quản lý Activity Log
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
<li class="breadcrumb-item active">Activity Log</li>
@endsection
@section('content')
<div class="col-md-12 detail-course-finish">
    @if(session("success"))
    <div class="bg-success">
        <p class="text-light">{{session("success")}}</p>
    </div>
    @elseif(session("fails"))
    <div class="bg-danger">
        <p class="text-light">{{session("fails")}}</p>
    </div>
    @endif
</div>
<div class="container-fluid">
  <!-- Timelime example  -->
  <div class="row">
      <div class="col-md-12">
          <div class="fillter_log mt-4 mb-4">
              <!-- <button type="button" class="btn btn-primary btn_filter" name="button"><i class="fas fa-clipboard-list"></i> Lọc</button> -->
          </div>
          <div class="card">
             <div class="card-body table-responsive p-0">
               <table class="table table-hover">
                  <thead>
                    <th>Thời gian</th>
                    <th>Người dùng</th>
                    <th>Model</th>
                    <th>Hành động</th>
                    <th>Ghi chú</th>
                    <th>Dịch vụ</th>
                  </thead>
                  <tbody>
                  </tbody>
                  <tfoot></tfoot>
               </table>
             </div>
          </div>
      </div>
  </div>
</div>
    <input type="hidden" id="page" value="list_log_activity">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('/js/admin_log.js') }}"></script>
@endsection
