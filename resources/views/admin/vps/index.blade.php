@extends('layouts.app')
@section('style')
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Danh sách VPS
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">VPS</li>
@endsection
@section('content')
@php
use Carbon\Carbon;
@endphp
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
                <a href="{{ route('admin.vps.createVPS') }}" class="btn btn-primary">Thêm VPS</a>
                <button type="button" name="button" class="btn btn-info" id="vps_type_user">Cá nhân</button>
                <button type="button" name="button" class="btn btn-danger" id="vps_type_enterprise">Doanh nghiệp</button>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <select class="form-control" id="select_status_vps">
                      <option value="" disabled selected>Chọn trạng thái của VPS</option>
                      <option value="on">Trạng thái đang bật</option>
                      <option value="off">Trạng thái đã tắt</option>
                      <option value="progressing">Trạng thái đang tạo</option>
                      <option value="rebuild">Trạng thái đang cài lại</option>
                      <option value="change_ip">Trạng thái đang đổi IP</option>
                      <option value="reset_password">Trạng thái đặt lại mật khẩu</option>
                      <option value="expire">Trạng thái hết hạn</option>
                      {{-- <option value="admin_off">Trạng thái Admin tắt</option>
                      <option value="suspend">Trạng thái đã khóa</option> --}}
                      <option value="change_user">Trạng thái đã chuyển khách hàng</option>
                      <option value="delete_vps">Trạng thái đã xóa</option>
                      <option value="cancel">Trạng thái đã hủy</option>
                    </select>
                </div>
            </div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                      <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Tìm kiếm VPS hoặc khách hàng" id="search">
                        <div class="input-group-append">
                          <button type="submit" class="btn btn-danger"><i class="fas fa-search"></i></button>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 row">
              <div class="col-md-1"></div>
              <div class="col-md-4">
                <button class="btn btn-success" type="button"  id="btn_on_multi"><i class="fas fa-search"></i> Tìm kiếm nhiều VPS</button>
              </div>
              <div class="col-md-1"></div>
              <div class="col-md-6">
                <div class="mb-3" id="on_search_multi">
                </div>
              </div>
            </div>
            <div class="col-md-2 mt-4 pl-4">
                <div class="form-group">
                    <span class="mt-1">Số lượng</span> 
                    <select id="qtt">
                        <option value="10">10</option>
                        <option value="30" selected>30</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                        <option value="200">200</option>
                        <option value="500">500</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6 mt-4 text-primary pl-4">
                <b>VPS <span class="first-item">{{ $list_vps->firstItem() }}</span> - <span class="last-item">{{ $list_vps->lastItem() }}</span> / <span class="total-item">{{ $list_vps->total() }}</span></b>
            </div>
            <div class="col-md-4 mt-4 link-right paginate_top">
                {{ $list_vps->links()  }}
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="col-md-12 " id="qttVpsSearch">
  
</div>
<div class="button-header">
  <span class="btn btn-sm btn-default qtt_vps"></span>
  <button class="btn btn-sm btn-warning btn-multi-action-vps text-default" data-type="off">Tắt</button>
  <button class="btn btn-sm btn-info btn-multi-action-vps text-default" data-type="on">Bật</button>
  <button class="btn btn-sm btn-success btn-multi-action-vps text-default" data-type="restart">Khởi động lại</button>
  <button class="btn btn-sm btn-secondary btn-multi-action-vps text-default" data-type="change_ip">Đổi IP</button>
  <button class="btn btn-sm btn-secondary btn-multi-action-vps text-default" data-type="change_user">Đổi khách hàng</button>
  <button class="btn btn-sm btn-primary text-default btn-add-contract" product-type="vps">Tạo hợp đồng</button>
  <button class="btn btn-sm btn-danger btn-action-vps" data-type="delete">Xóa</button>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-bordered" id="table_vps">
                <thead class="primary">
                    <th>
                      <input type="checkbox"  class="form-control vps_checkbox_all">
                    </th>
                    <th>Tên khách hàng</th>
                    <th>IP</th>
                    <th>Cấu hình</th>
                    {{-- <th>Loại</th> --}}
                    <th>Ngày tạo</th>
                    <th class="sort_next_due_date" data-sort="ASC" data-toggle="tooltip" data-placement="top" title="Sắp xếp ngày kết thúc">
                      Ngày kết thúc <i class="fas fa-sort"></i>
                      <input type="hidden" class="sort_type" value="">
                    </th>
                    <th>Tổng thời gian thuê</th>
                    <th>Chu kỳ thanh toán</th>
                    <th>Chi phí</th>
                    <th>Trạng thái</th>
                    <th>Chức năng</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                    @foreach ($list_vps as $vps)
                      @php
                        $total_time = '';
                        $create_date = new Carbon($vps->date_create);
                        $next_due_date = new Carbon($vps->next_due_date);
                        if ( $next_due_date->diffInYears($create_date) ) {
                          $year = $next_due_date->diffInYears($create_date);
                          $total_time = $year . ' Năm ';
                          $create_date = $create_date->addYears($year);
                          $month = $next_due_date->diffInMonths($create_date);
                          //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                          if ( $month ) {
                            $total_time .= $month . ' Tháng';
                          }
                        } else {
                          $diff_month = $next_due_date->diffInMonths($create_date);
                          $total_time = $diff_month . ' Tháng';
                        }
                      @endphp
                        <tr>
                            <td>
                              <input type="checkbox"  value="{{ $vps->id }}" data-ip="{{ $vps->ip }}" class="form-control vps_checkbox">
                            </td>
                            <td>
                              @if (!empty($vps->user_vps->id))
                                <a href="{{ route('admin.user.detail' , !empty($vps->user_vps->id) ? $vps->user_vps->id : 0 ) }}">{{ !empty($vps->user_vps->name) ? $vps->user_vps->name : 'Đã xóa' }}</a>
                              @else
                                <span class="text-danger">Đã xóa</span>
                              @endif
                            </td>
                            <td>
                                @if (!empty($vps->ip))
                                    <a href="{{ route('admin.vps.detail', $vps->id) }}">{{$vps->ip}}</a>
                                @else
                                    <span class="text-danger">Chưa tạo</span>
                                @endif
                            </td>
                            <td>
                              @php
                                $product = $vps->product;
                                if(!empty($product->meta_product)) {
                                  $cpu = $product->meta_product->cpu;
                                  $ram = $product->meta_product->memory;
                                  $disk = $product->meta_product->disk;
                                } else {
                                  $cpu = 1;
                                  $ram = 1;
                                  $disk = 20;
                                }
                                // Addon
                                if (!empty($vps->vps_config)) {
                                    $vps_config = $vps->vps_config;
                                    if (!empty($vps_config)) {

                                        $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                                        $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                                        $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                                    }
                                }
                              @endphp
                              {{ $cpu }} - {{ $ram }} - {{ $disk }}
                            </td>
                            {{-- <td>
                                @if($vps->type_vps == "vps")
                                  VPS
                                @else
                                  NAT VPS
                                @endif
                            </td> --}}
                            <td>@if (!empty($vps->date_create))
                                    <span>{{ date('H:i:m d-m-Y', strtotime($vps->created_at)) }}</span>
                                @else
                                    <span class="text-danger">Chưa tạo</span>
                                @endif
                            </td>
                            <td>@if (!empty($vps->next_due_date))
                                    <span>{{ date('d-m-Y', strtotime($vps->next_due_date)) }}</span>
                                @else
                                    <span class="text-danger">Chưa tạo</span>
                                @endif
                            </td>
                            <td>
                              {{ $total_time }}
                            </td>
                            <td>
                              {{ $billings[$vps->billing_cycle] }}
                            </td>
                            <td>
                              @if(!empty($vps->price_override))
                                <?php
                                  $sub_total = $vps->price_override;
                                  $order_addon_vps = $vps->order_addon_vps;
                                ?>
                                <b class="text-danger">{!!number_format( $sub_total ,0,",",".")!!} VNĐ</b>
                              @else
                                <?php
                                  $product = $vps->product;
                                  $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                                  if (!empty($vps->vps_config)) {
                                    $addon_vps = $vps->vps_config;
                                    $add_on_products = UserHelper::get_addon_product_private($vps->user_id);
                                    $pricing_addon = 0;
                                    foreach ($add_on_products as $key => $add_on_product) {
                                      if (!empty($add_on_product->meta_product->type_addon)) {
                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                          $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                          $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                                        }
                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                          $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                                        }
                                      }
                                    }
                                    $sub_total += $pricing_addon;
                                  }
                                ?>
                                <b>{!!number_format( $sub_total ,0,",",".")!!} VNĐ</b>
                              @endif
                            </td>
                            <td class="vps-status">
                                @if ($vps->status == 'Pending')
                                    <span class="text-danger">Chưa tạo</span>
                                @elseif ($vps->status_vps == 'progressing')
                                    <span class="text-info">Đang tạo</span>
                                @elseif ($vps->status_vps == 'rebuild')
                                    <span class="text-info">Đang cài lại</span>
                                @elseif ($vps->status_vps == 'change_ip')
                                    <span class="text-info">Đang đổi IP</span>
                                @elseif ($vps->status_vps == 'reset_password')
                                    <span class="text-info">Đang đặt lại mật khẩu</span>
                                @elseif ($vps->status_vps == 'expire')
                                    <span class="text-danger">Hết hạn</span>
                                @elseif ($vps->status_vps == 'on')
                                    <span class="text-success">Đang bật</span>
                                @elseif ($vps->status_vps == 'off')
                                    <span class="text-danger">Đã tắt</span>
                                @elseif ($vps->status_vps == 'admin_off')
                                    <span class="text-danger">Admin tắt</span>
                                @elseif ($vps->status_vps == 'suspend')
                                    <span class="text-danger">Đã khóa</span>
                                @elseif ($vps->status_vps == 'delete_vps')
                                    <span class="text-danger">Đã xóa</span>
                                @elseif ($vps->status_vps == 'change_user')
                                    <span class="text-danger">Đã chuyển</span>
                                @else
                                    <span class="text-secondary">Đã hủy</span>
                                @endif
                            </td>
                            <td>
                                @if ($vps->status_vps == 'on')
                                    <a href="{{ route('admin.vps.console', $vps->id) }}" target="_blank" class="btn  btn-sm btn-success"><i class="fas fa-desktop"></i></a>
                                @endif
                                @if ($vps->status_vps != 'delete_vps' && $vps->status_vps != 'cancel' && $vps->status_vps != 'change_user')
                                  <button class="btn btn-sm btn-outline-warning button-action-vps on" data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" @if($vps->status_vps == 'off' || $vps->status_vps == 'cancel') disabled @endif data-id="{{ $vps->id }}" data-ip="{{ $vps->ip }}"><i class="fas fa-power-off"></i></button>
                                  <button class="btn btn-sm btn-outline-info button-action-vps off" data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" @if($vps->status_vps == 'on' || $vps->status_vps == 'cancel') disabled @endif data-id="{{ $vps->id }}" data-ip="{{ $vps->ip }}"><i class="far fa-stop-circle"></i></button>
                                  <button class="btn btn-sm btn-outline-success button-action-vps restart" data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" @if($vps->status_vps == 'off' || $vps->status_vps == 'cancel') disabled @endif data-id="{{ $vps->id }}" data-ip="{{ $vps->ip }}"><i class="fas fa-sync-alt"></i></button>
                                @endif
                            </td>
                            <td class="button-action">
                                @if ( $vps->status_vps != 'delete_vps' && $vps->status_vps != 'cancel' && $vps->status_vps != 'change_user' &&  $vps->status == 'Active' )
                                  <button class="btn btn-info button-action-vps" data-toggle="tooltip" data-placement="top" data-action="change_user" title="Chuyển khách hàng" data-id="{{$vps->id}}" data-ip="{{$vps->ip}}" ><i class="fas fa-exchange-alt"></i></button>
                                @endif
                                <a href="{{ route('admin.vps.detail', $vps->id) }}" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>
                                @if ( $vps->status_vps != 'delete_vps' )
                                  <button class="btn btn-danger btn-delete-vps" data-action="delete" data-id="{{$vps->id}}" data-ip="{{$vps->ip}}" ><i class="fas fa-trash-alt"></i></button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot class="card-footer clearfix">
                    <td colspan="14" class="link-right">
                        {{ $list_vps->links()  }}
                    </td>
                </tfoot>
            </table>
        </div>
    </div>
    <input type="hidden" id="type_user_personal" value="">
    <div class="container" id="group_button">
        <div class="table-responsive" style="display: block;">
            <button class="ml-2 btn btn-warning btn-multi-action-vps" data-type="off">Tắt</button>
            <button class="ml-2 btn btn-info btn-multi-action-vps" data-type="on">Bật</button>
            <button class="ml-2 btn btn-success btn-multi-action-vps" data-type="restart">Khởi động lại</button>
            <button class="ml-2 btn btn-secondary btn-multi-action-vps" data-type="change_ip">Đổi IP</button>
            <button class="ml-2 btn btn-default btn-multi-action-vps" data-type="change_user">Đổi khách hàng</button>
            <button class="ml-2 btn btn-danger btn-action-vps" data-type="delete">Xóa</button>
        </div>
    </div>
</div>
{{-- Modal xoa order --}}
<div class="modal fade" id="delete-order">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-order" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-order" value="Xóa đơn hàng">
          <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
{{-- Modal xoa vps --}}
<div class="modal fade" id="delete-vps">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button-vps">Xóa VPS</button>
          <button type="button" class="btn btn-danger" id="button-vps-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
{{-- Modal xoa vps --}}
<div class="modal fade" id="delete-multi-vps">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-multi" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button-multi-vps" disabled="disabled">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-multi-vps-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="page" value="list_vps">
@endsection
@section('scripts')
<script src="{{ asset('js/vps.js') }}"></script>
<script src="{{ asset('js/admin_contract.js') }}"></script>
@endsection
