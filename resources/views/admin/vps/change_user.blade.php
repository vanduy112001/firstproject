@extends('layouts.app')
@section('style')
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('content')
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item"><a href="{{ route('admin.vps.index') }}"><i class="fa fa-users" aria-hidden="true"></i> Danh sách VPS</a></li>
<li class="breadcrumb-item active"><i class="fa fa-user" aria-hidden="true"></i> Chuyển VPS</li>
@endsection
<div class="col-md-6" style="margin:auto">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title text-center">Chuyển VPS cho khách hàng</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        @if ($errors->any())
        <div class="alert alert-danger" style="margin-top:20px">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        @if(session("success"))
            <div class="bg-success p-4">
                <p class="text-light">{{session("success")}}</p>
            </div>
        @elseif(session("fails"))
            <div class="bg-danger p-4">
                <p class="text-light">{{session("fails")}}</p>
            </div>
        @endif
        @if(session("error"))
        <div class="bg-success p-4">
            <p class="text-light">{{session("error")}}</p>
        </div>
        @endif
        <form role="form" method="post" action="{{ route('admin.vps.change_user') }}" id="form_select">
            {{ csrf_field() }}
            <div class="card-body">
                <div class="form-group">
                    <label for="">Danh sách VPS</label>
                    <div class="p-3">
                        @foreach ($list_vps as $vps)
                            <div class="detail-vps">
                                {{ $vps->ip }} - 
                                <a href="{{ route('admin.user.detail', $vps->user_id) }}">{{ !empty($vps->user_vps->name) ? $vps->user_vps->name : 'Đã xóa' }}</a>
                                <input type="hidden" name="id[]" class="id_vps" value="{{ $vps->id }}">
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="form-group">
                    <label for="user_id">Chọn khách hàng</label>
                    <select name="user_id" class="select2" style="width:100%">
                        <option value="" selected disabled>Chọn khách hàng</option>
                        @foreach ($list_user as $user)
                            @php
                                $selected = '';
                                if ( old('user_id') == $user->id ) {
                                    $selected = 'selected';
                                }
                            @endphp
                            <option value="{{ $user->id }}" {{ $selected }}>{{ $user->name }} - {{ $user->email }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <a class="btn btn-default" href="{{ route('admin.vps.index') }} ">Hủy</a>
                <button type="submit" class="btn btn-info float-right">Xác nhận</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('scripts')
    <script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
    <script type="text/javascript">
      $(document).ready(function() {
        //Date picker
        $('#datepicker').datepicker({
          autoclose: true
        });
        // select
        $('.select2').select2();
      });
    </script>
@endsection
