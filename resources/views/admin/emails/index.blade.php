@extends('layouts.app')
@section('title')
    <i class="fas fa-envelope-square"></i> Quản lý thông tin email template
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
<li class="breadcrumb-item active">Email template</li>
@endsection
@section('content')
<div class="col-md-12 detail-course-finish">
    @if(session("success"))
    <div class="bg-success">
        <p class="text-light">{{session("success")}}</p>
    </div>
    @elseif(session("fails"))
    <div class="bg-danger">
        <p class="text-light">{{session("fails")}}</p>
    </div>
    @endif
</div>
<email-dashboard></email-dashboard>
<input type="hidden" id="page" value="list_email">
@endsection
@section('scripts')
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>
<!-- script vue -->
<script src="{{ asset('libraries/vue/vue.min.js') }}"></script>
@endsection
