@extends('layouts.app')
@section('title')
    <i class="fas fa-envelope-square"></i> Quản lý thông tin email template
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
<li class="breadcrumb-item active">Email template</li>
@endsection
@section('content')
<div class="col col-md-12">
    <h4 class="mb-2">Sửa email template</h4>
    <hr class="text-secondary">
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <form action="{{route('email.updateEmail')}}" method="post">
            @csrf
            <div class="form-group">
                <label for="nameEmail">Tiêu đề</label>
                <input type="text" id="nameEmail" name="name" value="{{ $email->name }}" class="form-control">
                <input type="hidden" name="id" value="{{ $email->id }}">
            </div>
            <div class="form-group">
                <label for="subject-email">Subject</label>
                <input type="text" id="subject-email" name="subject" class="form-control" @if(!@empty($email->meta_email)) value="{{ $email->meta_email->subject }}" @endif>
            </div>
            <div class="form-group">
                <label for="content-email">Nội dung</label>
                <textarea name="content" id="content-email" cols="30" rows="10" class="form-control">@if(!@empty($email->meta_email)) {{ $email->meta_email->content }} @endif</textarea>
            </div>
            <div class="card-footer text-center">
                <input type="submit" class="btn btn-primary" value="Lưu thay đổi">
                <input type="submit" class="btn btn-default" value="Hủy thay đổi">
            </div>
        </form>
    </div>
    <div class="tips-email">
        <div class="row">
            <div class="col col-md-6">
                <b>Client</b>
                <br>
                <div class="row">
                    <div class="col col-md-6">
                        User ID
                    </div>
                    <div class="col col-md-6">
                        {$user_id}
                    </div>
                    <div class="col col-md-6">
                        User Name
                    </div>
                    <div class="col col-md-6">
                        {$user_name}
                    </div>
                    <div class="col col-md-6">
                        Email Address
                    </div>
                    <div class="col col-md-6">
                        {$user_email}
                    </div>
                    <div class="col col-md-6">
                        Địa chỉ
                    </div>
                    <div class="col col-md-6">
                        {$user_addpress}
                    </div>
                    <div class="col col-md-6">
                        Số điện thoại
                    </div>
                    <div class="col col-md-6">
                        {$user_phone}
                    </div>
                    <div class="col col-md-6">
                        Url website
                    </div>
                    <div class="col col-md-6">
                        {$url}
                    </div>
                    <div class="col col-md-6">
                        Url control panel
                    </div>
                    <div class="col col-md-6">
                        {$url_control_panel}
                    </div>
                    <div class="col col-md-6">
                        Url kích hoạt order
                    </div>
                    <div class="col col-md-6">
                        portal.cloudzone.vn/order/verify/{$token}
                    </div>
                </div>
                <br>
                <b>Server</b>
                <br>
                <div class="row">
                  <div class="col col-md-6">
                      Cấu hình CPU
                  </div>
                  <div class="col col-md-6">
                      {$server_cpu}
                  </div>
                  <div class="col col-md-6">
                      Cấu hình RAM
                  </div>
                  <div class="col col-md-6">
                      {$server_ram}
                  </div>
                  <div class="col col-md-6">
                      Cấu hình DISK (có Addon)
                  </div>
                  <div class="col col-md-6">
                      {$server_disk}
                  </div>
                  <div class="col col-md-6">
                      Số lượng IP
                  </div>
                  <div class="col col-md-6">
                      {$server_qtt_ip}
                  </div>
                  <div class="col col-md-6">
                      RAID
                  </div>
                  <div class="col col-md-6">
                      {$server_raid}
                  </div>
                  <div class="col col-md-6">
                      Datacenter
                  </div>
                  <div class="col col-md-6">
                      {$server_datacenter}
                  </div>
                  <div class="col col-md-6">
                      Hệ điều hành của Server
                  </div>
                  <div class="col col-md-6">
                      {$server_os}
                  </div>
                  <div class="col col-md-6">
                      Server Management
                  </div>
                  <div class="col col-md-6">
                      {$server_management}
                  </div>
                  <div class="col col-md-6">
                      IP 1 Server
                  </div>
                  <div class="col col-md-6">
                      {$server_ip}
                  </div>
                  <div class="col col-md-6">
                      IP 2 Server
                  </div>
                  <div class="col col-md-6">
                      {$server_ip2}
                  </div>
                  <div class="col col-md-6">
                      Username
                  </div>
                  <div class="col col-md-6">
                      {$server_user_name}
                  </div>
                  <div class="col col-md-6">
                      Password
                  </div>
                  <div class="col col-md-6">
                      {$server_password}
                  </div>
                  <div class="col col-md-6">
                      Trạng thái
                  </div>
                  <div class="col col-md-6">
                      {$server_status}
                  </div>
                </div>
                <br>
                <b>Colocation</b>
                <br>
                <div class="row">
                    <div class="col col-md-6">
                        IP Colo
                    </div>
                    <div class="col col-md-6">
                        {$colocation_ip}
                    </div>
                    <div class="col col-md-6">
                        Loại Colo
                    </div>
                    <div class="col col-md-6">
                        {$type_colocation}
                    </div>
                    <div class="col col-md-6">
                        Datacenter
                    </div>
                    <div class="col col-md-6">
                        {$colocation_datacenter}
                    </div>
                </div>
            </div>
            <div class="col col-md-6">
                <b>Product / Services</b>
                <br>
                <div class="row">
                    <div class="col col-md-6">
                        Order ID
                    </div>
                    <div class="col col-md-6">
                        {$order_id}
                    </div>
                    <div class="col col-md-6">
                        Tên sản phẩm
                    </div>
                    <div class="col col-md-6">
                        {$product_name}
                    </div>
                    <div class="col col-md-6">
                        Domain
                    </div>
                    <div class="col col-md-6">
                        {$domain}
                    </div>
                    <div class="col col-md-6">
                        Giá
                    </div>
                    <div class="col col-md-6">
                        {$sub_total}
                    </div>
                    <div class="col col-md-6">
                        Thành tiền
                    </div>
                    <div class="col col-md-6">
                        {$total}
                    </div>
                    <div class="col col-md-6">
                        Số lượng
                    </div>
                    <div class="col col-md-6">
                        {$qtt}
                    </div>
                    <div class="col col-md-6">
                        Token
                    </div>
                    <div class="col col-md-6">
                        {$token}
                    </div>
                </div>
                <br>
                <b>VPS</b>
                <br>
                <div class="row">
                    <div class="col col-md-6">
                        IP
                    </div>
                    <div class="col col-md-6">
                        {$ip}
                    </div>
                    <div class="col col-md-6">
                        Port
                    </div>
                    <div class="col col-md-6">
                        {$port}
                    </div>
                    <div class="col col-md-6">
                        Thời gian thuê
                    </div>
                    <div class="col col-md-6">
                        {$billing_cycle}
                    </div>
                    <div class="col col-md-6">
                        Ngày kết thúc
                    </div>
                    <div class="col col-md-6">
                        {$next_due_date}
                    </div>
                    <div class="col col-md-6">
                        Ngày đặt hàng
                    </div>
                    <div class="col col-md-6">
                        {$order_created_at}
                    </div>
                    <div class="col col-md-6">
                        Username (Services)
                    </div>
                    <div class="col col-md-6">
                        {$services_username}
                    </div>
                    <div class="col col-md-6">
                        Password (Services)
                    </div>
                    <div class="col col-md-6">
                        {$services_password}
                    </div>
                    <div class="col col-md-6">
                        Hệ điều hành
                    </div>
                    <div class="col col-md-6">
                        {$os}
                    </div>
                    <div class="col col-md-6">
                        List IP VPS yêu cầu thay đổi
                    </div>
                    <div class="col col-md-6">
                        {$list_ip_vps}
                    </div>
                    <div class="col col-md-6">
                        List IP VPS đã thay đổi
                    </div>
                    <div class="col col-md-6">
                        {$list_ip_changed_vps}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="page" value="edit_email">
@endsection
@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        CKEDITOR.replace( 'content');
    })
</script>
@endsection
