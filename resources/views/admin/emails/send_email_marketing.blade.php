@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('/libraries/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
<link rel="stylesheet" href="{{ asset('/libraries/toastr/toastr.min.css')}}">
@endsection
@section('title')
    <i class="fas fa-envelope-square"></i> Gửi Email Marketing
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
<li class="breadcrumb-item active">Email Marketing</li>
@endsection
@section('content')
<div class="col-md-12 detail-course-finish">
    @if(session("success"))
        <div class="bg-success">
            <p class="text-light">{{session("success")}}</p>
        </div>
    @elseif(session("fails"))
        <div class="bg-danger">
            <p class="text-light">{{session("fails")}}</p>
        </div>
    @endif
</div>
<div class="col-md-12" style="margin:auto">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title text-center">Gửi Email Marketing</h3>
        </div>
        <div class="card-body">
            <form action="{{ route('email.email_marketing.form_send_mail') }}" method="post">
                @csrf
                <div class="form-group">
                    <select id="user_id" class="select2" name="user_id" style="width: 100%;">
                        @foreach( $list_user as $user )
                        <option value="{{ $user->id }}">{{ $user->name }} - {{ $user->email }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <input class="form-control" name="subject" value="{{ $email->meta_email->subject }}" required placeholder="Subject:">
                </div>
                <div class="form-group">
                    <textarea name="content" id="content" cols="30" rows="10" class="form-control">{{ $email->meta_email->content }}</textarea>
                </div>
                <div class="card-footer">
                <div class="float-right">
                    <button type="submit" class="btn btn-primary"><i class="far fa-envelope"></i> Send</button>
                </div>
                <a href="{{ route('email.email_marketing.list') }}" class="btn btn-default"><i class="fas fa-times"></i> Hủy</a>
              </div>
            </form>
        </div>
    </div>
</div>
<input type="hidden" id="page" value="send_mail_with_user">
@endsection

@section('scripts')
<script src="{{ asset('/libraries/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('/libraries/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script type="text/javascript">
    $(document).ready(function() {
        CKEDITOR.replace( 'content');
        $('.select2').select2();
    })
</script>
@endSection
