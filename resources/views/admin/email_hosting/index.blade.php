@extends('layouts.app')
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Danh sách Email Hosting
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Email Hosting</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-bordered">
                <thead class="primary">
                    <th>ID</th>
                    <th>Tên khách hàng</th>
                    <th>Tên sản phẩm</th>
                    <th>Domain</th>
                    <th>Ngày tạo</th>
                    <th>Ngày kết thúc</th>
                    <th>Chi phí</th>
                    <th>Trạng thái</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                    @if ($email_hostings->count() > 0)
                      @foreach( $email_hostings as $email_hosting )
                        <tr>
                          <td><a href="{{ route('admin.email_hosting.detail' ,$email_hosting->id ) }}">{{ $email_hosting->id }}</a></td>
                          <td><a href="{{ route('admin.user.detail', $email_hosting->user_id) }}">{{ !empty($email_hosting->user_email_hosting->name) ? $email_hosting->user_email_hosting->name : 'Đã xóa' }}</a></td>
                          <td>
                              @if(!empty($email_hosting->product))
                                  @if(!empty($email_hosting->product->group_product->private))
                                      <a href="{{ route('admin.product_private.editPrivate' , $email_hosting->product->id) }}">{{ $email_hosting->product->name }}</a>
                                  @else
                                    <a href="{{ route('admin.product.edit' , $email_hosting->product->id) }}">{{ $email_hosting->product->name }}</a>
                                  @endif
                              @endif
                          </td>
                          <td><a href="{{ route('admin.email_hosting.detail' ,$email_hosting->id ) }}">{{ $email_hosting->domain }}</a></td>
                          <td>{{ date( 'd-m-Y' , strtotime($email_hosting->created_at) ) }}</td>
                          @if(!empty($email_hosting->next_due_date))
                            <td>{{  date( 'd-m-Y' , strtotime($email_hosting->next_due_date) ) }}</td>
                          @else
                            <td class="text-danger">Chưa thanh toán</td>
                          @endif
                          <td>
                            @if ( !empty($email_hosting->price_override) )
                                {!!number_format( $email_hosting->price_override ,0,",",".")!!} VNĐ
                            @else
                                {!!number_format( !empty($email_hosting->detail_order->order->total) ? $email_hosting->detail_order->order->total : 0 ,0,",",".")!!} VNĐ
                            @endif
                          </td>
                          @if ($email_hosting->status == 'Pending')
                              <td class="text-danger">Chưa thanh toán</td>
                          @elseif ($email_hosting->status_hosting == 'on')
                              <td class="text-success">Đang sử dụng</td>
                          @elseif ($email_hosting->status_hosting == 'off')
                              <td class="text-danger">Đã tắt</td>
                          @elseif ($email_hosting->status_hosting == 'cancel')
                              <td class="text-danger">Yêu cầu hủy</td>
                          @elseif ($email_hosting->status_hosting == 'delete_colo')
                              <td class="text-danger">Đã xóa</td>
                          @endif
                          <td class="button-action">
                              <a href="{{ route('admin.email_hosting.detail' ,$email_hosting->id ) }}" class="btn btn-warning text-white"><i class="fas fa-edit"></i></a>
                              <button class="btn btn-danger btn-delete-colo" data-id="{{$email_hosting->id}}" data-ip="{{$email_hosting->domain}}" ><i class="fas fa-trash-alt"></i></button>
                          </td>
                        </tr>
                      @endforeach
                    @else
                      <td colspan="9" class="text-center text-danger">
                          Không có dịch vụ Email Hosting trong dữ liệu!
                      </td>
                    @endif
                </tbody>
                <tfoot class="card-footer clearfix">
                    <td colspan="9" class="text-center">
                    </td>
                </tfoot>
            </table>
        </div>
    </div>
    <!-- <div class="container" id="group_button">
        <div class="table-responsive" style="display: block;">
            <button class="btn btn-success btn-action-vps" data-type="mark_paid">Tạo VPS</button>
            <button class="btn btn-outline-secondary btn-action-vps" data-type="cancel">Hủy</button>
            <button class="btn btn-danger btn-action-vps" data-type="delete">Xóa</button>
        </div>
    </div> -->
</div>
  <!-- /.modal -->
{{-- Modal xoa vps --}}
<div class="modal fade" id="delete_vps">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-vps" value="Xóa đơn hàng">
          <button type="button" class="btn btn-danger" id="button-vps-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="page" value="list_email_hosting">
@endsection
@section('scripts')
<script src="{{ asset('js/admin_email_hosting.js') }}"></script>
@endsection
