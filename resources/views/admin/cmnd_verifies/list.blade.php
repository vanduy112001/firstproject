@extends('layouts.app')
@section('title')
Danh sách chứng minh nhân dân
@endsection
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('admin.home') }}"><i class="icon-dashboard"></i> Home</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> List CMND</li>
@endsection
@section('content')
{{-- <div class="row">
    <div data-v-a16ce4fa="" class="col-md-4">
        <a class="btn btn-primary" href="{{ route('admin.user.create') }}">Tạo user</a>
    </div>
</div> --}}

@if(session("success"))
<div class="alert alert-success alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("success")}}</span>
</div>
@elseif(session("fail"))
<div class="alert alert-danger alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("fail")}}</span>
</div>
@elseif(session("error"))
<div class="alert alert-danger alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("error")}}</span>
</div>
@endif

<div class="card" style="margin-top: 20px">
    <div class="card-header">
        <div class="card-tools float-left">
            <div class="input-group input-group-sm" style="width: 180px;">
                <button type="submit" id="check_delete" class="btn btn-sm btn-default check_delete"><i
                        class="fa fa-trash" aria-hidden="true"></i> Xóa
                    mục đã chọn</button>
            </div>
        </div>
    </div>
    <!-- /.card-header -->
    <div class="card-body table-responsive">
        <table id="data-user-table" class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th>
                        <div>
                            <input class="check_all" id="check_all" type="checkbox" value="1">
                        </div>
                    </th>
                    <th>ID</th>
                    <th>Tên</th>
                    {{-- <th>Email</th> --}}
                    <th>Số CMND</th>
                    <th>CMND mặt trước</th>
                    <th>CMND mặt sau</th>
                    <th>Trạng thái</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if ($cmnds->count() > 0)
                    @foreach($cmnds as $cmnd)
                    <tr data-row-id="{{ $cmnd->id }}">
                        <td>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="check_id" value="{{ $cmnd->id }}">
                            </div>
                        </td>
                        <td>{{$cmnd->id}} </td>
                        <td>
                            @if (!empty($cmnd->user->name))
                                <a href="{{ route('admin.user.detail',['id' => $cmnd->user_id]) }}" target="_blank">{{$cmnd->user->name}}</a>
                             @else
                                Đã xóa
                            @endif
                        </td>
                        {{-- <td>{{ !empty($cmnd->user->email) ? $cmnd->user->email : 'Đã xóa' }}</td> --}}
                        <td>{{ !empty($cmnd->user->user_meta->cmnd) ? $cmnd->user->user_meta->cmnd : 'không có' }}</td>
                        <td>
                            @if (!empty($cmnd->cmnd_before))
                                <a data-fancybox="gallery-<?php echo $cmnd->id ?>" href="{{ asset($cmnd->cmnd_before) }}" data-caption="User: {{$cmnd->user->name}} <br> Số CMND: {{ !empty($cmnd->user->user_meta->cmnd) ? $cmnd->user->user_meta->cmnd : 'không có' }}">
                                    <img src="{{ asset($cmnd->cmnd_before) }}" alt="Scan CMND mặt trước" width="50">
                                </a>
                            @endif
                        </td>
                        <td>
                            @if (!empty($cmnd->cmnd_after))
                                <a data-fancybox="gallery-<?php echo $cmnd->id ?>" href="{{ url($cmnd->cmnd_after) }}" data-caption="User: {{$cmnd->user->name}} <br> Số CMND: {{ !empty($cmnd->user->user_meta->cmnd) ? $cmnd->user->user_meta->cmnd : 'không có' }}">
                                    <img src="{{ asset($cmnd->cmnd_after) }}" alt="Scan CMND mặt sau" width="50">
                                </a> 
                            @endif
                        </td>
                        <td>
                            @if ($cmnd->active === 0) <span class="btn btn-danger btn-sm mb-1"><i class="fa fa-times-circle"></i> Pending</span> 
                            @else <span class="btn btn-success btn-sm mb-1"><i class="fa fa-check-circle"></i> Active</span>  
                            @endif
                        </td>
                        <td>
                            @if ($cmnd->active === 0)
                                <a class="btn btn-success mb-1 btn-sm active-cmnd" id="{{ $cmnd->id }}" href="javascript:void(0)" data-toggle="modal" data-target="#active-cmnd" title="Kích hoạt">
                                    <i class="fa fa-check-circle"></i>
                                </a>    
                            @elseif ($cmnd->active === 1)
                                <a class="btn btn-warning mb-1 btn-sm text-white deactive-cmnd" id="{{ $cmnd->id }}" href="javascript:void(0)" data-toggle="modal" data-target="#active-cmnd" title="Bỏ kích hoạt">
                                    <i class="fa fa-times-circle"></i>
                                </a>
                            @endif
                            
                            <a class="btn btn-danger btn-sm mb-1 delete-cmnd" href="javascript:void(0)" data-id="{{ $cmnd->id }}" title="Delete">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <td colspan="9" class="text-danger text-center">Không có dữ liệu</td>
                @endif
            </tbody>
        </table>
    </div>
</div>
{{-- Modal kích hoạt chứng minh nhân dân --}}
<div class="modal fade" id="active-cmnd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title show-domain loading" id="exampleModalLongTitle"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        {{-- <div class="modal-body loading" style="margin:auto">

        </div> --}}
        <div class="modal-footer">
          {{-- <button type="button" class="btn btn-secondary active-cmnd-cancel" data-dismiss="modal">Đóng</button>
          <a class="btn btn-primary comfirm-cmnd" name="comfirm-cmnd" href="">Xác nhận</a> --}}
        </div>
      </div>
    </div>
  </div>
{{-- Modal bỏ kích hoạt chứng minh nhân dân --}}
<div class="modal fade" id="active-cmnd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true" data-backdrop="false">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title show-domain loading" id="exampleModalLongTitle"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
<input type="hidden" id="page" value="list_cmnd">


@endsection

@section('scripts')
<script>
    $(function(){
         $('.delete-cmnd').click(function (e) {
            var r = confirm("Bạn muốn xóa thông tin chứng minh thư này?");
            if (r == true) {
                var obj = $(this);
                var id = obj.attr('data-id');
                $.ajax({
                    type: "post",
                    url: "{{ route('admin.users.cmnd_verifies.delete') }}",
                    dataType: 'json',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": id
                    },
                    success: function (data) {
                        console.log(data);
                        alert(data.message);
                        if (data.status) {
                            obj.closest('tr').remove();
                        }
                    }, error: function () {
                        alert(data.message);
                    }
                });
            }
        });

        //Click để ẩn và hiện các row
        $("#check_all").click(function(){
            $('input:checkbox').prop('checked', this.checked);
        });

        // Hiện button danger
        $('input:checkbox').change(function () {
            var check = $('.form-check-input:checked');
            if (check.length > 0) {
                $("#check_delete").addClass('btn-danger').removeClass('btn-default');
            } else if(check.length == 0){
                $("#check_delete").addClass('btn-default').removeClass('btn-danger');
            }
        });

       //Gửi ajax lên serverver để xóa các row được chọn
       $('.check_delete').on('click', function () {
            var r = confirm("Bạn muốn xóa các mục đã chọn?");
            if (r == true) {
                var array = [];
                var value = $('.form-check-input:checked');
                if (value.length > 0) {
                    $(value).each(function () {
                        array.push($(this).val());
                    })
                } else {
                    alert('Chưa chọn mục cần xóa');
                }
                $.ajax({
                    type: "post",
                    url: "{{ route('admin.users.cmnd_verifies.multidelete') }}",
                    dataType: 'json',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": array
                    },
                    success: function (data) {
                        alert(data.message);
                        $.each(array, function( index, value ) {
                            $('table tr').filter("[data-row-id='" + value + "']").remove();
                        });
                    },
                    error: function () {
                        alert(data.message);
                    }
                });

                //Hiển thị lại bảng sau khi xóa row
                
            }
        });
        $('.active-cmnd').on('click', function(){
            var id = $(this).attr('id');
            var show = '';
            var title = '';
            $.ajax({
                url: "{{ route('admin.users.cmnd_verifies.detail') }}",
                data: {'id': id},
                dataType: 'json' ,
                type: 'get',
                beforeSend: function() {
                    $('.loading').html('<div class="spinner-border spinner-border-sm text-info"></div>');
                },
                success: function(data){
                    title = 'Xác thực thông tin chứng minh nhân dân ID: <span class="text-danger">'+ data.id +'</span> cho người dùng <span class="text-primary">'+ data.user.name +'</span><br>';
                    show += '<button type="button" class="btn btn-secondary active-cmnd-cancel" data-dismiss="modal">Đóng</button>';
                    show += '<a class="btn btn-primary comfirm-cmnd" name="comfirm-cmnd" href="{{ route('admin.users.cmnd_verifies.confirm') }}?id='+data.id+'">Xác nhận</a>';
                    $('.show-domain').html(title);
                    $('.modal-footer').html(show);
                },
                error: function(e) {
                    $('.modal-body').html('Truy vấn lỗi');
                },
            });
            
        });
        $('.deactive-cmnd').on('click', function(){
            var id = $(this).attr('id');
            var show = '';
            var title = '';
            $.ajax({
                url: "{{ route('admin.users.cmnd_verifies.detail') }}",
                data: {'id': id},
                dataType: 'json' ,
                type: 'get',
                beforeSend: function() {
                    $('.loading').html('<div class="spinner-border spinner-border-sm text-info"></div>');
                },
                success: function(data){
                    title = 'Bỏ kích hoạt thông tin chứng minh nhân dân ID: <span class="text-danger">'+ data.id +'</span> cho người dùng <span class="text-primary">'+ data.user.name +'</span><br>';
                    show += '<button type="button" class="btn btn-secondary active-cmnd-cancel" data-dismiss="modal">Đóng</button>';
                    show += '<a class="btn btn-primary comfirm-cmnd" name="comfirm-cmnd" href="{{ route('admin.users.cmnd_verifies.deactive') }}?id='+data.id+'">Xác nhận</a>';
                    $('.show-domain').html(title);
                    $('.modal-footer').html(show);
                },
                error: function(e) {
                    $('.modal-body').html('Truy vấn lỗi');
                },
            });
            
        });
    });

</script>

{{-- slide popup image --}}
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
<script type="text/javascript">
	jQuery(function ($) {
		$('[data-fancybox="gallery"]').fancybox({
			prevEffect    : 'none',
			nextEffect    : 'none',
			closeBtn    : false,
			helpers   : {
				title : { type : 'inside' },
				buttons : {}
			}
		});
	});
</script>
<!-- DataTables -->
<script src="{{ asset('libraries/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('libraries/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
    $(function () {
        $("#data-user-table").DataTable({
            "columnDefs": [
                 { "targets": [ 0, 4, 5, 7], "orderable": false }
            ]
        });
    });

</script>


@endSection
