@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Quản lý Server
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item "><a href="/admin/vps/">Server</a></li>
    <li class="breadcrumb-item active">Tạo Server</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper dt-bootstrap">
            <h4 class="text-center">Thông tin Server</h4>
            <div class="container">
                <form action="{{ route('admin.server.store') }}" method="post">
                  <div class="row">
                    <div class="col col-md-6">
                      <div class="order">
                        <div class="detail_order">
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Tên khách hàng:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="user_id" id="user_id" class="form-control select2">
                                <option value="" disabled>Chọn khách hàng</option>
                                @foreach ($users as $user)
                                  <option value="{{$user->id}}">{{$user->name}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Chọn sản phẩm:</label>
                            </div>
                            <div class="col col-md-8" id="product">
                              <select class="form-control select2" name="product_id">
                                <option value="" selected>Chọn sản phẩm</option>
                              </select>
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">IP:</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="ip" class="form-control" value="">
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">IP 2:</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="ip2" class="form-control" value="">
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Username:</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="user_name" class="form-control" value="">
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Password:</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="password" class="form-control" value="">
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                              <div class="col col-md-4 text-right">
                                <label for="disk2">Disk 2:</label>
                              </div>
                              <div class="col col-md-8">
                                <!-- select2 -->
                                <select name="disk2" id="disk2" class="addon_server form-control" style="width: 100%;" disabled>
                                  <option value="0" disabled>Chọn Addon Disk 2</option>
                                  <option value="0">None</option>
                                </select>
                              </div>
                          </div>
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="disk3">Disk 3:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="disk3" id="disk3" class="addon_server form-control" style="width: 100%;" disabled>
                                <option value="0" disabled>Chọn Addon Disk 3</option>
                                <option value="0">None</option>
                              </select>
                            </div>
                          </div>
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="disk4">Disk 4:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="disk4" id="disk4" class="addon_server form-control" style="width: 100%;" disabled>
                                <option value="0" disabled>Chọn Addon Disk 4</option>
                                <option value="0">None</option>
                              </select>
                            </div>
                          </div>
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="disk5">Disk 5:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="disk5" id="disk5" class="addon_server form-control" style="width: 100%;" disabled>
                                <option value="0" disabled>Chọn Addon Disk 5</option>
                                <option value="0">None</option>
                              </select>
                            </div>
                          </div>
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="disk6">Disk 6:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="disk6" id="disk6" class="addon_server form-control" style="width: 100%;" disabled>
                                <option value="0" disabled>Chọn Addon Disk 2</option>
                                <option value="0">None</option>
                              </select>
                            </div>
                          </div>
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="disk7">Disk 7:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="disk7" id="disk7" class="addon_server form-control" style="width: 100%;" disabled>
                                <option value="0" disabled>Chọn Addon Disk 7</option>
                                <option value="0">None</option>
                              </select>
                            </div>
                          </div>
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="disk8">Disk 8:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="disk8" id="disk8" class="addon_server form-control" style="width: 100%;" disabled>
                                <option value="0" disabled>Chọn Addon Disk 8</option>
                                <option value="0">None</option>
                              </select>
                            </div>
                          </div>
                          {{-- ./Addon Server --}}
                        </div>
                      </div>
                    </div>
                    <div class="col col-md-6">
                      <div class="order">
                        <div class="detail_order">
                          {{-- Rack --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Vị trí:</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="rack" class="form-control" value="">
                            </div>
                          </div>
                          {{-- status user --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Trạng thái của server:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="status_server" id="status_server" class="select2 form-control" style="width: 100%;" data-value="Hosting">
                                <option value="" disabled>Chọn trạng thái của Server</option>
                                <option value="on">Đang bật</option>
                                <option value="off">Đã tắt</option>
                                <option value="progressing">Đang cài đặt</option>
                                <option value="cancel">Hủy</option>
                                <option value="expire">Hết hạn</option>
                                <option value="delete_server">Đã xóa</option>
                              </select>
                            </div>
                          </div>
                          {{-- HĐH --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="os_server">Hệ điều hành:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="os_server" id="os_server" class="select2 form-control" style="width: 100%;">
                                <option value="" disabled selected>Chọn hệ điều hành</option>
                                @foreach ( $config_os_server as $os )
                                  <option value="{{ $os }}">{{ $os }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          {{-- RAID --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="raid">RAID:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="raid" id="raid" class="select2 form-control" style="width: 100%;">
                                @foreach ( $config_raid as $raid )
                                  <option value="{{ $raid }}">{{ $raid }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          {{-- DataCenter --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="datacenter">DataCenter:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="datacenter" id="datacenter" class="select2 form-control" style="width: 100%;">
                                @foreach ( $config_datacenter as $datacenter )
                                  <option value="{{ $datacenter }}">{{ $datacenter }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          {{-- Server Management --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="server_management">Server Management:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="server_management" id="server_management" class="form-control" style="width: 100%;">
                                <option value="Unmanaged" selected>Unmanaged</option>
                                <option value="Managed">Managed</option>
                              </select>
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Ngày tạo :</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" id="datepicker" name="date_create" value="{{date('d-m-Y')}}" class="form-control">
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Ngày hết hạn :</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" id="datepicker" name="next_due_date" value="{{date('d-m-Y')}}" class="form-control">
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Ngày thanh toán :</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" id="datepicker" name="payment_date" value="{{date('d-m-Y')}}" class="form-control">
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Thời gian:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="billing_cycle" id="billing_cycle" class="select2 form-control"  style="width: 100%;">
                                <option value="" disabled>Chọn thời gian</option>
                                @foreach ($billings as $key => $billing)
                                  <option value="{{ $key }}">{{ $billing }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                              <div class="col-md-4 text-right" style="padding-top: 5px;">
                                  <label for="">Giá áp tay :</label>
                              </div>
                              <div class="col-md-8 ml-2 icheck-primary d-inline">
                                  <input type="checkbox" class="custom-control-input" name="price_override" id="price_override" value="1">
                                  <label for="price_override">
                                  </label>
                              </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Thành tiền :</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="total" value="0" id="total" class="form-control">
                              <span class="text-danger" id="text-total"></span>
                            </div>
                          </div>
                          <div class="form-button mt-4 mr-4">
                            @csrf
                            <input type="hidden" name="type" value="server">
                            <input type="submit" value="Cập nhật Server" class="btn btn-primary float-right">
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- Modal xoa order --}}
<div class="modal fade" id="delete-order">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-order" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-order" value="Xóa đơn hàng">
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="page" value="edit_server">
<input type="hidden" id="loadAddon" value="0">
@endsection
@section('scripts')
<script src="{{ asset('js/edit_invoices.js') }}"></script>
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script type="text/javascript">
	$(document).ready(function() {
    //Date picker
		$('#datepicker').datepicker({
			autoclose: true,
            format: 'dd-mm-yyyy'
		});
    // select
    $('.select2').select2();

    $('#user_id').on('change', function() {
        var user_id = $(this).val();
        $.ajax({
            type: "get",
            url: "/admin/servers/list_product",
            data: {user_id: user_id},
            dataType: "json",
            beforeSend: function(){
                var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                $('#product').html(html);
            },
            success: function (data) {
                var html = '';
                // 1 form-group
                html += '<select name="product_id" id="product_id" data-value="Vps" class="select2 form-control" data-placeholder="Chọn sản phẩm" style="width: 100%;">';
                html += '<option disabled selected>Chọn sản phẩm</option>';
                if (data.length) {
                    $.each(data, function (index, product) {
                        html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                    });
                } else {
                    html += '<option disabled class="text-danger">Không có sản phẩm Server</option>';
                }
                html += '</select>';
                $('#product').html(html);
                $('.select2').select2();
            },
            error: function(e) {
                console.log(e);
                var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                $('#product').html(html);
            }
        });
    });

    $(document).on('change', '#product_id' , function() {
        var user_id = $('#user_id').val();
        var product_id = $(this).val();
        var loadAddon = $('#loadAddon').val();
        $.ajax({
            type: "get",
            url: "/admin/servers/list_addon_server",
            data: {user_id: user_id, product_id: product_id},
            dataType: "json",
            // beforeSend: function(){
            //     var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
            //     $('#product').html(html);
            // },
            success: function (data) {
              // console.log(data);
                var html = '';
                // 1 form-group
                html += '<option value="0" disabled>Chọn Addon Disk 5</option>';
                html += '<option value="0">None</option>';
                if (data.product.length) {
                    $.each(data.product, function (index, product) {
                        html += '<option value="'+ product.id +'">'+ product.name +'</option>';
                    });
                } else {
                    html += '<option disabled class="text-danger">Không có sản phẩm Server</option>';
                }
                if ( data.disk2 ) {
                  $('#disk2').html(html);
                  $('#disk2').attr('disabled', false);
                }
                if ( loadAddon == "0" ) {
                  $('#disk3').html(html);
                  $('#disk4').html(html);
                  $('#disk5').html(html);
                  $('#disk6').html(html);
                  $('#disk7').html(html);
                  $('#disk8').html(html);
                  $('#disk3').attr('disabled', false);
                  $('#disk4').attr('disabled', false);
                  $('#disk5').attr('disabled', false);
                  $('#disk6').attr('disabled', false);
                  $('#disk7').attr('disabled', false);
                  $('#disk8').attr('disabled', false);
                  $('#loadAddon').val("1");
                }
            },
            error: function(e) {
                console.log(e);
                var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
                $('#product').html(html);
            }
        });
        loadTotal();
    });

    $('.addon_server').on('change', function () {
      loadTotal();
    })

    $('#billing_cycle').on('change', function () {
      loadTotal();
    })

    function loadTotal() {
      var form = {
        user_id : $('#user_id').val(),
        productId : $('#product_id').val(),
        disk2 : $('#disk2').val(),
        disk3 : $('#disk3').val(),
        disk4 : $('#disk4').val(),
        disk5 : $('#disk5').val(),
        disk6 : $('#disk6').val(),
        disk7 : $('#disk7').val(),
        disk8 : $('#disk8').val(),
        billing_cycle : $('#billing_cycle').val(),
      };
      $.ajax({
          type: "get",
          url: "/admin/servers/loadTotal",
          data: form,
          dataType: "json",
          success: function (data) {
            if ( data.error == 0 ) {
              $('#total').val(data.total);
              $('#text-total').html('');
            } else {
              $('#text-total').html('Sản phẩm không có thời gian thanh toán này');
            }
          },
          error: function(e) {
              console.log(e);
              var html = '<div class="text-center">Đã có lỗi sảy ra</div>';
              $('#product').html(html);
          }
      });
    }

	});
</script>
@endsection
