@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Quản lý Server
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item "><a href="/admin/servers/">Server</a></li>
    <li class="breadcrumb-item active">Chỉnh sửa Server</li>
@endsection
@section('content')
@php
  $detail_order = $detail->detail_order;
@endphp
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper dt-bootstrap">
            <h4 class="text-center">Thông tin Server {{$detail->ip}}</h4>
            <div class="container">
                <form action="{{ route('admin.server.update') }}" method="post">
                  <div class="row">
                    <div class="col col-md-6">
                      <div class="order">
                        <div class="detail_order">
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Tên khách hàng:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="user_id" class="form-control select2">
                                <option value="" disabled>Chọn khách hàng</option>
                                @foreach ($users as $user)
                                  @php
                                    $selected = '';
                                    if ($user->id == $detail->user_id) {
                                      $selected = 'selected';
                                    }
                                  @endphp
                                  <option value="{{$user->id}}" {{$selected}}>{{$user->name}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Sản phẩm:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="product_id" class="form-control select2">
                                <option value="">Chọn sản phẩm</option>
                                @foreach ($products as $product)
                                    @php
                                        $selected = '';
                                        if ($product->id == $detail->product_id) {
                                                $selected = 'selected';
                                        }
                                    @endphp
                                    <option value="{{$product->id}}" {{$selected}}>{{$product->name}}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Tên server:</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="server_name" class="form-control"
                              value="{{ !empty($detail->product->meta_product->name_server) ? $detail->product->meta_product->name_server : '' }}">
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">IP:</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="ip" class="form-control" value="{{$detail->ip}}">
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">IP 2:</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="ip2" class="form-control" value="{{$detail->ip2}}">
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Username:</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="user_name" class="form-control" value="{{$detail->user_name}}">
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Password:</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="password" class="form-control" value="{{$detail->password}}">
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          @php
                            $product = $detail->product;
                            $list_os = !empty($product->vps_os) ? $product->vps_os : [];
                            $product_drive = !empty($product->product_drive) ? $product->product_drive : [];
                          @endphp
                          {{-- Addon Server --}}
                          {{-- addon ram --}}
                          @if ( !empty($detail->server_config->ram)  )
                            @foreach ($detail->server_config_rams as $key => $server_config_ram)
                              <div class="group-invoice">
                                <div class="col col-md-4 text-right">
                                  <label for="addon_ram">Addon RAM {{ $key + 1 }}:</label>
                                </div>
                                <div class="col col-md-8">
                                  <select name="addon_ram[]" id="addon_ram" class="select2 form-control" style="width: 100%;">
                                    <option value="" disabled>Chọn Addon RAM</option>
                                    <option value="0">None</option>
                                    @foreach( $list_add_on_ram_server as $addon_ram )
                                      @php
                                        $selected = '';
                                        if ( $addon_ram->id == $server_config_ram->product_id ) {
                                          $selected = 'selected';
                                        }
                                      @endphp
                                      <option value="{{ $addon_ram->id }}" {{ $selected }}>{{ $addon_ram->name }}</option>
                                    @endforeach
                                  </select>
                                </div>
                              </div> 
                            @endforeach
                          @else
                            <div class="group-invoice">
                              <div class="col col-md-4 text-right">
                                <label for="addon_ram">Addon RAM:</label>
                              </div>
                              <div class="col col-md-8">
                                <select name="addon_ram" id="addon_ram" class="select2 form-control" style="width: 100%;">
                                  <option value="" disabled>Chọn Addon RAM</option>
                                  <option value="0">None</option>
                                  @foreach( $list_add_on_ram_server as $addon_ram )
                                    <option value="{{ $addon_ram->id }}">{{ $addon_ram->name }}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                          @endif
                          {{-- addon disk --}}
                          @if ( empty($product_drive->second) )
                            <div class="group-invoice">
                              <div class="col col-md-4 text-right">
                                <label for="disk2">Disk 2:</label>
                              </div>
                              <div class="col col-md-8">
                                <!-- select2 -->
                                <select name="disk2" id="disk2" class=" form-control" style="width: 100%;">
                                  <option value="" disabled>Chọn Addon Disk 2</option>
                                  <option value="0">None</option>
                                  @foreach( $list_add_on_disk_server as $addon_server )
                                    @php
                                      $selected = '';
                                      if ( !empty($detail->server_config->disk2) ) {
                                        if ( $addon_server->id == $detail->server_config->disk2 ) {
                                          $selected = 'selected';
                                        }
                                      }
                                    @endphp
                                    <option value="{{ $addon_server->id }}" {{ $selected }}>{{ $addon_server->name }}</option>
                                  @endforeach
                                </select>
                              </div>
                            </div>
                          @endif
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="disk3">Disk 3:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="disk3" id="disk3" class="select2 form-control" style="width: 100%;">
                                <option value="" disabled>Chọn Addon Disk 3</option>
                                <option value="0">None</option>
                                @foreach( $list_add_on_disk_server as $addon_server )
                                  @php
                                    $selected = '';
                                    if ( !empty($detail->server_config->disk3) ) {
                                      if ( $addon_server->id == $detail->server_config->disk3 ) {
                                        $selected = 'selected';
                                      }
                                    }
                                  @endphp
                                  <option value="{{ $addon_server->id }}" {{ $selected }}>{{ $addon_server->name }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="disk4">Disk 4:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="disk4" id="disk4" class="select2 form-control" style="width: 100%;">
                                <option value="" disabled>Chọn Addon Disk 4</option>
                                <option value="0">None</option>
                                @foreach( $list_add_on_disk_server as $addon_server )
                                  @php
                                    $selected = '';
                                    if ( !empty($detail->server_config->disk4) ) {
                                      if ( $addon_server->id == $detail->server_config->disk4 ) {
                                        $selected = 'selected';
                                      }
                                    }
                                  @endphp
                                  <option value="{{ $addon_server->id }}" {{ $selected }}>{{ $addon_server->name }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="disk5">Disk 5:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="disk5" id="disk5" class="select2 form-control" style="width: 100%;">
                                <option value="" disabled>Chọn Addon Disk 5</option>
                                <option value="0">None</option>
                                @foreach( $list_add_on_disk_server as $addon_server )
                                  @php
                                    $selected = '';
                                    if ( !empty($detail->server_config->disk5) ) {
                                      if ( $addon_server->id == $detail->server_config->disk5 ) {
                                        $selected = 'selected';
                                      }
                                    }
                                  @endphp
                                  <option value="{{ $addon_server->id }}" {{ $selected }}>{{ $addon_server->name }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="disk6">Disk 6:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="disk6" id="disk6" class="select2 form-control" style="width: 100%;">
                                <option value="" disabled>Chọn Addon Disk 2</option>
                                <option value="0">None</option>
                                @foreach( $list_add_on_disk_server as $addon_server )
                                  @php
                                    $selected = '';
                                    if ( !empty($detail->server_config->disk6) ) {
                                      if ( $addon_server->id == $detail->server_config->disk6 ) {
                                        $selected = 'selected';
                                      }
                                    }
                                  @endphp
                                  <option value="{{ $addon_server->id }}" {{ $selected }}>{{ $addon_server->name }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="disk7">Disk 7:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="disk7" id="disk7" class="select2 form-control" style="width: 100%;">
                                <option value="" disabled>Chọn Addon Disk 7</option>
                                <option value="0">None</option>
                                @foreach( $list_add_on_disk_server as $addon_server )
                                  @php
                                    $selected = '';
                                    if ( !empty($detail->server_config->disk7) ) {
                                      if ( $addon_server->id == $detail->server_config->disk7 ) {
                                        $selected = 'selected';
                                      }
                                    }
                                  @endphp
                                  <option value="{{ $addon_server->id }}" {{ $selected }}>{{ $addon_server->name }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="disk3">Disk 8:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="disk8" id="disk8" class="select2 form-control" style="width: 100%;">
                                <option value="" disabled>Chọn Addon Disk 8</option>
                                <option value="0">None</option>
                                @foreach( $list_add_on_disk_server as $addon_server )
                                  @php
                                    $selected = '';
                                    if ( !empty($detail->server_config->disk8) ) {
                                      if ( $addon_server->id == $detail->server_config->disk8 ) {
                                        $selected = 'selected';
                                      }
                                    }
                                  @endphp
                                  <option value="{{ $addon_server->id }}" {{ $selected }}>{{ $addon_server->name }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          {{-- ./Addon Server --}}
                        </div>
                      </div>
                    </div>
                    <div class="col col-md-6">
                      <div class="order">
                        <div class="detail_order">
                          {{-- addon ip --}}
                          @if ( !empty($detail->server_config->ip)  )
                            @foreach ($detail->server_config_ips as $key => $server_config_ip)
                              <div class="group-invoice">
                                <div class="col col-md-4 text-right">
                                  <label for="addon_ip">Addon IP {{ $key + 1 }}:</label>
                                </div>
                                <div class="col col-md-8">
                                  <div class="row">
                                    <div class="col-md-10 pl-2 pr-0">
                                      <select name="addon_ip[]" id="addon_ip" class="select2 form-control" style="width: 100%;">
                                        <option value="" disabled>Chọn Addon IP</option>
                                        <option value="0">None</option>
                                        @foreach( $list_add_on_ip_server as $addon_ip )
                                          @php
                                            $selected = '';
                                            if ( $addon_ip->id == $server_config_ip->product_id ) {
                                              $selected = 'selected';
                                            }
                                          @endphp
                                          <option value="{{ $addon_ip->id }}" {{ $selected }}>{{ $addon_ip->name }}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                    <div class="col-md-2 p-0">
                                      <button type="button" class="btn btn-outline-primary" data-toggle="collapse" data-target="#addon_ip{{ $server_config_ip->id }}">
                                        <i class="fas fa-plus" data-toggle="tooltip" data-placement="top" title="Mở rộng"></i>
                                      </button>
                                    </div>
                                  </div>
                                  <div class="collapse mt-2" id="addon_ip{{ $server_config_ip->id }}">
                                    <textarea name="addon_ip_text[]" class="form-controll" cols="30" rows="5" placeholder="IP Addon">{{ $server_config_ip->ips }}</textarea>
                                  </div>
                                </div>
                              </div> 
                            @endforeach
                          @else
                            <div class="group-invoice">
                              <div class="col col-md-4 text-right">
                                <label for="addon_ip">Addon IP:</label>
                              </div>
                              <div class="col col-md-8">
                                <div class="row">
                                  <div class="col-md-10 pl-2 pr-0">
                                    <select name="addon_ip" id="addon_ip" class="select2 form-control" style="width: 100%;">
                                      <option value="" disabled>Chọn Addon IP</option>
                                      <option value="0">None</option>
                                      @foreach( $list_add_on_ip_server as $addon_ip )
                                        <option value="{{ $addon_ip->id }}">{{ $addon_ip->name }}</option>
                                      @endforeach
                                    </select>
                                  </div>
                                  <div class="col-md-2 p-0">
                                    <button type="button" class="btn btn-outline-primary" data-toggle="collapse" data-target="#addon_ip0">
                                      <i class="fas fa-plus" data-toggle="tooltip" data-placement="top" title="Mở rộng"></i>
                                    </button>
                                  </div>
                                </div>
                                <div class="collapse mt-2" id="addon_ip0">
                                  <textarea name="addon_ip_text[]" class="form-controll" cols="30" rows="5" placeholder="IP Addon"></textarea>
                                </div>
                              </div>
                            </div>
                          @endif
                          {{-- Rack --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Vị trí:</label>
                            </div>
                            <div class="col col-md-8">
                              <input type="text" name="rack" class="form-control" value="{{$detail->rack}}">
                            </div>
                          </div>
                          {{-- status user --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Trạng thái của server:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="status_server" id="status_server" class="select2 form-control" style="width: 100%;" data-value="Hosting">
                                <option value="" disabled>Chọn trạng thái của Server</option>
                                <option value="on" @if( $detail->status_server == 'on' ) selected @endif>Đang bật</option>
                                <option value="off" @if( $detail->status_server == 'off' ) selected @endif>Đã tắt</option>
                                <option value="progressing" @if( $detail->status_server == 'progressing' ) selected @endif>Đang cài đặt</option>
                                <option value="cancel" @if( $detail->status_server == 'cancel' ) selected @endif>Hủy</option>
                                <option value="expire" @if( $detail->status_server == 'expire' ) selected @endif>Hết hạn</option>
                                <option value="delete_server" @if( $detail->status_server == 'delete_server' ) selected @endif>Đã xóa</option>
                              </select>
                            </div>
                          </div>
                          {{-- HĐH --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="os_server">Hệ điều hành:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="os_server" id="os_server" class="select2 form-control" style="width: 100%;">
                                @foreach ( $list_os as $os )
                                  @php
                                    $selected = '';
                                    if ( $config_os_server[$os->os] == $detail->os ) {
                                      $selected = 'selected';
                                    }
                                  @endphp
                                  <option value="{{ $config_os_server[$os->os] }}" {{ $selected }}>{{ $config_os_server[$os->os] }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          {{-- RAID --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="raid">RAID:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="raid" id="raid" class="select2 form-control" style="width: 100%;">
                                @foreach ( $config_raid as $raid )
                                  @php
                                    $selected = '';
                                    if ( $raid == $detail->raid ) {
                                      $selected = 'selected';
                                    }
                                  @endphp
                                  <option value="{{ $raid }}" {{ $selected }}>{{ $raid }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          {{-- DataCenter --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="datacenter">DataCenter:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="datacenter" id="datacenter" class="select2 form-control" style="width: 100%;">
                                @foreach ( $config_datacenter as $datacenter )
                                  @php
                                    $selected = '';
                                    if ( $datacenter == $detail->location ) {
                                      $selected = 'selected';
                                    }
                                  @endphp
                                  <option value="{{ $datacenter }}" {{ $selected }}>{{ $datacenter }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Ngày đặt hàng:</label>
                            </div>
                            <div class="col col-md-8">
                              <p>{{date('m/d/Y', strtotime($detail->created_at))}}</p>
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Ngày tạo :</label>
                            </div>
                            <div class="col col-md-8">
                              @if (!empty($detail->date_create))
                              {{date('d/m/Y', strtotime($detail->date_create))}}
                              @else
                              <p class="text-danger">Chưa tạo</p>
                              @endif
                              <span class="ml-4">
                                <a href="#" class="btn btn-outline-default date_create" data-date="{{date('d-m-Y', strtotime($detail->date_create))}}"><i class="fas fa-edit"></i></a>
                              </span>
                              <div class="input_date_create">
                              </div>
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Ngày hết hạn :</label>
                            </div>
                            <div class="col col-md-8">
                              @if (!empty($detail->next_due_date))
                              {{date('d/m/Y', strtotime($detail->next_due_date))}}
                              @else
                              <p class="text-danger">Chưa tạo</p>
                              @endif
                              <span class="ml-4">
                                <a href="#" class="btn btn-outline-default next_due_date" data-date='{{date('d-m-Y', strtotime($detail->next_due_date))}}'><i class="fas fa-edit"></i></a>
                              </span>
                              <div class="input_next_due_date">
                              </div>
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Ngày thanh toán :</label>
                            </div>
                            <div class="col col-md-8">
                              @if(!empty($detail->paid))
                                @if($detail->paid == 'paid')
                                  <span class="text-success">{{date('m/d/Y', strtotime($detail_order->paid_date))}}</span>
                                @elseif($detail->paid == 'cancel')
                                  <span class="text-secondary">Hủy</span>
                                @else
                                  <span class="text-danger">Chưa thanh toán</span>
                                @endif
                              @else
                                <span class="text-danger">Chưa thanh toán</span>
                              @endif
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Thời gian:</label>
                            </div>
                            <div class="col col-md-8">
                              <select name="billing_cycle" id="billing_cycle" class="select2 form-control"  style="width: 100%;">
                                <option value="" disabled>Chọn thời gian</option>
                                @foreach ($billings as $key => $billing)
                                  @php
                                    $selected = '';
                                    if ($key == $detail->billing_cycle) {
                                      $selected = 'selected';
                                    }
                                  @endphp
                                  <option value="{{ $key }}" {{$selected}}>{{ $billing }}</option>
                                @endforeach
                              </select>
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Thành tiền :</label>
                            </div>
                            <div class="col col-md-8">
                              @if ( !empty($detail->amount) )
                                <b class="text-danger">{!!number_format(($detail->amount),0,",",".")!!} VNĐ</b>
                                <span class="ml-4">
                                  <a href="#" class="btn btn-outline-default price" data-date="{{ $detail->amount }}"><i class="fas fa-edit"></i></a>
                                </span>
                                <div class="input_price">
                                </div>
                              @else
                                @php
                                  $total = 0;
                                  $product = $detail->product;
                                  $total = !empty( $product->pricing[$detail->billing_cycle] ) ? $product->pricing[$detail->billing_cycle] : 0;
                                  if (!empty($detail->server_config)) {
                                    $server_config = $detail->server_config;
                                    $pricing_addon = 0;
                                    if ( !empty( $server_config->ram ) ) {
                                        foreach ($detail->server_config_rams as $server_config_ram) {
                                            $pricing_addon += !empty($server_config_ram->product->pricing[$detail->billing_cycle]) ? $server_config_ram->product->pricing[$detail->billing_cycle] : 0;
                                        }
                                    }
                                    if ( !empty( $server_config->ip ) ) {
                                        foreach ($detail->server_config_ips as $server_config_ip) {
                                            $pricing_addon += !empty($server_config_ip->product->pricing[$detail->billing_cycle]) ? $server_config_ip->product->pricing[$detail->billing_cycle] : 0;
                                        }
                                    }
                                    if ( !empty($server_config->disk2) ) {
                                      $pricing_addon += $server_config->product_disk2->pricing[$detail->billing_cycle];
                                    }
                                    if ( !empty($server_config->disk3) ) {
                                      $pricing_addon += $server_config->product_disk3->pricing[$detail->billing_cycle];
                                    }
                                    if ( !empty($server_config->disk4) ) {
                                      $pricing_addon += $server_config->product_disk4->pricing[$detail->billing_cycle];
                                    }
                                    if ( !empty($server_config->disk5) ) {
                                      $pricing_addon += $server_config->product_disk5->pricing[$detail->billing_cycle];
                                    }
                                    if ( !empty($server_config->disk6) ) {
                                      $pricing_addon += $server_config->product_disk6->pricing[$detail->billing_cycle];
                                    }
                                    if ( !empty($server_config->disk7) ) {
                                      $pricing_addon += $server_config->product_disk7->pricing[$detail->billing_cycle];
                                    }
                                    if ( !empty($server_config->disk8) ) {
                                      $pricing_addon += $server_config->product_disk8->pricing[$detail->billing_cycle];
                                    }
                                    $total += $pricing_addon;
                                  }
                                @endphp
                                {!!number_format(($total),0,",",".")!!} VNĐ
                                <span class="ml-4">
                                  <a href="#" class="btn btn-outline-default price" data-date="{{ $total }}"><i class="fas fa-edit"></i></a>
                                </span>
                                <div class="input_price">
                                </div>
                              @endif
                            </div>
                          </div>
                          {{-- 1 form group --}}
                          <div class="group-invoice">
                            <div class="col col-md-4 text-right">
                              <label for="">Cấu hình thay thế:</label>
                            </div>
                            <div class="col col-md-8">
                              <textarea id="config_text" name="config_text" class="form-controll" cols="32" rows="5" placeholder="Cấu hình thay thế">
                                {{ trim($detail->config_text, ' ') }}
                              </textarea>
                            </div>
                          </div>
                          <div class="form-button mt-4 mr-4">
                            @csrf
                            <input type="hidden" name="id" value="{{ $detail->id }}">
                            <input type="hidden" name="type" value="server">
                            <input type="hidden" name="invoice_id" value="{{ $detail->detail_order_id }}">
                            <input type="submit" value="Cập nhật Server" class="btn btn-primary float-right">
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                </form>
            </div>
        </div>
    </div>
</div>
{{-- Modal xoa order --}}
<div class="modal fade" id="delete-order">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-order" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-order" value="Xóa đơn hàng">
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="page" value="edit_server">
@endsection
@section('scripts')
<script src="{{ asset('js/edit_invoices.js') }}"></script>
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script type="text/javascript">
	$(document).ready(function() {
    $('#config_text').html($('#config_text').html().trim());
		//Date picker
    $('#datepicker2').datepicker({
			autoclose: true,
            format: 'dd-mm-yyyy'
		});
    // select
    $('.select2').select2();

    $('.price').on('click', function (e) {
      e.preventDefault();
      var amount = $(this).attr('data-date');
      var html = '<input type="text" value="'+ amount +'" class="form-control" name="price">';
      $('.input_price').html(html);
    })

    $('.next_due_date').on('click', function (e) {
      e.preventDefault();
      var date = $(this).attr('data-date');
      var html = '<input type="text" id="datepicker2" name="next_due_date" value="'+ date +'" class="form-control">';
      $('.input_next_due_date').html(html);
    })

    $('.date_create').on('click', function (e) {
      e.preventDefault();
      var date = $(this).attr('data-date');
      var html = '<input type="text" id="datepicker2" name="date_create" value="'+ date +'" class="form-control">';
      $('.input_date_create').html(html);
    })

	});
</script>
@endsection
