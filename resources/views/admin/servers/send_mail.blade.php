@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('/libraries/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
<link rel="stylesheet" href="{{ asset('/libraries/toastr/toastr.min.css')}}">
@endsection
@section('title')
    <i class="fas fa-envelope-square"></i> Quản lý Server
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
<li class="breadcrumb-item active">Server</li>
@endsection
@section('content')
<div class="col-md-12 detail-course-finish">
    @if(session("success"))
        <div class="bg-success">
            <p class="text-light">{{session("success")}}</p>
        </div>
    @elseif(session("fails"))
        <div class="bg-danger">
            <p class="text-light">{{session("fails")}}</p>
        </div>
    @endif
</div>
<div class="col-md-12" style="margin:auto">
    <div class="card card-info">
        <div class="card-header">
            <h3 class="card-title text-center">Gửi Email hoàn thành cài đặt Server</h3>
        </div>
        <div class="card-body">
            <form action="{{ route('admin.server.sendMail') }}" method="post">
                @csrf
                <div class="form-group">
                    <input class="form-control" type="text" name="userName" value="{{ $server->user->name }}" disabled>
                    <input type="hidden" name="userId" value="{{ $server->user_id }}">
                    <input type="hidden" name="serverId" value="{{ $server->id }}">
                </div>
                <div class="form-group">
                    <input class="form-control" name="subject" value="{{ $sendMailFinishConfig['subject'] }}" required placeholder="Subject:">
                </div>
                <div class="form-group">
                    <textarea name="content" id="content" class="form-control">{{ $sendMailFinishConfig['content'] }}</textarea>
                </div>
                <div class="card-footer">
                  <div class="float-right">
                      <button type="submit" class="btn btn-primary"><i class="far fa-envelope"></i> Send</button>
                  </div>
                  <a href="{{ route('admin.server.sendMail') }}" class="btn btn-default"><i class="fas fa-times"></i> Hủy</a>
                </div>
            </form>
        </div>
    </div>
</div>
<input type="hidden" id="page" value="send_mail_with_user">
@endsection
@section('scripts')
<script src="{{ asset('/libraries/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('/libraries/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script type="text/javascript">
    $(document).ready(function() {
        CKEDITOR.replace( 'content', { height: "400px" });
        $('.select2').select2();
    })
</script>
@endSection
