@extends('layouts.app')
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Danh sách Server
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Server</li>
@endsection
@section('content')
@php
use Carbon\Carbon;
@endphp
<div class="row">
	<div data-v-a16ce4fa="" class="col-md-4">
		<a class="btn btn-primary" href="{{ route('admin.server.create') }}">Tạo Server</a>
	</div>
  <div class="col-md-4">
    <div class="row">
        <select class="form-control" id="status">
          <option value="" disabled selected>Chọn trạng thái của Server</option>
          <option value="on">Trạng thái đang bật</option>
          <option value="off">Trạng thái đã tắt</option>
          <option value="progressing">Trạng thái đang tạo</option>
          <option value="rebuild">Trạng thái đang cài lại</option>
          <option value="change_ip">Trạng thái đang đổi IP</option>
          <option value="reset_password">Trạng thái đặt lại mật khẩu</option>
          <option value="expire">Trạng thái hết hạn</option>
          {{-- <option value="admin_off">Trạng thái Admin tắt</option>
          <option value="suspend">Trạng thái đã khóa</option> --}}
          <option value="change_user">Trạng thái đã chuyển khách hàng</option>
          <option value="delete_vps">Trạng thái đã xóa</option>
          <option value="cancel">Trạng thái đã hủy</option>
        </select>
    </div>
</div>
<!-- form search -->
<div class="col-md-4">
    <div class="row">
        <div class="form-group col-md-12">
          <div class="input-group">
            <input type="text" name="search" class="form-control" placeholder="Tìm kiếm Server" id="q">
            <div class="input-group-append">
              <button type="submit" class="btn btn-danger"><i class="fas fa-search"></i></button>
            </div>
          </div>
        </div>
    </div>
</div>
  <div class="mt-4 pl-4 col-md-12" style="display: inherit;">
    <div class="form-group">
        <span class="mt-1">Số lượng</span> 
        <select id="qtt">
            <option value="10">10</option>
            <option value="30" selected>30</option>
            <option value="50">50</option>
            <option value="100">100</option>
            <option value="200">200</option>
            <option value="500">500</option>
        </select>
    </div>
    
    <div class="form-group text-primary ml-4">
        <b>Đã chọn: <span class="last-item">0</span> / <span class="total-item">{{ $servers->count() }}</span></b>
    </div>
</div>
<div class="col-md-12" style='padding-left:30px; display:none' id='btn-hide-group'>
  <button class='btn btn-sm btn-danger' id='deleteServerBtn'><i class='fa fa-trash'></i> Xóa mục đã chọn</button>
</div>
</div>
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-bordered text-center">
                <thead class="primary">
                  <th>
                    <input type="checkbox"  class="checkbox_all">
                </th>
                    <th>Tên khách hàng</th>
                    <th>Sản phẩm</th>
                    <th>IP</th>
                    <th>Cấu hình</th>
                    <th>Datacenter <br> Vị trí</th>
                    <th>Ngày tạo</th>
                    <th>Ngày kết thúc</th>
                    <th>Tổng thời gian thuê</th>
                    <th>Chu kỳ thanh toán</th>
                    <th>Chi phí</th>
                    <th>Trạng thái</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                    @foreach ($servers as $server)
                        @php
                          $total_time = '';
                          $create_date = new Carbon($server->date_create);
                          $next_due_date = new Carbon($server->next_due_date);
                          if ( $next_due_date->diffInYears($create_date) ) {
                            $year = $next_due_date->diffInYears($create_date);
                            $total_time = $year . ' Năm ';
                            $create_date = $create_date->addYears($year);
                            $month = $next_due_date->diffInMonths($create_date);
                            //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                            if ( $month ) {
                              $total_time .= $month . ' Tháng';
                            }
                          } else {
                            $diff_month = $next_due_date->diffInMonths($create_date);
                            $total_time = $diff_month . ' Tháng';
                          }
                        @endphp
                        <tr>
                          <td>
                            <input type="checkbox" value="{{ $server->id }}" data-ip="{{ $server->ip }}" class="checkbox">
                        </td>
                            <td>
                              @if (!empty($server->user->id))
                                <a href="{{ route('admin.user.detail' , !empty($server->user->id) ? $server->user->id : 0 ) }}">{{ !empty($server->user->name) ? $server->user->name : 'Đã xóa' }}</a>
                              @else
                                <span class="text-danger">Đã xóa</span>
                              @endif
                            </td>
                            <td>
                              @if (!empty($server->product))
                                @if ( !empty($server->product->duplicate) )
                                  <a href="{{ route('admin.product.edit' , $server->product->id) }}">{{ $server->product->name }}</a>
                                @else
                                  @if(!empty($server->product->group_product->private))
                                      <a href="{{ route('admin.product_private.editPrivate' , [$server->product->group_product->id, $server->product->id]) }}">{{ $server->product->name }}</a>
                                  @else
                                    <a href="{{ route('admin.product.edit' , $server->product->id) }}">{{ $server->product->name }}</a>
                                  @endif
                                @endif    
                              @endif
                            </td>
                            <td>
                                @if (!empty($server->ip))
                                  <a href="{{ route('admin.server.detail', $server->id) }}">{{ $server->ip }}</a> <br>
                                  <a href="{{ route('admin.server.detail', $server->id) }}">{{ $server->ip2 }}</a>
                                @else
                                    <span class="text-danger">Chưa tạo</span>
                                @endif
                            </td>
                            <td>
                              @if ( !empty($server->config_text) )
                                {!! $server->config_text !!}
                              @else 
                                @php
                                    $addonConfig = '';
                                    $product = $server->product;
                                    $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                    $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                                    $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                    $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                @endphp
                                {{ $cpu }} ({{ $cores }}), {{ $ram }},
                                {{ $disk }} ({{ $server->raid }})
                              @endif  
                              @php
                                $addonConfig = GroupProduct::get_config_server($server->id);    
                              @endphp
                              <br>
                              HĐH: {{ $server->os }}
                              <br>
                              {!! $addonConfig !!}
                            </td>
                            <td>{{ $server->location }} <br> {{ $server->rack }}</td>
                            <td>
                                @if (!empty($server->date_create))
                                    <span>{{ date('d-m-Y', strtotime($server->date_create)) }}</span>
                                @else
                                    <span class="text-danger">Chưa tạo</span>
                                @endif
                            </td>
                            <td>
                                @if (!empty($server->next_due_date))
                                    <span>{{ date('d-m-Y', strtotime($server->next_due_date)) }}</span>
                                @else
                                    <span class="text-danger">Chưa tạo</span>
                                @endif
                            </td>
                            <td>{{ $total_time }}</td>
                            <td>
                                {{ $billings[$server->billing_cycle] }}
                            </td>
                            <td>
                              @if ( !empty($server->amount) )
                                <b class="text-danger">{!!number_format( $server->amount ,0,",",".")!!}</b>
                              @else
                              @php
                                 $total = 0;
                                 $product = $server->product;
                                 $total = !empty( $product->pricing[$server->billing_cycle] ) ? $product->pricing[$server->billing_cycle] : 0;
                                 if (!empty($server->server_config)) {
                                   $server_config = $server->server_config;
                                   $pricing_addon = 0;
                                   if ( !empty( $server_config->ram ) ) {
                                       foreach ($server->server_config_rams as $server_config_ram) {
                                           $pricing_addon += !empty($server_config_ram->product->pricing[$server->billing_cycle]) ? $server_config_ram->product->pricing[$server->billing_cycle] : 0;
                                       }
                                   }
                                   if ( !empty( $server_config->ip ) ) {
                                       foreach ($server->server_config_ips as $server_config_ip) {
                                           $pricing_addon += !empty($server_config_ip->product->pricing[$server->billing_cycle]) ? $server_config_ip->product->pricing[$server->billing_cycle] : 0;
                                       }
                                   }
                                   if ( !empty($server_config->disk2) ) {
                                     $pricing_addon += $server_config->product_disk2->pricing[$server->billing_cycle];
                                   }
                                   if ( !empty($server_config->disk3) ) {
                                     $pricing_addon += $server_config->product_disk3->pricing[$server->billing_cycle];
                                   }
                                   if ( !empty($server_config->disk4) ) {
                                     $pricing_addon += $server_config->product_disk4->pricing[$server->billing_cycle];
                                   }
                                   if ( !empty($server_config->disk5) ) {
                                     $pricing_addon += $server_config->product_disk5->pricing[$server->billing_cycle];
                                   }
                                   if ( !empty($server_config->disk6) ) {
                                     $pricing_addon += $server_config->product_disk6->pricing[$server->billing_cycle];
                                   }
                                   if ( !empty($server_config->disk7) ) {
                                     $pricing_addon += $server_config->product_disk7->pricing[$server->billing_cycle];
                                   }
                                   if ( !empty($server_config->disk8) ) {
                                     $pricing_addon += $server_config->product_disk8->pricing[$server->billing_cycle];
                                   }
                                   $total += $pricing_addon;
                                 }
                                @endphp
                                {!!number_format( $total ,0,",",".")!!}
                              @endif
                            </td>
                            <td>
                                @if ($server->status_server == 'on')
                                    <span class="text-success">Đang bật</span>
                                @elseif ($server->status_server == 'off')
                                    <span class="text-danger">Đã tắt</span>
                                @elseif ($server->status_server == 'progressing')
                                    <span class="server-progressing">Đang cài đặt ...</span>
                                @elseif ($server->status_server == 'expire')
                                    <span class="text-danger" data-id="{{ $server->id }}">Đã hết hạn</span>
                                @elseif ($server->status_server == 'cancel')
                                    <span class="text-danger">Đã hủy</span>
                                @elseif ($server->status_server == 'delete_server')
                                    <span class="text-danger">Đã xóa</span>
                                @else
                                    <span class="text-danger">Chưa thanh toán</span>
                                @endif
                            </td>
                            <td class="button-action">
                                <a href="{{ route('admin.server.detail', $server->id) }}" class="btn btn-sm btn-warning text-white"><i class="fas fa-edit"></i></a>
                                @if (empty($server->send_mail_create))
                                  <a class="btn btn-success btn-sm text-light" data-toggle="tooltip" href="{{ route('admin.server.sendMailFinishConfig', $server->id) }}" data-placement="top" title="Gửi email hoàn thành cài đặt"><i class="fas fa-envelope-open-text"></i></a>
                                @endif
                                @if ( $server->status_server != 'delete_server' )
                                  <button class="btn btn-sm btn-danger btn-delete-vps" data-id="{{$server->id}}" data-ip="{{$server->ip}}" ><i class="fas fa-trash-alt"></i></button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot class="card-footer clearfix">
                    <td colspan="13" class="text-center">
                        {{ $servers->links()  }}
                    </td>
                </tfoot>
            </table>
        </div>
    </div>
    <!-- <div class="container" id="group_button">
        <div class="table-responsive" style="display: block;">
            <button class="btn btn-success btn-action-vps" data-type="mark_paid">Tạo VPS</button>
            <button class="btn btn-outline-secondary btn-action-vps" data-type="cancel">Hủy</button>
            <button class="btn btn-danger btn-action-vps" data-type="delete">Xóa</button>
        </div>
    </div> -->
</div>
{{-- Modal xoa order --}}
<div class="modal fade" id="delete-order">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-order" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-order" value="Xóa đơn hàng">
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
{{-- Modal xoa vps --}}
<div class="modal fade" id="delete-vps">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-vps" value="Xóa đơn hàng">
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<input type="hidden" id="page" value="list_server">
@endsection
@section('scripts')
<script src="{{ asset('js/admin_server.js') }}"></script>
@endsection
