<!doctype html>
<html>
  <head>
    <title>Cloudzone Customer System</title>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
    <style>
      /* -------------------------------------
          GLOBAL RESETS
      ------------------------------------- */

      /*All the styling goes here*/

      img {
        border: none;
        -ms-interpolation-mode: bicubic;
        max-width: 100%;
      }

      body {
        background-color: #f6f6f6;
        font-family: sans-serif;
        -webkit-font-smoothing: antialiased;
        font-size: 14px;
        line-height: 1.4;
        margin: 0;
        padding: 0;
        -ms-text-size-adjust: 100%;
        -webkit-text-size-adjust: 100%;
      }

      table {
        border-collapse: separate;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
        width: 100%; }
        table td {
          font-family: sans-serif;
          font-size: 14px;
          vertical-align: top;
      }

      /* -------------------------------------
          BODY & CONTAINER
      ------------------------------------- */

      .body {
        background-color: #f6f6f6;
        width: 100%;
      }

      /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
      .container {
        display: block;
        margin: 0 auto !important;
        /* makes it centered */
        max-width: 580px;
        padding: 10px;
        width: 580px;
      }

      /* This should also be a block element, so that it will fill 100% of the .container */
      .content {
        box-sizing: border-box;
        display: block;
        margin: 0 auto;
        max-width: 580px;
        padding: 10px;
      }

      /* -------------------------------------
          HEADER, FOOTER, MAIN
      ------------------------------------- */
      .main {
        background: #ffffff;
        border-radius: 3px;
        width: 100%;
      }

      .wrapper {
        box-sizing: border-box;
        padding: 20px;
      }

      .wrapper-header {
        box-sizing: border-box;
        border-bottom: 1px solid #ddd;
        padding-bottom: 10px;
        padding-top: 10px;
      }

      .content-block {
        padding-bottom: 10px;
        padding-top: 10px;
      }

      .footer {
        clear: both;
        margin-top: 10px;
        text-align: center;
        width: 100%;
      }
        .footer td,
        .footer p,
        .footer span,
        .footer a {
          color: #999999;
          font-size: 12px;
          text-align: center;
      }

      /* -------------------------------------
          TYPOGRAPHY
      ------------------------------------- */
      h1,
      h2,
      h3,
      h4 {
        color: #000000;
        font-family: sans-serif;
        font-weight: 400;
        line-height: 1.4;
        margin: 0;
        margin-bottom: 30px;
      }

      h1 {
        font-size: 35px;
        font-weight: 300;
        text-align: center;
        text-transform: capitalize;
      }

      p,
      ul,
      ol {
        font-family: sans-serif;
        font-size: 14px;
        font-weight: normal;
        margin: 0;
        margin-bottom: 15px;
      }
        p li,
        ul li,
        ol li {
          list-style-position: inside;
          margin-left: 5px;
      }

      a {
        color: #3498db;
        text-decoration: underline;
      }

      /* -------------------------------------
          BUTTONS
      ------------------------------------- */
      .btn {
        box-sizing: border-box;
        width: 100%; }
        .btn > tbody > tr > td {
          padding-bottom: 15px; }
        .btn table {
          width: auto;
      }
        .btn table td {
          background-color: #ffffff;
          border-radius: 5px;
          text-align: center;
      }
        .btn a {
          background-color: #ffffff;
          border: solid 1px #3498db;
          border-radius: 5px;
          box-sizing: border-box;
          color: #3498db;
          cursor: pointer;
          display: inline-block;
          font-size: 14px;
          font-weight: bold;
          margin: 0;
          padding: 12px 25px;
          text-decoration: none;
          text-transform: capitalize;
      }

      .btn_tutorial a {
          background-color: #ffffff;
          border: solid 1px #3498db;
          border-radius: 5px;
          box-sizing: border-box;
          color: #3498db;
          cursor: pointer;
          display: inline-block;
          font-size: 13px;
          font-weight: bold;
          margin: 0;
          padding: 2px 8px;
          text-decoration: none;
          text-transform: capitalize;
      }

      .btn_primary a {
        background-color: #5271ff;
        border-color: #5271ff;
        color: #ffffff;
      }

      .btn-primary table td {
        background-color: #3498db;
      }

      .btn-primary a {
        /* background-color: #3498db;
        border-color: #3498db; #5271ff*/
        background-color: #5271ff;
        border-color: #5271ff;
        color: #ffffff;
      }

      /* -------------------------------------
          OTHER STYLES THAT MIGHT BE USEFUL
      ------------------------------------- */
      .last {
        margin-bottom: 0;
      }

      .first {
        margin-top: 0;
      }

      .align-center {
        text-align: center;
      }

      .align-right {
        text-align: right;
      }

      .align-left {
        text-align: left;
      }

      .clear {
        clear: both;
      }

      .mt0 {
        margin-top: 0;
      }

      .mb0 {
        margin-bottom: 0;
      }

      .preheader {
        color: transparent;
        display: none;
        height: 0;
        max-height: 0;
        max-width: 0;
        opacity: 0;
        overflow: hidden;
        mso-hide: all;
        visibility: hidden;
        width: 0;
      }

      .powered-by a {
        text-decoration: none;
      }

      hr {
        border: 0;
        border-bottom: 1px solid #f6f6f6;
        margin: 20px 0;
      }

      /* -------------------------------------
          RESPONSIVE AND MOBILE FRIENDLY STYLES
      ------------------------------------- */
      @media only screen and (max-width: 620px) {
        table[class=body] h1 {
          font-size: 28px !important;
          margin-bottom: 10px !important;
        }
        table[class=body] p,
        table[class=body] ul,
        table[class=body] ol,
        table[class=body] td,
        table[class=body] span,
        table[class=body] a {
          font-size: 16px !important;
        }
        table[class=body] .wrapper,
        table[class=body] .article {
          padding: 10px !important;
        }
        table[class=body] .content {
          padding: 0 !important;
        }
        table[class=body] .container {
          padding: 0 !important;
          width: 100% !important;
        }
        table[class=body] .main {
          border-left-width: 0 !important;
          border-radius: 0 !important;
          border-right-width: 0 !important;
        }
        table[class=body] .btn table {
          width: 100% !important;
        }
        table[class=body] .btn a {
          width: 100% !important;
        }
        table[class=body] .img-responsive {
          height: auto !important;
          max-width: 100% !important;
          width: auto !important;
        }
      }

      /* -------------------------------------
          PRESERVE THESE STYLES IN THE HEAD
      ------------------------------------- */
      @media all {
        .ExternalClass {
          width: 100%;
        }
        .ExternalClass,
        .ExternalClass p,
        .ExternalClass span,
        .ExternalClass font,
        .ExternalClass td,
        .ExternalClass div {
          line-height: 100%;
        }
        .apple-link a {
          color: inherit !important;
          font-family: inherit !important;
          font-size: inherit !important;
          font-weight: inherit !important;
          line-height: inherit !important;
          text-decoration: none !important;
        }
        #MessageViewBody a {
          color: inherit;
          text-decoration: none;
          font-size: inherit;
          font-family: inherit;
          font-weight: inherit;
          line-height: inherit;
        }
        .btn-primary table td:hover {
          background-color: #3498db !important;
        }
        .btn-primary a:hover {
          background-color: #3498db !important;
          border-color: #3498db !important;
        }
        .btn_primary a:hover {
          background-color: #3498db !important;
          border-color: #3498db !important;
        }
      }

    </style>
  </head>
  <body class="">
    <!-- <span class="preheader">This is preheader text. Some clients will show this text as a preview.</span> -->
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body">
      <tr>
        <td>&nbsp;</td>
        <td class="container">
          <div class="content">

            <!-- START CENTERED WHITE CONTAINER -->
            <table role="presentation" class="main">

              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="wrapper-header">
                  <table role="presentation" class="section header" border="0" cellpadding="0" cellspacing="0">
                      <tr>
                          <td class="column" align="center">
                              <table>
                                  <tbody>
                                      <tr>
                                          <td align="center">
                                              <img src="https://portal.cloudzone.vn/images/logo-cloudzone-mail.png" alt="logo-mail" width="200px">
                                          </td>
                                      </tr>
                                  </tbody>
                              </table>
                          </td>
                      </tr>
                  </table>
                </td>
              </tr>
              <tr>
                <td class="wrapper">
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                      <td>
                        <p>Kính chào <b style="color: #5271ff;">{{ $name }}</b>,</p>
                        <p  style="text-align: justify;">
                          Cảm ơn Quý khách đã sử dụng tài khoản Web Portal của Cloudzone - đơn vị cung cấp dịch vụ Cloud VPS, Hosting chuyên nghiệp.
                        </p>
                        <p>Để nâng cao chất lượng dịch vụ, Cloudzone vừa cập nhật một loạt chức năng mới trên Web Portal.</p>
                        <ul>
                          <li>
                            Chức năng đổi IP cho VPS US: <a href="https://support.cloudzone.vn/knowledge-base/huong-dan-doi-ip-cho-vps-us/" target="_blank">https://support.cloudzone.vn/knowledge-base/huong-dan-doi-ip-cho-vps-us/</a>
                          </li>
                          <!-- style="display: block;" -->
                          <li>
                            Chức năng cài lại hệ điều hành cho VPS US: <a href="https://support.cloudzone.vn/knowledge-base/huong-dan-cai-lai-he-dieu-hanh-vps-us/" target="_blank">https://support.cloudzone. vn/knowledge-base/huong-dan-cai-lai-he-dieu-hanh-vps-us/</a>
                          </li>
                          <li>
                            Reset password cho VPS US
                          </li>
                        </ul>
                        <p style="text-align: justify;">Ngoài ra, quý khách có thể trải nghiệm nhiều tính năng khác đã có trên Portal như:</p>
                        <!--   class="btn_primary btn_tutorial" -->
                        <ul>
                          <li>
                            Tìm kiếm, gia hạn nhiều VPS cùng lúc 
                          </li>
                          <li>
                            Chức năng Console VPS
                          </li>
                        </ul>
                        <p style="display: block;">Toàn bộ các hướng dẫn liên quan đến Portal tham khảo tại đây: <a href="https://support.cloudzone.vn/article-categories/huong-dan-su-dung-cloudzone-portal/" target="_blank">https://support.cloudzone.vn/article-categories/huong-dan-su-dung-cloudzone-portal/</a></p>
                        <p>Mọi thắc mắc trong quá trình sử dụng xin liên hệ Cloudzone qua Fanpage, Email, SĐT để được tư vấn và hỗ trợ.</p>
                        <p>Cảm ơn Quý khách đã tin tưởng và lựa chọn sử dụng dịch vụ tại
                          <span style="color: #5271ff;font-weight: bold;">CLOUDZONE!</span>
                        </p>
                        <p>Trân trọng!</p>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->
            </table>
            <!-- END CENTERED WHITE CONTAINER -->

            <!-- START FOOTER -->
            <div class="footer">
              <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td class="content-block">
                    <span class="apple-link" style="color: #878787;">
                      <b>CÔNG TY TNHH MTV CÔNG NGHỆ ĐẠI VIỆT SỐ</b>
                    </span> <br>
                    <span class="apple-link">Địa chỉ: 257 Lê Duẩn , Đà Nẵng.</span> <br>
                    <span class="apple-link">Văn phòng: 67 Nguyễn Thị Định, Đà Nẵng.</span> <br>
                    <span class="apple-link">Điện thoại: 08 8888 0043 - Website: <a href="https://cloudzone.vn ">https://cloudzone.vn </a></span> <br>
                    Email: <a href="mailto:support@cloudzone.vn">support@cloudzone.vn</a>
                    - Fanpage: <a href="https://www.facebook.com/cloudzone.vn">https://www.facebook.com/cloudzone.vn</a>
                    <div style="padding-top: 5px;">
                        <a href="https://m.me/cloudzone.vn" style="padding-right: 5px;"><img src="https://portal.cloudzone.vn/images/icon-facebook.png" width="30px"></a>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
            <!-- END FOOTER -->
          </div>
        </td>
        <td>&nbsp;</td>
      </tr>
    </table>
  </body>
</html>
