@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Quản lý hóa đơn
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Hóa đơn</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-bordered">
                <thead class="primary">
                    <!-- <th></th> -->
                    <th>ID</th>
                    <th id="user_invoice">
                      Tên khách hàng
                      <span class="float-right">
                        <i class="fas fa-filter"></i>
                        <input type="hidden" id="input_filter_user" value="">
                      </span>
                    </th>
                    <th id="type_invoice">
                      Loại
                      <span class="float-right">
                        <i class="fas fa-filter"></i>
                        <input type="hidden" id="input_filter_type" value="">
                      </span>
                    </th>
                    <th>Ngày tạo</th>
                    <th>Ngày kết thúc</th>
                    <th>Ngày thanh toán</th>
                    <th>Giá thành</th>
                    <th>Số lượng</th>
                    <th id="status_invoice">
                      Trạng thái
                      <span class="float-right">
                        <i class="fas fa-filter"></i>
                        <input type="hidden" id="input_filter_status" value="all">
                      </span>
                    </th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                    @foreach ($detail_orders as $detail_order)
                        <tr>
                            <!-- <td><input type="checkbox" value="{{ $detail_order->id }}" class="invoice_checkbox"></td> -->
                            <td>
                              <a href="{{ route('admin.invoices.edit', $detail_order->id) }}">#{{ $detail_order->id }}</a>
                            </td>
                            <td>
                              <a href="{{ route('admin.user.detail', !empty($detail_order->user->id) ? $detail_order->user->id : 6) }}">{{ !empty($detail_order->user->name) ? $detail_order->user->name : '' }}</a>
                            </td>
                            <td>
                              {{ $detail_order->type }}
                              (
                                @if($detail_order->type == 'addon_vps')
                                  Addon
                                @elseif($detail_order->order->type == 'expired')
                                  Gia hạn
                                @else
                                  Tạo
                                @endif
                              )
                            </td>
                            <td>{{ date('d-m-Y', strtotime($detail_order->created_at)) }}</td>
                            <td>{{ date('d-m-Y', strtotime($detail_order->due_date)) }}</td>
                            <td>
                                @if(!empty($detail_order->paid_date))
                                    {{ date('d-m-Y', strtotime($detail_order->paid_date)) }}
                                @else
                                    <span class="text-danger">Chưa thanh toán<span>
                                @endif
                            </td>
                            <td>{!!number_format($detail_order->sub_total,0,",",".") . ' VNĐ'!!}</td>
                            <td>{{ $detail_order->quantity }}</td>
                            <td>
                                @if ($detail_order->status == 'paid')
                                    <span class="text-success">{{ $invoices[$detail_order->status] }}</span>
                                @elseif ($detail_order->status == 'unpaid')
                                    <span class="text-danger">{{ $invoices[$detail_order->status] }}</span>
                                @else
                                    <span class="text-default">{{ $invoices[$detail_order->status] }}</span>
                                @endif
                            </td>
                            <td class="button-action">
                                <a href="{{route('admin.invoices.edit', $detail_order->id )}}" data-id="{{ $detail_order->id }}" class="btn btn-warning text-white detail_invoice"><i class="fas fa-edit"></i></a>
                                <button class="btn btn-danger btn-delete-invoices" data-id="{{$detail_order->id}}"><i class="fas fa-trash-alt"></i></button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot class="card-footer clearfix">
                    <td colspan="10" class="text-center">
                        {{ $detail_orders->links()  }}
                    </td>
                </tfoot>
            </table>
        </div>
    </div>
    <!-- <div class="container" id="group_button">
        <div class="table-responsive" style="display: block;">
            <button class="btn btn-success btn-action-invoice" data-type="mark_paid">Thanh toán</button>
            <button class="btn btn-outline-warning btn-action-invoice" data-type="unpaid">Chưa thanh toán</button>
            <button class="btn btn-outline-secondary btn-action-invoice" data-type="cancel">Hủy</button>
            <button class="btn btn-danger btn-action-invoice" data-type="delete">Xóa</button>
        </div>
    </div> -->
</div>
{{-- Modal xoa order --}}
<div class="modal fade" id="delete-order">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-order" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-order" value="Xóa đơn hàng">
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
{{-- Modal xoa invoice --}}
<div class="modal fade" id="delete-invoice">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-invoice" value="Xóa đơn hàng">
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
{{-- Modal xoa invoice --}}
<div class="modal fade" id="detail_invoice">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Chi tiết đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-detail">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default float-right" data-dismiss="modal">Tắt</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
    <input type="hidden" id="page" value="list_detail_order">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('js/invoices.js') }}"></script>
@endsection
