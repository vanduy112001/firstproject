@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Quản lý hóa đơn
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item "><a href="/admin/invoices/">Hóa đơn</a></li>
    <li class="breadcrumb-item active">Chỉnh sửa hóa đơn</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                        <div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper dt-bootstrap">
            <h4>Hóa đơn số #{{$detail_order->id}}</h4>
            <div class="container">
                <div class="row">
                    <div class="col col-md-6">
                        <form action="" method="post">
                            <div class="row order">
                                {{-- 1 form group --}}
                                <div class="group-invoice">
                                    <div class="col col-md-4 text-right">
                                        <label for="">Tên khách hàng :</label>
                                    </div>
                                    <div class="col col-md-8">
                                        <a href="">{{$detail_order->user->name}}</a>
                                    </div>
                                </div>
                                {{-- 1 form group --}}
                                <div class="group-invoice">
                                    <div class="col col-md-4 text-right">
                                        <label for="">Loại :</label>
                                    </div>
                                    <div class="col col-md-8">
                                        @if($detail_order->type == 'addon_vps')
                                          Addon VPS
                                        @elseif($detail_order->order->type == 'expired')
                                          Gia hạn
                                        @else
                                          Tạo
                                        @endif
                                    </div>
                                </div>
                                {{-- 1 form group --}}
                                <div class="group-invoice">
                                    <div class="col col-md-4 text-right">
                                        <label for="">Ngày hết hạn :</label>
                                    </div>
                                    <div class="col col-md-8">
                                        {{date('d/m/Y', strtotime($detail_order->due_date))}}
                                    </div>
                                </div>
                                {{-- 1 form group --}}
                                <div class="group-invoice">
                                    <div class="col col-md-4 text-right">
                                        <label for="">Trạng thái :</label>
                                    </div>
                                    <div class="col col-md-8">
                                        @if(!empty($detail_order->paid_date))
                                            <span class="text-success">Đã thanh toán ( Ngày: {{date('d/m/Y', strtotime($detail_order->paid_date))}})</span>
                                        @else
                                            <span class="text-danger">Chưa thanh toán</span>
                                        @endif
                                    </div>
                                </div>
                                {{-- 1 form group --}}
                                <div class="group-invoice">
                                    <div class="col col-md-4 text-right">
                                        <label for="">Thành tiền :</label>
                                    </div>
                                    <div class="col col-md-8">
                                        {!!number_format($detail_order->order->total,0,",",".") . ' VNĐ'!!}
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col col-md-6">
                        <div class="order">
                            <div class="detail_order text-center">
                                <div class="paid">
                                    @if($detail_order->status == 'paid')
                                        <span class="text-success">
                                            {{$invoices[$detail_order->status]}}
                                        </span>
                                    @elseif($detail_order->status == 'unpaid')
                                        <span class="text-danger">
                                            {{$invoices[$detail_order->status]}}
                                        </span>
                                    @else
                                        <span class="text-default">
                                            {{$invoices[$detail_order->status]}}
                                        </span>
                                    @endif
                                </div>
                                <div class="date_paid">
                                    @if(!empty($detail_order->paid_date))
                                        <b class="text-success">{{date('d/m/Y', strtotime($detail_order->paid_date))}}</b>
                                    @else
                                        <b class="text-danger">Chưa thanh toán</b>
                                    @endif
                                </div>
                                <div class="date_due">
                                    Ngày đặt hàng: <b>{{date('d/m/Y', strtotime($detail_order->created_at))}}</b>
                                </div>
                                <div class="date_due">
                                    Ngày hết hạn: <b>{{date('d/m/Y', strtotime($detail_order->due_date))}}</b>
                                </div>
                                <!-- <div class="button-invoice-action">
                                    <a href="{{route('admin.invoices.paid', $detail_order->id)}}?action=mark_paid" class="btn btn-outline-success  @if($detail_order->status == 'paid') disabled @endif">Thanh toán</a>
                                    <a href="{{route('admin.invoices.paid', $detail_order->id)}}?action=unpaid" class="btn btn-outline-danger @if($detail_order->status == 'unpain') disabled  @endif">Chưa thanh toán</a>
                                    <a href="{{route('admin.invoices.paid', $detail_order->id)}}?action=cancel" class="btn btn-outline-secondary @if($detail_order->status == 'cancel') disabled @endif">Hủy</a>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <h4>Thông tin dịch vụ / sản phẩm</h4>
                <div class="container">
                    <div class="col col-md-12">
                        <div class="order">
                            @if($detail_order->order->type == 'expired')
                              <table class="table table-hover table-bordered">
                                  <thead class="primary">
                                      <th>Dịch vụ</th>
                                      <th>Cấu hình</th>
                                      <th>Ngày đặt hàng</th>
                                      <th>Thời gian</th>
                                      <th>Giá</th>
                                  </thead>
                                  <tbody>
                                      @if($detail_order->type == 'VPS' || $detail_order->type == 'NAT-VPS' || $detail_order->type == 'VPS US') )
                                          @php
                                              $order_expireds = $detail_order->order_expireds;
                                          @endphp
                                          @foreach ($order_expireds as $order_expired)
                                            <tr>
                                                <td>
                                                    Gia hạn VPS - <a href="{{ route('admin.vps.detail', $order_expired->vps->id) }}">{{$order_expired->vps->ip}}</a>
                                                </td>
                                                <td>
                                                  @php
                                                      $product = $order_expired->vps->product;
                                                      if(!empty($product->meta_product)) {
                                                        $cpu = $product->meta_product->cpu;
                                                        $ram = $product->meta_product->memory;
                                                        $disk = $product->meta_product->disk;
                                                      } else {
                                                        $cpu = 1;
                                                        $ram = 1;
                                                        $disk = 20;
                                                      }
                                                      // Addon
                                                      if (!empty($vps->vps_config)) {
                                                          $vps_config = $vps->vps_config;
                                                          if (!empty($vps_config)) {

                                                              $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                                                              $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                                                              $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                                                          }
                                                      }
                                                  @endphp
                                                  {{ $cpu }} - {{ $ram }} - {{ $disk }}
                                                </td>
                                                <td>{{ date('d-m-Y' , strtotime($order_expired->created_at)) }}</td>
                                                <td>{{ !empty($order_expired->billing_cycle) ? $billings[$order_expired->billing_cycle] : '1 Tháng' }}</td>
                                                <td>{!!number_format($detail_order->sub_total,0,",",".") . ' VNĐ'!!}</td>
                                            </tr>
                                          @endforeach
                                      @elseif($detail_order->type == 'Server')
                                          @php
                                              $order_expireds = $detail_order->order_expireds;
                                          @endphp
                                          @foreach ($order_expireds as $order_expired)
                                            <tr>
                                                <td>
                                                    Gia hạn Server - <a href="{{ route('admin.server.detail', $order_expired->server->id) }}">{{$order_expired->server->ip}}</a>
                                                </td>
                                                <td>{{ date('d-m-Y' , strtotime($order_expired->created_at)) }}</td>
                                                <td>{{ !empty($order_expired->billing_cycle) ? $billings[$order_expired->billing_cycle] : '1 Tháng' }}</td>
                                                <td>{!!number_format($detail_order->sub_total,0,",",".") . ' VNĐ'!!}</td>
                                            </tr>
                                          @endforeach
                                      @else
                                          @php
                                              $order_expireds = $detail_order->order_expireds;
                                          @endphp
                                          @foreach ($order_expireds as $order_expired)
                                            <tr>
                                                <td>
                                                  Gia hạn Hosting - <a href="{{ route('admin.hosting.detail', $order_expired->hosting->id) }}">{{$order_expired->hosting->domain}}</a>
                                                </td>
                                                <td></td>
                                                <td>{{ date('d-m-Y' , strtotime($order_expired->created_at)) }}</td>
                                                <td>{{ !empty($order_expired->billing_cycle) ? $billings[$order_expired->billing_cycle] : '1 Tháng' }}</td>
                                                <td>{!!number_format($detail_order->sub_total,0,",",".") . ' VNĐ'!!}</td>
                                          </tr>
                                          @endforeach
                                      @endif
                                  </tbody>
                              </table>
                            @elseif($detail_order->type == 'addon_vps')
                                <table class="table table-hover table-bordered">
                                    <thead class="primary">
                                        <th>IP</th>
                                        <th>CPU</th>
                                        <th>RAM</th>
                                        <th>DISK</th>
                                        <th>Ngày đặt hàng</th>
                                        <th>Thời gian</th>
                                        <th>Giá</th>
                                    </thead>
                                    <tbody>
                                        @php
                                            $order_addon_vps = $detail_order->order_addon_vps;
                                            $add_on_products = GroupProduct::get_addon_product_private($detail_order->user_id);
                                            $billing_cycle = 'monthly';
                                        @endphp
                                        @foreach($order_addon_vps as $addon_vps)
                                            <td>
                                                Addon VPS - <a href="{{ route('admin.vps.detail', $addon_vps->vps->id) }}">{{$addon_vps->vps->ip}}</a>
                                            </td>
                                            <td>
                                                {{ $addon_vps->cpu }}
                                            </td>
                                            <td>
                                                {{ $addon_vps->ram }}
                                            </td>
                                            <td>
                                                {{ $addon_vps->disk }}
                                            </td>
                                            <td>{{ date('d-m-Y' , strtotime($addon_vps->created_at)) }}</td>
                                            <td>
                                                {{ !empty($addon_vps->month) ? $addon_vps->month . ' Tháng' : '' }} {{ !empty($addon_vps->day) ? $addon_vps->day .' Ngày' : '' }}
                                            </td>
                                            <td>
                                                <?php
                                                    $pricing_addon = 0;
                                                    foreach ($add_on_products as $key => $add_on_product) {
                                                        if (!empty($add_on_product->meta_product->type_addon)) {
                                                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                              $pricing_addon += $addon_vps->month * $addon_vps->cpu * $add_on_product->pricing[$billing_cycle] + (int) round($addon_vps->day  * $addon_vps->cpu * $add_on_product->pricing[$billing_cycle] / 30, -3);
                                                            }
                                                        }
                                                    }
                                                    foreach ($add_on_products as $key => $add_on_product) {
                                                        if (!empty($add_on_product->meta_product->type_addon)) {
                                                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                              $pricing_addon += $addon_vps->month * $addon_vps->ram * $add_on_product->pricing[$billing_cycle] + (int) round($addon_vps->day  * $addon_vps->ram * $add_on_product->pricing[$billing_cycle] / 30, -3);
                                                            }
                                                        }
                                                    }
                                                    foreach ($add_on_products as $key => $add_on_product) {
                                                        if (!empty($add_on_product->meta_product->type_addon)) {
                                                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                              $pricing_addon += $addon_vps->month * $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10 + (int) round($addon_vps->day  * $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 30, -3)/10;
                                                            }
                                                        }
                                                    }
                                                ?>
                                                {!!number_format( $pricing_addon,0,",",".") . ' VNĐ'!!}
                                            </td>
                                        @endforeach
                                    </tbody>
                                </table>
                            @else
                                <table class="table table-hover table-bordered">
                                    <thead class="primary">
                                        <th>Dịch vụ</th>
                                        <td>Cấu hình</td>
                                        <th>Ngày bắt đầu</th>
                                        <th>Ngày kết thúc</th>
                                        <th>Giá</th>
                                    </thead>
                                    <tbody>
                                        @if( $detail_order->type == 'VPS' || $detail_order->type == 'NAT-VPS' )
                                            @foreach ($detail_order->vps as $vps)
                                            <tr>
                                                <td>{{ $detail_order->type }} - {{$vps->ip}}</td>
                                                <td>
                                                    @php
                                                        $product = $vps->product;
                                                        if(!empty($product->meta_product)) {
                                                          $cpu = $product->meta_product->cpu;
                                                          $ram = $product->meta_product->memory;
                                                          $disk = $product->meta_product->disk;
                                                        } else {
                                                          $cpu = 1;
                                                          $ram = 1;
                                                          $disk = 20;
                                                        }
                                                        // Addon
                                                        if (!empty($vps->vps_config)) {
                                                            $vps_config = $vps->vps_config;
                                                            if (!empty($vps_config)) {

                                                                $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                                                                $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                                                                $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                                                            }
                                                        }
                                                    @endphp
                                                    {{ $cpu }} - {{ $ram }} - {{ $disk }}
                                                </td>
                                                <td>{{ date('d-m-Y' , strtotime($vps->created_at)) }}</td>
                                                <td>{{ !empty($vps->next_due_date) ? date('d-m-Y' , strtotime($vps->next_due_date)) : '#' }}</td>
                                                <td>{!!number_format($detail_order->sub_total ,0,",",".") . ' VNĐ'!!}</td>
                                          </tr>
                                            @endforeach
                                        @elseif($detail_order->type == 'Server')
                                            @foreach ($detail_order->servers as $server)
                                                <tr>
                                                    <td>{{ $detail_order->type }} - {{$server->ip}}</td>
                                                    <td>{{ date('d-m-Y' , strtotime($server->created_at)) }}</td>
                                                    <td>{{ !empty($server->next_due_date) ? date('d-m-Y' , strtotime($server->next_due_date)) : '#' }}</td>
                                                    <td>{!!number_format($detail_order->sub_total ,0,",",".") . ' VNĐ'!!}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            @foreach ($detail_order->hostings as $hosting)
                                                <tr>
                                                    <td>{{ $detail_order->type }} - {{$hosting->domain}}</td>
                                                    <td>{{ date('d-m-Y' , strtotime($hosting->created_at)) }}</td>
                                                    <td>{{ !empty($hosting->next_due_date) ? date('d-m-Y' , strtotime($hosting->next_due_date)) : '#' }}</td>
                                                    <td>{!!number_format($detail_order->sub_total,0,",",".") . ' VNĐ'!!}</td>
                                                </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Modal xoa order --}}
<div class="modal fade" id="delete-order">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-order" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-order" value="Xóa đơn hàng">
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
@endsection
@section('scripts')
<script src="{{ asset('js/edit_invoices.js') }}"></script>
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>

<script type="text/javascript">
	$(document).ready(function() {
		//Date picker
		$('#datepicker').datepicker({
			autoclose: true
		});
	});
</script>
@endsection
