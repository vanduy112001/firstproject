@extends('layouts.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Danh sách Hosting
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Hosting</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
              <button type="button" name="button" class="btn btn-info" id="hosting_type_user">Khách hàng cá nhân</button>
              <button type="button" name="button" class="btn btn-danger" id="hosting_type_enterprise">Khách hàng doanh nghiệp</button>
            </div>
            <div class="col-md-4">
              <div class="row">
                  <select class="form-control" id="select_status_hosting">
                    <option value="" disabled selected>Chọn trạng thái của VPS</option>
                    <option value="on">Trạng thái đang bật</option>
                    <option value="off">Trạng thái đã tắt</option>
                    <option value="expire">Trạng thái hết hạn</option>
                    <option value="admin_off">Trạng thái Admin tắt</option>
                    <option value="suspend">Trạng thái đã khóa</option>
                    <option value="delete_hosting">Trạng thái đã xóa</option>
                    <option value="cancel">Trạng thái đã hủy</option>
                  </select>
              </div>
            </div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12">
                      <div class="input-group">
                        <input type="text" name="search" class="form-control" placeholder="Tìm kiếm hosting hoặc khách hàng" id="search">
                        <div class="input-group-append">
                          <button type="submit" name="submit" class="btn btn-danger"><i class="fas fa-search"></i>
                          </button>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4 mt-4 text-primary text-center">
                <b>Hosting {{ $hostings->firstItem() }} - {{ $hostings->lastItem() }} / {{ $hostings->total() }}</b>
            </div>
            <div class="col-md-4"></div>
            <div class="col-md-4 mt-4 link-right paginate_top">
                {{ $hostings->links()  }}
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="button-header">
  <button class="btn btn-sm btn-secondary button-multiple-action-hostings" data-action="cancel">Hủy</button>
  <button class="btn btn-sm btn-primary text-default btn-add-contract" product-type="hosting">Tạo hợp đồng</button>
  <button class="btn btn-sm btn-danger button-multiple-action-hostings" data-action="delete"><i class="fas fa-trash-alt"></i> Xóa</button>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-bordered"  id="table_vps">
                <thead class="primary">
                    <th></th>
                    <th>Tên khách hàng</th>
                    <th>Domain</th>
                    <th>Tên sản phẩm</th>
                    <th>Ngày tạo</th>
                    <th class="sort_next_due_date" data-sort="ASC" data-toggle="tooltip" data-placement="top" title="Sắp xếp ngày kết thúc">
                      Ngày kết thúc <i class="fas fa-sort"></i>
                      <input type="hidden" class="sort_type" value="">
                    </th>
                    <th>Thành tiền</th>
                    <th>Trạng thái</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                    @foreach ($hostings as $hosting)
                        <tr>
                            <td><input type="checkbox" value="{{ $hosting->id }}" class="hosting_checkbox"></td>
                            <td><a href="{{ route('admin.user.detail', $hosting->user_hosting->id ) }}">{{ $hosting->user_hosting->name }}</a></td>
                            <td>
                                @if (!empty($hosting->domain))
                                    <span>
                                      <a href="{{ route('admin.hosting.detail', $hosting->id) }}">{{$hosting->domain}}</a>
                                    </span>
                                @else
                                    <span class="text-danger">Chưa tạo</span>
                                @endif
                            </td>
                            <td>
                                @if(!empty($hosting->product))
                                    @if(!empty($hosting->product->group_product->private))
                                        <a href="{{ route('admin.product_private.editPrivate' , [$hosting->product->group_product->id, $hosting->product->id]) }}">{{ $hosting->product->name }}</a>
                                    @else
                                      <a href="{{ route('admin.product.edit' , $hosting->product->id) }}">{{ $hosting->product->name }}</a>
                                    @endif
                                @endif
                            </td>
                            <td class="data-create">
                                @if (!empty($hosting->date_create))
                                    <span>{{ date('H:i:m d-m-Y', strtotime($hosting->created_at)) }}</span>
                                @else
                                    <span class="text-danger">Chưa tạo</span>
                                @endif
                            </td>
                            <td>
                                @if (!empty($hosting->next_due_date))
                                    <span>{{ date('d-m-Y', strtotime($hosting->next_due_date)) }}</span>
                                @else
                                    <span class="text-danger">Chưa tạo</span>
                                @endif
                            </td>
                            <td>
                                <?php
                                   $price_hosting = 0;
                                   if ( !empty($hosting->price_override) ) {
                                      $price_hosting = $hosting->price_override;
                                      if (!empty($hosting->order_upgrade_hosting)) {
                                        foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                                           $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                                        }
                                      }
                                   } else {
                                      $price_hosting = !empty($hosting->detail_order->sub_total) ? $hosting->detail_order->sub_total : 0;
                                      if (!empty($hosting->order_upgrade_hosting)) {
                                        foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                                           $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                                        }
                                      }
                                   }
                                ?>
                                {!!number_format( $price_hosting ,0,",",".")!!} VNĐ
                            </td>
                            <td class="hosting-status">
                                @if ($hosting->status_hosting == 'on')
                                    <span class="text-success">Đang bật</span>
                                @elseif ($hosting->status_hosting == 'off')
                                    <span class="text-success">Đã tắt</span>
                                @elseif ($hosting->status_hosting == 'expire')
                                    <span class="text-danger">Hết hạn</span>
                                @elseif ($hosting->status_hosting == 'admin_off')
                                    <span class="text-danger">Admin tắt</span>
                                @elseif ($hosting->status_hosting == 'cancel')
                                    <span class="text-danger">Đã hủy</span>
                                @elseif ($hosting->status_hosting == 'delete')
                                    <span class="text-danger">Đã xóa</span>
                                @else
                                    <span class="text-danger">Chưa tạo</span>
                                @endif
                            </td>
                            <td class="button-action-services">
                                <a href="{{ route('admin.hosting.detail', $hosting->id) }}" class="btn btn-warning text-white" data-toggle="tooltip" title="Chỉnh sửa thông tin Hosting"><i class="fas fa-edit"></i></a>
                                <button type="button" class="btn btn-outline-warning button-action-hosting" @if($hosting->status_hosting == 'off') disabled @endif data-id="{{$hosting->id}}" data-type='suspend' data-domain="{{$hosting->domain}}" data-toggle="tooltip" title="Suspend Hosting"><i class="fas fa-power-off"></i></button>
                                <button type="button" class="btn btn-outline-info button-action-hosting"  @if($hosting->status_hosting == 'on') disabled @endif data-id="{{$hosting->id}}" data-type='unsuspend' data-domain="{{$hosting->domain}}" data-toggle="tooltip" title="Unsuspend Hosting"><i class="fas fa-toggle-on"></i></button>
                                <button class="btn btn-danger button-action-hosting" data-id="{{$hosting->id}}" data-type='delete' data-domain="{{$hosting->domain}}" data-toggle="tooltip" title="Xóa hosting"><i class="fas fa-trash-alt"></i></button>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot class="card-footer clearfix">
                    <td colspan="9" class="text-center link-right">
                        {{ $hostings->links()  }}
                    </td>
                </tfoot>
            </table>
        </div>
    </div>
    <div id="group_button">
        <div class="table-responsive" style="display: block;">
            <!-- <button class="btn btn-outline-success button-multiple-action-hostings" data-action="create">Tạo Hosting</button> -->
            <!-- <button class="btn btn-outline-warning button-multiple-action-hostings" data-action="suspend">Suspend Hosting</button> -->
            <!-- <button class="btn btn-outline-info button-multiple-action-hostings" data-action="unsuspend">UnSuspend Hosting</button> -->
            <button class="btn btn-outline-secondary button-multiple-action-hostings" data-action="cancel">Hủy</button>
            <button class="btn btn-danger button-multiple-action-hostings" data-action="delete"><i class="fas fa-trash-alt"></i> Xóa</button>
        </div>
    </div>
</div>
{{-- Modal xoa vps --}}
<div class="modal fade" id="delete-vps">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button-submit-hosting">Xác nhận</button>
          <button type="button" class="btn btn-success hidden" id="button-finish-hosting" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
{{-- Modal xoa nhiều hosting --}}
<div class="modal fade" id="delete-multiple-hosting">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-hostings" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button-submit-multi-hosting">Xác nhận</button>
          <button type="button" class="btn btn-success hidden" id="button-finish-multi-hosting" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
  <input type="hidden" id="type_user_personal" value="">
<input type="hidden" id="page" value="list_hosting">
@endsection
@section('scripts')
<script src="{{ asset('/libraries/bootstrap/js/bootstrap.bundle.min.js') }}" defer></script>
<script src="{{ asset('/libraries/sweetalert2/sweetalert2.min.js') }}" defer></script>
<script src="{{ asset('/libraries/toastr/toastr.min.js') }}" defer></script>
<script src="{{ asset('libraries/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('libraries/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('js/admin_hosting.js') }}"></script>
<script src="{{ asset('js/admin_contract.js') }}"></script>
@endsection
