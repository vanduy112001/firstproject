@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-invoice nav-icon "></i> Danh sách khuyến mãi
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Khuyến mãi</li>
@endsection
@section('content')
<div class="title">
    <div class="title-body">
        <div class="row">
            <!-- button tạo -->
            <div class="col-md-4">
                <button  id="create_event" class="btn btn-danger">Thêm khuyến mãi</button>
            </div>
            <div class="col-md-4"></div>
            <!-- form search -->
            <div class="col-md-4">
                <div class="row">
                    <div class="form-group col-md-12" id="select_product_form">
                      <select class="select2 form-control" id="select_product" style="width:100%;">
                        <option value="" selected>---Chọn sản phẩm---</option>
                        @foreach($products as $product)
                            <option value="{{$product->id}}">{{$product->name}} - {{$product->group_product->name}}</option>
                        @endforeach
                      </select>
                    </div>
                </div>
            </div>
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="dataTables_wrapper form-inline dt-bootstrap">
            <table class="table table-bordered">
                <thead class="primary">
                    <th>Tên sản phẩm</th>
                    <th>Thời gian</th>
                    <th>Điểm cộng</th>
                    <th>Hành động</th>
                </thead>
                <tbody>
                  @foreach($events as $event)
                  <tr>
                    <td>
                      @if ( !empty($event->product) )
                        <a href="{{ $event->product->id }}">{{ $event->product->name }}</a>
                      @endif
                    </td>
                    <td> {{ $billings[$event->billing_cycle] }} </td>
                    <td>{{ $event->point }}</td>
                    <td>
                      <button type="button" class="btn btn-warning text-light edit_event mr-2" data-toggle="tooltip" data-id="{{ $event->id }}" data-placement="top" title="Chỉnh sửa khuyến mãi"><i class="far fa-edit"></i></button>
                      <button type="button" class="btn btn-danger delete_event"  data-id="{{ $event->id }}" data-toggle="tooltip" data-placement="top" title="Xóa khuyến mãi"><i class="far fa-trash-alt"></i></button>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
                <tfoot class="card-footer clearfix" id="event_pagination">
                </tfoot>
            </table>
        </div>
    </div>
</div>
{{-- Modal xoa vps --}}
<div class="modal fade" id="delete_vps">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication_invoice" class="text-center">

            </div>
            <form id="form_group_event" action="" method="post">
                @csrf
                <div class="form-group">
                    <label for="product">Chọn sản phẩm</label>
                    <select class="select2 form-control" id="product" name="product_id" style="width:100%;">
                          <option value="" selected disabled>---Chọn sản phẩm---</option>
                        @foreach($products as $product)
                          <option value="{{$product->id}}">{{$product->name}} - {{$product->group_product->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="billing">Chọn thời gian nhận thưởng</label>
                    <select class="form-control" id="billing" name="billing_cycle">
                        <option value="">Chọn thời gian nhận thưởng</option>
                        @foreach($billings as $key => $billing)
                          <option value="{{$key}}">{{ $billing }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="point">Điểm thưởng</label>
                    <input type="text" name="point" id="point" class="form-control" value="">
                </div>
                <div class="hidden">
                    <input type="hidden" name="action" value="create" id="create">
                    <input type="hidden" name="event_id" value="" id="event_id">
                </div>
            </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button-submit">Xác nhận</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
{{-- Modal xoa vps --}}
<div class="modal fade" id="delete_tutorial">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa khuyến mãi</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication_tutorial" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button_delete">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button_finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
<input type="hidden" id="page" value="list_event">
@endsection
@section('scripts')
<script src="{{ asset('/libraries/bootstrap/js/bootstrap.bundle.min.js') }}" defer></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('/libraries/sweetalert2/sweetalert2.min.js') }}" defer></script>
<script src="{{ asset('/libraries/toastr/toastr.min.js') }}" defer></script>
<script src="{{ asset('js/admin_event.js') }}"></script>
@endsection
