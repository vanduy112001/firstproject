@extends('layouts.app_login')
@section('content')
<div class="container">
    <div class="row">
        <div class="login-box">
            <div class="card card-primary">
                <div class="card-header">
                    <div class="login-logo">
                        <a href="/" alt="Cloudzone"><img src="/images/logo.png" alt="Cloudzone"></a>
                    </div>
                </div>
                <form action="{{ route('admin.checklogin') }}" method="post" role="form" id="quickForm">
                    <div class="card-body">
                        @if(session("fails"))
			                <div class="error-login text-center">
				                <p class="maudo">{{session("fails")}}</p>
			                </div>
                        @endif
                        @csrf
                        <div class="form-group has-feedback">
                            <label for="InputEmail">Email address</label>
                            <input type="email" id="InputEmail" class="form-control @error('email') is-invalid @enderror" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="InputPassword">Password</label>
                            <input id="password" id="InputPassword" type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Mật khẩu" required>
                            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        </div>
                        <div class="float-right">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Đăng nhập</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection