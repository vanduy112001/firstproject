@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-chart-line"></i> Quản lý tài khoản API
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Tài khoản API</li>
@endsection
@section('content')
<div class="col-md-12 detail-course-finish">
    @if(session("success"))
    <div class="bg-success">
        <p class="text-light">{{session("success")}}</p>
    </div>
    @elseif(session("fails"))
    <div class="bg-danger">
        <p class="text-light">{{session("fails")}}</p>
    </div>
    @endif
    <div id="notication">

    </div>
</div>
<div class="container-fluid">
  <!-- Timelime example  -->
  <div class="row">
      <div class="col-md-12">
          <div class="m-4">
            <button type="button" class="btn btn-primary create_agency" name="button">Tạo tài khoản API</button>
          </div>
          <div class="card">
             <div class="card-body table-responsive p-0">
               <table class="table table-hover">
                  <thead>
                    <th>Khách hàng</th>
                    <th>Tài khoản api</th>
                    <th>Mật khẩu</th>
                    <th>Hành động</th>
                  </thead>
                  <tbody>
                  </tbody>
               </table>
             </div>
          </div>
      </div>
  </div>
</div>
{{-- Modal --}}
<div class="modal fade" id="modal-product">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tạo tài khoản đại API</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication_vld">

            </div>
            <form id="form-group-product">
                @csrf
                <div class="form-group">
                    <label for="type">Khách hàng</label>
                    <select class="form-control select2" name="user_id" id="user" style="width:100%;">
                        <option value="" selected disabled>Chọn khách hàng</option>
                        @foreach($users as $user)
                          <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="user_api">Tài khoản</label>
                    <input type="text" id="user_api" name="user_api" required class="form-control" placeholder="Tài khoản API">
                </div>
                <div class="form-group">
                    <label for="password">Mật khẩu</label>
                    <input type="text" id="password" id="password" name="password" required class="form-control" placeholder="Mật khẩu tài khoản API">
                </div>
                <div class="hidden">
                    <input type="text" name="id" id="agency_id" value="">
                    <input type="text" id="action" name="action" value="create">
                </div>
            </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-submit" value="Tạo tài khoản API">
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
{{-- Modal xoa product --}}
  <div class="modal fade" id="delete-product">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Xóa sản phẩm</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div id="notication-product">

              </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
            <input type="submit" class="btn btn-primary" id="button-product" value="Xóa sản phẩm">
          </div>
        </div>
        <!-- /.modal-content -->
    </div>
      <!-- /.modal-dialog -->
</div>
    <!-- /.modal -->
<input type="hidden" id="page" value="list_agency">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('/js/agency_api.js') }}"></script>
@endsection
