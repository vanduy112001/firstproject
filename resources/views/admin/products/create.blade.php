@extends('layouts.app')
@section('title')
<i class="nav-icon fas fa-cubes"></i> Quản lý thông tin sản phẩm
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="/admin">Home</a></li>
<li class="breadcrumb-item"><a href="{{ route('admin.product.index') }}">Sản phẩm</a></li>
<li class="breadcrumb-item active">Tạo Sản phẩm</li>
@endsection
@section('content')
<div class="title">
  <div class="title-body">
    <div class="row">
      <!-- button tạo -->
      <div class="col-md-4">
        <h3>Tạo sản phẩm</h3>
      </div>
      <div class="col-md-4"></div>
      <!-- form search -->
      <div class="col-md-4">
        <div class="row">
          <div class="form-group col-md-12">
            <div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="box box-primary">
  <div class="box-body table-responsive">
    <div class="dataTables_wrapper dt-bootstrap">
      <form action="{{ route('admin.product.store') }}" method="post">
        @csrf
        <div>
        </div>
        <div class="form-group">
          <label for="">Tên sản phẩm</label>
          <input type="text" name="name" value="{{ old('name') }}" class="form-control" placeholder="Tên sản phẩm">
          @if($errors->has('name'))
          <p class="text-danger">{{$errors->first('name')}}</p>
          @endif
        </div>
        <div class="form-group">
          <label for="">Nhóm sản phẩm</label>
          <select name="group_product_id" class="form-control">
            @foreach ($group_products as $group_product)
            @php
            $selected = '';
            if ($group_product->id  == old('group_product_id') ) {
              $selected = 'selected';
            }
            @endphp
            <option value="{{ $group_product->id }}" {{ $selected }}>{{ $group_product->name }} #{{ $group_product->id }}</option>
            @endforeach
          </select>
          @if($errors->has('group_product_id'))
          <p class="text-danger">{{$errors->first('group_product_id')}}</p>
          @endif
        </div>
        <div class="form-group">
          <label for="">Loại sản phẩm</label>
          <select name="type_product" id="type_product" class="form-control">
            @foreach ($type_products as $type_product)
            @php
            $selected = '';
            if ($type_product  == old('type_product') ) {
              $selected = 'selected';
            }
            @endphp
            <option value="{{ $type_product }}">{{ $type_product }}</option>
            @endforeach
          </select>
          @if($errors->has('type_product'))
          <p class="text-danger">{{ $errors->first('type_product') }}</p>
          @endif
        </div>
        <div class="form-group">
          <label for="">Mô tả</label>
          <textarea name="description" class="form-control" rows="4" cols="40" placeholder="Mô tả">{{ old('description') }}</textarea>
        </div>
        <div class="form-group">
          <label for="">Package</label>
          <div id="package">

          </div>
        </div>
        <div class="form-group">
          <label for="">Số thứ tự</label>
          <input type="text" class="form-control" name="stt" value="{{ old('stt') }}">
        </div>
        <div class="form-group">
          <label for="">Ẩn sản phẩm: </label>
          <input type="checkbox" value="1" name="hidden">
        </div>
        <div class="text-center">
          <input type="submit" value="Tạo sản phẩm" class="btn btn-primary">
        </div>
      </form>
    </div>
  </div>
</div>
<input type="hidden" id="page" value="create_product">

@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('js/admin_create_product.js') }}"></script>
@endsection
