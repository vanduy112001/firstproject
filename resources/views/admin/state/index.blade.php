@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-chart-line"></i> Quản lý Bang của VPS US
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Quản lý Bang của VPS US</li>
@endsection
@section('content')
<div class="col-md-12 detail-course-finish">
    @if(session("success"))
    <div class="bg-success">
        <p class="text-light">{{session("success")}}</p>
    </div>
    @elseif(session("fails"))
    <div class="bg-danger">
        <p class="text-light">{{session("fails")}}</p>
    </div>
    @endif
    <div id="notication">

    </div>
</div>
<div class="container-fluid">
  <!-- Timelime example  -->
  <div class="row">
      <div class="col-md-12">
          <div class="m-4">
            <button type="button" class="btn btn-primary create_state" name="button">Thêm Bang</button>
          </div>
          <div class="card">
             <div class="card-body table-responsive p-0">
               <table class="table table-hover">
                  <thead>
                    <th>Bang</th>
                    <th>ID Bang</th>
                    <th>Trạng thái</th>
                    <th>Hành động</th>
                  </thead>
                  <tbody>
                  </tbody>
               </table>
             </div>
          </div>
      </div>
  </div>
</div>
{{-- Modal --}}
<div class="modal fade" id="modal-product">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Tạo Bang</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication_vld">

            </div>
            <form id="form-group-product">
                @csrf
                <div class="form-group">
                    <label for="type">Bang</label>
                    <input type="text" id="name" name="name" required class="form-control" placeholder="Tên Bang">
                </div>
                <div class="form-group">
                    <label for="user_api">ID Bang</label>
                    <input type="text" id="id_state" name="id_state" required class="form-control" placeholder="ID của Bang">
                </div>
                <div class="form-group row">
                    <div class="col-md-4">
                        <label>Ẩn nhóm sản phẩm</label>
                    </div>
                    <div class="col-md-8 icheck-primary d-inline">
                        <input type="checkbox" class="custom-control-input" name="hidden" id="hidden" value="1">
                        <label for="hidden"></label>
                    </div>
                </div>
                <div class="hidden">
                    <input type="text" name="id" id="id" value="">
                    <input type="text" name="location" id="location" value="us">
                    <input type="text" id="action" name="action" value="create">
                </div>
            </form>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <input type="submit" class="btn btn-primary" id="button-submit" value="Xác nhận">
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
{{-- Modal xoa product --}}
  <div class="modal fade" id="delete-product">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Xóa sản phẩm</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div id="notication-product">

              </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <input type="submit" class="btn btn-primary" id="button-product" value="Xác nhận">
          </div>
        </div>
        <!-- /.modal-content -->
    </div>
      <!-- /.modal-dialog -->
</div>
    <!-- /.modal -->
<input type="hidden" id="page" value="list_state">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('/js/admin_state.js') }}"></script>
@endsection