@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-alt"></i> Hợp đồng {{ $contract->contract_number }}
@endsection
@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
  <li class="breadcrumb-item active"><a href="{{ route('admin.contract.index') }}">Danh sách hợp đồng</a></li>
  <li class="breadcrumb-item active">Hợp đồng {{ $contract->contract_number }}</li>
@endsection
@section('content')

@if(session("success"))
<div class="alert alert-success alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("success")}}</span>
</div>
@elseif(session("fail"))
<div class="alert alert-danger alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("fail")}}</span>
</div>
@elseif(session("error"))
<div class="alert alert-danger alert-dismissible" style="margin-top:20px">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="text-light">{{session("error")}}</span>
</div>
@endif

<div class="col col-md-12">
    <h4 class="mb-2">Thông tin hợp đồng</h4>
    <hr class="text-secondary">
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <form action="{{ route( 'admin.contract.update', [ 'id' => $contract->id ] ) }}" method="post">
            @csrf
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="title">Tiêu đề hợp đồng</label>
                        <input type="text" id="title" name="title" value="{{ $contract->title }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="contract_type">Loại hợp đồng</label>
                        <select class="form-control select2" name="contract_type" id="contract_type" required>
                            <option value="VPS" {{ ($contract->contract_type === 'VPS') ? 'selected' : '' }}>VPS</option>
                            <option value="HOST" {{ ($contract->contract_type === 'HOST') ? 'selected' : '' }}>Hosting</option>
                            <option value="EHOST" {{ ($contract->contract_type === 'EHOST') ? 'selected' : '' }}>Email Hosting</option>
                            <option value="COLO" {{ ($contract->contract_type === 'COLO') ? 'selected' : '' }}>Colocation</option>
                            <option value="SV" {{ ($contract->contract_type === 'SV') ? 'selected' : '' }}>Server</option>
                            <option value="DM" {{ ($contract->contract_type === 'DM') ? 'selected' : '' }}>Domain</option>
                            <option value="TKW" {{ ($contract->contract_type === 'TKW') ? 'selected' : '' }}>Thiết kế web</option>
                        </select>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <label for="contract_number">Số hợp đồng</label>
                    <div class="form-group input-group">
                        <input type="text" id="contract_number" name="contract_number" value="{{ $contract->contract_number }}" class="form-control" required>
                        <span class="input-group-append">
                          <button type="button" class="btn btn-info" id="create_new_contract_number">Tạo mới!</button>
                        </span>
                    </div>
                </div>
                <div class="col-12" id="add-domain-website" @if( $contract->contract_type !== 'TKW' ) style="display: none" @endif>
                    <div class="form-group">
                        <label for="domain_website">Tên miền website</label>
                        <input type="text" id="domain_website" name="domain_website" value="{{  $contract->domain_website ?  $contract->domain_website : '' }}" class="form-control">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row customer-info">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="user">Thông tin khách hàng</label>
                        <select class="form-control select2" name="user_id" id="user" required>
                            <option value="" disabled selected>Chọn khách hàng</option>
                            @foreach( $users as $user )
                              <?php
                                  $selected = '';
                                  if ( $user->id === $contract->user_id ) {
                                      $selected = 'selected';
                                  }
                               ?>
                               <option value="{{ $user->id }}" {{ $selected }}>{{ $user->name }} - {{ $user->email }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>      
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="company">Công ty</label>
                        <input type="text" id="company" name="company" value="{{ $contract->company }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="abbreviation_name">Tên viết tắt (Cá nhân/Công ty/Tổ chức)</label>
                        <input type="text" id="abbreviation_name" name="abbreviation_name" value="{{ $contract->abbreviation_name }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="deputy">Người đại diện</label>
                        <input type="text" id="deputy" name="deputy" value="{{ $contract->deputy }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="deputy_position">Chức vụ người đại diện</label>
                        <input type="text" id="deputy_position" name="deputy_position" value="{{ $contract->deputy_position }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="address">Địa chỉ</label>
                        <input type="text" id="address" name="address" value="{{ $contract->address }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" id="email" name="email" value="{{ $contract->email }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="phone">Số điện thoại</label>
                        <input type="text" id="phone" name="phone" value="{{ $contract->phone }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="mst">Mã số thuế</label>
                        <input type="text" id="mst" name="mst" value="{{ $contract->mst }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <label for="mst">Cập nhập thông tin khách hàng</label>
                    <div class="form-group">
                        <button class="btn btn-outline-info" id="update-customer-info" type="button" data-user-id="{{ $contract->user_id }}" title="Cập nhập khách hàng với thông tin trên hợp đồng">
                            <span class="spinner-grow spinner-grow-sm  spinner-status-update-customer-info" style="display: none" role="status" aria-hidden="true"></span>
                            <span class="status-update-customer-info" style="display: none"></span>
                            Cập nhập
                        </button>
                    </div>
                </div>
                
            </div>
            <hr>
            <div class="row mt-3 mb-5 row-add-products">
                <div class="col-12">
                    <h4 class="text-center mb-4">Thêm thông tin sản phẩm trong hợp đồng</h4>
                </div>
                <div class="col-12 show-products">
                    <table class="table table-bordered table-sm table-hover table-responsive-sm">                       
                        
                        @if ($contract_details)
                        <thead class="text-center">
                            <tr>
                                <th>#</th>
                                <th>Loại sản phẩm</th>
                                <th>Thời gian thuê</th>
                                <th>Số lượng</th>
                                <th>Đơn giá(vnđ)</th>
                                <th>Ghi chú</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contract_details as $key => $contract_detail)
                                <tr>
                                    <td class="text-center">{{ $key+1 }}</td>
                                    <td>
                                        @if ($contract_detail->product_type == 'vps' || $contract_detail->product_type == 'product_vps')
                                            @php $vps = UserHelper::get_contract_detail_vps($contract_detail->target_id, $contract_detail->product_type) @endphp
                                            <span class="btn btn-info btn-xs">Vps</span> 
                                            @if ($contract_detail->product_type === 'vps') 
                                                IP <a href="{{ route('admin.vps.detail', ['id' => $vps->id]) }}" target="_blank">{{ $vps->ip ? $vps->ip : '' }}</a> - Type: {{ isset($vps->product->name) ? $vps->product->name : '' }}
                                            @else
                                                Type: {{ $vps->name }}
                                            @endif

                                        @elseif ($contract_detail->product_type == 'vps_us' || $contract_detail->product_type == 'product_vps_us')
                                            @php $vps_us = UserHelper::get_contract_detail_vps_us($contract_detail->target_id, $contract_detail->product_type) @endphp
                                            <span class="btn btn-warning btn-xs">Vps US</span> 
                                            @if ($contract_detail->product_type == 'vps_us') 
                                                IP <a href="{{ route('admin.vps.detail', ['id' => $vps_us->id]) }}" target="_blank">{{ $vps_us->ip ? $vps_us->ip : '' }}</a> - Type: {{ $vps_us->product->name ? $vps_us->product->name : '' }}
                                            @else
                                                Type: {{ $vps_us->name }}
                                            @endif

                                        @elseif($contract_detail->product_type == 'hosting' || $contract_detail->product_type == 'product_hosting')
                                            @php $hosting = UserHelper::get_contract_detail_hosting($contract_detail->target_id, $contract_detail->product_type) @endphp
                                            <span class="btn bg-indigo btn-xs">Hosting</span>
                                            @if ($contract_detail->product_type == 'hosting')
                                                <a href="{{ route('admin.hosting.detail', ['id' => $hosting->id]) }}" target="_blank">{{ $hosting->domain ? $hosting->domain : '' }} </a>
                                            @else
                                                Type: {{ $hosting->name }}
                                            @endif

                                        @elseif($contract_detail->product_type == 'email_hosting' || $contract_detail->product_type == 'product_email_hosting')
                                            @php $email_hosting = UserHelper::get_contract_detail_email_hosting($contract_detail->target_id, $contract_detail->product_type) @endphp
                                            <span class="btn bg-secondary btn-xs">Email Hosting</span>
                                            @if ($contract_detail->product_type == 'email_hosting')
                                                <a href="{{ route('admin.email_hosting.detail', ['id' => $email_hosting->id]) }}" target="_blank">{{ $email_hosting->domain ? $email_hosting->domain : '' }} </a>
                                            @else
                                                Domain: {{ isset($email_hosting->domain) ? $email_hosting->domain : '' }}
                                            @endif

                                        @elseif ($contract_detail->product_type == 'server' || $contract_detail->product_type == 'product_server')
                                            @if ($contract_detail->product_type === 'server')
                                                @php $server = UserHelper::get_contract_detail_server($contract_detail->target_id) @endphp
                                                <span class="btn btn-danger btn-xs">Server</span> IP: <a href="{{ route('admin.server.detail', ['id' => $server->id]) }}" target="_blank">{{ $server->ip ? $server->ip : '' }}</a> - Type: {{ $server->type ? $server->type : '' }} - Name: {{ $server->server_name ? $server->server_name : '' }} 
                                            @else
                                                <span class="btn btn-danger btn-xs">Server</span>  
                                                {!! $contract_detail->product_option ? $contract_detail->product_option : '' !!}
                                            @endif
                                        
                                        @elseif($contract_detail->product_type == 'colocation' || $contract_detail->product_type == 'product_colocation')
                                            <span class="btn btn-danger btn-xs">Colocation</span>
                                            @if ($contract_detail->product_type == 'colocation')
                                                @php $colocation = UserHelper::get_contract_detail_colocation($contract_detail->target_id) @endphp
                                                Cấu hình: ( Type colo: {{ $colocation->type_colo ?  $colocation->type_colo : ' '}}, IP: {{ $colocation->ip ?  $colocation->ip : ' '}}, Location: {{ $colocation->location ?  $colocation->location : ' '}})
                                            @else
                                                {!! $contract_detail->product_option ? $contract_detail->product_option : '' !!}
                                            @endif

                                        @elseif($contract_detail->product_type == 'domain' || $contract_detail->product_type == 'product_domain')
                                            <span class="btn btn-success btn-xs">Domain</span>
                                            @if ($contract_detail->product_type === 'domain')
                                                @php $domain = UserHelper::get_contract_detail_domain($contract_detail->target_id) @endphp
                                                <a href="{{ route('admin.domain.detail', ['id' => $domain->id]) }}" target="_blank">{{ $domain->domain ? $domain->domain : '' }}</a>
                                            @else
                                                {!! $contract_detail->product_option ? $contract_detail->product_option : '' !!}
                                            @endif
                                            
                                        @elseif($contract_detail->product_type === 'product_website')
                                            {!! $contract_detail->product_option ? $contract_detail->product_option : '' !!}
                                        @endif

                                    </td>
                                    <td class="text-center">{{ $contract_detail->billing_cycle ? $billings[$contract_detail->billing_cycle] : '' }}</td>
                                    <td class="text-center">{{ $contract_detail->qtt ? $contract_detail->qtt : '' }}</td>
                                    <td class="text-center">{{ $contract_detail->amount ? number_format($contract_detail->amount, 0, ',', '.') : '' }}</td>
                                    <td class="text-center">{{ $contract_detail->note ? $contract_detail->note : '' }}</td>
                                    <td style="text-align: center">
                                        <a class="btn btn-warning btn-sm modal-update-contract-detail" data-toggle="modal" data-target="#modal-update-contract-detail" data-id="{{ $contract_detail->id }}" data-user-id="{{ $contract->user_id }}" title="Update"><i class="fas fa-edit"></i></a>
                                        <a class="btn btn-danger btn-sm delete-contract-product" href="javascript:void(0)" data-id="{{ $contract_detail->id }}" title="Delete"><i class="fas fa-trash-alt"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        @endif
                        
                    </table>

                    <div class="col-12 text-center">
                        <button class="btn btn-sm btn-success add-products" data-toggle="modal" data-target="#add-products"><i class="fa fa-plus" aria-hidden="true"></i> Thêm sản phẩm</button>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-12 my-3">
                    <label for="vat">Thuế VAT</label>
                    <div class="icheck-primary d-inline ml-3">
                        <input type="checkbox" id="vat" name="vat" @if ($contract->vat === 1) checked @endif>
                        <label for="vat"></label>
                    </div>
                    {{-- <label for="contract_website" style="margin-left: 20px">Hợp đồng website</label>
                    <div class="icheck-primary d-inline ml-3">
                        <input type="checkbox" id="contract_website" name="contract_website" @if ($contract->contract_website === 1) checked @endif>
                        <label for="contract_website"></label>
                    </div> --}}
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="date_create">Ngày bắt đầu hợp đồng</label>
                        <input type="text" id="date_create" name="date_create" value="{{ date('d-m-Y', strtotime($contract->date_create)) }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="date_end">Ngày kết thúc hợp đồng</label>
                        <input type="text" id="date_end" name="date_end" value="{{ ($contract->date_end) ? date('d-m-Y', strtotime($contract->date_end)) : '' }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="status">Trạng thái</label>
                        <select class="form-control" name="status" id="status" required>
                            <option value="active" {{ ($contract->status === 'active') ? 'selected' : '' }}>Active</option>
                            <option value="pending" {{ ($contract->status === 'pending') ? 'selected' : '' }}>Pending</option>
                            <option value="cancel" {{ ($contract->status === 'cancel') ? 'selected' : '' }}>Cancel</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <a href="{{ route('admin.contract.index') }}" class="btn btn-sm btn-default p-1"><i class="fas fa-angle-double-left"></i> Danh sách hợp đồng</a>
                <div class="float-right">
                    <a href="{{ route( 'admin.contract.print_pdf', [ 'id' => $contract->id ] ) }}" class="btn btn-sm btn-secondary p-1" target="_blank"><i class="fas fa-print"></i> Xem trước</a>
                    <input type="submit" class="btn btn-sm btn-primary p-1" value="Cập nhập">
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Modal thêm sản phẩm -->
<div class="modal fade" id="add-products" tabindex="-1" role="dialog" aria-labelledby="add-products" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="add-product-title"><b>Thêm sản phẩm cho hợp đồng</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row justify-content-center mb-4">
                    <ul class="nav nav-pills" id="choose-type-to-add-product">
                        <li class="nav-item">
                            <a class="nav-link active nav-create-product" href="javascript:void(0)">Đang sử dụng</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link nav-add-new-product" href="javascript:void(0)">Thêm mới</a>
                        </li>
                    </ul>
                </div>

                {{-- Form thêm sản phẩm đã có --}}
                <form action="" method="POST" id="form-create-product">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="product_type">Chọn loại sản phẩm</label>
                                <select class="form-control select2" name="add_product_type" id="add_product_type" data-user-id="{{ $contract->user_id }}" required style="width:100%;">
                                    <option value="" disabled selected>Chọn loại sản phẩm</option>
                                    <option value="vps">VPS</option>
                                    <option value="vps_us">VPS US</option>
                                    <option value="hosting">Hosting</option>
                                    <option value="email_hosting">Email Hosting</option>
                                    <option value="server">Server</option>
                                    <option value="colocation">Colocation</option>
                                    <option value="domain">Domain</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12" id="load-taget">
                            <div class="form-group">
                                <label for="product_type">Chọn sản phẩm</label>
                                <select class="form-control select2" name="add_target_id" id="add_target_id" required style="width:100%;">
                                    <option value="" disabled selected>Chọn sản phẩm</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="qtt">Số lượng</label>
                                <input type="text" id="add_qtt" name="add_qtt" value="{{ $contract->qtt }}" class="form-control" disabled required>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="add_billing_cycle">Thời gian thuê</label>
                                <select class="form-control" name="add_billing_cycle" id="add_billing_cycle" required>
                                    @foreach( $billings as $key => $billing )
                                    <?php
                                        $selected = '';
                                        if ( $key === $contract->billing_cycle ) {
                                            $selected = 'selected';
                                        }
                                    ?>
                                    <option value="{{ $key }}" {{ $selected }}>{{ $billing }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="add_date_create">Ngày bắt đầu</label>
                                <input type="text" id="add_date_create" name="add_date_create" value="" class="form-control" placeholder="vd: {{ date('d-m-Y') }}" disabled>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="add_date_end">Ngày hết hạn</label>
                                <input type="text" id="add_date_end" name="add_date_end" value="" class="form-control" placeholder="vd: {{ date('d-m-Y') }}" disabled>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="add_amount">Thành tiền</label>
                                <input type="text" id="add_amount" name="add_amount" value="" class="form-control" required disabled>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="add_note">Ghi chú</label>
                                <input type="text" id="add_note" name="add_note" value="" class="form-control" disabled>
                            </div>
                        </div>
                        <input type="hidden" id="add_contract_id" name="add_contract_id" value="{{ $contract->id }}" class="form-control">
                    </div>
                </form>

                {{-- Form thêm sản phẩm mới --}}
                <form action="" id="form-add-new-product" style="display: none">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="add_product_type_new">Chọn loại sản phẩm</label>
                                <select class="form-control select2" name="add_product_type_new" id="add_product_type_new" data-user-id="{{ $contract->user_id }}" required style="width:100%;">
                                    <option value="" disabled selected>Chọn loại sản phẩm</option>
                                    <option value="product_vps">VPS</option>
                                    <option value="product_vps_us">VPS US</option>
                                    <option value="product_hosting">Hosting</option>
                                    <option value="product_email_hosting">Email Hosting</option>
                                    <option value="product_server">Server</option>
                                    <option value="product_colocation">Colocation</option>
                                    <option value="product_domain">Domain</option>
                                    <option value="product_website">Website</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-12" id="load-taget-new">
                            <div class="form-group">
                                <label for="add_target_id_new">Chọn sản phẩm</label>
                                <div class="product">
                                <select class="form-control select2" name="add_target_id_new" id="add_target_id_new" required style="width:100%;">
                                        <option value="" disabled selected>Chọn sản phẩm</option>
                                        {{-- Ajax load sản phẩm vào đây --}}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-12" id="create-product-option" style="display: none">
                            <div class="form-group">
                                <label for="add_product_option">Thêm nội dung cho sản phẩm</label>
                                <textarea name="add_product_option" id="add_product_option" cols="30" rows="10" class="form-control"></textarea>
                            </div>
                            <div class="row" id="load_template_product_option_website" style="display: none">
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm" id="btn_copy_template_option_website_1">Copy mẫu option website 1 </a>
                                    <div id="template_option_website_1">
                                        <label for="add_product_option">Mẫu option website 1:</label>
                                        <div id="copy_template_option_website_1" class="template_option_website">
                                            <b>Thiết kế Website doanh nghiệp</b> <br>
                                            <p>
                                                - Tên miền website: {{ $contract->domain_website ? $contract->domain_website : '' }}<br>
                                                - Mục đích: quảng bá hình ảnh doanh nghiệp trên Internet. <br>
                                                - Giao diện: máy tính và thiết bị di động. <br>
                                                - Nhóm chức năng chính: hiển thị hình ảnh, sản phẩm doanh nghiệp, tin tức và quản lý tin bài, các chức năng hiển thị khác (như liên kết fanpage, bản đồ google map, tuyển dụng, …)
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                                    <a href="javascript:void(0)" class="btn btn-primary btn-sm" id="btn_copy_template_option_website_2">Copy mẫu option website 2 </a>
                                    <div id="template_option_website_2">
                                        <label for="add_product_option">Mẫu option website 2:</label>
                                        <div id="copy_template_option_website_2" class="template_option_website">
                                            <b>Dịch vụ hỗ trợ đi kèm trong 01 năm đầu</b> <br>
                                            <p>
                                                - Hỗ trợ gói Cloud Hosting Medium, cấu hình chi tiết: 2 GB lưu trữ, băng thông không giới hạn, HĐH Linux, Control panel Direct Admin. <br>
                                                - Hỗ trợ hướng dẫn sử dụng, quản trị website từ xa qua điện thoại và phần mềm điều khiển từ xa (đăng, sửa, xóa bài viết, chèn hình, file, nội dung... lên trang web). <br>
                                                - Theo dõi, giám sát và sửa các lỗi kỹ thuật liên quan của website trong vòng 01 tháng đầu kể từ khi chính thức hoạt động. <br>
                                                - Hỗ trợ các vấn đề kỹ thuật, sửa chữa nhỏ website như thay đổi giao diện, sửa bố cục giao diện; thay đổi, sửa chữa các chức năng cơ bản đã có sẵn của website. <br>
                                                - Hỗ trợ hosting website cho khách hàng trong trường hợp sử dụng hosting của bên thứ 3.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="add_qtt_new">Số lượng</label>
                                <input type="text" id="add_qtt_new" name="add_qtt_new" value="{{ $contract->qtt }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="add_billing_cycle_new">Thời gian thuê</label>
                                <select class="form-control" name="add_billing_cycle_new" id="add_billing_cycle_new" required>
                                    @foreach( $billings as $key => $billing )
                                    <?php
                                        $selected = '';
                                        if ( $key === $contract->billing_cycle ) {
                                            $selected = 'selected';
                                        }
                                    ?>
                                    <option value="{{ $key }}" {{ $selected }}>{{ $billing }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="add_date_create_new">Ngày bắt đầu</label>
                                <input type="text" id="add_date_create_new" name="add_date_create_new" value="" class="form-control" placeholder="vd: {{ date('d-m-Y') }}">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="add_date_end_new">Ngày hết hạn</label>
                                <input type="text" id="add_date_end_new" name="add_date_end_new" value="" class="form-control" placeholder="vd: {{ date('d-m-Y') }}">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="add_amount_new">Thành tiền</label>
                                <input type="text" id="add_amount_new" name="add_amount_new" value="" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="add_note_new">Ghi chú</label>
                                <input type="text" id="add_note_new" name="add_note_new" value="" class="form-control">
                            </div>
                        </div>
                        
                        <input type="hidden" id="add_contract_id_new" name="add_contract_id_new" value="{{ $contract->id }}" class="form-control">
                    </div>
                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" id="submit-add-product" add_contract_id="{{ $contract->id }}" form_type="create">Thêm</button>
                <button type="button" class="btn btn-primary" id="submit-add-new-product" add_contract_id="{{ $contract->id }}" form_type="add new" style="display: none">Thêm mới</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal sửa sản phẩm -->
<div class="modal fade" id="modal-update-contract-detail" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <form action="{{ route('admin.contract.update_contract_detail') }}" method="POST" id="form-update-product">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="add-product-title"><b>Sửa thông tin sản phẩm</b></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group" id="load_update_product_option">
                                
                            </div>
                            
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="update_qtt">Số lượng</label>
                                <input type="text" id="update_qtt" name="update_qtt" value="" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="update_billing_cycle">Thời gian thuê</label>
                                <select class="form-control" name="update_billing_cycle" id="update_billing_cycle" required>
                                </select>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="update_date_create">Ngày bắt đầu</label>
                                <input type="text" id="update_date_create" name="update_date_create" value="" class="form-control" placeholder="vd: {{ date('d-m-Y') }}">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="update_date_end">Ngày hết hạn</label>
                                <input type="text" id="update_date_end" name="update_date_end" value="" class="form-control" placeholder="vd: {{ date('d-m-Y') }}">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="update_amount">Thành tiền</label>
                                <input type="text" id="update_amount" name="update_amount" value="" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <label for="amount">Ghi chú</label>
                                <input type="text" id="update_note" name="update_note" value="" class="form-control">
                            </div>
                        </div>
                        @csrf
                        <input type="hidden" id="update_contract_detail_id" name="update_contract_detail_id" value="" class="form-control">
                        <input type="hidden" id="id_contract_detail" name="id" value="" class="form-control">
                    </div>
                    <input type="hidden" id="page" value="update_contract">
            </div>
            <div class="modal-footer">
            </div>
        </form>
        </div>
    </div>
</div>

@endsection

@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}" defer></script>
<script src="{{ asset('libraries/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();

        $('#date_create').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
    	});

        $('#date_end').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
        });

        $('#add_date_create_new').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
    	});

        $('#add_date_end_new').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
        });

        $('#choose-type-to-add-product .nav-link').click(function(){
            $('#choose-type-to-add-product .nav-link').removeClass('active');
            $(this).addClass('active');
        });

        $('#choose-type-to-add-product .nav-link.nav-create-product').click(function(){
            $('#form-create-product').show();
            $('#form-add-new-product').hide();
            $('#submit-add-product').show();
            $('#submit-add-new-product').hide();
        });

        $('#choose-type-to-add-product .nav-link.nav-add-new-product').click(function(){
            $('#form-create-product').hide();
            $('#form-add-new-product').show();
            $('#submit-add-product').hide();
            $('#submit-add-new-product').show();
        });

        $('.add-products').on('click', function(e){
            e.preventDefault();
        });
        CKEDITOR.replace( 'add_product_option', {
            enterMode : CKEDITOR.ENTER_BR,
            shiftEnterMode: CKEDITOR.ENTER_P
        });

        // $('#contract_website').change(function() {
        //     console.log('aaa');
        //     if(this.checked) {
        //         $('#add-domain-website').show();
        //     } else {
        //         $('#add-domain-website').hide();
        //     }      
        // });

        $('#contract_type').change(function(){
            if($(this).val() === 'TKW') {
                $('#add-domain-website').show();
            } else {
                $('#add-domain-website').hide();
            } 
        });

    })
</script>
<script>
    jQuery(document).ready(function($){
        
        // -------------------------------------------Chọn người dùng---------------------------------------
        $('#user').on('change', function(){
            var id = $(this).val();
            $('#qtt').val('');
            $('#amount').val('');
            $('.row-add-products').remove();
            $.ajax({
                url: "{{ route('admin.contract.add_data_contract_form') }}",
                data: {'id': id},
                dataType: 'json',
                type: 'GET',
                success: function(data) {
                    // console.log(data);
                    $('[type="text"]').removeAttr('disabled');
                    var html = '';
                    if (data.data_group_product) {
                    html += '<select class="form-control select2" name="product_id" id="product" required style="width:100%;">';
                    html += '<option value="" disabled selected>Chọn sản phẩm</option>';
                    $.each(data.data_group_product, function (index, group_product) {
                        html += '<optgroup label="' + group_product.name + '">';
                        $.each(group_product.products, function (index2, product) {
                        html += '<option value="' + product.id + '">' + product.name + '</option>';
                        });
                        html += '</optgroup>';
                    });
                    html += '</select>';
                    }
                    $('.product').html(html);
                    data.data_user.name ? $('#deputy').val(data.data_user.name) : $('#deputy').val('');
                    // data.data_user.name ? $('#create_new_contract_number').attr('data-name', data.data_user.name) : $('#create_new_contract_number').val('');
                    data.data_user.name ? $('#contract_number').val(create_contract_number(data.data_user.name)+'-'+random_char(5)) : $('#contract_number').val('');
                    data.data_user.email ? $('#email').val(data.data_user.email) : $('#email').val('');
                    data.data_user.user_meta.address ? $('#address').val(data.data_user.user_meta.address) : $('#address').val('');
                    data.data_user.user_meta.phone ? $('#phone').val(data.data_user.user_meta.phone) : $('#phone').val('');
                    data.data_user.user_meta.mst ? $('#mst').val(data.data_user.user_meta.mst) : $('#mst').val('');
                    data.data_user.id ? $('#add-products #product_type').attr('data-user-id', data.data_user.id) : $('#add-products #product_type').attr('data-user-id', '');
                    data.data_user.id ? $('#update-customer-info').attr('data-user-id', data.data_user.id) : $('#add-products #product_type').attr('data-user-id', '');
                },
                error: function(e) {
                    console.error(e);
                }
            })
        });

        // -------------------------------------------Tạo số hợp đồng---------------------------------------------
        $('#create_new_contract_number').on('click', function() {
            var current_date = $.datepicker.formatDate('ddmmy', new Date());
            var contract_type = $('#contract_type').val();
            var abbreviation_name = $('#abbreviation_name').val();
            if (!contract_type || !abbreviation_name) {
                alert('Chưa chọn loại hợp đồng hoặc chưa có tên viết tắt của đối tác');
            } else {
                var contract_number = current_date + '/' + contract_type + '/ĐVS-' +  abbreviation_name;
                $.ajax({
                    url: "{{ route('admin.contract.create_new_contract_number') }}",
                    data: { contract_number: contract_number },
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        $('#contract_number').val(data);
                    },
                    error: function(e) {
                        console.error(e);
                    }
                });
            }
        });

        function create_contract_number(name) {
            var last_name = name.split(' ').slice(-1).join(' ');
            var new_last_name = last_name.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
            var upper_case_last_name = new_last_name.toUpperCase();
            return upper_case_last_name;
        }
        function random_char(length) {
            var result           = [];
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
                result.push(characters.charAt(Math.floor(Math.random() * charactersLength)));
            }
        return result.join('');
        }

        //------------------------------------------ Nút cập nhập lại thông tin khách hàng--------------------------------------
        $('#update-customer-info').click(function() {
            var r = confirm("Bạn muốn cập nhập lại khách hàng với thông tin trên hợp đồng? Thông tin cập nhập (công ty, tên viết tắt, điện thoại, địa chỉ, mã số thuế)");
            if (r == true) {
                var data = {
                    user_id: $(this).attr('data-user-id'),
                    company: $('#company').val(),
                    abbreviation_name: $('#abbreviation_name').val(),
                    phone: $('#phone').val(),
                    address: $('#address').val(),
                    mst: $('#mst').val(),
                };
                $.ajax({
                    url: "{{ route('admin.contract.update_customer_info') }}",
                    data: {
                        "_token": "{{ csrf_token() }}",
                        data
                    },
                    type: 'POST',
                    dataType: 'json',
                    beforeSend: function() {
                        $('.spinner-status-update-customer-info').show();
                        $('.status-update-customer-info').hide().html('');
                    },
                    success: function(data) {
                        $('.spinner-status-update-customer-info').hide();
                        if(data.status == true) {
                            $('.status-update-customer-info').show().html('<i class="far fa-check-circle" title="Cập nhập thành công"></i> ');
                        } else {
                            $('.status-update-customer-info').show().html('<i class="far fa-times-circle" title="Cập nhật thất bại, vui lòng thử lại"></i> ');
                        }
                        console.log(data);
                    },
                    error: function(e) {
                        console.error(e);
                    }
                });
            }
        });


        // ******************************** Form modal thêm sản phẩm đã có ********************************

        // Select chọn loại sản phẩm (VPS, Hosting, Server, ...) --> Lấy dữ kiệu từ bảng vps, hosting, ... tương ứng với người dùng
        $('#add-products #form-create-product #add_product_type').on('change', function(){
            $('#add-products [type="text"]').val('');
            var user_id = $(this).attr('data-user-id');
            var product_type = $(this).val();
            var html = '<option value="" disabled selected>Chọn sản phẩm</option>';
            $.ajax({
                url: "{{ route('admin.contract.get_target_from_user') }}",
                data: {user_id: user_id, product_type: product_type},
                dataType: 'json',
                type: 'GET',
                beforeSend: function() {

                },
                success: function(data) {
                    if (data && data.length > 0) {
                        if (product_type === 'vps' || product_type === 'vps_us' || product_type === 'server' || product_type === 'colocation') {
                            $.each(data, function (index, target) {
                                html += '<option value="' + target.id + '">ID: ' + target.id + ' - IP: ' + target.ip + '</option>';
                            });
                        } else if (product_type == 'hosting' || product_type == 'email_hosting') {
                            $.each(data, function (index, target) {
                                html += '<option value="' + target.id + '">ID: ' + target.id + ' - Hosting: ' + target.domain + '</option>';
                            }); 
                        } else if (product_type === 'domain'){
                            $.each(data, function (index, target) {
                                html += '<option value="' + target.id + '">ID: ' + target.id + ' - Domain: ' + target.domain + '</option>';
                            }); 
                        }
                        $('#add-products #form-create-product #load-taget #add_target_id').html(html);
                        $('#add-products #form-create-product [type="text"]').prop('disabled', false);
                    } else {
                        $('#add-products #form-create-product #load-taget #add_target_id').html(html);
                        $('#add-products #form-create-product [type="text"]').prop('disabled', true);
                    }
                    // console.log(data);
                },
                error: function(e) {
                    console.error(e);
                }
            });
        });

        // Chọn id sản phẩm đã có (target_id) 
        $('#add-products #form-create-product #add_target_id').on('change', function(){
            var target_id = $(this).val();
            var product_type = $('#add-products #form-create-product #add_product_type').val();
            $.ajax({
                url: "{{ route('admin.contract.get_target_detail') }}",
                data: {target_id: target_id, product_type: product_type},
                dataType: 'json',
                type: 'GET',
                beforeSend: function() {
                },
                success: function(data) {
                    if(data) {
                        console.log(data);
                        data.next_due_date ? $('#add-products #form-create-product #add_date_end').val($.datepicker.formatDate('dd-mm-yy', new Date(data.next_due_date))) : $('#add-products #form-create-product #add_date_end').val('');
                        $('#add-products #form-create-product #add_qtt').val('1');
                        data.billing_cycle ? $('#add-products #form-create-product #add_billing_cycle').val(data.billing_cycle) : $('#add-products #form-create-product #add_billing_cycle').val('');
                        data.detail_order.sub_total ? $('#add-products #form-create-product #add_amount').val(data.detail_order.sub_total) : $('#add-products #form-create-product #add_amount').val('');
                    }
                    // console.log(data)
                },
                error: function(e) {
                    console.error(e);
                } 
            });
        });

        // Lưu sản phẩm đã có (Nút submit)
        $('#add-products #submit-add-product').on('click', function() {
            var form_type = $(this).attr('form_type');
            var data = $('form#form-create-product').serialize() + '&form_type=' + form_type;
            $.ajax({
                url: "{{ route('admin.contract.create_contract_detail') }}",
                type: 'POST',
                dataType: 'json',
                data: {
                    "_token": "{{ csrf_token() }}", 
                    data
                },
                beforeSend: function(){
                    $('#add-products .modal-body').html('<div class="text-center"><div class="spinner-border spinner-border-sm text-info" ></div></div>');
                },
                success: function(data) {
                    console.log(data);
                    if (data.status === true) {
                        $('#add-products .modal-body').html('<p class="text-success text-center"><b>'+data.message+'</b></p>');
                        $('#add-products .modal-footer').html('<a href="{{ route('admin.contract.update_form', [ 'id' => $contract->id ]) }}" class="btn btn-success">Hoàn thành</a>');
                    } else if(data.status === false) {
                        $('#add-products .modal-body').html('<p class="text-danger text-center"><b>'+data.message+'</b></p>');
                        $('#add-products .modal-footer').html('<a href="{{ route('admin.contract.update_form', [ 'id' => $contract->id ]) }}" class="btn btn-default">Hủy</a>');
                    }
                },
                error: function(e) {
                    console.error(e);
                }
            });
        });

        // ******************************** Kết thúc thêm sản phẩm đã có ********************************
  

        // ******************************** Form modal thêm sản phẩm mà user chưa có ********************************

        // Select chọn loại sản phẩm (VPS, Hosting, Server, ...) --> Lấy dữ kiệu từ bảng products
        $('#add-products #form-add-new-product #add_product_type_new').on('change', function(){
            $('#add-products [type="text"]').val('');
            var user_id = $(this).attr('data-user-id');
            var product_type = $(this).val();
            var html = '<option value="" disabled selected>Chọn sản phẩm</option>';
            $.ajax({
                url: "{{ route('admin.contract.get_target_product_from_user') }}",
                data: {user_id: user_id, product_type: product_type},
                dataType: 'json',
                type: 'GET',
                beforeSend: function() {
                },
                success: function(data) {
                    if (data && data.length > 0) {
                        if (product_type == 'product_vps' || product_type == 'product_vps_us' || product_type == 'product_hosting' || product_type == 'product_server' || product_type == 'product_email_hosting') {
                            $.each(data, function (index, target_group) {
                                // html += '<option value="' + target.id + '">ID: ' + target.id + ' - IP: ' + target.ip + '</option>';
                                html+='<optgroup label="'+target_group.name+'">';
                                    $.each(target_group.products, function(key, product){
                                        html += '<option value="'+product.id+'">'+product.name+'</option>';
                                    });
                                html+='</optgroup>';
                            });
                        }
                        $('#add-products #load-taget-new #add_target_id_new').html(html);
                    } else {
                        $('#add-products #load-taget-new #add_target_id_new').html(html);
                    }
                    // console.log(data);
                },
                error: function(e) {
                    console.error(e);
                }
            });
            if( product_type == 'product_website' || product_type == 'product_server' || product_type == 'product_colocation' || product_type == 'product_domain') {
                $('#create-product-option').show();
                $('#load-taget-new').hide();
                if (product_type === 'product_website') {
                    $('#load_template_product_option_website').show();
                } else {
                    $('#load_template_product_option_website').hide();
                }
            } else {
                $('#create-product-option').hide();
                $('#load-taget-new').show();
            }
        });
        // Chọn id sản phẩm thêm mới (target_id) 
        $('#add-products #form-add-new-product #add_target_id_new').on('change', function(){
            var target_id = $(this).val();
            var product_type = $('#add-products #form-add-new-product #add_product_type_new').val();
            $.ajax({
                url: "{{ route('admin.contract.get_target_detail_product') }}",
                data: {target_id: target_id, product_type: product_type},
                dataType: 'json',
                type: 'GET',
                beforeSend: function() {
                },
                success: function(data) {
                    if(data) {
                        $('#add-products #form-add-new-product #add_qtt_new').val('1');
                        var add_billing_cycle_new = $('#add-products #form-add-new-product #add_billing_cycle_new').val();
                        data.pricing ? $('#add-products #form-add-new-product #add_amount_new').val(data.pricing[add_billing_cycle_new]) : $('#add-products #form-add-new-product #add_amount_new').val('');
                    }
                    // console.log(data)
                },
                error: function(e) {
                    console.error(e);
                } 
            });
        });

        //Tạo giá sản phẩm khi chọn số lượng
        $('#add-products #form-add-new-product #add_qtt_new').on('keyup', function(){
            var qtt = $(this).val();
            var product_type = $('#form-add-new-product #add_product_type_new').val();
            if( product_type == 'product_vps' || product_type == 'product_vps_us' || product_type == 'product_hosting' || product_type == 'product_email_hosting') {
                var id_product = $('#add-products #form-add-new-product #add_target_id_new').val();
                if(!id_product) {
                    alert('Vui lòng chọn sản phẩm');
                } else {
                    var billing = $('#add-products #form-add-new-product #add_billing_cycle_new').val();
                    var html = '#add-products #form-add-new-product #add_amount_new';
                    get_amount_contract_form_add_new(id_product, billing, qtt, html);
                }
            }
        });

        //Tạo giá sản phẩm khi chọn thời gian thuê
        $('#add-products #form-add-new-product #add_billing_cycle_new').on('change', function(){
            var billing = $(this).val();
            var product_type = $('#form-add-new-product #add_product_type_new').val();
            var id_product = $('#add-products #form-add-new-product #add_target_id_new').val();
            if( product_type == 'product_vps' || product_type == 'product_vps_us' || product_type == 'product_hosting' || product_type == 'product_email_hosting') {
                var id_product = $('#add-products #form-add-new-product #add_target_id_new').val();
                if(!id_product) {
                    alert('Vui lòng chọn sản phẩm');
                    $('#add-products #form-add-new-product #add_amount_new').val('');
                } else {
                    var qtt = $('#add-products #form-add-new-product #add_qtt_new').val();
                    html = '#add-products #form-add-new-product #add_amount_new';
                    if(qtt) {
                        get_amount_contract_form_add_new(id_product, billing, qtt, html);
                    }
                }
            }
        });

        // Lưu sản phẩm chưa có (Nút submit)
        $('#add-products #submit-add-new-product').on('click', function() {
            var form_type = $(this).attr('form_type');
            CKEDITOR.config.autoParagraph = false;
            CKEDITOR.instances.add_product_option.updateElement();
            var data = $('form#form-add-new-product').serialize() + '&form_type=' + form_type;
            $.ajax({
                url: "{{ route('admin.contract.create_contract_detail') }}",
                type: 'POST',
                dataType: 'json',
                data: {
                    "_token": "{{ csrf_token() }}", 
                    data
                },
                beforeSend: function(){
                    $('#add-products .modal-body').html('<div class="text-center"><div class="spinner-border spinner-border-sm text-info" ></div></div>');
                },
                success: function(data) {
                    if (data.status === true) {
                        $('#add-products .modal-body').html('<p class="text-success text-center"><b>'+data.message+'</b></p>');
                        $('#add-products .modal-footer').html('<a href="{{ route('admin.contract.update_form', [ 'id' => $contract->id ]) }}" class="btn btn-success">Hoàn thành</a>');
                    } else if(data.status === false) {
                        $('#add-products .modal-body').html('<p class="text-danger text-center"><b>'+data.message+'</b></p>');
                        $('#add-products .modal-footer').html('<a href="{{ route('admin.contract.update_form', [ 'id' => $contract->id ]) }}" class="btn btn-default">Hủy</a>');
                    }
                },
                error: function(e) {
                    console.error(e);
                }
            });
        });

        // ******************************** Kết thúc thêm sản phẩm mà user chưa có ********************************

        //-----------------------------Xóa sản phẩm trong hợp đồng-----------------------------------
        $('.delete-contract-product').on('click', function(e){
            e.preventDefault();
            var r = confirm("Bạn muốn xóa sản phẩm này?");
            if (r === true) {
                var id = $(this).attr('data-id');
                $.ajax({
                    type: "post",
                    url: "{{ route('admin.contract.delete_contract_detail') }}",
                    dataType: 'json',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "id": id
                    },
                    success: function (data) {
                        alert(data.message);
                        window.location.reload();
                    }, 
                    error: function (e) {
                        console.error(e);
                    }
                });
            }
        });

        // -----------------------------Cập nhập sản phẩm--------------------------------------------
        $('.modal-update-contract-detail').on('click', function(){
            var id = $(this).attr('data-id');
            var user_id = $(this).attr('data-user-id');
            var html_billing = '';
            var html_product = '';
            $('#id_contract_detail').val(id);
            $.ajax({
                type: "get",
                url: "{{ route('admin.contract.modal_update_contract_detail') }}",
                dataType: 'json',
                data: {
                    "id": id,
                    "user_id": user_id
                },
                success: function (data) {
                    $('#modal-update-contract-detail #update_qtt').val(data.contract_detail.qtt);
                    $('#modal-update-contract-detail #update_date_create').val(data.contract_detail.date_create ? $.datepicker.formatDate('dd-mm-yy', new Date(data.contract_detail.date_create)) : '');
                    $('#modal-update-contract-detail #update_date_end').val(data.contract_detail.date_end ? $.datepicker.formatDate('dd-mm-yy', new Date(data.contract_detail.date_end)) : '');
                    $('#modal-update-contract-detail #update_note').val(data.contract_detail.note);
                    $('#modal-update-contract-detail #update_amount').val(data.contract_detail.amount);
                    $('#modal-update-contract-detail #update_contract_detail_id').val(data.contract_detail.id);
                    if(data.contract_detail.product_option) {
                        $('#modal-update-contract-detail #load_update_product_option').show();
                        var html = '<textarea name="update_product_option" id="update_product_option" cols="30" rows="5" class="form-control update_product_option">'+ data.contract_detail.product_option +'</textarea>';
                        $('#load_update_product_option').html(html);
                        CKEDITOR.replace('update_product_option');
                    } else {
                        $('#modal-update-contract-detail #load_update_product_option').hide();
                    }

                    if (data.billings) {
                        $.each(data.billings, function( index, value ) {
                            if (index === data.contract_detail.billing_cycle) {
                                var selected_billing_cycle = 'selected';
                            } else {
                                var selected_billing_cycle = '';
                            }
                            html_billing += '<option value="' + index + '" '+selected_billing_cycle+'>' + value + '</option>'; 
                        });
                    }
                    $('#modal-update-contract-detail #update_billing_cycle').html(html_billing);                 
                    $('#modal-update-contract-detail .product').html(html_product);
                    $('#modal-update-contract-detail .modal-footer').html('<button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Hủy</button> <button type="submit" class="btn btn-sm btn-primary" id="submit-update-product" data-id="'+data.contract_detail.id+'">Sửa</button>');  
                }, 
                error: function (e) {
                    console.error(e);
                }
            });    
        });

        
        $('#modal-update-contract-detail').delegate('#billing_cycle','change', function(){
            var billing = $(this).val();
            var id_product = $('#modal-update-contract-detail #product').val();
            if(!id_product) {
                alert('Vui lòng chọn sản phẩm');
                $('#modal-update-contract-detail #amount').val('');
            } else {
                var qtt = $('#modal-update-contract-detail #qtt').val();
                if(qtt) {
                    get_amount_contract_to_modal_form(id_product, billing, qtt);
                }
            }
        });

        $('#modal-update-contract-detail').delegate('#qtt', 'keyup', function(){
            var qtt = $(this).val();
            var id_product = $('#modal-update-contract-detail #product').val();
            if(!id_product) {
                alert('Vui lòng chọn sản phẩm');
            }
            var billing = $('#modal-update-contract-detail #billing_cycle').val();
            get_amount_contract_to_modal_form(id_product, billing, qtt);
        });

        //Ajax xuất giá sản phẩm khi chọn thời gian thanh toán và số lượng (ở form tạo mới)
        function get_amount_contract_form_add_new(id_product, billing, qtt, html) {
            $.ajax({
                url: "{{ route('admin.contract.fill_amount_contract_form') }}",
                data: {
                    'id': id_product,
                    'billing': billing
                },
                type: 'GET',
                dataType: 'json',
                success: function(data) {
                    $(html).val(data*qtt);
                },
                error: function(e) {
                    console.error(e);
                }
            });
        }   

        $('#btn_copy_template_option_website_1').click(function(e){
            e.preventDefault();
            var html = $('#copy_template_option_website_1').html();
            console.log(html);
            CKEDITOR.instances['add_product_option'].setData(html);
        });

        $('#btn_copy_template_option_website_2').click(function(e){
            e.preventDefault();
            var html = $('#copy_template_option_website_2').html();
            console.log(html);
            CKEDITOR.instances['add_product_option'].setData(html);
        });


    });
</script>
@endsection
