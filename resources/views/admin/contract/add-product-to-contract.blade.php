@php
    use Carbon\Carbon;
@endphp
@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-alt"></i> Tạo hợp đồng
@endsection
@section('breadcrumb')
  <li class="breadcrumb-item"><a href="{{ route('admin.home') }}">Home</a></li>
  <li class="breadcrumb-item active"><a href="{{ route('admin.contract.index') }}">Danh sách hợp đồng</a></li>
  <li class="breadcrumb-item active">Tạo hợp đồng</li>
@endsection
@section('content')

<div class="col col-md-12">
    <h4 class="mb-2">Tạo hợp đồng</h4>
    <hr class="text-secondary">
</div>
<div class="box box-primary">
    <div class="box-body table-responsive">
        <form action="{{ route('admin.contract.save_product_to_contract') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="form-group">
                        <label for="title">Tiêu đề hợp đồng</label>
                        <input type="text" id="title" name="title" value="{{ old('title') ? old('title') : 'Hợp đồng cung cấp dịch vụ'}}" class="form-control" required>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="contract_type">Loại hợp đồng</label>
                        <select class="form-control select2" name="contract_type" id="contract_type" required style="width:100%;">
                            <option value="VPS" @if($contract_type == 'VPS') selected @endif>VPS</option>
                            <option value="HOST" @if($contract_type == 'HOST') selected @endif>Hosting</option>
                            <option value="EHOST" @if($contract_type == 'EHOST') selected @endif>Email Hosting</option>
                            <option value="COLO" @if($contract_type == 'COLO') selected @endif>Colocation</option>
                            <option value="SV" @if($contract_type == 'SV') selected @endif>Server</option>
                            <option value="DM" @if($contract_type == 'DM') selected @endif>Domain</option>
                            <option value="TKW" @if($contract_type == 'TKW') selected @endif>Thiết kế web</option>
                        </select>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <label for="contract_number">Số hợp đồng</label>
                    <div class="form-group input-group">
                        <input type="text" id="contract_number" name="contract_number" value="{{ old('contract_number') }}" class="form-control" required>
                        <span class="input-group-append">
                          <button type="button" class="btn btn-info" id="create_new_contract_number">Tạo mới!</button>
                        </span>
                    </div>
                </div>
                <div class="col-12" id="add-domain-website" style="display: none">
                    <div class="form-group">
                        <label for="domain_website">Tên miền website</label>
                        <input type="text" id="domain_website" name="domain_website" value="{{ old('domain_website') }}" class="form-control">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-12 form-group">
                    <label for="user">Thông tin khách hàng</label>
                    <select class="form-control select2" name="user_id" id="user" required style="width:100%;">
                        <option value="" disabled selected>Chọn khách hàng</option>
                        @foreach( $users as $user )
                          <?php
                              $selected = '';
                              if ( $user->id == $owner->id ) {
                                  $selected = 'selected';
                              }
                           ?>
                           <option value="{{ $user->id }}" {{ $selected }}>{{ $user->name }} - {{ $user->email }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="company">Công ty</label>
                        <input type="text" id="company" name="company" value="{{ $owner->user_meta->company ? $owner->user_meta->company : '' }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="abbreviation_name">Tên viết tắt (Cá nhân/Công ty/Tổ chức)</label>
                        <input type="text" id="abbreviation_name" name="abbreviation_name" value="{{ $owner->user_meta->abbreviation_name ? $owner->user_meta->abbreviation_name : '' }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="deputy">Người đại diện</label>
                        <input type="text" id="deputy" name="deputy" value="{{ $owner->name ? $owner->name : '' }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="deputy_position">Chức vụ người đại diện</label>
                        <input type="text" id="deputy_position" name="deputy_position" value="{{ old('deputy_position') }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="address">Địa chỉ</label>
                        <input type="text" id="address" name="address" value="{{ $owner->user_meta->address ? $owner->user_meta->address : '' }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="text" id="email" name="email" value="{{ $owner->email ? $owner->email : '' }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="phone">Số điện thoại</label>
                        <input type="text" id="phone" name="phone" value="{{ $owner->user_meta->phone ? $owner->user_meta->phone : '' }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-3">
                    <div class="form-group">
                        <label for="mst">Mã số thuế</label>
                        <input type="text" id="mst" name="mst" value="{{ $owner->user_meta->mst ? $owner->user_meta->mst : '' }}" class="form-control">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-12 my-3">
                    <label for="vat">Thuế VAT</label>
                    <div class="icheck-primary d-inline ml-3">
                        <input type="checkbox" id="vat" name="vat" checked>
                        <label for="vat"></label>
                    </div>
                </div>
            </div>
            <hr>

            <div class="row mt-3 mb-5 row-add-products">
                <div class="col-12">
                    <h4 class="text-center mb-4">Thông tin sản phẩm trong hợp đồng</h4>
                </div>
                <div class="col-12 show-products">
                    <table class="table table-bordered table-sm table-hover table-responsive-sm">                       
                        
                        @if ($products)
                        <thead class="text-center">
                            <tr>
                                <th>#</th>
                                <th>Loại sản phẩm</th>
                                <th>Thời gian thuê</th>
                                <th>Ngày bắt đầu</th>
                                <th>Ngày kết thúc</th>
                                <th>Đơn giá(vnđ)</th>
                                <th>Ghi chú</th>
                                <th>Hành động</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($products as $key => $product)
                                <tr>
                                    <td class="text-center">
                                        {{ $key+1 }}
                                        <input type="hidden" name="target_ids[{{$key}}]" value="{{ $product->id }}">
                                        <input type="hidden" name="product_types[{{$key}}]" value="{{ isset($product_type) ? $product_type : '' }}">
                                        <input type="hidden" name="billing_cycles[{{$key}}]" value="{{ $product->billing_cycle ? $product->billing_cycle : '' }}">
                                    </td>
                                    <td>
                                        @if ($product_type == 'vps' || $product_type == 'product_vps')
                                            <span class="btn btn-info btn-xs">Vps</span> 
                                            @if ($product_type === 'vps') 
                                                IP <a href="{{ route('admin.vps.detail', ['id' => $product->id]) }}" target="_blank">{{ $product->ip ? $product->ip : '' }}</a> - Type: {{ isset($product->product->name) ? $product->product->name : '' }}
                                            @else
                                                Type: {{ $product->name }}
                                            @endif

                                        @elseif ($product_type == 'vps_us' || $product_type == 'product_vps_us')
                                            <span class="btn btn-warning btn-xs">Vps US</span> 
                                            @if ($product_type == 'vps_us') 
                                                IP <a href="{{ route('admin.vps.detail', ['id' => $product->id]) }}" target="_blank">{{ $product->ip ? $product->ip : '' }}</a> - Type: {{ $product->product->name ? $product->product->name : '' }}
                                            @else
                                                Type: {{ $product->name }}
                                            @endif

                                        @elseif ($product_type == 'hosting' || $product_type == 'product_hosting')
                                            <span class="btn bg-indigo btn-xs">Hosting</span>
                                            @if ($product_type == 'hosting')
                                                <a href="{{ route('admin.hosting.detail', ['id' => $product->id]) }}" target="_blank">{{ $product->domain ? $product->domain : '' }} </a>
                                            @else
                                                Type: {{ $hosting->name }}
                                            @endif

                                        {{-- @elseif($contract_detail->product_type == 'email_hosting' || $contract_detail->product_type == 'product_email_hosting')
                                            @php $email_hosting = UserHelper::get_contract_detail_email_hosting($contract_detail->target_id, $contract_detail->product_type) @endphp
                                            <span class="btn bg-secondary btn-xs">Email Hosting</span>
                                            @if ($contract_detail->product_type == 'email_hosting')
                                                <a href="{{ route('admin.email_hosting.detail', ['id' => $email_hosting->id]) }}" target="_blank">{{ $email_hosting->domain ? $email_hosting->domain : '' }} </a>
                                            @else
                                                Domain: {{ isset($email_hosting->domain) ? $email_hosting->domain : '' }}
                                            @endif

                                        @elseif ($contract_detail->product_type == 'server' || $contract_detail->product_type == 'product_server')
                                            @if ($contract_detail->product_type === 'server')
                                                @php $server = UserHelper::get_contract_detail_server($contract_detail->target_id) @endphp
                                                <span class="btn btn-danger btn-xs">Server</span> IP: <a href="{{ route('admin.server.detail', ['id' => $server->id]) }}" target="_blank">{{ $server->ip ? $server->ip : '' }}</a> - Type: {{ $server->type ? $server->type : '' }} - Name: {{ $server->server_name ? $server->server_name : '' }} 
                                            @else
                                                <span class="btn btn-danger btn-xs">Server</span>  
                                                {!! $contract_detail->product_option ? $contract_detail->product_option : '' !!}
                                            @endif
                                        
                                        @elseif($contract_detail->product_type == 'colocation' || $contract_detail->product_type == 'product_colocation')
                                            <span class="btn btn-danger btn-xs">Colocation</span>
                                            @if ($contract_detail->product_type == 'colocation')
                                                @php $colocation = UserHelper::get_contract_detail_colocation($contract_detail->target_id) @endphp
                                                Cấu hình: ( Type colo: {{ $colocation->type_colo ?  $colocation->type_colo : ' '}}, IP: {{ $colocation->ip ?  $colocation->ip : ' '}}, Location: {{ $colocation->location ?  $colocation->location : ' '}})
                                            @else
                                                {!! $contract_detail->product_option ? $contract_detail->product_option : '' !!}
                                            @endif

                                        @elseif($contract_detail->product_type == 'domain' || $contract_detail->product_type == 'product_domain')
                                            <span class="btn btn-success btn-xs">Domain</span>
                                            @if ($contract_detail->product_type === 'domain')
                                                @php $domain = UserHelper::get_contract_detail_domain($contract_detail->target_id) @endphp
                                                <a href="{{ route('admin.domain.detail', ['id' => $domain->id]) }}" target="_blank">{{ $domain->domain ? $domain->domain : '' }}</a>
                                            @else
                                                {!! $contract_detail->product_option ? $contract_detail->product_option : '' !!}
                                            @endif
                                            
                                        @elseif($contract_detail->product_type === 'product_website')
                                            {!! $contract_detail->product_option ? $contract_detail->product_option : '' !!} --}}
                                        @endif

                                    </td>
                                    <td class="text-center">{{ $product->billing_cycle ? $billings[$product->billing_cycle] : '' }}</td>
                                    <td class="text-center"><input type="text" name="date_creates[{{$key}}]" class="form-control date_creates" required></td>
                                    <td class="text-center"><input type="text" name="date_ends[{{$key}}]" value="{{ $product->next_due_date ? Carbon::parse($product->next_due_date)->format('d-m-Y') : '' }}" class="form-control date_ends" required></td>
                                    <td class="text-center"><input type="text" name="amounts[{{$key}}]" value="{{ $product->billing_cycle ? $product->product->pricing[$product->billing_cycle] : '' }}" class="form-control"></td>
                                    <td class="text-center"><input type="text" name="add_notes[{{$key}}]" class="form-control" class="form-control"></td>
                                    <td style="text-align: center">
                                        <a class="btn btn-danger btn-sm delete-contract-product" href="javascript:void(0)" data-product-id="{{ $product->id }}" title="Delete"><i class="fas fa-trash-alt"></i></a> 
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        @endif
                        
                    </table>

                </div>
            </div>
            <hr>

            <div class="row mb-5">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="date_create">Ngày bắt đầu</label>
                        <input type="text" id="date_create" name="date_create" value="{{ old('date_create') }}" class="form-control" required>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="form-group">
                        <label for="date_end">Ngày kết thúc</label>
                        <input type="text" id="date_end" name="date_end" value="{{ old('date_end') }}" class="form-control">
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="form-group">
                        <label for="status">Trạng thái</label>
                        <select class="form-control" name="status" id="status" required>
                            <option value="active">Active</option>
                            <option value="pending">Pending</option>
                            <option value="cancel">Cancel</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <a href="{{ route('admin.contract.index') }}" class="btn btn-sm btn-default"><i class="fas fa-angle-double-left"></i> Back</a>
                <input type="submit" class="btn btn-primary btn-sm float-right" value="Tạo hợp đồng">
            </div>
        </form>
    </div>
</div>
<input type="hidden" id="page" value="create_contract">
@endsection

@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}" defer></script>
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}" defer></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();
        $('#date_create').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
    	});
        $('#date_end').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
    	});
        $('.date_ends').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
    	});
        $('.date_creates').datepicker({
          autoclose: true,
          format: 'dd-mm-yyyy'
    	});
    })
</script>
<script>
    jQuery(document).ready(function($){
        $('#date_create').val($.datepicker.formatDate('dd-mm-yy', new Date()));
        $('#user').on('change', function(){
            var id = $(this).val();
            $('#qtt').val('');
            $('#amount').val('');
            $.ajax({
                url: "{{ route('admin.contract.add_data_contract_form') }}",
                data: {'id': id},
                dataType: 'json',
                type: 'GET',
                success: function(data) {
                    console.log(data);
                    $('[type="text"]').removeAttr('disabled');
                    data.data_user.name ? $('#deputy').val(data.data_user.name) : $('#deputy').val('');
                    // data.data_user.name ? $('#contract_number').val(create_contract_number(data.data_user.name)+'-'+random_char(5)) : $('#contract_number').val('');
                    data.data_user.email ? $('#email').val(data.data_user.email) : $('#email').val('');
                    data.data_user.user_meta.address ? $('#address').val(data.data_user.user_meta.address) : $('#address').val('');
                    data.data_user.user_meta.phone ? $('#phone').val(data.data_user.user_meta.phone) : $('#phone').val('');
                    data.data_user.user_meta.mst ? $('#mst').val(data.data_user.user_meta.mst) : $('#mst').val('');
                    data.data_user.user_meta.company ? $('#company').val(data.data_user.user_meta.company) : $('#company').val('');
                    data.data_user.user_meta.abbreviation_name ? $('#abbreviation_name').val(data.data_user.user_meta.abbreviation_name) : $('#abbreviation_name').val('');
                },
                error: function(e) {
                    console.error(e);
                }
            })
        });

        function create_contract_number(name) {
            var last_name = name.split(' ').slice(-1).join(' ');
            var new_last_name = last_name.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/đ/g, "d").replace(/Đ/g, "D");
            var upper_case_last_name = new_last_name.toUpperCase();
            return upper_case_last_name;
        }
        function random_char(length) {
            var result           = [];
            var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
            var charactersLength = characters.length;
            for ( var i = 0; i < length; i++ ) {
                result.push(characters.charAt(Math.floor(Math.random() * charactersLength)));
            }
            return result.join('');
        }

        $('#contract_type').change(function(){
            if($(this).val() === 'TKW') {
                $('#add-domain-website').show();
            } else {
                $('#add-domain-website').hide();
            } 
        });

        $('#create_new_contract_number').on('click', function() {
            var current_date = $.datepicker.formatDate('ddmmy', new Date());
            var contract_type = $('#contract_type').val();
            var abbreviation_name = $('#abbreviation_name').val();
            if (!contract_type || !abbreviation_name) {
                alert('Chưa chọn loại hợp đồng hoặc chưa có tên viết tắt của đối tác');
            } else {
                var contract_number = current_date + '/' + contract_type + '/ĐVS-' +  abbreviation_name;
                $.ajax({
                    url: "{{ route('admin.contract.create_new_contract_number') }}",
                    data: { contract_number: contract_number },
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        $('#contract_number').val(data);
                    },
                    error: function(e) {
                        console.error(e);
                    }
                });
            }
        });

        $('.delete-contract-product').on('click', function() {
            $(this).closest('tr').remove();
        });

    });

</script>
@endsection
