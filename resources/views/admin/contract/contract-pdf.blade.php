<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Content-Type" content="application/pdf">

    <meta name="Keywords" content="" />
    <meta name="Description" content="" />
    <title>Cloudzone Portal Manager</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Cloudzone">

    <link rel="stylesheet" href="{{ asset('libraries/contract-pdf/style.css') }}" media="all" />

    <style type="text/css">
      html, body
        {
          font-family: "dejavu sans", serif;
        }
    </style>
  </head>
  <body style="border: solid #464646 1px; padding: 15px; text-align: justify">
    <main>
        <div class="text-center bieu-ngu">
            <b><h3 style="text-transform: uppercase; margin: 0">CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM</h3></b>
            <h5 style="margin: 0">Độc lập - Tự do - Hạnh phúc</h5>
            <div style="margin-top: -5pt">-----oOo------</div>
        </div>
        <div style="text-align: right"><h6 style="margin: 0; font-weight: 400">Số: {{ $contract->contract_number }}</h6></div>
        <div class="text-center contract-title">
            <h3 style="text-transform: uppercase; margin-top: 0">{{ $contract->title }}</h3>
        </div>
        <div>
            <p>
                @if ($contract->contract_type == 'SV')
                    1. Căn cứ Bộ luật Dân sự số 33/2005/QH11 ngày 14/6/2005 của Quốc hội nước Cộng hoà xã hội chủ nghĩa Việt Nam; <br>
                    2. Căn cứ Luật Thương mại số 36/2005/QH11 ngày 14/6/2005 của Quốc hội nước Cộng hoà xã hội chủ nghĩa Việt Nam; <br>
                    3. Căn cứ Thông tư Số: 09/2008/TT-BTTT; Thông tư số 10/2008/TT-BTTTT và các quy định về quản lý, cung cấp và sử dụng tài nguyên Internet hiện hành của Bộ Thông Tin và Truyền Thông; <br>
                    4. Căn cứ vào yêu cầu và khả năng của hai bên; <br>
                @elseif ($contract->contract_type == 'HOST')
                    1. Căn cứ vào Bộ Luật dân sự số 91/2015/QH13 ngày 24/11/2015 của Quốc hội nước Cộng hoà xã hội chủ nghĩa Việt Nam; <br>
                    2. Căn cứ Thông tư Số: 09/2008/TT-BTTT; Thông tư số 10/2008/TT-BTTTT và các quy định về quản lý, cung cấp và sử dụng tài nguyên Internet hiện hành của Bộ Thông Tin và Truyền Thông; <br>
                    3. Căn cứ vào yêu cầu và khả năng của hai bên;
                @else
                    1. Căn cứ vào Bộ Luật dân sự số 91/2015/QH13 ngày 24/11/2015 của Quốc hội nước Cộng hòa xã hội chủ nghĩa Việt Nam; <br>
                    2. Căn cứ Thông tư Số: 09/2008/TT-BTTT; Thông tư số 10/2008/TT-BTTTT và các quy định về quản lý, cung cấp và sử dụng tài nguyên Internet hiện hành của Bộ Thông tin và Truyền Thông; <br>
                    3. Căn cứ Luật Thương mại số 36/2005/QH11 ngày 14/6/2005 của Quốc hội nước Cộng hoà xã hội chủ nghĩa Việt Nam; <br>
                    4. Căn cứ vào yêu cầu của khách hàng và khả năng cung cấp dịch vụ của Công ty TNHH MTV Công nghệ Đại Việt Số;
                @endif
            </p>
        </div>
        <div>
            <p style="font-weight: bold; margin-top: 0">
                Hôm nay, {{ $contract->date_create ? date('\n\g\à\y d \t\h\á\n\g m \n\ă\m Y', strtotime($contract->date_create)) : '.......................' }}, tại văn phòng Công ty TNHH MTV Công nghệ Đại Việt Số, chúng tôi gồm có:
            </p>
        </div>
        <div>
            <table class="ben-a">
                <tbody>
                    <tr>
                        <td style="padding-left: 0"><b>Bên A</b></td>
                        <td style="text-transform: uppercase"><b>{{ $contract->company ? $contract->company : '' }}</b></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Đại diện bởi: </td>
                        <td>{{ $contract->deputy ? $contract->deputy : '' }}</td>
                        <td>Chức vụ: {{ $contract->deputy_position ? $contract->deputy_position : '' }}</td>
                    </tr>
                    <tr>
                        <td>Địa chỉ: </td>
                        <td>{{ $contract->address ? $contract->address : '' }}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Điện thoại: </td>
                        <td>{{ $contract->phone ? $contract->phone : '' }}</td>
                        <td></td>
                    </tr>
                    
                    @if ($contract->email)
                        <tr>
                            <td>Email: </td>
                            <td>{{ $contract->email ? $contract->email : '' }}</td>
                            <td></td>
                        </tr>
                    @endif
                    
                    <tr>
                        <td>Mã số thuế: </td>
                        <td>{{ $contract->mst ? $contract->mst : '' }}</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div>
            <table class="ben-b">
                <tbody style="vertical-align: top;">
                    <tr>
                        <td style="padding-left: 0"><b>Bên B</b></td>
                        <td style="text-transform: uppercase"><b>Công ty TNHH MTV Công nghệ Đại Việt Số</b></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Đại diện bởi: </td>
                        <td>Bà NGUYỄN THỊ ÁI HOA</td>
                        <td>Chức vụ: GIÁM ĐỐC</td>
                    </tr>
                    <tr>
                        <td>Địa chỉ: </td>
                        <td>257 Lê Duẩn, P.Tân Chính, Q.Thanh Khê, TP.Đà Nẵng</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Văn phòng: </td>
                        <td>67 Nguyễn Thị Định, P.An Hải Bắc, Q.Sơn Trà, TP.Đà Nẵng</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Điện thoại: </td>
                        <td>08 8888 0043</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Email: </td>
                        <td>support@cloudzone.vn</td>
                        <td>Website: https://cloudzone.vn</td>
                    </tr>
                    <tr>
                        <td>Mã số thuế: </td>
                        <td>0401765630</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Số tài khoản: </td>
                        <td>
                            5651.00000.57234<br> 
                            Ngân hàng Đầu tư và Phát triển - CN.Sông Hàn, Đà Nẵng <br>
                            Chủ tài khoản: Công ty TNHH MTV Công nghệ Đại Việt Số
                        </td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div>
            <p style="font-weight: bold"><u>Hai bên thỏa thuận ký kết hợp đồng với những điều khoản sau:</u></p>
            <p><b><u>ĐIỀU 1:</u> NỘI DUNG HỢP ĐỒNG</b></p>
            <p class="p-tab"> Bên A sử dụng các gói dịch vụ hosting của bên B với nội dung gói dịch vụ được mô tả tại điều 2 của hợp đồng này</p>
            <p><b><u>ĐIỀU 2:</u> GIÁ TRỊ HỢP ĐỒNG VÀ PHƯƠNG THỨC THANH TOÁN</b></p>
            <p class="p-tab"> 2.1 Giá trị hợp đồng:</p>
        </div>
        <table class="table table-bordered">
            <thead class="bg-table-cyal">
                <tr>
                    <th width="4%">STT</th>
                    <th width="40%">Dịch vụ</th>
                    <th width="10%">Đơn giá (VNĐ)</th>
                    <th width="8%">Số lượng</th>
                    <th width="8%">Thành tiền</th>
                    <th width="30%">Ghi chú</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($contract_details as $key => $contract_detail)
                    <tr>
                        <td style="text-align: center">{{ $key+1 }}</td>
                        <td>
                            @if ($contract_detail->product_type == 'vps' || $contract_detail->product_type == 'product_vps')
                                @php $vps = UserHelper::get_contract_detail_vps($contract_detail->target_id, $contract_detail->product_type) @endphp
                                <b>Dịch vụ vps</b> <br>
                                @if ($contract_detail->product_type == 'vps')
                                    Địa chỉ IP : <b>{{ $vps->ip ? $vps->ip : '' }}</b><br>
                                    Cấu hình : <b>{{ $vps->product->name ? $vps->product->name : '' }}</b> <br>
                                @else
                                    Cấu hình : <b>{{ $vps->name ? $vps->name : '' }}</b> <br>
                                @endif
                                Thời gian sử dụng: <b>{{ $contract_detail->billing_cycle ? $billings[$contract_detail->billing_cycle] : '' }}</b><br>
                                Từ ngày <b>{{ !empty($contract_detail->date_create) ? date('d/m/Y', strtotime($contract_detail->date_create)) : '..../..../......' }}</b> đến ngày <b>{{ !empty($contract_detail->date_end) ? date('d/m/Y', strtotime($contract_detail->date_end)) : '..../..../......' }}</b>

                            @elseif ($contract_detail->product_type == 'vps_us' || $contract_detail->product_type == 'product_vps_us')
                                @php $vps_us = UserHelper::get_contract_detail_vps_us($contract_detail->target_id, $contract_detail->product_type) @endphp
                                <b>Dịch vụ vps US</b> <br>
                                @if ($contract_detail->product_type == 'vps_us')
                                    Địa chỉ IP : <b>{{ $vps_us->ip ? $vps_us->ip : '' }}</b><br>
                                    Cấu hình : <b>{{ $vps_us->product->name ? $vps_us->product->name : '' }}</b> <br>
                                @else
                                    Cấu hình : <b>{{ $vps_us->name ? $vps_us->name : '' }}</b> <br>
                                @endif
                                Thời gian sử dụng: <b>{{ $contract_detail->billing_cycle ? $billings[$contract_detail->billing_cycle] : '' }}</b><br>
                                Từ ngày <b>{{ !empty($contract_detail->date_create) ? date('d/m/Y', strtotime($contract_detail->date_create)) : '..../..../......' }}</b> đến ngày <b>{{ !empty($contract_detail->date_end) ? date('d/m/Y', strtotime($contract_detail->date_end)) : '..../..../......' }}</b>   
                            
                            @elseif ($contract_detail->product_type == 'hosting' || $contract_detail->product_type == 'product_hosting')
                                @php $hosting = UserHelper::get_contract_detail_hosting($contract_detail->target_id, $contract_detail->product_type) @endphp
                                <b>Dịch vụ hosting</b> <br>
                                @if ($contract_detail->product_type === 'hosting')
                                    Loại: <b>{{ $hosting->product->name ? $hosting->product->name : '' }}</b> <br>
                                    Domain: <b>{{ $hosting->domain ? $hosting->domain : '' }}</b> <br>
                                @else 
                                    Loại: <b>{{ $hosting->name ? $hosting->name : '' }}</b> <br>
                                    Domain: <b>{{ $hosting->domain ? $hosting->domain : '' }}</b> <br>
                                @endif
                                Thời gian sử dụng: <b>{{ $contract_detail->billing_cycle ? $billings[$contract_detail->billing_cycle] : '' }}</b><br>
                                @if ($contract_detail->billing_cycle != 'one_time_pay')
                                    Từ ngày <b>{{ !empty($contract_detail->date_create) ? date('d/m/Y', strtotime($contract_detail->date_create)) : '..../..../......' }}</b> đến ngày <b>{{ !empty($contract_detail->date_end) ? date('d/m/Y', strtotime($contract_detail->date_end)) : '..../..../......' }}</b>
                                @endif
                                
                            @elseif ($contract_detail->product_type == 'email_hosting' || $contract_detail->product_type == 'product_email_hosting')
                                @php $email_hosting = UserHelper::get_contract_detail_email_hosting($contract_detail->target_id, $contract_detail->product_type) @endphp
                                <b>Dịch vụ email hosting</b> <br>
                                @if ($contract_detail->product_type === 'email_hosting')
                                    Loại: <b>{{ $email_hosting->product->name ? $email_hosting->product->name : '' }}</b> <br>
                                    Domain: <b>{{ $email_hosting->domain ? $email_hosting->domain : '' }}</b> <br>
                                @else
                                    Loại: <b>{{ $email_hosting->name ? $email_hosting->name : '' }}</b> <br>
                                    Domain: <b>{{ $email_hosting->domain ? $email_hosting->domain : '' }}</b> <br>
                                @endif
                                Thời gian sử dụng: <b>{{ $contract_detail->billing_cycle ? $billings[$contract_detail->billing_cycle] : '' }}</b><br>
                                Từ ngày <b>{{ !empty($contract_detail->date_create) ? date('d/m/Y', strtotime($contract_detail->date_create)) : '..../..../......' }}</b> đến ngày <b>{{ !empty($contract_detail->date_end) ? date('d/m/Y', strtotime($contract_detail->date_end)) : '..../..../......' }}</b>
                            
                            @elseif ($contract_detail->product_type == 'server' || $contract_detail->product_type == 'product_server')
                                <b>Dịch vụ server</b> <br>
                                @if ($contract_detail->product_type === 'server')
                                    @php $server = UserHelper::get_contract_detail_server($contract_detail->target_id) @endphp
                                    Cấu hình: <b> {{ $server->type ? $server->type : '' }} </b> - Tên server: <b> {{ $server->server_name ? $server->server_name : '' }}</b> <br>
                                    Địa chỉ Ip:  <b>{{ $server->ip ? $server->ip : '' }}</b> <br>
                                @else 
                                    {!! $contract_detail->product_option ? $contract_detail->product_option : '' !!} <br>
                                @endif
                                Thời gian sử dụng: <b>{{ $contract_detail->billing_cycle ? $billings[$contract_detail->billing_cycle] : '' }}</b><br>
                                Từ ngày <b>{{ !empty($contract_detail->date_create) ? date('d/m/Y', strtotime($contract_detail->date_create)) : '..../..../......' }}</b> đến ngày <b>{{ !empty($contract_detail->date_end) ? date('d/m/Y', strtotime($contract_detail->date_end)) : '..../..../......' }}</b>

                            @elseif ($contract_detail->product_type == 'colocation' || $contract_detail->product_type == 'product_colocation')
                                <b>Dịch vụ colocation</b> <br>
                                @if ($contract_detail->product_type === 'colocation')
                                    @php $colocation = UserHelper::get_contract_detail_colocation($contract_detail->target_id) @endphp
                                    Cấu hình: <b>{{ $colocation->type_colo ?  $colocation->type_colo : ' '}}</b> <br>
                                    IP: <b>{{ $colocation->ip ?  $colocation->ip : ' '}}</b> <br>
                                    Vị trí: <b>{{ $colocation->location ?  $colocation->location : ' '}}</b> <br>
                                @else 
                                    {!! $contract_detail->product_option ? $contract_detail->product_option : '' !!} <br>
                                @endif
                                Thời gian sử dụng: <b>{{ $contract_detail->billing_cycle ? $billings[$contract_detail->billing_cycle] : '' }}</b><br>
                                Từ ngày <b>{{ !empty($contract_detail->date_create) ? date('d/m/Y', strtotime($contract_detail->date_create)) : '..../..../......' }}</b> đến ngày <b>{{ !empty($contract_detail->date_end) ? date('d/m/Y', strtotime($contract_detail->date_end)) : '..../..../......' }}</b>

                            @elseif ($contract_detail->product_type == 'domain' || $contract_detail->product_type == 'product_domain')
                                <b>Dịch vụ tên miền</b> <br>
                                @if ($contract_detail->product_type === 'domain')
                                    @php $domain = UserHelper::get_contract_detail_domain($contract_detail->target_id) @endphp
                                    Tên miền: <b>{{ $domain->domain ? $domain->domain : '' }}</b> <br>
                                @else
                                    {!! $contract_detail->product_option ? $contract_detail->product_option : '' !!} <br>
                                @endif
                                Thời gian sử dụng: <b>{{ $contract_detail->billing_cycle ? $billings[$contract_detail->billing_cycle] : '' }}</b><br>
                                Từ ngày <b>{{ !empty($contract_detail->date_create) ? date('d/m/Y', strtotime($contract_detail->date_create)) : '..../..../......' }}</b> đến ngày <b>{{ !empty($contract_detail->date_end) ? date('d/m/Y', strtotime($contract_detail->date_end)) : '..../..../......' }}</b>

                            @elseif ($contract_detail->product_type == 'product_website')
                                {!! $contract_detail->product_option ? $contract_detail->product_option : '' !!}
                            @endif

                        </td>
                        <td style="text-align: center">{{ $contract_detail->don_gia? number_format($contract_detail->don_gia, 0, ',', '.') : '' }}</td>
                        <td style="text-align: center">{{ $contract_detail->qtt ? $contract_detail->qtt : '' }}</td>
                        <td>{{ $contract_detail->amount ? number_format($contract_detail->amount, 0, ',', '.') : '' }}</td>
                        <td>{{ $contract_detail->note ? $contract_detail->note : '' }}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="4" style="text-align: center"><b>TỔNG CỘNG</b></td>
                    <td style="text-align: center">{{ number_format($sub_total, 0, ',', '.') }}</td>
                    <td></td>
                </tr>
                @if ($contract->vat)
                    <tr>
                        <td colspan="4" style="text-align: center"><b>THUẾ VAT (10%)</b></td>
                        <td style="text-align: center">{{ number_format($vat, 0, ',', '.') }}</td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: center"><b>TỔNG CHI PHÍ (ĐÃ CÓ VAT)</b></td>
                        <td style="text-align: center">{{ number_format($total, 0, ',', '.') }}</td>
                        <td></td>
                    </tr>
                @endif
            </tbody>
        </table>
        <span style="font-size: 11px; margin-left: 40px; font-style: italic; ">(Bằng chữ: {{ $contract->text_total }})</span>

        @if ($contract->contract_type === 'TKW')
            <p class="p-tab">
                Nếu Bên A có yêu cầu phát sinh hoặc chỉnh sửa thêm, ngoài những phạm vi công việc, ngoài thời gian và nội dung bảo hành chính đã được hai bên thỏa thuận trước, hai bên sẽ cùng nhau bàn bạc thời gian và chi phí thực hiện. 
            </p>
            <p class="p-tab">
                Sau khi hết thời hạn bảo hành website (01 năm đầu tiên), bên B cung cấp cho bên A dịch vụ bảo trì, hỗ trợ & sửa chữa website như sau:
            </p>
            <table class="table table-bordered">
                <thead class="bg-table-cyal">
                    <tr>
                        <th width="5%">STT</th>
                        <th width="80%">Nội dung</th>
                        <th width="15%">Đơn giá (VNĐ) <br> (chưa bao gồm VAT)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td  style="text-align: center">1</td>
                        <td>
                            <b>Gói dịch vụ bảo trì website:</b> <br>
                            <p>
                                - Hỗ trợ các vấn đề kỹ thuật liên quan đến website <br>
                                - Hỗ trợ các sửa chữa nhỏ website : thay đổi giao diện, sửa bố cục giao diện ; thay đổi, sửa chữa các chức năng cơ bản đã có sẵn của website
                            </p>
                        </td>
                        <td  style="text-align: center">
                            2.000.000
                        </td>
                    </tr>
                </tbody>
            </table>
            
        @endif

        <div>
            <p class="p-tab">
                @if ($contract->contract_type === 'TKW')
                    2.2 Phương thức thanh toán: Sau khi hai bên ký kết hợp đồng, Bên A thanh toán cho Bên B 50% các khoản phí theo Điều 2.1, và thanh toán 50% còn lại sau khi nhận bàn giao, nghiệm thu website. Hình thức thanh toán thông qua chuyển khoản hoặc tiền mặt.
                @elseif ($contract->contract_type === 'SV')
                    2.2 Thời hạn hợp đồng: <b>{{ isset($contract_detail->billing_cycle) ? $billings[$contract_detail->billing_cycle] : '' }}</b>
                @elseif ($contract->contract_type == 'HOST' || $contract->contract_type == 'EHOST' )
                    2.2 Phương thức thanh toán: Sau khi hai bên ký kết hợp đồng, Bên A thanh toán ngay cho Bên B các khoản phí theo Điều 2.1. Đối với gia hạn dịch vụ, thời gian thanh toán được thực hiện vào đầu mỗi chu kỳ, cụ thể là trong vòng 10 ngày đầu tiên của chu kỳ. Hình thức thanh toán thông qua chuyển khoản hoặc tiền mặt.
                @else
                    2.2 Phương thức thanh toán: Sau khi hai bên ký kết hợp đồng, Bên A thanh toán ngay cho Bên B các khoản phí theo Điều 2.1. Hình thức thanh toán thông qua chuyển khoản hoặc tiền mặt.
                @endif
            </p>
            @if ($contract->contract_type === 'SV')
                <p class="p-tab">
                    2.3 Phương thức thanh toán: Sau khi hai bên ký kết hợp đồng, Bên A thanh toán ngay cho Bên B các khoản phí theo Điều 2.1. Đối với gia hạn dịch vụ, thời gian thanh toán được thực hiện vào đầu mỗi chu kỳ, cụ thể là trong vòng 10 ngày đầu tiên của chu kỳ. Hình thức thanh toán thông qua chuyển khoản hoặc tiền mặt.
                </p>
            @endif
            
        </div>
        <div>
            <p><b><u>ĐIỀU 3:</u> QUYỀN VÀ TRÁCH NHIỆM CỦA BÊN A</b></p>
            <p class="p-tab">

                @if ($contract->contract_type === 'TKW')
                    3.1 Chủ động thanh toán chi phí nêu trên cho Bên B đầy đủ ngay sau khi nhận được hóa đơn từ bên B. <br>
                    3.2	Cung cấp vật dụng và tài liệu cần thiết cho bên B trong quá trình triển khai công việc. <br>
                    3.3	Yêu cầu bên B bồi thường thiệt hại do sự bất cẩn nghiêm trọng hoặc do hành vi sai trái cố ý của nhân viên bên B. <br>
                    3.4	Đảm bảo cho bên B khỏi những khiếu nại phát sinh (nếu có) ngoài phạm vi dịch vụ do bên B cung cấp. <br>
                    3.5	Tự chủ động sử dụng các tài nguyên hosting/máy chủ đã được bên B cung cấp theo yêu cầu công việc/hợp đồng của bên A. <br>
                    3.6	Chịu hoàn toàn trách nhiệm pháp lý với các dịch vụ và nội dung được thiết lập, đăng tải trên hosting/máy chủ bên A sử dụng khi truyền qua mạng dịch vụ của bên B. <br>
                    3.7 Tuân thủ theo các quy định pháp luật của Nhà nước về việc sử dụng dịch vụ Internet. Đảm bảo người đại diện ký Hợp đồng này là người có quyền hoặc đã được ủy quyền của Bên A để ký hợp đồng, chứng từ.
                @elseif ($contract->contract_type === 'SV')
                    3.1 Chủ động thanh toán đầy đủ và đúng hạn chi phí đã nêu trong hợp đồng cho Bên B và chịu mọi chi phí phát sinh trong quá trình thanh toán cước (phí chuyển tiền, phí ngân hàng và các chi phí khác có liên quan). <br>
                    3.2 Tự chủ động quản trị và sử dụng các tài nguyên đã được cung cấp theo yêu cầu công việc/hợp đồng của bên A, bao gồm cả các công việc liên quan đến sao lưu dữ liệu, an ninh bảo mật, và các hoạt động quản trị, vận hành khác trong phạm vi hệ điều hành, phần mềm, dữ liệu nằm trên máy chủ vật lý bên A đang thuê. Trong trường hợp có các công việc phát sinh, hai bên cùng tiến hành bổ sung và hiệu chỉnh hợp đồng dựa trên sự thỏa thuận đồng ý của cả hai bên. <br>
                    3.3	Tuân thủ theo các quy định pháp luật của Nhà nước về việc sử dụng dịch vụ Internet. Chịu hoàn toàn trách nhiệm pháp lý với các dịch vụ và nội dung được thiết lập, đăng tải trên máy chủ của bên A. <br>
                    3.4	Bên A có quyền khiếu nại về chất lượng dịch vụ. Mọi khiếu nại hoặc thông báo về hư hỏng, sự cố kết nối được thực hiện thông qua số điện thoại đường dây nóng: 08 8888 0043, nếu bằng văn bản thì theo địa chỉ: <br>
                    <p class="p-2tab">
                        Công ty TNHH MTV Công nghệ Đại Việt Số <br>
                        Số 67, đường Nguyễn Thị Định, phường An Hải Bắc, quận Sơn Trà, Tp. Đà Nẵng <br>
                        Điện thoại: 08 8888 0043
                    </p>
                @elseif ($contract->contract_type === 'HOST')
                    3.1 Chủ động thanh toán đầy đủ và đúng hạn chi phí đã nêu trong hợp đồng cho Bên B và chịu mọi chi phí phát sinh trong quá trình thanh toán cước (phí chuyển tiền, phí ngân hàng và các chi phí khác có liên quan). <br>
                    3.2 Tự chủ động sử dụng các tài nguyên hosting đã được cung cấp theo yêu cầu công việc/hợp đồng của bên A. Đối với các dịch vụ máy chủ ảo, mặc nhiên bên A có trách nhiệm xây dựng các phương án vận hành bao gồm cả sao lưu dữ liệu, an ninh bảo mật, quản trị cho máy chủ bên A thuê sử dụng, trừ trường hợp bên A thuê bên B các dịch vụ tương ứng được hai bên thống nhất và thỏa thuận trong hợp đồng này. <br>
                    3.3	Tuân thủ theo các quy định pháp luật của Nhà nước về việc sử dụng dịch vụ Internet. Chịu hoàn toàn trách nhiệm pháp lý với các dịch vụ và nội dung được thiết lập, đăng tải trên máy chủ/hosting của bên A. <br>
                    3.4	Bên A có quyền khiếu nại về chất lượng dịch vụ. Mọi khiếu nại hoặc thông báo về hư hỏng, sự cố kết nối được thực hiện thông qua số điện thoại đường dây nóng: 08 8888 0043, nếu bằng văn bản thì theo địa chỉ: <br>
                    <p class="p-2tab">
                        Công ty TNHH MTV Công nghệ Đại Việt Số <br>
                        Số 67, đường Nguyễn Thị Định, phường An Hải Bắc, quận Sơn Trà, Tp. Đà Nẵng <br>
                        Điện thoại: 08 8888 0043 <br>
                    </p>
                @else
                    3.1 Chủ động thanh toán chi phí nêu trên cho Bên B đầy đủ. <br>
                    3.2 Tự chủ động sử dụng các tài nguyên hosting đã được cung cấp theo yêu cầu công việc/hợp đồng của bên A. <br>
                    3.3 Chịu hoàn toàn trách nhiệm với các dịch vụ và nội dung được thiết lập, đăng tải trên hosting của bên A.
                @endif

            </p>
        </div>
        <div>
            <p><b><u>ĐIỀU 4:</u> QUYỀN VÀ TRÁCH NHIỆM CỦA BÊN B</b></p>
            <p class="p-tab">

                @if ($contract->contract_type == 'TKW')
                    4.1	Bảo đảm dịch vụ cung cấp cho bên A hoạt động ổn định và giải quyết nhanh chóng các khiếu nại của bên A về chất lượng dịch vụ. <br>
                    4.2	Khi thực hiện tiến hành các thao tác bảo trì, thay đổi hệ thống ảnh hưởng đến dịch vụ phải thông báo cho bên A trước ít nhất 02 ngày, trừ các trường hợp sự cố bất khả kháng do cháy, nổ, thiên tai, lũ lụt,... <br>
                    4.3	Bảo đảm dữ liệu của bên A không bị mất mát, trừ các trường hợp lỗi chủ quan đến từ bên A (hoạt động vận hành quản trị, xóa nhầm dữ liệu, lỗ hỗng phần mềm dẫn đến bị xâm nhập, thay đổi dữ liệu, ...) hoặc do các sự cố bất khả kháng do cháy, nổ, thiên tai, lũ lụt,... <br>
                    4.4 Trong trường hợp có sự thay đổi trong hoạt động của bên B làm ảnh hưởng đến các dịch vụ đang sử dụng của bên A, bên B có trách nhiệm bảo đảm các dịch vụ của bên A hoạt động ổn định thông qua việc hỗ trợ di chuyển nhà cung cấp dịch vụ, duy trì hệ thống hoạt động cho đến khi chấm dứt hợp đồng giữa 2 bên mà không phát sinh thêm các chi phí khác. <br>
                    4.5	Bên B có quyền đơn phương tạm ngừng cung cấp dịch vụ hoặc chấm dứt thực hiện hợp đồng đã ký trong trường hợp Bên A sử dụng dịch vụ không theo đúng các cam kết trong hợp đồng này hoặc không tuân thủ pháp luật.
                @elseif ($contract->contract_type == 'SV')
                    4.1	Bảo đảm dịch vụ máy chủ vật lý cung cấp cho bên A đúng cấu hình trong hợp đồng và hoạt động ổn định. <br>
                    4.2 Trong trường hợp dịch vụ máy chủ vật lý bị gián đoạn do lỗi phần cứng, bên B có trách nhiệm thay thế linh kiện/thiết bị hoặc có phương án thay thế thiết bị tương đương trong thời gian không quá 02 ngày làm việc tính từ lúc hai bên xác nhận sự cố lỗi. <br>
                    4.3	Khi thực hiện tiến hành các thao tác bảo trì, thay đổi hệ thống ảnh hưởng đến dịch vụ phải thông báo cho bên A trước ít nhất 01 ngày, trừ các trường hợp sự cố bất khả kháng do cháy, nổ, thiên tai, lũ lụt được mô tả trong điều 5. <br>
                    4.4 Trong trường hợp có sự thay đổi trong hoạt động của bên B làm ảnh hưởng đến các dịch vụ đang sử dụng của bên A, bên B có trách nhiệm bảo đảm các dịch vụ của bên A hoạt động ổn định thông qua việc hỗ trợ di chuyển nhà cung cấp dịch vụ, duy trì hệ thống hoạt động cho đến khi chấm dứt hợp đồng giữa 2 bên mà không phát sinh thêm các chi phí khác. <br>
                    4.5	Bên B có quyền đơn phương tạm ngừng cung cấp dịch vụ hoặc chấm dứt thực hiện hợp đồng đã ký trong trường hợp Bên A sử dụng dịch vụ không theo đúng các cam kết trong hợp đồng này hoặc không tuân thủ pháp luật.     
                @elseif ($contract->contract_type == 'HOST')
                    4.1	Bảo đảm dịch vụ hosting cung cấp cho bên A đúng cấu hình trong hợp đồng và hoạt động ổn định. <br>
                    4.2	Khi thực hiện tiến hành các thao tác bảo trì, thay đổi hệ thống ảnh hưởng đến dịch vụ phải thông báo cho bên A trước ít nhất 01 ngày, trừ các trường hợp sự cố bất khả kháng do cháy, nổ, thiên tai, lũ lụt được mô tả trong điều 5. <br>
                    4.3 Bảo đảm dữ liệu của bên A không bị mất mát, trừ các trường hợp lỗi chủ quan đến từ bên B (xóa nhầm dữ liệu, lỗ hổng phần mềm dẫn đến bị xâm nhập, thay đổi dữ liệu,...) hoặc do các sự cố bất khả kháng do cháy, nổ, thiên tai, lũ lụt được mô tả trong điều 5. <br>
                    4.4	Trong trường hợp dịch vụ do bên B cung cấp bị gián đoạn, bên B có trách nhiệm sửa chữa và đưa hệ thống trở lại hoạt động nhanh nhất. Đối với các sai sót gây ra mất mác dữ liệu của bên A từ phía bên B, hai bên tiến hành trao đổi để thống nhất phương án bồi thường nhưng tổng chi phí bồi thường không vượt quá 06 tháng đối với dịch vụ bị ảnh hưởng. Hai bên cùng thống nhất chi phí bồi thường được quy đổi ra thời gian sử dụng tặng thêm, giảm giá cước hoặc giá trị tương đương. <br>
                    4.5 Trong trường hợp có sự thay đổi trong hoạt động của bên B làm ảnh hưởng đến các dịch vụ đang sử dụng của bên A, bên B có trách nhiệm bảo đảm các dịch vụ của bên A hoạt động ổn định thông qua việc hỗ trợ di chuyển nhà cung cấp dịch vụ, duy trì hệ thống hoạt động cho đến khi chấm dứt hợp đồng giữa 2 bên mà không phát sinh thêm các chi phí khác. <br>
                    4.6	Bên B có quyền đơn phương tạm ngừng cung cấp dịch vụ hoặc chấm dứt thực hiện hợp đồng đã ký trong trường hợp Bên A sử dụng dịch vụ không theo đúng các cam kết trong hợp đồng này hoặc không tuân thủ pháp luật.
                @else
                    4.1 Bảo đảm dịch vụ cung cấp cho bên A hoạt động ổn định <br>
                    4.2 Khi thực hiện tiến hành các thao tác bảo trì, thay đổi hệ thống ảnh hưởng đến dịch vụ phải thông báo cho bên A trước ít nhất 02 ngày, trừ các trường hợp sự cố bất khả kháng do cháy, nổ, thiên tai, lũ lụt được mô tả trong điều 5. <br>
                    4.3. Bảo đảm dữ liệu của bên A không bị mất mát, trừ các trường hợp lỗi chủ quan đến từ bên B (xóa nhầm dữ liệu, lỗ hổng phần mềm dẫn đến bị xâm nhập, thay đổi dữ liệu,...) hoặc do các sự cố bất khả kháng do cháy, nổ, thiên tai, lũ lụt được mô tả trong điều 5. <br>
                    4.4. Trong trường hợp có sự thay đổi trong hoạt động của bên B làm ảnh hưởng đến các dịch vụ đang sử dụng của bên A, bên B có trách nhiệm bảo đảm các dịch vụ của bên A hoạt động ổn định thông qua việc hỗ trợ di chuyển nhà cung cấp dịch vụ, duy trì hệ thống hoạt động cho đến khi chấm dứt hợp đồng giữa 2 bên mà không phát sinh thêm các chi phí khác.                
                @endif
            </p>
        </div>
        <div>
            @if ($contract->contract_type === 'TKW')
                <p><b><u>ĐIỀU 5:</u> NHỮNG THỎA THUẬN KHÁC</b></p>
                <p class="p-tab">
                    5.1	Phát triển ứng dụng/nâng cấp hệ thống: Vì nhu cầu thực tế, bên A có thể sẽ phát triển, nâng cấp website, nâng cấp hạ tầng máy chủ. Trong quá trình thực hiện và bảo trì, nâng cấp, bên B sẽ chỉnh sửa theo các yêu cầu của bên A với các chi phí được thỏa thuận giữa 2 bên. <br>
                    5.2	Quyền sở hữu trí tuệ (chỉ áp dụng đối với sản phẩm là website/phần mềm): Các bên có quyền sở hữu trí tuệ theo quy định tại Luật sở hữu trí tuệ. Bên A là chủ sở hữu và có quyền tài sản đối với phần mềm/website theo quy định của pháp luật.
                </p>
                           
            @else
                <p><b><u>ĐIỀU 5:</u> BẤT KHẢ KHÁNG</b></p>
                <p class="p-tab">
                    5.1 Các sự kiện như thiên tai, dịch họa, lũ lụt, bão, hỏa hoạn, động đất, cháy nổ hoặc các hiểm họa thiên tai khác hoặc sự can thiệp của cơ quan chức năng gọi là bất khả kháng. <br>
                    5.2 Bên bị ảnh hưởng bởi sự kiện bất khả kháng có nghĩa vụ thông báo cho bên còn lại. <br>
                    5.3 Khi sự kiện bất khả kháng chấm dứt, và có khả năng tiếp tục dịch vụ thì các bên sẽ tiếp tục thực hiện hợp đồng. <br>
                </p>
            @endif

        </div>

        @if ($contract->contract_type === 'TKW')
            <div>
                <p><b><u>ĐIỀU 6:</u> CHẤM DỨT HỢP ĐỒNG TRƯỚC THỜI HẠN</b></p>
                <p class="p-tab">
                    6.1	Bên A có quyền chấm dứt hợp đồng nếu Bên B không chuyển giao các kết quả dịch vụ đúng cam kết. Khi đó, Bên B phải hoàn trả lại toàn bộ số tiền bên A đã thanh toán theo hợp đồng. <br>
                    6.2	Bên B có quyền chấm dứt hợp đồng nếu Bên A vi phạm nội dung của hợp đồng này dẫn đến quá trình phát triển và triển khai không đúng như lịch trình đã cam kết. Trong trường hợp này, phí dịch vụ sẽ được tính trên chi phí thực tế mà bên B đã thực hiện.
                </p>
            </div>
            <div>
                <p><b><u>ĐIỀU 7:</u> GIA HẠN VÀ THANH LÝ HỢP ĐỒNG</b></p>
                <p class="p-tab">
                    7.1.  Hợp đồng mặc nhiên được gia hạn hằng năm nếu một trong 2 bên không có các yêu cầu liên quan đến hủy dịch vụ khi đến hạn. <br>
                    7.2.  Hợp đồng sẽ được thanh lý khi một trong các trường hợp sau: <br>
                        + Cả hai bên hoàn tất mọi nghĩa vụ và quyền lợi liên quan tại Hợp đồng này. <br>
                        + Bên A gửi văn bản yêu cầu Bên B chấm dứt việc cung cấp dịch vụ, hai bên cùng trao đổi để hoàn tất các nghĩa vụ liên quan trong hợp đồng trước khi chấm dứt. <br>
                        + Theo yêu cầu của các cơ quan chức năng.
                </p>
            </div>

        @else
            <div>
                <p><b><u>ĐIỀU 6:</u> GIA HẠN VÀ THANH LÝ HỢP ĐỒNG</b></p>
                <p class="p-tab">
                    6.1 Hợp đồng mặc nhiên được gia hạn hằng năm nếu một trong 2 bên không có các yêu cầu liên quan đến hủy dịch vụ khi đến hạn. <br>
                    6.2 Hợp đồng sẽ được thanh lý khi một trong các trường hợp sau: 
                    <p class="p-2tab">
                        + Cả hai bên hoàn tất mọi nghĩa vụ và quyền lợi liên quan tại Hợp đồng này. <br>
                        + Bên A gửi văn bản yêu cầu Bên B chấm dứt việc cung cấp dịch vụ, hai bên cùng trao đổi để hoàn tất các nghĩa vụ liên quan trong hợp đồng trước khi chấm dứt. <br>
                        + Theo yêu cầu của các cơ quan chức năng. 
                    </p>
                </p>
            </div>

        @endif
        
        <p> Hợp đồng được lưu thành 02 bản, mỗi bên giữ 01 bản, có giá trị như nhau.</p>
        <p>
            <table class="sign">
                <tbody>
                    <tr>
                        <td>
                            <b>ĐẠI DIỆN BÊN A</b>
                        </td>
                        <td>
                            <b>ĐẠI DIỆN BÊN B</b>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="padding-top: 50pt"><b>NGUYỄN THỊ ÁI HOA</b></td>
                    </tr>
                </tbody>
            </table>
        </p>
    </main>
  </body>
</html>