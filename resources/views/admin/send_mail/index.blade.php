@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-alt"></i> Quản lý danh sách gửi mail
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Danh sách gửi mail</li>
@endsection
@section('content')
<div class="col-md-12 detail-course-finish">
    @if(session("success"))
    <div class="bg-success">
        <p class="text-light">{{session("success")}}</p>
    </div>
    @elseif(session("fails"))
    <div class="bg-danger">
        <p class="text-light">{{session("fails")}}</p>
    </div>
    @endif
</div>
<div class="container-fluid">
  <!-- Timelime example  -->
  <div class="row">
        <div class="col-md-12">
            <div class="fillter_log mt-4 mb-4">
                <button type="button" class="btn btn-primary btn_filter" name="button"><i class="fas fa-clipboard-list"></i> Lọc</button>
            </div>
            <div class="mt-4 mb-4">
              Đã gửi 
              <span class="text-success"><b class="total_send"></b></span> 
              /
              <span class="text-danger"><b class="total_mail"></b></span>
            </div>
            <div class="card">
                <div class="card-body table-responsive p-0">
                <table class="table table-hover">
                    <thead>
                        <th>Khách hàng</th>
                        <th>Email</th>
                        <th>Loại</th>
                        <th>Ngày tạo</th>
                        <th>Trạng thái</th>
                        <th>Hành động</th>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot></tfoot>
                </table>
                </div>
            </div>
        </div>
  </div>
</div>
{{-- Modal --}}
  <!-- /.modal -->
{{-- Modal xoa product --}}
  <div class="modal fade" id="detail_invoice">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Chi tiết mail</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div id="notication_mail" class="m-4">

              </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
            <button type="button" class="btn btn-danger" id="btn_delete_quotation">Xác nhận</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal" id="finish_quotation">Hoàn thành</button>
          </div>
        </div>
        <!-- /.modal-content -->
    </div>
      <!-- /.modal-dialog -->
</div>
    <!-- /.modal -->
<input type="hidden" id="page" value="list_send_mail">
<input type="hidden" id="user" value="">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('js/admin_send_mail.js') }}"></script>
@endsection
