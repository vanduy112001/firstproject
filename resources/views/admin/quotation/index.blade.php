@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    <i class="fas fa-file-alt"></i> Quản lý danh sách báo giá
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="/admin">Home</a></li>
    <li class="breadcrumb-item active">Danh sách báo giá</li>
@endsection
@section('content')
<div class="col-md-12 detail-course-finish">
    @if(session("success"))
    <div class="bg-success">
        <p class="text-light">{{session("success")}}</p>
    </div>
    @elseif(session("fails"))
    <div class="bg-danger">
        <p class="text-light">{{session("fails")}}</p>
    </div>
    @endif
    <div id="notication">

    </div>
</div>
<div class="container-fluid">
  <!-- Timelime example  -->
  <div class="row">
      <div class="col-md-12">
          <div class="m-4">
            <a href="{{ route('admin.quotation.create') }}" class="btn btn-primary">Tạo báo giá mới</a>
          </div>
          <div class="card">
             <div class="card-body table-responsive p-0">
               <table class="table table-hover">
                  <thead>
                    <th>Khách hàng</th>
                    <th>Tiêu đề</th>
                    <th>Ngày tạo</th>
                    <th>Hành động</th>
                  </thead>
                  <tbody>
                  </tbody>
               </table>
             </div>
          </div>
      </div>
  </div>
</div>
{{-- Modal --}}
  <!-- /.modal -->
{{-- Modal xoa product --}}
  <div class="modal fade" id="delete-product">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Xóa báo giá</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div id="notication-product" class="text-center">

              </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
            <button type="button" class="btn btn-primary" id="delete_quotation">Xóa báo giá</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal" id="finish_quotation">Hoàn thành</button>
          </div>
        </div>
        <!-- /.modal-content -->
    </div>
      <!-- /.modal-dialog -->
</div>
    <!-- /.modal -->
<input type="hidden" id="page" value="list_qoutation">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}" defer></script>
<script src="{{ asset('js/create_quotation.js') }}"></script>
@endsection
