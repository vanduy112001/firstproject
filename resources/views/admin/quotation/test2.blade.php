<!DOCTYPE html>
<html lang="vi">
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="application/xhtml+xml; charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="Content-Type" content="application/pdf">

    <meta name="Keywords" content="" />
    <meta name="Description" content="" />
    <title>Cloudzone Portal Manager</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Cloudzone">
    

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ asset('fonts/fontawesome-free/css/all.min.css') }}"   type="text/css">
    <link rel="stylesheet" href="{{ asset('/libraries/bootstrap/dist/css/bootstrap.min.css')}}"  type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style  type="text/css">
        html, body
        {
          /* font-family: DejaVu Sans !important; */
          /* font-size: 12pt !important; */
          font-family: Roboto,sans-serif !important; 
        }
        .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            margin-right: -7.5pt;
            margin-left: -7.5pt;
        }
        .col-md-8 {
            -ms-flex: 0 0 66.666667%;
            flex: 0 0 66.666667%;
            max-width: 66.666667%;
        }
        .col-md-4 {
            -ms-flex: 0 0 33.333333%;
            flex: 0 0 33.333333%;
            max-width: 33.333333%;
        }
        .text-right {
            text-align: right !important;
        }
        .p-3 {
            padding: 7.5pt !important;
        }
        .mb-3, .my-3 {
            margin-bottom: 7.5pt !important;
        }
        .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
            border:0;
            padding:0;
            margin-left:-0.00001;
        }
        #invoice-page {
            margin: 11.25pt auto;
            padding: 0;
            max-width: 645pt;
            background-color: #fff;
        }
        .invoice {
            background: #fff;
            border: 0.75pt solid rgba(0,0,0,.125);
            border-radius: 4.5pt;
            font-family: Roboto !important;
        }
        .quotation-header {
            padding-left: 10%;
            padding-right: 10%;
            position: relative;
            opacity: 0.7;
        }
        .address {
            padding-right: 11.25pt;
            padding-left: 11.25pt;
        }
        .company {
            margin: 0 !important;
            /* line-height: 10pt; */
            font-style: normal;
            font-size: 7pt !important;
            color: #000000c5;
        }
        .quotation-header-hr-top {
            height: 0.75pt;
            color: #ae4a00;
            background: #ae4a00;
            width: 100%;
            padding-bottom: 20pt;
        }
        .quotation-header-hr-bottom {
            height: 2.25pt;
            color: #ae4a00c4;
            background: #ae4a00;
            width: 100%;
            padding-bottom: 20pt;
        }
    </style>
</head>
<body  class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">
        <section class="content">
            <div class="container-fluid">
                <main class="py-4">
                    <div class="box-body table-responsive">
                        <div id="invoice-page">
                            <!-- Main content -->
                            <div class="invoice quotation-body p-3 mb-3">
                                <!-- title row -->
                                <div class="row invoice-header quotation-header">
                                    <div class="col-xs-8 col-sm-8 invoice-status">
                                      <div class="company address">
                                          <span style="color: #3f3e3e;">Công ty TNHH MTV Công nghệ Đại Việt Số</span><br>
                                          <span style="color: #3f3e3e;">Địa chỉ: 257 Lê Duẩn, Đà Nẵng</span><br>
                                          <span style="color: #3f3e3e;">Văn phòng: 67 Nguyễn Thị Định, Đà Nẵng</span> <br>
                                          <span style="color: #3f3e3e;">SĐT: 0236 4455 789</span> <span class="ml-3 mr-3">-</span> 
                                          <span style="color: #3f3e3e;">Website: <a href="https://cloudzone.vn">https://cloudzone.vn</a></span>
                                      </div>
                                    </div>
                                    <div class="col-xs-4 col-sm-4 logo text-right">
                                      <img src="{{ url('images/LogoCloud.png') }}" alt="cloudzone.vn" width="120">
                                    </div>
                                    <hr class="quotation-header-hr-top">
                                    <hr class="quotation-header-hr-bottom">
                                <!-- /.col -->
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
            </div>
        </section>
    </div>
</body>
</html>