@extends('layouts.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('/libraries/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/libraries/toastr/toastr.min.css')}}">
    <style>
        .ticket-status-pending {
            color: orange;
        }

        .ticket-status-done {
            color: green;
        }

        .ticket-status-processing {
            color: blue;
        }

        .ticket-status-error {
            color: red;
        }
    </style>
@endsection
@section('title')
    <div class="text-primary">Quản lý tickets</div>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
    <li class="breadcrumb-item active">Quản lý tickets</li>
@endsection
@section('content')
    <div class="row">
        <div class="col col-md-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="col-md-12 detail-course-finish">
                            @if(session("success"))
                                <div class="bg-success">
                                    <p class="text-light">{{session("success")}}</p>
                                </div>
                            @elseif(session("fails"))
                                <div class="bg-danger">
                                    <p class="text-light">{{session("fails")}}</p>
                                </div>
                            @endif
                        </div>
                        <div id="group_button">
                            <div class="table-responsive" style="display: block;">
                                <button class="btn btn-danger btn-action btn-icon-split btn-sm mb-1" data-type="on">
                                    <span class="icon"><i class="fa fa-ban" aria-hidden="true"></i> </span>
                                    <span class="text"> Xóa Ticket</span>
                                </button>
                            </div>
                        </div>
                        <table class="table table-bordered table-hover">
                            <thead class="info">
                              <th width="3%"><input type="checkbox" class="checkbox_all" data-toggle="tooltip" data-placement="top" title="Chọn tất cả IP VPS"></th>
                              <th>Mã ticket</th>
                              <th>User</th>
                              <th>Tiêu đề</th>
                              <th>Nội dung</th>
                              <th>Trạng thái</th>
                              <th>Ngày tạo</th>
                              <th>Action</th>
                            </thead>
                            <tbody>
                            @if($list->count() > 0)
                                @foreach($list as $item)
                                    <tr>
                                        <td><input type="checkbox" value="{{ $item->id }}" class="checkbox"></td>
                                        <td>
                                            <a href="{{ route('admin.ticket.detail',['id' => $item->id]) }}">#{{ $item->id }}</a>
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.user.detail', $item->user_id) }}">{{ !empty($item->user->name ) ? $item->user->name  : 'Đã xóa'}}</a>
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.ticket.detail',['id' => $item->id]) }}">{{ $item->title }}</a>
                                        </td>
                                        <td>{{ !empty($item->content) ? substr($item->content, 0, 100) : '' }}</td>
                                        <td>
                                            <span
                                                class="ticket-status ticket-status-{{ $item->status }}">{{ $item->status }}</span>
                                        </td>
                                        <td>
                                            {{ date('d/m/Y H:i:s',strtotime($item->created_at)) }}
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.ticket.detail',['id' => $item->id]) }}">Xem</a>
                                            <button class="btn btn-danger btn-delete ml-2" data-id="{{ $item->id }}"><i class="fas fa-trash-alt"></i></button>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <td class="text-center text-danger" colspan="8">
                                    Chưa có tickets nào
                                </td>
                            @endif
                            </tbody>
                            <tfoot class="card-footer clearfix">
                            <td colspan="8" class="text-center">
                                {{ $list->links()  }}
                            </td>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <div id="group_button">
                    <div class="table-responsive" style="display: block;">
                        <button class="btn btn-danger btn-action btn-icon-split btn-sm mb-1" data-type="on">
                            <span class="icon"><i class="fa fa-ban" aria-hidden="true"></i> </span>
                            <span class="text"> Xóa Ticket</span>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- Modal xoa vps --}}
    <div class="modal fade" id="delete_vps">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title"></h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div id="notication-invoice" class="text-center">

                </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
              <input type="submit" class="btn btn-primary" id="button-vps" value="Xóa ticket">
              <button type="button" class="btn btn-danger" id="button-vps-finish" data-dismiss="modal">Hoàn thành</button>
            </div>
          </div>
          <!-- /.modal-content -->
      </div>
        <!-- /.modal-dialog -->
    </div>
      <!-- /.modal -->
      <!-- Model action từng services -->
      <div class="modal fade" id="modal-service">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Xóa Ticket</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                  <div id="notication-service" class="text-center">

                  </div>
              </div>
              <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                <button type="button" class="btn btn-primary" id="button-service">Xác nhận</button>
                <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
              </div>
            </div>
            <!-- /.modal-content -->
        </div>
          <!-- /.modal-dialog -->
      </div>
<input type="hidden" id="page" value="list_ticket">
@endsection
@section('scripts')
    <script type="text/javascript">
      $(document).ready(function () {
        var $vpsCheckbox = $('.checkbox');
        var lastChecked = null;

        $('.btn-delete').on('click', function () {
          var id = $(this).attr('data-id');
          $('tr').removeClass('action_ticket');
          $(this).closest('tr').addClass('action_ticket');
          $('#delete_vps').modal('show');
          $('.modal-title').text('Xoá Ticket');
          $('#notication-invoice').html('<span>Bạn có muốn xóa Ticket này không?</span>');
          $('#button-vps').attr('data-id', id);
          $('#button-vps').fadeIn();
          $('#button-vps-finish').fadeOut();
        })

        $('#button-vps').on('click', function () {
            var id = $(this).attr('data-id');
            var token = $('meta[name="csrf-token"]').attr('content');
            $.ajax({
               url: '/admin/tickets/delete',
               type: 'post',
               data: {id: id, _token: token},
               dataType: 'json',
               beforeSend: function(){
                   var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                   $('#button-vps').attr('disabled', true);
                   $('#notication-invoice').html(html);
               },
               success: function (data) {
                   // console.log(data);
                   if (data) {
                       var html = '<p class="text-danger">Xóa Ticket thành công</p>';
                       $('#notication-invoice').html(html);
                       $('#button-vps').attr('disabled', false);
                       $('.action_ticket').fadeOut(1500);
                       $('#button-vps').fadeOut();
                       $('#button-vps-finish').fadeIn();
                   } else {
                       var html = '<p class="text-danger">Xóa Ticket thất bại</p>';
                       $('#notication-invoice').html(html);
                       $('#button-vps').attr('disabled', false);
                   }
               },
               error: function (e) {
                  console.log(e);
                  $('#notication-invoice').html('<span class="text-danger">Truy vấn Ticket thất bại!</span>');
               }
            })
        })

        $('.checkbox_all').on('click', function () {
            if ( $(this).is(':checked') ) {
              $('.checkbox').prop('checked', this.checked);
              $('tbody tr').addClass('action');
            } else {
              $('.checkbox').prop('checked', this.checked);
              $('tbody tr').removeClass('action');
            }
        })

        // Lưu các checkbox vps
        $(document).on('click', '.checkbox' , function(e) {
            if( $(this).is(':checked') ) {
                $(this).closest('tr').addClass('action');
            } else {
                $(this).closest('tr').removeClass('action');
            }
            // console.log($vpsCheckbox);
            if (!lastChecked) {
                lastChecked = this;
                return;
            }
            if ( e.shiftKey ) {
                // console.log('da click 2');
                var start = $vpsCheckbox.index(this);
                var end = $vpsCheckbox.index(lastChecked);
                $vpsCheckbox.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
                $.each( $vpsCheckbox , function (index , value) {
                    if( $(this).is(':checked') ) {
                        $(this).closest('tr').addClass('action');
                    } else {
                        $(this).closest('tr').removeClass('action');
                    }
                })
            }
            lastChecked = this;
        });

        $('.btn-action').on('click', function () {
            var checked = $('.checkbox:checked');
            if ( checked.length > 0 ) {
              $('#modal-service').modal('show');
              $('#button-service').fadeIn();
              $('#button-finish').fadeOut();
              $('#notication-service').html('<span>Bạn có muốn xóa tất cả các Ticket vừa chọn không?</span>');
            }
            else {
              alert('Vui lòng chọn một Ticket.');
            }
        })

        $('#button-service').on('click', function () {
          var checked = $('.checkbox:checked');
          if ( checked.length > 0 ) {
              var list = [];
              $.each(checked, function(index, value) {
                  list.push($(this).val());
              })
              // console.log(list);
              var token = $('meta[name="csrf-token"]').attr('content');
              $.ajax({
                 url: '/admin/tickets/deleteAll',
                 type: 'post',
                 data: {list: list, _token: token},
                 dataType: 'json',
                 beforeSend: function(){
                     var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                     $('#button-service').attr('disabled', true);
                     $('#notication-service').html(html);
                 },
                 success: function (data) {
                     // console.log(data);
                     if (data) {
                         var html = '<p class="text-danger">Xóa Ticket thành công</p>';
                         $('#notication-service').html(html);
                         $('#notication-service').attr('disabled', false);
                         $('.action').fadeOut(1500);
                         $('#button-service').fadeOut();
                         $('#button-finish').fadeIn();
                     } else {
                         var html = '<p class="text-danger">Xóa Ticket thất bại</p>';
                         $('#notication-service').html(html);
                         $('#notication-service').attr('disabled', false);
                     }
                 },
                 error: function (e) {
                    console.log(e);
                    $('#notication-service').html('<span class="text-danger">Truy vấn Ticket thất bại!</span>');
                 }
              })
          }
          else {
            alert('Vui lòng chọn một Ticket.');
          }
        })

      })
    </script>
@endsection
