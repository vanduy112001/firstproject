@extends('layouts.app_mail')
@section('content')
    Xin chào, {{ $name }} - {{ $email }} <br><br>

    <p>Cloudzone admin đã có phản hồi mới về ticket #{{ $ticket_id }} của bạn với nội dung sau : </p>
    <p>`` {!! $content !!} ``</p>
    <p>Bạn vui lòng click vào link bên dưới để xem chi tiết và đưa ra các phản hồi</p>

    <div class="mail-button" style="padding-bottom: 25px;">
        <a href="{{ route('user.ticket.detail', ['id' => $ticket_id]) }}" style="color: blue;" target="_blank">
            Xem chi tiết
        </a>
    </div>

    <p>Trân trọng cám ơn.</p>
@endsection
@section('footer')
    <div style="text-align: center;line-height: 20px; font-size: 16px;font-family: Aria;color: rgb(16, 55, 132);">
        Email này được gửi đến {{ $name }} bởi vì bạn đã đăng ký tài khoản với Cloudzone. <br>
        Các đường link trong email này đều dẫn đến trang <a href="http://cloudzone.vn"
                                                                  style="text-decoration: none;">cloudzone.vn</a>.
    </div>
@endsection
