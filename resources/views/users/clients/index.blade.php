@extends('layouts.client.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
{{-- @section('title')
<div class="text-primary">Kính chào quí khách!</div>
@endsection --}}
@section('breadcrumb')
{{-- <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li> --}}
@endsection
@section('content')

<style>
    div#content {
        background: white;
    }
    .content-row {
    padding: 0;
    }
    .row.section-top {
        margin-top: -60px;
    }
    .section-top{
        background-image: url(../images/bg-banner-fb-fix2.png);
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .title-info h1 {
        color: #268fff;
        font-size: 40px;
        font-weight: 100;
        line-height: 61px;
        margin: 0px 0px 13px 0px;
        text-align: center;
        padding: 20px 30px;
        text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff;
    }
    .title-common h3 {
        text-align: center;
        color: #1E1666;
        font-size: 34px;
        font-weight: 300;
        line-height: 45px;
        letter-spacing: -0.3px;
        margin-top: 20px;
    }
    .title-common p {
        padding: 0 20px;
        text-align: justify;
    }
    .title-common ul {
        padding: 0 20px 0 40px;
        list-style: none;
        text-align: justify;
    }
    .title-common li {
        margin-bottom: 10px;
    }
    .title-common li i {
        color: #4CAF50;
    }
    @media only screen and (max-width: 767px) {
        .title-info h1 {
            padding-top: 50px;
        }
        .webdesign-banner {
            display: none;
        }
        /* .row.section-top {
            margin-top: -50px;
        } */
    }
</style>
<div class="row section-top">
    <div class="col-12 col-md-6 d-flex align-items-center">
        <div class="title-info">
            <h1>Cloudzone Customer Portal</h1>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <img class="webdesign-banner img-fluid" src="/../images/webdesign-banner.png" alt="">
    </div>
</div>
<div class="row section-content">
    <div class="col-12 col-md-6 d-flex align-items-center">
        <img class="img-fluid" src="/../images/multidevice.png" alt="">
    </div>
    <div class="col-12 col-md-6 ">
        <div class="title-common">
            <h3>Chào quý khách!!</h3>
            <p style="font-weight: bold;text-align: center;">
                Cloudzone chúc quý khách một ngày làm việc tốt đẹp              
            </p>
            <p>
                Nhằm đảm bảo sự thuận lợi, tương tác dễ dàng khi sử dụng các dịch vụ tại Cloudzone. Ban quản trị cho ra mắt sản phẩm Cloudzone Portal Customer, sản phẩm bao gồm những tính năng như sau:
            </p>
            <ul>
                <li><i class="fas fa-check-circle"></i> Tạo tài khoản để quản lí các dịch vụ của Cloudzone: Hosting, VPS, ...</li>
                <li><i class="fas fa-check-circle"></i> Thanh toán dịch vụ thông qua chuyển khoản, nạp tiền vào tài khoản Cloudzone Portal hoặc ví điện tử MoMo.</li>
                <li><i class="fas fa-check-circle"></i> Các dịch vụ (Hosting, VPS, ...) sau khi hoàn tất thanh toán sẽ được tạo tự động, thông tin được chuyển qua email và trang quản lí dịch vụ.</li>
                <li><i class="fas fa-check-circle"></i> Users có thể tạo ra danh sách khách hàng riêng để thuận tiện việc quản lí dịch vụ của mình.</li>
            </ul>
            <p>Trong quá trình sử dụng dịch vụ, chúng tôi rất mong nhận được ý kiến đóng góp của quý khách để sản phẩm của Cloudzone ngày càng hoàn thiện hơn.</p>
        <div class="text-center"><a href="{{ route('login') }}" class="btn btn-primary">Đăng ký ngay !</a></div>
        </div>
    </div>
</div>

@endsection
@section('scripts')

@endsection