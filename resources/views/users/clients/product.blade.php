@extends('layouts.client.app')
@section('title')
<div class="text-primary">Dịch vụ {{ !empty($group_product->title) ? $group_product->title : '' }}</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('client.index') }}"><i class="fa fa-users"></i> Client</a></li>
<li class="breadcrumb-item active"><i class="fa fa-cubes" aria-hidden="true"></i> Dịch vụ</li>
@endsection
@section('content')
<div class="row">
    {{-- <div class="col-md-12">
        <div class="header-lined">
            <h1>{{ $group_product->title }}</h1>
        </div>
    </div> --}}
    <div class="col col-md-12">
        <div class="show-product">
            <div class="row">
                @if (!empty($products))
                    @foreach ($products as $product)
                        <div class="col-xl-6 col-md-12 mb-4 card-deck">
                            <div class="card shadow h-100">
                                <div class="card-header py-3 text-center">
                                    <h5 class="m-0 font-weight-bold text-primary">{{ $product->name }}</h5>
                                </div>
                                <div class="card-body text-center">
                                    @if ($product->type_product == 'VPS' || $product->type_product == 'VPS-US')
                                        <div class="pricing-product">
                                            <h4 class="card-title pricing-card-title" style="float: none;">
                                                @if (!empty($product->pricing->one_time_pay))
                                                    {!!number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ Vĩnh viễn</small>
                                                @elseif (!empty($product->pricing->monthly))
                                                    {!!number_format($product->pricing->monthly,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 1 Tháng</small>
                                                @elseif (!empty($product->pricing->quarterly))
                                                    {!!number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 3 Tháng</small>
                                                @elseif (!empty($product->pricing->semi_annually))
                                                    {!!number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 6 Tháng</small>
                                                @elseif (!empty($product->pricing->annually))
                                                    {!!number_format($product->pricing->annually,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 1 Năm</small>
                                                @elseif (!empty($product->pricing->biennially))
                                                    {!!number_format($product->pricing->biennially,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 2 Năm</small>
                                                @elseif (!empty($product->pricing->triennially))
                                                    {!!number_format($product->pricing->triennially,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 3 Năm</small>
                                                @else
                                                    Free
                                                @endif
                                            </h4>
                                        </div>
                                        <div class="singlePrice">
                                            <ul class="list-unstyled ">
                                                <li>CPU: {{ !empty($product->meta_product->cpu) ? $product->meta_product->cpu : '0'  }} core</li>
                                                <li>Memory: {{ !empty($product->meta_product->memory) ?  $product->meta_product->memory : '0'  }} GB</li>
                                                <li>Disk SSD: {{ !empty($product->meta_product->disk) ? $product->meta_product->disk : '0'  }} GB</li>
                                                <li>Ip: {{ !empty($product->meta_product->ip) ? $product->meta_product->ip : '0'  }} IP public</li>
                                                <li>Hệ điều hành: {{ !empty($product->meta_product->os) ? $product->meta_product->os : 'Windows Server 2012'  }}</li>
                                                <li>Băng thông: {{ !empty($product->meta_product->bandwidth) ? $product->meta_product->bandwidth : '0'  }}</li>
                                            </ul>
                                        </div>
                                    @elseif ($product->type_product == 'Server')
                                        <div class="pricing-product">
                                            <h4 class="card-title pricing-card-title" style="float: none;">
                                                @if (!empty($product->pricing->one_time_pay))
                                                    {!!number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ Vĩnh viễn</small>
                                                @elseif (!empty($product->pricing->monthly))
                                                    {!!number_format($product->pricing->monthly,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 1 Tháng</small>
                                                @elseif (!empty($product->pricing->quarterly))
                                                    {!!number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 3 Tháng</small>
                                                @elseif (!empty($product->pricing->semi_annually))
                                                    {!!number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 6 Tháng</small>
                                                @elseif (!empty($product->pricing->annually))
                                                    {!!number_format($product->pricing->annually,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 1 Năm</small>
                                                @elseif (!empty($product->pricing->biennially))
                                                    {!!number_format($product->pricing->biennially,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 2 Năm</small>
                                                @elseif (!empty($product->pricing->triennially))
                                                    {!!number_format($product->pricing->triennially,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 3 Năm</small>
                                                @else
                                                    Free
                                                @endif
                                            </h4>
                                        </div>
                                        <div class="singlePrice">
                                            <ul class="list-unstyled ">
                                                <li>Máy chủ: {{ !empty($product->meta_product->name_server ) ? $product->meta_product->name_server  : '' }}</li>
                                                <li>CPU: {{ !empty($product->meta_product->cpu) ? $product->meta_product->cpu : '0'  }} core</li>
                                                <li>Memory: {{ !empty($product->meta_product->memory) ? $product->meta_product->memory : '0'  }} GB</li>
                                                <li>Disk SSD: {{ !empty($product->meta_product->disk) ? $product->meta_product->disk : '0'  }} GB</li>
                                                <li>Ip: {{ !empty($product->meta_product->ip) ? $product->meta_product->ip : '0'  }} IP public</li>
                                                <li>Băng thông: {{ !empty($product->meta_product->bandwidth) ? $product->meta_product->bandwidth : '0'  }}</li>
                                            </ul>
                                        </div>
                                    @elseif ( $product->type_product == 'Addon-VPS' || $product->type_product == 'Addon-Server' )
                                        <div class="pricing-product">
                                            <h4 class="card-title pricing-card-title" style="float: none;">
                                                @if (!empty($product->pricing->one_time_pay))
                                                    {!!number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ Vĩnh viễn</small>
                                                @elseif (!empty($product->pricing->monthly))
                                                    {!!number_format($product->pricing->monthly,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 1 Tháng</small>
                                                @elseif (!empty($product->pricing->quarterly))
                                                    {!!number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 3 Tháng</small>
                                                @elseif (!empty($product->pricing->semi_annually))
                                                    {!!number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 6 Tháng</small>
                                                @elseif (!empty($product->pricing->annually))
                                                    {!!number_format($product->pricing->annually,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 1 Năm</small>
                                                @elseif (!empty($product->pricing->biennially))
                                                    {!!number_format($product->pricing->biennially,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 2 Năm</small>
                                                @elseif (!empty($product->pricing->triennially))
                                                    {!!number_format($product->pricing->triennially,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 3 Năm</small>
                                                @else
                                                    Free
                                                @endif
                                            </h4>
                                        </div>
                                        <div class="singlePrice">
                                            <ul class="list-unstyled ">
                                                <li>CPU: {{ !empty($product->meta_product->cpu) ? $product->meta_product->cpu : '0'  }} core</li>
                                                <li>Memory: {{ !empty($product->meta_product->memory) ? $product->meta_product->memory : '0'  }} GB</li>
                                                <li>Disk SSD: {{ !empty($product->meta_product->disk) ? $product->meta_product->disk : '0'  }} GB</li>
                                                <li>Ip: {{ !empty($product->meta_product->ip) ? $product->meta_product->ip : '0'  }} IP public</li>
                                            </ul>
                                        </div>
                                    @elseif ( $product->type_product == 'Addon-Hosting')
                                    <div class="pricing-product">
                                        <h4 class="card-title pricing-card-title" style="float: none;">
                                            @if (!empty($product->pricing->one_time_pay))
                                                {!!number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'!!}
                                                <small class="text-muted">/ Vĩnh viễn</small>
                                            @elseif (!empty($product->pricing->monthly))
                                                {!!number_format($product->pricing->monthly,0,",",".") . ' VNĐ'!!}
                                                <small class="text-muted">/ 1 Tháng</small>
                                            @elseif (!empty($product->pricing->quarterly))
                                                {!!number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'!!}
                                                <small class="text-muted">/ 3 Tháng</small>
                                            @elseif (!empty($product->pricing->semi_annually))
                                                {!!number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'!!}
                                                <small class="text-muted">/ 6 Tháng</small>
                                            @elseif (!empty($product->pricing->annually))
                                                {!!number_format($product->pricing->annually,0,",",".") . ' VNĐ'!!}
                                                <small class="text-muted">/ 1 Năm</small>
                                            @elseif (!empty($product->pricing->biennially))
                                                {!!number_format($product->pricing->biennially,0,",",".") . ' VNĐ'!!}
                                                <small class="text-muted">/ 2 Năm</small>
                                            @elseif (!empty($product->pricing->triennially))
                                                {!!number_format($product->pricing->triennially,0,",",".") . ' VNĐ'!!}
                                                <small class="text-muted">/ 3 Năm</small>
                                            @else
                                                Free
                                            @endif
                                        </h4>
                                    </div>
                                    <div class="singlePrice">
                                        <ul class="list-unstyled ">
                                            <li>Dung lượng: {{ !empty( $product->meta_product->storage) ?  $product->meta_product->storage : '0'  }} GB</li>
                                            <li>Hosting: {{ !empty ($product->meta_product->domain) ? $product->meta_product->domain : '0'  }} domain</li>
                                        </ul>
                                    </div>
                                    @else
                                        <div class="pricing-product">
                                            <h4 class="card-title pricing-card-title" style="float: none;">
                                                @if (!empty($product->pricing->one_time_pay))
                                                    {!!number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ Vĩnh viễn</small>
                                                @elseif (!empty($product->pricing->monthly))
                                                    {!!number_format($product->pricing->monthly,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 1 Tháng</small>
                                                @elseif (!empty($product->pricing->quarterly))
                                                    {!!number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 3 Tháng</small>
                                                @elseif (!empty($product->pricing->semi_annually))
                                                    {!!number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 6 Tháng</small>
                                                @elseif (!empty($product->pricing->annually))
                                                    {!!number_format($product->pricing->annually,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 1 Năm</small>
                                                @elseif (!empty($product->pricing->biennially))
                                                    {!!number_format($product->pricing->biennially,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 2 Năm</small>
                                                @elseif (!empty($product->pricing->triennially))
                                                    {!!number_format($product->pricing->triennially,0,",",".") . ' VNĐ'!!}
                                                    <small class="text-muted">/ 3 Năm</small>
                                                @else
                                                    Free
                                                @endif
                                            </h4>
                                        </div>
                                        <div class="singlePrice">
                                            <ul class="list-unstyled ">
                                                <li>Dung lượng: {{ !empty( $product->meta_product->storage) ?  $product->meta_product->storage : '0'  }} MB</li>
                                                <li><b>Unlimited Data Transfer</b></li>
                                                <li>Hosting: {{ !empty ($product->meta_product->domain) ? $product->meta_product->domain : '0'  }} domain</li>
                                                <li>Sub Domain: {{ !empty($product->meta_product->sub_domain) ? $product->meta_product->sub_domain: '0'  }}</li>
                                                <li>Alias Domain: {{ !empty($product->meta_product->alias_domain) ? $product->meta_product->alias_domain : '0'  }}</li>
                                                <li>Database: {{ !empty($product->meta_product->database) ? $product->meta_product->database : '0'  }} database</li>
                                                <li>FTP: {{ !empty($product->meta_product->ftp) ? $product->meta_product->ftp : '0'  }} tài khoản</li>
                                                <li>Control Panel: {{ !empty($product->meta_product->panel) ? $product->meta_product->panel : ''  }}</li>
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                                <div class="card-footer text-center">
                                    <a href="{{ route('user.addCard', $product->id) }}" class="btn btn-primary btn-icon-split">
                                        <span class="icon text-white-50"><i class="fas fa-shopping-cart"></i></span> 
                                        <span class="text">Đăng ký ngay</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
