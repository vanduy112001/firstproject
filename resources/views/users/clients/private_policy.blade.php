@extends('layouts.client.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
{{-- @section('title')
<div class="text-primary">Kính chào quí khách!</div>
@endsection --}}
@section('breadcrumb')
{{-- <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li> --}}
@endsection
@section('content')

<style>
    div#content {
        background: white;
    }
    .content-row {
    padding: 0;
    }
    .row.section-top {
        margin-top: -60px;
    }
    .section-top{
        background-image: url(../images/bg-banner-fb-fix2.png);
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .title-info h1 {
        color: #268fff;
        font-size: 40px;
        font-weight: 100;
        line-height: 61px;
        margin: 0px 0px 13px 0px;
        text-align: center;
        padding: 20px 30px;
        text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff;
    }
    .title-common h3 {
        text-align: center;
        color: #1E1666;
        font-size: 34px;
        font-weight: 300;
        line-height: 45px;
        letter-spacing: -0.3px;
        margin-top: 20px;
    }
    .title-common p {
        padding: 0 20px;
        text-align: justify;
    }
    .title-common ul {
        padding: 0 20px 0 40px;
        list-style: none;
        text-align: justify;
    }
    .title-common li {
        margin-bottom: 10px;
    }
    .title-common li i {
        color: #4CAF50;
    }
    @media only screen and (max-width: 767px) {
        .title-info h1 {
            padding-top: 50px;
        }
        .webdesign-banner {
            display: none;
        }
        /* .row.section-top {
            margin-top: -50px;
        } */
    }
</style>
<div class="row section-top">
    <div class="col-12 col-md-6 d-flex align-items-center">
        <div class="title-info">
            <h1>Cloudzone Customer Portal</h1>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <img class="webdesign-banner img-fluid" src="/../images/webdesign-banner.png" alt="">
    </div>
</div>
<div class="row section-content m-4">
    <div class="col-12 text-center">
      <h3>Chính sách về quyền riêng tư – sử dụng dữ liệu cá nhân</h3>
    </div>
    <div class="col-12 policy-body">
      <div class="policy-container">
        <h4>Giới thiệu</h4>
        <p>
          Trang web này được điều hành bởi Cloudzone (Công ty TNHH Đại Việt Số).
          Chúng tôi rất coi trọng việc bảo mật dữ liệu và tuân thủ nghiêm ngặt luật bảo vệ dữ liệu.
          Chính sách quyền riêng tư này giải thích cách chúng tôi thu thập, lưu trữ, sử dụng và tiết lộ bất kỳ dữ liệu cá nhân nào mà
          chúng tôi thu thập về quý vị khi quý vị sử dụng trang web này, cũng như cách chúng tôi bảo vệ quyền riêng tư và bảo mật
          thông tin cá nhân của quý vị. Quyền riêng tư của quý vị quan trọng với chúng tôi vì vậy dù quý vị mới sử dụng dịch vụ của
          chúng tôi hay là người dùng lâu năm, vui lòng dành thời gian để tìm hiểu các thông lệ của chúng tôi - và liên hệ với
          chúng tôi nếu quý vị có bất kỳ thắc mắc nào.
        </p>
        <p>
          Chúng tôi cũng không dùng cho bất kỳ mục đích nào khác ngoài việc giúp giám sát lượng truy cập trên trang web của chúng tôi,
          truy vấn dịch vụ, quản lý dịch vụ,...
          hoặc (trong trường hợp có hoạt động tội phạm hay lạm dụng thông tin của chúng tôi) phối hợp với cơ quan thực thi pháp luật.
        </p>
        <h4>Dữ liệu cá nhân chúng tôi thu thập</h4>
        <p>
          Chúng tôi nhận và lưu trữ mọi thông tin mà bạn nhập vào website của chúng tôi hoặc cung cấp cho chúng tôi theo hình thức khác.
          Thông tin này bao gồm những thông tin có thể dùng để nhận dạng bạn với tư cách là một cá nhân hoặc để liên hệ trực tiếp với bạn
          (“thông tin cá nhân”).Thông tin cá nhân bao gồm thông tin mà bạn cung cấp cho chúng tôi như họ và tên, ngày sinh, giới tính,
          số điện thoại, địa chỉ gửi thư và địa chỉ thư điện tử,… Các thông tin cá nhân này chúng tôi phục vụ cho các dịch vụ của quý Khách
          và liên hệ quý khách khi quy khách sử dụng các dịch vụ của chúng tôi (Như gửi email dịch vụ, gọi điện thoại tư vấn,...).
        </p>
        <h4>Dữ liệu cá nhân nhạy cảm chúng tôi thu thập</h4>
        <p>
          Trong một số trường hợp, Chúng tôi nhận và lưu trữ một số thông tin nhạy cảm của quý khách nhằm phục vụ cho các hoạt động
          của quý khách trên website và theo quy định hiện hành của pháp luật.
        </p>
        <p>
          Trước khi xử lý các dữ liệu cá nhân nhạy cảm về quý vị, chúng tôi sẽ xin sự chấp thuận của quý vị.
          Do đó, chúng tôi yêu cầu quý vị sử dụng các biểu mẫu liên hệ chuyên dụng trên trang web của chúng tôi để gửi bất kỳ
          dữ liệu nhạy cảm nào. Các biểu mẫu liên hệ cho phép quý vị cung cấp cho chúng tôi sự chấp thuận được yêu cầu theo luật
          bảo vệ dữ liệu hiện hành. Tất nhiên quý vị có thể rút lại sự chấp thuận này bất cứ lúc nào. Chúng tôi sẽ không xử lý bất
          kỳ dữ liệu cá nhân nhạy cảm nào không được quý vị cho phép, hoặc quý vị chưa cung cấp cho chúng tôi.
        </p>
        <h4>Mục đích của việc thu thập dữ liệu của bạn</h4>
        <p>
          Một trong những mục đích của trang web của chúng tôi là để thông báo cho bạn biết chúng tôi là ai và những công việc
          chúng tôi làm. Chúng tôi thu thập và sử dụng thông tin cá nhân của bạn để tư vấn, cung cấp cho bạn các dịch vụ
          hoặc thông tin cần thiết một cách tốt hơn. Do đó, chúng tôi sẽ sử dụng thông tin cá nhân của bạn để:
        </p>
        <ul>
          <li>Tư vấn giải pháp và dịch vụ mà bạn cần</li>
          <li>Hồi đáp lại các thắc mắc hoặc yêu cầu mà bạn gửi</li>
          <li>Tạo ra các dịch vụ và giải pháp có thể đáp ứng nhu cầu của bạn</li>
          <li>Giải quyết các vấn đề với bất kỳ dịch vụ nào đã cung cấp cho bạn</li>
        </ul>
        <p>
          Chúng tôi tôn trọng quyền riêng tư của bạn, chúng tôi sẽ chỉ sử dụng dữ liệu cá nhân của bạn cho mục đích này
          khi bạn biết về mục đích sử dụng đó và nếu cần, chúng tôi sẽ yêu cầu sự đồng ý của bạn trước khi sử dụng dữ liệu
          cá nhân của bạn.
        </p>
        <p>
          Ngoài ra, vào bất kỳ thời điểm nào, nếu bạn muốn chúng tôi ngừng sử dụng thông tin của bạn cho bất kỳ hoặc tất cả những mục đích trên, vui lòng liên hệ với chúng tôi theo phương thức đã nêu ở dưới. Chúng tôi sẽ ngừng việc sử dụng thông tin của bạn cho các mục đích như vậy ngay khi có khả năng hợp lý để làm điều đó.
          Ngoại trừ những nội dung đã trình bày trong chính sách về quyền riêng tư này, chúng tôi sẽ không tiết lộ bất cứ thông tin nhận dạng cá nhân nào mà không có sự cho phép của bạn trừ khi chúng tôi có quyền hợp pháp hoặc bắt buộc phải làm như vậy (ví dụ: khi quy trình pháp lý bắt buộc chúng tôi phải làm như vậy hoặc phục vụ mục đích của công tác phòng chống gian lận hoặc tội phạm khác) hoặc nếu chúng tôi tin rằng hành động đó là cần thiết để bảo vệ quyền, tài sản hoặc an toàn cá nhân của chúng tôi và của người dùng/khách hàng hoặc cá nhân khác của chúng tôi.
          Xin hãy an tâm rằng chúng tôi sẽ không sử dụng thông tin của bạn vì bất kỳ mục đích nào nếu bạn đã chỉ rõ rằng bạn không muốn chúng tôi sử dụng thông tin của bạn theo cách này vào thời điểm bạn gửi thông tin hoặc vào một thời điểm sau đó.
        </p>
        <h4>Các quyền của quý vị</h4>
        <p>
          Theo luật bảo vệ dữ liệu hiện hành, quý vị có một số quyền nhất định với tư cách là “đối tượng dữ liệu”.
          Dưới đây, chúng tôi đã liệt kê các quyền của quý vị. Các quyền của quý vị bao gồm:
        </p>
        <ol>
          <li><b>Quyền truy cập</b> – Quý vị có quyền truy cập dữ liệu cá nhân mà chúng tôi xử lý. Quý vị cũng được quyền nhận các thông tin nhất định về những điều chúng tôi làm với dữ liệu cá nhân. Những thông tin này được cung cấp trong tài liệu này.</li>
          <li><b>Quyền sửa đổi</b> – Trong một số trường hợp nhất định, quý vị có quyền sửa các dữ liệu cá nhân không chính xác liên quan đến quý vị và hoàn thành các dữ liệu cá nhân chưa đầy đủ.</li>
          <li><b>Quyền xóa bỏ</b> – Trong một số trường hợp nhất định, quý vị có quyền xóa dữ liệu cá nhân của mình. Đây được gọi là “quyền được quên”.</li>
          <li><b>Quyền di chuyển dữ liệu</b> – Quý vị có quyền nhận dữ liệu cá nhân của mình (hoặc để dữ liệu cá nhân được truyền trực tiếp đến một đơn vị kiểm soát dữ liệu khác) theo định dạng có cấu trúc, thường được sử dụng và có thể đọc được từ chúng tôi.</li>
          <li><b>Quyền phản đối</b> – Quý vị có quyền phản đối một số loại hình xử lý dữ liệu cá nhân mà chúng tôi thực hiện. Điều này áp dụng cho tất cả các hoạt động dựa trên "lợi ích hợp pháp" của chúng tôi.</li>
        </ol>
        <p>Cuối cùng, quý vị cũng có quyền khiếu nại với cơ quan giám sát bảo vệ dữ liệu hiện hành.</p>
        <h4>Thông tin liên hệ</h4>
        <ul>
          <li>Địa chỉ: 257 Lê Duẩn , Đà Nẵng.</li>
          <li>Văn phòng: 67 Nguyễn Thị Định, Đà Nẵng.</li>
          <li>Điện thoại: 08 8888 0043</li>
          <li>Website: <a href="https://cloudzone.vn ">https://cloudzone.vn </a></span></li>
          <li>Email: <a href="mailto:support@cloudzone.vn">support@cloudzone.vn</a></li>
          <li>Fanpage: <a href="https://www.facebook.com/cloudzone.vn">https://www.facebook.com/cloudzone.vn</a></li>
        </ul>
      </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection
