@extends('layouts.client.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
{{-- @section('title')
<div class="text-primary">Kính chào quí khách!</div>
@endsection --}}
@section('breadcrumb')
{{-- <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li> --}}
@endsection
@section('content')

<style>
    div#content {
        background: white;
    }
    .content-row {
    padding: 0;
    }
    .row.section-top {
        margin-top: -60px;
    }
    .section-top{
        background-image: url(../images/bg-banner-fb-fix2.png);
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
    }
    .title-info h1 {
        color: #268fff;
        font-size: 40px;
        font-weight: 100;
        line-height: 61px;
        margin: 0px 0px 13px 0px;
        text-align: center;
        padding: 20px 30px;
        text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff;
    }
    .title-common h3 {
        text-align: center;
        color: #1E1666;
        font-size: 34px;
        font-weight: 300;
        line-height: 45px;
        letter-spacing: -0.3px;
        margin-top: 20px;
    }
    .title-common p {
        padding: 0 20px;
        text-align: justify;
    }
    .title-common ul {
        padding: 0 20px 0 40px;
        list-style: none;
        text-align: justify;
    }
    .title-common li {
        margin-bottom: 10px;
    }
    .title-common li i {
        color: #4CAF50;
    }
    @media only screen and (max-width: 767px) {
        .title-info h1 {
            padding-top: 50px;
        }
        .webdesign-banner {
            display: none;
        }
        /* .row.section-top {
            margin-top: -50px;
        } */
    }
</style>
<div class="row section-top">
    <div class="col-12 col-md-6 d-flex align-items-center">
        <div class="title-info">
            <h1>Cloudzone Customer Portal</h1>
        </div>
    </div>
    <div class="col-12 col-md-6">
        <img class="webdesign-banner img-fluid" src="/../images/webdesign-banner.png" alt="">
    </div>
</div>
<div class="row section-content m-4">
    <div class="col-12 text-center">
      <h3>Chính sách về Điều khoản và quy định dịch vụ</h3>
    </div>
    <div class="col-12 policy-body">
      <div class="policy-container">
        <h4>Giới thiệu</h4>
        <p>
          Các điều khoản sau đây ("Điều khoản dịch vụ") mô tả các điều khoản và điều kiện áp dụng cho việc bạn truy cập và sử dụng ứng dụng,
          dịch vụ cũng như bất kỳ phần mềm và sản phẩm liên quan nào. Tài liệu này là một thỏa thuận ràng buộc về mặt pháp lý giữa bạn và Công ty
          TNHH Đại Việt Số.
        </p>
        <p>
          Các Điều khoản Dịch vụ này áp dụng đối với tất cả các Dịch vụ được sở hữu và cung cấp bởi Cloudzone, bao gồm: VPS Việt Nam,
          VPS nước ngoài, Hosting, Domain,... Bằng cách đăng ký Dịch vụ, bạn đồng ý bị ràng buộc bởi Điều khoản Dịch vụ. Bất kỳ tính năng hoặc
          công cụ mới nào được thêm vào Dịch vụ hiện tại cũng phải tuân theo Điều khoản Dịch vụ. Chúng tôi có quyền cập nhật và
          thay đổi Điều khoản Dịch vụ bằng cách đăng các cập nhật và thay đổi. Bạn nên kiểm tra Điều khoản Dịch vụ thường xuyên
          để nhận biết bất kỳ cập nhật hoặc thay đổi nào có thể ảnh hưởng đến bạn.
        </p>
        <h4>Định nghĩa</h4>
        <p>
          Nội dung của khách hàng có nghĩa là tất cả thông tin và dữ liệu hoặc bất kỳ nội dung nào khác ở bất kỳ phương tiện
          hoặc định dạng nào do bạn cung cấp hoặc thay mặt bạn cung cấp liên quan đến việc bạn sử dụng Dịch vụ,
          bao gồm thông tin và dữ liệu có sẵn trong Tài khoản của bạn, nó có thể bao gồm thông tin hoặc dữ liệu từ Nền
          Tảng Được Hỗ Trợ được cài đặt trên Tài khoản của bạn.
        </p>
        <h4>Tài khoản Cloudzone</h4>
        <ul>
          <li>
            Để truy cập và sử dụng Dịch vụ, bạn phải đăng nhập bằng thông tin đăng nhập Tài khoản của bạn,
            bao gồm địa chỉ email của bạn và bất kỳ thông tin nào khác được xác định là bắt buộc.
            Bạn đồng ý duy trì thông tin chính xác, đầy đủ và cập nhật cho Tài khoản của mình.
          </li>
          <li>
            Là người dùng Tài khoản Cloudzone, bạn cũng phải tuân theo Điều khoản dịch vụ,
            Chính sách bảo mật của Cloudzone, và bất kỳ điều khoản áp dụng nào khác.
             Nếu có bất kỳ xung đột hoặc mâu thuẫn nào giữa Điều khoản Dịch vụ Cloudzone, Chính sách bảo mật,
              các thỏa thuận sẽ chỉ áp dụng theo thứ tự sau trong phạm vi cần thiết để giải quyết bất kỳ xung đột hoặc mâu thuẫn nào.
          </li>
          <li>
            Bạn xác nhận và đồng ý rằng Dịch vụ sẽ có thể truy cập và sửa đổi dữ liệu lưu trữ cho Tài khoản Cloudzone của bạn,
            bao gồm thông tin từ các Nền tảng được hỗ trợ.
          </li>
          <li>
            Nếu Tài khoản Cloudzone của bạn bị chấm dứt vì bất kỳ lý do gì, quyền truy cập của bạn vào Dịch vụ cũng sẽ bị chấm dứt.
          </li>
        </ul>
        <h4>Điều khoản chung</h4>
        <ul>
          <li>Hỗ trợ kỹ thuật có sẵn thông qua Fanpage <a href="https://www.facebook.com/cloudzone.vn">https://www.facebook.com/cloudzone.vn</a> </li>
          <li>Dịch vụ hiện chỉ được cung cấp bằng tiếng Anh và tiếng Việt.</li>
          <li>Bạn xác nhận và đồng ý rằng chúng tôi có thể sửa đổi các Điều khoản Dịch vụ này bất cứ lúc nào bằng cách đăng Điều khoản Dịch vụ được sửa đổi và chỉnh sửa có liên quan</li>
          <li>Bạn không được sử dụng Dịch vụ cho bất kỳ mục đích bất hợp pháp hoặc trái phép nào cũng như khi sử dụng Dịch vụ, bạn không được vi phạm bất kỳ luật nào trong khu vực pháp lý mà bạn hoạt động.</li>
          <li>Bạn không được sử dụng Dịch vụ khi vi phạm các điều khoản và điều kiện áp dụng cho bất kỳ Nền tảng được Hỗ trợ nào mà bạn có thể sử dụng liên quan đến Dịch vụ. Nếu chúng tôi được thông báo về việc bạn vi phạm các điều khoản của Nền tảng được hỗ trợ, chúng tôi có thể, theo quyết định riêng và tuyệt đối của chúng tôi, chấm dứt quyền truy cập của bạn vào Dịch vụ.</li>
          <li>Bạn đồng ý không tái dựng, nhân bản, sao chép, mua bán, hoặc khai thác bất kỳ phần nào của Dịch vụ, việc sử dụng Dịch vụ hoặc truy cập vào Dịch vụ mà không có sự cho phép rõ ràng bằng văn bản của chúng tôi.</li>
        </ul>
        <h4>Quyền hạn của chúng tôi</h4>
        <ul>
          <li>
            Chúng tôi có quyền sửa đổi, bao gồm nhưng không giới hạn ở việc thêm hoặc bớt các tính năng, ngừng hoặc chấm dứt Dịch vụ hoặc bất kỳ phần nào trong đó, hoặc chấm dứt Tài khoản Cloudzone của bạn hay quyền truy cập của bạn vào Dịch vụ,
          </li>
          <li>
            Chúng tôi có quyền từ chối Dịch vụ cho bất kỳ ai vì bất kỳ lý do gì bất cứ lúc nào.
          </li>
          <li>
            Chúng tôi có thể, nhưng không có nghĩa vụ, xóa mà không cần thông báo bất kỳ Nội dung hoặc Nhận xét của Khách hàng nào
            mà chúng tôi xác định là bất hợp pháp theo quyết định riêng của chúng tôi như xúc phạm, đe dọa, bôi nhọ, phỉ báng, khiêu dâm, tục tĩu, phản cảm,
            vi phạm tài sản trí tuệ của bất kỳ bên nào,...
          </li>
          <li>
            Việc vi phạm bất kỳ điều khoản nào của Điều khoản Dịch vụ, được xác định theo quyết định riêng của chúng tôi, có thể dẫn đến việc chấm dứt ngay lập tức quyền truy cập của bạn vào Dịch vụ.
          </li>
          <li>
            Việc lạm dụng bằng lời nói hoặc bằng văn bản dưới bất kỳ hình thức nào (bao gồm cả các mối đe dọa lạm dụng hoặc trả thù) đối với bất kỳ nhân viên, thành viên Cloudzone nào sẽ dẫn đến việc chấm dứt dịch vụ ngay lập tức.
          </li>
          <li>
            Chúng tôi có quyền, nhưng không bắt buộc, để giới hạn tính khả dụng của Dịch vụ, hoặc các sản phẩm, dịch vụ được cung cấp thông qua Dịch vụ, cho bất kỳ người nào, khu vực địa lý, phạm vi pháp lý nào. Chúng tôi có thể thực hiện quyền này trong từng trường hợp cụ thể.
          </li>
          <li>
            Chúng tôi có quyền cung cấp Dịch vụ của chúng tôi cho các đối thủ cạnh tranh của bạn và không hứa hẹn độc quyền trong bất kỳ phân khúc thị trường cụ thể nào.
          </li>
        </ul>
        <h4>Các quyền của quý vị</h4>
        <p>
          Theo luật bảo vệ dữ liệu hiện hành, quý vị có một số quyền nhất định với tư cách là “đối tượng dữ liệu”.
          Dưới đây, chúng tôi đã liệt kê các quyền của quý vị. Các quyền của quý vị bao gồm:
        </p>
        <ol>
          <li><b>Quyền truy cập</b> – Quý vị có quyền truy cập dữ liệu cá nhân mà chúng tôi xử lý. Quý vị cũng được quyền nhận các thông tin nhất định về những điều chúng tôi làm với dữ liệu cá nhân. Những thông tin này được cung cấp trong tài liệu này.</li>
          <li><b>Quyền sửa đổi</b> – Trong một số trường hợp nhất định, quý vị có quyền sửa các dữ liệu cá nhân không chính xác liên quan đến quý vị và hoàn thành các dữ liệu cá nhân chưa đầy đủ.</li>
          <li><b>Quyền xóa bỏ</b> – Trong một số trường hợp nhất định, quý vị có quyền xóa dữ liệu cá nhân của mình. Đây được gọi là “quyền được quên”.</li>
          <li><b>Quyền di chuyển dữ liệu</b> – Quý vị có quyền nhận dữ liệu cá nhân của mình (hoặc để dữ liệu cá nhân được truyền trực tiếp đến một đơn vị kiểm soát dữ liệu khác) theo định dạng có cấu trúc, thường được sử dụng và có thể đọc được từ chúng tôi.</li>
          <li><b>Quyền phản đối</b> – Quý vị có quyền phản đối một số loại hình xử lý dữ liệu cá nhân mà chúng tôi thực hiện. Điều này áp dụng cho tất cả các hoạt động dựa trên "lợi ích hợp pháp" của chúng tôi.</li>
        </ol>
        <p>Cuối cùng, quý vị cũng có quyền khiếu nại với cơ quan giám sát bảo vệ dữ liệu hiện hành.</p>
        <h4>Thông tin liên hệ</h4>
        <ul>
          <li>Địa chỉ: 257 Lê Duẩn , Đà Nẵng.</li>
          <li>Văn phòng: 67 Nguyễn Thị Định, Đà Nẵng.</li>
          <li>Điện thoại: 08 8888 0043</li>
          <li>Website: <a href="https://cloudzone.vn ">https://cloudzone.vn </a></span></li>
          <li>Email: <a href="mailto:support@cloudzone.vn">support@cloudzone.vn</a></li>
          <li>Fanpage: <a href="https://www.facebook.com/cloudzone.vn">https://www.facebook.com/cloudzone.vn</a></li>
        </ul>
      </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection
