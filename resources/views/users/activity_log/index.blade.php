@extends('layouts.user2.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('/libraries/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
@endsection
@section('title')
    <div class="text-primary">Quản lý nhật ký hoạt động</div>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
    <li class="breadcrumb-item active">Nhật ký hoạt động</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-12" id="user_fillter">
          <button type="button" name="button" class="btn btn-primary btn-sm btn_filter"><i class="fas fa-clipboard-list"></i> Lọc nhật ký</button>
        </div>
        <div class="col-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead class="info">
                            <th width="20%">Thời gian</th>
                            <th width="20%">Hành động</th>
                            <th>Ghi chú</th>
                            <th width="20%">Dịch vụ</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="{{ url('js/user_log.js') }}"></script>
@endsection
