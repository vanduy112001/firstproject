@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    <i class="fa fa-paste bg-c-blue"></i> Tạo hợp đồng
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
    <li class="breadcrumb-item active"><i class="fas fa-paste"></i> Tạo hợp đồng</li>
@endsection
@section('content')
    <div class="title">
        <div class="title-body">
            <div class="row">
                <div class="col-md-12 detail-course-finish">
                    @if(session("success"))
                        <div class="bg-success">
                            <p class="text-light">{{session("success")}}</p>
                        </div>
                    @elseif(session("fails"))
                        <div class="bg-danger">
                            <p class="text-light">{{session("fails")}}</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>  
    <div class="box box-primary">
        <form action="{{ route('user.contract.create_data_contract') }}" method="post">
            <div class="box-body">
                    @csrf
                    <div class="row">
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="company">Công ty</label>
                                <input type="text" id="company" name="company" value="{{ old('company') ? old('company') : $user_detail->user_meta->company }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="deputy">Người đại diện</label>
                                <input type="text" id="deputy" name="deputy" value="{{ old('deputy') ? old('deputy') : $user_detail->name }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="deputy_position">Chức vụ người đại diện</label>
                                <input type="text" id="deputy_position" name="deputy_position" value="{{ old('deputy_position') }}" class="form-control">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="address">Địa chỉ</label>
                                <input type="text" id="address" name="address" value="{{ old('address') ? old('address') : $user_detail->user_meta->address }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" id="email" name="email" value="{{ old('email') ? old('email') : $user_detail->email }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="phone">Số điện thoại</label>
                                <input type="text" id="phone" name="phone" value="{{ old('phone') ? old('phone') : $user_detail->user_meta->phone }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="form-group">
                                <label for="mst">Mã số thuế</label>
                                <input type="text" id="mst" name="mst" value="{{ old('mst') ?  old('mst') : $user_detail->user_meta->mst }} " class="form-control">
                            </div>
                        </div>
                    </div>
                
            </div>
            <div class="card-footer">
                <a href="{{ route('user.contract.index') }}" class="btn btn-sm btn-secondary"><i class="fas fa-angle-double-left"></i> Back</a>
                <input type="submit" class="btn btn-primary btn-sm float-right" value="Tạo hợp đồng">
            </div>
        </form>
    </div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript">

</script>

@endsection
