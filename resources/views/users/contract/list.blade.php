@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    <i class="fa fa-paste bg-c-blue"></i> Hợp đồng
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
    <li class="breadcrumb-item active"><i class="fas fa-paste"></i> Hợp đồng</li>
@endsection
@section('content')
    <div class="title">
        <div class="title-body">
            <div class="row">
                <div class="col-md-12 detail-course-finish">
                    @if(session("success"))
                        <div class="bg-success">
                            <p class="text-light">{{session("success")}}</p>
                        </div>
                    @elseif(session("fails"))
                        <div class="bg-danger">
                            <p class="text-light">{{session("fails")}}</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>  
    <div class="box box-primary">
        <div class="box-body">
            <div>
                <a class="btn btn-primary mb-2" href="{{ route('user.contract.create') }}"><i class="fas fa-paste"></i> Gửi yêu cầu hợp đồng</a>
            </div>
            <div class="dataTables_wrapper form-inline dt-bootstrap">
                <table class="table table-bordered">
                    <thead class="primary">
                        <th>Hợp đồng</th>
                        <th>Ngày tạo</th>
                        <th>Trạng thái</th>
                        <th>Action</th>
                    </thead>
                    <tbody>
                        @if ($contracts->count() > 0)
                            @foreach ($contracts as $key => $contract)
                                <tr>
                                    <td>
                                        @if ($contract->status === 'pending')
                                            <a href="{{ route( 'user.contract.detail', ['id' => $contract->id ] ) }}" title="Update"><span class="btn btn-outline-danger btn-sm mb-1"><i class="fa fa-times-circle"></i> Đang chờ xét duyệt</span></a>
                                        @else
                                            {{ !empty($contract->title) ? $contract->title : '' }} / Số HĐ: {{ !empty($contract->contract_number) ? $contract->contract_number : '' }}
                                        @endif
                                        
                                    </td>
                                    <td>{{ !empty($contract->created_at) ? date('d-m-Y', strtotime($contract->created_at)) : '' }}</td>
                                    <td>
                                        @if (!empty($contract->status))
                                            @if ($contract->status === "pending")
                                                <span class="btn btn-outline-danger btn-sm mb-1"><i class="fa fa-times-circle"></i> Pending</span>
                                            @elseif($contract->status === "active")
                                                <span class="btn btn-outline-success btn-sm mb-1"><i class="fa fa-check-circle"></i> Active</span>
                                            @elseif($contract->status === "cancel")
                                                <span class="btn btn-outline-warning btn-sm mb-1"><i class="fa fa-exclamation-triangle"></i> Cancel</span>
                                            @endif
                                        @endif
                                    </td>
                                    <td>
                                        @if ($contract->status === 'pending')
                                            <a class="btn btn-warning btn-sm mb-1 update-contract" href="{{ route( 'user.contract.detail', ['id' => $contract->id ] ) }}" title="Chi tiết"><i class="fas fa-edit"></i></a>
                                        @elseif ($contract->status === 'cancel')
                                            <a class="btn btn-danger btn-sm mb-1 update-contract" href="" title="Hợp đồng bị hủy"><i class="fas fa-ban"></i></a>
                                        @else
                                            <a class="btn btn-primary btn-sm mb-1 view-contract" href="{{ route( 'user.contract.print_pdf', [ 'id' => $contract->id ] ) }}" target="_blank" title="View"><i class="fa fa-eye"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <td colspan="5" class="text-danger text-center">Không có dữ liệu</td>
                        @endif
                    </tbody>
                    <tfoot class="card-footer clearfix">
                        <td colspan="5" class="text-center">
                            {{ $contracts->links()  }}
                        </td>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript">

</script>

@endsection
