@extends('layouts.user.app_login')
@section('content')
<div class="row">
    <div class="col" style="margin: auto;">
        @if(session("success"))
        <div class="alert alert-success">
            {{session("success")}}
        </div>
        @elseif(session("fails"))
        <div class="alert alert-danger">
            {{session("fails")}}
        </div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul style="margin-bottom: 0;">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
<div class="p-3 pt-sm-4 pb-sm-4 px-sm-4">
    <div class="pt-4 pt-sm-0 text-center">
        <h1 class="h4 h4 text-primary mb-2">Bạn đã quên mật khẩu?</h1>
        <p class="mb-4">Vui lòng nhập email đã đăng ký, chúng tôi sẽ gửi đường dẫn khôi phục lại mật khẩu qua email này.
        </p>
        <p><img class="gioi-thieu" src="{{ url('images/logo-bg-login.png') }}" alt=""></p>
    </div>
    <form class="user" action="{{ route('send_email_forgot') }}" method="post">
        @csrf
        <div class="form-group">
            <input type="email" class="form-control rounded-pill" id="inputEmail" name="email"
                aria-describedby="emailHelp" placeholder="Nhập email..." value="{{ old('email') }}" required>
        </div>
        <div>
            <button type="submit" class="btn btn-primary btn-block rounded-pill">Gửi Email xác nhận</button>
        </div>
    </form>
    <hr>
    <div class="mb-5">
        <div class="text-center float-left">
            <a class="small" href="{{ route('register') }}">Tạo tài khoản</a>
        </div>
        <div class="text-center float-right">
            <a class="small" href="{{ route('login') }}">Đăng nhập</a>
        </div>
    </div>
    <div class="text-center">
        {{-- <p class="mb-2 small text-secondary">Liên hệ</p> --}}
        <div class="contact-icon">
            <a href="https://www.facebook.com/cloudzone.vn" target="_blank"><button class="btn btn-default btn-icon border-primary text-primary mr-1"><i class="fab fa-facebook-f"></i></button></a>
            <a href="tel:0236-4455-789"><button class="btn btn-default btn-icon border-success text-success mr-1"><i class="fas fa-phone"></i></button></a>
            <a href="mailto:support@cloudzone.vn"><button class="btn btn-default btn-icon border-danger text-danger"><i class="fas fa-envelope"></i></button></a>
        </div>
    </div>
</div>
@endsection