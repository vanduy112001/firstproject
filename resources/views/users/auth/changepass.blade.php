@php
$user = UserHelper::get_user(Auth::user()->id);
@endphp
@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" type="text/css"
    href="{{ url('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">

@endsection
@section('title')

@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> {{ $user->name }}</li>
@endsection
@section('content')
<!-- Content Row -->
<div class="row">
    <div class="col-md-6" style="margin: auto;">
        @if(session("success"))
        <div class="alert alert-success">
            {{session("success")}}
        </div>
        @elseif(session("fails"))
        <div class="alert alert-danger">
            {{session("fails")}}
        </div>
        @endif
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul style="margin-bottom: 0;">
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-6" style="margin: auto;">
        {{-- <div class="box box-primary">
            <div class="box-body">
                <h3 style="font-size: 20px;text-transform: uppercase;margin-top: 14px;font-weight: 600">Thay đổi mật khẩu</h3>
            </div>
        </div> --}}
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Đổi mật khẩu</h3>
            </div>
            <!-- /.card-header -->

            <!-- form start -->
            <form action="{{ route('update_pass') }}" method="post">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="old_password">Mật khẩu hiện tại</label>
                        <input type="password" class="form-control" name="old_password" id="old_password"
                            placeholder="Nhập mật khẩu hiện tại" data-toggle="password">
                    </div>
                    <div class="form-group">
                        <label for="new_password">Mật khẩu mới</label>
                        <input type="password" class="form-control" name="new_password" id="new_password"
                            placeholder="Nhập mật khẩu mới" data-toggle="password">
                    </div>
                    <div class="form-group">
                        <label for="password_confirm">Nhập lại mật khẩu mới</label>
                        <input type="password" class="form-control" name="password_confirm" id="password_confirm"
                            placeholder="Nhập lại mật khẩu mới" data-toggle="password">
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <a href="{{ route('index') }}" class="btn btn-default">Hủy</a>
                    <button type="submit" class="btn btn-primary float-right">Cập nhập</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript">
	$("#old_password").password('toggle');
	$("#new_password").password('toggle');
	$("#password_confirm").password('toggle');
</script>
@endsection