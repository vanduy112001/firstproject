@extends('layouts.user.app_login')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
<script src="https://www.google.com/recaptcha/api.js?render={{ $capcha['site_key'] }}"></script>
@endsection
@section('content')
<div class="p-3 pt-sm-4 pb-sm-4 px-sm-4">
    <div class="pt-4 pt-sm-0 text-center">
        <h1 class="h4 h4 text-primary mb-2">Tạo tài khoản</h1>
        <p class="mb-4">Quản lý tất cả các dịch vụ của bạn</p>
        <p><img class="gioi-thieu" src="{{ url('images/logo-bg-login.png') }}" alt=""></p>
    </div>
    <form class="user" action="{{ route('checkRegister') }}" method="post" id="fomr_register">
        @csrf
        <div class="form-group row mb-0">
            <div class="col-sm-6 mb-3">
                <input type="text" value="{{ old('last_name') }}" name="last_name"
                    class="form-control rounded-pill @error('last_name') is-invalid @enderror" id="LastName"
                    placeholder="Họ">
                @if($errors->has('last_name'))
                <span class="invalid-feedback" role="alert">
                    <p class="maudo">{{$errors->first('last_name')}}</p>
                </span>
                @endif
            </div>
            <div class="col-sm-6 mb-3">
                <input type="text" name="first_name" value="{{ old('first_name') }}"
                    class="form-control rounded-pill @error('first_name') is-invalid @enderror" id="FirstName"
                    placeholder="Tên">
                @if($errors->has('first_name'))
                <span class="invalid-feedback" role="alert">
                    <p class="maudo">{{$errors->first('first_name')}}</p>
                </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            <input type="email" name="email" value="{{ old('email') }}"
                class="form-control rounded-pill @error('email') is-invalid @enderror" id="InputEmail"
                placeholder="Nhập email">
            @if($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <p class="maudo">{{$errors->first('email')}}</p>
            </span>
            @endif
        </div>
        <div class="form-group @error('gender') is-invalid @enderror" id="selectGender">
            <select name="gender" class="form-control rounded-pill">
                <option value="" disabled selected>Chọn giới tính</option>
                <option value="Nam" @if( old('gender')=='Nam' ) selected @endif>Nam</option>
                <option value="Nữ" @if( old('gender')=='Nữ' ) selected @endif>Nữ</option>
            </select>
            @if($errors->has('gender'))
            <span class="invalid-feedback" role="alert">
                <p class="maudo">{{$errors->first('gender')}}</p>
            </span>
            @endif
        </div>
        <div class="form-group">
            <input type="text" name="phone" value="{{ old('phone') }}"
                class="form-control rounded-pill @error('phone') is-invalid @enderror" id="InputPhone"
                placeholder="Số điện thoại">
            @if($errors->has('phone'))
            <span class="invalid-feedback" role="alert">
                <p class="maudo">{{$errors->first('phone')}}</p>
            </span>
            @endif
        </div>
        <div class="form-group">
            <input type="text" name="address" value="{{ old('address') }}"
                class="form-control rounded-pill @error('address') is-invalid @enderror" id="InputAddress"
                placeholder="Địa chỉ">
            @if($errors->has('address'))
            <span class="invalid-feedback" role="alert">
                <p class="maudo">{{$errors->first('address')}}</p>
            </span>
            @endif
        </div>
        <div class="form-group">
            <input type="text" name="company" value="{{ old('company') }}"
                class="form-control rounded-pill @error('company') is-invalid @enderror" id="InputAddress"
                placeholder="Công ty / Tổ chức">
            @if($errors->has('company'))
            <span class="invalid-feedback" role="alert">
                <p class="maudo">{{$errors->first('company')}}</p>
            </span>
            @endif
        </div>
        <div class="form-group">
            <input type="text" name="date" value="{{ old('date') }}"
                class="form-control rounded-pill @error('date') is-invalid @enderror" id="InputDate"
                placeholder="Ngày sinh" autocomplete="off">
            @if($errors->has('date'))
            <span class="invalid-feedback" role="alert">
                <p class="maudo">{{$errors->first('date')}}</p>
            </span>
            @endif
        </div>
        <div class="form-group row">
            <div class="col-sm-6 mb-3 mb-sm-0">
                <input type="password" name="password" value="{{ old('password') }}"
                    class="form-control rounded-pill @error('password') is-invalid @enderror" id="InputPassword"
                    placeholder="Mật khẩu">
                @if($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <p class="maudo">{{$errors->first('password')}}</p>
                </span>
                @endif
            </div>
            <div class="col-sm-6">
                <input type="password" value="{{ old('password_confirmation') }}" name="password_confirmation"
                    class="form-control rounded-pill @error('password_confirmation') is-invalid @enderror"
                    id="RepeatPassword" placeholder="Nhập lại mật khẩu">
                @if($errors->has('password_confirmation'))
                <span class="invalid-feedback" role="alert">
                    <p class="maudo">{{$errors->first('password_confirmation')}}</p>
                </span>
                @endif
            </div>
            <input type="hidden" name="recaptcha" id="recaptcha" value="">
        </div>
        <!-- <input type="submit" value="Tạo tài khoản" class="btn btn-primary btn-block rounded-pill"> -->
        <button class="g-recaptcha btn btn-primary btn-block rounded-pill" data-sitekey="{{ $capcha['site_key'] }}" data-callback='onSubmit' data-action='submit'>Tạo tài khoản</button>
        {{-- <hr>
        <a href="{{ route('social.redirect', 'google') }}" class="btn btn-google btn-block rounded-pill">
            <i class="fab fa-google fa-fw"></i> Đăng ký với Google
        </a>
        <a href="{{ route('social.redirect', 'facebook') }}" class="btn btn-facebook btn-block rounded-pill">
            <i class="fab fa-facebook-f fa-fw"></i> Đăng ký với Facebook
        </a> --}}
    </form>
    <hr>
    <div class="mb-5">
        <div class="text-center float-left">
            <a class="small" href="{{ route('forgot_password_form') }}">Quên mật khẩu ?</a>
        </div>
        <div class="text-center float-right">
            <a class="small" href="{{ route('login') }}">Đăng nhập !</a>
        </div>
    </div>
    <div class="text-center">
        {{-- <p class="mb-2 small text-secondary">Liên hệ</p> --}}
        <div class="contact-icon">
            <a href="https://www.facebook.com/cloudzone.vn" target="_blank"><button class="btn btn-default btn-icon border-primary text-primary mr-1"><i class="fab fa-facebook-f"></i></button></a>
            <a href="tel:0236-4455-789"><button class="btn btn-default btn-icon border-success text-success mr-1"><i class="fas fa-phone"></i></button></a>
            <a href="mailto:support@cloudzone.vn"><button class="btn btn-default btn-icon border-danger text-danger"><i class="fas fa-envelope"></i></button></a>
        </div>
    </div>
</div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        //Date picker
        $('#InputDate').datepicker({
            autoclose: true
        });
        // select
        $('.select2').select2({
            placeholder: 'Chọn giới tính'
        });
        function onSubmit(token) {
            // console.log('da click');
           document.getElementById("fomr_register").submit();
       }
       $('.g-recaptcha').on('click', function () {
            // console.log('da click 2');
            $('#fomr_register').submit();
       })
    });
    grecaptcha.ready(function() {
       grecaptcha.execute('{{ $capcha['site_key'] }}', {action: 'contact'}).then(function(token) {
            console.log(token);
            if (token) {
              $('#recaptcha').val(token);
            }
        });
   });
    // https://www.google.com/search?client=firefox-b-d&q=capcha+v3+in+laravel
</script>
@endsection