@extends('layouts.user.app_login')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
@endsection
@section('content')
<div class="p-3 pt-sm-4 pb-sm-4 px-sm-4">
    <div class="col-md-12 detail-course-finish">
        @if(session("success"))
        <div class="bg-success">
            <p class="text-light">{{session("success")}}</p>
        </div>
        @elseif(session("fails"))
        <div class="bg-danger">
            <p class="text-light">{{session("fails")}}</p>
        </div>
        @endif
    </div>
    <div class="pt-4 pt-sm-0 text-center">
        <h1 class="h4 h4 text-primary mb-2">Cập nhật thông tin tài khoản</h1>
        <p class="mb-4">Quản lý tất cả các dịch vụ của bạn</p>
        <p><img class="gioi-thieu" src="{{ url('images/logo-bg-login.png') }}" alt=""></p>
    </div>
    <form class="user" action="{{ route('form_update_profile') }}" method="post">
        @csrf
        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
        <div class="form-group">
            <input type="text" value="{{ Auth::user()->name }}" name="name"
                class="form-control rounded-pill @error('last_name') is-invalid @enderror" id="LastName"
                placeholder="Tên">
            @if($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <p class="maudo">{{$errors->first('name')}}</p>
            </span>
            @endif
        </div>
        <div class="form-group">
            <input type="email" name="email" value="{{ Auth::user()->email }}" disabled
                class="form-control rounded-pill @error('email') is-invalid @enderror" id="InputEmail"
                placeholder="Nhập email">
            @if($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <p class="maudo">{{$errors->first('email')}}</p>
            </span>
            @endif
        </div>
        <div class="form-group @error('gender') is-invalid @enderror" id="selectGender">
            <select name="gender" class="form-control rounded-pill">
                <option value="" disabled selected>Chọn giới tính</option>
                <option value="Nam" @if( old('gender')=='Nam' ) selected @endif>Nam</option>
                <option value="Nữ" @if( old('gender')=='Nữ' ) selected @endif>Nữ</option>
            </select>
            @if($errors->has('gender'))
            <span class="invalid-feedback" role="alert">
                <p class="maudo">{{$errors->first('gender')}}</p>
            </span>
            @endif
        </div>
        <div class="form-group">
            <input type="text" name="phone" value="{{ old('phone') }}"
                class="form-control rounded-pill @error('phone') is-invalid @enderror" id="InputPhone"
                placeholder="Số điện thoại">
            @if($errors->has('phone'))
            <span class="invalid-feedback" role="alert">
                <p class="maudo">{{$errors->first('phone')}}</p>
            </span>
            @endif
        </div>
        <div class="form-group">
            <input type="text" name="address" value="{{ old('address') }}"
                class="form-control rounded-pill @error('address') is-invalid @enderror" id="InputAddress"
                placeholder="Địa chỉ">
            @if($errors->has('address'))
            <span class="invalid-feedback" role="alert">
                <p class="maudo">{{$errors->first('address')}}</p>
            </span>
            @endif
        </div>
        <div class="form-group">
            <input type="text" name="company" value="{{ old('company') }}"
                class="form-control rounded-pill @error('company') is-invalid @enderror" id="InputAddress"
                placeholder="Công ty / Tổ chức">
            @if($errors->has('company'))
            <span class="invalid-feedback" role="alert">
                <p class="maudo">{{$errors->first('company')}}</p>
            </span>
            @endif
        </div>
        <div class="form-group">
            <input type="text" name="date" value="{{ old('date') }}"
                class="form-control rounded-pill @error('date') is-invalid @enderror"
                placeholder="Ngày sinh (ngày/tháng/năm)" autocomplete="off">
            @if($errors->has('date'))
            <span class="invalid-feedback" role="alert">
                <p class="maudo">{{$errors->first('date')}}</p>
            </span>
            @endif
        </div>
        <input type="submit" value="Cập nhật tài khoản" class="btn btn-primary rounded-pill btn-block">
    </form>
</div>
@endsection
@section('scripts')
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
		//Date picker
		$('#InputDate').datepicker({
			autoclose: true
		});
        // select
        $('.select2').select2();
	});
</script>
@endsection