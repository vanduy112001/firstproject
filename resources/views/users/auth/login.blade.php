@extends('layouts.user.app_login')
@section('content')
<style>
    @-webkit-keyframes not-blink {
        0% {
            opacity: 1
        }
        70% {
            opacity: 1
        }
        80% {
            opacity: 0
        }
        100% {
            opacity: 0
        }
    }
</style>
<div class="p-3 pt-sm-4 pb-sm-4 px-sm-4">
    <div class="pt-4 pt-sm-0 text-center">
        <h1 class="h4 h4 text-primary mb-2">Welcome to Cloudzone</h1>
        <p class="mb-4">Quản lý tất cả các dịch vụ của bạn</p>
        <p><img class="gioi-thieu" src="{{ url('images/logo-bg-login.png') }}" alt=""></p>
    </div>
    <div class="messager-login error-login text-center text-danger">
        @if(session("success"))
        <p class="alert alert-success">{{session("success")}}</p>
        @endif
        @if(session("confirm"))
        <p class="alert alert-success">
            {{session("confirm")}} Quý khách vui lòng liên hệ với chúng tôi qua Fanpage để kích hoạt tài khoản. 
            Bấm <a href="https://www.facebook.com/cloudzone.vn" target="_blank">vào đây</a> để đến Fanpage Cloudzone.vn.
        </p>
        @endif
        @if(session("fails"))
        <p class="alert alert-danger">{!! session("fails") !!}</p>
        @endif
        <p class="alert alert-danger" style="font-size: 14px"><span class="qttVpsPros">Thông báo:</span> Việc đăng kí tài khoản mới với mạng xã hội hiện đang tạm ngưng. Quý khách vui lòng bấm <a href="https://portal.cloudzone.vn/register">vào đây</a> để tạo tài khoản bằng email.</p>
    </div>
    <form class="user" action="{{ route('checkLogin') }}" method="POST">
        @csrf
        <div class="form-group">
            <input type="email" value="{{ old('email') }}" name="email"
                class="form-control rounded-pill" id="exampleInputEmail" aria-describedby="emailHelp"
                placeholder="Nhập email ...">
        </div>
        <div class="form-group">
            <input type="password" value="{{ old('password') }}" name="password"
                class="form-control rounded-pill" id="exampleInputPassword"
                placeholder="Nhập password ...">
        </div>
        <div class="form-group">
            <div class="custom-control custom-checkbox small">
                <input type="checkbox" class="custom-control-input" id="customCheck" name="remember">
                <label class="custom-control-label" for="customCheck">Ghi nhớ</label>
            </div>
        </div>
        <input type="submit" class="btn btn-primary btn-block rounded-pill" value="Login">
    </form>
    <hr>
    <div class="row mb-3">
        <div class="col-6">
            <span style="font-size: 14px">Đăng nhập với: <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="top" title="Chỉ đăng nhập với những tài khoản đã tạo với mạng xã hội từ trước"></i></span> <br>
            <a href="{{ route('social.redirect', 'google') }}" class="btn btn-google rounded-pill"><i class="fab fa-google fa-fw"></i></a>
            <a href="{{ route('social.redirect', 'facebook') }}" class="btn btn-facebook rounded-pill"><i class="fab fa-facebook-f fa-fw"></i></a>
        </div>
        <div class="col-6 mt-2">
            <div class="text-center float-right">
                <a class="small" href="{{ route('register') }}">Tạo tài khoản !</a> <br>
                <a class="small" href="{{ route('forgot_password_form') }}">Quên mật khẩu ?</a>
            </div>
        </div>
    </div>
    <div class="row text-center">
        {{-- <p class="mb-2 small text-secondary">Liên hệ</p> --}}
        <div class="col-12 contact-icon">
            <a href="https://www.facebook.com/cloudzone.vn" target="_blank"><button class="btn btn-default btn-icon border-primary text-primary mr-1"><i class="fab fa-facebook-f"></i></button></a>
            <a href="tel:0236-4455-789"><button class="btn btn-default btn-icon border-success text-success mr-1"><i class="fas fa-phone"></i></button></a>
            <a href="mailto:support@cloudzone.vn"><button class="btn btn-default btn-icon border-danger text-danger"><i class="fas fa-envelope"></i></button></a>
        </div>
    </div>

</div>
@endsection
@section('scripts')
    <script>
        jQuery(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip()
        });
    </script>
@endsection