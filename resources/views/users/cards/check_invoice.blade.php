@extends('layouts.user2.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
Chi tiết hóa đơn
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Chi tiết hóa đơn</li>
@endsection
@section('content')
<div class="box box-primary">
    <div class="box-body table-responsive">
        <div class="col-md-12 detail-course-finish">
            @if(session("success"))
                <div class="bg-success">
                    <p class="text-light">{{session("success")}}</p>
                </div>
            @elseif(session("fails"))
                <div class="bg-danger">
                    <p class="text-light">{{session("fails")}}</p>
                </div>
            @elseif(session("errorExpired"))
                <div class="bg-danger">
                    <p class="text-light">{!! session("errorExpired") !!}</p>
                </div>
            @endif
        </div>
        <div id="invoice-page">
            <!-- Main content -->
            <div class="invoice p-3 mb-3">
                <!-- title row -->
                <div class="row invoice-header">
                    <div class="col col-md-6 logo">
                        <img src="{{ url('images/LogoCloud.png') }}" alt="cloudzone.vn" width="200">
                        <p class="invoice-number">Hóa đơn số #{{ $invoice->id }}</p>
                    </div>
                    <div class="col col-md-6 invoice-status text-center">
                        @if (!empty($invoice->paid_date))
                            <div class="paid">Đã thanh toán</div>
                            <div class="date">Ngày thanh toán: {{ date('d-m-Y', strtotime($invoice->paid_date)) }}</div>
                            <div class="dua_date">Ngày kết thúc: {{ date('d-m-Y', strtotime($invoice->due_date)) }}</div>
                        @else
                            <div class="unpaid">Chưa thanh toán</div>
                            <div class="dua_date">Ngày kết thúc: {{ date('d-m-Y', strtotime($invoice->due_date)) }}</div>
                        @endif
                    </div>
                <!-- /.col -->
                </div>
                <hr>
                <!-- info row -->
                <div class="row invoice-info">
                    <div class="col-sm-6 invoice-col">
                        <div class="invoice-info-header">
                            <strong>Nhà cung cấp dịch vụ</strong>
                        </div>
                        <address>
                            Công ty TNHH MTV Đại Việt Số<br>
                            257 Lê Duẩn, Tân Chính, Thanh Khê, Đà Nẵng<br>
                            SĐT: 0236 4455 789<br>
                            Email: info@cloudzone.vn
                        </address>
                    </div>
                    <!-- /.col -->
                    @php
                        $user = UserHelper::get_user(Auth::user()->id);
                    @endphp
                    <div class="col-sm-6 invoice-col">
                        <div class="invoice-info-header">
                            <strong>Thông tin khách hàng</strong>
                        </div>
                        <address>
                            {{ $user->name }}<br>
                            {{ $user->user_meta->address }}<br>
                            SĐT: {{ $user->user_meta->phone }}<br>
                            Email: {{ $user->email }}
                        </address>
                    </div>
                    <!-- /.col -->
                </div>
                <div class="row">
                    <div class="col col-md-12 table-responsive">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th width="60%">Mô tả chi tiết</th>
                                    <th width="20%" class="text-center">Thời gian</th>
                                    <th width="20%" class="text-center">Số tiền</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($invoice->type == 'VPS' || $invoice->type == 'NAT-VPS' || $invoice->type == 'VPS-US' 
                                || $invoice->type == 'VPS US' || $invoice->type == ' US' )
                                  <!-- Gia han -->
                                        @if($invoice->order->type == 'expired')
                                            @php
                                                $order_expireds = $invoice->order_expireds;
                                            @endphp
                                            @foreach ($order_expireds as $order_expired)
                                                @php
                                                    $product = GroupProduct::get_product($order_expired->vps->product_id);
                                                    $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                                    $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                                    $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                                    // Addon
                                                @endphp
                                                @if ( !empty($order_expired->vps->price_override) )
                                                    @if ( $order_expired->vps->billing_cycle == $order_expired->billing_cycle )
                                                          @php
                                                            if (!empty($order_expired->vps->vps_config)) {
                                                              $vps_config = $order_expired->vps->vps_config;
                                                              if (!empty($vps_config)) {
                                                                $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                                                                $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                                                                $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                                                              }
                                                            }
                                                          @endphp
                                                          <tr>
                                                            <td>
                                                              Gia hạn {{ $invoice->type }} - {{ !empty($order_expired->vps->ip) ? $order_expired->vps->ip : '' }}
                                                              ( {{ $cpu }} CPU - {{ $ram }} RAM - {{ $disk }} Disk )
                                                            </td>
                                                            <td class="text-center">
                                                              {{ $billing_time[$order_expired->billing_cycle] }}
                                                            </td>
                                                            <td class="text-center">
                                                              @if(!empty($order_expired->vps->price_override))
                                                              {!!number_format($order_expired->vps->price_override,0,",",".") . ' VNĐ'!!}
                                                              @else
                                                              {!!number_format($product->pricing[$order_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                              @endif
                                                            </td>
                                                          </tr>
                                                    @else
                                                      <tr>
                                                        <td>
                                                          Gia hạn VPS - {{ !empty($order_expired->vps->ip) ? $order_expired->vps->ip : '' }}
                                                          ( {{ $cpu }} CPU - {{ $ram }} RAM - {{ $disk }} Disk )
                                                        </td>
                                                        <td class="text-center">
                                                          {{ $billing_time[$order_expired->billing_cycle] }}
                                                        </td>
                                                        <td class="text-center">
                                                          @if(!empty($order_expired->vps->price_override))
                                                            {!!number_format( ($order_expired->vps->price_override * $billing_dashboard[$order_expired->billing_cycle] / $billing_dashboard[$order_expired->vps->billing_cycle]) ,0,",",".") . ' VNĐ'!!}
                                                          @else
                                                            {!!number_format($product->pricing[$order_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                          @endif
                                                        </td>
                                                      </tr>
                                                      @if ( empty($order_expired->vps->price_override) )
                                                        @if(!empty($order_expired->vps->vps_config))
                                                        @php
                                                          $add_on_products = GroupProduct::get_addon_product_private(Auth::user()->id);
                                                          $add_on_vps = $order_expired->vps->vps_config;
                                                          $billing_cycle = $order_expired->billing_cycle;
                                                        @endphp
                                                        @if (!empty($add_on_vps->cpu))
                                                        <tr>
                                                          <td>
                                                            Add-on CPU ({{ $add_on_vps->cpu }} cores)
                                                          </td>
                                                          <td class="text-center">
                                                            {{ $billing_time[$order_expired->billing_cycle] }}
                                                          </td>
                                                          <td class="text-center">
                                                            <?php
                                                            $pricing_addon_cpu = 0;
                                                            foreach ($add_on_products as $key => $add_on_product) {
                                                              if (!empty($add_on_product->meta_product->type_addon)) {
                                                                if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                                  $pricing_addon_cpu = $add_on_product->pricing[$billing_cycle];
                                                                }
                                                              }
                                                            }
                                                            ?>
                                                            {!!number_format( $pricing_addon_cpu * $add_on_vps->cpu ,0,",",".") . ' VNĐ'!!}
                                                          </td>
                                                        </tr>
                                                        @endif
                                                        @if (!empty($add_on_vps->ram))
                                                        <tr>
                                                          <td>
                                                            Add-on RAM ({{ $add_on_vps->ram }} GB)
                                                          </td>
                                                          <td class="text-center">
                                                            {{ $billing_time[$order_expired->billing_cycle] }}
                                                          </td>
                                                          <td class="text-center">
                                                            <?php
                                                            $pricing_addon_ram = 0;
                                                            foreach ($add_on_products as $key => $add_on_product) {
                                                              if (!empty($add_on_product->meta_product->type_addon)) {
                                                                if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                                  $pricing_addon_ram = $add_on_product->pricing[$billing_cycle];
                                                                }
                                                              }
                                                            }
                                                            ?>
                                                            {!!number_format( $pricing_addon_ram * $add_on_vps->ram ,0,",",".") . ' VNĐ'!!}
                                                          </td>
                                                        </tr>
                                                        @endif
                                                        @if (!empty($add_on_vps->disk))
                                                        <tr>
                                                          <td>
                                                            Add-on DISK ({{ $add_on_vps->disk }} GB)
                                                          </td>
                                                          <td class="text-center">
                                                            {{ $billing_time[$order_expired->billing_cycle] }}
                                                          </td>
                                                          <td class="text-center">
                                                            <?php
                                                            $pricing_addon_disk = 0;
                                                            foreach ($add_on_products as $key => $add_on_product) {
                                                              if (!empty($add_on_product->meta_product->type_addon)) {
                                                                if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                                  $pricing_addon_disk = $add_on_product->pricing[$billing_cycle];
                                                                }
                                                              }
                                                            }
                                                            ?>
                                                            {!!number_format( $pricing_addon_disk * $add_on_vps->disk / 10,0,",",".") . ' VNĐ'!!}
                                                          </td>
                                                        </tr>
                                                        @endif
                                                        @endif
                                                      @endif
                                                    @endif
                                                @else
                                                    <tr>
                                                        <td>
                                                            Gia hạn {{ $invoice->type }} - {{ !empty($order_expired->vps->ip) ? $order_expired->vps->ip : '' }}
                                                            ( {{ $cpu }} CPU - {{ $ram }} RAM - {{ $disk }} Disk )
                                                        </td>
                                                        <td class="text-center">
                                                           {{ $billing_time[$order_expired->billing_cycle] }}
                                                        </td>
                                                        <td class="text-center">
                                                            @if(!empty($order_expired->vps->price_override))
                                                              {!!number_format($order_expired->vps->price_override,0,",",".") . ' VNĐ'!!}
                                                            @else
                                                              {!!number_format($product->pricing[$order_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @if(!empty($order_expired->vps->vps_config))
                                                            @php
                                                                $add_on_products = GroupProduct::get_addon_product_private(Auth::user()->id);
                                                                $add_on_vps = $order_expired->vps->vps_config;
                                                                $billing_cycle = $order_expired->billing_cycle;
                                                            @endphp
                                                        @if (!empty($add_on_vps->cpu))
                                                          <tr>
                                                              <td>
                                                                  Add-on CPU ({{ $add_on_vps->cpu }} cores)
                                                              </td>
                                                              <td class="text-center">
                                                                 {{ $billing_time[$order_expired->billing_cycle] }}
                                                              </td>
                                                              <td class="text-center">
                                                                  <?php
                                                                      $pricing_addon_cpu = 0;
                                                                      foreach ($add_on_products as $key => $add_on_product) {
                                                                          if (!empty($add_on_product->meta_product->type_addon)) {
                                                                              if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                                                $pricing_addon_cpu = $add_on_product->pricing[$billing_cycle];
                                                                              }
                                                                          }
                                                                      }
                                                                  ?>
                                                                  {!!number_format( $pricing_addon_cpu * $add_on_vps->cpu ,0,",",".") . ' VNĐ'!!}
                                                              </td>
                                                          </tr>
                                                        @endif
                                                        @if (!empty($add_on_vps->ram))
                                                          <tr>
                                                              <td>
                                                                  Add-on RAM ({{ $add_on_vps->ram }} GB)
                                                              </td>
                                                              <td class="text-center">
                                                                 {{ $billing_time[$order_expired->billing_cycle] }}
                                                              </td>
                                                              <td class="text-center">
                                                                  <?php
                                                                      $pricing_addon_ram = 0;
                                                                      foreach ($add_on_products as $key => $add_on_product) {
                                                                          if (!empty($add_on_product->meta_product->type_addon)) {
                                                                              if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                                                $pricing_addon_ram = $add_on_product->pricing[$billing_cycle];
                                                                              }
                                                                          }
                                                                      }
                                                                  ?>
                                                                  {!!number_format( $pricing_addon_ram * $add_on_vps->ram ,0,",",".") . ' VNĐ'!!}
                                                              </td>
                                                          </tr>
                                                        @endif
                                                        @if (!empty($add_on_vps->disk))
                                                          <tr>
                                                              <td>
                                                                  Add-on DISK ({{ $add_on_vps->disk }} GB)
                                                              </td>
                                                              <td class="text-center">
                                                                 {{ $billing_time[$order_expired->billing_cycle] }}
                                                              </td>
                                                              <td class="text-center">
                                                                  <?php
                                                                      $pricing_addon_disk = 0;
                                                                      foreach ($add_on_products as $key => $add_on_product) {
                                                                          if (!empty($add_on_product->meta_product->type_addon)) {
                                                                              if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                                                $pricing_addon_disk = $add_on_product->pricing[$billing_cycle];
                                                                              }
                                                                          }
                                                                      }
                                                                  ?>
                                                                  {!!number_format( $pricing_addon_disk * $add_on_vps->disk / 10,0,",",".") . ' VNĐ'!!}
                                                              </td>
                                                          </tr>
                                                        @endif
                                                    @endif
                                                @endif
                                            @endforeach
                                      <!-- Order -->
                                        @else
                                            @php
                                                $product = GroupProduct::get_product($invoice->vps[0]->product_id);
                                            @endphp
                                              <tr>
                                                  <td>
                                                      {{  $product->name }}
                                                  </td>
                                                  <td class="text-center">
                                                     {{ $billing_time[$invoice->vps[0]->billing_cycle] }}
                                                  </td>
                                                  <td class="text-center">
                                                      {!!number_format($product->pricing[$invoice->vps[0]->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                  </td>
                                              </tr>
                                              @if(!empty($invoice->addon_id))
                                                      @php
                                                          $add_on_products = GroupProduct::get_addon_product_private(Auth::user()->id);
                                                          $add_on_vps = $invoice->vps[0]->vps_config;
                                                          $billing_cycle = $invoice->vps[0]->billing_cycle;
                                                      @endphp
                                                  @if (!empty($add_on_vps->cpu))
                                                    <tr>
                                                        <td>
                                                            Add-on CPU ({{ $add_on_vps->cpu }} cores)
                                                        </td>
                                                        <td class="text-center">
                                                           {{ $billing_time[$invoice->vps[0]->billing_cycle] }}
                                                        </td>
                                                        <td class="text-center">
                                                            <?php
                                                                $pricing_addon_cpu = 0;
                                                                foreach ($add_on_products as $key => $add_on_product) {
                                                                    if (!empty($add_on_product->meta_product->type_addon)) {
                                                                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                                          $pricing_addon_cpu = $add_on_product->pricing[$billing_cycle];
                                                                        }
                                                                    }
                                                                }
                                                            ?>
                                                            {!!number_format( $pricing_addon_cpu * $add_on_vps->cpu ,0,",",".") . ' VNĐ'!!}
                                                        </td>
                                                    </tr>
                                                  @endif
                                                  @if (!empty($add_on_vps->ram))
                                                    <tr>
                                                        <td>
                                                            Add-on RAM ({{ $add_on_vps->ram }} GB)
                                                        </td>
                                                        <td class="text-center">
                                                           {{ $billing_time[$invoice->vps[0]->billing_cycle] }}
                                                        </td>
                                                        <td class="text-center">
                                                            <?php
                                                                $pricing_addon_ram = 0;
                                                                foreach ($add_on_products as $key => $add_on_product) {
                                                                    if (!empty($add_on_product->meta_product->type_addon)) {
                                                                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                                          $pricing_addon_ram = $add_on_product->pricing[$billing_cycle];
                                                                        }
                                                                    }
                                                                }
                                                            ?>
                                                            {!!number_format( $pricing_addon_ram * $add_on_vps->ram ,0,",",".") . ' VNĐ'!!}
                                                        </td>
                                                    </tr>
                                                  @endif
                                                  @if (!empty($add_on_vps->disk))
                                                    <tr>
                                                        <td>
                                                            Add-on DISK ({{ $add_on_vps->disk }} GB)
                                                        </td>
                                                        <td class="text-center">
                                                           {{ $billing_time[$invoice->vps[0]->billing_cycle] }}
                                                        </td>
                                                        <td class="text-center">
                                                            <?php
                                                                $pricing_addon_disk = 0;
                                                                foreach ($add_on_products as $key => $add_on_product) {
                                                                    if (!empty($add_on_product->meta_product->type_addon)) {
                                                                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                                          $pricing_addon_disk = $add_on_product->pricing[$billing_cycle];
                                                                        }
                                                                    }
                                                                }
                                                            ?>
                                                            {!!number_format( $pricing_addon_disk * $add_on_vps->disk / 10,0,",",".") . ' VNĐ'!!}
                                                        </td>
                                                    </tr>
                                                  @endif
                                              @endif
                                              @if ( !empty($invoice->order->promotion) )
                                                <tr>
                                                  <td>
                                                    Mã giảm giá {{ $invoice->order->promotion->code }} <br>
                                                    @if ( $invoice->order->promotion->type == 'percentage' )
                                                      Giảm {{ $invoice->order->promotion->value }} %
                                                    @elseif ( $invoice->order->promotion->type == 'fixed_amount' )
                                                      Giảm {!!number_format($invoice->order->promotion->value,0,",",".") . ' VNĐ'!!}
                                                    @elseif ( $invoice->order->promotion->type == 'free' )
                                                      Miễn phí cho đơn hàng
                                                    @endif
                                                  </td>
                                                  <td class="text-center"></td>
                                                  <td class="text-center">
                                                    @if ( $invoice->order->promotion->type == 'percentage' )
                                                      @php
                                                        $amount_promotion = ($invoice->order->total * 100) / ( 100 - $invoice->order->promotion->value ) - ($invoice->order->total);
                                                      @endphp
                                                      - {!!number_format($amount_promotion,0,",",".") . ' VNĐ'!!}
                                                    @elseif ( $invoice->order->promotion->type == 'fixed_amount' )
                                                      - {!!number_format($invoice->order->promotion->value,0,",",".") . ' VNĐ'!!}
                                                    @elseif ( $invoice->order->promotion->type == 'free' )
                                                      {!!number_format($invoice->order->total,0,",",".") . ' VNĐ'!!}
                                                    @endif
                                                  </td>
                                                </tr>
                                              @endif
                                        @endif
                                    <tr>
                                        <td class="text-right" colspan="2"><b>Số lượng</b></td>
                                        <td class="text-center">{{ $invoice->quantity }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right" colspan="2"><b>Thành tiền</b></td>
                                        <td class="text-center">{!!number_format( !empty($invoice->order->total) ? $invoice->order->total : 0 ,0,",",".") . ' VNĐ'!!}</td>
                                    </tr>
                                @elseif ( $invoice->type == 'Proxy' )
                                    @if($invoice->order->type == 'expired')
                                      @php
                                          $order_expireds = $invoice->order_expireds;
                                      @endphp
                                      @foreach ($order_expireds as $order_expired)
                                        <tr>
                                          @php
                                              $proxy = $order_expired->proxy;
                                              $product = $proxy->product;
                                          @endphp
                                          <td>
                                              Gia hạn Proxy {{ $proxy->ip }} <br>
                                          </td>
                                          <td class="text-center">
                                            {{ $billing_time[$order_expired->billing_cycle] }}
                                          </td>
                                          <td class="text-center">
                                              @if ( !empty( $proxy->price_override ) )
                                                {!!number_format( $proxy->price_override ,0,",",".") . ' VNĐ'!!}
                                              @else
                                                {!!number_format( $product->pricing[$order_expired->billing_cycle] ,0,",",".") . ' VNĐ'!!}
                                              @endif
                                          </td>
                                        </tr>
                                      @endforeach
                                    @else
                                      @php
                                          $proxy = $invoice->proxies[0];
                                          $product = $proxy->product;
                                      @endphp
                                      <tr>
                                        <td>
                                            Đặt hàng Proxy {{ $product->name }} <br>
                                        </td>
                                        <td class="text-center">
                                          {{ $billing_time[$proxy->billing_cycle] }}
                                        </td>
                                        <td class="text-center">
                                            {!!number_format($product->pricing[$proxy->billing_cycle],0,",",".") . ' VNĐ'!!}
                                        </td>
                                      </tr>
                                    @endif
                                    <tr>
                                        <td class="text-right" colspan="2"><b>Số lượng</b></td>
                                        <td class="text-center">{{ $invoice->quantity }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right" colspan="2"><b>Thành tiền</b></td>
                                        <td class="text-center">{!!number_format( !empty($invoice->order->total) ? $invoice->order->total : ( $invoice->sub_total * $invoice->quantity ) ,0,",",".") . ' VNĐ'!!}</td>
                                    </tr> 
                                @elseif($invoice->type == 'Server')
                                    @if($invoice->order->type == 'expired')
                                        @php
                                            $order_expireds = $invoice->order_expireds;
                                        @endphp
                                        @foreach ($order_expireds as $order_expired)
                                          <tr>
                                              @php
                                                  $server = $order_expired->server;
                                                  $product = $server->product;
                                                  $server_config = !empty($server->server_config) ? $server->server_config : null;
                                                  // cấu hình
                                                  if ( !empty($server->config_text) ) {
                                                    $text_server_config = $server->config_text;
                                                  } else {
                                                    $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                                    $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                                                    $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                                    $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                                    $text_server_config = $cpu . ' (' . $cores . '), ' . $ram . ', ' . $disk . ' (' . $server->raid . ')';
                                                  }
                                              @endphp
                                              <td>
                                                  Gia hạn Server {{  $server->server_name . ' (IP: ' . $server->ip . ')' }} <br>
                                                  Cấu hình: {!! $text_server_config !!}
                                              </td>
                                              <td class="text-center">
                                                 {{ $billing_time[$order_expired->billing_cycle] }}
                                              </td>
                                              <td class="text-center">
                                                  @if ( !empty( $server->amount ) )
                                                    {!!number_format( $server->amount ,0,",",".") . ' VNĐ'!!}
                                                  @else
                                                    {!!number_format( $product->pricing[$order_expired->billing_cycle] ,0,",",".") . ' VNĐ'!!}
                                                  @endif
                                              </td>
                                          </tr>
                                          @if(empty( $server->amount ))
                                            @if( !empty($server_config) )
                                                @if ( !empty( $server_config->ram ) )
                                                  @php
                                                    $add_ram = 0;
                                                    $total_add_ram = 0;
                                                    foreach ($server->server_config_rams as $server_config_ram) {
                                                      $add_ram += !empty($server_config_ram->product->meta_product->memory) ? $server_config_ram->product->meta_product->memory : 0;
                                                      $total_add_ram += !empty($server_config_ram->product->pricing[$order_expired->billing_cycle]) ? $server_config_ram->product->pricing[$order_expired->billing_cycle] : 0;
                                                    }
                                                  @endphp
                                                  <tr>
                                                    <td>
                                                      Addon RAM: {{ $add_ram }} GB RAM
                                                    </td>
                                                    <td class="text-center">
                                                       {{ $billing_time[$order_expired->billing_cycle] }}
                                                    </td>
                                                    <td class="text-center">
                                                        {!!number_format($total_add_ram,0,",",".") . ' VNĐ'!!}
                                                    </td>
                                                  </tr>
                                                @endif
                                                @if ( !empty($server_config->disk2) )
                                                  <tr>
                                                    <td>
                                                      Addon Disk 2: {{ $server_config->product_disk2->name }}
                                                    </td>
                                                    <td class="text-center">
                                                       {{ $billing_time[$order_expired->billing_cycle] }}
                                                    </td>
                                                    <td class="text-center">
                                                        {!!number_format($server_config->product_disk2->pricing[$order_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                    </td>
                                                  </tr>
                                                @endif
                                                @if ( !empty($server_config->disk3) )
                                                  <tr>
                                                    <td>
                                                      Addon Disk 3: {{ $server_config->product_disk3->name }}
                                                    </td>
                                                    <td class="text-center">
                                                       {{ $billing_time[$order_expired->billing_cycle] }}
                                                    </td>
                                                    <td class="text-center">
                                                        {!!number_format($server_config->product_disk3->pricing[$order_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                    </td>
                                                  </tr>
                                                @endif
                                                @if ( !empty($server_config->disk4) )
                                                  <tr>
                                                    <td>
                                                      Addon Disk 4: {{ $server_config->product_disk4->name }}
                                                    </td>
                                                    <td class="text-center">
                                                       {{ $billing_time[$order_expired->billing_cycle] }}
                                                    </td>
                                                    <td class="text-center">
                                                        {!!number_format($server_config->product_disk4->pricing[$order_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                    </td>
                                                  </tr>
                                                @endif
                                                @if ( !empty($server_config->disk5) )
                                                  <tr>
                                                    <td>
                                                      Addon Disk 5: {{ $server_config->product_disk5->name }}
                                                    </td>
                                                    <td class="text-center">
                                                       {{ $billing_time[$order_expired->billing_cycle] }}
                                                    </td>
                                                    <td class="text-center">
                                                        {!!number_format($server_config->product_disk5->pricing[$order_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                    </td>
                                                  </tr>
                                                @endif
                                                @if ( !empty($server_config->disk6) )
                                                  <tr>
                                                    <td>
                                                      Addon Disk 6: {{ $server_config->product_disk6->name }}
                                                    </td>
                                                    <td class="text-center">
                                                       {{ $billing_time[$order_expired->billing_cycle] }}
                                                    </td>
                                                    <td class="text-center">
                                                        {!!number_format($server_config->product_disk6->pricing[$order_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                    </td>
                                                  </tr>
                                                @endif
                                                @if ( !empty($server_config->disk7) )
                                                  <tr>
                                                    <td>
                                                      Addon Disk 7: {{ $server_config->product_disk7->name }}
                                                    </td>
                                                    <td class="text-center">
                                                       {{ $billing_time[$order_expired->billing_cycle] }}
                                                    </td>
                                                    <td class="text-center">
                                                        {!!number_format($server_config->product_disk7->pricing[$order_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                    </td>
                                                  </tr>
                                                @endif
                                                @if ( !empty($server_config->disk8) )
                                                  <tr>
                                                    <td>
                                                      Addon Disk 8: {{ $server_config->product_disk8->name }}
                                                    </td>
                                                    <td class="text-center">
                                                       {{ $billing_time[$order_expired->billing_cycle] }}
                                                    </td>
                                                    <td class="text-center">
                                                        {!!number_format($server_config->product_disk8->pricing[$order_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                    </td>
                                                  </tr>
                                                @endif
                                                @if ( !empty( $server_config->ip ) )
                                                  @php
                                                    $add_ip = 0;
                                                    $total_add_ip = 0;
                                                    foreach ($server->server_config_ips as $server_config_ip) {
                                                      $add_ip += !empty($server_config_ip->product->meta_product->ip) ? $server_config_ip->product->meta_product->ip : 0;
                                                      $total_add_ip += !empty($server_config_ip->product->pricing[$order_expired->billing_cycle]) ? $server_config_ip->product->pricing[$order_expired->billing_cycle] : 0;
                                                    }
                                                  @endphp
                                                  <tr>
                                                    <td>
                                                      Addon IP: {{ $add_ip }} IP Public
                                                    </td>
                                                    <td class="text-center">
                                                       {{ $billing_time[$order_expired->billing_cycle] }}
                                                    </td>
                                                    <td class="text-center">
                                                        {!!number_format($total_add_ip,0,",",".") . ' VNĐ'!!}
                                                    </td>
                                                  </tr>
                                                @endif
                                            @endif
                                          @endif
                                        @endforeach
                                    @else
                                        @php
                                            $server = $invoice->servers[0];
                                            $ip  = !empty ( $server->ip ) ? 1 : 0;
                                            $product = $server->product;
                                            $server_config = !empty($server->server_config) ? $server->server_config : null;
                                            // cấu hình
                                            if ( !empty($server->config_text) ) {
                                              $text_server_config = $server->config_text;
                                            } else {
                                              $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                              $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                                              $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                              $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                              $text_server_config = $cpu . ' (' . $cores . '), ' . $ram . ', ' . $disk . ' (' . $server->raid . ')';
                                            }
                                        @endphp
                                        <tr>
                                            <td>
                                                Đặt hàng Server {{ $product->name }} <br>
                                                Cấu hình: {!! $text_server_config !!}
                                            </td>
                                            <td class="text-center">
                                               {{ $billing_time[$server->billing_cycle] }}
                                            </td>
                                            <td class="text-center">
                                                {!!number_format($product->pricing[$server->billing_cycle],0,",",".") . ' VNĐ'!!}
                                            </td>
                                        </tr>
                                        @if( !empty($server_config) )
                                            @if ( !empty( $server_config->ram ) )
                                              @php
                                                $add_ram = 0;
                                                $total_add_ram = 0;
                                                $add_ram += !empty($server->server_config_rams[0]->product->meta_product->memory) ? $server->server_config_rams[0]->product->meta_product->memory : 0;
                                                $total_add_ram += !empty($server->server_config_rams[0]->product->pricing[$server->billing_cycle]) ? $server->server_config_rams[0]->product->pricing[$server->billing_cycle] : 0;
                                              @endphp
                                              <tr>
                                                <td>
                                                  Addon RAM: {{ $add_ram }} GB RAM
                                                </td>
                                                <td class="text-center">
                                                  {{ $billing_time[$server->billing_cycle] }}
                                                </td>
                                                <td class="text-center">
                                                    {!!number_format($total_add_ram,0,",",".") . ' VNĐ'!!}
                                                </td>
                                              </tr>
                                            @endif
                                            @if ( !empty($server_config->disk2) )
                                              <tr>
                                                <td>
                                                  Addon Disk 2: {{ $server_config->product_disk2->name }}
                                                </td>
                                                <td class="text-center">
                                                   {{ $billing_time[$server->billing_cycle] }}
                                                </td>
                                                <td class="text-center">
                                                    {!!number_format($server_config->product_disk2->pricing[$server->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                </td>
                                              </tr>
                                            @endif
                                            @if ( !empty($server_config->disk3) )
                                              <tr>
                                                <td>
                                                  Addon Disk 3: {{ $server_config->product_disk3->name }}
                                                </td>
                                                <td class="text-center">
                                                   {{ $billing_time[$server->billing_cycle] }}
                                                </td>
                                                <td class="text-center">
                                                    {!!number_format($server_config->product_disk3->pricing[$server->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                </td>
                                              </tr>
                                            @endif
                                            @if ( !empty($server_config->disk4) )
                                              <tr>
                                                <td>
                                                  Addon Disk 4: {{ $server_config->product_disk4->name }}
                                                </td>
                                                <td class="text-center">
                                                   {{ $billing_time[$server->billing_cycle] }}
                                                </td>
                                                <td class="text-center">
                                                    {!!number_format($server_config->product_disk4->pricing[$server->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                </td>
                                              </tr>
                                            @endif
                                            @if ( !empty($server_config->disk5) )
                                              <tr>
                                                <td>
                                                  Addon Disk 5: {{ $server_config->product_disk5->name }}
                                                </td>
                                                <td class="text-center">
                                                   {{ $billing_time[$server->billing_cycle] }}
                                                </td>
                                                <td class="text-center">
                                                    {!!number_format($server_config->product_disk5->pricing[$server->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                </td>
                                              </tr>
                                            @endif
                                            @if ( !empty($server_config->disk6) )
                                              <tr>
                                                <td>
                                                  Addon Disk 6: {{ $server_config->product_disk6->name }}
                                                </td>
                                                <td class="text-center">
                                                   {{ $billing_time[$server->billing_cycle] }}
                                                </td>
                                                <td class="text-center">
                                                    {!!number_format($server_config->product_disk6->pricing[$server->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                </td>
                                              </tr>
                                            @endif
                                            @if ( !empty($server_config->disk7) )
                                              <tr>
                                                <td>
                                                  Addon Disk 7: {{ $server_config->product_disk7->name }}
                                                </td>
                                                <td class="text-center">
                                                   {{ $billing_time[$server->billing_cycle] }}
                                                </td>
                                                <td class="text-center">
                                                    {!!number_format($server_config->product_disk7->pricing[$server->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                </td>
                                              </tr>
                                            @endif
                                            @if ( !empty($server_config->disk8) )
                                              <tr>
                                                <td>
                                                  Addon Disk 8: {{ $server_config->product_disk8->name }}
                                                </td>
                                                <td class="text-center">
                                                   {{ $billing_time[$server->billing_cycle] }}
                                                </td>
                                                <td class="text-center">
                                                    {!!number_format($server_config->product_disk8->pricing[$server->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                </td>
                                              </tr>
                                            @endif
                                            @if ( !empty( $server_config->ip ) )
                                              @php
                                                $add_ip = 0;
                                                $total_add_ip = 0;
                                                $add_ip += !empty($server->server_config_ips[0]->product->meta_product->ip) ? $server->server_config_ips[0]->product->meta_product->ip : 0;
                                                $total_add_ip += !empty($server->server_config_ips[0]->product->pricing[$server->billing_cycle]) ? $server->server_config_ips[0]->product->pricing[$server->billing_cycle] : 0;
                                              @endphp
                                              <tr>
                                                <td>
                                                  Addon IP: {{ $add_ip }} IP Public
                                                </td>
                                                <td class="text-center">
                                                  {{ $billing_time[$server->billing_cycle] }}
                                                </td>
                                                <td class="text-center">
                                                    {!!number_format($total_add_ip,0,",",".") . ' VNĐ'!!}
                                                </td>
                                              </tr>
                                            @endif
                                        @endif
                                    @endif
                                    <tr>
                                        <td class="text-right" colspan="2"><b>Số lượng</b></td>
                                        <td class="text-center">{{ $invoice->quantity }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right" colspan="2"><b>Thành tiền</b></td>
                                        <td class="text-center">{!!number_format( !empty($invoice->order->total) ? $invoice->order->total : ( $invoice->sub_total * $invoice->quantity ) ,0,",",".") . ' VNĐ'!!}</td>
                                    </tr>
                                @elseif($invoice->type == 'Colocation')
                                    @if($invoice->order->type == 'expired')
                                        @php
                                            $order_expireds = $invoice->order_expireds;
                                        @endphp
                                        @foreach ($order_expireds as $order_expired)
                                          <tr>
                                              @php
                                                  $colocation = $order_expired->colocation;
                                                  $product = $colocation->product;
                                                  $ip  = !empty ( $colocation->ip ) ? $colocation->ip : 0;
                                              @endphp
                                              <td>
                                                  Gia hạn Colocation {{  $ip }} <br>
                                              </td>
                                              <td class="text-center">
                                                 {{ $billing_time[$order_expired->billing_cycle] }}
                                              </td>
                                              <td class="text-center">
                                                @if ( $colocation->billing_cycle == $order_expired->billing_cycle )
                                                  @if ( !empty( $colocation->amount ) )
                                                    {!!number_format( $colocation->amount ,0,",",".") . ' VNĐ'!!}
                                                  @else
                                                    {!!number_format( $product->pricing[$order_expired->billing_cycle] ,0,",",".") . ' VNĐ'!!}
                                                  @endif
                                                @else
                                                  @if ( !empty( $colocation->amount ) )
                                                    @php
                                                      $amount = $colocation->amount * $billing_dashboard[$order_expired->billing_cycle] / $billing_dashboard[$colocation->billing_cycle];
                                                      $amount = (int) round( $amount , -3);;
                                                    @endphp
                                                    {!!number_format( $amount ,0,",",".") . ' VNĐ'!!}
                                                  @else
                                                    {!!number_format( $product->pricing[$order_expired->billing_cycle] ,0,",",".") . ' VNĐ'!!}
                                                  @endif
                                                @endif
                                              </td>
                                          </tr>
                                          @if (!empty($colocation->colocation_config_ips))
                                            @foreach ( $colocation->colocation_config_ips as $colocation_config_ip )
                                              <tr>
                                                <td>
                                                    Addon {{  $colocation_config_ip->product->name }}
                                                </td>
                                                <td class="text-center">
                                                  {{ $billing_time[$order_expired->billing_cycle] }}
                                                </td>
                                                <td class="text-center">
                                                  {!!number_format($colocation_config_ip->product->pricing[$order_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                </td>
                                              </tr>
                                            @endforeach
                                          @endif
                                        @endforeach
                                    @else
                                        @php
                                            $colocations = $invoice->colocations;
                                        @endphp
                                        @foreach ($colocations as $colocation)
                                          <tr>
                                              @php
                                                  $product  = !empty ( $colocation->product ) ? $colocation->product : null;
                                              @endphp
                                              <td>
                                                  Đặt hàng {{  $product->name }}
                                              </td>
                                              <td class="text-center">
                                                 {{ $billing_time[$colocation->billing_cycle] }}
                                              </td>
                                              <td class="text-center">
                                                {!!number_format($product->pricing[$colocation->billing_cycle],0,",",".") . ' VNĐ'!!}
                                              </td>
                                          </tr>
                                          @if ($invoice->addon_id)
                                            @php
                                                $colocation_config_ip = !empty($colocation->colocation_config_ips[0]) ? $colocation->colocation_config_ips[0] : null;
                                            @endphp
                                            @if ( !empty($colocation_config_ip) )
                                              <tr>
                                                <td>
                                                    Addon {{  $colocation_config_ip->product->name }}
                                                </td>
                                                <td class="text-center">
                                                  {{ $billing_time[$colocation->billing_cycle] }}
                                                </td>
                                                <td class="text-center">
                                                  {!!number_format($colocation_config_ip->product->pricing[$colocation->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                </td>
                                              </tr>
                                            @endif
                                          @endif
                                        @endforeach
                                    @endif
                                    <tr>
                                        <td class="text-right" colspan="2"><b>Số lượng</b></td>
                                        <td class="text-center">{{ $invoice->quantity }}</td>
                                    </tr>
                                    <tr>
                                        <td class="text-right" colspan="2"><b>Thành tiền</b></td>
                                        <td class="text-center">{!!number_format( !empty($invoice->order->total) ? $invoice->order->total : ( $invoice->sub_total * $invoice->quantity ) ,0,",",".") . ' VNĐ'!!}</td>
                                    </tr>

                                @elseif($invoice->type == 'addon_vps')
                                    @php
                                        $add_on_products = GroupProduct::get_addon_product_private(Auth::user()->id);
                                        $billing_cycle = 'monthly';
                                    @endphp
                                    @foreach($invoice->order_addon_vps as $order_addon_vps)
                                        <!-- Addon CPU -->
                                        @if (!empty($order_addon_vps->cpu))
                                          <tr>
                                              <td>
                                                  Add-on CPU ({{ $order_addon_vps->cpu }} cores) {{ !empty($order_addon_vps->vps->ip) ? ' - ' . $order_addon_vps->vps->ip : '' }}
                                              </td>
                                              <td class="text-center">
                                                {{ !empty($order_addon_vps->month) ? $order_addon_vps->month . ' Tháng' : '' }} {{ !empty($order_addon_vps->day) ? $order_addon_vps->day . ' Ngày':  '' }}
                                              </td>
                                              <td class="text-center">
                                                  <?php
                                                      $pricing_addon_cpu = 0;
                                                      $day_cpu = ($order_addon_vps->day > 15) ? 1 : 0.5;
                                                      foreach ($add_on_products as $key => $add_on_product) {
                                                          if (!empty($add_on_product->meta_product->type_addon)) {
                                                              if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                                                $pricing_addon_cpu = $order_addon_vps->month * $order_addon_vps->cpu * $add_on_product->pricing[$billing_cycle] + (int) round( $day_cpu * $order_addon_vps->cpu * $add_on_product->pricing[$billing_cycle], -3);

                                                              }
                                                          }
                                                      }
                                                  ?>
                                                  {!!number_format( $pricing_addon_cpu ,0,",",".") . ' VNĐ'!!}
                                              </td>
                                          </tr>
                                        @endif
                                        <!-- Addon RAM -->
                                        @if (!empty($order_addon_vps->ram))
                                          <tr>
                                              <td>
                                                  Add-on RAM ({{ $order_addon_vps->ram }} GB) {{ !empty($order_addon_vps->vps->ip) ? ' - ' . $order_addon_vps->vps->ip : '' }}
                                              </td>
                                              <td class="text-center">
                                                {{ !empty($order_addon_vps->month) ? $order_addon_vps->month . ' Tháng' : '' }} {{ !empty($order_addon_vps->day) ? $order_addon_vps->day .' Ngày' : '' }}
                                              </td>
                                              <td class="text-center">
                                                  <?php
                                                      $pricing_addon_ram = 0;
                                                      $day_ram = ($order_addon_vps->day > 15) ? 1 : 0.5;
                                                      foreach ($add_on_products as $key => $add_on_product) {
                                                          if (!empty($add_on_product->meta_product->type_addon)) {
                                                              if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                                                $pricing_addon_ram = $order_addon_vps->month * $order_addon_vps->ram * $add_on_product->pricing[$billing_cycle] + (int) round($day_ram  * $order_addon_vps->ram * $add_on_product->pricing[$billing_cycle], -3);
                                                              }
                                                          }
                                                      }
                                                  ?>
                                                  {!!number_format( $pricing_addon_ram ,0,",",".") . ' VNĐ'!!}
                                              </td>
                                          </tr>
                                        @endif
                                        <!-- Addon DISK -->
                                        @if (!empty($order_addon_vps->disk))
                                          <tr>
                                              <td>
                                                  Add-on DISK ({{ $order_addon_vps->disk }} GB) {{ !empty($order_addon_vps->vps->ip) ? ' - ' . $order_addon_vps->vps->ip : '' }}
                                              </td>
                                              <td class="text-center">
                                                {{ !empty($order_addon_vps->month) ? $order_addon_vps->month . ' Tháng' : '' }}  {{ !empty($order_addon_vps->day) ? $order_addon_vps->day . ' Ngày' : '' }}
                                              </td>
                                              <td class="text-center">
                                                  <?php
                                                      $pricing_addon_disk = 0;
                                                      $day_disk = ($order_addon_vps->day > 15) ? 1 : 0.5;
                                                      foreach ($add_on_products as $key => $add_on_product) {
                                                          if (!empty($add_on_product->meta_product->type_addon)) {
                                                              if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                                                $pricing_addon_disk = $order_addon_vps->month * $order_addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10 + (int) round($day_disk  * $order_addon_vps->disk * $add_on_product->pricing[$billing_cycle], -3)/10;
                                                              }
                                                          }
                                                      }
                                                  ?>
                                                  {!!number_format( $pricing_addon_disk,0,",",".") . ' VNĐ'!!}
                                              </td>
                                          </tr>
                                        @endif
                                        <tr>
                                            <td class="text-right" colspan="2"><b>Thành tiền</b></td>
                                            <td class="text-center">{!!number_format( !empty($invoice->order->total) ? $invoice->order->total : 0 ,0,",",".") . ' VNĐ'!!}</td>
                                        </tr>
                                    @endforeach

                                @elseif($invoice->type == 'change_ip')
                                    @php
                                        $product = GroupProduct::get_product_change_ip(Auth::user()->id);
                                    @endphp
                                    @foreach($invoice->order_change_vps as $order_change_vps)
                                      <tr>
                                        <td>Thay đổi IP của VPS - {{ !empty($order_change_vps->ip) ? $order_change_vps->ip : '' }}</td>
                                        <td></td>
                                        <td class="text-center">20.000 VNĐ</td>
                                      </tr>
                                    @endforeach
                                    <tr>
                                        <td class="text-right" colspan="2"><b>Thành tiền</b></td>
                                        <td class="text-center">{!!number_format(20000 * $invoice->order_change_vps->count(),0,",",".") . ' VNĐ'!!}</td>
                                    </tr>

                                {{-- Hóa đơn đăng ký tên miền --}}
                                @elseif($invoice->type == 'Domain')
                                    @php
                                        $domains = $invoice->domains;
                                    @endphp
                                    @foreach ($domains as $domain)
                                        @php
                                            $product_domain = $domain->domain_product;
                                        @endphp
                                        <tr>
                                            <td>
                                                Đặt hàng tiên miền - {{  $domain->domain }}
                                            </td>
                                            <td class="text-center">
                                            {{-- {{ $billing_time[$invoice->domains[0]->billing_cycle] }} --}}
                                                {{ $domain->domain_year }} Năm
                                            </td>
                                            <td class="text-center">
                                                {!!number_format($product_domain[$domain->billing_cycle],0,",",".") . ' VNĐ'!!}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="text-right" colspan="2"><b>Thành tiền</b></td>
                                            <td class="text-center">{!!number_format($invoice->sub_total,0,",",".") . ' VNĐ'!!}</td>
                                        </tr>
                                    @endforeach

                                {{-- Hóa đơn gia hạn domain --}}
                                @elseif($invoice->type == 'Domain_exp')
                                    @php
                                        $billing_time_domain_exp = config('billing_domain_exp');
                                    @endphp
                                    @if ($invoice->order->type == 'expired')
                                        @php
                                            $domain_expireds = $invoice->domain_expireds;
                                        @endphp
                                        @foreach ($domain_expireds as $domain_expired)
                                            @php
                                                $domain = GroupProduct::get_domain_by_id($domain_expired->domain_id);
                                                $product_domain = GroupProduct::get_product_domain($domain->product_domain_id);
                                            @endphp
                                            <tr>
                                                <td>
                                                    Gia hạn tên miền - {{ $domain->domain }}
                                                </td>
                                                <td class="text-center">
                                                {{ $billing_time_domain_exp[$domain_expired->billing_cycle] }} Năm
                                                </td>
                                                <td class="text-center">
                                                    {!!number_format($product_domain[$domain_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text-right" colspan="2"><b>Thành tiền</b></td>
                                                <td class="text-center">{!!number_format($invoice->sub_total,0,",",".") . ' VNĐ'!!}</td>
                                            </tr>
                                        @endforeach
                                    @endif
                                @elseif ($invoice->type == 'upgrade_hosting')
                                    @php
                                      $upgrade_hosting = $invoice->order_upgrade_hosting;
                                    @endphp
                                    <tr>
                                        <td>
                                            Nâng cấp Hosting: {{ $upgrade_hosting->hosting->domain }} <br>
                                            Gói Hosting: {{ $upgrade_hosting->product->name }}
                                        </td>
                                        <td class="text-center">
                                        {{ $billing_time[$upgrade_hosting->hosting->billing_cycle] }}
                                        </td>
                                        <td class="text-center">
                                            {!!number_format($invoice->sub_total,0,",",".") . ' VNĐ'!!}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-right" colspan="2"><b>Thành tiền</b></td>
                                        <td class="text-center">{!!number_format($invoice->sub_total,0,",",".") . ' VNĐ'!!}</td>
                                    </tr>
                                @elseif ($invoice->type == 'Email Hosting')
                                  @if($invoice->order->type == 'expired')
                                      @php
                                          $order_expireds = $invoice->order_expireds;
                                      @endphp
                                      @foreach ($order_expireds as $order_expired)
                                          @php
                                              $product = GroupProduct::get_product($order_expired->email_hosting->product_id);
                                          @endphp
                                          <tr>
                                              <td>
                                                  Gia hạn Email Hosting - {{ !empty($order_expired->email_hosting->domain) ? $order_expired->email_hosting->domain : '' }}
                                              </td>
                                              <td class="text-center">
                                                 {{ $billing_time[$order_expired->billing_cycle] }}
                                              </td>
                                              <td class="text-center">
                                                  @if ($order_expired->email_hosting->billing_cycle == $order_expired->billing_cycle)
                                                      {!!number_format(!empty($order_expired->email_hosting->price_override) ? $order_expired->email_hosting->price_override : $product->pricing[$order_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                  @else
                                                      {!!number_format($product->pricing[$order_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                  @endif
                                              </td>
                                          </tr>
                                      @endforeach
                                  @else
                                      @foreach ($invoice->email_hostings as $email_hosting)
                                      <tr>
                                          @php
                                              $product = GroupProduct::get_product($email_hosting->product_id);
                                          @endphp
                                          <td>
                                              {{  GroupProduct::get_product($email_hosting->product_id)->name }} - {{ $email_hosting->domain }}
                                          </td>
                                          <td class="text-center">
                                             {{ $billing_time[$email_hosting->billing_cycle] }}
                                          </td>
                                          <td class="text-center">
                                              {!!number_format($product->pricing[$email_hosting->billing_cycle],0,",",".") . ' VNĐ'!!}

                                          </td>
                                      </tr>
                                      @endforeach
                                  @endif
                                  <tr>
                                      <td class="text-right" colspan="2"><b>Thành tiền</b></td>
                                      <td class="text-center" colspan="2">{!!number_format(!empty($invoice->order->total) ? $invoice->order->total : 0,0,",",".") . ' VNĐ'!!}</td>
                                  </tr>
                                @else
                                    @if($invoice->order->type == 'expired')
                                        @php
                                            $order_expireds = $invoice->order_expireds;
                                        @endphp
                                        @foreach ($order_expireds as $order_expired)
                                            @php
                                                $product = GroupProduct::get_product($order_expired->hosting->product_id);
                                            @endphp
                                            <tr>
                                                <td>
                                                    Gia hạn Hosting - {{ !empty($order_expired->hosting->domain) ? $order_expired->hosting->domain : '' }}
                                                </td>
                                                <td class="text-center">
                                                   {{ $billing_time[$order_expired->billing_cycle] }}
                                                </td>
                                                <td class="text-center">
                                                    @if ($order_expired->hosting->billing_cycle == $order_expired->billing_cycle)
                                                        {!!number_format(!empty($order_expired->hosting->price_override) ? $order_expired->hosting->price_override : $product->pricing[$order_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                    @else
                                                        {!!number_format($product->pricing[$order_expired->billing_cycle],0,",",".") . ' VNĐ'!!}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        @foreach ($invoice->hostings as $hosting)
                                        <tr>
                                            @php
                                                $product = GroupProduct::get_product($hosting->product_id);
                                            @endphp
                                            <td>
                                                {{  GroupProduct::get_product($hosting->product_id)->name }} - {{ $hosting->domain }}
                                            </td>
                                            <td class="text-center">
                                               {{ $billing_time[$hosting->billing_cycle] }}
                                            </td>
                                            <td class="text-center">
                                                {!!number_format($product->pricing[$hosting->billing_cycle],0,",",".") . ' VNĐ'!!}

                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                    <tr>
                                        <td class="text-right" colspan="2"><b>Thành tiền</b></td>
                                        <td class="text-center" colspan="2">{!!number_format(!empty($invoice->order->total) ? $invoice->order->total : 0,0,",",".") . ' VNĐ'!!}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.row -->
                {{-- row --}}
                <div class="invoice-paid">
                    @php
                        $user = UserHelper::get_user(Auth::user()->id);
                    @endphp
                        @if (!empty($invoice->paid_date))
                        @else
                            <div class="payment-method row">
                                <div class="col-md-12 col-lg-6">
                                </div>
                                <div class="col-md-12 col-lg-6">
                                    <p class="lead">Ngày kết thúc: {{ date('d-m-Y', strtotime($invoice->due_date)) }}</p>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th>Thành tiền</th>
                                                    <td>{!!number_format(!empty($invoice->order->total) ? $invoice->order->total : 0,0,",",".") . ' VNĐ'!!}</td>
                                                </tr>
                                                <tr>
                                                    <th>Số dư tài khoản</th>
                                                    <td>
                                                        @if(!empty($user->credit->value))
                                                            {!!number_format($user->credit->value,0,",",".") . ' VNĐ'!!}
                                                        @else
                                                            0 VNĐ
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            {{-- row --}}
                            <div class="row no-print">
                                <div class="col-md-12">
                                    <a href="{{ route('invoice.prince', $invoice->id) }}" target="_blank">
                                        <button type="button" class="btn btn-default float-left m-1"><i class="fas fa-print"></i> Print</button>
                                    </a>
                                    @if($invoice->order->type == 'expired')
                                        @if ($invoice->type == 'Domain_exp')
                                            <a href="{{ route('invoice.payment_domain_expired_order', $invoice->id) }}" class="text-light">
                                                <button type="button" class="btn btn-success float-right m-1 text-white">
                                                <input type="hidden" id="invoice_id" name="invoice_id" value="{{ $invoice->id }}">
                                                <i class="far fa-credit-card"></i> Thanh toán
                                                </button>
                                            </a>
                                        @else
                                            <a href="{{ route('invoice.payment_expired', $invoice->id) }}" class="text-light">
                                              <button type="button" class="btn btn-success float-right m-1 text-white">
                                                <input type="hidden" id="invoice_id" name="invoice_id" value="{{ $invoice->id }}">
                                                <i class="far fa-credit-card"></i> Thanh toán
                                              </button>
                                            </a>
                                        @endif
                                    @elseif($invoice->type == 'addon_vps')
                                        <a href="{{ route('invoice.payment_addon', $invoice->id) }}" class="text-light">
                                          <button type="button" class="btn btn-success float-right m-1 text-white">
                                            <input type="hidden" id="invoice_id" name="invoice_id" value="{{ $invoice->id }}">
                                            <i class="far fa-credit-card"></i> Thanh toán
                                          </button>
                                        </a>
                                    @elseif($invoice->type == 'change_ip')
                                        <a href="{{ route('invoice.payment_change_ip', $invoice->id) }}" class="text-light">
                                          <button type="button" class="btn btn-success float-right m-1 text-white">
                                            <input type="hidden" id="invoice_id" name="invoice_id" value="{{ $invoice->id }}">
                                            <i class="far fa-credit-card"></i> Thanh toán
                                          </button>
                                        </a>
                                    @elseif($invoice->type == 'Domain')
                                        <a href="{{ route('invoice.payment_domain_order', $invoice->id) }}" class="text-light">
                                            <button type="button" class="btn btn-success float-right m-1 text-white">
                                            <input type="hidden" id="invoice_id" name="invoice_id" value="{{ $invoice->id }}">
                                            <i class="far fa-credit-card"></i> Thanh toán
                                            </button>
                                        </a>
                                    @elseif($invoice->type == 'upgrade_hosting')
                                        <a href="{{ route('invoice.payment_upgrade_hosting', $invoice->id) }}" class="text-light">
                                            <button type="button" class="btn btn-success float-right m-1 text-white">
                                            <input type="hidden" id="invoice_id" name="invoice_id" value="{{ $invoice->id }}">
                                            <i class="far fa-credit-card"></i> Thanh toán
                                            </button>
                                        </a>
                                    @else
                                        <a href="{{ route('invoice.payment_invoice', $invoice->id) }}" class="text-light">
                                          <button type="button" class="btn btn-success float-right m-1 text-white">
                                            <input type="hidden" id="invoice_id" name="invoice_id" value="{{ $invoice->id }}">
                                            <i class="far fa-credit-card"></i> Thanh toán
                                          </button>
                                        </a>
                                    @endif
                                    <!-- <form action="{{ route('invoice.payment') }}" method="post">
                                        @csrf
                                        <button type="submit" class="btn btn-success float-right m-1"><i class="far fa-credit-card"></i> Thanh toán</button>
                                    </form> -->
                                </div>
                            </div>
                            {{-- /row --}}
                        @endif
                </div>
                {{-- /row --}}
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}"></script>
<script src="{{ asset('js/user_order.js') }}"></script>
<script src="{{ asset('js/payment_live.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@endsection
