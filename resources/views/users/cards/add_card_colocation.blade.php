@extends('layouts.user2.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Thiết lập</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Thiết lập</li>
@endsection
@section('content')
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger-order" style="margin-top:20px">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4 class="text-center mb-4">Cấu hình tùy chọn và tiếp tục thanh toán</h4>
            </div>
            <div class="col-12 col-lg-7">
                <form action="{{ route('user.order') }}" method="post" id="form-user-order">
                <div class="product-info rounded">
                    <p class="product-title">
                      {{ $product->name }}
                      @if(!empty($product->pricing[$billing_cycle]))
                        ({!! number_format($product->pricing['monthly'],0,",",".") !!}/tháng)
                      @endif
                    </p>
                    <p class="config-product">
                        Loại: {{ !empty($product->meta_product->os) ? $product->meta_product->os : '1 U'  }} <br>
                        IP: {{ $product->meta_product->ip  }} IP public 
                        @if (!empty($list_add_on_ip))
                            @if (count($list_add_on_ip))
                                <button type="button" class="btn btn-sm btn-outline-primary" id="btn_addon_ip_colo" data-type="plus" data-toggle="tooltip" data-placement="top" title="Addon IP"><i class="fas fa-plus"></i></button>
                            @endif 
                        @endif
                        <br>
                        Băng thông: {{ $product->meta_product->bandwidth  }} <br>
                        @if(!empty($product->meta_product->product_special))
                          <span class="text-danger">Ghi chú: Không áp dụng nâng cấp cấu hình với các gói đặc biệt</span>
                        @endif
                    </p>
                </div>
                @if(!empty($product->pricing->one_time_pay))
                  <input type="hidden" name="one_time_pay" id="one_time_pay" value="{{ $product->pricing->one_time_pay }}">
                @else
                    <div class="form-group" id="select-billing">
                        <h5 style="display: none">Thời gian:
                            @if( $billing_cycle == 'one_time' )
                            Vĩnh viển
                            @elseif( $billing_cycle == 'free' )
                            Free
                            @elseif( $billing_cycle == 'annually' )
                            1 Năm
                            @elseif( $billing_cycle == 'monthly' )
                            1 Tháng
                            @elseif( $billing_cycle == 'quarterly' )
                            3 Tháng
                            @elseif( $billing_cycle == 'semi_annually' )
                            6 Tháng
                            @elseif( $billing_cycle == 'biennially' )
                            2 Năm
                            @elseif( $billing_cycle == 'triennially' )
                            3 Năm
                            @endif
                        </h5>
                        <input type="hidden" id="billing-price" name="billing-price" value="{{ $billing_cycle }}" data-pricing="{{ $product->pricing[$billing_cycle] }}">
                    </div>
                @endif
                <div class="form-group">
                    <label for="">Số lượng</label>
                    <input type="text" value="{{ !empty(old('quantity'))? old('quantity') : 1 }}" name="quantity" id="qtt" class="form-control" placeholder="Số lượng">
                    <div class="quantity-order mb-4 mt-4">

                    </div>
                </div>
                <div class="form-group">
                    <label for="datacenter" class="">Datacenter</label>
                    <select class="form-control" id="datacenter" name="datacenter">
                        {{-- <option value="All">Chọn tất cả</option> --}}
                        @if (!empty($list_datacenter))
                            @foreach ($list_datacenter as $datacenter)
                                <option value="{{ $datacenter->datacenter }}">{{ $datacenter->datacenter }}</option>
                            @endforeach
                        @else
                            @foreach ($config_datacenter as $datacenter)
                                <option value="{{ $datacenter }}">{{ $datacenter }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group" id="addon_ip_colo" style="display: none;">
                    <label for="addon_ip" class="">Addon IP</label>
                    <select class="form-control" id="addon_ip" name="addon_ip">
                        <option value="0">None</option>
                        @if (!empty($list_add_on_ip))
                            @if (count($list_add_on_ip))
                                @foreach ($list_add_on_ip as $add_on_ip)
                                    <option value="{{ $add_on_ip->id }}">
                                        {{ $add_on_ip->name }} - 
                                        {{ !empty($add_on_ip->pricing[$billing_cycle]) ? number_format($add_on_ip->pricing[$billing_cycle],0,",",".") : 0 }} 
                                        ₫/{{ !empty($billings[$billing_cycle]) ? $billings[$billing_cycle] : '1 Tháng' }}
                                    </option>
                                @endforeach
                            @endif
                        @endif
                    </select>
                </div>
            </div>
            <div class="col-12 col-lg-5">
                <div class="form-group">
                    <label for="">Thời gian thanh toán</label>
                    <select name="billing" class="form-control select2" id="billing">
                         <option value="" disabled>Chọn thời gian</option>
                         @php
                             $term = false;
                         @endphp
                         @foreach ($billings as $key => $billing)
                             @php
                                 $selected = '';
                                 $disabled = false;
                                 if (!$term) {
                                     if (!empty($product->pricing[$key])) {
                                         $term = true;
                                     }
                                 }
                                 if (empty($product->pricing[$key])) {
                                     $disabled = true;
                                 }
                             @endphp
                             @if(!$disabled)
                               <option value="{{$key}}" @if($key === $billing_cycle) selected @endif data-pricing="{{ $product->pricing[$key] }}">{!!number_format($product->pricing[$key],0,",",".") . ' VNĐ'!!} / {{$billing}}</option>
                             @endif
                         @endforeach
                     </select>
                </div>
                <div id="ordersumm">
                    {{-- <div class="ordersummarytitle">
                        Tóm tắt đơn hàng
                    </div> --}}
                    <div id="ordersummary" style="display: none">
                        <div class="before-send">
                        </div>
                        <div class="order_sum">
                            <div class="ordersummary_product">
                                <div class="item itemproduct">
                                    <div class="itemtitle">
                                        {{ $product->group_product->name }} - {{ $product->name }}
                                    </div>
                                    <div class="row">
                                        @if ($billing_cycle == 'free')
                                            <div class="col col-md-6 item_billing">
                                                Free
                                            </div>
                                            <div class="col col-md-6 item_pricing text-right">
                                                0 VNĐ
                                            </div>
                                            <div class="hidden">
                                                <input type="hidden" name="sub_total" id="sub_total" value="0">
                                            </div>
                                        @elseif ($billing_cycle == 'one_time')
                                            <div class="col col-md-6 item_billing">
                                                Vĩnh viễn
                                            </div>
                                            <div class="col col-md-6 item_pricing text-right">
                                                {!!number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'!!}
                                            </div>
                                            <div class="hidden">
                                                <input type="hidden" name="sub_total" id="sub_total" value="{{ $product->pricing->one_time_pay }}">
                                            </div>
                                        @else
                                            @if ($billing_cycle == 'monthly')
                                                <div class="col col-md-6 item_billing">
                                                    1 Tháng
                                                </div>
                                                <div class="col col-md-6 item_pricing text-right">
                                                    {!!number_format($product->pricing->monthly,0,",",".") . ' VNĐ'!!}
                                                </div>
                                                <div class="hidden">
                                                    <input type="hidden" name="sub_total" id="sub_total" value="{{ $product->pricing->monthly }}">
                                                </div>
                                            @elseif ($billing_cycle == 'quarterly')
                                                <div class="col col-md-6 item_billing">
                                                    3 Tháng
                                                </div>
                                                <div class="col col-md-6 item_pricing text-right">

                                                    {!!number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'!!}
                                                </div>
                                                <div class="hidden">
                                                    <input type="hidden" name="sub_total" id="sub_total" value="{{ $product->pricing->quarterly }}">
                                                </div>
                                            @elseif ($billing_cycle == 'semi_annually')
                                                <div class="col col-md-6 item_billing">
                                                    6 Tháng
                                                </div>
                                                <div class="col col-md-6 item_pricing text-right">
                                                    {!!number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'!!}
                                                </div>
                                                <div class="hidden">
                                                    <input type="hidden" name="sub_total" id="sub_total" value="{{ $product->pricing->semi_annually }}">
                                                </div>
                                            @elseif ($billing_cycle == 'annually')
                                                <div class="col col-md-6 item_billing">
                                                    1 Năm
                                                </div>
                                                <div class="col col-md-6 item_pricing text-right">

                                                    {!!number_format($product->pricing->annually,0,",",".") . ' VNĐ'!!}
                                                </div>
                                                <div class="hidden">
                                                    <input type="hidden" name="sub_total" id="sub_total" value="{{ $product->pricing->annually }}">
                                                </div>
                                            @elseif ($billing_cycle == 'biennially')
                                                <div class="col col-md-6 item_billing">
                                                    2 Năm
                                                </div>
                                                <div class="col col-md-6 item_pricing text-right">

                                                    {!!number_format($product->pricing->biennially,0,",",".") . ' VNĐ'!!}
                                                </div>
                                                <div class="hidden">
                                                    <input type="hidden" name="sub_total" id="sub_total" value="{{ $product->pricing->biennially }}">
                                                </div>
                                            @else
                                                <div class="col col-md-6 item_billing">
                                                    3 Năm
                                                </div>
                                                <div class="col col-md-6 item_pricing text-right">

                                                    {!!number_format($product->pricing->triennially,0,",",".") . ' VNĐ'!!}
                                                </div>
                                                <div class="hidden">
                                                    <input type="hidden" name="sub_total" id="sub_total" value="{{ $product->pricing->triennially }}">
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                <div class="addon-item-cpu">

                                </div>
                                <div class="addon-item-ram">

                                </div>
                                <div class="addon-item-disk">

                                </div>
                                <div class="addon-item-storage">

                                </div>
                                <div class="addon-item-database">

                                </div>
                            </div>
                            <div class="sub_total">
                                <div class="row">
                                    <div class="col col-md-3">
                                        Sub Total
                                    </div>
                                    <div class="col col-md-9 sub_total_pricing">
                                        0 VNĐ
                                    </div>
                                </div>
                            </div>
                            <div class="so-luong">
                                <div class="row">
                                    <div class="col col-md-3">
                                        Số lượng
                                    </div>
                                    <div class="col col-md-9 quantity-printf">
                                        1
                                    </div>
                                </div>
                            </div>
                            <div class="total">
                                <div class="row">
                                    <div class="col col-md-3">
                                        Total
                                    </div>
                                    <div class="col col-md-9 total_pricing">
                                        0 VNĐ
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="margin: 30px 0;">
                        <div class="card-header">
                          <h3 class="card-title mb-0">Tóm tắt đơn hàng</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0 summary">
                          <table class="table table-sm table-bordered" id="table-show-price">
                            <thead>
                              <tr>
                                <th>Sản phẩm</th>
                                <th>Số lượng</th>
                                <th class="text-center" style="width: 125px">Chi phí</th>
                              </tr>
                            </thead>
                            <tbody class="show-select-addon">
                              <tr>
                                <td>{{ $product->group_product->name }} - {{ $product->name }}</td>
                                <td class="quantity-printf text-left"></td>
                                <td class="item_pricing_cell text-right"></td>
                              </tr>
                              <tr class="addon-item-ip">
                              </tr>
                              <tr class="addon-item-ram">
                              </tr>
                              <tr class="addon-item-disk">
                              </tr>
                              <tr class="detail-promotion">
                              </tr>
                            </tbody>
                            <tfoot style="background: #e7ffda">
                                <tr>
                                    <td class="text-right" colspan="2">Tổng cộng</td>
                                    <td class="total_pricing"></td>
                                </tr>
                            </tfoot>
                          </table>
                        </div>
                        <!-- /.card-body -->
                      </div>
                </div>
                <div class="button_submit text-center">
                    @csrf
                    <input type="hidden" name="product_id" value="{{ $product->id }}" id='product_id'>
                    <input type="hidden" name="type_product" value="{{ $product->type_product }}" id="type_product">
                    <input type="submit" class="btn btn-block btn-primary" value="Đặt hàng">
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="product_special" value="{{ $product->meta_product->product_special }}">
<input type="hidden" id="total_order" value="{{ !empty($product->pricing[$billing_cycle]) ? $product->pricing[$billing_cycle] : $product->pricing['monthly'] }}">
<input type="hidden" id="promotion_order" data-promotion="" value="">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}"></script>
<script src="{{ asset('js/user_order_colo.js') }}?token={{ date('YmdH') }}"></script>
<script src="{{ asset('js/cutomer.js')  }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
        $('[data-toggle="tooltip-security"]').tooltip();
    });
</script>
<script>
    $("input[type='number']").inputSpinner();
</script>
@endsection