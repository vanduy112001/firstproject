<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport"  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->

    <title>Cloudzone</title>

    <style>
        body {
            font-family: DejaVu Sans;
        }
    </style>
</head>
<body id="page-top">
    <div id="wrapper">
        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">
            <!-- Main Content -->
            <div id="content">
                <div id="app">
                    <div class="content-row">
                        <div id="invoice-page">
                            <!-- Main content -->
                            <div class="invoice p-3 mb-3">
                                <!-- title row -->
                                <div class="row invoice-header">
                                    <div class="col col-md-6 logo">
                                        <img src="{{ url('images/LogoCloud.png') }}" alt="cloudzone.vn" width="200">
                                        <p class="invoice-number">Hóa đơn số #{{ $invoice->id }}</p>
                                    </div>
                                    <div class="col col-md-6 invoice-status text-center">
                                        @if (!empty($invoice->paid_date))
                                            <div class="paid">Đã thanh toán</div>
                                            <div class="date">Ngày thanh toán: {{ date('d-m-Y', strtotime($invoice->paid_date)) }}</div>
                                            <div class="dua_date">Ngày kết thúc: {{ date('d-m-Y', strtotime($invoice->dua_date)) }}</div>
                                        @else
                                            <div class="unpaid">Chưa thanh toán</div>
                                            <div class="dua_date">Ngày kết thúc: {{ date('d-m-Y', strtotime($invoice->due_date)) }}</div>
                                        @endif
                                    </div>
                                <!-- /.col -->
                                </div>
                                <hr>
                                <!-- info row -->
                                <div class="row invoice-info">
                                    <div class="col-sm-6 invoice-col">
                                        <div class="invoice-info-header">
                                            <strong>Nhà cung cấp dịch vụ</strong>
                                        </div>
                                        <address>
                                            Công ty TNHH MTV Đại Việt Số<br>
                                            257 Lê Duẩn, Tân Chính, Thanh Khê, Đà Nẵng<br>
                                            SĐT: 08 8888 0043<br>
                                            Email: info@cloudzone.vn
                                        </address>
                                    </div>
                                    <!-- /.col -->
                                    @php
                                        $user = UserHelper::get_user(Auth::user()->id);
                                    @endphp
                                    <div class="col-sm-6 invoice-col text-right">
                                        <div class="invoice-info-header">
                                            <strong>Thông tin khách hàng</strong>
                                        </div>
                                        <address>
                                            {{ $user->name }}<br>
                                            {{ $user->user_meta->address }}<br>
                                            SĐT: {{ $user->user_meta->phone }}<br>
                                            Email: {{ $user->email }}
                                        </address>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <div class="row">
                                    <div class="col col-md-12 table-responsive">
                                        <table class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Mô tả chi tiết</th>
                                                    <th class="text-center">Số tiền</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if ($invoice->type == 'VPS' )
                                                    <tr>
                                                        <td>
                                                            {{  GroupProduct::get_product($invoice->vps[0]->product_id)->name }}
                                                        </td>
                                                        <td class="text-center">
                                                            {!!number_format($invoice->sub_total,0,",",".") . ' VNĐ'!!}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right"><b>Số lượng</b></td>
                                                        <td class="text-center">{{ $invoice->quantity }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right"><b>Thành tiền</b></td>
                                                        <td class="text-center">{!!number_format($invoice->sub_total * $invoice->quantity,0,",",".") . ' VNĐ'!!}</td>
                                                    </tr>
                                                @elseif($invoice->type == 'Server')
                                                    <tr>
                                                        <td>
                                                            {{  GroupProduct::get_product($invoice->servers[0]->product_id)->name }}
                                                        </td>
                                                        <td class="text-center">
                                                            {!!number_format($invoice->sub_total,0,",",".") . ' VNĐ'!!}
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right"><b>Số lượng</b></td>
                                                        <td class="text-center">{{ $invoice->quantity }}</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-right"><b>Thành tiền</b></td>
                                                        <td class="text-center">{!!number_format($invoice->sub_total * $invoice->quantity,0,",",".") . ' VNĐ'!!}</td>
                                                    </tr>
                                                @else
                                                    @foreach ($invoice->hostings as $hosting)
                                                    <tr>
                                                        <td>
                                                            {{  GroupProduct::get_product($hosting->product_id)->name }} - {{ $hosting->domain }}
                                                        </td>
                                                        <td class="text-center">
                                                            {!!number_format($invoice->sub_total,0,",",".") . ' VNĐ'!!}
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    <tr>
                                                        <td class="text-right"><b>Thành tiền</b></td>
                                                        <td class="text-center">{!!number_format($invoice->sub_total * $invoice->quantity,0,",",".") . ' VNĐ'!!}</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- /.row -->
                                {{-- row --}}
                            </div>
                        </div>
                    </div>
                    <!-- End of Main Content -->
                </div>
            </div>
        </div>
    </div>
</body>
</html>
