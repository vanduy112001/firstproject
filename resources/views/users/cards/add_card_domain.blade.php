@php
    $user = Auth::user();
@endphp
@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/bootstrap-datepicker/dist//css/bootstrap-datepicker.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Tùy chọn và thanh toán</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Thiết lập</li>
@endsection
@section('content')
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                <div class="bg-success">
                    <p class="text-light">{{session("success")}}</p>
                </div>
                @elseif(session("fails"))
                <div class="bg-danger">
                    <p class="text-light">{{session("fails")}}</p>
                </div>
                @endif
                @if ($errors->any())
                <div class="alert alert-danger-order" style="margin-top:20px">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
            </div>
            <div class="col-12 col-lg-7">
            <form action="{{ route('user.order_domain') }}" method="post" enctype="multipart/form-data" id="form-user-order">
                    {{-- <p>Tùy chọn và thanh toán.</p> --}}
                    <div class="product-info rounded">
                        <p class="product-title"> Sản phẩm</p>
                        <p class="config-product">
                            Domain:  {{ $domain }} <br>
                            Vùng:    {{ $product->local_type }} <br>
                        </p>
                    </div>
                    <div class="form-group" id="select-billing">
                        <label for="">Chọn thời gian thanh toán</label>
                        <select name="billing" class="form-control select2" id="billing">
                            <option value="" disabled>Chọn thời gian</option>
                            @php
                                $term = false;
                            @endphp
                            @foreach ($billings as $key => $billing)
                                @php
                                    $selected = '';
                                    $disabled = false;
                                    if (!$term) {
                                        if (!empty($product->$key)) {
                                            $term = true;
                                            $selected = 'selected';
                                        }
                                    }
                                    if (empty($product->$key)) {
                                        $disabled = true;
                                    }
                                @endphp
                                @if(!$disabled)
                                    <option value="{{$key}}" {{ $selected }} data-pricing="{{ $product->$key }}" data-year="{{$billing}}">
                                    {!!number_format($product->$key,0,",",".") . ' VNĐ'!!} / {{$billing}} Năm</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <label class="text-danger">Lưu ý: Các thông tin có dấu * là bắt buộc</label><br>
                    <div class="form-group">
                        <label for="password-domain" class="required">Mật khẩu domain</label>
                        <div class="password-domain">
                            <input type="text" name="password-domain" id="password-domain" placeholder="8-15 kí tự (phải có chữ và số), không có kí tự đặt biệt" class="form-control" title="Lưu ý: Password domain phải có độ dài từ 8 đến 15 ký tự (phải bao gồm cả số và chữ từ a-z, 0-9)." value="{{ old('password-domain') }}" pattern="(?=.*\d)(?=.*[a-z])[a-z0-9]+" minlength="8" maxlength="15" required>
                            <div class="luu-y">
                                Lưu ý: Password domain phải có độ dài từ 8 đến 15 ký tự (phải bao gồm cả số và chữ từ a-z, 0-9 và không có kí tự đặt biệt).
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="customer-domain" class="required">Khách hàng sử dụng</label>
                        <div class="customer-domain">
                            <input type="radio" name="customer-domain" id="canhan" value="canhan" checked> <label for="canhan">Cá nhân</label>
                            <input type="radio" name="customer-domain" id="congty" value="congty"> <label for="congty">Công ty</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="checkbox-zoom zoom-primary">
                            <label>
                                <input class="get-profile-info" type="checkbox" value="">
                                <span class="cr"><i class="fa fa-check cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                <span>Lấy thông tin cá nhân để tự động điền vào form bên dưới</span><span class="text-danger font-weight-normal"> (Chú ý: Cập nhập tất cả <a class="text-danger font-weight-bold" href="{{ route('profile') }}">thông tin cá nhân </a> để form tự động điền đầy đủ nhất.)</span>
                            </label>
                        </div>
                    </div>
                    {{-- Thông tin user đặt hàng --}}
                    <div class="owner-info">
                        <div class="card card-outline card-primary">
                            <div class="card-header"><h5>Thông tin khách hàng</h5></div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="owner-name" class="required">Tên (cá nhân bắt buộc viết theo CMND có dấu, nếu là công ty bắt buộc viết theo giấy ĐKKD có dấu)</label>
                                    <div class="owner-name">
                                    <input type="text" name="owner-name" id="owner-name" placeholder="Nhập tên" class="form-control" value="{{ old('owner-name') ? old('owner-name') : $user->name }}" title="Tên của khách hàng" required>
                                    </div>
                                </div>
                                <div class="form-group ownerid-number">
                                    <label for="ownerid-number" class="required">Chứng minh thư</label>
                                    <div class="ownerid-number-input">
                                        <input type="text" name="ownerid-number" id="ownerid-number" placeholder="CMT phải trùng thông tin với tên khách hàng" class="form-control" value="{{ old('ownerid-number') ? old('ownerid-number') : $user->user_meta->cmnd }}" title="Số chứng minh thư của chủ thể" required>
                                    </div>
                                </div>
                                <div class="form-group owner-taxcode">
                                    {{-- <label for="owner-taxcode" class="required">Mã số thuế </label>
                                    <div class="owner-taxcode">
                                        <input type="text" name="owner-taxcode" id="owner-taxcode" placeholder="Bắt buộc đối với khách hàng công ty" class="form-control" required>
                                    </div> --}}
                                </div>
                                <div class="form-group">
                                    <label for="owner-address" class="required">Địa chỉ (theo CMND)</label>
                                    <div class="owner-address">
                                        <input type="text" name="owner-address" id="owner-address" placeholder="Nhập địa chỉ" class="form-control" value="{{ old('owner-address') ? old('owner-address') : $user->user_meta->address }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="owner-email" class="required">Email </label>
                                    <div class="owner-email">
                                        <input type="email" name="owner-email" id="owner-email" placeholder="Nhập email" class="form-control" pattern="[^ @]*@[^ @]*" value="{{ old('owner-email') ? old('owner-email') : $user->email }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="owner-phone" class="required">Phone </label>
                                    <div class="owner-phone">
                                    <input type="text" name="owner-phone" id="owner-phone" placeholder="Nhập sđt" class="form-control" value="{{ old('owner-phone') ? old('owner-phone') : $user->user_meta->phone }}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ui-info">
                        <div class="card card-outline card-primary">
                            <div class="card-header"><h5>Thông tin chủ thể (để lưu thông tin whois)</h5></div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="ui-name" class="required">Tên</label>
                                    <div class="ui-name">
                                        <input type="text" name="ui-name" id="ui-name" placeholder="Tên chủ thể hoặc công ty" class="form-control" value="{{ old('ui-name') }}" required>
                                    </div>
                                </div>

                                @if ($product->local_type == 'Việt Nam')
                                    <div class="form-group">
                                        <label for="uiid-number">Chứng minh thư</label>
                                        <div class="uiid-number">
                                            <input type="text" name="uiid-number" id="uiid-number" placeholder="Nhập CMT chủ thể" class="form-control"  value="{{ old('uiid-number') }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group" >
                                        <label for="ownerid-scan-before" class="required">Ảnh chứng minh thư mặt trước:</label>
                                        <div class="ownerid-number-input ownerid-scan-before">
                                          <input type="file" name="cmnd_before" id="ownerid-scan-before" title = "Chọn ảnh chứng minh nhân dân mặc trước">
                                        </div>
                                    </div>
                                    <div class="form-group mt-2" >
                                        <label for="ownerid-scan-after" class="required">Ảnh chứng minh thư mặt sau:</label>
                                        <div class="ownerid-number-input ownerid-scan-after">
                                          <input type="file" name="cmnd_after" id="ownerid-scan-after" title = "Chọn ảnh chứng minh nhân dân mặc sau">
                                        </div>
                                    </div>
                                    <div class="form-group ui-gender">
                                        <label for="ui-gender" class="required">Giới tính</label>
                                        <select class="form-control @error('ui-gender') is-invalid @enderror" name="ui-gender" required>
                                            <option value="Nam" @if( old('ui-gender') == 'Nam' ) selected @endif>Nam</option>
                                            <option value="Nữ" @if( old('ui-gender') == 'Nữ' ) selected @endif>Nữ</option>
                                        </select>
                                    </div>
                                    <div class="form-group ui-birthdate ">
                                        <label for="ui-birthdate" class="required">Ngày sinh</label>
                                        <input type="text" name="ui-birthdate" id="ui-birthdate" value="{{ old('ui-birthdate') }}" class="form-control  @error('ui-birthdate') is-invalid @enderror" placeholder="Bắt buộc đối với khách hàng cá nhân" required>
                                        <div class="luu-y">
                                            Lưu ý: Nhập theo đúng định dạng Ngày / Tháng / Năm. Ví dụ: 16/06/1992
                                        </div>
                                    </div>
                                @endif

                                {{-- Quốc tê --}}
                                @if ($product->local_type == 'Quốc tế')
                                    <div class="form-group uiid-number-quoc-te">
                                        <label for="uiid-number">Chứng minh thư (bắt buộc viết theo CMND có dấu)</label>
                                        <div class="uiid-number">
                                            <input type="text" name="uiid-number" id="uiid-number" placeholder="Nhập CMT chủ thể" class="form-control"  value="{{ old('uiid-number') }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group" >
                                        <label for="ownerid-scan-before" class="required">Ảnh chứng minh thư mặt trước:</label>
                                        <div class="ownerid-number-input ownerid-scan-before">
                                          <input type="file" name="cmnd_before" id="ownerid-scan-before">
                                        </div>
                                    </div>
                                    <div class="form-group mt-2" >
                                        <label for="ownerid-scan-after" class="required">Ảnh chứng minh thư mặt sau:</label>
                                        <div class="ownerid-number-input ownerid-scan-after">
                                          <input type="file" name="cmnd_after" id="ownerid-scan-after" title = "Chọn ảnh chứng minh nhân dân mặc sau">
                                        </div>
                                    </div>
                                    <div class="form-group ui-taxcode">
                                        {{-- <label for="owner-taxcode" class="required">Mã số thuế </label>
                                        <div class="owner-taxcode">
                                            <input type="text" name="owner-taxcode" id="owner-taxcode" placeholder="Bắt buộc đối với khách hàng công ty" class="form-control" required>
                                        </div> --}}
                                    </div>
                                @endif

                                <div class="form-group">
                                    <label for="ui-address" class="required">Địa chỉ (Theo CMND)</label>
                                    <div class="ui-address">
                                        <input type="text" name="ui-address" id="ui-address" placeholder="Nhập địa chỉ chủ thể" class="form-control" value="{{ old('ui-address') }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ui-province" class="required">Tỉnh/Thành phố</label>
                                    <select class="form-control @error('ui-province') is-invalid @enderror" name="ui-province" required>
                                        <option value="" disabled selected>---Tỉnh/Thành phố ---</option>
                                        @foreach($list_city as $city)
                                        <?php
                                        $selected = '';
                                        if (old('ui-province') == $city) {
                                          $selected = 'selected';
                                        }
                                        ?>
                                        <option value="{{$city}}" {{$selected}}>{{$city}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="ui-email" class="required">Email </label>
                                    <div class="ui-email">
                                        <input type="email" name="ui-email" id="ui-email" placeholder="Nhập email chủ thể" class="form-control" pattern="[^ @]*@[^ @]*" value="{{ old('ui-email') }}"  required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="ui-phone" class="required">Phone</label>
                                    <div class="ui-phone">
                                        <input type="text" name="ui-phone" id="ui-phone" placeholder="Nhập sđt chủ thể" class="form-control" value="{{ old('ui-phone') }}" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @if ($product->local_type == 'Việt Nam')
                        <div class="admin-info">
                            <div class="card card-outline card-primary">
                                <div class="card-header"><h5>Thông tin người quản lý (để lưu thông tin whois)</h5></div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="admin-name" class="required">Tên</label>
                                        <div class="admin-name">
                                            <input type="text" name="admin-name" id="admin-name" placeholder="Nhập tên người quản lý" class="form-control" value="{{ old('admin-name') ? old('admin-name') : $user->name }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="adminid-number" class="required">Chứng minh thư (bắt buộc viết theo CMND có dấu)</label>
                                        <div class="adminid-number">
                                            <input type="text" name="adminid-number" id="adminid-number" placeholder="Nhập CMT người quản lý" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="admin-gender" class="required">Giới tính</label>
                                        <select class="form-control @error('admin-gender') is-invalid @enderror" name="admin-gender" required>
                                            <option value="Nam" @if( old('admin-gender') == 'Nam' ) selected @endif>Nam</option>
                                            <option value="Nữ" @if( old('admin-gender') == 'Nữ' ) selected @endif>Nữ</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="admin-birthdate" class="required">Ngày sinh</label>
                                        <input type="text" name="admin-birthdate" id="admin-birthdate" value="{{ old('admin-birthdate') }}" class="form-control  @error('admin-birthdate') is-invalid @enderror" placeholder="Ngày sinh" required>
                                        <div class="luu-y">
                                            Lưu ý: Nhập theo đúng định dạng Ngày / Tháng / Năm. Ví dụ: 16/06/1992
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="admin-address" class="required">Địa chỉ (Theo CMND)</label>
                                        <div class="admin-address">
                                            <input type="text" name="admin-address" id="admin-address" placeholder="Nhập địa chỉ" class="form-control" value="{{ old('admin-address') ? old('admin-address') : $user->user_meta->address }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="admin-province" class="required">Tỉnh/Thành phố</label>
                                        <select class="form-control @error('admin-province') is-invalid @enderror" name="admin-province" required>
                                            <option value="" disabled selected>---Tỉnh/Thành phố ---</option>
                                            @foreach($list_city as $city)
                                            <?php
                                            $selected = '';
                                            if (old('admin-province') == $city) {
                                            $selected = 'selected';
                                            }
                                            ?>
                                            <option value="{{$city}}" {{$selected}}>{{$city}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="admin-email" class="required">Email </label>
                                        <div class="admin-email">
                                            <input type="email" name="admin-email" id="admin-email" placeholder="Nhập email" class="form-control" pattern="[^ @]*@[^ @]*" value="{{ old('admin-email') ? old('admin-email') : $user->email }}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="admin-phone" class="required">Phone</label>
                                        <div class="admin-phone">
                                            <input type="text" name="admin-phone" id="admin-phone" placeholder="Nhập sđt" class="form-control" value="{{ old('admin-phone') ? old('admin-phone') : $user->user_meta->phone }}" required>
                                        </div>
                                    </div>
                            </div>
                        </div>
                        </div>
                    @endif


                    {{-- <div class="form-group">
                        <label for="">Chọn khách hàng</label>
                        <div class="khach-hang">
                            <div class="input-kh">
                                <select class="form-control select2" name="makh">
                                    <option value="" disabled>---Chọn khách hàng---</option>
                                    <option value="0">Cho tôi</option>
                                    @foreach($list_customers as $customer)
                                    <option value="{{ $customer->ma_customer }}">
                                        {{ !empty($customer->customer_name) ? $customer->customer_name : $customer->customer_tc_name }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div> --}}

                    @php
                        $domain_name = strstr($domain,'.',true);
                    @endphp

                    @if ($product->local_type == "Việt Nam")

                    @elseif ($product->local_type == "Quốc tế")

                    @endif
                    <div class="form-group">
                        <label for="">Ghi chú</label>
                        <div class="description">
                            <div class="row input-description">
                                <textarea name="description" id="" cols="30" rows="4"
                                    class="form-control">{{ old('description') }}</textarea>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-12 col-lg-5">
                <div id="ordersumm">
                    <div class="ordersummarytitle">
                        Tóm tắt đơn hàng
                    </div>
                    <div id="ordersummary">
                        <div class="order_sum">
                            <div class="ordersummary_product">
                                <div class="item itemproduct">
                                    <div class="itemtitle">
                                        Domain {{ $domain }}
                                    </div>
                                    <div class="row">
                                        @if (!empty($product->annually))
                                        <div class="col col-md-6 item_billing">
                                            1 Năm
                                        </div>
                                        <div class="col col-md-6 item_pricing text-right">

                                            {!!number_format($product->annually,0,",",".") . ' VNĐ'!!}
                                        </div>
                                        @elseif (!empty($product->biennially))
                                        <div class="col col-md-6 item_billing">
                                            2 Năm
                                        </div>
                                        <div class="col col-md-6 item_pricing text-right">

                                            {!!number_format($product->biennially,0,",",".") . ' VNĐ'!!}
                                        </div>
                                        @else
                                        <div class="col col-md-6 item_billing">
                                            3 Năm
                                        </div>
                                        <div class="col col-md-6 item_pricing text-right">
                                            {!!number_format($product->triennially,0,",",".") . ' VNĐ'!!}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{-- <div class="sub_total">
                                <div class="row">
                                    <div class="col col-md-3">
                                        Sub Total
                                    </div>
                                    <div class="col col-md-9 sub_total_pricing">
                                        0 VNĐ
                                    </div>
                                </div>
                            </div> --}}
                            <div class="total">
                                <div class="loading">
                                </div>
                                <div class="row load-total">
                                    <div class="col col-md-3">
                                        Total
                                    </div>
                                    <div class="col col-md-9 total_pricing">
                                        0 VNĐ
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="button_submit text-center">
                    @csrf
                    <input type="hidden" name="product_id" value="{{ $product->id }}" id='product_id'>
                    <input type="hidden" name="type_product" value="Domain" id="type_product">
                    <input type="hidden" name="domain" value="{{ $domain }}" id="domain">
                    <input type="hidden" name="domain-ext" value="{{ $product->type }}" id="domain-ext">
                    <input type="hidden" name="domain-name" value="{{ $domain_name }}" id="domain-name">
                    <input type="hidden" name="domain-year" value="" id="domain-year">
                    <input type="submit" class="btn btn-block btn-primary" value="Đặt hàng">
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
{{-- <script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}"></script> --}}
{{-- <script src="{{ asset('js/user_order_domain.js') }}"></script> --}}
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
    });
    $('#ui-birthdate').datepicker({
        format: 'dd/mm/yyyy'
    });
    $('#admin-birthdate').datepicker({
        format: 'dd/mm/yyyy'
    });
    $(document).ready(function() {
        var pricing = $('#billing').find(':selected').attr('data-pricing');
        var domain_year = $('#billing').find(':selected').attr('data-year');
        var total = pricing;
        // console.log(total);
        printf_order(total);
        function printf_order(total) {
            $('.total_pricing').html(addCommas(total) + ' VNĐ');
            $('#domain-year').val(domain_year);
        }

        //ajax chọn thông tin sản phẩm theo năm
        $('#billing').on('change', function() {
            var id_product = $('#product_id').val();
            var billing    = $(this).val();
            $.ajax({
                type: 'get',
                url:  '/order/check_billing_domain',
                data: {id: id_product},
                datatype: 'json',
                beforeSend: function(){
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('.loading').css('opacity', '0.5');
                    $('.loading').html(html);
                    $('.load-total').hide();
                },
                success: function(data) {
                     console.log(data);
                     var total = data.product[billing];
                    //  console.log(total);
                    $('.item_billing').text( data.billing[billing] + ' Năm' );
                    $('.item_pricing').text(addCommas(total) + ' VNĐ');
                    // $('.itemtitle').html(data.product.name + 'x' + data.group_product.name);
                    $('.total_pricing').html(addCommas(total) + ' VNĐ');
                    $('#domain-year').val(data.billing[billing]);
                    $('.loading').css('opacity', '1');
                    $('.loading').html('');
                    $('.load-total').show();

                },
                error: function(e) {
                    console.error(e);
                    $('.loading').css('opacity', '1');
                    $('.loading').html('Đã có lỗi xảy ra. Vui lòng thông báo cho quản trị viên để tránh đặt hàng thất bại');
                }

            });
        });

        // Hiển thị form cho thuộc tính cá nhân hoặc tổ chức
        $('input[name="customer-domain"]').click(function(){
            var customer = $(this).val();
            var html_owner_number = '<label for="ownerid-number" class="required">Chứng minh thư</label><div class="ownerid-number-input"><input type="text" name="ownerid-number" id="ownerid-number" placeholder="CMT phải trùng thông tin với tên khách hàng" class="form-control" required></div>';
            var html_ui_birthdate = '<label for="ui-birthdate" class="required">Ngày sinh</label><input type="text" name="ui-birthdate" id="ui-birthdate" value="" placeholder="Bắt buộc đối với khách hàng cá nhân" class="form-control" required><div class="luu-y">Lưu ý: Nhập theo đúng định dạng Ngày / Tháng / Năm. Ví dụ: 16/06/1992</div>';
            var html_ui_gender = '<label for="ui-gender" class="required">Giới tính</label><select class="form-control" name="ui-gender" required><option value="Nam">Nam</option><option value="Nữ">Nữ</option></select>';
            var html_owner_taxcode = '<label for="owner-taxcode" class="required">Mã số thuế </label><div class="owner-taxcode"><input type="text" name="owner-taxcode" id="owner-taxcode" placeholder="Bắt buộc đối với khách hàng công ty" class="form-control" required></div>';
            var html_ui_taxcode = '<label for="ui-taxcode" class="required">Mã số thuế </label><div class="ui-taxcode"><input type="text" name="ui-taxcode" id="ui-taxcode" placeholder="Bắt buộc đối với khách hàng công ty" class="form-control" required></div>';
            var html_uiid_number = '<label for="uiid-number" class="required">Chứng minh thư</label><div class="uiid-number"><input type="text" name="uiid-number" id="uiid-number" placeholder="CMT phải trùng thông tin với tên" class="form-control" required></div>';
            if(customer == 'canhan') {
                //thêm block chứng minh thư
                $('.ownerid-number').html(html_owner_number);
                $('.ui-birthdate').html(html_ui_birthdate);
                $('.ui-gender').html(html_ui_gender);
                $('.owner-taxcode').html('');
                $('.ui-taxcode').html('');
                $('.uiid-number-quoc-te').html(html_uiid_number);

            } else if (customer == 'congty') {
                //thêm block mã số thuế
                $('.ownerid-number').html('');
                $('.owner-taxcode').html(html_owner_taxcode);
                $('.ui-gender').html('');
                $('.ui-birthdate').html('');
                $('.uiid-number-quoc-te').html('');
                $('.ui-taxcode').html(html_ui_taxcode);

            }
        });

        //Tự động điền vào chứng minh thư chủ thể và người quản lí khi nhập chứng minh thư khách hàng
        $('input[name="ownerid-number"]').keyup(function() {
            var value = $(this).val();
            // console.log(value);
            $('input[name="uiid-number"]').val(value);
            $('input[name="adminid-number"]').val(value);
        });
        //Tự động điền vào chứng minh thư người quản lí khi nhập chứng minh thư chủ thể
        $('input[name="uiid-number"]').keyup(function() {
            var valueadd = $(this).val();
            $('input[name="adminid-number"]').val(valueadd);
        });
        //Tự động điền vào mã số thuế chủ thể khi nhập mã số thuế khách hàng
        $('input[name="owner-taxcode"]').keyup(function() {
            var value_taxcode = $(this).val();
            // console.log(value_taxcode);
            $('input[name="ui-taxcode"]').val(value_taxcode);
        });

        $('.get-profile-info').on('change', function(){
            if (this.checked) {
                $.ajax({
                    type: 'get',
                    url:  '/add-card-domain/auto_fill_oder_domain_form',
                    datatype: 'json',
                    success: function (data) {
                        // console.log(data);
                        $('input[name="owner-name"]').val(data.name);
                        $('input[name="ui-name"]').val(data.name);
                        $('input[name="admin-name"]').val(data.name);
                        $('input[name="ownerid-number"]').val(data.user_meta.cmnd);
                        $('input[name="uiid-number"]').val(data.user_meta.cmnd);
                        $('input[name="adminid-number"]').val(data.user_meta.cmnd);
                        $('input[name="owner-email"]').val(data.email);
                        $('input[name="ui-email"]').val(data.email);
                        $('input[name="admin-email"]').val(data.email);
                        $('input[name="owner-phone"]').val(data.user_meta.phone);
                        $('input[name="ui-phone"]').val(data.user_meta.phone);
                        $('input[name="admin-phone"]').val(data.user_meta.phone);
                        $('input[name="owner-address"]').val(data.user_meta.address);
                        $('input[name="ui-address"]').val(data.user_meta.address);
                        $('input[name="admin-address"]').val(data.user_meta.address);
                        $('input[name="ui-birthdate"]').val(data.user_meta.date);
                        $('input[name="admin-birthdate"]').val(data.user_meta.date);
                        $('input[name="owner-taxcode"]').val(data.user_meta.mst);
                        if(data.user_meta.gender) {
                            $('select[name="ui-gender"] option[value='+data.user_meta.gender+'').attr('selected','selected');
                            $('select[name="admin-gender"] option[value='+data.user_meta.gender+'').attr('selected','selected');
                        }
                        if(data.user_meta.cmnd_before) {
                            $('.ownerid-scan-before').append('<div style="margin: 10px 0" class="ownerid-scan-before-img"><img style="height:100px" src="/'+data.user_meta.cmnd_before+'"><input type="hidden" name="cmnd_before_in_profile" value="'+data.user_meta.cmnd_before+'"></div>');
                        }
                        if(data.user_meta.cmnd_after) {
                            $('.ownerid-scan-after').append('<div style="margin: 10px 0" class="ownerid-scan-after-img"><img style="height:100px" src="/'+data.user_meta.cmnd_after+'"><input type="hidden" name="cmnd_after_in_profile" value="'+data.user_meta.cmnd_after+'"></div>');
                        }
                        
                    },
                    error: function(e) {
                        console.error(e);
                    }
                });
            } else {
                $('.owner-info input').val('');
                $('.ui-info input').val('');
                $('.admin-info input').val('');
                $('select[name="ui-gender"]').find('option:selected').removeAttr('selected');
                $('select[name="admin-gender"]').find('option:selected').removeAttr('selected');
                $('.ownerid-scan-before-img').remove();
                $('.ownerid-scan-after-img').remove();
            }            
        });
    });


    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }

    $('[data-toggle="tooltip"]').tooltip();

</script>
@endsection
