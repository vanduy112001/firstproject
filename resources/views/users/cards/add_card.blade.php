@extends('layouts.user2.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Thiết lập</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Thiết lập</li>
@endsection
@section('content')
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger-order" style="margin-top:20px">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4 class="text-center mb-4">Cấu hình tùy chọn và tiếp tục thanh toán</h4>
            </div>
            <div class="col-12 col-lg-7">
                <form action="{{ route('user.order') }}" method="post" id="form-user-order">
                <div class="product-info rounded">
                    <p class="product-title">
                      {{ $product->name }}
                      @if(!empty($product->pricing[$billing_cycle]))
                        ({!! number_format($product->pricing['monthly'],0,",",".") !!}/tháng)
                      @endif
                    </p>
                    <p class="config-product">
                        @if ($product->type_product == 'VPS' || $product->type_product == 'NAT-VPS')
                            Processor: {{ $product->meta_product->cpu  }} <br>
                            Memory: {{ $product->meta_product->memory  }} GB <br>
                            Disk SSD: {{ $product->meta_product->disk  }} GB <br>
                            IP: {{ $product->meta_product->ip  }} IP public <br>
                            Hệ điều hành: {{ $product->meta_product->os  }} <br>
                            Băng thông: {{ $product->meta_product->bandwidth  }} <br>
                            @if(!empty($product->meta_product->product_special))
                              <span class="text-danger">Ghi chú: Không áp dụng nâng cấp cấu hình với các gói đặc biệt</span>
                            @endif
                        @elseif($product->type_product == 'VPS-US')
                            Processor: {{ $product->meta_product->cpu  }} <br>
                            Memory: {{ $product->meta_product->memory  }} GB <br>
                            Disk SSD: {{ $product->meta_product->disk  }} GB <br>
                            IP: {{ $product->meta_product->ip  }} IP public <br>
                            Hệ điều hành: {{ $product->meta_product->os  }} <br>
                            Băng thông: {{ $product->meta_product->bandwidth  }} <br>
                            <span class="text-danger">Ghi chú: Không áp dụng nâng cấp cấu hình với các gói VPS US</span>
                        @elseif($product->type_product == 'Server')
                            Máy chủ: {{ $product->meta_product->name_server  }}
                            Processor: {{ $product->meta_product->cpu  }} <br>
                            Memory: {{ $product->meta_product->memory  }} GB <br>
                            Disk SSD: {{ $product->meta_product->disk  }} GB <br>
                            IP: {{ $product->meta_product->ip  }} IP public <br>
                            Băng thông: {{ $product->meta_product->bandwidth  }}
                        @elseif($product->type_product == 'Proxy')
                            IP: {{ $product->meta_product->ip  }} IP public <br>
                            Băng thông: {{ $product->meta_product->bandwidth  }}
                        @else
                            Dung lượng: {{ $product->meta_product->storage  }} MB <br>
                            <b>Unlimited Data Transfer</b> <br>
                            Hosting: {{ $product->meta_product->domain  }} domain <br>
                            Sub Domain: {{ $product->meta_product->sub_domain  }} <br>
                            Alias Domain: {{ $product->meta_product->alias_domain  }} <br>
                            Database: {{ $product->meta_product->database  }} database <br>
                            FTP: {{ $product->meta_product->ftp  }} tài khoản <br>
                            Control Panel: {{ $product->meta_product->panel  }} <br>
                        @endif
                    </p>
                </div>
                @php
                    $event_promotion = GroupProduct::get_event_promotion($product->id, $billing_cycle);
                @endphp
                @if( isset($event_promotion) )
                    <div class="alert alert-success-order alert-dismissible">
                      <h5 class="text-success"><i class="fas fa-gift"></i> Khuyến mãi!</h5>
                      Tặng {{ $event_promotion->point }} Cloudzone Point khi đăng ký sản phẩm dịch vụ {{ $product->name }} thời hạn theo {{ $billings[$billing_cycle] }}!
                    </div>
                @endif
                @if(!empty($product->pricing->one_time_pay))
                  <input type="hidden" name="one_time_pay" id="one_time_pay" value="{{ $product->pricing->one_time_pay }}">
                @else
                  <div class="form-group" id="select-billing">
                      <h5 style="display: none">Thời gian:
                        @if( $billing_cycle == 'one_time' )
                          Vĩnh viển
                        @elseif( $billing_cycle == 'free' )
                          Free
                        @elseif( $billing_cycle == 'annually' )
                          1 Năm
                        @elseif( $billing_cycle == 'monthly' )
                          1 Tháng
                        @elseif( $billing_cycle == 'quarterly' )
                          3 Tháng
                        @elseif( $billing_cycle == 'semi_annually' )
                          6 Tháng
                        @elseif( $billing_cycle == 'biennially' )
                          2 Năm
                        @elseif( $billing_cycle == 'triennially' )
                          3 Năm
                        @endif
                      </h5>
                      <input type="hidden" id="billing-price" name="billing-price" value="{{ $billing_cycle }}" data-pricing="{{ $product->pricing[$billing_cycle] }}">
                  </div>
                @endif


                @if ($product->type_product == 'VPS' || $product->type_product == 'Server' || $product->type_product == 'NAT-VPS' || $product->type_product == 'VPS-US')
                    @if(!$product->meta_product->product_special)
                        <div class="form-group">
                            @if ($product->type_product == 'VPS' || $product->type_product == 'NAT-VPS')
                              {{-- <button type="button" id="addon-vps" class="btn btn-outline-success"><i class="far fa-plus-square"></i> Thêm cấu hình</button> --}}
                              <input id="get-addon" value="addon-vps" type="hidden" >
                            @elseif ($product->type_product == 'Server')
                              <button type="button" id="addon-server" class="btn btn-outline-success"><i class="far fa-plus-square"></i> Thêm cấu hình</button>
                            @elseif ($product->type_product == 'Hosting')
                              <button type="button" id="addon-hosting" class="btn btn-outline-success"><i class="far fa-plus-square"></i> Thêm cấu hình</button>
                            @endif
                            @if ($product->type_product == 'VPS' || $product->type_product == 'NAT-VPS')
                              <div class="screen-addon mt-4">

                              </div>
                            @endif
                        </div>
                    @endif
                    @if ($product->type_product == 'VPS')
                      <!-- <div class="form-group form-check-inline" data-toggle="tooltip-security" data-placement="top" title="Tùy chọn này sẽ tự động cài phần mềm bảo mật cho VPS của quí khách">
                        <label for="security" class="form-check-label mr-3"><i class="fas fa-shield-alt"></i> Bảo mật</label>
                        <div class="icheck-success">
                            <input type="checkbox" class="custom-control-input form-check-input" name="security" id="security" value="1" checked>
                            <label for="security">
                            </label>
                        </div>
                      </div> -->
                    @endif
                    @if ($product->type_product == 'VPS-US')
                      <div class="form-group">
                          <label for="">Hệ điều hành</label>
                          <div class="select-os">
                            <label class="radio-inline">
                                <input type="radio" name="os" value="2" checked> Windows Server 2012 R2
                            </label>
                          </div>
                      </div>
                      @if ($product->group_product->type == 'vps_us')
                        <button type="button" id="btn_add_state" class="btn btn-success btn-sm"><i class="far fa-plus-square"></i> Mua nhiều bang</button>
                      @endif
                      <div id="add_state" class="row">
                        <div class="col divstate">
                          @if ($product->group_product->type == 'vps_us')
                            <div class="form-group">
                              {{-- <label for="">Chọn quốc gia</label>
                              <span class="mt-3"></span> --}}
                              <input type="hidden" id="inp_add_state" name="add_state" value="{{ old('add_state') }}">
                              <input type="hidden" name="type_country" class="chosse_country" value="US">
                              {{-- <div class="select-os  mt-1">
                                <label class="radio-inline">
                                    <input type="radio" name="type_country" class="chosse_country" value="US" checked> US
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="type_country" class="chosse_country" value="UK"> UK
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="type_country" class="chosse_country" value="Netherlands"> Netherlands (Hà Lan)
                                </label>
                              </div> --}}
                            </div>
                            <div class="form-group" id="select_states">
                                <label for="states">Bang</label>
                                <div class="select-states">
                                  <select id="states" class="form-control">
                                    @if ($list_state->count() > 0)
                                      @foreach($list_state as $key => $bang)
                                        @if ( !$product->meta_product->product_special )
                                          @if ($bang->location == 'us')
                                            @if( $bang->id_state != 13 && $bang->id_state != 12 && $bang->id_state != 16 )
                                              @php
                                                $selected = '';
                                                if ( $bang->id_state == old('state') ) {
                                                  $selected = 'selected';
                                                }
                                              @endphp
                                              <option value="{{ $bang->name }}" {{ $selected }}>{{ $bang->name }}</option>   
                                            @endif
                                          @endif
                                        @else
                                          @if ($bang->location == 'cloudzone')
                                            @if( $bang->id_state != 13 && $bang->id_state != 12 && $bang->id_state != 16 )
                                              @php
                                                $selected = '';
                                                if ( $bang->id_state == old('state') ) {
                                                  $selected = 'selected';
                                                }
                                              @endphp
                                              <option value="{{ $bang->name }}" {{ $selected }}>{{ $bang->name }}</option>   
                                            @endif
                                          @endif
                                        @endif
                                      @endforeach
                                    @else
                                      @foreach($states as $key => $state)
                                        @if($key != 13 && $key != 12 && $key != 16)
                                          @php
                                            $selected = '';
                                            if ( $state == old('state') ) {
                                              $selected = 'selected';
                                            }
                                          @endphp
                                          <option value="{{ $state }}" {{ $selected }}>{{ $state }}</option>
                                        @endif
                                      @endforeach
                                    @endif
                                  </select>
                                </div>
                            </div>
                            <input type="hidden" name="state" id="choosse_state" value="Random">
                          @elseif ($product->group_product->type == 'vps_uk')
                            <input type="hidden" name="type_country" value="UK">
                            <input type="hidden" name="state" value="UK">
                          @elseif ($product->group_product->type == 'vps_sing')
                            <input type="hidden" name="type_country" value="Singapore">
                            <input type="hidden" name="state" value="Singapore">
                          @elseif ($product->group_product->type == 'vps_hl')
                            <input type="hidden" name="type_country" value="Netherlands">
                            <input type="hidden" name="state" value="Netherlands">
                          @elseif ($product->group_product->type == 'vps_uc')
                            <input type="hidden" name="type_country" value="Australia">
                            <input type="hidden" name="state" value="Australia">
                          @elseif ($product->group_product->type == 'vps_ca')
                            <input type="hidden" name="type_country" value="Canada">
                            <input type="hidden" name="state" value="Canada">
                          @endif
                          <div class="form-group">
                            <label for="">Số lượng</label>
                            <input type="text" value="{{ !empty(old('quantity'))? old('quantity') : 1 }}" name="quantity" id="qtt" class="form-control" placeholder="Số lượng">
                            <div class="quantity-order mb-4 mt-4">

                            </div>
                          </div>
                        </div>
                      </div>
                    @else
                      <div class="form-group">
                          <label for="">Hệ điều hành</label><br>
                          <div class="select-os">
                              @if ($list_vps_os->count() > 0)
                                  @if ($list_vps_os->count() == 1)
                                    @foreach ($list_vps_os as $key => $vps_os)
                                        <label class="radio-inline">
                                            <input type="radio" name="os" value="{{ $vps_os->os }}" checked> {{$list_os[$vps_os->os]}}
                                        </label>
                                    @endforeach
                                  @else
                                    @foreach ($list_vps_os as $key => $vps_os)
                                        @php
                                          $selected = '';
                                          if ( $vps_os->os == 2 ) {
                                            $selected = 'checked';
                                          }
                                        @endphp
                                        <label class="radio-inline">
                                            <input type="radio" name="os" value="{{ $vps_os->os }}" {{ $selected }}> {{$list_os[$vps_os->os]}}
                                        </label>
                                    @endforeach
                                  @endif
                              @else
                                  @foreach ($list_os as $key => $os)
                                    @if ( $key != 30 )
                                      @php
                                        $selected = '';
                                        if ( $key == 2 ) {
                                          $selected = 'checked';
                                        }
                                      @endphp
                                      @if ($product->type_product == 'NAT-VPS')
                                              @if ($key != 4 && $key != 31)
                                                  <label class="radio-inline">
                                                      <input type="radio" name="os" value="{{ $key }}" {{ $selected }}> {{$os}}
                                                  </label>
                                              @endif
                                      @else
                                          <label class="radio-inline">
                                              <input type="radio" name="os" value="{{ $key }}" {{ $selected }}> {{$os}}
                                          </label>
                                      @endif
                                    @endif
                                  @endforeach
                              @endif
                          </div>
                      </div>
                    @endif
                @elseif ($product->type_product == 'Proxy') 
                    <div class="form-group">
                      <input type="hidden" id="inp_add_state" name="add_state" value="{{ old('add_state') }}">
                      <input type="hidden" name="type_country" class="chosse_country" value="US">
                    </div>
                    <div class="form-group" id="select_states">
                        <label for="states">Bang</label>
                        <div class="select-states">
                          <select id="states" class="form-control">
                            @if ($list_state_proxy->count() > 0)
                              @foreach($list_state_proxy as $key => $bang)
                                <option value="{{ $bang->name }}">{{ $bang->name }}</option>
                              @endforeach
                            @else
                              <option value="Unknow">Unknow</option>
                            @endif
                          </select>
                        </div>
                        <input type="hidden" name="state" id="choosse_state" value="Random">
                    </div>
                    <div class="form-group">
                      <label for="">Số lượng</label>
                      <input type="text" value="{{ !empty(old('quantity'))? old('quantity') : 1 }}" name="quantity" id="qtt" class="form-control" placeholder="Số lượng">
                      <div class="quantity-order mb-4 mt-4">

                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-2">
                          <label for="account">Tài khoản</label>
                        </div>
                        <div class="col-md-10">
                          <label class="radio-inline">
                            <input type="radio" name="account_proxy" class="chosse_account type_account" value="random" checked> Random
                          </label>
                          <label class="radio-inline">
                              <input type="radio" name="account_proxy" class="chosse_account" value="config"> Tùy chọn
                          </label>
                        </div>
                      </div>
                      <div class="form-group">
                        <label for="">Username</label>
                        <input type="text" value="{{ !empty(old('username'))? old('username') : '' }}" disabled name="username" id="username" class="form-control" placeholder="Username">
                      </div>
                      <div class="form-group">
                        <label for="">Password</label>
                        <input type="text" value="{{ !empty(old('password'))? old('password') : '' }}" disabled name="password" id="password" class="form-control" placeholder="Password">
                      </div>
                    </div>
                @else
                    <div class="form-group">
                        <label for="">Domain</label>
                        <div class="domain ">
                            <div class="input-domain">
                                <input type="text" value="{{ old('domain') }}" name="domain[]" placeholder="Domain" class="form-control name_domain">
                            </div>
                        </div>
                    </div>
                @endif
                <!-- <div class="form-group">
                    <label for="">Ghi chú</label>
                    <div class="description">
                        <div class="row input-description">
                            <textarea name="description" id="" cols="30" rows="4" class="form-control">{{ old('description') }}</textarea>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="col-12 col-lg-5">
                <div class="form-group">
                    <label for="">Thời gian thanh toán</label>
                    <select name="billing" class="form-control select2" id="billing">
                         <option value="" disabled>Chọn thời gian</option>
                         @php
                             $term = false;
                         @endphp
                         @foreach ($billings as $key => $billing)
                             @php
                                 $selected = '';
                                 $disabled = false;
                                 if (!$term) {
                                     if (!empty($product->pricing[$key])) {
                                         $term = true;
                                     }
                                 }
                                 if (empty($product->pricing[$key])) {
                                     $disabled = true;
                                 }
                             @endphp
                             @if(!$disabled)
                               <option value="{{$key}}" @if($key === $billing_cycle) selected @endif data-pricing="{{ $product->pricing[$key] }}">{!!number_format($product->pricing[$key],0,",",".") . ' VNĐ'!!} / {{$billing}}</option>
                             @endif
                         @endforeach
                     </select>
                </div>
                @if ($product->type_product != 'VPS-US' && $product->type_product != 'Proxy')
                  <div class="form-group">
                      <label for="">Số lượng</label>
                      <input type="text" value="{{ !empty(old('quantity'))? old('quantity') : 1 }}" name="quantity" id="qtt" class="form-control" placeholder="Số lượng">
                      <div class="quantity-order mb-4 mt-4">

                      </div>
                  </div>
                @endif
                <!-- <div class="form-group">
                    <label for="">Chọn khách hàng</label>
                    <div class="khach-hang row">
                        <div class="input-kh col-8">
                            <select class="form-control select2" name="makh" id="makh">
                              <option value="" disabled>---Chọn khách hàng---</option>
                                <option value="0">Cho tôi</option>
                                @foreach($list_customers as $customer)
                                    <option value="{{ $customer->ma_customer }}">{{ !empty($customer->customer_name) ? $customer->customer_name : $customer->customer_tc_name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-4">
                          <button type="button" class="btn btn-sm btn-success add_customer">Thêm khách hàng</button>
                        </div>
                    </div>
                </div> -->
                {{-- <div class="form-group">
                    <label for="">Mã khuyến mãi</label>
                    <div class="khuyen-mai">
                        <div class="input-km row">
                          <div class="col-8">
                            <input type="text" name="promotion" id="promotion" class="form-control" value="">
                          </div>
                          <div class="col-4">
                            <button type="button" class="btn btn-success check_promotion" data-order="order">Kiểm tra</button>
                          </div>
                          <div class="col-12 pt-2 notification_promotion">

                          </div>
                        </div>
                    </div>
                </div> --}}
                <div id="ordersumm">
                    {{-- <div class="ordersummarytitle">
                        Tóm tắt đơn hàng
                    </div> --}}
                    <div id="ordersummary" style="display: none">
                        <div class="before-send">
                        </div>
                        <div class="order_sum">
                            <div class="ordersummary_product">
                                <div class="item itemproduct">
                                    <div class="itemtitle">
                                        {{ $product->group_product->name }} - {{ $product->name }}
                                    </div>
                                    <div class="row">
                                        @if ($billing_cycle == 'free')
                                            <div class="col col-md-6 item_billing">
                                                Free
                                            </div>
                                            <div class="col col-md-6 item_pricing text-right">
                                                0 VNĐ
                                            </div>
                                            <div class="hidden">
                                                <input type="hidden" name="sub_total" id="sub_total" value="0">
                                            </div>
                                        @elseif ($billing_cycle == 'one_time')
                                            <div class="col col-md-6 item_billing">
                                                Vĩnh viễn
                                            </div>
                                            <div class="col col-md-6 item_pricing text-right">
                                                {!!number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'!!}
                                            </div>
                                            <div class="hidden">
                                                <input type="hidden" name="sub_total" id="sub_total" value="{{ $product->pricing->one_time_pay }}">
                                            </div>
                                        @else
                                            @if ($billing_cycle == 'monthly')
                                                <div class="col col-md-6 item_billing">
                                                    1 Tháng
                                                </div>
                                                <div class="col col-md-6 item_pricing text-right">
                                                    {!!number_format($product->pricing->monthly,0,",",".") . ' VNĐ'!!}
                                                </div>
                                                <div class="hidden">
                                                    <input type="hidden" name="sub_total" id="sub_total" value="{{ $product->pricing->monthly }}">
                                                </div>
                                            @elseif ($billing_cycle == 'quarterly')
                                                <div class="col col-md-6 item_billing">
                                                    3 Tháng
                                                </div>
                                                <div class="col col-md-6 item_pricing text-right">

                                                    {!!number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'!!}
                                                </div>
                                                <div class="hidden">
                                                    <input type="hidden" name="sub_total" id="sub_total" value="{{ $product->pricing->quarterly }}">
                                                </div>
                                            @elseif ($billing_cycle == 'semi_annually')
                                                <div class="col col-md-6 item_billing">
                                                    6 Tháng
                                                </div>
                                                <div class="col col-md-6 item_pricing text-right">
                                                    {!!number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'!!}
                                                </div>
                                                <div class="hidden">
                                                    <input type="hidden" name="sub_total" id="sub_total" value="{{ $product->pricing->semi_annually }}">
                                                </div>
                                            @elseif ($billing_cycle == 'annually')
                                                <div class="col col-md-6 item_billing">
                                                    1 Năm
                                                </div>
                                                <div class="col col-md-6 item_pricing text-right">

                                                    {!!number_format($product->pricing->annually,0,",",".") . ' VNĐ'!!}
                                                </div>
                                                <div class="hidden">
                                                    <input type="hidden" name="sub_total" id="sub_total" value="{{ $product->pricing->annually }}">
                                                </div>
                                            @elseif ($billing_cycle == 'biennially')
                                                <div class="col col-md-6 item_billing">
                                                    2 Năm
                                                </div>
                                                <div class="col col-md-6 item_pricing text-right">

                                                    {!!number_format($product->pricing->biennially,0,",",".") . ' VNĐ'!!}
                                                </div>
                                                <div class="hidden">
                                                    <input type="hidden" name="sub_total" id="sub_total" value="{{ $product->pricing->biennially }}">
                                                </div>
                                            @else
                                                <div class="col col-md-6 item_billing">
                                                    3 Năm
                                                </div>
                                                <div class="col col-md-6 item_pricing text-right">

                                                    {!!number_format($product->pricing->triennially,0,",",".") . ' VNĐ'!!}
                                                </div>
                                                <div class="hidden">
                                                    <input type="hidden" name="sub_total" id="sub_total" value="{{ $product->pricing->triennially }}">
                                                </div>
                                            @endif
                                        @endif
                                    </div>
                                </div>
                                <div class="addon-item-cpu">

                                </div>
                                <div class="addon-item-ram">

                                </div>
                                <div class="addon-item-disk">

                                </div>
                                <div class="addon-item-storage">

                                </div>
                                <div class="addon-item-database">

                                </div>
                            </div>
                            <div class="sub_total">
                                <div class="row">
                                    <div class="col col-md-3">
                                        Sub Total
                                    </div>
                                    <div class="col col-md-9 sub_total_pricing">
                                        0 VNĐ
                                    </div>
                                </div>
                            </div>
                            <div class="so-luong">
                                <div class="row">
                                    <div class="col col-md-3">
                                        Số lượng
                                    </div>
                                    <div class="col col-md-9 quantity-printf">
                                        1
                                    </div>
                                </div>
                            </div>
                            <div class="total">
                                <div class="row">
                                    <div class="col col-md-3">
                                        Total
                                    </div>
                                    <div class="col col-md-9 total_pricing">
                                        0 VNĐ
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="margin: 30px 0;">
                        <div class="card-header">
                          <h3 class="card-title mb-0">Tóm tắt đơn hàng</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0 summary">
                          <table class="table table-sm table-bordered" id="table-show-price">
                            <thead>
                              <tr>
                                <th>Sản phẩm</th>
                                <th>Số lượng</th>
                                <th class="text-center" style="width: 125px">Chi phí</th>
                              </tr>
                            </thead>
                            <tbody class="show-select-addon">
                              <tr>
                                <td>{{ $product->group_product->name }} - {{ $product->name }}</td>
                                <td class="quantity-printf text-left"></td>
                                <td class="item_pricing_cell text-right"></td>
                              </tr>
                              <tr class="addon-item-cpu">
                              </tr>
                              <tr class="addon-item-ram">
                              </tr>
                              <tr class="addon-item-disk">
                              </tr>
                              <tr class="detail-promotion">
                              </tr>
                            </tbody>
                            <tfoot style="background: #e7ffda">
                                <tr>
                                    <td class="text-right" colspan="2">Tổng cộng</td>
                                    <td class="total_pricing"></td>
                                </tr>
                            </tfoot>
                          </table>
                        </div>
                        <!-- /.card-body -->
                      </div>
                </div>
                <div class="button_submit text-center">
                    @csrf
                    <input type="hidden" name="product_id" value="{{ $product->id }}" id='product_id'>
                    <input type="hidden" name="type_product" value="{{ $product->type_product }}" id="type_product">
                    <input type="submit" class="btn btn-block btn-primary" value="Đặt hàng">
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="product_special" value="{{ $product->meta_product->product_special }}">
<input type="hidden" id="total_order" value="{{ !empty($product->pricing[$billing_cycle]) ? $product->pricing[$billing_cycle] : $product->pricing['monthly'] }}">
<input type="hidden" id="promotion_order" data-promotion="" value="">
{{-- Modal xoa product --}}
  <div class="modal fade" id="add_customer">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Thêm khách hàng</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <div id="notication-product" class="m-4">
              </div>
              <div class="form-customer">
                <form action="" id="form_add_customer">
                  @csrf
                  <div class="row form-group">
                    <div class="col-sm-4">
                      <label for="type_customer">Loại chủ thể</label>
                    </div>
                    <input class="type_checked" type="hidden"  name="type_customer" value="0">
                    <div class="luu-y col-sm-12">
                      Lưu ý: Các thông tin có dấu * là bắt buộc
                    </div>
                  </div>
                  <!-- loại chủ thể cá nhân -->
                  <div class="radio_personal">
                    <div class="card-danger">
                      <div class="card-body">
                        <div class="row form-group">
                          <div class="col-sm-4">
                            <label for="type_customer" class="required">Họ và tên người đăng ký</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" name="customer_name" id="personal_name" value="" placeholder="Họ và tên" class="form-control">
                            <div class="error_personal_name">
                            </div>
                          </div>
                        </div>
                        <div class="row form-group">
                          <div class="col-sm-4">
                            <label for="">Số điện thoại</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" name="customer_phone" id="personal_phone" value="" class="form-control" placeholder="Số điện thoại">
                            <div class="error_personal_phone">

                            </div>
                          </div>
                        </div>
                        <div class="row form-group">
                          <div class="col-sm-4">
                            <label for="">Email</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="email" name="customer_email" id="personal_email" value="" class="form-control" placeholder="Địa chỉ Email">
                            <div class="error_personal_email">

                            </div>
                          </div>
                        </div>
                        <div class="row form-group">
                          <div class="col-sm-4">
                            <label for="">Facebook</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" name="customer_address" id="personal_address" value="" class="form-control" placeholder="Facebook">
                            <div class="error_personal_address">

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.loại chủ thể cá nhân -->
                  <!-- Loại chủ thể tổ chức -->
                  <div class="radio_organization">
                    <!-- Thông tin tổ chức -->
                    <div class="company_info">
                      <div class="card-danger">
                        <div class="card-header">
                          <h3 class="card-title">Thông tin tổ chức</h3>
                        </div>
                        <div class="card-body">
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="customer_tc_name" class="required">Tên tổ chức</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_tc_name" id="customer_tc_name" value="" placeholder="Tên tổ chức theo giấy phép kinh doanh hoặc con dấu" class="form-control">
                              <div class="error_customer_tc_name">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="customer_tc_mst" class="required">Mã số thuế</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_tc_mst" id="customer_tc_mst" value="" placeholder="Mã số thuế" class="form-control">
                              <div class="error_customer_tc_mst">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="customer_tc_sdt" class="required">Số điện thoại</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_tc_sdt" id="customer_tc_sdt" value="" placeholder="Số điện thoại tổ chức" class="form-control @error('customer_tc_sdt') is-invalid @enderror">
                              <div class="error_customer_tc_sdt">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="customer_tc_dctc"  class="required">Địa chỉ tổ chức</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_tc_dctc" value="" id="customer_tc_dctc" placeholder="Địa chỉ tổ chức" class="form-control @error('customer_tc_dctc') is-invalid @enderror">
                              <div class="error_customer_tc_dctc">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="customer_tc_city" class="required">Tỉnh / Thành phố</label>
                            </div>
                            <div class="col-sm-8">
                              <select class="form-control" name="customer_tc_city" id="customer_tc_city">
                                <option value="" disabled selected>--- Chọn Tỉnh / Thành phố ---</option>
                                @foreach($list_city as $city)
                                <?php
                                $selected = '';
                                if (old('customer_tc_city') == $city) {
                                  $selected = 'selected';
                                }
                                ?>
                                <option value="{{$city}}" {{$selected}}>{{$city}}</option>
                                @endforeach
                              </select>
                              <div class="error_customer_tc_city">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="customer_dk_name" class="required">Họ và tên người đăng ký tài khoản</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_dk_name" value="" id="customer_dk_name" placeholder="Họ và tên phải theo CMND" class="customer_dk_name form-control @error('customer_dk_name') is-invalid @enderror">
                              <div class="error_customer_dk_name">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Giới tính</label>
                            </div>
                            <div class="col-sm-8">
                              <select class="form-control customer_dk_gender" id="customer_dk_gender" name="customer_dk_gender">
                                <option value="Nam">Nam</option>
                                <option value="Nữ">Nữ</option>
                              </select>
                              <div class="error_customer_dk_gender">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Chức vụ</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_dk_cv" value="" id="customer_dk_cv" class="customer_dk_cv form-control" placeholder="Chức vụ người đăng ký">
                              <div class="error_customer_dk_cv">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Ngày sinh (mm/dd/YYYY)</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_dk_date" id="customer_dk_date" value="{{ old('customer_dk_date') }}" class="customer_dk_date form-control  @error('customer_dk_date') is-invalid @enderror" placeholder="Ngày sinh">
                              <div class="luu-y">
                                Lưu ý: Nhập theo đúng định dạng Ngày / Tháng / Năm. Ví dụ: 16/06/1992
                              </div>
                              <div class="error_customer_dk_date">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Số chứng minh nhân dân</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_dk_cmnd" value="" id="customer_dk_cmnd" class="customer_dk_cmnd form-control  @error('customer_dk_cmnd') is-invalid @enderror" placeholder="Số chứng minh nhân dân">
                              <div class="error_customer_dk_cmnd">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Số điện thoại người đăng ký</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_dk_phone" value="" id="customer_dk_phone" class="customer_dk_phone form-control  @error('customer_dk_phone') is-invalid @enderror" placeholder="Số điện thoại">
                              <div class="error_customer_dk_phone">

                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-4">

                            </div>
                            <div class="col-sm-8">
                              <button type="button" class="btn btn-info info_copy" title="Sao chép thông tin người đăng ký cho người đại diện và người đại diện làm thủ tục">Sao chép thông tin</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- /.Thông tin tổ chức -->
                    <!-- Thông tin người đại diện -->
                    <div class="info_dd">
                      <div class="card-danger">
                        <div class="card-header">
                          <h3 class="card-title">Thông tin người đại diện</h3>
                        </div>
                        <div class="card-body">
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="customer_dd_name" class="required">Họ và tên người đại diện</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_dd_name" value="" id="customer_dd_name" placeholder="Họ và tên phải theo CMND" class="customer_dd_name form-control @error('customer_dd_name') is-invalid @enderror">
                              <div class="error_customer_dd_name">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Chức vụ</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_dd_cv" value="" id="customer_dd_cv" class="customer_dd_cv form-control  @error('customer_dd_cv') is-invalid @enderror" placeholder="Chức vụ người đại diện">
                              <div class="error_customer_dd_cv">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Giới tính</label>
                            </div>
                            <div class="col-sm-8">
                              <select class="customer_dd_gender form-control" id="customer_dd_gender" name="customer_dd_gender">
                                <option value="Nam">Nam</option>
                                <option value="Nữ">Nữ</option>
                              </select>
                              <div class="error_customer_dd_gender">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Ngày sinh (mm/dd/YYYY)</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_dd_date" id="customer_dd_date" value="{{ old('customer_dd_date') }}" class="form-control  @error('customer_dd_date') is-invalid @enderror" placeholder="Ngày sinh">
                              <div class="luu-y">
                                Lưu ý: Nhập theo đúng định dạng  Ngày /Tháng / Năm. Ví dụ: 16/06/1992
                              </div>
                              <div class="error_customer_dd_date">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Số chứng minh nhân dân</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_dd_cmnd" value="" id="customer_dd_cmnd" class="customer_dd_cmnd form-control  @error('customer_dd_cmnd') is-invalid @enderror" placeholder="Số chứng minh nhân dân">
                              <div class="error_customer_dd_cmnd">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Email</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="email" name="customer_dd_email" value="" id="customer_dd_email" class="customer_dd_email form-control  @error('customer_dd_email') is-invalid @enderror" placeholder="Địa chỉ Email">
                              <div class="error_customer_dd_email">

                              </div>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-4">

                            </div>
                            <div class="col-sm-8">
                              <button type="button" class="btn btn-info info_dd_copy" title="Sao chép thông tin người đại diện cho người đại diện làm thủ tục">Sao chép thông tin</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- /.Thông tin người đại diện -->
                    <!-- Thông tin người đại diện làm thủ tục -->
                    <div class="info_dd">
                      <div class="card-danger">
                        <div class="card-header">
                          <h3 class="card-title">Thông tin người đại diện làm thủ tục</h3>
                        </div>
                        <div class="card-body">
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="customer_tt_name" class="required">Tên người đại diện làm thủ tục</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_tt_name" value="" id="customer_tt_name" placeholder="Họ và tên phải theo CMND" class="customer_tt_name form-control @error('customer_tt_name') is-invalid @enderror">
                              <div class="error_customer_tt_name">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Giới tính</label>
                            </div>
                            <div class="col-sm-8">
                              <select class="customer_tt_gender form-control" id="customer_tt_gender" name="customer_tt_gender">
                                <option value="Nam" @if( old('customer_tt_gender') == 'Nam' ) selected @endif>Nam</option>
                                <option value="Nữ" @if( old('customer_tt_gender') == 'Nữ' ) selected @endif>Nữ</option>
                              </select>
                              <div class="error_customer_tt_gender">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Ngày sinh (mm/dd/YYYY)</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_tt_date" id="customer_tt_date" value="{{ old('customer_tt_date') }}" class="form-control  @error('customer_tt_date') is-invalid @enderror" placeholder="Ngày sinh">
                              <div class="luu-y">
                                Lưu ý: Nhập theo đúng định dạng  Ngày /Tháng / Năm. Ví dụ: 16/06/1992
                                <div class="error_customer_tt_date">

                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Số chứng minh nhân dân</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_tt_cmnd" value="" id="customer_tt_cmnd" class="customer_tt_cmnd form-control  @error('customer_tt_cmnd') is-invalid @enderror" placeholder="Số chứng minh nhân dân">
                              <div class="error_customer_tt_cmnd">

                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Email</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="email" name="customer_tt_email" value="" id="customer_tt_email" class="customer_tt_email form-control  @error('customer_tt_email') is-invalid @enderror" placeholder="Địa chỉ Email">
                              <div class="error_customer_tt_email">

                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- /.Thông tin người đại diện làm thủ tục -->
                  </div>
                  <!-- /.Loại chủ thể tổ chức -->
                </form>
              </div>
          </div>
          <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
            <button type="button" class="btn btn-primary" id="button_add_customer">Thêm khách hàng</button>
            <button type="button" class="btn btn-danger" data-dismiss="modal" id="finish_add_customer">Hoàn thành</button>
          </div>
        </div>
        <!-- /.modal-content -->
    </div>
      <!-- /.modal-dialog -->
</div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}"></script>
<script src="{{ asset('js/user_order.js') }}?token={{ date('YmdH') }}"></script>
<script src="{{ asset('js/cutomer.js')  }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
        $('[data-toggle="tooltip-security"]').tooltip();
    });
</script>
<script>
    $("input[type='number']").inputSpinner();
</script>
@endsection
