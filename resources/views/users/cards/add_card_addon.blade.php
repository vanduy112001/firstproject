@extends('layouts.user2.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Yêu cầu nâng cấp cấu hình</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"> Yêu cầu nâng cấp cấu hình</li>
<li class="breadcrumb-item active"> VPS </li>
@endsection
@section('content')
<form action="{{ route('users.orders.createOrderAddon') }}" method="post" id="form-user-order">
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-md-12 detail-course-finish">
                @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger-order" style="margin-top:20px">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h4 class="text-center mb-4">Cấu hình @php !empty($vps) ? 'VPS' : 'Hosting' @endphp và tiếp tục thanh toán</h4>
            </div>
              <div class="col-12 col-lg-7 pl-4">
                  <div class="product-info rounded">
                      <p class="product-title">VPS IP: {{ !empty($vps) ? $vps->ip : '' }}</p>
                      <p class="config-product">
                            @php
                                $cpu = 0;
                                $ram = 0;
                                $disk = 0;
                                $product = $vps->product;
                                $cpu = $product->meta_product->cpu;
                                $ram = $product->meta_product->memory;
                                $disk = $product->meta_product->disk;
                                if ( !empty($vps->vps_config) ) {
                                    $vps_config = $vps->vps_config;
                                    $cpu += $vps_config->cpu;
                                    $ram += $vps_config->ram;
                                    $disk += $vps_config->disk;
                                }
                                $os = $listOs[$vps->os];

                                $month = $check_time_addon['month'];
                                $day = $check_time_addon['day'];
                                if ( $day <= 15 ) {
                                    $day = 1/2;
                                } else {
                                    $day = 1;
                                }
                            @endphp
                            CPU: {{ $cpu  }} GB<br>
                            RAM: {{ $ram  }} GB <br>
                            Disk: {{ $disk  }} GB <br>
                            Hệ điều hành: {{ $os  }} <br>
                            Ngày kết thúc: {{ date('d-m-Y' , strtotime($vps->next_due_date) ) }}
                      </p>
                  </div>
                  <div class="form-group">
                        <div class="screen-addon mt-4">
                            <div class="form-group addon-product mt-4 mb-4">
                                <label for="addon">Chọn gói cấu hình thêm</label>
                                <input name="addon" value="1" id="addon-hidden" type="hidden">
                                <div class="row rounded add-on-vps">
                                    @foreach ($product_addons as $product_addon)
                                        @if ( $product_addon->meta_product->type_addon == 'addon_cpu' )
                                            <input name="id-addon-cpu" value="{{ $product_addon->id }}" id="addon-hidden" type="hidden">
                                            <div class="col-md-4 text-center addon-cpu addon_vps_class" data-type="cpu">
                                                <label for="">CPU</label>
                                                <input name="addon-cpu" id="addon_cpu" type="number" value="0" min="0" max="16" step="1"
                                                    data-name="{{ $product_addon->name }}"
                                                    data-pricing="{{ $product_addon->pricing['monthly'] * $month + (int) round($product_addon->pricing['monthly'] * $day, -3) }}" />
                                            </div>
                                        @endif
                                        @if ( $product_addon->meta_product->type_addon == 'addon_ram' )
                                            <input name="id-addon-ram" value="{{ $product_addon->id }}" id="addon-hidden" type="hidden">
                                            <div class="col-md-4 text-center addon-ram">
                                                <label for="">RAM</label>
                                                <input name="addon-ram" type="number" value="0" min="0" max="16" step="1"
                                                    data-name="{{ $product_addon->name }}"
                                                    data-pricing="{{ $product_addon->pricing['monthly'] * $month + (int) round($product_addon->pricing['monthly'] * $day, -3) }}"  />
                                            </div>
                                        @endif
                                        @if ( $product_addon->meta_product->type_addon == 'addon_disk' )
                                            <input name="id-addon-disk" value="{{ $product_addon->id }}" id="addon-hidden" type="hidden">
                                            <div class="col-md-4 text-center addon-disk">
                                                <label for="">DISK</label>
                                                <input name="addon-disk" type="number" value="0" min="0" max="200" step="10"
                                                    data-name="{{ $product_addon->name }}"
                                                    data-pricing="{{ $product_addon->pricing['monthly'] * $month + (int) round($product_addon->pricing['monthly'] * $day, -3) }}" />
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                  </div>
                  <!-- <div class="form-group">
                      <label for="">Ghi chú</label>
                      <div class="description">
                          <div class="row input-description">
                              <textarea name="description" id="" cols="30" rows="4" class="form-control">{{ old('description') }}</textarea>
                          </div>
                      </div>
                  </div> -->
              </div>
              <div class="col-12 col-lg-5 pr-4">
                <div id="ordersumm">
                    {{-- <div class="ordersummarytitle">
                        Tóm tắt đơn hàng
                    </div> --}}
                    <div id="ordersummary" style="display: none">
                        <div class="before-send">
                        </div>
                        <div class="order_sum">
                            <div class="ordersummary_product">
                                <div class="item itemproduct">
                                    <div class="itemtitle">
                                        {{ $vps->ip }}
                                    </div>
                                    <div class="row">
                                        <div class="col col-md-6 item_billing">
                                            Chọn số lượng cấu hình
                                        </div>
                                        <div class="col col-md-6 item_pricing text-right">
                                            0 VNĐ
                                        </div>
                                        <div class="hidden">
                                            <input type="hidden" name="sub_total" id="sub_total" value="0">
                                        </div>
                                    </div>
                                </div>
                                <div class="addon-item-cpu">

                                </div>
                                <div class="addon-item-ram">

                                </div>
                                <div class="addon-item-disk">

                                </div>
                            </div>
                            <div class="sub_total">
                                <div class="row">
                                    <div class="col col-md-3">
                                        Sub Total
                                    </div>
                                    <div class="col col-md-9 sub_total_pricing">
                                        0 VNĐ
                                    </div>
                                </div>
                            </div>
                            <div class="so-luong">
                                <div class="row">
                                    <div class="col col-md-3">
                                        Số lượng
                                    </div>
                                    <div class="col col-md-9 quantity-printf">
                                        0
                                    </div>
                                </div>
                            </div>
                            <div class="total">
                                <div class="row">
                                    <div class="col col-md-3">
                                        Total
                                    </div>
                                    <div class="col col-md-9 total_pricing">
                                        0 VNĐ
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card" style="margin: 30px 0;">
                        <div class="card-header">
                          <h3 class="card-title mb-0">Tóm tắt đơn hàng</h3>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-0 summary">
                          <table class="table table-sm table-bordered" id="table-show-price">
                            <thead>
                              <tr>
                                <th>Sản phẩm</th>
                                <th>Số lượng</th>
                                <th class="text-center" style="width: 125px">Chi phí</th>
                              </tr>
                            </thead>
                            <tbody class="show-select-addon">
                              <tr class="addon-item-cpu">
                              </tr>
                              <tr class="addon-item-ram">
                              </tr>
                              <tr class="addon-item-disk">
                              </tr>
                              <tr class="detail-promotion">
                              </tr>
                            </tbody>
                            <tfoot style="background: #e7ffda">
                                <tr>
                                    <td class="text-right" colspan="2">Tổng cộng</td>
                                    <td class="total_pricing"></td>
                                </tr>
                            </tfoot>
                          </table>
                        </div>
                        <!-- /.card-body -->
                      </div>
                </div>
                <div class="button_submit text-center">
                    @csrf
                    <input type="hidden" name="vpsId" value="{{ $vps->id }}">
                    <input type="submit" class="btn btn-block btn-primary" value="Đặt hàng">
                </div>
              </div>
            <div>
    </div>
</div>
</form>
<input type="hidden" id="total_order" value="">
<input type="hidden" id="promotion_order" data-promotion="" value="">
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}"></script>
{{-- <script src="{{ asset('js/user_order.js') }}"></script> --}}
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
        $('[data-toggle="tooltip-security"]').tooltip();
        $("input[type='number']").inputSpinner();

        $('tr.addon-item-cpu').hide();
        $('tr.addon-item-ram').hide();
        $('tr.addon-item-disk').hide();

        // khi click vào chọn gói addon sẽ tăng set lại sub_total
        $(document).on('click', '.addon-cpu' , function() {
            var qtt_addon = parseInt($(".addon-cpu input").val());
            var pricing = parseInt($(".addon-cpu input").attr('data-pricing'));
            var sub_total = parseInt($('#sub_total').val());
            // console.log(qtt_addon, pricing, sub_total);
            var name_addon = $(".addon-cpu input").attr('data-name');
            // lay cac addon con lai
            var qtt_addon_ram = parseInt($(".addon-ram input").val());
            var pricing_ram = parseInt($(".addon-ram input").attr('data-pricing'));
            var qtt_addon_disk = parseInt($(".addon-disk input").val());
            var pricing_disk = parseInt($(".addon-disk input").attr('data-pricing'));
            sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_ram * pricing_ram) + (qtt_addon_disk * pricing_disk/10);
            total =  sub_total  + (qtt_addon * pricing) + (qtt_addon_ram * pricing_ram) + (qtt_addon_disk * pricing_disk/10) ;
            $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
            $('.total_pricing').text(addCommas(total) + ' VNĐ');
            $('#total_order').val(total);
            var html = '';
            html += '<div class="item">';
            html += '<div class="itemtitle">';
            html += name_addon;
            html += '</div>';
            html += '<div class="row">';
            html += '<div class="col col-md-6 item_billing">';
            html += qtt_addon + ' cores';
            html += '</div>';
            html += '<div class="col col-md-6 item_pricing text-right">';
            html += addCommas(pricing * qtt_addon) + ' VNĐ';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            $('.addon-item-cpu').html(html);

            //thêm dòng hiển thị add on cpu vào bảng tóm tắt đơn hàng
            if (qtt_addon > 0) {
                var table = '';
                table += '<td>CPU Addon</td>';
                table += '<td>'+ qtt_addon + ' cores</td>';
                table += '<td class="item_pricing text-right">'+ addCommas(pricing * qtt_addon) + ' VNĐ</td>';
                $('tr.addon-item-cpu').html(table);
                $('tr.addon-item-cpu').show();
            } else {
                $('tr.addon-item-cpu').hide();
            }
        });
        // khi click vào chọn gói addon sẽ tăng set lại sub_total
        $(document).on('click', '.addon-ram' , function() {
            var qtt_addon = parseInt($('.addon-ram input').val());
            var pricing = parseInt($('.addon-ram input').attr('data-pricing'));
            var sub_total = parseInt($('#sub_total').val());
            var name_addon = $('.addon-ram input').attr('data-name');
            // lay cac addon con load
            var qtt_addon_cpu = parseInt($(".addon-cpu input").val());
            var pricing_cpu = parseInt($(".addon-cpu input").attr('data-pricing'));
            var qtt_addon_disk = parseInt($(".addon-disk input").val());
            var pricing_disk = parseInt($(".addon-disk input").attr('data-pricing'));

            sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_cpu * pricing_cpu) + (qtt_addon_disk*pricing_disk/10);
            total =  sub_total + (qtt_addon * pricing)  + (qtt_addon_disk * pricing_disk/10 )  + (qtt_addon_cpu * pricing_cpu );
            $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
            $('.total_pricing').text(addCommas(total) + ' VNĐ');
            $('#total_order').val(total);
            var html = '';
            html += '<div class="item">';
            html += '<div class="itemtitle">';
            html += name_addon;
            html += '</div>';
            html += '<div class="row">';
            html += '<div class="col col-md-6 item_billing">';
            html += qtt_addon + ' GB';
            html += '</div>';
            html += '<div class="col col-md-6 item_pricing text-right">';
            html += addCommas(pricing * qtt_addon) + ' VNĐ';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            $('.addon-item-ram').html(html);
            //thêm dòng hiển thị add on ram vào bảng tóm tắt đơn hàng
            if (qtt_addon > 0) {
                var table = '';
                table += '<td>RAM Addon</td>';
                table += '<td>'+ qtt_addon + 'Gb </td>';
                table += '<td class="item_pricing text-right">'+ addCommas(pricing * qtt_addon) + ' VNĐ</td>';
                $('tr.addon-item-ram').html(table);
                $('tr.addon-item-ram').show();
            } else {
                $('tr.addon-item-ram').hide();
            }
            var value_promotion = $('#promotion_order').val();
            var type_promotion = $('#promotion_order').attr('data-promotion');
            if (type_promotion != '') {
                var total = parseInt($('#total_order').val());
                if ( type_promotion == 'percentage' ) {
                    var total = total - (total  * value_promotion / 100);
                    $('.total_pricing').html(addCommas(total) + ' VNĐ');
                }
                else if ( type_promotion == 'fixed_amount' ) {
                    var total = (total) - value_promotion;
                    $('.total_pricing').html(addCommas(total) + ' VNĐ');
                }
                else if (type_promotion == 'free') {
                    $('.total_pricing').html(' 0 VNĐ');
                    // $('tr.detail-promotion').show();
                }
            }
            // console.log(total);
        });
        // khi click vào chọn gói addon sẽ tăng set lại sub_total
        $(document).on('click', '.addon-disk' , function() {
            var qtt_addon = parseInt($('.addon-disk input').val()) / 10;
            var pricing = parseInt($('.addon-disk input').attr('data-pricing'));
            var sub_total = parseInt($('#sub_total').val());
            var name_addon = $('.addon-disk input').attr('data-name');
            // lay cac addon con lai
            var qtt_addon_cpu = parseInt($(".addon-cpu input").val());
            var pricing_cpu = parseInt($(".addon-cpu input").attr('data-pricing'));
            var qtt_addon_ram = parseInt($(".addon-ram input").val());
            var pricing_ram = parseInt($(".addon-ram input").attr('data-pricing'));

            sub_total_new = sub_total + (qtt_addon * pricing) + (qtt_addon_cpu * pricing_cpu) + (qtt_addon_ram * pricing_ram);
            total =  sub_total + (qtt_addon * pricing ) + (qtt_addon_cpu * pricing_cpu ) + (qtt_addon_ram * pricing_ram );

            $('.sub_total_pricing').text(addCommas(sub_total_new) + ' VNĐ');
            $('.total_pricing').text(addCommas(total) + ' VNĐ');
            $('#total_order').val(total);
            var html = '';
            html += '<div class="item">';
            html += '<div class="itemtitle">';
            html += name_addon;
            html += '</div>';
            html += '<div class="row">';
            html += '<div class="col col-md-6 item_billing">';
            html += qtt_addon * 10 + ' GB';
            html += '</div>';
            html += '<div class="col col-md-6 item_pricing text-right">';
            html += addCommas(pricing * qtt_addon) + ' VNĐ';
            html += '</div>';
            html += '</div>';
            html += '</div>';
            $('.addon-item-disk').html(html);
            // console.log(total);
            //thêm dòng hiển thị add on disk vào bảng tóm tắt đơn hàng
            if (qtt_addon > 0) {
                var table = '';
                table += '<td>DISK Addon</td>';
                table += '<td>'+ qtt_addon*10 + 'Gb</td>';
                table += '<td class="item_pricing text-right">'+ addCommas(pricing * qtt_addon) + ' VNĐ</td>';
                $('tr.addon-item-disk').html(table);
                $('tr.addon-item-disk').show();
            } else {
                $('tr.addon-item-disk').hide();
            }
            var value_promotion = $('#promotion_order').val();
            var type_promotion = $('#promotion_order').attr('data-promotion');
            if (type_promotion != '') {
                var total = parseInt($('#total_order').val());
                if ( type_promotion == 'percentage' ) {
                    var total = total - (total  * value_promotion / 100);
                    $('.total_pricing').html(addCommas(total) + ' VNĐ');
                }
                else if ( type_promotion == 'fixed_amount' ) {
                    var total = (total) - value_promotion;
                    $('.total_pricing').html(addCommas(total) + ' VNĐ');
                }
                else if (type_promotion == 'free') {
                    $('.total_pricing').html(' 0 VNĐ');
                    // $('tr.detail-promotion').show();
                }
            }
        });

        function addCommas(nStr)
        {
            nStr += '';
            x = nStr.split('.');
            x1 = x[0];
            x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + '.' + '$2');
            }
            return x1 + x2;
        }
    });
</script>
@endsection
