@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
  Thông tin {{ $type }} <br>
  - Khách hàng: {{ $user_name }} <br>
  - Email: {{ $user_email }} <br>
  - IP trước khi đổi: {{ $ip }} <br>
  - IP sau khi đổi: {{ $ip_change }} <br>
  - Thành tiền: {{ number_format($amount,0,",",".") . ' VNĐ' }} <br> <br>
<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
@endsection
