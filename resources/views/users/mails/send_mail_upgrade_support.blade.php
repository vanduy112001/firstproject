@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
  Thông tin {{ $type }} <br>
  - Khách hàng: {{ $name }} <br>
  - Email: {{ $user_email }} <br>
  - Hosting: {{ $domain }} <br>
  - Gói Hosting: {{ $product_name }} <br>
<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
@endsection
