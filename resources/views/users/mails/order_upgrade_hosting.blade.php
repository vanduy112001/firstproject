@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
    Xin chào {{ $name }} <br>
    Cảm ơn quý khách đã đặt hàng. Thông tin đặt hàng của quý khách: <br><br>
    <b>Thông tin nâng cấp Hosting</b> <br>
    Hosting: {{ $domain }} <br>
    Gói Hosting: {{ $product_name }} <br>
    Thành tiền: {{ number_format($amount,0,",",".") . ' VNĐ' }} <br> <br>
    <b>Cloudzone xin cảm ơn quý khách!</b>
<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
<div style="text-align: center;line-height: 20px; font-size: 16px;font-family: Aria;color: rgb(16, 55, 132);">
	Email này được gửi đến {{ $name }} bởi vì bạn đã đăng ký tài khoản với Cloudzone. <br>
	Các đường link trong email này đều dẫn đến trang <a href="http://cloudzone.vn" style="text-decoration: none;">cloudzone.vn</a>.
</div>
@endsection
