@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
  Thông tin thay đổi IP VPS: <br>
  - Khách hàng: {{ $user_name }} <br>
  - Email: {{ $user_email }} <br>
  - Order ID: {{ $order_id }} <br>
  - Tên SP: {{ $product_name }} <br>
  - Số lượng: {{ $qtt }} <br>
  - Thành tiền: {{ $total }} <br>
  - List VPS:<br>
  {!! $list_ip_vps !!}
<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
@endsection
