@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
  Thông tin {{ $type }} <br>
  - Khách hàng: {{ $user_name }} <br>
  - Email: {{ $user_email }} <br>
  - Order ID: {{ $order_id }} <br>
  - Tên SP: {{ $product_name }} <br>
  - Thời gian: {{ $billing_cycle }} <br>
  - Số lượng: {{ $qtt }} <br>
  - Thành tiền: {{ $total }} <br>
  - OS:
  @if($os == 1)
    Windows 7
  @elseif($os == 2)
    Windows 2012
  @elseif($os == 3)
    Windows 2016
  @elseif($os == 4)
    CentOs 7 64bit
  @endif
  <br>
  - Security: {{ $security }} <br>
<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
@endsection
