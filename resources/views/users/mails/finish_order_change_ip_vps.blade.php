@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
    Xin chào {{ $name }} <br>
    Cảm ơn quý khách đã đặt hàng. Thông tin đặt hàng của quý khách: <br><br>
    <b>Thông tin thay đổi IP cho VPS</b> <br>
    - IP trước khi đổi: {{ $ip }} <br>
    - IP sau khi đổi: {{ $ip_change }} <br>
    - Thành tiền: {{ number_format($amount,0,",",".") . ' VNĐ' }} <br> <br>
    <b>Cloudzone xin cảm ơn quý khách!</b>
<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
<div style="text-align: center;line-height: 20px; font-size: 16px;font-family: roboto;color: rgb(16, 55, 132);">
	Email này được gửi đến {{ $name }} bởi vì bạn đã đăng ký tài khoản với Cloudzone. <br>
	Các đường link trong email này đều dẫn đến trang <a href="http://cloudzone.vn" style="text-decoration: none;">cloudzone.vn</a>.
</div>
@endsection
