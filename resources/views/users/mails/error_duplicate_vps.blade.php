@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
    <b>Thông tin VPS</b> <br>
    - VMID: {{ $vm_id }} <br>
    - IP: {{ $ip }} <br>
    - VPS bị trùng: {{ $vps_id }} <br>
    - Thời gian: {{ date('d-m-Y h:m:s') }} <br>
<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
@endsection