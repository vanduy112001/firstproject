@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
    Xin chào, {{ $name }} <br>
    Cảm ơn bạn đã sử dụng dịch vụ của Cloudzone.vn, dưới đây là thông tin nạp tiền vào tài khoản của quý khách: <br><br>
    <b>Chi tiết nạp tiền:</b> <br>
    Số tiền: {{ number_format($amount,0,",",".") . ' VNĐ' }} <br>
    Thời gian: {{ date('d-m-Y h:m:s') }}<br>
    Mã giao dịch: {{ $ma_gd }}<br>
    <br>
    Cloudzone xin cảm ơn quý khách!

<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
<div style="text-align: center;line-height: 20px; font-size: 16px;font-family: roboto;color: rgb(16, 55, 132);">
	Email này được gửi đến {{ $name }} bởi vì bạn đã đăng ký tài khoản với Cloudzone. <br>
	Các đường link trong email này đều dẫn đến trang <a href="http://cloudzone.vn" style="text-decoration: none;">cloudzone.vn</a>.
</div>
@endsection
