@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
    {!! $content !!}
<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
<div style="text-align: center;line-height: 20px; font-size: 16px;font-family: roboto;color: rgb(16, 55, 132);">
	Email này được gửi đến {{ $user_name }} bởi vì bạn đã đăng ký sản phẩm / dịch vụ với Cloudzone. <br>
	Các đường link trong email này đều dẫn đến trang <a href="http://cloudzone.vn" style="text-decoration: none;">cloudzone.vn</a>.
</div>
@endsection
