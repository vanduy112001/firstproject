@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
  Thông tin yêu cầu hổ trợ <br>
  - Khách hàng: {{ $user_name }} <br>
  - Email: {{ $user_email }} <br>
  - Tiêu đề: {{ $title }} <br>
  - Nội dung: {{ $content }} <br>
<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
@endsection
