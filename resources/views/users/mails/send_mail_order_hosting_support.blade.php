@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
  Thông tin {{ $type }} <br>
  - Khách hàng: {{ $user_name }} <br>
  - Email: {{ $user_email }} <br>
  - Order ID: {{ $order_id }} <br>
  - Domain: {{ $domain }} <br>
  - Tên SP: {{ $product_name }} <br>
  - Thời gian: {{ $billing_cycle }} <br>
  - Số lượng: {{ $qtt }} <br>
  - Thành tiền: {{ $total }} <br>
<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
@endsection
