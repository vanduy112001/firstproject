@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
  Thông tin khách hàng yêu cầu cài đặt lại VPS <br>
  - Vm_id: {{ $vm_id }} <br>
  - OS:
  @if($template == 1)
    Windows 7 <br>
    - Security: Có <br>
  @elseif($template == 2)
    Windows 2012 <br>
    - Security: Có <br>
  @elseif($template == 3)
    Windows 2016 <br>
    - Security: Có <br>
  @elseif($template == 4)
    CentOs 7 64bit <br>
    - Security: Có <br>
  @elseif($template == 11)
    Windows 7 <br>
    - Security: Không <br>
  @elseif($template == 12)
    Windows 2012 <br>
    - Security: Không <br>
  @elseif($template == 13)
    Windows 2016 <br>
    - Security: Không <br>
  @elseif($template == 14)
    CentOs 7 64bit <br>
    - Security: Không <br>
  @endif


<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
@endsection
