@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
    <b>Thông tin VPS</b> <br>
    - Customer ID: {{ $customer }} <br>
    - Khách hàng: {{ !empty($vps->user_vps->name) ?  $vps->user_vps->name : $vps->user_id }} <br>
    - CPU: {{ $cpu }} <br>
    - RAM: {{ $ram }} <br>
    - Disk: {{ $disk }} <br>
    - Thời gian: {{ date('d-m-Y h:m:s') }}
<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
@endsection
