@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
    Xin chào {{ $name }} <br>
    Cảm ơn quý khách đã đặt hàng thành công. Thông tin đặt hàng của quý khách: <br><br>
    <b>Thông tin dịch vụ Hosting</b> <br>
    <b>Hosting: </b> {{ $domain }} <br>
    <b>Dịch vụ Hosting: </b> {{ $product_name }} <br>
    <b>Cloudzone đã hoàn tất nâng cấp dịch vụ Hosting cho quý khách!</b>
<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
<div style="text-align: center;line-height: 20px; font-size: 16px;font-family: roboto;color: rgb(16, 55, 132);">
	Email này được gửi đến {{ $name }} bởi vì bạn đã đăng ký tài khoản với Cloudzone. <br>
	Các đường link trong email này đều dẫn đến trang <a href="http://cloudzone.vn" style="text-decoration: none;">cloudzone.vn</a>.
</div>
@endsection
