@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
    Xin chào, {{ $name }} <br>
    Yêu cầu thanh toán dịch vụ của quý khách thất bại vì lý do số tiền chuyển khoản thấp hơn
    số tiền thanh toán dịch vụ
    <br><br>
    <b>Chi tiết giao dịch thanh toán dịch vụ:</b> <br>
    Mã giao dịch: {{ $payment->ma_gd }}<br>
    Số tiền thanh toán dịch vụ: {{ number_format($payment->money,0,",",".") . ' VNĐ' }}<br>
    <br>
    <b>Chi tiết giao dịch của khách hàng</b><br>
    Số tiền: {{ number_format($amount,0,",",".") . ' VNĐ' }}<br>
    Hình thức thanh toán: chuyển khoản {{ $method }} <br>
    Số tiền còn thiếu: {{ number_format( (int) $payment->money - (int) $data['amount'] ,0,",",".") }} VNĐ <br>

    <br>
    Chúng tôi sẻ chuyển tất cả số tiền giao dịch của quý khách vào số dư tài khoản của quý khách. 
    Sau khi chuyển khoản thêm số tiền còn thiếu, quý khách vui lòng liên hệ với chúng tôi qua Fanpage / email / sdt để chúng tôi hỗ trợ và kích hoạt dịch vụ 
    <br>
    <b>Chi tiết giao dịch:</b> <br>
    Số tiền: {{ number_format($amount,0,",",".") . ' VNĐ' }}<br>
    Thời gian: {{ date('h:i:s d-m-Y') }}<br>
    Mã giao dịch: {{ $ma_gd }}<br>
    Số dư tài khoản: {{ number_format($credit,0,",",".") . ' VNĐ' }}<br>
    <br>
    Cloudzone xin cảm ơn quý khách!

<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
<div style="text-align: center;line-height: 20px; font-size: 16px;font-family: roboto;color: rgb(16, 55, 132);">
	Email này được gửi đến {{ $name }} bởi vì bạn đã đăng ký tài khoản với Cloudzone. <br>
	Các đường link trong email này đều dẫn đến trang <a href="http://cloudzone.vn" style="text-decoration: none;">cloudzone.vn</a>.
</div>
@endsection
