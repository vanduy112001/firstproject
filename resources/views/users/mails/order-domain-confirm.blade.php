@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
    Xin chào {{ $user_name }}! <br><br>
    Cảm ơn quý khách đã đặt hàng. Dưới đây là thông tin đặt hàng của quý khách: <br><br>
    <b>Domain: {{ $domain}}</b> <br>
    Password: {{ $password_domain }} <br>
    Thời hạn: {{ $domain_year }} <br>
    Tổng thanh toán: {{ $total }} VNĐ<br>
    <b>Cloudzone xin cảm ơn quý khách!</b>
<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
<div style="text-align: center;line-height: 20px; font-size: 16px;font-family: roboto;color: rgb(16, 55, 132);">
	Email này được gửi đến {{ $user_name }} bởi vì bạn đã đặt hàng domain {{ $domain}} . <br>
	Các đường link trong email này đều dẫn đến trang <a href="http://cloudzone.vn" style="text-decoration: none;">cloudzone.vn</a>.
</div>
@endsection
