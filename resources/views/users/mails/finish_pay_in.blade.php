@extends('layouts.app_mail')
@section('content')
<!-- Nội dung mail tạo tài khoản -->
    Xin chào, {{ $name }} <br>
    Yêu cầu nạp tiền vào tài khoản của bạn đã được thực hiện thành công! <br><br>
    <b>Chi tiết giao dịch:</b> <br>
    Số tiền: {{ number_format($amount,0,",",".") . ' VNĐ' }}<br>
    Thời gian: {{ date('h:i:s d-m-Y') }}<br>
    Mã giao dịch: {{ $ma_gd }}<br>
    Số dư tài khoản: {{ number_format($credit,0,",",".") . ' VNĐ' }}<br>
    <br>
    Cloudzone xin cảm ơn quý khách!

<!-- kết thúc mail tạo tài khoản -->
@endsection
@section('footer')
<div style="text-align: center;line-height: 20px; font-size: 16px;font-family: roboto;color: rgb(16, 55, 132);">
	Email này được gửi đến {{ $name }} bởi vì bạn đã đăng ký tài khoản với Cloudzone. <br>
	Các đường link trong email này đều dẫn đến trang <a href="http://cloudzone.vn" style="text-decoration: none;">cloudzone.vn</a>.
</div>
@endsection
