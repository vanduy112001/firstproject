@php
$user = UserHelper::get_user(Auth::user()->id);
@endphp
@extends('layouts.user2.app')
@section('title')
<div class="text-primary">Tạo hồ sơ khách hàng</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Tạo hồ sơ khách hàng</li>
@endsection
@section('content')
<div class="row">
  <div class="container-fluid">
    <div class="col-md-12 detail-course-finish">
      @if(session("success"))
      <div class="bg-success" style="width:100%;">
        <p class="text-light">{{session("success")}}</p>
      </div>
      @elseif(session("fails"))
      <div class="bg-danger" style="width:100%;">
        <p class="text-light">{{session("fails")}}</p>
      </div>
      @endif
      @if ($errors->any())
      <div class="alert alert-danger" style="margin-top:20px;width:100%;">
        <ul>
          @foreach ($errors->all() as $error)
          <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif
    </div>
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Thông tin chi tiết</h3>
      </div>
      <div class="card-body">
        <div class="form-customer">
          <form action="{{ route('user.customer.store') }}" method="post" id="form_customer">
            @csrf
            <div class="row form-group">
              <input class="type_checked" type="hidden"  name="type_customer" value="0">
              <div class="luu-y col-sm-12">
                  Lưu ý: Các thông tin có dấu * là bắt buộc
              </div>
            </div>
            <!-- loại chủ thể cá nhân -->
            <div class="radio_personal">
              <div class="card-danger">
                <div class="card-body">
                  <div class="row form-group">
                    <div class="col-sm-4">
                      <label for="type_customer" class="required">Họ và tên người đăng ký</label>
                    </div>
                    <div class="col-sm-8">
                      <input type="text" name="customer_name" value="{{ old('customer_name') }}" placeholder="Họ và tên" class="form-control @error('customer_name') is-invalid @enderror">
                    </div>
                  </div>
                  {{-- <div class="row form-group">
                    <div class="col-sm-4">
                      <label for="" class="required">Giới tính</label>
                    </div>
                    <div class="col-sm-8">
                      <select class="form-control @error('customer_gender') is-invalid @enderror" name="customer_gender">
                        <option value="Nam" @if( old('customer_gender') == 'Nam' ) selected @endif>Nam</option>
                        <option value="Nữ" @if( old('customer_gender') == 'Nữ' ) selected @endif>Nữ</option>
                      </select>
                    </div>
                  </div> --}}
                  {{-- <div class="row form-group">
                    <div class="col-sm-4">
                      <label for="" class="required">Ngày sinh (mm/dd/YYYY)</label>
                    </div>
                    <div class="col-sm-8">
                      <input type="text" name="customer_date" id="customer_date" value="{{ old('customer_date') }}" class="form-control  @error('customer_date') is-invalid @enderror" placeholder="Ngày sinh">
                      <div class="luu-y">
                          Lưu ý: Nhập theo đúng định dạng Tháng / Ngày / Năm. Ví dụ: 06/16/1992
                      </div>
                    </div>
                  </div> --}}
                  {{-- <div class="row form-group">
                    <div class="col-sm-4">
                      <label for="" class="required">Số chứng minh nhân dân</label>
                    </div>
                    <div class="col-sm-8">
                      <input type="text" name="customer_cmnd" value="{{ old('customer_cmnd') }}" class="form-control  @error('customer_cmnd') is-invalid @enderror" placeholder="Số chứng minh nhân dân">
                    </div>
                  </div> --}}
                  <div class="row form-group">
                    <div class="col-sm-4">
                      <label for="">Số điện thoại</label>
                    </div>
                    <div class="col-sm-8">
                      <input type="text" name="customer_phone" value="{{ old('customer_phone') }}" class="form-control  @error('customer_phone') is-invalid @enderror" placeholder="Số điện thoại">
                    </div>
                  </div>
                  <div class="row form-group">
                    <div class="col-sm-4">
                      <label for="">Email</label>
                    </div>
                    <div class="col-sm-8">
                      <input type="email" name="customer_email" value="{{ old('customer_email') }}" class="form-control  @error('customer_email') is-invalid @enderror" placeholder="Địa chỉ Email">
                    </div>
                  </div>
                  <div class="row form-group">
                    <div class="col-sm-4">
                      <label for="">Facebook</label>
                    </div>
                    <div class="col-sm-8">
                      <input type="text" name="customer_address" value="{{ old('customer_address') }}" class="form-control  @error('customer_address') is-invalid @enderror" placeholder="Facebook">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.loại chủ thể cá nhân -->
            <!-- Loại chủ thể tổ chức -->
            <div class="radio_organization">
                <!-- Thông tin tổ chức -->
                <div class="company_info">
                  <div class="card-danger">
                      <div class="card-header">
                        <h3 class="card-title">Thông tin tổ chức</h3>
                      </div>
                      <div class="card-body">
                        <div class="row form-group">
                          <div class="col-sm-4">
                            <label for="customer_tc_name" class="required">Tên tổ chức</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" name="customer_tc_name" id="customer_tc_name" value="{{ old('customer_tc_name') }}" placeholder="Tên tổ chức theo giấy phép kinh doanh hoặc con dấu" class="form-control @error('customer_tc_name') is-invalid @enderror">
                          </div>
                        </div>
                        <div class="row form-group">
                          <div class="col-sm-4">
                            <label for="customer_tc_mst" class="required">Mã số thuế</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" name="customer_tc_mst" value="{{ old('customer_tc_mst') }}" placeholder="Mã số thuế" class="form-control @error('customer_tc_mst') is-invalid @enderror">
                          </div>
                        </div>
                        <div class="row form-group">
                          <div class="col-sm-4">
                            <label for="customer_tc_sdt" class="required">Số điện thoại</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" name="customer_tc_sdt" value="{{ old('customer_tc_sdt') }}" placeholder="Số điện thoại tổ chức" class="form-control @error('customer_tc_sdt') is-invalid @enderror">
                          </div>
                        </div>
                        <div class="row form-group">
                          <div class="col-sm-4">
                            <label for="customer_tc_dctc"  class="required">Địa chỉ tổ chức</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" name="customer_tc_dctc" value="{{ old('customer_tc_dctc') }}" placeholder="Địa chỉ tổ chức" class="form-control @error('customer_tc_dctc') is-invalid @enderror">
                          </div>
                        </div>
                        <div class="row form-group">
                          <div class="col-sm-4">
                            <label for="customer_tc_city" class="required">Tỉnh / Thành phố</label>
                          </div>
                          <div class="col-sm-8">
                            <select class="form-control @error('customer_tc_city') is-invalid @enderror" name="customer_tc_city">
                              <option value="" disabled selected>--- Chọn Tỉnh / Thành phố ---</option>
                              @foreach($list_city as $city)
                              <?php
                                $selected = '';
                                if (old('customer_tc_city') == $city) {
                                  $selected = 'selected';
                                }
                              ?>
                              <option value="{{$city}}" {{$selected}}>{{$city}}</option>
                              @endforeach
                            </select>
                          </div>
                        </div>
                        <div class="row form-group">
                          <div class="col-sm-4">
                            <label for="customer_dk_name" class="required">Họ và tên người đăng ký tài khoản</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" name="customer_dk_name" value="{{ old('customer_dk_name') }}" placeholder="Họ và tên phải theo CMND" class="customer_dk_name form-control @error('customer_dk_name') is-invalid @enderror">
                          </div>
                        </div>
                        <div class="row form-group">
                          <div class="col-sm-4">
                            <label for="" class="required">Giới tính</label>
                          </div>
                          <div class="col-sm-8">
                            <select class="form-control customer_dk_gender @error('customer_dk_gender') is-invalid @enderror" name="customer_dk_gender">
                              <option value="Nam" @if( old('customer_dk_gender') == 'Nam' ) selected @endif>Nam</option>
                              <option value="Nữ" @if( old('customer_dk_gender') == 'Nữ' ) selected @endif>Nữ</option>
                            </select>
                          </div>
                        </div>
                        <div class="row form-group">
                          <div class="col-sm-4">
                            <label for="" class="required">Chức vụ</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" name="customer_dk_cv" value="{{ old('customer_dk_cv') }}" class="customer_dk_cv form-control  @error('customer_dk_cv') is-invalid @enderror" placeholder="Chức vụ người đăng ký">
                          </div>
                        </div>
                        <div class="row form-group">
                          <div class="col-sm-4">
                            <label for="" class="required">Ngày sinh (mm/dd/YYYY)</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" name="customer_dk_date" id="customer_dk_date" value="{{ old('customer_dk_date') }}" class="customer_dk_date form-control  @error('customer_dk_date') is-invalid @enderror" placeholder="Ngày sinh">
                            <div class="luu-y">
                                Lưu ý: Nhập theo đúng định dạng Tháng / Ngày / Năm. Ví dụ: 06/16/1992
                            </div>
                          </div>
                        </div>
                        <div class="row form-group">
                          <div class="col-sm-4">
                            <label for="" class="required">Số chứng minh nhân dân</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" name="customer_dk_cmnd" value="{{ old('customer_dk_cmnd') }}" class="customer_dk_cmnd form-control  @error('customer_dk_cmnd') is-invalid @enderror" placeholder="Số chứng minh nhân dân">
                          </div>
                        </div>
                        <div class="row form-group">
                          <div class="col-sm-4">
                            <label for="" class="required">Số điện thoại người đăng ký</label>
                          </div>
                          <div class="col-sm-8">
                            <input type="text" name="customer_dk_phone" value="{{ old('customer_dk_phone') }}" class="customer_dk_phone form-control  @error('customer_dk_phone') is-invalid @enderror" placeholder="Số điện thoại">
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-sm-4">

                          </div>
                          <div class="col-sm-8">
                              <button type="button" class="btn btn-info info_copy" title="Sao chép thông tin người đăng ký cho người đại diện và người đại diện làm thủ tục">Sao chép thông tin</button>
                          </div>
                        </div>
                      </div>
                  </div>
                </div>
                <!-- /.Thông tin tổ chức -->
                <!-- Thông tin người đại diện -->
                <div class="info_dd">
                    <div class="card-danger">
                        <div class="card-header">
                          <h3 class="card-title">Thông tin người đại diện</h3>
                        </div>
                        <div class="card-body">
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="customer_dd_name" class="required">Họ và tên người đại diện</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_dd_name" value="{{ old('customer_dd_name') }}" placeholder="Họ và tên phải theo CMND" class="customer_dd_name form-control @error('customer_dd_name') is-invalid @enderror">
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Chức vụ</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_dd_cv" value="{{ old('customer_dd_cv') }}" class="customer_dd_cv form-control  @error('customer_dd_cv') is-invalid @enderror" placeholder="Chức vụ người đại diện">
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Giới tính</label>
                            </div>
                            <div class="col-sm-8">
                              <select class="customer_dd_gender form-control @error('customer_dd_gender') is-invalid @enderror" name="customer_dd_gender">
                                <option value="Nam" @if( old('customer_dd_gender') == 'Nam' ) selected @endif>Nam</option>
                                <option value="Nữ" @if( old('customer_dd_gender') == 'Nữ' ) selected @endif>Nữ</option>
                              </select>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Ngày sinh (mm/dd/YYYY)</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_dd_date" id="customer_dd_date" value="{{ old('customer_dd_date') }}" class="form-control  @error('customer_dd_date') is-invalid @enderror" placeholder="Ngày sinh">
                              <div class="luu-y">
                                  Lưu ý: Nhập theo đúng định dạng Tháng / Ngày / Năm. Ví dụ: 06/16/1992
                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Số chứng minh nhân dân</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_dd_cmnd" value="{{ old('customer_dd_cmnd') }}" class="customer_dd_cmnd form-control  @error('customer_dd_cmnd') is-invalid @enderror" placeholder="Số chứng minh nhân dân">
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Email</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="email" name="customer_dd_email" value="{{ old('customer_dd_email') }}" class="customer_dd_email form-control  @error('customer_dd_email') is-invalid @enderror" placeholder="Địa chỉ Email">
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-sm-4">

                            </div>
                            <div class="col-sm-8">
                                <button type="button" class="btn btn-info info_dd_copy" title="Sao chép thông tin người đại diện cho người đại diện làm thủ tục">Sao chép thông tin</button>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
                <!-- /.Thông tin người đại diện -->
                <!-- Thông tin người đại diện làm thủ tục -->
                <div class="info_dd">
                    <div class="card-danger">
                        <div class="card-header">
                          <h3 class="card-title">Thông tin người đại diện làm thủ tục</h3>
                        </div>
                        <div class="card-body">
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="customer_tt_name" class="required">Tên người đại diện làm thủ tục</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_tt_name" value="{{ old('customer_tt_name') }}" placeholder="Họ và tên phải theo CMND" class="customer_tt_name form-control @error('customer_tt_name') is-invalid @enderror">
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Giới tính</label>
                            </div>
                            <div class="col-sm-8">
                              <select class="customer_tt_gender form-control @error('customer_tt_gender') is-invalid @enderror" name="customer_tt_gender">
                                <option value="Nam" @if( old('customer_tt_gender') == 'Nam' ) selected @endif>Nam</option>
                                <option value="Nữ" @if( old('customer_tt_gender') == 'Nữ' ) selected @endif>Nữ</option>
                              </select>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Ngày sinh (mm/dd/YYYY)</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_tt_date" id="customer_tt_date" value="{{ old('customer_tt_date') }}" class="form-control  @error('customer_tt_date') is-invalid @enderror" placeholder="Ngày sinh">
                              <div class="luu-y">
                                  Lưu ý: Nhập theo đúng định dạng Tháng / Ngày / Năm. Ví dụ: 06/16/1992
                              </div>
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Số chứng minh nhân dân</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="text" name="customer_tt_cmnd" value="{{ old('customer_tt_cmnd') }}" class="customer_tt_cmnd form-control  @error('customer_tt_cmnd') is-invalid @enderror" placeholder="Số chứng minh nhân dân">
                            </div>
                          </div>
                          <div class="row form-group">
                            <div class="col-sm-4">
                              <label for="" class="required">Email</label>
                            </div>
                            <div class="col-sm-8">
                              <input type="email" name="customer_tt_email" value="{{ old('customer_tt_email') }}" class="customer_tt_email form-control  @error('customer_tt_email') is-invalid @enderror" placeholder="Địa chỉ Email">
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
                <!-- /.Thông tin người đại diện làm thủ tục -->
            </div>
            <!-- /.Loại chủ thể tổ chức -->
            <div class="row button-form">
              <div class="col-sm-4">

              </div>
              <div class="col-sm-8">
                <button type="button" class="btn btn-secondary" id="reset_form">Nhập lại</button>
                <input type="submit" value="Tạo hồ sơ" class="btn btn-primary">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

  </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('libraries//bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('js/cutomer.js')  }}"></script>
@endsection
