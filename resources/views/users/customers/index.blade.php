@php
    $user = UserHelper::get_user(Auth::user()->id);
@endphp
@extends('layouts.user2.app')
@section('title')
<div class="text-primary">Hồ sơ khách hàng</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Hồ sơ khách hàng</li>
@endsection
@section('content')
<div class="row">
  <div class="title col-md-12">
      <div class="title-body">
          <div class="row">
              <!-- button tạo -->
              <div class="col-md-4">
                  <a href="{{ route('user.customer.create') }}" class="btn btn-info btn-sm"><i class="fas fa-user-plus"></i> Tạo khách hàng mới</a>
              </div>
              <div class="col-md-4"></div>
              <!-- form search -->
              <div class="col-md-4">
                  <div class="row">
                      <div class="form-group col-md-12">
                          <div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-12  mt-4">
                  @if(session("success"))
                      <div class="bg-success  p-3">
                          {{session("success")}}
                      </div>
                  @elseif(session("fails"))
                      <div class="bg-danger p-3">
                          {{session("fails")}}
                      </div>
                  @endif
                  @if ($errors->any())
                      <div class="alert alert-danger" style="margin-top:20px;width:100%;">
                          <ul>
                              @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
              </div>
          </div>
      </div>
  </div>
  <div class="col-12">
    <div class="box box-primary">
        <div class="box-body">
            <div class="col-md-12 detail-course-finish table-responsive">
                <div class="dataTables_wrapper form-inline dt-bootstrap">
                    <table class="table table-bordered table-hover">
                        <thead class="primary">
                            <th>STT</th>
                            <th>MAKH</th>
                            <th>Chủ thể</th>
                            <th>Tên đăng ký</th>
                            <th>Số điện thoại</th>
                            <th>Thao tác</th>
                        </thead>
                        @if ( $list_customers->count() > 0 )
                          @foreach($list_customers as $key => $customer)
                            <tr>
                              <td>{{ $key + 1 }}</td>
                              <td><a href="{{ route('user.customer.detail', $customer->id) }}">{{ $customer->ma_customer }}</a></td>
                              <td>{{ $customer->type_customer == 0 ? 'Cá nhân' : 'Tổ chức' }}</td>
                              <td>{{ !empty($customer->customer_name) ? $customer->customer_name : $customer->customer_tc_name }}</td>
                              <td>{{ !empty($customer->customer_phone) ? $customer->customer_phone : $customer->customer_tc_sdt }}</td>
                              <td>
                                  <a href="{{ route('user.customer.detail', $customer->id) }}" class="btn btn-outline-success"><i class="fas fa-chart-bar"></i></a>
                                  <a href="{{ route('user.customer.edit', $customer->id) }}" class="btn btn-outline-warning"><i class="fas fa-edit"></i></a>
                                  <a href="#" class="btn btn-outline-danger btn-delete-customer"
                                        data-id="{{ $customer->id }}" data-name="{{ !empty($customer->customer_name) ? $customer->customer_name : $customer->customer_tc_name }}">
                                    <i class="far fa-trash-alt"></i>
                                  </a>
                              </td>
                            </tr>
                          @endforeach
                        @else
                            <tbody>
                                <tr>
                                    <td colspan="6" class="text-center text-danger">Dữ liệu khách hàng không tồn tại.</td>
                                </tr>
                            </tbody>
                        @endif
                    </table>
                </div>
            </div>
        </div>
  </div>
</div>
</div>
{{-- Modal xoa customer --}}
<div class="modal fade" id="delete-customer">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa hồ sơ khách hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-customer" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-customer" value="Xóa khách hàng">
          <button type="button" class="btn btn-danger" id="button-finish"  data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
@endsection
@section('scripts')
<script src="{{ asset('js/cutomer.js')  }}"></script>
@endsection
