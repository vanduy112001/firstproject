@php
$user = UserHelper::get_user(Auth::user()->id);
@endphp
@extends('layouts.user2.app')
@section('title')
<div class="text-primary">Hồ sơ khách hàng</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item"><a href="{{ route('user.customer.index') }}"> Danh sách khách hàng</a></li>
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i>
    {{ !empty($customer->makh) ? $customer->makh : '' }}</li>
@endsection
@section('content')
<div class="">
    <div class="title col-md-12">
        <div class="title-body">
            <div class="row">
                <!-- button tạo -->
                <a href="{{ route('user.customer.list_order_with_customer', $customer->ma_customer) }}"
                    class="btn btn-primary m-1"><i class="fas fa-clipboard-list"></i> Danh sách đơn hàng</a>
                <a href="{{ route('user.customer.edit', $customer->id) }}" class="btn btn-warning m-1"><i
                        class="fas fa-edit"></i> Chỉnh sửa thông tin</a>
                <a href="#" class="btn btn-danger btn-detail-delete-customer m-1" data-id="{{ $customer->id }}"
                    data-name="{{ !empty($customer->customer_name) ? $customer->customer_name : $customer->customer_tc_name }}">
                    <i class="far fa-trash-alt"></i> Xóa khách hàng
                </a>

            </div>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-body">
            <div class="col-md-12 detail-course-finish table-responsive">
                <div class="dataTables_wrapper form-inline dt-bootstrap">
                    <table class="table-detail-customer" style="width:100%">
                        <tr>
                            <td class="td-title" width="40%">Chủ thể</td>
                            <td>{{ $customer->type_customer == 0 ? 'Cá nhân' : 'Tổ chức' }}</td>
                        </tr>
                        @if ($customer->type_customer == 0)
                        <tr>
                            <td class="td-title" width="40%">Tên đầy đủ</td>
                            <td>{{ $customer->customer_name }}</td>
                        </tr>
                        <tr>
                            <td class="td-title" width="40%">Số điện thoại</td>
                            <td>{{ $customer->customer_phone }}</td>
                        </tr>
                        <tr>
                            <td class="td-title" width="40%">Email</td>
                            <td>{{ !empty($customer->customer_email) ? $customer->customer_email : $customer->customer_tc_name }}</td>
                        </tr>
                        <tr>
                            <td class="td-title" width="40%">Facebook</td>
                            <td>{{ $customer->customer_address }}</td>
                        </tr>
                        @else
                        <tr>
                            <td class="td-title" width="40%">Tên tổ chức</td>
                            <td>{{ $customer->customer_tc_name }}</td>
                        </tr>
                        <tr>
                            <td class="td-title" width="40%">Địa chỉ liên lạc</td>
                            <td>{{ $customer->customer_tc_dctc }}</td>
                        </tr>
                        <tr>
                            <td class="td-title" width="40%">Thành phố</td>
                            <td>{{ $customer->customer_tc_city }}</td>
                        </tr>
                        <tr>
                            <td class="td-title" width="40%">Số điện thoại tổ chức</td>
                            <td>{{ $customer->customer_tc_sdt }}</td>
                        </tr>
                        <tr>
                            <td class="td-title" width="40%">Mã số thuế</td>
                            <td>{{ $customer->customer_tc_mst }}</td>
                        </tr>
                        <tr>
                            <td class="td-title" width="40%">Tên người đại diện</td>
                            <td>{{ $customer->customer_dd_name }}</td>
                        </tr>
                        <tr>
                            <td class="td-title" width="40%">Chức vụ</td>
                            <td>{{ $customer->customer_dd_cv }}</td>
                        </tr>
                        <tr>
                            <td class="td-title" width="40%">Ngày sinh</td>
                            <td>{{ date('m/d/Y', strtotime($customer->customer_dd_date)) }}</td>
                        </tr>
                        <tr>
                            <td class="td-title" width="40%">Số điện thoại người đại diện</td>
                            <td>{{ $customer->customer_dk_phone }}</td>
                        </tr>
                        <tr>
                            <td class="td-title" width="40%">Email người đại diện</td>
                            <td>{{ $customer->customer_dd_email }}</td>
                        </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- Modal xoa customer --}}
<div class="modal fade" id="delete-customer">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Xóa hồ sơ khách hàng</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="notication-customer" class="text-center">

                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                <input type="submit" class="btn btn-primary" id="button-detail-customer" value="Xóa khách hàng">
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@endsection
@section('scripts')
<script src="{{ asset('js/cutomer.js')  }}"></script>
@endsection
