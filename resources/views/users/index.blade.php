@php
    $user = UserHelper::get_user(Auth::user()->id);
@endphp
@extends('layouts.user2.app')
@section('title')
    Xin chào, {{ $user->name }}
@endsection
@section('breadcrumb')
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Khu vực khách hàng</li>
@endsection
@section('content')
<div class="row">
    <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-8">
        <div class="row">
          <div class="col-md-12">
              @if(session("success"))
                  <div class="bg-success">
                      <p class="text-light">{{session("success")}}</p>
                  </div>
              @elseif(session("fails"))
                  <div class="bg-danger">
                      <p class="text-light">{{session("fails")}}</p>
                  </div>
              @endif
          </div>

          <!-- Khu vực sliderbar top -->

          <!-- Tổng số dịch vụ -->
          <div class="col-xl-3 col-md-6 col-6 mb-4">
              <div class="card border-left-primary shadow h-100">
                  <a href="javascript:void(0)">
                      <div class="card-body p-sm-3 p-2">
                          <div class="row no-gutters align-items-center total_services">
                              <div class="col mr-2">
                                  <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Dịch vụ</div>
                                  <div class="h5 mb-0 font-weight-bold text-gray-800 text_siderbar_top"></div>
                              </div>
                              <div class="col-auto">
                                  <i class="fas fa-calendar fa-2x text-gray-300"></i>
                              </div>
                          </div>
                      </div>
                  </a>
              </div>
          </div>

          <!-- Số dư tài khoản -->
          <div class="col-xl-3 col-md-6 col-6 mb-4">
              <div class="card border-left-primary shadow h-100">
                  <div class="card-body p-sm-3 p-2">
                      <div class="row no-gutters align-items-center">
                          <div class="col mr-2">
                              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Tài khoản</div>
                              <div class="mb-0 font-weight-bold text-gray-800 account_balance" style="width: 140%;">
                                  @if(!empty($user->credit->value))
                                      {!!number_format($user->credit->value,0,",",".") . ' ₫'!!}
                                  @else
                                      0 ₫
                                  @endif
                              </div>
                          </div>
                          <div class="col-auto">
                              <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

          <!-- Tổng số thanh toán chờ -->
          <div class="col-xl-3 col-md-6 col-6 mb-4">
              <div class="card border-left-primary shadow h-100">
                  <a href="{{ route('order.list_invoices.progressing')  }}">
                      <div class="card-body p-sm-3 p-2">
                          <div class="row no-gutters align-items-center">
                                  <div class="col mr-2 total_notifica">
                                      <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Thanh toán chờ</div>
                                      <div class="h5 mb-0 font-weight-bold text-gray-800 text_siderbar_top">
                                      </div>
                                  </div>
                                  <div class="col-auto">
                                      <i class="fa fa-credit-card fa-2x text-gray-300"></i>
                                  </div>
                          </div>
                      </div>
                  </a>
              </div>
          </div>

          <!-- Số lượng ticket  -->
          <div class="col-xl-3 col-md-6 col-6 mb-4">
              <div class="card border-left-primary shadow h-100">
                  <div class="card-body p-sm-3 p-2">
                      <div class="row no-gutters align-items-center">
                          <div class="col mr-2 total_ticket">
                              <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Tickets</div>
                              <div class="h5 mb-0 font-weight-bold text-gray-800 total_notifica">0</div>
                          </div>
                          <div class="col-auto">
                              <i class="fas fa-comments fa-2x text-gray-300"></i>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

          <!-- Khu vực trung tâm -->

          <!-- Thống kê dịch vụ -->
          <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 mb-4" id="home-services-center">
              <div class="card card-outline card-primary mb-0 shadow">
                  <div class="card-header">
                      <h4 class="card-title title-services" style="font-size: 14px"><i class="fas fa-chart-pie"></i> Thống kê dịch vụ</h4>
                  </div>
                  <div class="card-body py-1 px-4" id="home-service-chart">
                    {{-- <div class="row" id="services_header" style="margin-bottom: 10px;border: 1px solid white;background: #007bff;color: white;">
                      <div class="col-sm-4 home-mobile-repon" style="border-right: 0.1px solid;padding: 8px 10px;">
                          Dịch vụ
                      </div>
                      <div class="col-sm-4 home-mobile-repon" style="border-right: 0.1px solid;padding: 8px 10px;">
                          Số lượng
                      </div>
                      <div class="col-4 text-right home-mobile-repon" style="border-right: 0.1px solid;padding: 8px 10px;">
                      </div>
                    </div> --}}
                    <div class="home-service-chart">
                    </div>
                  </div>
              </div>
          </div>

          <!-- Thông báo -->
          <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 mb-4 scroll-info" id="home-notication">
              <div class="card card-outline card-primary shadow content_center">
                  <div class="card-header">
                      <h4 class="info-header card-title"><i class="fas fa-newspaper"></i> Thông báo mới</h4>
                  </div>
                  <div class="card-body">

                  </div>
              </div>
          </div>

          {{--  Dịch vụ VPS gần hết hạn  --}}
          <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 mb-4" id="total_vps_service_nearly" style="display:none !important;">
              <div class="card card-outline card-primary shadow content_center collapsed-card">
                  <div class="card-header">
                      <h4 class="info-header card-title"><i class="fas fa-calculator"></i> Dịch vụ VPS VN gần hết hạn <a href="/dich-vu/vps/gan-het-han-va-qua-han">Xem chi tiết</a></h4>
                      <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                        </button>
                      </div>
                  </div>
                  <div class="card-body p-sm-3 p-2">
                  </div>
              </div>
          </div>
          {{--  /.Dịch vụ VPS gần hết hạn  --}}

          {{--  Dịch vụ VPS US gần hết hạn  --}}
          <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 mb-4"  id="total_vps_us_service_nearly" style="display:none !important;">
              <div class="card card-outline card-primary shadow content_center collapsed-card">
                  <div class="card-header">
                      <h4 class="info-header card-title"><i class="fas fa-calculator"></i> Dịch vụ VPS US gần hết hạn <a href="/dich-vu/vps_us/gan-het-han-va-qua-han">Xem chi tiết</a></h4>
                      <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i>
                        </button>
                      </div>
                  </div>
                  <div class="card-body p-sm-3 p-2">
                  </div>
              </div>
          </div>
          {{--  /.Dịch vụ VPS US gần hết hạn  --}}

          {{--  Dịch vụ hosting gần hết hạn --}}
          <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 mb-4 scroll-info" id="total_hosting_service_nearly" style="display:none !important;">
              <div class="card card-outline card-primary shadow content_center">
                  <div class="card-header">
                      <h4 class="info-header card-title"><i class="fas fa-comments"></i> Dịch vụ Hosting gần hết hạn</h4>
                  </div>
                  <div class="card-body p-sm-3 p-2">

                  </div>
              </div>
          </div>
          {{--  /.Dịch vụ hosting gần hết hạn  --}}

          {{--  Dịch vụ server gần hết hạn  --}}
          <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 mb-4" id="home-service" style="display:none !important;">
              <div class="card card-outline card-primary shadow content_center">
                  <div class="card-header">
                      <h4 class="info-header card-title"><i class="fas fa-cube"></i> Dịch vụ Server gần hết hạn </h4>
                  </div>
                  <div class="card-body p-sm-3 p-2">

                  </div>
              </div>
          </div>
          {{--  /.Dịch vụ server gần hết hạn   --}}

          {{--  Dịch vụ colocation gần hết hạn --}}
          <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 mb-4 scroll-info" id="home-colocations" style="display:none !important;">
              <div class="card card-outline card-primary shadow content_center">
                  <div class="card-header">
                      <h4 class="info-header card-title"><i class="fas fa-bookmark"></i> Dịch vụ Colocation gần hết hạn </h4>
                  </div>
                  <div class="card-body p-sm-3 p-2">
                  </div>
              </div>
          </div>
          {{--  /.Dịch vụ colocation gần hết hạn --}}

          {{--  Dịch vụ email hosting gần hết hạn --}}
          <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 mb-4 scroll-info" id="home-email-hosting" style="display:none !important;">
              <div class="card card-outline card-primary shadow content_center">
                  <div class="card-header">
                      <h4 class="info-header card-title"><i class="fas fa-bookmark"></i> Dịch vụ Email Hosting gần hết hạn </h4>
                  </div>
                  <div class="card-body p-sm-3 p-2">
                  </div>
              </div>
          </div>
          {{--  /.Dịch vụ email hosting gần hết hạn --}}

        {{-- Block join telegram --}}
        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 mb-4">
            <div class="card">
                <div class="card-body">
                    <img src="{{ asset('/images/bg-block-telegram.png') }}" alt="Telegram" class="join-telegram">
                    <h4>Kênh Telegram Cloudzone 🎉!</h4>
                    <p> Tham gia cập nhập thông tin nhanh chóng về hệ thống, máy chủ, IP  mới cũng như các ưu đãi, khuyến mãi dành cho thành viên </p>
                    <a href="https://t.me/joinchat/f9nGkfaCnco4MDM1" target="_blank"><button type="button" class="btn btn-primary"> Tham gia! </button></a>
                </div>
            </div>
        </div>
        {{-- /.Block join telegram --}}

        {{-- Block join support facebook --}}
        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 mb-4">
            <div class="card facebook-group">
                <div class="card-body">
                    <div class="row">
                        <div class="col-4 col-sm-4 col-lg-4">
                            {{-- <img src="/images/cloud-face-1.png" alt=""> --}}
                        </div>
                        <div class="col-8 col-sm-8 col-lg-8 facebook-group-text">
                            <p>Tham gia nhóm Facebook của Cloudzone để được hỗ trợ, giao lưu và cập nhập các thông tin mới nhất về VPS</p>
                            <a href="https://www.facebook.com/groups/cloudzone.vn" target="_blank"><button type="button" class="btn btn-default btn-sm"><i class="fab fa-facebook-f"></i> Tham gia! </button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- /.Block join support facebook --}}

        </div>
    </div>

    {{--  siderbar right   --}}

    <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-4">
        <div class="row">

            {{-- Thông tin cá nhân --}}
            <div class="col-xl-12 col-md-12">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h3 class="card-title"><i class="fas fa-user-tie"></i> Thông tin cá nhân</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body" style="display: block;">
                        <div class="info-user">
                            Họ và tên: {{ $user->name }} <br>
                            Email: {{ $user->email }} <br>
                            Đặc quyền: {{ !empty($user->group_user->name) ? $user->group_user->name : 'Cơ bản' }} <br>
                            Cloudzone Point: <b class="text-danger">{{ !empty($user->user_meta->point) ? $user->user_meta->point : '0' }} Điểm</b>  <br>
                            Số dư tài khoản:
                              <b class="text-danger">
                                  @if(!empty($user->credit->value))
                                      {!!number_format($user->credit->value,0,",",".") . ' VNĐ'!!}
                                  @else
                                      0 VNĐ
                                  @endif
                              </b> <br>
                            Tổng tiền đã chi:
                              <b class="text-danger">
                                  @if( !empty($user->credit->total) && !empty($user->credit->value) )
                                    @if ($user->credit->total > $user->credit->value)
                                      {!!number_format( abs($user->credit->total - $user->credit->value) ,0,",",".") . ' VNĐ'!!}
                                    @else
                                      0 VNĐ
                                    @endif
                                  @else
                                      0 VNĐ
                                  @endif
                              </b> <br>
                            Tổng tiền đã nạp:
                              <b class="text-danger">
                                @if(!empty($user->credit->total))
                                    {!!number_format($user->credit->total,0,",",".") . ' VNĐ'!!}
                                @else
                                    0 VNĐ
                                @endif
                              </b> <br>
                        </div>
                        <div class="button-info text-center">
                            <a href="{{ route('user.pay_in_online') }}" class="btn btn-warning btn-sm"><i class="far fa-money-bill-alt"></i> Nạp tiền</a>
                            <a href="{{ route('user.history_payment') }}" class="btn btn-danger btn-sm">Tất cả giao dịch</a>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            {{-- /.Thông tin cá nhân --}}

            {{--  Hướng dẫn --}}
            <div class="col-xl-12 col-md-12 mb-4 scroll-info" id="home-tutorials">
                <div class="card card-outline card-secondary shadow content_center">
                    <div class="card-header">
                        <h4 class="info-header card-title"><i class="fas fa-bookmark"></i> Hướng dẫn</h4>
                    </div>
                    <div class="card-body p-sm-3 p-2">
                    </div>
                </div>
            </div>

            {{--  /.Hướng dẫn --}}
            {{-- Danh sách khách hàng --}}
            {{-- <div class="col-xl-12 col-md-12">
                <h3 class="title-services"><i class="fas fa-users"></i> Danh sách khách hàng</h3>
                <div class="card card-info">
                    <div class="card-header text-white">
                      <div class="row">
                        <div class="col-sm-3 home-mobile-repon-3">
                            MAKH
                        </div>
                        <div class="col-sm-7 home-mobile-repon-7">
                            Tên khách hàng
                        </div>
                        <div class="col-2 text-right home-mobile-repon-2">
                          <div class="card-tools">
                              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                              </button>
                          </div>
                        </div>
                      </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body" style="display: block;">
                      <div class="home-service-customer">

                      </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div> --}}
            {{-- /.Danh sách khách hàng --}}
        </div>

    </div>
</div>
@endsection
@section('scripts')
<!-- <script src="{{ url('libraries/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script> -->
<script src="{{ url('js/home.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#scroll-product').slimscroll({
            // height: 'auto'
        });
        $('#scroll-ticket').slimscroll({
            // height: 'auto'
        });
        $('#scroll-notication').slimscroll({
            // height: 'auto'
        });
        $('#scroll-tutorials').slimscroll({
            // height: 'auto'
        });
        $('#home-service-chart').slimscroll({

        });
    });
</script>
@endsection
