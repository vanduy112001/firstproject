@extends('layouts.user2.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('/libraries/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/libraries/toastr/toastr.min.css')}}">
    <style>
        .ticket-status-pending {
            color: orange;
        }

        .ticket-status-done {
            color: green;
        }

        .ticket-status-processing {
            color: blue;
        }

        .ticket-status-error {
            color: red;
        }
    </style>
@endsection
@section('title')
    <div class="text-primary">Quản lý tickets</div>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
    <li class="breadcrumb-item active">Quản lý tickets</li>
@endsection
@section('content')
    <div class="row">
        <div class="col col-md-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <a href="{{ route('user.ticket.create') }}" class="btn btn-success">+ Tạo ticket</a>
                    <hr>

                    <div class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="col-md-12 detail-course-finish">
                            @if(session("success"))
                                <div class="bg-success">
                                    <p class="text-light">{{session("success")}}</p>
                                </div>
                            @elseif(session("fails"))
                                <div class="bg-danger">
                                    <p class="text-light">{{session("fails")}}</p>
                                </div>
                            @endif
                        </div>
                        <table class="table table-bordered table-hover">
                            <thead class="info">
                            <th>Mã ticket</th>
                            <th>Tiêu đề</th>
                            <th>Nội dung</th>
                            <th>Trạng thái</th>
                            <th>Ngày tạo</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @if($list->count() > 0)
                                @foreach($list as $item)
                                    <tr>
                                        <td>
                                            <a href="{{ route('user.ticket.detail',['id' => $item->id]) }}">#{{ $item->id }}</a>
                                        </td>
                                        <td>
                                            <a href="{{ route('user.ticket.detail',['id' => $item->id]) }}">{{ $item->title }}</a>
                                        </td>
                                        <td>{{ !empty($item->content) ? substr($item->content, 0, 100) : '' }}</td>
                                        <td>
                                            <span
                                                class="ticket-status ticket-status-{{ $item->status }}">{{ $item->status }}</span>
                                        </td>
                                        <td>
                                            {{ date('d/m/Y H:i:s',strtotime($item->created_at)) }}
                                        </td>
                                        <td>
                                            <a href="{{ route('user.ticket.detail',['id' => $item->id]) }}">Xem</a>

                                            @if($item->status == 'pending')
                                                /
                                                <a href="{{ route('user.ticket.update',['id' => $item->id]) }}">Sửa</a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <td class="text-center text-danger" colspan="6">
                                    Chưa có tickets nào
                                </td>
                            @endif
                            </tbody>
                            <tfoot class="card-footer clearfix">
                            <td colspan="6" class="text-center">
                                {{ $list->links()  }}
                            </td>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/service.js') }}"></script>
@endsection
