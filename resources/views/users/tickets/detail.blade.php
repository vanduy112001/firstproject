@extends('layouts.user2.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('/libraries/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/libraries/toastr/toastr.min.css')}}">
    <style>
        .box-message {
            border: 1px solid rgba(238, 133, 28, 0.68);
            background: rgba(238, 133, 28, 0.17);
            padding: 20px;
            border-radius: 5px;
            margin-bottom: 20px;
        }

        .box-message.admin {
            background: rgba(0, 180, 78, 0.39);
            border: 1px solid #00b44e;
        }

        .box-message .meta .author {
            font-weight: bold;
            color: #333;
        }

        .box-message .meta .date {
            font-size: 13px;
            font-style: italic;
            color: darkgrey;
        }

        .ticket-status {
            padding: 7px 15px;
            background: green;
            color: white;
            border-radius: 10px;
            font-size: 13px;
        }

        .ticket-status-pending {
            background: orange;
        }

        .ticket-status-done {
            background: green;
        }

        .ticket-status-processing {
            background: blue;
        }

        .ticket-status-error {
            background: red;
        }
    </style>
@endsection
@section('title')
    <div class="text-primary">Ticket #{{ $detail->id }}</div>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
    <li class="breadcrumb-item active"><a href="{{ route('user.ticket.getList') }}">Quản lý tickets</a></li>
    <li class="breadcrumb-item active"> Ticket</li>
@endsection
@section('content')
    <div class="row">
        <div class="col col-md-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">

                    @if ($errors->any())
                        <div class="alert alert-danger" style="margin-top:20px">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @if(session("success"))
                        <div class="bg-success">
                            <p class="text-light">{{session("success")}}</p>
                        </div>
                    @elseif(session("fails"))
                        <div class="bg-danger">
                            <p class="text-light">{{session("fails")}}</p>
                        </div>
                    @endif

                    <div class="info-box-content">
                        <h3>{{ $detail->title }}</h3>
                        <p>{{ $detail->content }}</p>
                        <p><span class="ticket-status ticket-status-{{ $detail->status }}">{{ $detail->status }}</span>
                        </p>
                    </div>
                    <hr>
                    <div class="ctn-message">
                        @if(!empty($detail->messages))
                            @php
                                $userRepo = App\Factories\UserFactories::userRepositories();
                            @endphp
                            @foreach($detail->messages as $message)
                                @php
                                    $user = $userRepo->detail($message->user_id);
                                @endphp

                                <div class="box-message {{ $user->id == $detail->user_id ? 'current_user' : 'admin' }}">
                                    <div class="row">
                                        <div class="col-md-3 ">
                                            <div class="meta" style="color: #333">
                                                <span class="author">{{ $user->name }}</span><br>
                                                <span
                                                    class="date">{{ date('d/m/Y H:i:s',strtotime($message->created_at)) }}</span>
                                            </div>
                                        </div>
                                        <div class=" col-md-9">
                                            <div class="content">
                                                {{ $message->content }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>

                    @if($detail->status == "processing")
                        <hr>
                        <form method="post" action="{{ route('user.ticket.message') }}" class="form-send-message">
                            <div class="form-group">
                                <label>
                                    Nội dung phản hổi
                                </label>
                                <textarea name="content" class="form-control" rows="5"
                                          placeholder="Vui lòng nhập nội dung phản hổi..."></textarea>
                            </div>
                            <button type="submit" class="btn btn-success">
                                Gửi
                            </button>
                            <a href="{{ route('user.ticket.getList') }}" class="btn btn-default">Quay lại</a>


                            <input type="hidden" name="ticket_id" value="{{ $detail->id }}">
                            {{ csrf_field() }}
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/service.js') }}"></script>
@endsection
