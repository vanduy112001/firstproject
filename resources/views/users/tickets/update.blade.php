@extends('layouts.user2.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('/libraries/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
    <link rel="stylesheet" href="{{ asset('/libraries/toastr/toastr.min.css')}}">
@endsection
@section('title')
    <div class="text-primary">Chỉnh sửa ticket #{{ $detail->id }}</div>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
    <li class="breadcrumb-item active"><a href="{{ route('user.ticket.getList') }}">Quản lý tickets</a></li>
    <li class="breadcrumb-item active"> Chỉnh sửa ticket</li>
@endsection
@section('content')
    <div class="row">
        <div class="col col-md-12">
            <div class="box box-primary">
                <div class="box-body table-responsive">

                    @if ($errors->any())
                        <div class="alert alert-danger" style="margin-top:20px">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form method="post" action="{{ route('user.ticket.updateHandle',['id' => $detail->id]) }}">
                        <div class="form-group">
                            <label>Vấn đề bạn cần hỗ trợ?</label>
                            <input type="text" name="title" value="{{ $detail->title }}" class="form-control  @error('title') is-invalid @enderror">
                        </div>
                        <div class="form-group">
                            <label>Miêu tả chi tiết vấn đề đó</label>
                            <textarea name="content" class="form-control
                            @error('content') is-invalid @enderror" rows="5">{{ $detail->content }}</textarea>
                        </div>
                        <button type="submit" class="btn btn-success">Sửa ticket</button>
                        <a href="{{ route('user.ticket.getList') }}" class="btn btn-default">Quay lại</a>

                        {{ csrf_field() }}
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('js/service.js') }}"></script>
@endsection
