@php
    $user = UserHelper::get_user(Auth::user()->id);
    $total_product = GroupProduct::get_total_product(Auth::user()->id);
    $total_notifications = GroupProduct::get_total_notificationRepositories(Auth::user()->id);
    $total_server = GroupProduct::get_total_server(Auth::user()->id);
    $total_vps = GroupProduct::get_total_vps(Auth::user()->id);
    $total_hosting = GroupProduct::get_total_hosting(Auth::user()->id);
    $notifications_reads = GroupProduct::get_total_notificationRepositories(Auth::user()->id);
    $total_price_use = GroupProduct::total_price_use(Auth::user()->id);
@endphp
@extends('layouts.user2.app')
@section('title')
    Xin chào, {{ $user->name }}
@endsection
@section('breadcrumb')
<li class="breadcrumb-item active"><i class="fa fa-users" aria-hidden="true"></i> Khu vực khách hàng</li>
@endsection
@section('content')
<!-- Content Row -->
<div class="row">
    <div class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-8">
        <div class="row">
            {{--  Ticket--}}
            <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 mb-4 scroll-info" id="home-list-ticket">
                <div class="card card-outline card-warning shadow">
                    <div class="card-header">
                        <h4 class="info-header card-title"><i class="fas fa-comments"></i> Danh sách ticket</h4>
                    </div>
                    <div class="card-body p-sm-3 p-2">
                        @if (!empty($tickets))
                            <div class="box-body chat" id="scroll-ticket">
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="all-inverted timeline">
                                            @foreach ($tickets as $ticket)
                                                <a href="">
                                                    <li class="timeline-inverted">
                                                        <div class="timeline-panel">
                                                            <div class="timeline-title">
                                                                <strong>#{{ $ticket->id }} - {{ $ticket->title }}</strong> <br>
                                                                <span class="text-domain">Last update: {{date('D, M, Y', strtotime($ticket->created_at))}}</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </a>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="text-center" style="margin: 10px;">
                                Không có Tickets nào. Nếu bạn cần giúp đỡ, vui lòng <a href="{{ route('user.ticket.create') }}">mở ticket</a>.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            {{--  /.Ticket --}}
            {{--  Services Product --}}
            <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 mb-4" id="home-service">
                <div class="card card-outline card-primary shadow">
                    <div class="card-header">
                        <h4 class="info-header card-title"><i class="fas fa-cube"></i> Sản phẩm / Dịch vụ đang sử dụng</h4>
                    </div>
                    <div class="card-body">
                        @if($total_product > 0)
                            <div class="box-body chat" id="scroll-product">
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="all-inverted timeline">
                                            @foreach ($services as $service)
                                                @if (!empty($service->domain))
                                                    @if(!empty($service->date_create))
                                                      <a href="{{ route('services.detail', $service->id) }}?type=hosting">
                                                          <li class="timeline-inverted">
                                                              <div class="timeline-panel">
                                                                  <div class="timeline-title">
                                                                      @php
                                                                          $product = GroupProduct::get_product($service->product_id);
                                                                      @endphp
                                                                      {{ $product->group_product->name }} - {{ $product->name }} <br>
                                                                      <span class="text-domain">{{$service->domain}} - {{ !empty($service->next_due_date) ? date('d/m/Y', strtotime($service->next_due_date)) : '' }}</span>
                                                                  </div>
                                                              </div>
                                                          </li>
                                                      </a>
                                                    @endif
                                                @else
                                                    @if(!empty($service->ip))
                                                      <a href="{{ route('services.detail', $service->id) }}?type=vps">
                                                          <li class="timeline-inverted">
                                                              <div class="timeline-panel">
                                                                  <div class="timeline-title">
                                                                      @php
                                                                          $product = GroupProduct::get_product($service->product_id);
                                                                      @endphp
                                                                          @if(!empty($product->group_product->name) && !empty($product->name))
                                                                          {{ $product->group_product->name }} - {{ $product->name }} <br>
                                                                      <span class="text-ip">{{$service->ip}} - {{ !empty($service->next_due_date) ? date('d/m/Y', strtotime($service->next_due_date) ) : '' }}</span>
                                                                      @endif
                                                                  </div>
                                                              </div>
                                                          </li>
                                                      </a>
                                                    @endif
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="text-center" style="margin: 10px;">
                                Có vẻ như bạn chưa có bất kỳ sản phẩm / dịch vụ nào với chúng tôi.<a href="">Đặt hàng để bắt đầu.</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            {{--  /.Services Product  --}}
            {{--  THông báo --}}
            <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 mb-4 scroll-info" id="home-notication">
                <div class="card card-outline card-info shadow">
                    <div class="card-header">
                        @if (!empty($notifications_reads))
                            <h4 class="info-header card-title"><i class="fas fa-newspaper"></i> Có {{ $notifications_reads->count() }} thông báo mới</h4>
                        @else
                            <h4 class="info-header card-title"><i class="fas fa-newspaper"></i> Thông báo mới</h4>
                        @endif
                    </div>
                    <div class="card-body">
                        @if ( $notifications_reads->count() > 0 )
                            <div class="box-body chat" id="scroll-notication">
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="all-inverted timeline">
                                            @foreach ($notifications_reads as $notifications_read)
                                                <a href="{{ route('user.notification.detail' , $notifications_read->notification->id) }}">
                                                    <li class="timeline-inverted">
                                                        <div class="timeline-panel">
                                                            <div class="timeline-title">
                                                                {{ $notifications_read->notification->name }} <br>
                                                                <span class="text-domain">Ngày tạo: {{date('d-m-Y', strtotime($notifications_read->created_at))}}</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </a>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @else
                          <div class="text-center" style="margin: 10px;">
                              Không có thông báo nào.
                          </div>
                        @endif
                    </div>
                </div>
            </div>
            {{--  /.Thoogn báo --}}
            {{--  Hướng dẫn --}}
            <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 mb-4 scroll-info" id="home-tutorials">
                <div class="card card-outline card-secondary shadow">
                    <div class="card-header">
                        <h4 class="info-header card-title"><i class="fas fa-bookmark"></i> Hướng dẫn</h4>
                    </div>
                    <div class="card-body">
                        @if ($tutorials->count() > 0)
                            <div class="box-body chat" id="scroll-tutorials">
                                <div class="row">
                                    <div class="col-md-12">
                                        <ul class="all-inverted timeline">
                                            @foreach ($tutorials as $tutorial)
                                                <a href="{{ $tutorial->link }}" target="_blank">
                                                    <li class="timeline-inverted">
                                                        <div class="timeline-panel">
                                                            <div class="timeline-title">
                                                                <span class="text-tutorials">{{ $tutorial->title }}</span>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </a>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            {{--  /.Hướng dẫn --}}

        </div>
    </div>
{{--    siderbar right   --}}
    <div class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-4">
        <div class="row">
            {{-- Thông tin cá nhân --}}
            <div class="col-xl-12 col-md-12">
                <div class="card card-success">
                    <div class="card-header">
                        <h3 class="card-title"><i class="fas fa-user-tie"></i> Thông tin cá nhân</h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                            </button>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body" style="display: block;">
                        <div class="info-user">
                            Họ và tên: {{ $user->name }} <br>
                            Email: {{ $user->email }} <br>
                            Đặc quyền: {{ !empty($user->group_user->name) ? $user->group_user->name : 'Cơ bản' }} <br>
                            Cloudzone Point: <b class="text-danger">{{ !empty($user->user_meta->point) ? $user->user_meta->point : '0' }} Điểm</b>  <br>
                            Số dư tài khoản:
                              <b class="text-danger">
                                  @if(!empty($user->credit->value))
                                      {!!number_format($user->credit->value,0,",",".") . ' VNĐ'!!}
                                  @else
                                      0 VNĐ
                                  @endif
                              </b> <br>
                            Tổng tiền đã chi:
                              <b class="text-danger">
                                  @if(!empty($total_price_use))
                                      {!!number_format($total_price_use,0,",",".") . ' VNĐ'!!}
                                  @else
                                      0 VNĐ
                                  @endif
                              </b> <br>
                            Tổng tiền đã nạp:
                              <b class="text-danger">
                                @if(!empty($user->credit->total))
                                    {!!number_format($user->credit->total,0,",",".") . ' VNĐ'!!}
                                @else
                                    0 VNĐ
                                @endif
                              </b> <br>
                        </div>
                        <div class="button-info text-center">
                            <a href="{{ route('user.pay_in_online') }}" class="btn btn-warning btn-sm"><i class="far fa-money-bill-alt"></i> Nạp tiền</a>
                            <a href="{{ route('user.history_payment') }}" class="btn btn-danger btn-sm">Tất cả giao dịch</a>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            {{-- /.Thông tin cá nhân --}}
            {{-- Thống kê dịch vụ --}}
            <div class="col-xl-12 col-md-12">
                <h3 class="title-services"><i class="fas fa-chart-pie"></i> Thống kê dịch vụ</h3>
                <div class="card card-primary">
                    <div class="card-header">
                        <div class="row">
                          <div class="col-sm-4 home-mobile-repon">
                              Dịch vụ
                          </div>
                          <div class="col-sm-4 home-mobile-repon">
                              Số lượng
                          </div>
                          <div class="col-4 text-right home-mobile-repon">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                                </button>
                            </div>
                          </div>
                        </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body" style="display: block;">
                        <div class=" home-service-chart">
                          <div class="row service_info">
                            <div class="col-sm-4 home-mobile-repon">
                                VPS
                            </div>
                            <div class="col-sm-4 home-mobile-repon">
                                {{ $total_vps }}
                            </div>
                            <div class="col-sm-4 text-right home-mobile-repon">
                                <a href="{{ route('service.vps.on') }}" class="btn btn-secondary btn-sm">Chi tiết</a>
                            </div>
                          </div>
                          <div class="row service_info">
                            <div class="col-sm-4 home-mobile-repon">
                                Hosting
                            </div>
                            <div class="col-sm-4 home-mobile-repon">
                                {{ $total_hosting }}
                            </div>
                            <div class="col-sm-4 text-right home-mobile-repon">
                                <a href="{{ route('service.hosting.on') }}" class="btn btn-secondary btn-sm">Chi tiết</a>
                            </div>
                          </div>
                          <div class="row service_info">
                            <div class="col-sm-4 home-mobile-repon">
                                Server
                            </div>
                            <div class="col-sm-4 home-mobile-repon">
                                {{ $total_server }}
                            </div>
                            <div class="col-sm-4 text-right home-mobile-repon">
                                <a href="javascript:void(0)" class="btn btn-secondary btn-sm">Chi tiết</a>
                            </div>
                          </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            {{-- /.Thống kê dịch vụ --}}
            {{-- Danh sách khách hàng --}}
            <div class="col-xl-12 col-md-12">
                <h3 class="title-services"><i class="fas fa-users"></i> Danh sách khách hàng</h3>
                <div class="card card-info">
                    <div class="card-header text-white">
                      <div class="row">
                        <div class="col-sm-3 home-mobile-repon-3">
                            MAKH
                        </div>
                        <div class="col-sm-7 home-mobile-repon-7">
                            Tên khách hàng
                        </div>
                        <div class="col-2 text-right home-mobile-repon-2">
                          <div class="card-tools">
                              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                              </button>
                          </div>
                        </div>
                      </div>
                        <!-- /.card-tools -->
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body" style="display: block;">
                      <div class="home-service-customer">

                      </div>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            {{-- /.Danh sách khách hàng --}}

        </div>
    </div>
</div>
{{--    ./siderbar right   --}}
@endsection
@section('scripts')
<!-- <script src="{{ url('libraries/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script> -->
<script src="{{ url('js/home.js') }}"></script>
<script>
    $(document).ready(function () {
        $('#scroll-product').slimscroll({
            // height: 'auto'
        });
        $('#scroll-ticket').slimscroll({
            // height: 'auto'
        });
        $('#scroll-notication').slimscroll({
            // height: 'auto'
        });
        $('#scroll-tutorials').slimscroll({
            // height: 'auto'
        });
    });
</script>
@endsection
