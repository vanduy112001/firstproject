@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
    <i class="fa fa-globe bg-c-blue"></i> Tên miền của tôi
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
    <li class="breadcrumb-item active"><i class="fas fa-globe"></i> My domain</li>
@endsection
@section('content')
    <div class="title">
        <div class="title-body">
            <div class="row">
                <div class="col-md-12 detail-course-finish">
                    @if(session("success"))
                        <div class="bg-success">
                            <p class="text-light">{{session("success")}}</p>
                        </div>
                    @elseif(session("fails"))
                        <div class="bg-danger">
                            <p class="text-light">{{session("fails")}}</p>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="box box-primary">
        <div class="box-body table-responsive">
            <div class="dataTables_wrapper form-inline dt-bootstrap">
                <table class="table table-bordered">
                    <thead class="primary">
                        <th>Tên domain</th>
                        <th>Vùng</th>
                        <th>Ngày đăng ký</th>
                        <th>Ngày kết thúc</th>
                        <th>Hành động</th>
                    </thead>
                    <tbody>
                        @foreach ($domains as $key => $domain)
                            @php
                                $isExpire = false;
                                $expired = false;
                                if(!empty($domain->next_due_date)){
                                    $next_due_date = strtotime(date('Y-m-d', strtotime($domain->next_due_date)));
                                    $date = date('Y-m-d');
                                    $date = strtotime(date('Y-m-d', strtotime($date . '+15 Days')));
                                    if($next_due_date <= $date) {
                                    $isExpire = true;
                                    }
                                    $date_now = strtotime(date('Y-m-d'));
                                    if ($next_due_date < $date_now) {
                                        $expired = true;
                                    }
                                }
                            @endphp
                            <tr>
                                <td><a href="#" {{ ($expired || ($domain->status === 'Pending')) ? '' : 'id='.$domain->id.'' }} class="btn btn-link info-domain {{ ($expired || ($domain->status === 'Pending')) ? 'disabled' : '' }}" data-toggle="modal" data-target="#info-domain" title="Thông tin domain">{{ $domain->domain }}</a></td>
                                <td>
                                {{ $domain->domain_product->local_type }}
                                @if ($expired)
                                    <span class="text-danger">- Hết hạn</span>
                                @elseif ($isExpire)
                                    <span class="text-danger">- Gần hết hạn</span>
                                @endif
                                </td>
                                <td>
                                    {{ date('d/m/Y', strtotime($domain->created_at)) }}
                                </td>
                                @if (!empty($domain->next_due_date))
                                    <td>{{ date('d/m/Y', strtotime($domain->next_due_date)) }}</td>
                                @else
                                    <td class="text-danger">Chưa đăng ký</td>
                                @endif
                                <td>
                                    @if ($domain->status == 'Active')
                                        <button type="button" class="btn btn-warning btn-sm expired-domain" data-toggle="modal" data-id="{{ $domain->id }}" data-target="#domain-{{ $domain->id }}" data-toggle="tooltip" title="Gia hạn">
                                            <i class="fas fa-plus-circle"></i> Gia hạn
                                        </button>
                                    @endif
                                    @if ($domain->status == 'Active' && !$expired)
                                        <button name="info-domain" id="{{ $domain->id }}" class="btn btn-info btn-sm info-domain" data-toggle="modal" data-target="#info-domain" title="Thông tin domain"><i class="fa fa-info-circle"></i> Chi tiết</button>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot class="card-footer clearfix">
                        <td colspan="5" class="text-center">
                            {{ $domains->links()  }}
                        </td>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    {{-- Modal gia hạn --}}
    <div class="modal fade modal-exp" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Yêu cầu gia hạn domain</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div id="notication-invoice" class="text-center">
                    <form action="{{ route('user.domain.order_domain_expired') }}" method="post">
                        <div class="form-group">
                                <label for="billing">Chọn thời gian gia hạn</label>
                                <div class="mt-3 mb-3">
                                    <select name="billing" class="form-control select2 time" id="billing" style="width: 80%">
                                    </select>
                                </div>

                                <div class="card">
                                    <div class="card-header text-center">
                                        <h3 class="card-title m-0">Bảng giá gia hạn</h3>
                                    </div>
                                    <div class="loading"></div>
                                    <div class="card-body p-0 show-price">
                                        <table class="table table-sm">
                                            <thead><tr class="add-domain"></tr></thead>
                                            <tbody class="add-billing"></tbody>
                                        </table>
                                    </div>
                                </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                @csrf
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
              <input type="hidden" name="domain" value="">
              <input type="submit" class="btn btn-block btn-primary" style="width:auto" value="Xác nhận">
            </div>
        </form>
          </div>
        </div>
      </div>
{{-- Modal thông tin domain --}}
<!-- Modal -->
<div class="modal fade" id="info-domain" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title show-domain" id="exampleModalLongTitle"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body loading" style="margin:auto">

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary info-domain-cancel" data-dismiss="modal">Đóng</button>
        </div>
      </div>
    </div>
  </div>

@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.select2').select2();
        $('[data-toggle="tooltip"]').tooltip();
    });
    $(document).ready(function() {
        //show thông tin domain
        $('.info-domain').on('click', function(){
            var id = $(this).attr('id');
            var show = '';
            var title = '';
            $.ajax({
                url: '/domain/info',
                data: {'id': id},
                dataType: 'json' ,
                type: 'get',
                beforeSend: function(){
                    var html = '<div class="text-center; margin: auto"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('.loading').html(html);
                },
                success: function(data){
                    title = 'Thông tin quản lý tên miền <a href="http://'+ data.domain +'" target="_blank">'+ data.domain +'</a></span><br>';
                    show += '- Tên miền đăng ký: <a href="http://'+ data.domain +'" target="_blank">'+ data.domain +'</a></span><br>';
                    show += '- Ngày hết hạn: '+data.next_due_date;
                    show += '<br>';
                    show += '- Quý khách đăng nhập với thông tin bên dưới để cấu hình Tên miền:<br>';
                    show += '<div style="margin-top: 10px; padding: 10px; border: solid 1px; border-radius: 5px;">';
                    show += '<a href="http://dotvn.cloudzone.vn" target="_blank">http://dotvn.cloudzone.vn</a><br>';
                    show += '+ Tên miền: '+data.domain+'<br>';
                    show += '+ Mật khẩu: <span class="show-pass">'+data.password_domain+'</span><button class="btn btn-info badge badge-pill ml-1 show-form" data-toggle="collapse" data-target="#form-change-pass">Đổi mật khẩu</button><br>';
                    show += '<div class="show-form-change-pass-domain collapse" id="form-change-pass">';
                    show += '<form id="form-change-pass-domain" class="form-inline input-group-sm mt-2" method="POST" action="{{ route('user.domain.change_pass_domain') }}">';
                    show += '@csrf';
                    show += '<div class="form-group input-group-sm mb-0">';
                    show += '<input type="text" name="password_domain" class="form-control password-domain" placeholder="Nhập mật khẩu mới" autocomplete="off" required>';
                    show += '<input type="hidden" name="domain-id" value="'+data.id+'" class="form-control password-domain">';
                    show += '</div>';
                    show += '<input type="submit" class="btn btn-primary btn-sm ml-2" value="Xác nhận">';
                    show += '</form>';
                    show += '<div class="status">';
                    show += '</div>';
                    show += '</div>';
                    show += '</div>';
                    $('.show-domain').html(title);
                    $('.loading').html(show);
                },
                error: function(e) {
                    $('.modal-body').html('Truy vấn lỗi');
                },
            });
        });
        $('#info-domain').delegate('#form-change-pass-domain', 'submit', function(e) {
            e.preventDefault();
            var form = $(this);
            var url = form.attr('action');
            var html = '';
            $.ajax({
                url: url,
                type: 'POST',
                dataType: 'JSON',
                data: form.serialize(),
                beforeSend: function() {
                    $('.status').html('<div class="spinner-border spinner-border-sm text-info"></div>');
                },
                success: function(data) {
                    if(data.status == true) {
                        $('.status').html('<span class="text-success">Đổi mật khẩu thành công</span>');
                        $('.show-pass').text(data.password);
                    } else {
                        console.log(data);
                        if(data.message.password_domain == null) {
                            $('.status').html('<span class="text-danger">'+data.message+'</span>');
                        } else {
                            $('.status').html('<span class="text-danger">'+data.message.password_domain+'</span>');
                        }
                    }
                },
                error: function(e) {
                    $('.status').html('<span class="text-danger">Truy vấn lỗi</span>');
                }
            });
        });

        //show modal bảng giá và xác nhận gia hạn
        $('.expired-domain').on('click', function(){
            var get_id = $(this).attr('data-id');
            var class_domain_id = 'domain-'+get_id;
            $('.modal-exp').attr('id', class_domain_id);
            $.ajax({
                type: 'get',
                url: '/domain/domain-product-expired-price',
                data: {id: get_id},
                dataType: 'json',
                beforeSend: function(){
                    var html = '<div class="text-center"><div class="vong-xoay"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>';
                    $('.loading').html(html);
                    $('.show-price').hide();
                },
                success: function(data) {
                    var html = '';
                    var html_billing = '';
                    var html_time = '<option value="" disabled>Chọn thời gian</option>';
                    html += '<th>domain</th>';
                    html += '<th class="text-danger">'+data.domain.domain+'</th>';
                    $.each(data.billings, function(index, value){
                        if(data.domain_product[index] != null) {
                            html_billing += '<tr>';
                            html_billing += '<td>'+ value +' Năm</td>';
                            html_billing += '<td>'+ addCommas(data.domain_product[index]) +' VNĐ</td>';
                            html_billing += '</tr>';
                            html_time +='<option value="'+index+'" data-year="'+value+'" data-domain="'+data.domain.domain+'">'+ value +' Năm</option>';
                        }
                    });
                    $('input[name="domain"]').val(data.domain.domain);
                    $('.add-billing').html(html_billing);
                    $('.add-domain').html(html);
                    $('.time').html(html_time);
                    $('.show-price').show();
                    $('.loading').html('');
                },
                error: function(e) {
                    $('.loading').css('opacity', '1');
                    $('.loading').html('Không hiển thị được giá sản phẩm');
                    $('.modal-exp .modal-footer [type=submit]').remove();
                },

            });
        });
    });
    function addCommas(nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
        }
        return x1 + x2;
    }
</script>

@endsection
