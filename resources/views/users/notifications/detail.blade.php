@extends('layouts.user2.app')
@section('title')
<div class="text-primary">Thông báo</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-bell" aria-hidden="true"></i> Thông báo</li>
@endsection
@section('content')
<div class="row">
    <div class="title col-md-12">
        <div class="title-body">
            <div class="row">
                <div class="col-md-12 detail-course-finish">
                    @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                    @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                    @endif
                    @if ($errors->any())
                    <div class="alert alert-danger" style="margin-top:20px;width:100%;">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <div class="card border-bottom-info border-header-info shadow" style="margin-top: 20px">
            <div class="card-header">
                <h3 class="card-title" style="margin: 0">{{ $notification_detail->name }}</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive">
                <div style="color: #999;font-size: 13px;">
                    {{ date_format($notification_detail->created_at, 'H:i d-m-Y') }}
                </div>
                <hr>
                <div class="content-noti">
                    {!! $notification_detail->content !!}
                </div>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
                <div>
                <a href="{{ route('user.notification.index') }}" class="btn btn-default">Tất cả thông báo</a>
                </div>
            </div>
            <!-- /.card-footer -->
        </div>
    </div>
</div>
@endsection

@section('scripts')
@endsection