@extends('layouts.user2.app')
@section('title')
<div class="text-primary">Thông báo</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fa fa-bell" aria-hidden="true"></i> Thông báo</li>
@endsection
@section('content')
<div class="row">
    <div class="title col-md-12">
        <div class="title-body">
            <div class="row">
                <div class="col-md-12 detail-course-finish">
                    @if(session("success"))
                    <div class="bg-success">
                        <p class="text-light">{{session("success")}}</p>
                    </div>
                    @elseif(session("fails"))
                    <div class="bg-danger">
                        <p class="text-light">{{session("fails")}}</p>
                    </div>
                    @endif
                    @if ($errors->any())
                    <div class="alert alert-danger" style="margin-top:20px;width:100%;">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <button class="btn btn-success btn-action btn-icon-split btn-sm" data-type="on">
            <span class="icon"><i class="fas fa-check-circle"></i></span>
            <span class="text"> Đánh dấu đã xem</span>
        </button>
    </div>
    <div class="col-12">
        <div class="card border-bottom-info border-header-info shadow" style="margin-top: 20px">
            <div class="card-header">
                {{-- <h3 class="card-title">Bảng danh thông báo</h3> --}}
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th width="10%" class="text-center"><input type="checkbox"  class="checkbox_all"></th>
                            <th>Thời gian</th>
                            <th>Thông báo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if ($notifications->count() > 0)
                            @foreach($notifications as $key => $notification)
                                    <tr>
                                        <td align="center"><input type="checkbox" value="{{ $notification->notification->id }}" class="checkbox"></td>
                                        <td>
                                            <a href="{{ route('user.notification.detail', $notification->notification->id) }}" @if ($notification->read_at == null) class="unread" data-id="{{ $notification->id }}" @endif >{{ date_format($notification->created_at, 'H:i d-m-Y') }}</a>
                                        </td>
                                        <td>
                                            <a href="{{ route('user.notification.detail', $notification->notification->id) }}"
                                                @if ($notification->read_at == null) class="unread" data-id="{{ $notification->id }}" @endif>
                                                {{ $notification->notification->name }}
                                                @if ($notification->read_at == null)<span class="right badge badge-danger notifi-new">Mới</span>@endif
                                            </a>
                                        </td>
                                    </tr>
                            @endforeach
                        @else
                            <td colspan="3" class="text-center text-danger">
                                Không có thông báo
                            </td>
                        @endif
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
            <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                        {{ $notifications->links() }}
                    </div>
                </ul>
            </div>
            <!-- /.card-footer -->
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        var $vpsCheckbox = $('.checkbox');
        var lastChecked = null;

        $(document).on('click', '.checkbox_all', function () {
            if ( $(this).is(':checked') ) {
              $('.checkbox').prop('checked', this.checked);
              $('tbody tr').addClass('action');
            } else {
              $('.checkbox').prop('checked', this.checked);
              $('tbody tr').removeClass('action');
            }
        })

        $(document).on('click', '.checkbox' , function(e) {
            if( $(this).is(':checked') ) {
                $(this).closest('tr').addClass('action');
            } else {
                $(this).closest('tr').removeClass('action');
            }
            if (!lastChecked) {
                lastChecked = this;
                return;
            }
            if ( e.shiftKey ) {
                // console.log(lastChecked, this);
                var start = $vpsCheckbox.index(this);
                var end = $vpsCheckbox.index(lastChecked);
                $vpsCheckbox.slice(Math.min(start,end), Math.max(start,end)+ 1).prop('checked', lastChecked.checked);
                $.each( $vpsCheckbox , function (index , value) {
                    if( $(this).is(':checked') ) {
                        $(this).closest('tr').addClass('action');
                    } else {
                        $(this).closest('tr').removeClass('action');
                    }
                })
            }
            lastChecked = this;
        });

        $('.btn-action').on('click', function() {
            // $('.action .notifi-new').fadeOut();
            var checked = $('.checkbox:checked');
            if ( checked.length ) {
                var list_id = [];
                $.each(checked, function (index, value) {
                    list_id.push($(this).val());
                });
                $.ajax({
                    url: '/notifications/ajax_read',
                    type: 'get',
                    data: {list_id: list_id},
                    success: function (data) {
                        // console.log(data);
                        if (data) {
                            // console.log("da den");
                            $('.action .notifi-new').fadeOut();
                            $(this).closest('tr').removeClass('action');
                        }
                    }
                })
            }
        });


    });
</script>
@endsection
