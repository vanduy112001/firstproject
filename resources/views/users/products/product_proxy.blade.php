@extends('layouts.user2.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/SimpleDropDownEffects/css/style6.css') }}">
    <script src="{{ asset('libraries/SimpleDropDownEffects/js/modernizr.custom.63321.js') }}"></script>
@endsection
@section('title')
<div class="text-primary">Dịch vụ {{ $group_product->title }}</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-cloud"></i> Dịch vụ</li>
<li class="breadcrumb-item active"> {{ $group_product->title }}</li>
@endsection
@section('content')
<div class="row">
    {{-- <div class="col-md-12">
        <div class="header-lined">
            <h1>{{ $group_product->title }}</h1>
        </div>
    </div> --}}
    <div class="col col-md-12">
        <div class="show-product">
            <div class="row">
                @if (!empty($products))
                    @foreach ($products as $key => $product)
                        <div class="col-xl-4 col-md-6 mb-4 card-deck">
                            <div class="card border-bottom-info border-header-info shadow h-100">
                                <div class="card-header py-3 text-center">
                                    <h5 class="m-0 font-weight-bold text-primary">{{ $product->name }}</h5>
                                </div>
                                <div class="card-body text-center pt-1">
                                    <div class="pricing-product">
                                      @if (!empty($product->pricing->one_time_pay))
                                        <div class="card-drop">
                                          <a class='toggle' href="#">
                                            <span class='label-active'>
                                              {{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}} / Vĩnh viễn
                                            </span>
                                            <input type="hidden" name="" class="billing_cycle_product" value="one_time">
                                          </a>
                                          <ul>
                                            <li class='active'>
                                              <a data-label="{{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}} / Vĩnh viễn" data-product="{{ $product->id }}" data-billing="one_time_pay" href="#">

                                                {{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}} <small class="text-muted">/ Vĩnh viễn</small>
                                              </a>
                                            </li>
                                          </ul>
                                        </div>
                                      @elseif (!empty($product->pricing->type) && $product->pricing->type == 'free')
                                        <div class="card-drop">
                                          <a class='toggle' href="#">
                                            <span class='label-active'>
                                              Free
                                            </span>
                                            <input type="hidden" name="" class="billing_cycle_product" value="free">
                                          </a>
                                          <ul>
                                            <li class='active'>
                                              <a data-label="Free"  data-billing="free" href="#">
                                                Free
                                              </a>
                                            </li>
                                          </ul>
                                        </div>
                                      @else
                                        <div class="card-drop">
                                            @if(!empty($product->pricing->monthly))
                                              <a class='toggle' href="#">
                                                <span class='label-active'>
                                                  {{number_format($product->pricing->monthly,0,",",".") . ' VNĐ'}} / 1 Tháng
                                                </span>
                                                <input type="hidden" name="" class="billing_cycle_product" value="monthly">
                                              </a>
                                            @elseif (!empty($product->pricing->twomonthly))
                                              <a class='toggle' href="#">
                                                <span class='label-active'>
                                                  {{number_format($product->pricing->twomonthly,0,",",".") . ' VNĐ'}} / 2 Tháng
                                                </span>
                                                <input type="hidden" name="" class="billing_cycle_product" value="twomonthly">
                                              </a>
                                            @elseif (!empty($product->pricing->quarterly))
                                              <a class='toggle' href="#">
                                                <span class='label-active'>
                                                  {{number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'}} / 3 Tháng
                                                </span>
                                                <input type="hidden" name="" class="billing_cycle_product" value="quarterly">
                                              </a>
                                            @elseif (!empty($product->pricing->semi_annually))
                                              <a class='toggle' href="#">
                                                <span class='label-active'>
                                                  {{number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'}} / 6 Tháng
                                                </span>
                                                <input type="hidden" name="" class="billing_cycle_product" value="semi_annually">
                                              </a>
                                            @elseif (!empty($product->pricing->annually))
                                              <a class='toggle' href="#">
                                                <span class='label-active'>
                                                  {{number_format($product->pricing->annually,0,",",".") . ' VNĐ'}} / 1 Năm
                                                </span>
                                                <input type="hidden" name="" class="billing_cycle_product" value="annually">
                                              </a>
                                            @elseif (!empty($product->pricing->biennially))
                                              <a class='toggle' href="#">
                                                <span class='label-active'>
                                                  {{number_format($product->pricing->biennially,0,",",".") . ' VNĐ'}} / 2 Năm
                                                </span>
                                                <input type="hidden" name="" class="billing_cycle_product" value="biennially">
                                              </a>
                                            @elseif (!empty($product->pricing->triennially))
                                              <a class='toggle' href="#">
                                                <span class='label-active'>
                                                  {{number_format($product->pricing->triennially,0,",",".") . ' VNĐ'}} / 3 Năm
                                                </span>
                                                <input type="hidden" name="" class="billing_cycle_product" value="triennially">
                                              </a>
                                            @endif
                                            <ul>
                                              @if(!empty($product->pricing->monthly))
                                                <li class='active'>
                                                  <a data-label="{{number_format($product->pricing->monthly,0,",",".") . ' VNĐ'}} / 1 Tháng" data-billing="monthly" data-product="{{ $product->id }}" href="#">
                                                    {{number_format($product->pricing->monthly,0,",",".") . ' VNĐ'}}
                                                    <small class="text-muted">/ 1 Tháng</small>
                                                  </a>
                                                </li>
                                              @endif
                                              @if(!empty($product->pricing->twomonthly))
                                                @php
                                                  $active = '';
                                                  if ( empty($product->pricing->monthly) ) {
                                                    $active = 'active';
                                                  }
                                                @endphp
                                                <li class="{{ $active }}">
                                                  <a data-label="{{number_format($product->pricing->twomonthly,0,",",".") . ' VNĐ'}} / 2 Tháng" data-billing="twomonthly" data-product="{{ $product->id }}" href="#">

                                                    {{number_format($product->pricing->twomonthly,0,",",".") . ' VNĐ'}}
                                                    <small class="text-muted">/ 2 Tháng</small>
                                                  </a>
                                                </li>
                                              @endif
                                              @if (!empty($product->pricing->quarterly))
                                                @php
                                                  $active = '';
                                                  if ( empty($product->pricing->monthly) && empty($product->pricing->twomonthly) ) {
                                                    $active = 'active';
                                                  }
                                                @endphp
                                                <li class="{{ $active }}">
                                                  <a data-label="{{number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'}} / 3 Tháng" data-billing="quarterly" data-product="{{ $product->id }}"  href="#">

                                                    {{number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'}}
                                                    <small class="text-muted">/ 3 Tháng</small>
                                                  </a>
                                                </li>
                                              @endif
                                              @if (!empty($product->pricing->semi_annually))
                                                @php
                                                  $active = '';
                                                  if ( empty($product->pricing->monthly) && empty($product->pricing->twomonthly) && empty($product->pricing->quarterly) ) {
                                                    $active = 'active';
                                                  }
                                                @endphp
                                                <li class="{{ $active }}">
                                                  <a data-label="{{number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'}} / 6 Tháng" data-billing="semi_annually" data-product="{{ $product->id }}" href="#">

                                                    {{number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'}}
                                                    <small class="text-muted">/ 6 Tháng</small>
                                                  </a>
                                                </li>
                                              @endif
                                              @if (!empty($product->pricing->annually))
                                                @php
                                                  $active = '';
                                                  if ( empty($product->pricing->monthly) && empty($product->pricing->twomonthly) && empty($product->pricing->quarterly)
                                                    && empty($product->pricing->semi_annually) ) {
                                                    $active = 'active';
                                                  }
                                                @endphp
                                                <li class="{{ $active }}">
                                                  <a data-label="{{number_format($product->pricing->annually,0,",",".") . ' VNĐ'}} / 1 Năm" data-billing="annually" data-product="{{ $product->id }}" href="#">

                                                    {{number_format($product->pricing->annually,0,",",".") . ' VNĐ'}}
                                                    <small class="text-muted">/ 1 Năm</small>
                                                  </a>
                                                </li>
                                              @endif
                                              @if (!empty($product->pricing->biennially))
                                                @php
                                                  $active = '';
                                                  if ( empty($product->pricing->monthly) && empty($product->pricing->twomonthly) && empty($product->pricing->quarterly)
                                                    && empty($product->pricing->semi_annually) && empty($product->pricing->annually) ) {
                                                    $active = 'active';
                                                  }
                                                @endphp
                                                <li class="{{ $active }}">
                                                  <a data-label="{{number_format($product->pricing->biennially,0,",",".") . ' VNĐ'}} / 2 Năm" data-billing="biennially" data-product="{{ $product->id }}" href="#">

                                                    {{number_format($product->pricing->biennially,0,",",".") . ' VNĐ'}}
                                                    <small class="text-muted">/ 2 Năm</small>
                                                  </a>
                                                </li>
                                              @endif
                                              @if (!empty($product->pricing->triennially))
                                                @php
                                                  $active = '';
                                                  if ( empty($product->pricing->monthly) && empty($product->pricing->twomonthly) && empty($product->pricing->quarterly)
                                                    && empty($product->pricing->semi_annually) && empty($product->pricing->annually) && empty($product->pricing->biennially) ) {
                                                    $active = 'active';
                                                  }
                                                @endphp
                                                <li class="{{ $active }}">
                                                  <a data-label="{{number_format($product->pricing->triennially,0,",",".") . ' VNĐ'}} / 3 Năm" data-billing="triennially" data-product="{{ $product->id }}" href="#">

                                                    {{number_format($product->pricing->triennially,0,",",".") . ' VNĐ'}}
                                                    <small class="text-muted">/ 3 Năm</small>
                                                  </a>
                                                </li>
                                              @endif
                                            </ul>
                                        </div>
                                      @endif
                                    </div>
                                    <div class="singlePrice">
                                        <ul class="list-unstyled ">
                                            <li>IP: {{ !empty($product->meta_product->ip) ? $product->meta_product->ip : '0'  }} IP public</li>
                                            <li>Băng thông: {{ !empty($product->meta_product->bandwidth) ? $product->meta_product->bandwidth : '0'  }}</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-footer text-center">
                                    <a href="{{ route('user.addCard', $product->id) }}" class="btn btn-primary btn-icon-split btn_add_card">
                                        <span class="icon text-white-50"><i class="fas fa-shopping-cart"></i></span>
                                        <span class="text">Đăng ký ngay</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/SimpleDropDownEffects/js/jquery.dropdown.js') }}"></script>
<script src="{{ asset('js/user_choose_product.js') }}"></script>
@endsection
