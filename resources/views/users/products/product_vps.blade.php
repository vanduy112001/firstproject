@extends('layouts.user2.app')
@section('style')
    <link rel="stylesheet" href="{{ asset('libraries/SimpleDropDownEffects/css/style6.css') }}">
    <script src="{{ asset('libraries/SimpleDropDownEffects/js/modernizr.custom.63321.js') }}"></script>
@endsection
@section('title')
<div class="text-primary">Dịch vụ {{ $group_product->title }}</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-cloud"></i> Dịch vụ VPS</li>
<li class="breadcrumb-item active"> {{ $group_product->title }}</li>
@endsection
@section('content')
<div class="row">
    {{-- <div class="col-md-12">
        <div class="header-lined">
            <h1>{{ $group_product->title }}</h1>
        </div>
    </div> --}}
    <div class="col col-md-12">
        <div class="show-product">
            <div class="row">
                @if (!empty($products))
                    @foreach ($products as $key => $product)
                        <div class="col-xl-3 col-lg-4 col-md-6 mb-4 card-deck">
                            <div class="card border-bottom-info border-header-info shadow h-100">
                                <div class="card-header py-3 text-center">
                                    <h5 class="m-0 font-weight-bold text-primary">{{ $product->name }}</h5>
                                </div>
                                <div class="card-body text-center p-1">
                                    @if ($product->type_product == 'VPS' || $product->type_product == 'NAT-VPS' || $product->type_product == 'VPS-US')
                                        <div class="pricing-product">
                                            @if (!empty($product->pricing->one_time_pay))
                                                <div class="card-drop">
                                                    <a class='toggle' href="#">
                                                        <span class='label-active'>
                                                        {{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}} / Vĩnh viễn
                                                        </span>
                                                        <input type="hidden" name="" class="billing_cycle_product" value="one_time">
                                                    </a>
                                                    <ul>
                                                        <li class='active'>
                                                            <a data-label="{{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}} / Vĩnh viễn" data-product="{{ $product->id }}" data-billing="one_time_pay" href="#">

                                                                {{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}} <small class="text-muted">/ Vĩnh viễn</small>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            @elseif (!empty($product->pricing->type) && $product->pricing->type == 'free')
                                                <div class="card-drop">
                                                    <a class='toggle' href="#">
                                                        <span class='label-active'>
                                                        Free
                                                        </span>
                                                        <input type="hidden" name="" class="billing_cycle_product" value="free">
                                                    </a>
                                                    <ul>
                                                        <li class='active'>
                                                            <a data-label="Free"  data-billing="free" href="#">
                                                                Free
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            @else
                                                <div class="card-drop">
                                                    <a class='toggle' href="#">
                                                        <span class='label-active'>
                                                            {{number_format($product->pricing->monthly,0,",",".") . ' VNĐ'}} / 1 Tháng
                                                        </span>
                                                        <input type="hidden" name="" class="billing_cycle_product" value="monthly">
                                                    </a>
                                                    <ul>
                                                        @if(!empty($product->pricing->monthly))
                                                            <li class='active'>
                                                                <a data-label="{{number_format($product->pricing->monthly,0,",",".") . ' VNĐ'}} / 1 Tháng" data-billing="monthly" data-product="{{ $product->id }}" href="#">
                                                                {{number_format($product->pricing->monthly,0,",",".") . ' VNĐ'}}
                                                                <small class="text-muted">/ 1 Tháng</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if(!empty($product->pricing->twomonthly))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->twomonthly,0,",",".") . ' VNĐ'}} / 2 Tháng" data-billing="twomonthly" data-product="{{ $product->id }}" href="#">

                                                                {{number_format($product->pricing->twomonthly,0,",",".") . ' VNĐ'}}
                                                                <small class="text-muted">/ 2 Tháng</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->quarterly))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'}} / 3 Tháng" data-billing="quarterly" data-product="{{ $product->id }}"  href="#">

                                                                {{number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'}}
                                                                <small class="text-muted">/ 3 Tháng</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->semi_annually))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'}} / 6 Tháng" data-billing="semi_annually" data-product="{{ $product->id }}" href="#">

                                                                {{number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'}}
                                                                <small class="text-muted">/ 6 Tháng</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->annually))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->annually,0,",",".") . ' VNĐ'}} / 1 Năm" data-billing="annually" data-product="{{ $product->id }}" href="#">

                                                                {{number_format($product->pricing->annually,0,",",".") . ' VNĐ'}}
                                                                <small class="text-muted">/ 1 Năm</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->biennially))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->biennially,0,",",".") . ' VNĐ'}} / 2 Năm" data-billing="biennially" data-product="{{ $product->id }}" href="#">

                                                                {{number_format($product->pricing->biennially,0,",",".") . ' VNĐ'}}
                                                                <small class="text-muted">/ 2 Năm</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->triennially))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->triennially,0,",",".") . ' VNĐ'}} / 3 Năm" data-billing="triennially" data-product="{{ $product->id }}" href="#">

                                                                {{number_format($product->pricing->triennially,0,",",".") . ' VNĐ'}}
                                                                <small class="text-muted">/ 3 Năm</small>
                                                                </a>
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="singlePrice">
                                            <ul class="list-unstyled ">
                                                <li>CPU: {{ !empty($product->meta_product->cpu) ? $product->meta_product->cpu : '0'  }} core</li>
                                                <li>Memory: {{ !empty($product->meta_product->memory) ?  $product->meta_product->memory : '0'  }} GB</li>
                                                <li>Disk SSD: {{ !empty($product->meta_product->disk) ? $product->meta_product->disk : '0'  }} GB</li>
                                                <li>IP: {{ !empty($product->meta_product->ip) ? $product->meta_product->ip : '0'  }} IP public</li>

                                                @if ($product->type_product == 'VPS' || $product->type_product == 'NAT-VPS')
                                                  <li>Băng thông: {{ !empty($product->meta_product->bandwidth) ? $product->meta_product->bandwidth : '0'  }}</li>
                                                  <li>Hệ điều hành: {{ !empty($product->meta_product->os) ? $product->meta_product->os : '0'  }}</li>

                                                @elseif ($product->type_product == 'VPS-US')
                                                  <li>Hệ điều hành: Windows Server 2012 R2</li>
                                                @endif

                                                @if(!empty($product->meta_product->girf))
                                                  <li class="text-danger">{{ $product->meta_product->girf }}</li>
                                                @endif

                                            </ul>
                                        </div>
                                    @elseif ($product->type_product == 'Server')
                                        <div class="pricing-product">

                                            @if (!empty($product->pricing->one_time_pay))
                                                <div class="card-drop">
                                                    <a class='toggle' href="#">
                                                        <span class='label-active'>
                                                        {{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}} / Vĩnh viễn
                                                        </span>
                                                        <input type="hidden" name="" class="billing_cycle_product" value="one_time">
                                                    </a>
                                                    <ul>
                                                        <li class='active'>
                                                            <a data-label="{{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}} / Vĩnh viễn" data-product="{{ $product->id }}" data-billing="one_time_pay" href="#">

                                                                {{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}} <small class="text-muted">/ Vĩnh viễn</small>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            @elseif (!empty($product->pricing->type) && $product->pricing->type == 'free')
                                                <div class="card-drop">
                                                    <a class='toggle' href="#">
                                                        <span class='label-active'>
                                                        Free
                                                        </span>
                                                        <input type="hidden" name="" class="billing_cycle_product" value="free">
                                                    </a>
                                                    <ul>
                                                        <li class='active'>
                                                            <a data-label="Free"  data-billing="free" href="#">
                                                                Free
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            @else
                                                <div class="card-drop">
                                                    <a class='toggle' href="#">
                                                        <span class='label-active'>{{number_format($product->pricing->monthly,0,",",".") . ' VNĐ'}} / 1 Tháng </span>
                                                        <input type="hidden" name="" class="billing_cycle_product" value="monthly">
                                                    </a>
                                                    <ul>
                                                        @if(!empty($product->pricing->monthly))
                                                            <li class='active'>
                                                                <a data-label="{{number_format($product->pricing->monthly,0,",",".") . ' VNĐ'}} / 1 Tháng" data-billing="monthly" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->monthly,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 1 Tháng</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if(!empty($product->pricing->twomonthly))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->twomonthly,0,",",".") . ' VNĐ'}} / 2 Tháng" data-billing="twomonthly" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->twomonthly,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 2 Tháng</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->quarterly))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'}} / 3 Tháng" data-billing="quarterly" data-product="{{ $product->id }}"  href="#">
                                                                    {{number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 3 Tháng</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->semi_annually))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'}} / 6 Tháng" data-billing="semi_annually" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 6 Tháng</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->annually))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->annually,0,",",".") . ' VNĐ'}} / 1 Năm" data-billing="annually" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->annually,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 1 Năm</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->biennially))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->biennially,0,",",".") . ' VNĐ'}} / 2 Năm" data-billing="biennially" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->biennially,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 2 Năm</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->triennially))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->triennially,0,",",".") . ' VNĐ'}} / 3 Năm" data-billing="triennially" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->triennially,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 3 Năm</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="singlePrice">
                                            <ul class="list-unstyled ">
                                                <li>Máy chủ: {{ !empty($product->meta_product->name_server ) ? $product->meta_product->name_server  : '' }}</li>
                                                <li>CPU: {{ !empty($product->meta_product->cpu) ? $product->meta_product->cpu : '0'  }} core</li>
                                                <li>Memory: {{ !empty($product->meta_product->memory) ? $product->meta_product->memory : '0'  }} GB</li>
                                                <li>Disk SSD: {{ !empty($product->meta_product->disk) ? $product->meta_product->disk : '0'  }} GB</li>
                                                <li>Ip: {{ !empty($product->meta_product->ip) ? $product->meta_product->ip : '0'  }} IP public</li>
                                                <li>Băng thông: {{ !empty($product->meta_product->bandwidth) ? $product->meta_product->bandwidth : '0'  }}</li>
                                                @if(!empty($product->meta_product->girf))
                                                  <li class="text-danger">{{ $product->meta_product->girf }}</li>
                                                @endif
                                            </ul>
                                        </div>
                                    @elseif ( $product->type_product == 'Addon-VPS' || $product->type_product == 'Addon-Server' )
                                        <div class="pricing-product">

                                            @if (!empty($product->pricing->one_time_pay))
                                                <div class="card-drop">
                                                    <a class='toggle' href="#">
                                                        <span class='label-active'>{{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}} / Vĩnh viễn </span>
                                                        <input type="hidden" name="" class="billing_cycle_product" value="one_time">
                                                    </a>
                                                    <ul>
                                                        <li class='active'>
                                                            <a data-label="{{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}} / Vĩnh viễn" data-product="{{ $product->id }}" data-billing="one_time_pay" href="#">
                                                                {{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}} <small class="text-muted">/ Vĩnh viễn</small>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            @elseif (!empty($product->pricing->type) && $product->pricing->type == 'free')
                                                <div class="card-drop">
                                                    <a class='toggle' href="#">
                                                        <span class='label-active'>Free</span>
                                                        <input type="hidden" name="" class="billing_cycle_product" value="free">
                                                    </a>
                                                    <ul>
                                                        <li class='active'>
                                                            <a data-label="Free"  data-billing="free" href="#">Free</a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            @else
                                                <div class="card-drop">
                                                    <a class='toggle' href="#">
                                                        <span class='label-active'>{{number_format($product->pricing->monthly,0,",",".") . ' VNĐ'}} / 1 Tháng</span>
                                                        <input type="hidden" name="" class="billing_cycle_product" value="monthly">
                                                    </a>
                                                    <ul>
                                                        @if(!empty($product->pricing->monthly))
                                                            <li class='active'>
                                                                <a data-label="{{number_format($product->pricing->monthly,0,",",".") . ' VNĐ'}} / 1 Tháng" data-billing="monthly" data-product="{{ $product->id }}" href="#">
                                                                {{number_format($product->pricing->monthly,0,",",".") . ' VNĐ'}}
                                                                <small class="text-muted">/ 1 Tháng</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if(!empty($product->pricing->twomonthly))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->twomonthly,0,",",".") . ' VNĐ'}} / 2 Tháng" data-billing="twomonthly" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->twomonthly,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 2 Tháng</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->quarterly))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'}} / 3 Tháng" data-billing="quarterly" data-product="{{ $product->id }}"  href="#">
                                                                    {{number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 3 Tháng</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->semi_annually))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'}} / 6 Tháng" data-billing="semi_annually" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 6 Tháng</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->annually))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->annually,0,",",".") . ' VNĐ'}} / 1 Năm" data-billing="annually" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->annually,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 1 Năm</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->biennially))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->biennially,0,",",".") . ' VNĐ'}} / 2 Năm" data-billing="biennially" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->biennially,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 2 Năm</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->triennially))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->triennially,0,",",".") . ' VNĐ'}} / 3 Năm" data-billing="triennially" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->triennially,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 3 Năm</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                    </ul>
                                                </div>
                                            @endif
                                        </div>
                                        <div class="singlePrice">
                                            <ul class="list-unstyled ">
                                                <li>CPU: {{ !empty($product->meta_product->cpu) ? $product->meta_product->cpu : '0'  }} core</li>
                                                <li>Memory: {{ !empty($product->meta_product->memory) ?  $product->meta_product->memory : '0'  }} GB</li>
                                                <li>Disk SSD: {{ !empty($product->meta_product->disk) ? $product->meta_product->disk : '0'  }} GB</li>
                                                <li>Ip: {{ !empty($product->meta_product->ip) ? $product->meta_product->ip : '0'  }} IP public</li>
                                                <li>Hệ điều hành: {{ !empty($product->meta_product->os) ? $product->meta_product->os : '0'  }}</li>
                                                <li>Băng thông: {{ !empty($product->meta_product->bandwidth) ? $product->meta_product->bandwidth : '0'  }}</li>
                                                @if(!empty($product->meta_product->girf))
                                                    <li class="text-danger">{{ $product->meta_product->girf }}</li>
                                                @endif
                                            </ul>
                                        </div>

                                    @elseif ( $product->type_product == 'Addon-Hosting')
                                        <div class="pricing-product">
                                            <select class="cd-select cd-dropdown{{$key}}">
                                                <option value="one_time" selected>
                                                    @if (!empty($product->pricing->one_time_pay))
                                                        {{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}}
                                                        <small class="text-muted">/ Vĩnh viễn</small>
                                                    @elseif (!empty($product->pricing->monthly))
                                                        {{number_format($product->pricing->monthly,0,",",".") . ' VNĐ'}}
                                                        <small class="text-muted">/ 1 Tháng</small>
                                                    @elseif (!empty($product->pricing->quarterly))
                                                        {{number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'}}
                                                        <small class="text-muted">/ 3 Tháng</small>
                                                    @elseif (!empty($product->pricing->semi_annually))
                                                        {{number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'}}
                                                        <small class="text-muted">/ 6 Tháng</small>
                                                    @elseif (!empty($product->pricing->annually))
                                                        {{number_format($product->pricing->annually,0,",",".") . ' VNĐ'}}
                                                        <small class="text-muted">/ 1 Năm</small>
                                                    @elseif (!empty($product->pricing->biennially))
                                                        {{number_format($product->pricing->biennially,0,",",".") . ' VNĐ'}}
                                                        <small class="text-muted">/ 2 Năm</small>
                                                    @elseif (!empty($product->pricing->triennially))
                                                        {{number_format($product->pricing->triennially,0,",",".") . ' VNĐ'}}
                                                        <small class="text-muted">/ 3 Năm</small>
                                                    @else
                                                        Free
                                                    @endif
                                                </option>
                                            </select>
                                        </div>
                                        <div class="singlePrice">
                                            <ul class="list-unstyled ">
                                                <li>Dung lượng: {{ !empty( $product->meta_product->storage) ?  $product->meta_product->storage : '0'  }} GB</li>
                                                <li>Hosting: {{ !empty ($product->meta_product->domain) ? $product->meta_product->domain : '0'  }} domain</li>
                                                @if(!empty($product->meta_product->girf))
                                                    <li class="text-danger">{{ $product->meta_product->girf }}</li>
                                                @endif
                                            </ul>
                                        </div>

                                    @elseif ( $product->type_product == 'Change-IP')
                                        <div class="pricing-product">
                                            @if (!empty($product->pricing->one_time_pay))
                                                <span class='one_time'>
                                                    {{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}} / 1 lần đổi
                                                </span>
                                            @else
                                                <span class='one_time'>
                                                    Free
                                                </span>
                                            @endif
                                        </div>
                                        <div class="singlePrice">
                                            <ul class="list-unstyled ">
                                                <li>Thay đổi: 1 IP</li>
                                                @if(!empty($product->meta_product->girf))
                                                  <li class="text-danger">{{ $product->meta_product->girf }}</li>
                                                @endif
                                            </ul>
                                        </div>

                                    @else
                                        <div class="pricing-product">
                                            @if (!empty($product->pricing->one_time_pay))
                                                <div class="card-drop">
                                                    <a class='toggle' href="#">
                                                        <span class='label-active'>{{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}} / Vĩnh viễn </span>
                                                        <input type="hidden" name="" class="billing_cycle_product" value="one_time">
                                                    </a>
                                                    <ul>
                                                        <li class='active'>
                                                            <a data-label="{{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}} / Vĩnh viễn" data-product="{{ $product->id }}" data-billing="one_time_pay" href="#">
                                                                {{number_format($product->pricing->one_time_pay,0,",",".") . ' VNĐ'}} <small class="text-muted">/ Vĩnh viễn</small>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            @elseif (!empty($product->pricing->type) && $product->pricing->type == 'free')
                                                <div class="card-drop">
                                                    <a class='toggle' href="#">
                                                        <span class='label-active'>Free</span>
                                                        <input type="hidden" name="" class="billing_cycle_product" value="free">
                                                    </a>
                                                    <ul>
                                                        <li class='active'>
                                                            <a data-label="Free"  data-billing="free" href="#">
                                                            Free
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            @else
                                                <div class="card-drop">
                                                    <a class='toggle' href="#">
                                                        <span class='label-active'>{{number_format($product->pricing->annually,0,",",".") . ' VNĐ'}} / 1 Năm</span>
                                                        <input type="hidden" name="" class="billing_cycle_product" value="annually">
                                                    </a>
                                                    <ul>
                                                        @if(!empty($product->pricing->monthly))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->monthly,0,",",".") . ' VNĐ'}} / 1 Tháng" data-billing="monthly" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->monthly,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 1 Tháng</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if(!empty($product->pricing->twomonthly))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->twomonthly,0,",",".") . ' VNĐ'}} / 2 Tháng" data-billing="twomonthly" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->twomonthly,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 2 Tháng</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->quarterly))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'}} / 3 Tháng" data-billing="quarterly" data-product="{{ $product->id }}"  href="#">
                                                                    {{number_format($product->pricing->quarterly,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 3 Tháng</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->semi_annually))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'}} / 6 Tháng" data-billing="semi_annually" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->semi_annually,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 6 Tháng</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->annually))
                                                            <li class='active'>
                                                                <a data-label="{{number_format($product->pricing->annually,0,",",".") . ' VNĐ'}} / 1 Năm" data-billing="annually" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->annually,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 1 Năm</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->biennially))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->biennially,0,",",".") . ' VNĐ'}} / 2 Năm" data-billing="biennially" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->biennially,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 2 Năm</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                        @if (!empty($product->pricing->triennially))
                                                            <li>
                                                                <a data-label="{{number_format($product->pricing->triennially,0,",",".") . ' VNĐ'}} / 3 Năm" data-billing="triennially" data-product="{{ $product->id }}" href="#">
                                                                    {{number_format($product->pricing->triennially,0,",",".") . ' VNĐ'}}
                                                                    <small class="text-muted">/ 3 Năm</small>
                                                                </a>
                                                            </li>
                                                        @endif

                                                    </ul>
                                                </div>
                                            @endif

                                        </div>
                                        <div class="singlePrice">
                                            <ul class="list-unstyled ">
                                                <li>Dung lượng: {{ !empty( $product->meta_product->storage) ?  $product->meta_product->storage : '0'  }} MB</li>
                                                <li><b>Unlimited Data Transfer</b></li>
                                                <li>Hosting: {{ !empty ($product->meta_product->domain) ? $product->meta_product->domain : '0'  }} domain</li>
                                                <li>Sub Domain: {{ !empty($product->meta_product->sub_domain) ? $product->meta_product->sub_domain: '0'  }}</li>
                                                <li>Alias Domain: {{ !empty($product->meta_product->alias_domain) ? $product->meta_product->alias_domain : '0'  }}</li>
                                                <li>Database: {{ !empty($product->meta_product->database) ? $product->meta_product->database : '0'  }} database</li>
                                                <li>FTP: {{ !empty($product->meta_product->ftp) ? $product->meta_product->ftp : '0'  }} tài khoản</li>
                                                <li>Control Panel: {{ !empty($product->meta_product->panel) ? $product->meta_product->panel : ''  }}</li>
                                                @if(!empty($product->meta_product->girf))
                                                  <li class="text-danger">{{ $product->meta_product->girf }}</li>
                                                @endif
                                            </ul>
                                        </div>
                                    @endif
                                </div>

                                @if($product->type_product == 'Hosting' || $product->type_product == 'VPS' || $product->type_product == 'Server' 
                                  || $product->type_product == 'NAT-VPS' || $product->type_product == 'Hosting-Singapore' || $product->type_product == 'VPS-US')
                                    <div class="card-footer text-center">
                                        <a href="{{ route('user.addCard', $product->id) }}" class="btn btn-primary btn-sm btn-icon-split btn_add_card">
                                            <span class="icon"><i class="fas fa-shopping-cart"></i></span>
                                            <span class="text">Đăng ký ngay</span>
                                        </a>
                                    </div>
                                @endif

                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script src="{{ asset('libraries/SimpleDropDownEffects/js/jquery.dropdown.js') }}"></script>
<script src="{{ asset('js/user_choose_product.js') }}"></script>
@endsection
