@extends('layouts.user2.app_console')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/console/wmks-all.css') }}">
@endsection
@section('title')
<div class="text-primary">Console VPS {{ !empty($data_console['ip']) ? $data_console['ip'] : '' }} </div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><a href="{{ route('service.on') }}"><i class="fas fa-server"></i> Dịch vụ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-desktop"></i> Console</li>
@endsection
@section('content')
    @if ($data_console['error'] == 0)
      <input type="hidden" id="vm_host" value="{{ $data_console['vm_host'] }}">
      <input type="hidden" id="vm_ticket" value="{{ $data_console['vm_ticket'] }}">
      <div id="buttonBar">
          <div class="buttonC">
              <button id="cad" class="mb-3 btn btn-sm">
                  Send Ctrl+Alt+Delete
              </button>
          </div>
      </div>
      <div id="wmksContainer" style="position:absolute;width:100%;height:100%"></div>
    @else
      <div class="error-connect text-center">
         <b class="text-danger">Lỗi kết nối ....</b>
      </div>
    @endif
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('libraries/jquery-ui/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('libraries/console/wmks.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function() {
      var wmks = WMKS.createWMKS("wmksContainer", {}).register(WMKS.CONST.Events.CONNECTION_STATE_CHANGE, function (event, data) {
          if (data.state == WMKS.CONST.ConnectionState.CONNECTED) {
              console.log("connection state change : connected");
          }
      });

      var vm_host = $('#vm_host').val();
      var vm_ticket = $('#vm_ticket').val();

      var connect_string = 'wss://' + vm_host + ':443/ticket/' + vm_ticket;
      wmks.connect(connect_string);
      $('#cad').on("click", function () {
          wmks.sendCAD();
      });

  })
</script>
@endsection
