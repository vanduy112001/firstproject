@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('/libraries/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('/libraries/toastr/toastr.min.css')}}">
@endsection
@section('title')
    <div class="text-primary">Chi tiết sản phẩm</div>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
    <li class="breadcrumb-item active"><i class="fas fa-server"></i> Dịch vụ</li>
    <li class="breadcrumb-item active"> Chi tiết sản phẩm</li>
@endsection
@section('content')
  <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Thông tin dịch vụ / sản phẩm</h3>
                </div>
                <div class="card-body row product-details clearfix">
                    @include('users.services.includes.detail-server',compact('detail','billings','payment_methods','isExpired'))
                </div>
            </div>
            <div class="card mt-4">
                <div class="card-header">
                    <h3 class="card-title">Thông tin chi tiết Server</h3>
                </div>
                <div class="card-body row mb-4">
                    @php
                      $product = $detail->product;
                    @endphp
                    @if($type == 'server')
                        <div class="col-md-5 text-right">
                            <b>IP:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $detail->ip }}
                        </div>
                        <div class="col-md-5 text-right">
                            <b>IP 2:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $detail->ip2 }}
                        </div>
                        <div class="col-md-5 text-right">
                            <b>Username:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $detail->user_name }}
                        </div>
                        <div class="col-md-5 text-right">
                            <b>Password:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ !empty($detail->password) ? $detail->password : '' }}
                        </div>
                        <div class="col-md-5 text-right">
                            <b>CPU:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $product->meta_product->cpu }} ({{ $product->meta_product->cores }})
                        </div>
                        <div class="col-md-5 text-right">
                            <b>RAM:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $product->meta_product->memory }}
                        </div>
                        <div class="col-md-5 text-right">
                            <b>DISK:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            @php
                              $addonConfig = GroupProduct::get_config_server($detail->id);
                            @endphp
                            {{ $product->meta_product->disk }} {{ $addonConfig }}
                        </div>
                        <div class="col-md-5 text-right">
                            <b>Hệ điều hành:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $detail->os }}
                        </div>
                        <div class="col-md-5 text-right">
                            <b>RAID:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $detail->raid }}
                        </div>
                        <div class="col-md-5 text-right">
                            <b>Data Center:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $detail->location }}
                        </div>
                        <div class="col-md-5 text-right">
                            <b>Port Network:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $product->meta_product->port_network }}
                        </div>
                        <div class="col-md-5 text-right">
                            <b>Băng thông:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $product->meta_product->bandwidth }}
                        </div>
                    @endif
                </div>
            </div>
            <input type="hidden" id="billing" value="{{ $detail->billing_cycle }}">
            <input type="hidden" id="id" value="{{ $detail->id }}">
            <div class="card mt-4" id="card_config">
            </div>
        </div>
    </div>
    <div class="card card-outline card-danger">
        <div class="card-header">
          <h3 class="card-title">Hoạt động</h3>
          <!-- /.card-tools -->
        </div>
        <div class="card-body">
          <table class="table table-bordered">
              <thead class="primary">
                  <th>Loại</th>
                  <th>Trạng thái</th>
                  <th>Thời gian</th>
              </thead>
              <tbody>
                @foreach ( $log_activities as $log_activity )
                    <tr>
                      <td>{{ $log_activity->action }}</td>
                      <td>{!! !empty($log_activity->description_user) ? $log_activity->description_user : '' !!}</td>
                      <td>{{ date('H:i:s d-m-Y', strtotime($log_activity->created_at) ) }}</td>
                    </tr>
                @endforeach
              </tbody>
              <tfoot class="card-footer clearfix">
                <td colspan="11" class="text-center link-right">
                    {{ $log_activities->links()  }}
                </td>
              </tfoot>
          </table>
        </div>
    </div>
    <div class="card card-outline card-success">
        <div class="card-header">
          <h3 class="card-title">Thanh toán</h3>
          <!-- /.card-tools -->
        </div>
        <div class="card-body table-responsive">
          <table class="table table-bordered">
              <thead class="primary">
                  <th>Loại</th>
                  <th>Trạng thái</th>
                  <th>Thời gian</th>
              </thead>
              <tbody>
                @foreach ( $log_payments as $log_payment )
                    <tr>
                      <td>{{ $log_payment->action }}</td>
                      <td>{!! !empty($log_activity->description_user) ? $log_activity->description_user : '' !!}</td>
                      <td>{{ date('H:i:s d-m-Y', strtotime($log_payment->created_at) ) }}</td>
                    </tr>
                @endforeach
              </tbody>
              <tfoot class="card-footer clearfix">
                <td colspan="11" class="text-center link-right">
                    {{ $log_payments->links()  }}
                </td>
              </tfoot>
          </table>
        </div>
    </div>
    {{-- Modal xoa invoice --}}
    <div class="modal fade" id="terminated_services">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Xóa đơn đặt hàng</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div id="notication-invoice" class="text-center">

                </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
              <input type="submit" class="btn btn-primary" id="button-terminated" value="Xác nhận">
            </div>
          </div>
          <!-- /.modal-content -->
      </div>
        <!-- /.modal-dialog -->
    </div>
      <!-- /.modal -->
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}"></script>
<script src="{{ asset('js/detail_service.js') }}"></script>
@endsection
