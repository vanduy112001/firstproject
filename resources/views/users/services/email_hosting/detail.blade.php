@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('/libraries/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('/libraries/toastr/toastr.min.css')}}">
@endsection
@section('title')
    <div class="text-primary">Chi tiết sản phẩm</div>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
    <li class="breadcrumb-item active"><i class="fas fa-server"></i> Dịch vụ</li>
    <li class="breadcrumb-item active"> Chi tiết sản phẩm</li>
@endsection
@section('content')
  <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Thông tin dịch vụ / sản phẩm</h3>
                </div>
                <div class="card-body row product-details clearfix">
                  <div class="col-md-6">
                      @if ($detail->status_hosting == 'on')
                          <div class="product-status product-status-on">
                              <div class="product-icon text-center">
                                  <span class="fa-stack fa-lg">
                                      <i class="fas fa-circle fa-stack-2x"></i>
                                      <i class="fas fa-server fa-stack-1x fa-inverse"></i>
                                  </span>
                                  <h3>{{ $detail->domain }}</h3>
                                  <h4>Loại {{ $detail->product->name }}</h4>
                              </div>
                              <div class="product-status-text">
                                  @if($isExpired)
                                      Đã hết hạn
                                  @else
                                      Đang sử dụng
                                  @endif
                              </div>

                          </div>
                          <div class="row">
                              <div class="button_service col-md-12" style="max-width:70%;margin:auto;">
                                  <a href="{{ route('service.terminated', $detail->id) }}?type=colocation" class="btn btn-block btn-danger">Hủy dịch vụ</a>
                              </div>
                          </div>
                          @if($isExpired)
                              <br>
                              <div class="row">
                                  <div class="button_service col-md-12" style="max-width:70%;margin:auto;">
                                      <a href="{{ route('service.extend', $detail->id) }}?type=colocation"
                                         class="btn btn-block btn-success">Gia hạn ngay</a>
                                  </div>
                              </div>
                          @endif
                      @elseif  ($detail->status_hosting == 'off')
                          <div class="product-status product-status-off">
                              <div class="product-icon text-center">
                                <span class="fa-stack fa-lg">
                                  <i class="fas fa-circle fa-stack-2x"></i>
                                  <i class="fas fa-database fa-stack-1x fa-inverse"></i>
                                </span>
                                <h3>{{ $detail->domain }}</h3>
                                <h4>{{ $detail->product->name }}</h4>
                              </div>
                              <div class="product-status-text">
                                  Đang tắt
                              </div>
                          </div>
                          <div class="row">
                              <div class="button_service col-md-12" style="max-width:70%;margin:auto;">
                                  <a href="{{ route('service.terminated', $detail->id) }}?type=colocation" class="btn btn-block btn-danger">Hủy dịch vụ</a>
                              </div>
                          </div>
                      @else
                          <div class="product-status product-status-cancelled">
                              <div class="product-icon text-center">
                                <span class="fa-stack fa-lg">
                                  <i class="fas fa-circle fa-stack-2x"></i>
                                  <i class="fas fa-database fa-stack-1x fa-inverse"></i>
                                </span>
                                <h3>{{ $detail->domain }}</h3>
                                <h4>{{ $detail->product->name }}</h4>
                              </div>
                              <div class="product-status-text">
                                  @if($detail->status_hosting == 'cancel')
                                      Đợi hủy
                                  @else
                                      Đã hủy
                                  @endif
                              </div>
                          </div>
                      @endif
                  </div>

                  <div class="col-md-6 text-center mt-4 info-services">
                      <h4>Ngày tạo</h4>
                      {{ !empty($detail->date_create) ? date('d-m-Y', strtotime($detail->date_create)) : 'Chưa tạo'  }}
                      <h4>Chi phí</h4>
                      {{ !empty($detail->price_override) ? number_format($detail->price_override,0,",",".") . ' VNĐ' : number_format($detail->detail_order->sub_total,0,",",".") . ' VNĐ' }}
                      <h4>Thời gian thuê</h4>
                      {{ !empty($billings[$detail->billing_cycle]) ? $billings[$detail->billing_cycle] : '' }}
                      @if(!empty($detail->next_due_date))
                          <h4>Ngày hết hạn</h4>
                          {{ date('d-m-Y', strtotime($detail->next_due_date)) }}
                      @endif
                      <h4>Hình thức thanh toán</h4>
                      {{ !empty($payment_methods[$detail->detail_order->payment_method]) ? $payment_methods[$detail->detail_order->payment_method] : 'Thanh toán bằng số dư tài khoản' }}
                  </div>
                </div>
            </div>
            <div class="card mt-4">
                <div class="card-header">
                    <h3 class="card-title">Thông tin chi tiết Email Hosting</h3>
                </div>
                <div class="card-body row mb-4">
                    <div class="col-md-10 text-center">
                        <h4>Thông tin Email Hosting</h4>
                    </div>
                    <div class="col-md-5 text-right">
                        <b>Domain:</b>
                    </div>
                    <div class="col-md-7 text-left">
                        {{ $detail->domain }}
                    </div>
                    <div class="col-md-5 text-right">
                        <b>Tên sản phẩm:</b>
                    </div>
                    <div class="col-md-7 text-left">
                        {{ $detail->product->name }}
                    </div>
                    <div class="col-md-5 text-right">
                        <b>Tài khoản:</b>
                    </div>
                    <div class="col-md-7 text-left">
                        {{ !empty($detail->user) ? $detail->user : '' }}
                    </div>
                    <div class="col-md-5 text-right">
                        <b>Mật khẩu:</b>
                    </div>
                    <div class="col-md-7 text-left">
                        {{ !empty($detail->password) ? $detail->password : '' }}
                    </div>
                </div>
            </div>
            <input type="hidden" id="billing" value="{{ $detail->billing_cycle }}">
            <input type="hidden" id="id" value="{{ $detail->id }}">
            <div class="card mt-4" id="card_config">

            </div>
        </div>
    </div>
    {{-- Modal xoa invoice --}}
    <div class="modal fade" id="terminated_services">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Xóa đơn đặt hàng</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div id="notication-invoice" class="text-center">

                </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
              <input type="submit" class="btn btn-primary" id="button-terminated" value="Xác nhận">
            </div>
          </div>
          <!-- /.modal-content -->
      </div>
        <!-- /.modal-dialog -->
    </div>
      <!-- /.modal -->
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}"></script>
<script src="{{ asset('js/detail_service.js') }}"></script>
@endsection
