@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Dịch vụ Hosting gần hết hạn hoặc quá hạn</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-database"></i> Dịch vụ Hosting</li>
@endsection
@section('content')
<div class="row">
    <div class="col col-md-12">
        <!-- <div class="button-list">
            <div class="menu-vps">
                <button class="btn btn-primary btn-list-vps btn-icon-split">
                    <span class="icon text-white-50"><i class="fa fa-sitemap" aria-hidden="true"></i></span>
                    <span class="text">List VPS</span>
                </button>
            </div>
            <div class="menu-hosting">
                <button class="btn btn-danger btn-list-hosting btn-icon-split">
                    <span class="icon text-white-50"><i class="fa fa-globe" aria-hidden="true"></i></span>
                    <span class="text">List Hosting</span>
                </button>
            </div>
            <div class="menu-server">
                <button class="btn btn-info btn-list-server btn-icon-split">
                    <span class="icon text-white-50"><i class="fa fa-server" aria-hidden="true"></i> </span>
                    <span class="text">List Server</span>
                </button>
            </div>
        </div> -->
        <div class="col-md-12 detail-course-finish">
            @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
            @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
            @endif
        </div>
        {{-- List Hosting --}}

          <div class="list_hosting_nearly" id="hosting">
              <div class="box box-primary">
                  <div class="box-body table-responsive">
                      <div class="dataTables_wrapper form-inline dt-bootstrap">
                          <table class="table table-bordered table-hover" id="data-hosting-table">
                              <thead class="danger">
                                <!-- <th><input type="checkbox" class="hosting_checkbox_all" data-toggle="tooltip" data-placement="top" title="Chọn tất cả Hosting"></th> -->
                                <th>Dịch vụ</th>
                                <th>Domain</th>
                                <th>Ngày tạo</th>
                                <th>Ngày kết thúc</th>
                                <th>Thời gian thuê</th>
                                <th>Trạng thái</th>
                                <th>Hành động</th>
                              </thead>
                              <tbody>
                              </tbody>
                              <tfoot class="card-footer clearfix">
                              </tfoot>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
        </div>
        {{-- Kết thúc list Hosting --}}
    </div>
</div>
{{-- Modal xoa invoice --}}
<div class="modal fade" id="modal-services">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button_email_hosting_terminated" value="Xác nhận">
          <button type="button" class="btn btn-danger" id="button_email_hosting_rebuil" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<!-- Model action từng services -->
<div class="modal fade" id="modal-service">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-service" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button_email_hosting_service">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>

@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('libraries/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('js/email_hosting_nearly_and_expire.js') }}"></script>
@endsection
