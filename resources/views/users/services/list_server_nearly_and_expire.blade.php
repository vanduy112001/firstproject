@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Dịch vụ Server gần hết hạn hoặc quá hạn</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-cloud"></i> Dịch vụ Server</li>
@endsection
@section('content')
<div class="row">
    <div class="col col-md-12">
        <div class="col-md-12 detail-course-finish">
            @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
            @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
            @endif
        </div>
        {{-- List Server --}}
        <div id="custom-tabs-two-tabContent" class="tab-content">
          <div class="list_server_nearly tab-pane fade show active" id="server" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
              <div class="box box-primary">
                  <div class="box-body table-responsive">
                      <div class="dataTables_wrapper form-inline dt-bootstrap">
                          <div class="col-md-12 detail-course-finish">
                              @if(session("success"))
                              <div class="bg-success">
                                  <p class="text-light">{{session("success")}}</p>
                              </div>
                              @elseif(session("fails"))
                              <div class="bg-danger">
                                  <p class="text-light">{{session("fails")}}</p>
                              </div>
                              @endif
                          </div>
                          <div class="col-md-12 row">
                            <div class="col-md-8 mt-2 title_list_server">
                              <h3 class="mb-4">Dịch vụ Server hủy / hết hạn</h3>
                          </div>
                          <table class="table table-bordered table-hover" id="data-server-table">
                              <thead class="primary">
                                  <th>IP</th>
                                  <th>Cấu hình</th>
                                  <th>Ngày tạo</th>
                                  <th>Ngày kết thúc</th>
                                  <th>Thời gian</th>
                                  <th>Chi phí</th>
                                  <th>Trạng thái</th>
                                  <th>Action</th>
                              </thead>
                              <tbody>
                              </tbody>
                              <tfoot class="card-footer clearfix">
                              </tfoot>
                          </table>
                      </div>
                  </div>
              </div>
          </div>

        {{-- Kết thúc list Server --}}
    </div>
</div>
{{-- Modal xoa invoice --}}
<div class="modal fade" id="modal-services">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-terminated" value="Xác nhận">
          <button type="button" class="btn btn-danger" id="button-rebuil">Xác nhận</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<!-- Model action từng services -->
<div class="modal fade" id="modal-service">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-service" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button_server_service">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>

@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('libraries/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('js/server_nearly_and_expire.js') }}"></script>
@endsection
