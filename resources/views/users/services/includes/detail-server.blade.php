@php
use Carbon\Carbon;
@endphp
<div class="col-md-6">
    @if ($detail->status_server == 'on')
        <div class="product-status product-status-on">
            <div class="product-icon text-center">
                <span class="fa-stack fa-lg">
                    <i class="fas fa-circle fa-stack-2x"></i>
                    <i class="fas fa-database fa-stack-1x fa-inverse"></i>
                </span>
                <h3>{{ $detail->server_name }}</h3>
                <h4>{{ $detail->type }}</h4>
            </div>
            <div class="product-status-text">
                @if($isExpired)
                    Đã hết hạn
                @else
                    Đang sử dụng
                @endif
            </div>

        </div>
        <div class="row">
            <div class="button_service col-md-12" style="max-width:70%;margin:auto;">
                <a href="{{ route('service.terminated', $detail->id) }}?type=server" class="btn btn-block btn-danger button_service_cancel" data-type="server" data-id="{{ $detail->id }}" data-name="{{ $detail->ip }}">Hủy dịch vụ</a>
            </div>
        </div>
        @if($isExpired)
            <br>
            <div class="row">
                <div class="button_service col-md-12" style="max-width:70%;margin:auto;">
                    <a href="{{ route('service.extend', $detail->id) }}?type=server"
                       class="btn btn-block btn-success">Gia hạn ngay</a>
                </div>
            </div>
        @endif
    @elseif  ($detail->status_server == 'off')
        <div class="product-status product-status-off">
            <div class="product-icon text-center">
              <span class="fa-stack fa-lg">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-database fa-stack-1x fa-inverse"></i>
              </span>
              <h3>{{ $detail->server_name }}</h3>
              <h4>{{ $detail->type }}</h4>
            </div>
            <div class="product-status-text">
                Đang tắt
            </div>
        </div>
        <div class="row">
            <div class="button_service col-md-12" style="max-width:70%;margin:auto;">
                <a href="{{ route('service.terminated', $detail->id) }}?type=server" class="btn btn-block btn-danger button_service_cancel" data-type="server" data-id="{{ $detail->id }}" data-name="{{ $detail->ip }}">Hủy dịch vụ</a>
            </div>
        </div>
    @else
        <div class="product-status product-status-cancelled">
            <div class="product-icon text-center">
              <span class="fa-stack fa-lg">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fas fa-database fa-stack-1x fa-inverse"></i>
              </span>
              <h3>{{ $detail->server_name }}</h3>
              <h4>{{ $detail->type }}</h4>
            </div>
            <div class="product-status-text">
                @if($detail->status_server == 'cancel')
                    Đợi hủy
                @elseif($detail->status_server == 'progressing')
                    Đang cài đặt
                @elseif($detail->status_server == 'delete_server')
                    Đã xóa
                @else
                    Đã hủy
                @endif
            </div>
        </div>
    @endif
</div>

<div class="col-md-6 text-center mt-4 info-services">
    <div class="m-1">
      <h4>Ngày tạo</h4>
      {{ !empty($detail->date_create) ? date('d-m-Y', strtotime($detail->date_create)) : 'Chưa tạo'  }}
    </div>
    <div class="m-1">
      <h4>Chi phí</h4>
      @php
        $total = 0;
        if ( !empty($detail->amount) ) {
            $total = $detail->amount;
        }
        else {
            $product = $detail->product;
            $total = !empty( $product->pricing[$detail->billing_cycle] ) ? $product->pricing[$detail->billing_cycle] : 0;
            if (!empty($detail->server_config)) {
              $server_config = $detail->server_config;
              $pricing_addon = 0;
              if ( !empty($server_config->disk2) ) {
                $pricing_addon += $server_config->product_disk2->pricing[$detail->billing_cycle];
              }
              if ( !empty($server_config->disk3) ) {
                $pricing_addon += $server_config->product_disk3->pricing[$detail->billing_cycle];
              }
              if ( !empty($server_config->disk4) ) {
                $pricing_addon += $server_config->product_disk4->pricing[$detail->billing_cycle];
              }
              if ( !empty($server_config->disk5) ) {
                $pricing_addon += $server_config->product_disk5->pricing[$detail->billing_cycle];
              }
              if ( !empty($server_config->disk6) ) {
                $pricing_addon += $server_config->product_disk6->pricing[$detail->billing_cycle];
              }
              if ( !empty($server_config->disk7) ) {
                $pricing_addon += $server_config->product_disk7->pricing[$detail->billing_cycle];
              }
              if ( !empty($server_config->disk8) ) {
                $pricing_addon += $server_config->product_disk8->pricing[$detail->billing_cycle];
              }
              $total += $pricing_addon;
            }
        }
      @endphp
      {{ number_format($total,0,",",".") . ' VNĐ' }}
    </div>
    <div class="m-1">
      <h4>Chu kỳ thanh toán</h4>
      {{ !empty($billings[$detail->billing_cycle]) ? $billings[$detail->billing_cycle] : '' }}
    </div>
    <div class="m-1">
      <h4>Tổng thời gian thuê</h4>
      @php
        $total_time = '';
        $create_date = new Carbon($detail->date_create);
        $next_due_date = new Carbon($detail->next_due_date);
        if ( $next_due_date->diffInYears($create_date) ) {
          $year = $next_due_date->diffInYears($create_date);
          $total_time = $year . ' Năm ';
          $create_date = $create_date->addYears($year);
          $month = $next_due_date->diffInMonths($create_date);
          if ( $month ) {
            $total_time .= $month . ' Tháng';
          }
        } else {
          $diff_month = $next_due_date->diffInMonths($create_date);
          $total_time = $diff_month . ' Tháng';
        }
      @endphp
      {{ $total_time }}
    </div>
    <div class="m-1">
      @if(!empty($detail->next_due_date))
          <h4>Ngày hết hạn</h4>
          {{ date('d-m-Y', strtotime($detail->next_due_date)) }}
      @endif
    </div>
    <div class="m-1">
      <h4>Hình thức thanh toán</h4>
      {{ !empty($payment_methods[$detail->detail_order->payment_method]) ? $payment_methods[$detail->detail_order->payment_method] : 'Thanh toán bằng số dư tài khoản' }}
    </div>
    <div class="m-1">
      @if ( $detail->status_server != 'cancel' )
        @if ( $detail->status_server != 'admin_off' )
          @if ( $detail->status_server != 'expire' )
            @if ( $detail->status_server != 'delete_server' )
              @if ( $detail->status_server != 'suspend' )
                @if ( $detail->status_server != 'progressing' )
                  <!-- <div class="button_server_add_config mt-4">
                      <button type="button" class="btn btn-primary" name="button">Thêm cấu hình</button>
                  </div> -->
                @endif
              @endif
            @endif
          @endif
        @endif
      @endif
    </div>
</div>
