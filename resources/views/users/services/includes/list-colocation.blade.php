<div class="list_server" id="server">
    <div class="box box-primary">
        <div class="box-body table-responsive">
            <div class="dataTables_wrapper form-inline dt-bootstrap">
                <h3>Dịch vụ Server</h3>
                <div class="col-md-12 row">
                  <div class="col-md-8 mt-2 title_list_vps">
                    <span class="ml-3">Số lượng: </span>
                    <select class="vp_classic">
                      <option value="20" selected>20 Server</option>
                      <option value="30">30 Server</option>
                      <option value="40">40 Server</option>
                      <option value="50">50 Server</option>
                      <option value="100">100 Server</option>
                    </select>
                  </div>
                  <div class="col-md-4 mb-3">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                      </div>
                      <input type="text" class="form-control" placeholder="Tìm kiếm Server" id="vps_server">
                    </div>
                  </div>
                </div>
                <table class="table table-bordered table-hover">
                    <thead class="info">
                        <th></th>
                        <th>IP</th>
                        <th>Loại</th>
                        <th>Ngày tạo</th>
                        <th>Ngày kết thúc</th>
                        <th>Chi phí</th>
                        <th>Trạng thái</th>
                        <th>Hành động</th>
                    </thead>
                    <tbody>
                    @if($list_colocations->count() > 0)
                        @foreach ($list_colocations as $colocation)
                            @php
                                $isExpire = false;
                                $expired = false;
                                if(!empty($colocation->next_due_date)){
                                    $next_due_date = strtotime(date('Y-m-d', strtotime($colocation->next_due_date)));
                                    $date = date('Y-m-d');
                                    $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                                    if($next_due_date <= $date) {
                                      $isExpire = true;
                                    }
                                    $date_now = strtotime(date('Y-m-d'));
                                    if ($next_due_date < $date_now) {
                                        $expired = true;
                                    }
                                }
                            @endphp
                            <tr>
                                <td><input type="checkbox" value="{{ $colocation->id }}"  data-ip="{{ $colocation->ip }}" class="server_checkbox"></td>
                                <td>
                                    @if (!empty($server->ip))
                                        <a href="{{ route( 'service.server_detail', $colocation->id ) }}">{{$colocation->ip}}</a>
                                        @if($expired)
                                          - <span class="text-danger">Hết hạn</span>
                                        @elseif($isExpire)
                                          - <span class="text-danger">Gần hết hạn</span>
                                        @endif
                                    @else
                                        <span class="text-danger">Chưa tạo</span>
                                    @endif
                                </td>
                                <td>
                                    {{ $colocation->type_colo }}
                                </td>
                                <td>
                                  @if (!empty($colocation->date_create))
                                      <span>{{ date('d-m-Y', strtotime($colocation->date_create)) }}</span>
                                  @else
                                      <span class="text-danger">Chưa tạo</span>
                                  @endif
                                </td>
                                <td>
                                  @if (!empty($colocation->next_due_date))
                                      <span>{{ date('d-m-Y', strtotime($colocation->next_due_date)) }}</span>
                                  @else
                                      <span class="text-danger">Chưa tạo</span>
                                  @endif
                                </td>
                                <td>
                                    {{ $billing[$colocation->billing_cycle] }}
                                </td>
                                <td> <b>{!!number_format( $colocation->amount ,0,",",".")!!} VNĐ</b> </td>
                                <td class="server-status">
                                    @if ($colocation->colocation == 'on')
                                        <span class="text-success">Đang bật</span>
                                    @elseif ($colocation->status_colo == 'off')
                                        <span class="text-danger">Đã tắt</span>
                                    @elseif ($colocation->status_colo == 'cancel')
                                        <span class="text-danger">Đã hủy</span>
                                    @elseif ($colocation->status_colo == 'delete_colo')
                                        <span class="text-danger">Đã xóa</span>
                                    @else
                                        <span class="text-secondary">Đã hủy</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($colocation->status_colo != 'suspend')
                                        @if ($isExpire || $expired)
                                            <button type="button" class="btn btn-warning button-action-server expired" data-toggle="tooltip" data-placement="top" title="Gia hạn Colocation" data-action="expired" data-id="{{ $colocation->id }}" data-ip="{{ $colocation->ip }}"><i class="fas fa-plus-circle"></i></button>
                                        @endif
                                        @if ($colocation->status_colo != 'expire')
                                          <!-- <button class="btn btn-outline-warning button-action-vps on" @if($server->status_vps == 'off' || $server->status_vps == 'cancel') disabled @endif data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="{{ $server->id }}" data-ip="{{ $server->ip }}"><i class="fas fa-power-off"></i></button>
                                          <button class="btn btn-outline-info button-action-vps off" @if($server->status_vps == 'on' || $server->status_vps == 'cancel') disabled @endif data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="{{ $server->id }}" data-ip="{{ $server->ip }}"><i class="far fa-stop-circle"></i></button>
                                          <button class="btn btn-outline-success button-action-vps restart" @if($server->status_vps == 'off' || $server->status_vps == 'cancel') disabled @endif data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="{{ $server->id }}" data-ip="{{ $server->ip }}"><i class="fas fa-sync-alt"></i></button> -->
                                          @if($colocation->status_colo != 'cancel')
                                              <button class="btn btn-outline-danger button-action-server terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ Colocation" data-action="terminated" data-id="{{ $colocation->id }}" data-ip="{{ $colocation->ip }}"><i class="fa fa-ban" aria-hidden="true"></i></button>
                                          @endif
                                        @endif
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <td class="text-center text-danger" colspan="9">
                            Không có dịch vụ Colocation được sử dụng
                        </td>
                    @endif
                    </tbody>
                    <tfoot class="card-footer clearfix">
                        <td colspan="9" class="text-center link-right">
                            {{ $list_colocations->links()  }}
                        </td>
                    </tfoot>
                </table>
            </div>
            <div id="group_button">
                <div class="table-responsive" style="display: block;" >
                    <!-- <button class="btn btn-success btn-action-server btn-icon-split btn-sm mb-1" data-type="on">
                        <span class="icon"><i class="fa fa-toggle-on" aria-hidden="true"></i></span>
                        <span class="text">Bật Server</span>
                    </button>
                    <button class="btn btn-warning btn-action-server btn-icon-split btn-sm mb-1" data-type="off">
                        <span class="icon"><i class="fa fa-power-off" aria-hidden="true"></i></span>
                        <span class="text">Tắt Server</span>
                    </button> -->
                    <button class="btn bg-gradient-secondary btn-action-server btn-icon-split btn-sm mb-1 expired" data-type="expired">
                        <span class="icon"><i class="fas fa-plus-circle"></i></span>
                        <span class="text">Gia hạn Colocation</span>
                    </button>
                    <button class="btn btn-danger btn-action-server btn-icon-split btn-sm mb-1" data-type="delete">
                        <span class="icon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                        <span class="text">Hủy Colocation</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
