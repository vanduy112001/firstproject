@php
  $product = $detail->product;
@endphp
<div class="col-md-6">
  @if ($detail->status == 'on' || $detail->status == 'progressing')
      <div class="product-status @if($detail->status == 'on') product-status-on @else product-status-progressing @endif">
        <div class="product-icon text-center">
          <span class="fa-stack fa-lg">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fas fa-database fa-stack-1x fa-inverse"></i>
          </span>
          <h3>{{ $product->name }}</h3>
          <h4>{{ $product->group_product->name }}</h4>
        </div>
        <div class="product-status-text">
          @if ($detail->status == 'on')
              @if($isExpired)
                Đã hết hạn
              @else
                Đang sử dụng
              @endif
          @else
              Đang tạo
          @endif
        </div>
      </div>
      <div class="row">
        <div class="col-md-12" style="max-width:70%;margin:auto;">
          @if ($detail->status == 'on')
            <a href="{{ route('service.terminated', $detail->id) }}?type=proxy" class="button_service_cancel btn btn-block btn-danger" data-type="proxy" data-id="{{ $detail->id }}" data-name="{{ $detail->ip }}">Hủy dịch vụ
            </a>
          @endif
        </div>
      </div>
      @if($isExpired)
          <br>
          <div class="row">
              <div class="button_service col-md-12" style="max-width:70%;margin:auto;">
                  <a href="{{ route('service.extend', $detail->id) }}?type=proxy" class="btn btn-block btn-success">
                    Gia hạn ngay
                  </a>
              </div>
          </div>
      @endif

  @elseif  ($detail->status == 'off')
      <div class="product-status product-status-off">
        <div class="product-icon text-center">
          <span class="fa-stack fa-lg">
            <i class="fas fa-circle fa-stack-2x"></i>
            <i class="fas fa-database fa-stack-1x fa-inverse"></i>
          </span>
          <h3>{{ $product->name }}</h3>
          <h4>{{ $product->group_product->name }}</h4>
        </div>
        <div class="product-status-text">
          Đang tắt
        </div>
      </div>
      <div class="row">
        <div class=" col-md-12" style="max-width:70%;margin:auto;">
            <a href="{{ route('service.terminated', $detail->id) }}?type=proxy"  data-type="proxy" data-id="{{ $detail->id }}" data-name="{{ $detail->ip }}" class="button_service_cancel btn btn-block btn-danger">Hủy dịch vụ</a>
        </div>
      </div>
   @else
        <div class="product-status product-status-cancelled">
            <div class="product-icon text-center">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fas fa-database fa-stack-1x fa-inverse"></i>
                </span>
                <h3>{{ $product->name }}</h3>
                <h4>{{ $product->group_product->name }}</h4>
            </div>
            <div class="product-status-text">
              @if ($detail->status == 'cancel')
                Đã hủy
              @elseif ($detail->status == 'delete')
                Đã xóa
              @elseif ($detail->status == 'change_user')
                Đã chuyển
              @else
                Đã hủy
              @endif
            </div>
        </div>
    @endif
</div>

@include('users.services.includes.detail-right-content',compact('detail','payment_methods'))
