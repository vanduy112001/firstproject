<div class="col-md-6 text-center mt-4 info-services">
    <h4>Ngày tạo</h4>
    {{ !empty($detail->date_create) ? date('d-m-Y', strtotime($detail->date_create)) : 'Chưa tạo'  }}
    <h4>Chi phí</h4>
    @if($detail->detail_order->type == 'VPS' || $detail->detail_order->type == 'NAT-VPS')
        <?php
            $product = GroupProduct::get_product($detail->product_id);
            if (!empty($detail->price_override)) {
                $total = $detail->price_override;
            } else {
                $total = $product->pricing[$detail->billing_cycle];
                if (!empty($detail->vps_config)) {
                    $add_on_products = GroupProduct::get_addon_product_private(Auth::user()->id);
                    $pricing_addon = 0;
                    foreach ($add_on_products as $key => $add_on_product) {
                        if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $detail->vps_config->cpu * $add_on_product->pricing[$detail->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $detail->vps_config->ram * $add_on_product->pricing[$detail->billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $detail->vps_config->disk * $add_on_product->pricing[$detail->billing_cycle] / 10;
                            }
                        }
                    }
                    $total += $pricing_addon;
                }
            }
        ?>
        {{ !empty($total) ? number_format($total,0,",",".") . ' VNĐ' : '0 VNĐ' }}
    @else
        {{ !empty($detail->detail_order->sub_total) ? number_format($detail->detail_order->sub_total,0,",",".") . ' VNĐ' : '0 VNĐ' }}
    @endif
    <h4>Thời gian thuê</h4>
    {{ !empty($billings[$detail->billing_cycle]) ? $billings[$detail->billing_cycle] : '' }}
    @if(!empty($detail->next_due_date))
        <h4>Ngày hết hạn</h4>
        {{ date('d-m-Y', strtotime($detail->next_due_date)) }}
    @endif
    <h4>Hình thức thanh toán</h4>
    {{ !empty($payment_methods[$detail->detail_order->payment_method]) ? $payment_methods[$detail->detail_order->payment_method] : 'Thanh toán bằng số dư tài khoản' }}
    @if(!$product->meta_product->product_special)
      @if ($detail->detail_order->type == 'VPS' || $detail->detail_order->type == 'NAT-VPS')
        @if ( $detail->status_vps != 'cancel' )
          @if ( $detail->status_vps != 'admin_off' )
            @if ( $detail->status_vps != 'expire' )
              @if ( $detail->status_vps != 'delete_vps' )
                @if ( $detail->status_vps != 'change_user' )
                  @if ( $detail->status_vps != 'suspend' )
                    @if ( $detail->status_vps != 'progressing' )
                      @if(!empty($detail->product))
                        @if(!$detail->product->meta_product->product_special)
                          <div class="button_add_config mt-4">
                              <a href="{{ route('user.addCardAddon', $detail->id) }}" class="btn btn-primary" >Thêm cấu hình</a>
                          </div>
                        @endif
                      @endif
                    @endif
                  @endif
                @endif
              @endif
            @endif
          @endif
        @endif
      @elseif ($detail->detail_order->type == 'Hosting' || $detail->detail_order->type == 'Hosting-Singapore')
        @if ( $detail->status_hosting != 'cancel' )
          @if ( $detail->status_hosting != 'admin_off' )
            @if ( $detail->status_hosting != 'expire' )
              <div class="mt-4">
                <button type="button" class="btn btn-primary button_upgrade" data-id="{{ $detail->product_id }}" data-hosting="{{ $detail->id }}" data-billing="{{ $detail->billing_cycle }}" name="button"><i class="fas fa-upload"></i> Nâng cấp Hosting</button>
              </div>
            @endif
          @endif
        @endif
      @endif
    @endif
</div>
