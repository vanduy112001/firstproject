<div class="list_all_hosting text_list_vps" id="hosting">
    <div class="box box-default">
        <div class="box-body table-responsive">
            <div class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="col-md-12 my-2">
                    <div class="row">
                        <div class="col-md-4 title_list_vps px-1 mb-2" style="max-width: 150px;">
                            Số lượng
                            <select class="form-control form-control-sm all_hosting_classic">
                              <option value="20" selected>20</option>
                              <option value="30">30</option>
                              <option value="40">40</option>
                              <option value="50">50</option>
                              <option value="100">100</option>
                            </select>
                        </div>
                        <div class="col-md-4 mb-2 px-1 ml-auto">
                            <div class="input-group input-group-sm mb-0 mt-0">
                                <input type="text" id="all_hosting_search" class="form-control input-sm" placeholder="Tìm kiếm Hosting">
                                <div class="input-group-append">
                                    <button class="btn btn-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>

                  {{-- <div class="col-md-8 mt-2 title_list_vps">
                    <h3>Dịch vụ Hosting</h3>
                    <span class="ml-3">Số lượng: </span>
                    <select class="all_hosting_classic">
                      <option value="20" selected>20 Hosting</option>
                      <option value="30">30 Hosting</option>
                      <option value="40">40 Hosting</option>
                      <option value="50">50 Hosting</option>
                      <option value="100">100 Hosting</option>
                    </select>
                  </div>
                  <div class="col-md-4 mb-3">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-search"></i></span>
                      </div>
                      <input type="text" class="form-control" placeholder="Tìm kiếm Hosting" id="all_hosting_search">
                    </div>
                  </div>--}}
                  
                </div> 

                <table class="table table-bordered table-hover">
                    <thead class="">
                    <th>Dịch vụ</th>
                    <th>Domain</th>
                    <th>Khu vực</th>
                    <th>Khách hàng</th>
                    <th>Ngày tạo</th>
                    <th class="sort_all_hosting_next_due_date" data-sort="ASC" data-toggle="tooltip" data-placement="top" title="Sắp xếp ngày kết thúc">
                        Ngày kết thúc <i class="fas fa-sort"></i>
                        <input type="hidden" class="sort_all_hosting_type" value="">
                    </th>
                    <th>Thời gian thuê</th>
                    <th>Trạng thái</th>
                    </thead>
                    <tbody>
                    @if($hostings->count() > 0)
                        @foreach ($hostings as $hosting)
                            @php
                                $isExpire = false;
                                $expired = false;
                                if(!empty($hosting->next_due_date)){
                                    $next_due_date = strtotime(date('Y-m-d', strtotime($hosting->next_due_date)));
                                    $date = date('Y-m-d');
                                    $date = strtotime(date('Y-m-d', strtotime($date . '+15 Days')));
                                    if($next_due_date <= $date) {
                                      $isExpire = true;
                                    }
                                    $date_now = strtotime(date('Y-m-d'));
                                    if ($next_due_date < $date_now) {
                                        $expired = true;
                                    }
                                }
                            @endphp
                            <tr>
                                <td><a href="{{ route('services.detail', $hosting->id) }}?type=hosting">{{ $hosting->product->name }}</a></td>
                                <td class="service-domain">
                                    @if (!empty($hosting->domain))
                                        <a href="{{ route('services.detail', $hosting->id) }}?type=hosting">{{$hosting->domain}}</a>
                                        @if($expired)
                                          - <span class="text-danger">Hết hạn</span>
                                        @elseif($isExpire)
                                          - <span class="text-danger">Gần hết hạn</span>
                                        @endif
                                    @else
                                        <span class="text-danger">Chưa tạo</span>
                                    @endif
                                </td>
                                <td>
                                    @if ($hosting->location == 'vn')
                                        <span class="text-danger">Việt Nam</span>
                                    @elseif ($hosting->location == 'si')
                                        <span class="text-danger">Singapore</span>
                                    @else
                                        <span class="text-danger">Mỹ</span>
                                    @endif
                                </td>
                                @php
                                    $customer = GroupProduct::get_customer_with_service($hosting->id, 'hosting');
                                @endphp
                                @if(!empty($customer))
                                    @if(!empty($customer->customer_name))
                                      <td>{{ $customer->ma_customer }} - {{ $customer->customer_name }}</td>
                                    @else
                                      <td>{{ $customer->ma_customer }} - {{ $customer->customer_tc_name }}</td>
                                    @endif
                                @else
                                    <td>Chính tôi</td>
                                @endif
                                <td>@if (!empty($hosting->date_create))
                                        <span>{{ date('d-m-Y', strtotime($hosting->date_create)) }}</span>
                                    @else
                                        <span class="text-danger">Chưa tạo</span>
                                    @endif
                                </td>
                                <td class="next_due_date">
                                    @if (!empty($hosting->next_due_date))
                                        <span @if($isExpire) class="text-danger" @endif>{{ date('d-m-Y', strtotime($hosting->next_due_date)) }}</span>
                                    @else
                                        <span class="text-danger">Chưa tạo</span>
                                    @endif
                                </td>
                                <td>{{ $billing[$hosting->billing_cycle] }}</td>
                                <td class="hosting-status">
                                    @if(!empty($hosting->status_hosting))
                                        @if ($hosting->status_hosting == 'on')
                                            <span class="text-success">Đang bật</span>
                                        @elseif ($hosting->status_hosting == 'off')
                                            <span class="text-success">Đang bật</span>
                                        @elseif ($hosting->status_hosting == 'addmin_off')
                                            <span class="text-danger">Admin tắt</span>
                                        @elseif ($hosting->status_hosting == 'cancel')
                                            <span class="text-danger">Đã hủy</span>
                                        @elseif ($hosting->status_hosting == 'delete')
                                            <span class="text-danger">Đã xóa</span>
                                        @else
                                            <span class="text-danger">Đã tắt</span>
                                        @endif
                                    @else
                                        <span class="text-danger">Chưa tạo</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <td class="text-center text-danger" colspan="9">
                            Không có dịch vụ Hosting được sử dụng
                        </td>
                    @endif
                    </tbody>
                    <tfoot class="card-footer clearfix" id="all_hosting">
                        <td colspan="9" class="text-center link-right">
                            {{ $hostings->links()  }}
                        </td>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
