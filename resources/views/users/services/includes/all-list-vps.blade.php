@php
use Carbon\Carbon;
@endphp
<div class="list_all_vps text_list_vps" id="vps">
    <div class="box box-default">
        <div class="box-body table-responsive">
            <div class="dataTables_wrapper form-inline dt-bootstrap">
                <div class="col-md-12 my-2">
                    <div class="row">
                        <div class="col-md-2 title_list_vps px-1 mb-2" style="max-width: 70px;">
                            <select class="form-control form-control-sm vps_all_classic">
                                <option value="20" selected>20</option>
                                <option value="30">30</option>
                                <option value="40">40</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                        </div>
    
                        <div class="col-md-6 mb-2 px-1 ml-auto">
                            <div class="input-group input-group-sm mb-0 mt-0">
                                <input type="text" id="form_search_multi" class="form-control  input-sm" placeholder="Tìm VPS theo 1 hoặc nhiều IP">
                                <div class="input-group-append">
                                    <button id="btn_all_search_multi" class="btn btn-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <table class="table table-bordered table-hover text-center">
                    <thead class="">
                        <th>IP</th>
                        <th>Cấu hình</th>
                        <th>Ngày tạo</th>
                        <th class="sort_next_due_date_all_vps" data-sort="ASC" data-toggle="tooltip" data-placement="top" title="Sắp xếp ngày kết thúc">
                            Ngày kết thúc <i class="fas fa-sort"></i>
                            <input type="hidden" class="sort_type" value="">
                        </th>
                        <th>Tổng thời gian thuê</th>
                        <th>Chu kỳ thanh toán</th>
                        <th>Ghi chú</th>
                        <th>Trạng thái</th>
                    </thead>
                    <tbody>
                        @if($list_vps->count() > 0)
                            @foreach ($list_vps as $vps)
                                @if ( !empty($vps->status_vps))
                                    @php
                                        $isExpire = false;
                                        $expired = false;
                                        if(!empty($vps->next_due_date)){
                                            $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                                            $date = date('Y-m-d');
                                            $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                                            if($next_due_date <= $date) {
                                                $isExpire = true;
                                            }
                                            $date_now = strtotime(date('Y-m-d'));
                                            if ($next_due_date < $date_now) {
                                                $expired = true;
                                            }
                                        }
                                        $total_time = '';
                                        if(!empty($vps->next_due_date)) {
                                            $create_date = new Carbon($vps->date_create);
                                            $next_due_date = new Carbon($vps->next_due_date);
                                            if ( $next_due_date->diffInYears($create_date) ) {
                                                $year = $next_due_date->diffInYears($create_date);
                                                $total_time = $year . ' Năm ';
                                                $create_date = $create_date->addYears($year);
                                                $month = $next_due_date->diffInMonths($create_date);
                                                //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                                                if ( $month ) {
                                                    $total_time .= $month . ' Tháng';
                                                }
                                            } else {
                                                $diff_month = $next_due_date->diffInMonths($create_date);
                                                $total_time = $diff_month . ' Tháng';
                                            }
                                        }
                                    @endphp
                                      <tr>
                                          <td class="ip_vps" data-ip="{{ !empty($vps->ip) ? $vps->ip : '' }}">
                                              @if (!empty($vps->ip))
                                                  <a href="{{ route('services.detail', $vps->id) }}?type=vps">{{$vps->ip}}</a>
                                              @else
                                                  <span class="text-danger">Chưa tạo</span>
                                              @endif
                                          </td>
                                          <td>
                                            @php
                                              $product = $vps->product;
                                              $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                              $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                              $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                              // Addon

                                              if (!empty($vps->addon_id)) {
                                                  $vps_config = $vps->vps_config;
                                                  if (!empty($vps_config)) {

                                                      $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                                                      $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                                                      $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                                                  }
                                              }
                                            @endphp
                                            {{ $cpu }} CPU - {{ $ram }} RAM - {{ $disk }} Disk
                                          </td>
                                          <td>
                                              @if (!empty($vps->date_create))
                                                  <span>{{ date('d-m-Y', strtotime($vps->date_create)) }}</span>
                                              @else
                                                  <span class="text-danger">Chưa tạo</span>
                                              @endif
                                          </td>
                                          <td class="next_due_date">
                                              @if (!empty($vps->next_due_date))
                                                  <span class="@if($isExpire || $expired) text-danger @endif" >{{ date('d-m-Y', strtotime($vps->next_due_date)) }}</span>
                                              @else
                                                  <span class="text-danger">Chưa tạo</span>
                                              @endif
                                          </td>
                                          <td>{{ $total_time }}</td>
                                          <td>
                                              {{ $billing[$vps->billing_cycle] }}
                                          </td>
                                          <td>
                                              <span class="text-description">
                                                @if(!empty($vps->description))
                                                    {{ substr($vps->description, 0, 40) }}
                                                @endif
                                              </span>
                                              <span>
                                                <!-- <button type="button" name="button" class="btn btn-secondary button_edit_customer" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></button> -->
                                                <a href="#" class="text-secondary ml-2 button_edit_description" data-id="{{ $vps->id }}"><i class="fas fa-edit"></i></a>
                                              </span>
                                          </td>
                                          <td class="vps-status">
                                              @if(!empty($vps->status_vps))
                                                  @if ($vps->status_vps == 'on')
                                                    <span class="text-success" data-id="{{ $vps->id }}">Đang bật</span>
                                                  @elseif ($vps->status_vps == 'progressing' ||$vps->status_vps == 'rebuild')
                                                    <span class="vps-progressing" data-id="{{ $vps->id }}">Đang tạo ...</span>
                                                  @elseif ($vps->status_vps == 'rebuild')
                                                    <span class="vps-progressing" data-id="{{ $vps->id }}">Đang cài lại ...</span>
                                                  @elseif ($vps->status_vps == 'change_ip')
                                                    <span class="vps-progressing" data-id="{{ $vps->id }}">Đang đổi IP ...</span>
                                                  @elseif ($vps->status_vps == 'expire')
                                                      <span class="text-danger" data-id="{{ $vps->id }}">Đã hết hạn</span>
                                                  @elseif ($vps->status_vps == 'suspend')
                                                      <span class="text-danger" data-id="{{ $vps->id }}">Đang bị khoá</span>
                                                  @elseif ($vps->status_vps == 'admin_off')
                                                    <span class="text-danger" data-id="{{ $vps->id }}">Admin tắt</span>
                                                  @elseif ($vps->status_vps == 'delete_vps')
                                                    <span class="text-danger" data-id="{{ $vps->id }}">Đã xóa</span>
                                                  @elseif ($vps->status_vps == 'change_user')
                                                    <span class="text-danger" data-id="{{ $vps->id }}">Đã chuyển</span>
                                                  @elseif ($vps->status_vps == 'cancel')
                                                    <span class="text-danger" data-id="{{ $vps->id }}">Đã hủy</span>
                                                  @else
                                                    <span class="text-danger" data-id="{{ $vps->id }}">Đã tắt</span>
                                                  @endif
                                              @else
                                                  <span class="text-danger" data-id="{{ $vps->id }}">Chưa được tạo</span>
                                              @endif
                                          </td>
                                      </tr>
                                    @endif
                                @endforeach
                            @else
                                <td class="text-center text-danger" colspan="10">
                                    Không có dịch vụ VPS được sử dụng
                                </td>
                            @endif
                    </tbody>
                    <tfoot class="card-footer clearfix">
                        <td colspan="10" class="text-center link-right">
                            {{ $list_vps->links()  }}
                        </td>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
