@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Dịch vụ Server xóa - hủy - chuyển</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-cloud"></i> Dịch vụ Server</li>
<li class="breadcrumb-item active"> Xóa / Hủy / Chuyển</li>
@endsection
@section('content')
<div class="row">
    <div class="col col-md-12">
        <div class="col-md-12 detail-course-finish">
            @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
            @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
            @endif
        </div>
        {{-- List VPS --}}
        @php
        use Carbon\Carbon;
        @endphp
        <div class="list_server text_list_vps" id="server">
            <div class="box box-primary">
                <div class="box-body table-responsive">
                    <div class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="col-md-12 my-2">
                          <div class="row">
                              <div class="col-md-2 title_list_vps px-1 mb-2" style="max-width: 70px;">
                                  <select class="form-control form-control-sm server_cancel_classic">
                                  <option value="20" selected>20</option>
                                  <option value="30">30</option>
                                  <option value="40">40</option>
                                  <option value="50">50</option>
                                  <option value="100">100</option>
                                  </select>
                              </div>
                              <div class="col-md-4 px-1 mb-2" style="max-width: 240px;">
                              </div>
                              {{-- server_search --}}
                              <div class="col-md-6 mb-2 px-1 ml-auto">
                                      <div class="input-group input-group-sm mb-0 mt-0">
                                          <input type="text" id="form_search_multi" class="form-control  input-sm" placeholder="Tìm Server theo 1 hoặc nhiều IP">
                                          <div class="input-group-append">
                                              <button id="btn_server_cancel_search_multi" class="btn btn-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                          </div>
                                      </div>
                              </div>
                              {{-- <div class="col-md-12 text-primary">
                                  <b>
                                      Đã chọn: 
                                      <span class="qtt-checkbox">0</span> / <span class="total-item">{{ $servers->count() }}</span>
                                  </b>
                              </div> --}}
                            </div>
                        </div>
                        <table class="table table-sm table-bordered table-hover text-center">
                            <thead>
                                {{-- <th width="3%">
                                  <input type="checkbox" class="checkbox_server_all" data-toggle="tooltip" data-placement="top" title="Chọn tất cả">
                                </th> --}}
                                <th width="10%">IP</th>
                                <th width="22%">Cấu hình</th>
                                <th width="7%">Datacenter</th>
                                <th width="7%">Ngày tạo</th>
                                <th width="12%" class="sort_server_cancel" data-sort="" data-toggle="tooltip" data-placement="top" title="Sắp xếp ngày kết thúc">
                                  Ngày kết thúc <i class="fas fa-sort"></i>
                                  <input type="hidden" class="sort_type" value="">
                                </th>
                                <th width="7%">Tổng thời gian thuê</th>
                                <th width="7%">Chu kỳ thanh toán</th>
                                <th width="7%">Chi phí (VNĐ)</th>
                                <th width="5%">Ghi chú</th>
                                <th width="8%">Trạng thái</th>
                                {{-- <th width="13%">Hành động</th> --}}
                            </thead>
                            <tbody>
                            @if($servers->count() > 0)
                                @foreach ($servers as $server)
                                    @php
                                        $total_time = '';
                                        if(!empty($server->next_due_date)){
                                            $total_time = '';
                                            $create_date = new Carbon($server->date_create);
                                            $next_due_date = new Carbon($server->next_due_date);
                                            if ( $next_due_date->diffInYears($create_date) ) {
                                                $year = $next_due_date->diffInYears($create_date);
                                                $total_time = $year . ' Năm ';
                                                $create_date = $create_date->addYears($year);
                                                $month = $next_due_date->diffInMonths($create_date);
                                                //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                                                if ( $month ) {
                                                    $total_time .= $month . ' Tháng';
                                                }
                                            } else {
                                                $diff_month = $next_due_date->diffInMonths($create_date);
                                                $total_time = $diff_month . ' Tháng';
                                            }
                                        }
                                    @endphp
                                    <tr>
                                        {{-- <td><input type="checkbox" value="{{ $server->id }}"  data-ip="{{ $server->ip }}" class="server_checkbox"></td> --}}
                                        <td>
                                            @if (!empty($server->ip))
                                                <a href="{{ route( 'service.server_detail', $server->id ) }}">{{ $server->ip }}</a> <br>
                                                <a href="{{ route( 'service.server_detail', $server->id ) }}">{{ $server->ip2 }}</a> <br>
                                            @else
                                                <span class="text-danger">Đang cài đặt</span>
                                            @endif
                                        </td>
                                        <!-- <td>
                                          @if ( !empty($server->user_name) )
                                              <span>{{ $server->user_name }}</span>
                                          @else
                                              <span class="text-danger">Đang cài đặt</span>
                                          @endif
                                        </td>
                                        <td>
                                          @if ( !empty($server->password) )
                                              <span>{{ $server->password }}</span>
                                          @else
                                              <span class="text-danger">Đang cài đặt</span>
                                          @endif
                                        </td> -->
                                        <td>
                                            @if ( !empty($server->config_text) )
                                                {!! $server->config_text !!}
                                            @else
                                                @php
                                                    $product = $server->product;
                                                    $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                                    $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
                                                    $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                                    $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                                @endphp
                                                {{ $cpu }} ({{ $cores }}), {{ $ram }},
                                                {{ $disk }} ({{ $server->raid }}) 
                                            @endif
                                            @php
                                                $addonConfig = '';
                                                $addonConfig = GroupProduct::get_config_server($server->id);
                                            @endphp
                                            <br>
                                            {!! $addonConfig !!}
                                        </td>
                                        <td>
                                          {{ $server->location }}
                                        </td>
                                        <td>
                                          @if (!empty($server->date_create))
                                              <span>{{ date('d-m-Y', strtotime($server->date_create)) }}</span>
                                          @else
                                              <span class="text-danger">Chưa tạo</span>
                                          @endif
                                        </td>
                                        <td>
                                          @if (!empty($server->next_due_date))
                                              <span >{{ date('d-m-Y', strtotime($server->next_due_date)) }}</span>
                                          @else
                                              <span>Chưa tạo</span>
                                          @endif
                                        </td>
                                        <td>{{ $total_time }}</td>
                                        <td>
                                            {{ $billing[$server->billing_cycle] }}
                                        </td>
                                        <td>
                                            @if ( !empty($server->amount) )
                                              {!!number_format( $server->amount ,0,",",".")!!}
                                            @else
                                                @php
                                                  $total = 0;
                                                  $product = $server->product;
                                                  $total = !empty( $product->pricing[$server->billing_cycle] ) ? $product->pricing[$server->billing_cycle] : 0;
                                                  if (!empty($server->server_config)) {
                                                    $server_config = $server->server_config;
                                                    $pricing_addon = 0;
                                                    if ( !empty( $server_config->ram ) ) {
                                                        foreach ($server->server_config_rams as $server_config_ram) {
                                                            $pricing_addon += !empty($server_config_ram->product->pricing[$server->billing_cycle]) ? $server_config_ram->product->pricing[$server->billing_cycle] : 0;
                                                        }
                                                    }
                                                    if ( !empty( $server_config->ip ) ) {
                                                        foreach ($server->server_config_ips as $server_config_ip) {
                                                            $pricing_addon += !empty($server_config_ip->product->pricing[$server->billing_cycle]) ? $server_config_ip->product->pricing[$server->billing_cycle] : 0;
                                                        }
                                                    }
                                                    if ( !empty($server_config->disk2) ) {
                                                      $pricing_addon += $server_config->product_disk2->pricing[$server->billing_cycle];
                                                    }
                                                    if ( !empty($server_config->disk3) ) {
                                                      $pricing_addon += $server_config->product_disk3->pricing[$server->billing_cycle];
                                                    }
                                                    if ( !empty($server_config->disk4) ) {
                                                      $pricing_addon += $server_config->product_disk4->pricing[$server->billing_cycle];
                                                    }
                                                    if ( !empty($server_config->disk5) ) {
                                                      $pricing_addon += $server_config->product_disk5->pricing[$server->billing_cycle];
                                                    }
                                                    if ( !empty($server_config->disk6) ) {
                                                      $pricing_addon += $server_config->product_disk6->pricing[$server->billing_cycle];
                                                    }
                                                    if ( !empty($server_config->disk7) ) {
                                                      $pricing_addon += $server_config->product_disk7->pricing[$server->billing_cycle];
                                                    }
                                                    if ( !empty($server_config->disk8) ) {
                                                      $pricing_addon += $server_config->product_disk8->pricing[$server->billing_cycle];
                                                    }
                                                    $total += $pricing_addon;
                                                  }
                                                @endphp
                                                {!!number_format( $total ,0,",",".")!!}
                                              @endif
                                        </td>
                                        <td>
                                            <span class="text-description">
                                              @if(!empty($server->description))
                                                  {{ substr($server->description, 0, 40) }}
                                              @endif
                                            </span>
                                            <span>
                                              <a href="#" class="text-secondary ml-2 button_edit_description" data-id="{{ $server->id }}" ><i class="fas fa-edit"></i></a>
                                            </span>
                                        </td>
                                        <td class="server-status">
                                            @if ($server->status_server == 'on')
                                                <span class="text-success" data-id="{{ $server->id }}">Đang bật</span>
                                            @elseif ($server->status_server == 'off')
                                                <span class="text-danger" data-id="{{ $server->id }}">Đã tắt</span>
                                            @elseif ($server->status_server == 'progressing')
                                                <span class="server-progressing" data-id="{{ $server->id }}">Đang cài đặt ...</span>
                                            @elseif ($server->status_server == 'expire')
                                                <span class="text-danger" data-id="{{ $server->id }}">Đã hết hạn</span>
                                            @elseif ($server->status_server == 'cancel')
                                                <span class="text-danger" data-id="{{ $server->id }}">Đã hủy</span>
                                            @elseif ($server->status_server == 'delete_server')
                                                <span class="text-danger" data-id="{{ $server->id }}">Đã xóa</span>
                                            @elseif ($server->status_server == 'change_user')
                                                <span class="text-danger" data-id="{{ $server->id }}">Đã chuyển</span>
                                            @else
                                                <span class="text-secondary">Đã hủy</span>
                                            @endif
                                        </td>
                                        {{-- <td>
                                            @if ($server->status_server != 'suspend')
                                                <button type="button" class="btn btn-sm btn-warning button-action-server expired" data-toggle="tooltip" data-placement="top" title="Gia hạn Server" data-action="expired" data-id="{{ $server->id }}" data-ip="{{ $server->ip }}"><i class="fas fa-plus-circle"></i></button>
                                                @if($server->status_server != 'cancel')
                                                    <button class="btn btn-sm btn-outline-danger button-action-server terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ Server" data-action="terminated" data-id="{{ $server->id }}" data-ip="{{ $server->ip }}"><i class="fa fa-ban" aria-hidden="true"></i></button>
                                                @endif
                                            @endif
                                        </td> --}}
                                    </tr>
                                @endforeach
                            @else
                                <td class="text-center text-danger" colspan="15">
                                    Không có dịch vụ Server được sử dụng
                                </td>
                            @endif
                            </tbody>
                            <tfoot class="card-footer clearfix">
                                <td colspan="15" class="text-center link-right">
                                    {{ $servers->links()  }}
                                </td>
                            </tfoot>
                        </table>
                    </div>
                    <div id="group_button">
                        <div class="table-responsive" style="display: block;">
                            <!-- <button class="btn btn-success btn-action-server btn-icon-split btn-sm mb-1" data-type="on">
                                <span class="icon"><i class="fa fa-toggle-on" aria-hidden="true"></i></span>
                                <span class="text">Bật Server</span>
                            </button>
                            <button class="btn btn-warning btn-action-server btn-icon-split btn-sm mb-1" data-type="off">
                                <span class="icon"><i class="fa fa-power-off" aria-hidden="true"></i></span>
                                <span class="text">Tắt Server</span>
                            </button> -->
                            <!-- <button class="btn bg-gradient-secondary btn-action-server btn-icon-split btn-sm mb-1 expired" data-type="expired">
                                <span class="icon"><i class="fas fa-plus-circle"></i></span>
                                <span class="text">Gia hạn Server</span>
                            </button>
                            <button class="btn btn-danger btn-action-server btn-icon-split btn-sm mb-1" data-type="delete">
                                <span class="icon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                                <span class="text">Hủy Server</span>
                            </button> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Kết thúc list VPS --}}
    </div>
</div>
{{-- Modal xoa invoice --}}
<div class="modal fade" id="modal-services">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-terminated" value="Xác nhận">
          <button type="button" class="btn btn-danger" id="button-rebuil">Xác nhận</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<!-- Model action từng services -->
<div class="modal fade" id="modal-service">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-service" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button-service">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>

@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/service.js') }}"></script>
@endsection
