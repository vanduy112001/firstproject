@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('/libraries/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('/libraries/toastr/toastr.min.css')}}">
@endsection
@section('title')
    <div class="text-primary">Chi tiết sản phẩm</div>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
    <li class="breadcrumb-item active"><i class="fas fa-server"></i> Dịch vụ</li>
    <li class="breadcrumb-item active"> Chi tiết sản phẩm</li>
@endsection
@section('content')
  <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Thông tin dịch vụ / sản phẩm</h3>
                </div>
                <div class="card-body row product-details clearfix">
                    @if($type == 'vps')
                        @include('users.services.includes.detail-vps',compact('detail','billings','payment_methods','isExpired'))
                    @elseif($type == 'server')
                        @include('users.services.includes.detail-server',compact('detail','billings','payment_methods','isExpired'))
                    @elseif($type == 'proxy')
                        @include('users.services.includes.detail-proxy',compact('detail','billings','payment_methods','isExpired'))
                    @elseif($type == 'hosting')
                        @include('users.services.includes.detail-hosting',compact('detail','billings','payment_methods','isExpired'))
                    @endif
                </div>
            </div>
            <div class="card mt-4">
                <div class="card-header">
                    <h3 class="card-title">Thông tin chi tiết @if($type == 'vps') VPS @elseif($type == 'server')
                            Server @elseif($type == 'hosting') Hosting @elseif($type == 'proxy') Proxy @endif</h3>
                </div>
                <div class="card-body row mb-4">
                    @if($type == 'vps' || $type == 'server')
                        <div class="col-md-12">
                            <h4>Thông tin {{ $type == 'vps'? 'VPS' : 'Server' }}</h4>
                        </div>
                        <div class="col-md-5 text-right">
                            <b>IP:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $detail->ip }}
                        </div>
                        <div class="col-md-5 text-right">
                            <b>Username:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $detail->user }}
                        </div>
                        @if( $detail->status_vps != 'delete_vps' && $detail->status_vps != 'change_user' && $detail->status_vps != 'cancel' )
                        <div class="col-md-5 text-right">
                            <b>Password:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ !empty($detail->password) ? $detail->password : '' }}
                        </div>
                        @endif
                        <div class="col-md-5 text-right">
                          <b>Hệ điều hành:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ !empty($list_os[$detail->os]) ? $list_os[$detail->os] : 'Windows Server 2012 R2' }}
                        </div>
                        @php
                            $product = $detail->product;
                        @endphp
                        <div class="col-md-5 text-right">
                            <b>CPU:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ !empty($detail->vps_config->cpu) ? $product->meta_product->cpu + $detail->vps_config->cpu : $product->meta_product->cpu }}
                        </div>

                        <div class="col-md-5 text-right">
                            <b>RAM:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ !empty($detail->vps_config->ram) ? $product->meta_product->memory + $detail->vps_config->ram : $product->meta_product->memory }}
                        </div>

                        <div class="col-md-5 text-right">
                            <b>DISK:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ !empty($detail->vps_config->disk) ? $product->meta_product->disk + $detail->vps_config->disk : $product->meta_product->disk }}
                        </div>
                        @php
                            $makh = $detail->detail_order->order->makh;

                            if(!empty($makh)){
                               $customer = GroupProduct::check_customer($makh);
                            }
                        @endphp
                        @if(!empty($customer))
                          <hr>
                          <div class="col-md-10 mt-4 text-center">
                              <h4>Thông tin gói khách hàng</h4>
                          </div>
                          <div class="col-md-5 text-right">
                              <b>Tên khách hàng: </b>
                          </div>
                          <div class="col-md-7 text-left">
                              <a href="{{ route('user.customer.detail', $customer->id) }}">{{ !empty($customer->customer_name) ? $customer->customer_name : $customer->customer_tc_name }}</a>
                          </div>
                        @endif
                        @if(!empty($detail->addon_id))
                            <hr>
                            <div class="col-md-10 mt-4 text-center">
                                <h4>Thông tin gói Addon</h4>
                            </div>
                            @php
                                $product_addons = GroupProduct::get_addon_product_private(Auth::user()->id);
                            @endphp
                            @foreach($product_addons as $product_addon)
                                @if(!empty($detail->vps_config->cpu) && $product_addon->meta_product->type_addon == 'addon_cpu' )
                                    <div class="col-md-5 text-right">
                                        <b>Tên sản phẩm: </b>
                                    </div>
                                    <div class="col-md-7 text-left">
                                        {{ $product_addon->name }}
                                    </div>
                                    <div class="col-md-5 text-right">
                                        <b>Giá: </b>
                                    </div>
                                    <div class="col-md-7 text-left">
                                        {!! number_format($product_addon->pricing[$detail->billing_cycle],0,",",".") . ' VNĐ' !!}
                                    </div>
                                    <div class="col-md-5 text-right">
                                        <b>Cấu hình tăng: </b>
                                    </div>
                                    <div class="col-md-7 text-left">
                                        {{ $detail->vps_config->cpu }} CPU
                                    </div>
                                    <hr>
                                @endif
                                @if(!empty($detail->vps_config->ram) && $product_addon->meta_product->type_addon == 'addon_ram' )
                                    <div class="col-md-5 text-right">
                                        <b>Tên sản phẩm: </b>
                                    </div>
                                    <div class="col-md-7 text-left">
                                        {{ $product_addon->name }}
                                    </div>
                                    <div class="col-md-5 text-right">
                                        <b>Giá: </b>
                                    </div>
                                    <div class="col-md-7 text-left">
                                        {!! number_format($product_addon->pricing[$detail->billing_cycle],0,",",".") . ' VNĐ' !!}
                                    </div>
                                    <div class="col-md-5 text-right">
                                        <b>Cấu hình tăng: </b>
                                    </div>
                                    <div class="col-md-7 text-left">
                                        {{ $detail->vps_config->ram }} RAM
                                    </div>
                                    <hr>
                                @endif
                                @if(!empty($detail->vps_config->disk) && $product_addon->meta_product->type_addon == 'addon_disk' )
                                    <div class="col-md-5 text-right">
                                        <b>Tên sản phẩm: </b>
                                    </div>
                                    <div class="col-md-7 text-left">
                                        {{ $product_addon->name }}
                                    </div>
                                    <div class="col-md-5 text-right">
                                        <b>Giá: </b>
                                    </div>
                                    <div class="col-md-7 text-left">
                                        {!! number_format($product_addon->pricing[$detail->billing_cycle],0,",",".") . ' VNĐ' !!}
                                    </div>
                                    <div class="col-md-5 text-right">
                                        <b>Cấu hình tăng: </b>
                                    </div>
                                    <div class="col-md-7 text-left">
                                        {{ $detail->vps_config->disk }} GB
                                    </div>
                                    <hr>
                                @endif
                            @endforeach
                        @endif
                        @if ($detail->location == 'cloudzone')
                            @if ( $detail->status_vps != 'cancel' )
                              @if ( $detail->status_vps != 'admin_off' )
                                @if ( $detail->status_vps != 'expire' )
                                  @if ( $detail->status_vps != 'delete_vps' )
                                      @if ( $detail->status_vps != 'change_user' )
                                        @if ( $detail->status_vps != 'suspend' )
                                          @if ( $detail->status_vps != 'progressing' )
                                            @if(!empty($detail->product))
                                              @if(!$detail->product->meta_product->product_special)
                                                <div class="col-md-10 button_add_config mt-4 text-center">
                                                    <a href="{{ route('user.addCardAddon', $detail->id) }}" class="btn btn-primary" >Thêm cấu hình</a>
                                                </div>
                                              @endif
                                            @endif
                                          @endif
                                        @endif
                                      @endif
                                  @endif
                                @endif
                              @endif
                            @endif
                        @endif
                    @elseif($type == 'proxy')
                        <div class="col-md-12">
                            <h4>Thông tin Proxy</h4>
                        </div>
                        <div class="col-md-5 text-right">
                            <b>IP:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $detail->ip }}
                        </div>
                        <div class="col-md-5 text-right">
                            <b>Port:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $detail->port }}
                        </div>
                        <div class="col-md-5 text-right">
                            <b>Username:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $detail->username }}
                        </div>
                        @if( $detail->status != 'delete' && $detail->status != 'change_user' && $detail->status != 'cancel' )
                        <div class="col-md-5 text-right">
                            <b>Password:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ !empty($detail->password) ? $detail->password : '' }}
                        </div>
                        @endif
                    @elseif($type == 'hosting')
                        <div class="col-md-5 text-right">
                            <b>IP Address:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            @if ($detail->location == 'vn')
                                @if(!empty($detail->server_hosting_id))
                                  {{ $detail->server_hosting->ip }}
                                @else
                                  103.95.196.23
                                @endif
                            @elseif ($detail->location == 'si')
                                @if(!empty($detail->server_hosting_id))
                                  {{ $detail->server_hosting->ip }}
                                @else
                                  172.104.56.184
                                @endif
                            @endif
                        </div>
                        <div class="col-md-5 text-right">
                            <b>Khu vực:</b>
                        </div>
                        <div class="col-md-7 text-left text-danger">
                            @if ($detail->location == 'vn')
                                Việt Nam
                            @elseif ($detail->location == 'si')
                                Singapore
                            @endif
                        </div>
                        <div class="col-md-5 text-right">
                            <b>Domain:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $detail->domain }}
                        </div>
                        <div class="col-md-5 text-right">
                            <b>Username:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $detail->user }}
                        </div>
                        <div class="col-md-5 text-right">
                            <b>Password:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ !empty($detail->password) ? $detail->password : '' }}
                        </div>
                        <!-- https://help.directadmin.com/item.php?id=61 -->
                        @if ( $detail->status_hosting != 'cancel' )
                          @if ( $detail->status_hosting != 'admin_off' )
                            @if ( $detail->status_hosting != 'expire' )
                              <div class="mt-4 col-md-11 text-center">
                                <a href="{{ route('services.login_directadmin', $detail->id) }}" target="_blank" class="btn btn-default" >Đăng nhập đến DirectAdmin</a>
                              </div>
                            @endif
                          @endif
                        @endif
                        @if(!empty($detail->addon_id))
                            <hr>
                            <div class="col-md-10 mt-4 text-center">
                                <h4>Thông tin gói Addon</h4>
                            </div>
                            @php
                                $product_addon = GroupProduct::get_product($detail->addon_id);
                            @endphp
                            <div class="col-md-5 text-right">
                                <b>Tên sản phẩm: </b>
                            </div>
                            <div class="col-md-7 text-left">
                                {{ $product_addon->name }}
                            </div>
                            <div class="col-md-5 text-right">
                                <b>Giá: </b>
                            </div>
                            <div class="col-md-7 text-left">
                                {{ !empty($product_addon->pricing[$detail->billing_cycle]) ? number_format($product_addon->pricing[$detail->billing_cycle],0,",",".") . ' VNĐ' : '0 VNĐ' }}
                            </div>
                            <div class="col-md-5 text-right">
                                <b>Cấu hình tăng: </b>
                            </div>
                            <div class="col-md-7 text-left">
                                @if(!empty($product_addon->meta_product->domain))
                                    {{ $product_addon->meta_product->domain * $detail->addon_qtt }} Domain
                                @elseif(!empty($product_addon->meta_product->database))
                                    {{ $product_addon->meta_product->database * $detail->addon_qtt }} Database
                                @elseif(!empty($product_addon->meta_product->storage))
                                    {{ $product_addon->meta_product->storage * $detail->addon_qtt }} Storage
                                @endif
                            </div>
                        @endif
                    @endif
                </div>
            </div>
            <input type="hidden" id="billing" value="{{ $detail->billing_cycle }}">
            <input type="hidden" id="id" value="{{ $detail->id }}">
            <div class="card mt-4" id="card_config">

            </div>
        </div>
    </div>
    <div class="card card-outline card-danger">
        <div class="card-header">
          <h3 class="card-title">Hoạt động</h3>
          <!-- /.card-tools -->
        </div>
        <div class="card-body">
          <table class="table table-bordered">
              <thead class="primary">
                  <th>Loại</th>
                  <th>Trạng thái</th>
                  <th>Thời gian</th>
              </thead>
              <tbody>
                @foreach ( $log_activities as $log_activity )
                    <tr>
                      <td>{{ $log_activity->action }}</td>
                      <td>{!! !empty($log_activity->description_user) ? $log_activity->description_user : '' !!}</td>
                      <td>{{ date('H:i:s d-m-Y', strtotime($log_activity->created_at) ) }}</td>
                    </tr>
                @endforeach
              </tbody>
              <tfoot class="card-footer clearfix">
                <td colspan="11" class="text-center link-right">
                    {{ $log_activities->links()  }}
                </td>
              </tfoot>
          </table>
        </div>
    </div>
    <div class="card card-outline card-success">
        <div class="card-header">
          <h3 class="card-title">Thanh toán</h3>
          <!-- /.card-tools -->
        </div>
        <div class="card-body table-responsive">
          <table class="table table-bordered">
              <thead class="primary">
                  <th>Loại</th>
                  <th>Trạng thái</th>
                  <th>Thời gian</th>
              </thead>
              <tbody>
                @foreach ( $log_payments as $log_payment )
                    <tr>
                      <td>{{ $log_payment->action }}</td>
                      <td>{!! !empty($log_activity->description_user) ? $log_activity->description_user : '' !!}</td>
                      <td>{{ date('H:i:s d-m-Y', strtotime($log_payment->created_at) ) }}</td>
                    </tr>
                @endforeach
              </tbody>
              <tfoot class="card-footer clearfix">
                <td colspan="11" class="text-center link-right">
                    {{ $log_payments->links()  }}
                </td>
              </tfoot>
          </table>
        </div>
    </div>
    {{-- Modal xoa invoice --}}
    <div class="modal fade" id="terminated_services">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Xóa đơn đặt hàng</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div id="notication-invoice" class="text-center">

                </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
              <input type="submit" class="btn btn-primary" id="button-terminated" value="Xác nhận">
            </div>
          </div>
          <!-- /.modal-content -->
      </div>
        <!-- /.modal-dialog -->
    </div>
      <!-- /.modal -->
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}"></script>
<script src="{{ asset('js/detail_service.js') }}"></script>
@endsection
