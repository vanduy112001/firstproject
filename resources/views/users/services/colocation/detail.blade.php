@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('/libraries/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('/libraries/toastr/toastr.min.css')}}">
@endsection
@section('title')
    <div class="text-primary">Chi tiết sản phẩm</div>
@endsection
@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
    <li class="breadcrumb-item active"><i class="fas fa-server"></i> Dịch vụ</li>
    <li class="breadcrumb-item active"> Chi tiết sản phẩm</li>
@endsection
@section('content')
  <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Thông tin dịch vụ / sản phẩm</h3>
                </div>
                <div class="card-body row product-details clearfix">
                  <div class="col-md-6">
                      @if ($detail->status_colo == 'on')
                          <div class="product-status product-status-on">
                              <div class="product-icon text-center">
                                  <span class="fa-stack fa-lg">
                                      <i class="fas fa-circle fa-stack-2x"></i>
                                      <i class="fas fa-server fa-stack-1x fa-inverse"></i>
                                  </span>
                                  <h3>{{ $detail->ip }}</h3>
                                  <h4>Loại {{ $detail->type_colo }}</h4>
                              </div>
                              <div class="product-status-text">
                                  @if($isExpired)
                                      Đã hết hạn
                                  @else
                                      Đang sử dụng
                                  @endif
                              </div>

                          </div>
                          <div class="row">
                              <div class="button_service col-md-12" style="max-width:70%;margin:auto;">
                                  <a href="{{ route('service.terminated', $detail->id) }}?type=colocation" class="btn btn-block btn-danger">Hủy dịch vụ</a>
                              </div>
                          </div>
                      @elseif  ($detail->status_colo == 'off')
                          <div class="product-status product-status-off">
                              <div class="product-icon text-center">
                                <span class="fa-stack fa-lg">
                                  <i class="fas fa-circle fa-stack-2x"></i>
                                  <i class="fas fa-database fa-stack-1x fa-inverse"></i>
                                </span>
                                <h3>{{ $detail->ip }}</h3>
                                <h4>{{ $detail->type }}</h4>
                              </div>
                              <div class="product-status-text">
                                  Đang tắt
                              </div>
                          </div>
                          <div class="row">
                              <div class="button_service col-md-12" style="max-width:70%;margin:auto;">
                                  <a href="{{ route('service.terminated', $detail->id) }}?type=colocation" class="btn btn-block btn-danger">Hủy dịch vụ</a>
                              </div>
                          </div>
                      @else
                          <div class="product-status product-status-cancelled">
                              <div class="product-icon text-center">
                                <span class="fa-stack fa-lg">
                                  <i class="fas fa-circle fa-stack-2x"></i>
                                  <i class="fas fa-database fa-stack-1x fa-inverse"></i>
                                </span>
                                <h3>{{ $detail->ip }}</h3>
                                <h4>{{ $detail->type }}</h4>
                              </div>
                              <div class="product-status-text">
                                  @if($detail->status_colo == 'cancel')
                                      Đợi hủy
                                  @else
                                      Đã hủy
                                  @endif
                              </div>
                          </div>
                      @endif
                  </div>

                  <div class="col-md-6 text-center mt-4 info-services">
                      <h4>Ngày tạo</h4>
                      {{ !empty($detail->date_create) ? date('d-m-Y', strtotime($detail->date_create)) : 'Chưa tạo'  }}
                      <h4>Chi phí</h4>
                      @php
                        $amount = 0;
                        if ( !empty($colocation->amount) ) {
                          $amount = $colocation->amount;
                        } else {
                          $product = $colocation->product;
                          $amount = !empty( $product->pricing[$colocation->billing_cycle] ) ? $product->pricing[$colocation->billing_cycle] : 0;
                          if ( !empty($colocation->colocation_config_ips) ) {
                            foreach ($colocation->colocation_config_ips as $key => $colocation_config_ip) {
                              $amount += !empty( $colocation_config_ip->product->pricing[$colocation->billing_cycle] ) ? $colocation_config_ip->product->pricing[$colocation->billing_cycle] : 0;
                            }
                          }
                        }
                      @endphp
                      {{ number_format($amount,0,",",".") . ' VNĐ' }}
                      <h4>Chu kỳ thanh toán</h4>
                      {{ !empty($billings[$detail->billing_cycle]) ? $billings[$detail->billing_cycle] : '' }}
                      @if(!empty($detail->next_due_date))
                          <h4>Ngày hết hạn</h4>
                          {{ date('d-m-Y', strtotime($detail->next_due_date)) }}
                      @endif
                      <h4>Hình thức thanh toán</h4>
                      {{ !empty($payment_methods[$detail->detail_order->payment_method]) ? $payment_methods[$detail->detail_order->payment_method] : 'Thanh toán bằng số dư tài khoản' }}
                  </div>
                </div>
            </div>
            <div class="card mt-4">
                <div class="card-header">
                    <h3 class="card-title">Thông tin chi tiết @if($type == 'vps') VPS @elseif($type == 'server')
                            Server @elseif($type == 'hosting') Hosting @elseif($type == 'colocation') Colocation @endif</h3>
                </div>
                <div class="card-body row mb-4">
                    <div class="col-md-10 text-center">
                        <h4>Thông tin Colocation</h4>
                    </div>
                    <div class="col-md-5 text-right">
                        <b>IP:</b>
                    </div>
                    <div class="col-md-7 text-left">
                        {{ $detail->ip }}
                    </div>
                    <div class="col-md-5 text-right">
                        <b>Loại:</b>
                    </div>
                    <div class="col-md-7 text-left">
                        {{ $detail->type_colo }}
                    </div>
                    <div class="col-md-5 text-right">
                        <b>Băng thông:</b>
                    </div>
                    <div class="col-md-7 text-left">
                        {{ !empty($detail->bandwidth) ? $detail->bandwidth : '' }}
                    </div>
                    <div class="col-md-5 text-right">
                        <b>Công xuất nguồn:</b>
                    </div>
                    <div class="col-md-7 text-left">
                        {{ !empty($detail->power) ? $detail->power : '' }}
                    </div>
                    <div class="col-md-5 text-right">
                        <b>Data Center:</b>
                    </div>
                    <div class="col-md-7 text-left">
                        {{ $detail->location }}
                    </div>
                    @if ( !empty($detail->colocation_config->ip) )
                        <hr>
                        <div class="col-md-10 mt-4 text-center">
                            <h4>Thông tin gói Addon</h4>
                        </div>
                        <div class="col-md-5 text-right">
                            <b>Addon IP:</b>
                        </div>
                        <div class="col-md-7 text-left">
                            {{ $detail->colocation_config->ip }}
                            <button type="button" class="ml-1 btn btn-sm btn-outline-success collapsed tooggle-plus" data-toggle="collapse" data-target="#service{{ $detail->id }}" data-placement="top" title="" data-original-title="Chi tiết" aria-expanded="false">
                                <i class="fas fa-plus"></i>
                            </button>
                            <div id="service{{ $detail->id }}" class="mt-2 collapse" style="">
                                @foreach ($detail->colocation_ips as $colocation_ip)
                                    {{ $colocation_ip->ip }} <br>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <div class="card card-outline card-danger">
                <div class="card-header">
                <h3 class="card-title">Hoạt động</h3>
                <!-- /.card-tools -->
                </div>
                <div class="card-body">
                <table class="table table-bordered">
                    <thead class="primary">
                        <th>Loại</th>
                        <th>Trạng thái</th>
                        <th>Thời gian</th>
                    </thead>
                    <tbody>
                        @foreach ( $log_activities as $log_activity )
                            <tr>
                            <td>{{ $log_activity->action }}</td>
                            <td>{!! !empty($log_activity->description_user) ? $log_activity->description_user : '' !!}</td>
                            <td>{{ date('H:i:s d-m-Y', strtotime($log_activity->created_at) ) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot class="card-footer clearfix">
                        <td colspan="11" class="text-center link-right">
                            {{ $log_activities->links()  }}
                        </td>
                    </tfoot>
                </table>
                </div>
            </div>
            <div class="card card-outline card-success">
                <div class="card-header">
                <h3 class="card-title">Thanh toán</h3>
                <!-- /.card-tools -->
                </div>
                <div class="card-body table-responsive">
                <table class="table table-bordered">
                    <thead class="primary">
                        <th>Loại</th>
                        <th>Trạng thái</th>
                        <th>Thời gian</th>
                    </thead>
                    <tbody>
                        @foreach ( $log_payments as $log_payment )
                            <tr>
                            <td>{{ $log_payment->action }}</td>
                            <td>{!! !empty($log_activity->description_user) ? $log_activity->description_user : '' !!}</td>
                            <td>{{ date('H:i:s d-m-Y', strtotime($log_payment->created_at) ) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot class="card-footer clearfix">
                        <td colspan="11" class="text-center link-right">
                            {{ $log_payments->links()  }}
                        </td>
                    </tfoot>
                </table>
                </div>
            </div>
            <input type="hidden" id="billing" value="{{ $detail->billing_cycle }}">
            <input type="hidden" id="id" value="{{ $detail->id }}">
            <div class="card mt-4" id="card_config">

            </div>
        </div>
    </div>
    {{-- Modal xoa invoice --}}
    <div class="modal fade" id="terminated_services">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Xóa đơn đặt hàng</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div id="notication-invoice" class="text-center">

                </div>
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
              <input type="submit" class="btn btn-primary" id="button-terminated" value="Xác nhận">
            </div>
          </div>
          <!-- /.modal-content -->
      </div>
        <!-- /.modal-dialog -->
    </div>
      <!-- /.modal -->
@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/bootstrap-input-spinner-master/src/bootstrap-input-spinner.js') }}"></script>
<script src="{{ asset('js/detail_service.js') }}"></script>
@endsection
