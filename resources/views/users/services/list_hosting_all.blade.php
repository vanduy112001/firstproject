@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
Tất cả dịch vụ Hosting
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-database"></i> Dịch vụ Hosting</li>
<li class="breadcrumb-item active"> Tất cả</li>
@endsection
@section('content')
@php
use Carbon\Carbon;
@endphp
<div class="row">
    <div class="col col-md-12">
        <div class="col-md-12 detail-course-finish">
            @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
            @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
            @endif
        </div>
        {{-- List Hosting --}}
        <div class="list_hosting text_list_vps" id="hosting">
            <div class="box box-default">
                <div class="box-body table-responsive">
                    <div class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="col-md-12 my-2">
                            <div class="row">
                                <div class="col-md-2 title_list_vps px-1 mb-2" style="max-width: 70px;">
                                    <select class="form-control form-control-sm vps_all_classic">
                                        <option value="20" selected>20</option>
                                        <option value="30">30</option>
                                        <option value="40">40</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                                <div class="col-md-4 mb-2 px-1 ml-auto">
                                    <div class="input-group input-group-sm mb-0 mt-0">
                                        <input type="text" id="hosting_search_all" class="form-control input-sm" placeholder="Tìm kiếm Hosting">
                                        <div class="input-group-append">
                                            <button class="btn btn-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
        
                            </div>
                        </div>
                        <table class="table table-sm table-bordered table-hover text-center">
                            <thead class="">
                                <th width="20%">Dịch vụ</th>
                                <th width="20%">Domain</th>
                                <th width="10%">Ngày tạo</th>
                                <th width="10%" class="sort_next_due_date_all" data-sort="" data-toggle="tooltip" data-placement="top" title="Sắp xếp ngày kết thúc">
                                    Ngày kết thúc <i class="fas fa-sort"></i>
                                    <input type="hidden" class="sort_hosting_type" value="">
                                </th>
                                <th width="10%">Tổng thời gian thuê</th>
                                <th width="10%">Chu kỳ thanh toán</th>
                                <th width="10%">Chi phí (đ)</th>
                                <th width="10%">Trạng thái</th>
                            </thead>
                            <tbody>
                            @if($hostings->count() > 0)
                                @foreach ($hostings as $hosting)
                                    @php
                                        $isExpire = false;
                                        $expired = false;
                                        if(!empty($hosting->next_due_date)){
                                            $next_due_date = strtotime(date('Y-m-d', strtotime($hosting->next_due_date)));
                                            $date = date('Y-m-d');
                                            $date = strtotime(date('Y-m-d', strtotime($date . '+15 Days')));
                                            if($next_due_date <= $date) {
                                              $isExpire = true;
                                            }
                                            $date_now = strtotime(date('Y-m-d'));
                                            if ($next_due_date < $date_now) {
                                                $expired = true;
                                            }
                                            $text_day = '';
                                            $now = Carbon::now();
                                            $next_due_date = new Carbon($hosting->next_due_date);
                                            $diff_date = $next_due_date->diffInDays($now);
                                            if ( $next_due_date->isPast() ) {
                                                $text_day = 'Hết hạn ' . $diff_date . ' ngày';
                                            } else {
                                                $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
                                            }
                                            $create_date = new Carbon($hosting->date_create);
                                            $next_due_date = new Carbon($hosting->next_due_date);
                                            if ( $next_due_date->diffInYears($create_date) ) {
                                                $year = $next_due_date->diffInYears($create_date);
                                                $total_time = $year . ' Năm ';
                                                $create_date = $create_date->addYears($year);
                                                $month = $next_due_date->diffInMonths($create_date);
                                                //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                                                if ( $month ) {
                                                    $total_time .= $month . ' Tháng';
                                                }
                                            } else {
                                                $diff_month = $next_due_date->diffInMonths($create_date);
                                                $total_time = $diff_month . ' Tháng';
                                            }
                                        }
                                    @endphp
                                    <tr>
                                        <td><a href="{{ route('services.detail', $hosting->id) }}?type=hosting">{{ $hosting->product->name }}</a></td>
                                        <td class="service-domain">
                                            @if (!empty($hosting->domain))
                                                <a href="{{ route('services.detail', $hosting->id) }}?type=hosting">{{$hosting->domain}}</a>
                                            @else
                                                <span class="text-danger">Chưa tạo</span>
                                            @endif
                                        </td>
                                        <td>@if (!empty($hosting->date_create))
                                                <span>{{ date('d-m-Y', strtotime($hosting->date_create)) }}</span>
                                            @else
                                                <span class="text-danger">Chưa tạo</span>
                                            @endif
                                        </td>
                                        <td class="next_due_date">
                                            @if (!empty($hosting->next_due_date))
                                                <span>{{ date('d-m-Y', strtotime($hosting->next_due_date)) }}</span>
                                            @else
                                                <span class="text-danger">Chưa tạo</span>
                                            @endif
                                        </td>
                                        <td> {{ $total_time }} </td>
                                        <td>{{ $billing[$hosting->billing_cycle] }}</td>
                                        <td>
                                            <?php
                                                $price_hosting = 0;
                                                if ( !empty($hosting->price_override) ) {
                                                    $price_hosting = $hosting->price_override;
                                                    if (!empty($hosting->order_upgrade_hosting)) {
                                                    foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                                                        $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                                                    }
                                                    }
                                                } 
                                                else {
                                                    $product = $hosting->product;
                                                    $price_hosting = !empty( $product->pricing[$hosting->billing_cycle] ) ? $product->pricing[$hosting->billing_cycle] : 0;
                                                }
                                            ?>
                                            {!!number_format( $price_hosting ,0,",",".")!!}
                                        </td>
                                        <td class="hosting-status">
                                            @if(!empty($hosting->status_hosting))
                                                @if ($hosting->status_hosting == 'on')
                                                    <span class="text-success">Đang bật</span>
                                                @elseif ($hosting->status_hosting == 'off')
                                                    <span class="text-danger">Đã tắt</span>
                                                @elseif ($hosting->status_hosting == 'expire')
                                                    <span class="text-danger">Hết hạn</span>
                                                @elseif ($hosting->status_hosting == 'addmin_off')
                                                    <span class="text-danger">Admin tắt</span>
                                                @elseif ($hosting->status_hosting == 'cancel')
                                                    <span class="text-danger">Đã hủy</span>
                                                @elseif ($hosting->status_hosting == 'delete')
                                                    <span class="text-danger">Đã xóa</span>
                                                @else
                                                    <span class="text-danger">Đã tắt</span>
                                                @endif
                                            @else
                                                <span class="text-danger">Chưa tạo</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                <td class="text-center text-danger" colspan="15">
                                    Không có dịch vụ Hosting được sử dụng
                                </td>
                            @endif
                            </tbody>
                            <tfoot class="card-footer clearfix">
                                <td colspan="15"  class="text-center link-right">
                                    {{ $hostings->links()  }}
                                </td>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>        
        {{-- Kết thúc list Hosting --}}
    </div>
</div>
<div class="modal fade" id="modal-services">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <input type="submit" class="btn btn-primary" id="button-terminated" value="Xác nhận">
          <button type="button" class="btn btn-danger" id="button-multi-finish">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<!-- Model action từng services -->
<div class="modal fade" id="modal-service">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-service" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
          <button type="button" class="btn btn-primary" id="button-service">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>

@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/service_hosting.js') }}"></script>
@endsection
