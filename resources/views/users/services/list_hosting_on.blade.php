@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
Dịch vụ Hosting còn hạn
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-database"></i> Dịch vụ Hosting</li>
<li class="breadcrumb-item active"> Còn hạn</li>
@endsection
@section('content')
<div class="row">
    <div class="col col-md-12">
        <div class="col-md-12 detail-course-finish">
            @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
            @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
            @endif
        </div>
        {{-- List Hosting --}}
        @include('users.services.includes.list-hosting',compact('hostings'))
        {{-- Kết thúc list Hosting --}}
    </div>
</div>
<div class="modal fade" id="modal-services">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Xóa đơn đặt hàng</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div id="notication-invoice" class="text-center">

          </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
        <input type="submit" class="btn btn-primary" id="button-terminated" value="Xác nhận">
        <button type="button" class="btn btn-danger" id="button-multi-finish">Hoàn thành</button>
      </div>
    </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Model action từng services -->
<div class="modal fade" id="modal-service">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Xóa đơn đặt hàng</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div id="notication-service" class="text-center">

          </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
        <button type="button" class="btn btn-primary" id="button-service">Xác nhận</button>
        <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
      </div>
    </div>
    <!-- /.modal-content -->
</div>
  <!-- /.modal-dialog -->
</div>

@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/service_hosting.js') }}"></script>
@endsection
