@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/icheck-bootstrap/icheck-bootstrap.min.css') }}">
@endsection
@section('title')
    Dịch vụ Proxy đang sử dụng
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-cloud"></i> Dịch vụ Proxy</li>
<li class="breadcrumb-item active"> Đang sử dụng</li>
@endsection
@section('content')
@php
use Carbon\Carbon;
@endphp
<div class="row">
    <div class="col col-md-12">
        <div class="col-md-12 detail-course-finish">
            @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
            @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
            @endif
        </div>
        {{-- List VPS --}}
        {{-- List VPS --}}
        <div class="list_vps text_list_vps" id="vps">
            <div class="box box-default">
                <div class="box-body table-responsive">
                    <div class="dataTables_wrapper form-inline dt-bootstrap">
                        <div class="col-md-12 my-2">
                            <div class="row">
                                <div class="col-md-2 title_list_vps px-1 mb-2" style="max-width: 70px;">
                                    <select class="form-control form-control-sm vps_on_classic">
                                      <option value="20" selected>20</option>
                                      <option value="30">30</option>
                                      <option value="40">40</option>
                                      <option value="50">50</option>
                                      <option value="100">100</option>
                                    </select>
                                </div>
    
                                <div class="col-md-4 px-1 mb-2" style="max-width: 240px;">
                                    <div class="input-group input-group-sm m-0">
                                        <select class="custom-select list_action" name="" id="">
                                            <option value="" selected disabled>Chọn hành động</option>
                                            {{-- <option value="on">Bật VPS</option>
                                            <option value="off">Tắt VPS</option> --}}
                                            <option value="expired">Gia hạn</option>
                                            {{-- <option value="rebuild">Rebuild VPS</option>
                                            <option value="change_ip">Đổi IP</option>
                                            <option value="reset_password">Reset password</option> --}}
                                            <option value="delete">Hủy</option>
                                            {{-- <option value="auto_refurn">Tự động gia hạn</option>
                                            <option value="off_auto_refurn">Tắt tự động gia hạn</option>
                                            <option value="change_user">Chuyển khách hàng</option>
                                            <option value="export_vps">Xuất file excel</option> --}}
                                        </select>
                                    </div>
                                </div>
    
                                <div class="col-md-6 mb-2 px-1 ml-auto">
                                    <div class="input-group input-group-sm mb-0 mt-0">
                                        <input type="text" id="form_search_multi" class="form-control  input-sm" placeholder="Tìm VPS theo 1 hoặc nhiều IP">
                                        <div class="input-group-append">
                                            <button id="btn_on_search_multi_on" class="btn btn-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 text-primary">
                                  <b>
                                      Đã chọn: 
                                      <span class="qtt-checkbox">0</span> / <span class="total-item">{{ $list_proxy->count() }}</span>
                                  </b>
                                </div>
                            </div>
                        </div>

                        <table class="table table-sm table-bordered table-hover text-center">
                          <thead class="">
                              <th width="3%"><input type="checkbox" class="checkbox_all" data-toggle="tooltip" data-placement="top" title="Chọn tất cả"></th>
                              <th width="9%">IP</th>
                              <th width="5%">Port</th>
                              <th width="12%">Tài khoản:Mật khẩu</th>
                              <th width="5%">Bang</th>
                              <th width="7%">Ngày tạo</th>
                              <th width="10%" class="sort_next_due_date_on" data-sort="" data-toggle="tooltip" data-placement="top" title="Sắp xếp ngày kết thúc">
                                  Ngày kết thúc <i class="fas fa-sort"></i>
                                  <input type="hidden" class="sort_type" value="">
                              </th>
                              <th width="7%">Tổng thời gian thuê</th>
                              <th width="7%">Chu kỳ thanh toán</th>
                              <th width="7%">Chi phí (VNĐ)</th>
                              <th width="5%">Ghi chú</th>
                              {{-- <th width="5%" class="infoAuto text-center">
                                Gia hạn
                                <i class="fas fa-info-circle">
                                  <span class="auto_refurn_tooltip">
                                    Bật tính năng tự động gia hạn sẽ gia hạn tự động khi VPS gần hết hạn.
                                  </span>
                                </i>
                              </th> --}}
                              <th width="5%">Trạng thái</th>
                              <th width="15%">Hành động</th>
                          </thead>
                          <tbody>
                              @if($list_proxy->count() > 0)
                                  @foreach ($list_proxy as $proxy)
                                      @if ( !empty($proxy->status) && $proxy->status != 'delete')
                                        @php
                                            $isExpire = false;
                                            $expired = false;
                                            $total_time = '';
                                            if(!empty($proxy->next_due_date)){
                                                $next_due_date = strtotime(date('Y-m-d', strtotime($proxy->next_due_date)));
                                                $date = date('Y-m-d');
                                                $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                                                if($next_due_date <= $date) {
                                                  $isExpire = true;
                                                }
                                                $date_now = strtotime(date('Y-m-d'));
                                                if ($next_due_date < $date_now) {
                                                    $expired = true;
                                                }
                                                $text_day = '';
                                                $now = Carbon::now();
                                                $next_due_date = new Carbon($proxy->next_due_date);
                                                $diff_date = $next_due_date->diffInDays($now);
                                                if ( $next_due_date->isPast() ) {
                                                  $text_day = 'Hết hạn ' . $diff_date . ' ngày';
                                                } else {
                                                  $text_day = 'Còn hạn ' . ($diff_date + 1) . ' ngày';
                                                }
                                                $total_time = '';
                                                $create_date = new Carbon($proxy->date_create);
                                                $next_due_date = new Carbon($proxy->next_due_date);
                                                if ( $next_due_date->diffInYears($create_date) ) {
                                                  $year = $next_due_date->diffInYears($create_date);
                                                  $total_time = $year . ' Năm ';
                                                  $create_date = $create_date->addYears($year);
                                                  $month = $next_due_date->diffInMonths($create_date);
                                                  //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
                                                  if ( $month ) {
                                                      $total_time .= $month . ' Tháng';
                                                  }
                                                } else {
                                                  $diff_month = $next_due_date->diffInMonths($create_date);
                                                  $total_time = $diff_month . ' Tháng';
                                                }
                                            }
                                        @endphp
                                            <tr>
                                                <td><input type="checkbox" value="{{ $proxy->id }}" data-ip="{{ $proxy->ip }}" class="checkbox"></td>
                                                <td class="ip" data-ip="{{ !empty($proxy->ip) ? $proxy->ip : '' }}">
                                                    @if (!empty($proxy->ip))
                                                        <a href="{{ route('services.detail', $proxy->id) }}?type=proxy">{{$proxy->ip}}</a>
                                                    @else
                                                        <span class="text-danger">Chưa tạo</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $proxy->port }}
                                                </td>
                                                <td>
                                                  <span class="user_vps">{{ !empty($proxy->username) ? $proxy->username : '' }}</span> <b>:</b>
                                                  <span class="pass_vps">{{ !empty($proxy->password) ? $proxy->password : '' }}</span>
                                                </td>
                                                <td>
                                                    @if( !empty($proxy->state) )
                                                      {{ $proxy->state }}
                                                    @else
                                                      Random
                                                    @endif
                                                </td>
                                                <td>
                                                    @if (!empty($proxy->created_at))
                                                        <span>{{ date('d-m-Y', strtotime($proxy->created_at)) }}</span>
                                                    @else
                                                        <span class="text-danger">Chưa tạo</span>
                                                    @endif
                                                </td>
                                                <td class="next_due_date">
                                                    @if (!empty($proxy->next_due_date))
                                                        <span class="@if($isExpire || $expired) text-danger @endif" >{{ date('d-m-Y', strtotime($proxy->next_due_date)) }}</span>
                                                        @if($expired)
                                                          - <span class="text-danger">{{ $text_day }}</span>
                                                        @elseif($isExpire)
                                                          - <span class="text-danger">{{ $text_day }}</span>
                                                        @else
                                                          - <span>{{ $text_day }}</span>
                                                        @endif
                                                    @else
                                                        <span class="text-danger">Chưa tạo</span>
                                                    @endif
                                                </td>
                                                <td>{{ $total_time }}</td>
                                                <td>
                                                    {{ $billing[$proxy->billing_cycle] }}
                                                </td>
                                                <td>
                                                   @php
                                                      $total = 0;
                                                      if ( !empty($proxy->price_override) ) {
                                                         $total = $proxy->price_override;
                                                      } else {
                                                          $product = $proxy->product;
                                                          $total = !empty( $product->pricing[$proxy->billing_cycle] ) ? $product->pricing[$proxy->billing_cycle] : 0;
                                                      }
                                                   @endphp
                                                   {!!number_format( $total ,0,",",".")!!}
                                                </td>
                                                <td>
                                                    <span class="text-description">
                                                      @if(!empty($proxy->description))
                                                          {{ substr($proxy->description, 0, 40) }}
                                                      @endif
                                                    </span>
                                                    <span>
                                                      <!-- <button type="button" name="button" class="btn btn-secondary button_edit_customer" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></button> -->
                                                      <a href="#" class="text-secondary ml-2 button_edit_description" data-id="{{ $proxy->id }}" data-toggle="tooltip" title="Ghi chú"><i class="fas fa-edit"></i></a>
                                                    </span>
                                                </td>
                                                {{-- <td class="auto_refurn text-center">
                                                  <div class="form-group" style="padding-left: 10px;" @if( !empty($proxy->auto_refurn) ) data-toggle="tooltip" data-placement="top" title="Bật" @else data-toggle="tooltip" data-placement="top" title="Tắt" @endif>
                                                    <div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
                                                      <input type="checkbox" class="custom-control-input btnAutoRefurn" id="customSwitch{{$proxy->id}}"
                                                        data-id="{{$proxy->id}}" data-type="vps"
                                                        @if( !empty($proxy->auto_refurn) ) checked @endif
                                                      >
                                                      <label class="custom-control-label" for="customSwitch{{$proxy->id}}"></label>
                                                    </div>
                                                  </div>
                                                </td> --}}
                                                <td class="vps-status">
                                                    @if( $proxy->status != 'Pending' )
                                                        @if ($proxy->status == 'on')
                                                          <span class="text-success" data-id="{{ $proxy->id }}">Đang bật</span>
                                                        @elseif ($proxy->status == 'progressing')
                                                          <span class="vps-progressing" data-id="{{ $proxy->id }}">Đang tạo...</span>
                                                        @elseif ($proxy->status == 'rebuild')
                                                          <span class="vps-progressing" data-id="{{ $proxy->id }}">Đang cài lại ...</span>
                                                        @elseif ($proxy->status == 'change_ip')
                                                          <span class="vps-progressing" data-id="{{ $proxy->id }}">Đang đổi IP...</span>
                                                        @elseif ($proxy->status == 'reset_password')
                                                          <span class="vps-progressing" data-id="{{ $proxy->id }}">Đang đăt lại mật khẩu...</span>
                                                        @elseif ($proxy->status == 'expire')
                                                            <span class="text-danger" data-id="{{ $proxy->id }}">Đã hết hạn</span>
                                                        @elseif ($proxy->status == 'suspend')
                                                            <span class="text-danger" data-id="{{ $proxy->id }}">Đang bị khoá</span>
                                                        @elseif ($proxy->status == 'admin_off')
                                                          <span class="text-danger" data-id="{{ $proxy->id }}">Admin tắt</span>
                                                        @else
                                                          <span class="text-danger" data-id="{{ $proxy->id }}">Đã tắt</span>
                                                        @endif
                                                    @else
                                                        <span class="text-danger" data-id="{{ $proxy->id }}">Chưa được tạo</span>
                                                    @endif
                                                </td>
                                                <td class="page-service-action text-left">
                                                    <button type="button" class="btn btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn Proxy" data-action="expired" data-id="{{ $proxy->id }}" data-ip="{{ $proxy->ip }}"><i class="fas fa-plus-circle"></i></button>
                                                    <button class="btn btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ Proxy" data-action="terminated" data-id="{{ $proxy->id }}" data-ip="{{ $proxy->ip }}"><i class="fa fa-ban" aria-hidden="true"></i></button>
                                                </td>
                                            </tr>
                                          @endif
                                      @endforeach
                                  @else
                                      <td class="text-center text-danger" colspan="14">
                                          Không có dịch vụ Proxy được sử dụng
                                      </td>
                                  @endif
                          </tbody>
                          <tfoot class="card-footer clearfix">
                              <td colspan="14" class="text-center link-right">
                                  {{ $list_proxy->links()  }}
                              </td>
                          </tfoot>
                        </table>
                    </div>

                    {{-- <div class="col-md-4 px-1 mb-2" style="max-width: 240px;">
                        <div class="input-group input-group-sm m-0">
                            <select class="custom-select list_action" name="" id="">
                                <option value="" selected disabled>Chọn hành động</option>
                                <option value="on">Bật VPS</option>
                                <option value="off">Tắt VPS</option>
                                <option value="expired">Gia hạn VPS</option>
                                <option value="rebuild">Rebuild VPS</option>
                                <option value="change_ip">Đổi IP</option>
                                <option value="reset_password">Reset password</option>
                                <option value="delete">Hủy VPS</option>
                                <option value="auto_refurn">Tự động gia hạn</option>
                                <option value="off_auto_refurn">Tắt tự động gia hạn</option>
                                <option value="change_user">Chuyển khách hàng</option>
                                <option value="export_vps">Xuất file excel</option>
                            </select>
                            <div class="input-group-append">
                                <button class="btn btn-secondary btn-action-vps" type="button">Áp dụng</button>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
        {{-- Kết thúc list VPS --}}
    </div>
</div>
{{-- Modal xoa invoice --}}
<div class="modal fade" id="modal-services">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <input type="submit" class="btn btn-primary" id="button-terminated" value="Xác nhận">
          <button type="button" class="btn btn-success" id="button-rebuil">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-multi-finish" data-link="" data-type="action" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<!-- Model action từng services -->
<div class="modal fade" id="modal-service">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-service" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <button type="button" class="btn btn-primary" id="button-service">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-finish" data-link="" data-type="action" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>

@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/service_proxy.js') }}?token={{ date('YmdH') }}"></script>
@endsection