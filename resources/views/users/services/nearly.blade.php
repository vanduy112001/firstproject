@extends('layouts.user.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
@endsection
@section('title')
<div class="text-primary">Dịch vụ VPS hủy / hết hạn</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-server"></i> Tất cả dịch vụ</li>
@endsection
@section('content')
<style media="screen">
  #float-menu {
    opacity: 0 !important;
  }
</style>
<div class="row">
    <div class="col col-md-12">
        <div class="button-list">
            <ul  class="nav nav-tabs" id="custom-tabs-two-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link btn btn-primary active btn-icon-split" id="custom-tabs-two-home-tab" data-toggle="pill" href="#vps">
                      <span class="icon text-white-50"><i class="fa fa-sitemap" aria-hidden="true"></i></span>
                      <span class="text">List VPS</span>
                    </a>
                </li>
                <li class="nav-item ml-2">
                    <a class="btn btn-danger nav-link btn-icon-split" id="custom-tabs-two-pricing-tab" data-toggle="pill" href="#hosting">
                      <span class="icon text-white-50"><i class="fa fa-globe" aria-hidden="true"></i></span>
                      <span class="text">List Hosting</span>
                    </a>
                </li>
            </ul>
            <!-- <div class="menu-vps">
                <button class="btn btn-primary btn-list-vps btn-icon-split">
                    <span class="icon text-white-50"><i class="fa fa-sitemap" aria-hidden="true"></i></span>
                    <span class="text">List VPS</span>
                </button>
            </div> -->
            <!-- <div class="menu-hosting">
                <button class="btn btn-danger btn-list-hosting btn-icon-split">
                    <span class="icon text-white-50"><i class="fa fa-globe" aria-hidden="true"></i></span>
                    <span class="text">List Hosting</span>
                </button>
            </div> -->
            <!-- <div class="menu-server">
                <button class="btn btn-info btn-list-server btn-icon-split">
                    <span class="icon text-white-50"><i class="fa fa-server" aria-hidden="true"></i> </span>
                    <span class="text">List Server</span>
                </button>
            </div> -->
        </div>
        {{-- List VPS --}}
        <div id="custom-tabs-two-tabContent" class="tab-content">
          <div class="list_vps_nearly tab-pane fade show active" id="vps" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
              <div class="box box-primary">
                  <div class="box-body table-responsive">
                      <div class="dataTables_wrapper form-inline dt-bootstrap">
                          <div class="col-md-12 detail-course-finish">
                              @if(session("success"))
                              <div class="bg-success">
                                  <p class="text-light">{{session("success")}}</p>
                              </div>
                              @elseif(session("fails"))
                              <div class="bg-danger">
                                  <p class="text-light">{{session("fails")}}</p>
                              </div>
                              @endif
                          </div>
                          <div class="col-md-12 row">
                            <div class="col-md-8 mt-2 title_list_vps">
                              <h3>Dịch vụ VPS hủy / hết hạn</h3>
                              <span class="ml-3">Số lượng: </span>
                              <select class="vp_nearly_classic">
                                <option value="20" selected>20 VPS</option>
                                <option value="30">30 VPS</option>
                                <option value="40">40 VPS</option>
                                <option value="50">50 VPS</option>
                                <option value="100">100 VPS</option>
                              </select>
                            </div>
                            <div class="col-md-4 mb-3">
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="Tìm kiếm VPS" id="vps_nearly_search">
                              </div>
                            </div>
                          </div>
                          <table class="table table-bordered table-hover">
                              <thead class="primary">
                                  <th>IP</th>
                                  <th>Cấu hình</th>
                                  <th>Loại</th>
                                  <th>Khách hàng</th>
                                  <th>Ngày tạo</th>
                                  <th>Ngày kết thúc</th>
                                  <th>Thời gian thuê</th>
                                  <th>Ghi chú</th>
                                  <th>Trạng thái</th>
                                  <th>Hành động</th>
                              </thead>
                              <tbody>
                                  @if(isset($list_vps))
                                      @foreach ($list_vps as $vps)
                                          @if ( !empty($vps->status_vps) )
                                            @if(!empty($vps->product))
                                                  @php
                                                      $isExpire = false;
                                                      $expired = false;
                                                      if(!empty($vps->next_due_date)){
                                                          $next_due_date = strtotime(date('Y-m-d', strtotime($vps->next_due_date)));
                                                          $date = date('Y-m-d');
                                                          $date = strtotime(date('Y-m-d', strtotime($date . '+7 Days')));
                                                          if($next_due_date <= $date) {
                                                            $isExpire = true;
                                                          }
                                                          $date_now = strtotime(date('Y-m-d'));
                                                          if ($next_due_date < $date_now) {
                                                              $expired = true;
                                                          }
                                                      }
                                                  @endphp
                                                  <tr>
                                                      <td class="ip_vps" data-ip="{{ !empty($vps->ip) ? $vps->ip : '' }}">
                                                          @if (!empty($vps->ip))
                                                              <a href="{{ route('services.detail', $vps->id) }}?type=vps">{{$vps->ip}}</a>
                                                              @if($expired)
                                                                - <span class="text-danger">Hết hạn</span>
                                                              @elseif($isExpire)
                                                                - <span class="text-danger">Gần hết hạn</span>
                                                              @endif
                                                          @else
                                                              <span class="text-danger">Chưa tạo</span>
                                                          @endif
                                                      </td>
                                                      <td>
                                                        @php
                                                          $product = $vps->product;
                                                          $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
                                                          $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
                                                          $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
                                                          // Addon

                                                          if (!empty($vps->addon_id)) {
                                                              $vps_config = $vps->vps_config;
                                                              if (!empty($vps_config)) {

                                                                  $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                                                                  $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                                                                  $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                                                              }
                                                          }
                                                        @endphp
                                                        {{ $cpu }} CPU - {{ $ram }} RAM - {{ $disk }} Disk
                                                      </td>
                                                      <td>
                                                          @if ( $vps->type_vps == 'vps' )
                                                            VPS
                                                          @else
                                                            NAT VPS
                                                          @endif
                                                      </td>
                                                      <td>
                                                          @if(!empty($vps->ma_kh))
                                                              @if(!empty($vps->customer))
                                                                  @if(!empty($vps->customer->customer_name))
                                                                    {{ $vps->customer->ma_customer }} - {{ $vps->customer->customer_name }}
                                                                  @else
                                                                    {{ $vps->customer->ma_customer }} - {{ $vps->customer->customer_tc_name }}
                                                                  @endif
                                                              @else
                                                                  Chính tôi
                                                              @endif
                                                          @else
                                                              @php
                                                                  $customer = GroupProduct::get_customer_with_service($vps->id, 'vps');
                                                              @endphp
                                                              @if(!empty($customer))
                                                                  @if(!empty($customer->customer_name))
                                                                    {{ $customer->ma_customer }} - {{ $customer->customer_name }}
                                                                  @else
                                                                    {{ $customer->ma_customer }} - {{ $customer->customer_tc_name }}
                                                                  @endif
                                                              @else
                                                                  Chính tôi
                                                              @endif
                                                          @endif
                                                          <span>
                                                            <!-- <button type="button" name="button" class="btn btn-secondary button_edit_customer" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></button> -->
                                                            <a href="#" class="text-secondary ml-2 button_edit_customer" data-id="{{ $vps->id }}" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></a>
                                                          </span>
                                                      </td>
                                                      <td>
                                                          @if (!empty($vps->date_create))
                                                              <span>{{ date('d-m-Y', strtotime($vps->date_create)) }}</span>
                                                          @else
                                                              <span class="text-danger">Chưa tạo</span>
                                                          @endif
                                                      </td>
                                                      <td class="next_due_date">
                                                          @if (!empty($vps->next_due_date))
                                                              <span class="@if($isExpire || $expired) text-danger @endif" >{{ date('d-m-Y', strtotime($vps->next_due_date)) }}</span>
                                                          @else
                                                              <span class="text-danger">Chưa tạo</span>
                                                          @endif
                                                      </td>
                                                      <td>
                                                          {{ $billing[$vps->billing_cycle] }}
                                                      </td>
                                                      <td>
                                                          <span class="text-description">
                                                            @if(!empty($vps->description))
                                                                {{ substr($vps->description, 0, 40) }}
                                                            @endif
                                                          </span>
                                                          <span>
                                                            <!-- <button type="button" name="button" class="btn btn-secondary button_edit_customer" data-toggle="tooltip" title="Đổi khách hàng"><i class="fas fa-edit"></i></button> -->
                                                            <a href="#" class="text-secondary ml-2 button_edit_description" data-id="{{ $vps->id }}" data-toggle="tooltip" title="Ghi chú"><i class="fas fa-edit"></i></a>
                                                          </span>
                                                      </td>
                                                      <td class="vps-status">
                                                          @if(!empty($vps->status_vps))
                                                              @if ($vps->status_vps == 'on')
                                                                <span class="text-success" data-id="{{ $vps->id }}">Đang bật</span>
                                                              @elseif ($vps->status_vps == 'progressing' ||$vps->status_vps == 'rebuild')
                                                                <span class="vps-progressing" data-id="{{ $vps->id }}">Đang tạo ...</span>
                                                              @elseif ($vps->status_vps == 'rebuild')
                                                                <span class="vps-progressing" data-id="{{ $vps->id }}">Đang cài lại ...</span>
                                                              @elseif ($vps->status_vps == 'change_ip')
                                                                <span class="vps-progressing" data-id="{{ $vps->id }}">Đang đổi IP ...</span>
                                                              @elseif ($vps->status_vps == 'expire')
                                                                  <span class="text-danger" data-id="{{ $vps->id }}">Đã hết hạn</span>
                                                              @elseif ($vps->status_vps == 'admin_off')
                                                                  <span class="text-danger" data-id="{{ $vps->id }}">Admin tắt</span>
                                                              @elseif ($vps->status_vps == 'delete_vps')
                                                                  <span class="text-danger" data-id="{{ $vps->id }}">Đã xóa</span>
                                                              @elseif ($vps->status_vps == 'cancel')
                                                                  <span class="text-danger" data-id="{{ $vps->id }}">Đã hủy</span>
                                                              @else
                                                                <span class="text-danger" data-id="{{ $vps->id }}">Đã tắt</span>
                                                              @endif
                                                          @else
                                                              <span class="text-danger" data-id="{{ $vps->id }}">Chưa được tạo</span>
                                                          @endif
                                                      </td>

                                                      <td class="page-service-action">
                                                          @if ($vps->status_vps != 'delete_vps')
                                                            @if ($isExpire)
                                                                <button type="button" class="btn btn-warning button-action-vps expired" data-toggle="tooltip" data-placement="top" title="Gia hạn VPS" data-action="expired" data-id="{{ $vps->id }}" data-ip="{{ $vps->ip }}"><i class="fas fa-plus-circle"></i></button>
                                                            @endif
                                                            @if($vps->status_vps != 'cancel')
                                                              @if ($vps->status_vps != 'expire')
                                                                <button class="btn btn-outline-warning button-action-vps on" @if($vps->status_vps == 'off' || $vps->status_vps == 'cancel') disabled @endif data-toggle="tooltip" data-placement="top" title="Tắt VPS" data-action="off" data-id="{{ $vps->id }}" data-ip="{{ $vps->ip }}"><i class="fas fa-power-off"></i></button>
                                                                <button class="btn btn-outline-info button-action-vps off" @if($vps->status_vps == 'on' || $vps->status_vps == 'cancel') disabled @endif data-toggle="tooltip" data-placement="top" title="Bật vps" data-action="on" data-id="{{ $vps->id }}" data-ip="{{ $vps->ip }}"><i class="far fa-stop-circle"></i></button>
                                                                <button class="btn btn-outline-success button-action-vps restart" @if($vps->status_vps == 'off' || $vps->status_vps == 'cancel') disabled @endif data-toggle="tooltip" data-placement="top" title="Khởi động lại VPS" data-action="restart" data-id="{{ $vps->id }}" data-ip="{{ $vps->ip }}"><i class="fas fa-sync-alt"></i></button>
                                                                <button class="btn btn-outline-danger button-action-vps terminated" data-toggle="tooltip" data-placement="top" title="Hủy dịch vụ VPS" data-action="terminated" data-id="{{ $vps->id }}" data-ip="{{ $vps->ip }}"><i class="fa fa-ban" aria-hidden="true"></i></button>
                                                              @endif
                                                            @endif
                                                          @endif
                                                      </td>
                                                  </tr>
                                            @endif
                                          @endif
                                      @endforeach
                                  @else
                                      <td class="text-center text-danger" colspan="8">
                                              Không có dịch vụ VPS nào hết hạn
                                      </td>
                                  @endif
                              </tbody>
                              <tfoot class="card-footer clearfix">
                                  <td colspan="11" class="text-center">
                                      {{ $list_vps->links()  }}
                                  </td>
                              </tfoot>
                          </table>
                      </div>
                      <!-- <div id="group_button">
                          <div class="table-responsive" style="display: block;">
                              <button class="btn btn-success btn-action-vps btn-icon-split btn-sm mb-1" data-type="on">
                                  <span class="icon"><i class="fa fa-toggle-on" aria-hidden="true"></i> </span>
                                  <span class="text"> Bật VPS</span>
                              </button>
                              <button class="btn btn-warning btn-action-vps btn-icon-split btn-sm mb-1" data-type="off">
                                  <span class="icon"><i class="fa fa-power-off" aria-hidden="true"></i> </span>
                                  <span class="text"> Tắt VPS</span>
                              </button>
                              <button class="btn btn-info btn-action-vps btn-icon-split btn-sm mb-1" data-type="restart">
                                  <span class="icon"><i class="fas fa-sync-alt"></i> </span>
                                  <span class="text">Khởi động lại VPS</span>
                              </button>
                              <button class="btn bg-gradient-secondary btn-action-vps btn-icon-split btn-sm mb-1 expired" data-type="expired">
                                  <span class="icon"><i class="fas fa-plus-circle"></i></span>
                                  <span class="text">Gia hạn VPS</span>
                              </button>
                              <button class="btn btn-danger btn-action-vps btn-icon-split btn-sm mb-1" data-type="delete">
                                  <span class="icon"><i class="fa fa-ban" aria-hidden="true"></i> </span>
                                  <span class="text"> Hủy VPS</span>
                              </button>
                              <button class="btn btn-default btn-action-vps btn-icon-split btn-sm mb-1" data-type="rebuild">
                                  <span class="icon"><i class="fas fa-history"></i></span>
                                  <span class="text"> Rebuild VPS</span>
                              </button>
                              <button class="btn bg-purple btn-action-vps btn-icon-split btn-sm mb-1" data-type="change_ip">
                                  <span class="icon"><i class="fas fa-exchange-alt"></i></span>
                                  <span class="text"> Đổi IP VPS</span>
                              </button>
                          </div>
                      </div> -->
                  </div>
              </div>
          </div>

        {{-- Kết thúc list VPS --}}
        {{-- List Hosting --}}

          <div class="list_hosting_nearly tab-pane fade" id="hosting" role="tabpanel" aria-labelledby="custom-tabs-two-pricing-tab">
              <div class="box box-primary">
                  <div class="box-body table-responsive">
                      <div class="dataTables_wrapper form-inline dt-bootstrap">
                          <div class="col-md-12 row mt-4 mb-3">
                            <div class="col-md-8 mt-2 title_list_vps">
                              <span class="ml-3">Số lượng: </span>
                              <select class="hosting_nearly_classic">
                                <option value="20" selected>20 Hosting</option>
                                <option value="30">30 Hosting</option>
                                <option value="40">40 Hosting</option>
                                <option value="50">50 Hosting</option>
                                <option value="100">100 Hosting</option>
                              </select>
                            </div>
                            <div class="col-md-4 mb-3">
                              <div class="input-group">
                                <div class="input-group-prepend">
                                  <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                                <input type="text" class="form-control" placeholder="Tìm kiếm Hosting" id="list_hosting_nearly_search">
                              </div>
                            </div>
                          </div>
                          <table class="table table-bordered table-hover">
                              <thead class="danger">
                                <!-- <th><input type="checkbox" class="hosting_checkbox_all" data-toggle="tooltip" data-placement="top" title="Chọn tất cả Hosting"></th> -->
                                <th>Dịch vụ</th>
                                <th>Domain</th>
                                <th>Khu vực</th>
                                <th>Khách hàng</th>
                                <th>Ngày tạo</th>
                                <th>Ngày kết thúc</th>
                                <th>Thời gian thuê</th>
                                <th>Trạng thái</th>
                                <th>Hành động</th>
                              </thead>
                              <tbody>
                              @if( count($hostings) > 0)
                                  @foreach ($hostings as $hosting)
                                      @php
                                          $isExpire = false;
                                          $expired = false;
                                          if(!empty($hosting->next_due_date)){
                                              $next_due_date = strtotime(date('Y-m-d', strtotime($hosting->next_due_date)));
                                              $date = date('Y-m-d');
                                              $date = strtotime(date('Y-m-d', strtotime($date . '+10 Days')));
                                              if($next_due_date <= $date) {
                                                $isExpire = true;
                                              }
                                              $date_now = strtotime(date('Y-m-d'));
                                              if ($next_due_date < $date_now) {
                                                  $expired = true;
                                              }
                                          }
                                      @endphp
                                      <tr>
                                          <td><a href="{{ route('services.detail', $hosting->id) }}?type=hosting">{{ $hosting->product->name }}</a></td>
                                          <td class="service-domain">
                                              @if (!empty($hosting->domain))
                                                  <a href="{{ route('services.detail', $hosting->id) }}?type=hosting">{{$hosting->domain}}</a>
                                                  @if($expired)
                                                    - <span class="text-danger">Hết hạn</span>
                                                  @elseif($isExpire)
                                                    - <span class="text-danger">Gần hết hạn</span>
                                                  @endif
                                              @else
                                                  <span class="text-danger">Chưa tạo</span>
                                              @endif
                                          </td>
                                          <td>
                                              @if ($hosting->location == 'vn')
                                                  <span class="text-danger">Việt Nam</span>
                                              @elseif ($hosting->location == 'si')
                                                  <span class="text-danger">Singapore</span>
                                              @else
                                                  <span class="text-danger">Mỹ</span>
                                              @endif
                                          </td>
                                          @php
                                              $customer = GroupProduct::get_customer_with_service($hosting->id, 'hosting');
                                          @endphp
                                          @if(!empty($customer))
                                              @if(!empty($customer->customer_name))
                                                <td>{{ $customer->ma_customer }} - {{ $customer->customer_name }}</td>
                                              @else
                                                <td>{{ $customer->ma_customer }} - {{ $customer->customer_tc_name }}</td>
                                              @endif
                                          @else
                                              <td>Chính tôi</td>
                                          @endif
                                          <td>@if (!empty($hosting->date_create))
                                                  <span>{{ date('d-m-Y', strtotime($hosting->date_create)) }}</span>
                                              @else
                                                  <span class="text-danger">Chưa tạo</span>
                                              @endif
                                          </td>
                                          <td class="next_due_date">
                                              @if (!empty($hosting->next_due_date))
                                                  <span @if($isExpire) class="text-danger" @endif>{{ date('d-m-Y', strtotime($hosting->next_due_date)) }}</span>
                                              @else
                                                  <span class="text-danger">Chưa tạo</span>
                                              @endif
                                          </td>
                                          <td>{{ $billing[$hosting->billing_cycle] }}</td>
                                          <td class="hosting-status">
                                              @if(!empty($hosting->status_hosting))
                                                  @if ($hosting->status_hosting == 'on')
                                                      <span class="text-success">Đang bật</span>
                                                  @elseif ($hosting->status_hosting == 'cancel')
                                                      <span class="text-danger">Đã hủy</span>
                                                  @elseif ($hosting->status_hosting == 'admin_off')
                                                      <span class="text-danger">Admin tắt</span>
                                                  @elseif ($hosting->status_hosting == 'expire')
                                                      <span class="text-danger">Đã hết hạn</span>
                                                  @else
                                                      <span class="text-danger">Đã tắt</span>
                                                  @endif
                                              @else
                                                  <span class="text-danger">Chưa tạo</span>
                                              @endif
                                          </td>
                                          <td class="page-service-action">
                                              @if ($hosting->status_hosting != 'admin_off')
                                                  @if($hosting->status_hosting != 'cancel')
                                                    @if ($hosting->status_hosting == 'expire')
                                                        <button type="button" class="btn bg-gradient-secondary button-action-hosting expired" data-toggle="tooltip" data-placement="top" title="Gia hạn hosting" data-action="expired" data-id="{{ $hosting->id }}" data-domain="{{ $hosting->domain }}"><i class="fas fa-plus-circle"></i></button>
                                                    @endif
                                                  @endif
                                              @endif
                                          </td>
                                      </tr>
                                  @endforeach
                              @else
                                  <td class="text-center text-danger" colspan="9">
                                      Không có dịch vụ Hosting được sử dụng
                                  </td>
                              @endif
                              </tbody>
                              <tfoot class="card-footer clearfix">
                                  <td colspan="9" class="text-center">
                                      {{ $hostings->links()  }}
                                  </td>
                              </tfoot>
                          </table>
                      </div>
                      <!-- <div id="group_button">
                          <div class="table-responsive" style="display: block;">
                              <button class="btn btn-success btn-action-hosting btn-icon-split btn-sm mb-1" data-type="on">
                                  <span class="icon"><i class="far fa-play-circle"></i></span>
                                  <span class="text">Bật Hosting</span>
                              </button>
                              <button class="btn btn-warning btn-action-hosting btn-icon-split btn-sm mb-1" data-type="off">
                                  <span class="icon"><i class="far fa-stop-circle"></i></span>
                                  <span class="text">Tắt Hosting</span>
                              </button>
                              <button class="btn bg-gradient-secondary btn-action-hosting btn-icon-split btn-sm mb-1 expired" data-type="expired">
                                  <span class="icon"><i class="fas fa-plus-circle"></i></span>
                                  <span class="text">Gia hạn Hosting</span>
                              </button>
                              <button class="btn btn-danger btn-action-hosting btn-icon-split btn-sm mb-1" data-type="delete">
                                  <span class="icon"><i class="fa fa-ban" aria-hidden="true"></i></span>
                                  <span class="text">Hủy Hosting</span>
                              </button>
                          </div>
                      </div> -->
                  </div>
              </div>
          </div>
        </div>
        {{-- Kết thúc list Hosting --}}
        {{-- List Server --}}
        {{-- Kết thúc list Server --}}
    </div>
</div>
<div id="float-menu">
    <div class="menu-vps">
        <button class="btn btn-primary btn-list-vps"><i class="fa fa-sitemap" aria-hidden="true"></i> <span class="service-name">VPS</span></button>
    </div>
    <div class="menu-hosting">
        <button class="btn btn-danger btn-list-hosting"><i class="fa fa-globe" aria-hidden="true"></i> <span class="service-name">Hosting</span></button>
    </div>
    <div class="menu-server">
        <button class="btn btn-info btn-list-server"><i class="fa fa-server" aria-hidden="true"></i> <span class="service-name">Server</span></button>
    </div>
</div>
{{-- Modal xoa invoice --}}
<div class="modal fade" id="modal-services">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <input type="submit" class="btn btn-primary" id="button-terminated" value="Xác nhận">
          <button type="button" class="btn btn-danger" id="button-rebuil">Xác nhận</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<!-- Model action từng services -->
<div class="modal fade" id="modal-service">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-service" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <button type="button" class="btn btn-primary" id="button-service">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>

@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('js/service.js') }}"></script>
@endsection
