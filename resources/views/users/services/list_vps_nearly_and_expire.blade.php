@extends('layouts.user2.app')
@section('style')
<link rel="stylesheet" href="{{ asset('libraries/select2/css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('libraries/datatables-bs4/css/dataTables.bootstrap4.css') }}">
@endsection
@section('title')
<div class="text-primary">Dịch vụ VPS gần hết hạn và quá hạn</div>
@endsection
@section('breadcrumb')
<li class="breadcrumb-item"><a href="{{ route('index') }}"><i class="fas fa-tachometer-alt"></i> Trang chủ</a></li>
<li class="breadcrumb-item active"><i class="fas fa-cloud"></i> Dịch vụ VPS</li>
<li class="breadcrumb-item active"> Hết hạn / Hủy</li>
@endsection
@section('content')
<div class="row">
  <div class="col-md-12 ml-4">
    @php
        $group_products_vps = GroupProduct::get_group_product_vps_private(Auth::user()->id);
    @endphp
    @foreach ( $group_products_vps as $group )
      @if ($group->link == 'vps')
        <a href="{{ route('user.product.vps', $group->id) }}" class="btn btn-primary">Tạo VPS</a>
      @endif
    @endforeach
    @foreach ( $group_products_vps as $group )
      @if ($group->link == 'nat_vps')
        <a href="{{ route('user.product.vps', $group->id) }}" class="btn btn-success">Tạo NAT VPS</a>
      @endif
    @endforeach
  </div>
</div>
<div class="row">
    <div class="col col-md-12">
        <div class="col-md-12 detail-course-finish">
            @if(session("success"))
            <div class="bg-success">
                <p class="text-light">{{session("success")}}</p>
            </div>
            @elseif(session("fails"))
            <div class="bg-danger">
                <p class="text-light">{{session("fails")}}</p>
            </div>
            @endif
        </div>
        {{-- List VPS --}}
        <div id="custom-tabs-two-tabContent" class="tab-content">
          <div class="list_list_vps_nearly_and_expire text_list_vps tab-pane fade show active" id="vps" role="tabpanel" aria-labelledby="custom-tabs-two-home-tab">
              <div class="box box-primary">
                  <div class="box-body table-responsive">
                      <div class="dataTables_wrapper form-inline dt-bootstrap">
                          <div class="col-md-12 detail-course-finish">
                              @if(session("success"))
                              <div class="bg-success">
                                  <p class="text-light">{{session("success")}}</p>
                              </div>
                              @elseif(session("fails"))
                              <div class="bg-danger">
                                  <p class="text-light">{{session("fails")}}</p>
                              </div>
                              @endif
                          </div>
                          <div id="group_button">
                              <div class="table-responsive" style="display: block;">
                                  <button class="btn bg-gradient-secondary btn-action-vps btn-icon-split btn-sm mb-1 expired" data-type="expired">
                                      <span class="icon"><i class="fas fa-plus-circle"></i></span>
                                      <span class="text">Gia hạn</span>
                                  </button>
                                  <button class="btn btn-danger btn-action-vps btn-icon-split btn-sm mb-1" data-type="delete">
                                    <span class="icon"><i class="fa fa-ban" aria-hidden="true"></i> </span>
                                    <span class="text"> Hủy</span>
                                  </button>
                              </div>
                          </div>
                          <table class="table table-bordered table-hover" id="data-vps-table">
                              <thead class="primary">
                                  <th width="3%"><input type="checkbox" class="vps_checkbox_all" data-toggle="tooltip" data-placement="top" title="Chọn tất cả IP VPS"></th>
                                  <th>IP</th>
                                  <th>Cấu hình</th>
                                  <th>Loại</th>
                                  <th>Ngày tạo</th>
                                  <th>Ngày kết thúc</th>
                                  <th>Thời gian thuê</th>
                                  <th>Chi phí</th>
                                  <th>Trạng thái</th>
                                  <th>Hành động</th>
                              </thead>
                              <tbody>
                              </tbody>
                              <tfoot class="card-footer clearfix">
                              </tfoot>
                          </table>
                      </div>
                      <div id="group_button">
                          <div class="table-responsive" style="display: block;">
                              <button class="btn bg-gradient-secondary btn-action-vps btn-icon-split btn-sm mb-1 expired" data-type="expired">
                                  <span class="icon"><i class="fas fa-plus-circle"></i></span>
                                  <span class="text">Gia hạn VPS</span>
                              </button>
                              <button class="btn btn-danger btn-action-vps btn-icon-split btn-sm mb-1" data-type="delete">
                                <span class="icon"><i class="fa fa-ban" aria-hidden="true"></i> </span>
                                <span class="text"> Hủy</span>
                              </button>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

        {{-- Kết thúc list VPS --}}
    </div>
</div>
{{-- Modal xoa invoice --}}
<div class="modal fade" id="modal-services">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-invoice" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <input type="submit" class="btn btn-primary" id="button-terminated" value="Xác nhận">
          <button type="button" class="btn btn-danger" id="button-rebuil">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-multi-finish">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>
  <!-- /.modal -->
<!-- Model action từng services -->
<div class="modal fade" id="modal-service">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Xóa đơn đặt hàng</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div id="notication-service" class="text-center">

            </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
          <button type="button" class="btn btn-primary" id="button-service">Xác nhận</button>
          <button type="button" class="btn btn-danger" id="button-finish" data-dismiss="modal">Hoàn thành</button>
        </div>
      </div>
      <!-- /.modal-content -->
  </div>
    <!-- /.modal-dialog -->
</div>

@endsection
@section('scripts')
<script src="{{ asset('libraries/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('libraries/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('libraries/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('js/service_nearly_and_expire.js') }}"></script>
@endsection
