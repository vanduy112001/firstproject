const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .js('resources/js/products.js', 'public/js')
    .js('resources/js/edit_product.js', 'public/js')
    .js('resources/js/create_order.js', 'public/js')
    .js('resources/js/edit_order.js', 'public/js')
    .js('resources/js/edit_invoices.js', 'public/js')
    .js('resources/js/invoices.js', 'public/js')
    .js('resources/js/vps.js', 'public/js')
    .js('resources/js/admin_role.js', 'public/js')
    .js('resources/js/detailAdminRole.js', 'public/js')
    .js('resources/js/user.min.js', 'public/js')
    .js('resources/js/cutomer.js', 'public/js')
    .js('resources/js/user_order.js', 'public/js')
    .js('resources/js/product_privates.js', 'public/js')
    .js('resources/js/service.js', 'public/js')
    .js('resources/js/pay_in.js', 'public/js')
    .js('resources/js/payment_live.js', 'public/js')
    .js('resources/js/order.js', 'public/js')
    .js('resources/js/home.js', 'public/js')
    .js('resources/js/payments.js', 'public/js')
    .js('resources/js/all_page.js', 'public/js')
    .js('resources/js/home_admin.js', 'public/js')
    .js('resources/js/admin_create_product.js', 'public/js')
    .js('resources/js/detail_service.js', 'public/js')
    .js('resources/js/admin_edit_hosting.js', 'public/js')
    .js('resources/js/admin_hosting.js', 'public/js')
    .js('resources/js/payment_order.js', 'public/js')
    .js('resources/js/payment_domain_order.js', 'public/js')
    .js('resources/js/payment_domain_expired_order.js', 'public/js')
    .js('resources/js/payment_expired.js', 'public/js')
    .js('resources/js/payment_addon.js', 'public/js')
    .js('resources/js/group_user.js', 'public/js')
    .js('resources/js/detail_group_user.js', 'public/js')
    .js('resources/js/admin_edit_user.js', 'public/js')
    .js('resources/js/admin_hosting_da.js', 'public/js')
    .js('resources/js/admin_hosting_da_si.js', 'public/js')
    .js('resources/js/payment_change_ip_form.js', 'public/js')
    .js('resources/js/create_server_hosting.js', 'public/js')
    .js('resources/js/admin_server_hosting.js', 'public/js')
    .js('resources/js/user_choose_product.js', 'public/js')
    .js('resources/js/service_nearly_and_expire.js', 'public/js')
    .js('resources/js/admin_event.js', 'public/js')
    .js('resources/js/add_vps.js', 'public/js')
    .js('resources/js/payment_order_upgrade.js', 'public/js')
    .js('resources/js/server_nearly_and_expire.js', 'public/js')
    .js('resources/js/user_list_invoice.js', 'public/js')
    .js('resources/js/admin_tutorial.js', 'public/js')
    .js('resources/js/admin_log.js', 'public/js')
    .js('resources/js/user_log.js', 'public/js')
    .js('resources/js/admin_colo.js', 'public/js')
    .js('resources/js/admin_momo.js', 'public/js')
    .js('resources/js/hosting_nearly_and_expire.js', 'public/js')
    .js('resources/js/admin_email_hosting.js', 'public/js')
    .js('resources/js/admin_history_pay.js', 'public/js')
    .js('resources/js/email_hosting_nearly_and_expire.js', 'public/js')
    .js('resources/js/service_vps_us_nearly_and_expire.js', 'public/js')
    .js('resources/js/service_vps_us.js', 'public/js')
    .js('resources/js/colocation_nearly_and_expire.js', 'public/js')
    .js('resources/js/agency_api.js', 'public/js')
    .js('resources/js/vps_us.js', 'public/js')
    .js('resources/js/create_quotation.js', 'public/js')
    .js('resources/js/admin_message.js', 'public/js')
    .js('resources/js/admin_state.js', 'public/js')
    .js('resources/js/alert-pavietnam.js', 'public/js')
    .js('resources/js/editquotation.js', 'public/js')
    .js('resources/js/admin_send_mail.js', 'public/js')
    .js('resources/js/admin_email_marketing.js', 'public/js')
    .js('resources/js/admin_email_service.js', 'public/js')
    .js('resources/js/admin_contract.js', 'public/js')
    .js('resources/js/user_order_server.js', 'public/js')
    .js('resources/js/admin_server.js', 'public/js')
    .js('resources/js/admin_state_proxy.js', 'public/js')
    .js('resources/js/service_proxy.js', 'public/js')
    .js('resources/js/admin_state_cloudzone.js', 'public/js')
    .js('resources/js/admin_proxy.js', 'public/js')
    .js('resources/js/service_colo.js', 'public/js')
    .js('resources/js/service_hosting.js', 'public/js')
    .js('resources/js/user_order_colo.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/login.scss', 'public/css')
    .sass('resources/sass/usercss.scss', 'public/css')
    .sass('resources/sass/usercss2.scss', 'public/css')
    .sass('resources/sass/css.scss', 'public/css');



mix.copy("resources/images", "public/images");
mix.copy("resources/css", "public/css");
mix.copy("resources/libraries", "public/libraries");
mix.copy("resources/fonts", "public/fonts");
