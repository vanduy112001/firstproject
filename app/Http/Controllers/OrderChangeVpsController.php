<?php

namespace App\Http\Controllers;

use App\Model\OrderChangeVps;
use Illuminate\Http\Request;

class OrderChangeVpsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\OrderChangeVps  $orderChangeVps
     * @return \Illuminate\Http\Response
     */
    public function show(OrderChangeVps $orderChangeVps)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\OrderChangeVps  $orderChangeVps
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderChangeVps $orderChangeVps)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\OrderChangeVps  $orderChangeVps
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderChangeVps $orderChangeVps)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\OrderChangeVps  $orderChangeVps
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderChangeVps $orderChangeVps)
    {
        //
    }
}
