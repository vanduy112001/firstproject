<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\UserFactories;

class CustomerController extends Controller
{
    protected $order;
    protected $customer;
    public function __construct()
    {
        $this->customer = UserFactories::customerRepositories();
        $this->order = UserFactories::orderRepositories();
    }


    public function listCustomer()
    {
        $list_customers = $this->customer->listCustomer();
        return view('users.customers.index', compact('list_customers') );
    }

    public function create()
    {
        $list_city = config('city');
        return view( 'users.customers.create', compact('list_city') );
    }

    public function storeCustomer(Request $request)
    {
        $type_customer = $request->get('type_customer');
        if ($type_customer == 0) {
            $validate = $request->validate([
                'customer_name' => 'bail|required|min:6|max:60',
            ],
            [
                'customer_name.required' => 'Họ và tên người đăng ký không được để trống',
                'customer_name.min' => 'Họ và tên người đăng ký không được nhỏ hơn :min ký tự',
                'customer_name.max' => 'Họ và tên người đăng ký không được lớn hơn :max ký tự',
                'customer_gender.required' => 'Giới tính không được để trống',
                'customer_gender.not_in' => 'Giới tính không được để trống',
                'customer_date.required' => 'Ngày sinh không được để trống',
                'customer_cmnd.required' => 'Số chứng minh nhân dân không được để trống',
                'customer_cmnd.numeric' => 'Số chứng minh nhân dân phải là ký tự số',
                'customer_phone.numeric' => 'Số điện thoại phải là ký tự số',
                'customer_phone.required' => 'Số điện thoại không được để trống',
                'customer_email.required' => 'Địa chỉ email không được để trống',
                'customer_phone.numeric' => 'Số điện thoại phải là ký tự số',
                'customer_address.required' => 'Địa chỉ người đăng ký không được để trống',
                'customer_address.min' => 'Địa chỉ người đăng ký không được nhỏ hơn :min ký tự',
                'customer_city.required' => 'Tỉnh / Thành phố không được để trống',
                'customer_city.not_in' => 'Tỉnh / Thành phố không được để trống',
            ] );
        } else {
            $validate = $request->validate([
                'customer_tc_name' => 'bail|required|min:6|max:120',
                'customer_tc_mst' => 'bail|required|numeric',
                'customer_tc_dctc' => 'bail|required|min:6',
                'customer_tc_sdt' => 'bail|required|numeric',
                'customer_tc_city' => 'bail|required|not_in:0',
                'customer_dk_name' => 'bail|required|min:6|max:50',
                'customer_dk_date' => 'bail|required',
                'customer_dk_cv' => 'bail|required',
                'customer_dk_cmnd' => 'bail|required|numeric',
                'customer_dk_gender' => 'bail|required|not_in:0',
                'customer_dk_phone' => 'bail|required|numeric',
                'customer_dd_name' => 'bail|required|min:6|max:50',
                'customer_dd_date' => 'bail|required',
                'customer_dd_cv' => 'bail|required',
                'customer_dd_cmnd' => 'bail|required|numeric',
                'customer_dd_gender' => 'bail|required|not_in:0',
                'customer_dd_email' => 'bail|required',
                'customer_tt_name' => 'bail|required|min:6|max:50',
                'customer_tt_date' => 'bail|required',
                'customer_tt_cmnd' => 'bail|required|numeric',
                'customer_tt_gender' => 'bail|required|not_in:0',
                'customer_tt_email' => 'bail|required',
            ],
            [
                'customer_tc_name.required' => 'Tên tổ chức đăng ký không được để trống',
                'customer_tc_name.min' => 'Tên tổ chức đăng ký không được nhỏ hơn :min ký tự',
                'customer_tc_name.max' => 'Tên tổ chức đăng ký không được lớn hơn :max ký tự',
                'customer_tc_mst.required' => 'Mã số thuế không được để trống',
                'customer_tc_mst.numeric' => 'Mã số thuế phải là ký tự số',
                'customer_tc_dctc.required' => 'Địa chỉ cơ quan / tổ chức đăng ký không được để trống',
                'customer_tc_dctc.min' => 'Địa chỉ cơ quan / tổ chức đăng ký không được nhỏ hơn :min ký tự',
                'customer_tc_sdt.required' => 'Số điện thoại cơ quan / tổ chức không được để trống',
                'customer_tc_sdt.numeric' => 'Số điện thoại cơ quan / tổ chức phải là ký tự số',
                'customer_tc_city.required' => 'Tỉnh / Thành phố cơ quan / tổ chức không được để trống',
                'customer_tc_city.not_in' => 'Tỉnh / Thành phố cơ quan / tổ chức không được để trống',
                // kết thúc tổ chức
                'customer_dk_name.required' => 'Tên người đăng ký không được để trống',
                'customer_dk_name.min' => 'Tên người đăng đăng ký không được nhỏ hơn :min ký tự',
                'customer_dk_name.max' => 'Tên người đăng đăng ký không được lớn hơn :max ký tự',
                'customer_dk_gender.required' => 'Giới tính đăng người đăng ký không được để trống',
                'customer_dk_gender.not_in' => 'Giới tính người đăng ký không được để trống',
                'customer_dk_date.required' => 'Ngày sinh người đăng ký không được để trống',
                'customer_dk_cmnd.required' => 'Số chứng minh nhân dân người đăng ký không được để trống',
                'customer_dk_cmnd.numeric' => 'Số chứng minh nhân dân người đăng ký phải là ký tự số',
                'customer_dk_phone.numeric' => 'Số điện thoại người đăng ký phải là ký tự số',
                'customer_dk_phone.required' => 'Số điện thoại người đăng ký không được để trống',
                'customer_dk_cv.required' => 'Chức người đăng ký không được để trống',
                // kết thúc người đăng ký
                'customer_dd_name.required' => 'Tên người đại diện không được để trống',
                'customer_dd_name.min' => 'Tên người đăng đại diện không được nhỏ hơn :min ký tự',
                'customer_dd_name.max' => 'Tên người đăng đại diện không được lớn hơn :max ký tự',
                'customer_dd_gender.required' => 'Giới tính đăng người đại diện không được để trống',
                'customer_dd_gender.not_in' => 'Giới tính người đại diện không được để trống',
                'customer_dd_date.required' => 'Ngày sinh người đại diện không được để trống',
                'customer_dd_cmnd.required' => 'Số chứng minh nhân dân người đại diện không được để trống',
                'customer_dd_cmnd.numeric' => 'Số chứng minh nhân dân người đại diện phải là ký tự số',
                'customer_dd_cv.required' => 'Chức người đại diện không được để trống',
                'customer_dd_email.required' => 'Địa chỉ email người đại diện không được để trống',
                // kết thúc người đại diện
                'customer_tt_name.required' => 'Tên người đại diện làm thủ tục không được để trống',
                'customer_tt_name.min' => 'Tên người đăng đại diện làm thủ tục không được nhỏ hơn :min ký tự',
                'customer_tt_name.max' => 'Tên người đăng đại diện làm thủ tục không được lớn hơn :max ký tự',
                'customer_tt_gender.required' => 'Giới tính đăng người đại diện làm thủ tục không được để trống',
                'customer_tt_gender.not_in' => 'Giới tính người đại diện làm thủ tục không được để trống',
                'customer_tt_date.required' => 'Ngày sinh người đại diện làm thủ tục không được để trống',
                'customer_tt_cmnd.required' => 'Số chứng minh nhân dân người đại diện làm thủ tục không được để trống',
                'customer_tt_cmnd.numeric' => 'Số chứng minh nhân dân người đại diện làm thủ tục phải là ký tự số',
                'customer_tt_email.required' => 'Địa chỉ email người đại diện làm thủ tục không được để trống',
            ] );
        }
        $data = $request->all();
        $create = $this->customer->create_customer($data);
        if ($create) {
            return redirect()->route('user.customer.index')->with('success', "Tạo khách hàng mới thành công");
        } else {
           return redirect()->route('user.customer.create')->with('fails', 'Tạo khách hàng mới thất bại');
        }
    }

    public function editCustomer($id)
    {
        $customer = $this->customer->detail_customer($id);
        $list_city = config('city');
        return view('users.customers.edit', compact('customer', 'list_city'));
    }

    public function updateCustomer(Request $request)
    {
        $type_customer = $request->get('type_customer');
        if ($type_customer == 0) {
            $validate = $request->validate([
                'customer_name' => 'bail|required|min:6|max:60',
                'customer_gender' => 'bail|required|not_in:0',
                'customer_date' => 'bail|required',
                'customer_cmnd' => 'bail|required|numeric',
                'customer_phone' => 'bail|required|numeric',
                'customer_email' => 'bail|required',
                'customer_address' => 'bail|required|min:6',
                'customer_city' => 'bail|required|not_in:0',
            ],
            [
                'customer_name.required' => 'Họ và tên người đăng ký không được để trống',
                'customer_name.min' => 'Họ và tên người đăng ký không được nhỏ hơn :min ký tự',
                'customer_name.max' => 'Họ và tên người đăng ký không được lớn hơn :max ký tự',
                'customer_gender.required' => 'Giới tính không được để trống',
                'customer_gender.not_in' => 'Giới tính không được để trống',
                'customer_date.required' => 'Ngày sinh không được để trống',
                'customer_cmnd.required' => 'Số chứng minh nhân dân không được để trống',
                'customer_cmnd.numeric' => 'Số chứng minh nhân dân phải là ký tự số',
                'customer_phone.numeric' => 'Số điện thoại phải là ký tự số',
                'customer_phone.required' => 'Số điện thoại không được để trống',
                'customer_email.required' => 'Địa chỉ email không được để trống',
                'customer_phone.numeric' => 'Số điện thoại phải là ký tự số',
                'customer_address.required' => 'Địa chỉ người đăng ký không được để trống',
                'customer_address.min' => 'Địa chỉ người đăng ký không được nhỏ hơn :min ký tự',
                'customer_city.required' => 'Tỉnh / Thành phố không được để trống',
                'customer_city.not_in' => 'Tỉnh / Thành phố không được để trống',
            ] );
        } else {
            $validate = $request->validate([
                'customer_tc_name' => 'bail|required|min:6|max:120',
                'customer_tc_mst' => 'bail|required|numeric',
                'customer_tc_dctc' => 'bail|required|min:6',
                'customer_tc_sdt' => 'bail|required|numeric',
                'customer_tc_city' => 'bail|required|not_in:0',
                'customer_dk_name' => 'bail|required|min:6|max:50',
                'customer_dk_date' => 'bail|required',
                'customer_dk_cv' => 'bail|required',
                'customer_dk_cmnd' => 'bail|required|numeric',
                'customer_dk_gender' => 'bail|required|not_in:0',
                'customer_dk_phone' => 'bail|required|numeric',
                'customer_dd_name' => 'bail|required|min:6|max:50',
                'customer_dd_date' => 'bail|required',
                'customer_dd_cv' => 'bail|required',
                'customer_dd_cmnd' => 'bail|required|numeric',
                'customer_dd_gender' => 'bail|required|not_in:0',
                'customer_dd_email' => 'bail|required',
                'customer_tt_name' => 'bail|required|min:6|max:50',
                'customer_tt_date' => 'bail|required',
                'customer_tt_cmnd' => 'bail|required|numeric',
                'customer_tt_gender' => 'bail|required|not_in:0',
                'customer_tt_email' => 'bail|required',
            ],
            [
                'customer_tc_name.required' => 'Tên tổ chức đăng ký không được để trống',
                'customer_tc_name.min' => 'Tên tổ chức đăng ký không được nhỏ hơn :min ký tự',
                'customer_tc_name.max' => 'Tên tổ chức đăng ký không được lớn hơn :max ký tự',
                'customer_tc_mst.required' => 'Mã số thuế không được để trống',
                'customer_tc_mst.numeric' => 'Mã số thuế phải là ký tự số',
                'customer_tc_dctc.required' => 'Địa chỉ cơ quan / tổ chức đăng ký không được để trống',
                'customer_tc_dctc.min' => 'Địa chỉ cơ quan / tổ chức đăng ký không được nhỏ hơn :min ký tự',
                'customer_tc_sdt.required' => 'Số điện thoại cơ quan / tổ chức không được để trống',
                'customer_tc_sdt.numeric' => 'Số điện thoại cơ quan / tổ chức phải là ký tự số',
                'customer_tc_city.required' => 'Tỉnh / Thành phố cơ quan / tổ chức không được để trống',
                'customer_tc_city.not_in' => 'Tỉnh / Thành phố cơ quan / tổ chức không được để trống',
                // kết thúc tổ chức
                'customer_dk_name.required' => 'Tên người đăng ký không được để trống',
                'customer_dk_name.min' => 'Tên người đăng đăng ký không được nhỏ hơn :min ký tự',
                'customer_dk_name.max' => 'Tên người đăng đăng ký không được lớn hơn :max ký tự',
                'customer_dk_gender.required' => 'Giới tính đăng người đăng ký không được để trống',
                'customer_dk_gender.not_in' => 'Giới tính người đăng ký không được để trống',
                'customer_dk_date.required' => 'Ngày sinh người đăng ký không được để trống',
                'customer_dk_cmnd.required' => 'Số chứng minh nhân dân người đăng ký không được để trống',
                'customer_dk_cmnd.numeric' => 'Số chứng minh nhân dân người đăng ký phải là ký tự số',
                'customer_dk_phone.numeric' => 'Số điện thoại người đăng ký phải là ký tự số',
                'customer_dk_phone.required' => 'Số điện thoại người đăng ký không được để trống',
                'customer_dk_cv.required' => 'Chức người đăng ký không được để trống',
                // kết thúc người đăng ký
                'customer_dd_name.required' => 'Tên người đại diện không được để trống',
                'customer_dd_name.min' => 'Tên người đăng đại diện không được nhỏ hơn :min ký tự',
                'customer_dd_name.max' => 'Tên người đăng đại diện không được lớn hơn :max ký tự',
                'customer_dd_gender.required' => 'Giới tính đăng người đại diện không được để trống',
                'customer_dd_gender.not_in' => 'Giới tính người đại diện không được để trống',
                'customer_dd_date.required' => 'Ngày sinh người đại diện không được để trống',
                'customer_dd_cmnd.required' => 'Số chứng minh nhân dân người đại diện không được để trống',
                'customer_dd_cmnd.numeric' => 'Số chứng minh nhân dân người đại diện phải là ký tự số',
                'customer_dd_cv.required' => 'Chức người đại diện không được để trống',
                'customer_dd_email.required' => 'Địa chỉ email người đại diện không được để trống',
                // kết thúc người đại diện
                'customer_tt_name.required' => 'Tên người đại diện làm thủ tục không được để trống',
                'customer_tt_name.min' => 'Tên người đăng đại diện làm thủ tục không được nhỏ hơn :min ký tự',
                'customer_tt_name.max' => 'Tên người đăng đại diện làm thủ tục không được lớn hơn :max ký tự',
                'customer_tt_gender.required' => 'Giới tính đăng người đại diện làm thủ tục không được để trống',
                'customer_tt_gender.not_in' => 'Giới tính người đại diện làm thủ tục không được để trống',
                'customer_tt_date.required' => 'Ngày sinh người đại diện làm thủ tục không được để trống',
                'customer_tt_cmnd.required' => 'Số chứng minh nhân dân người đại diện làm thủ tục không được để trống',
                'customer_tt_cmnd.numeric' => 'Số chứng minh nhân dân người đại diện làm thủ tục phải là ký tự số',
                'customer_tt_email.required' => 'Địa chỉ email người đại diện làm thủ tục không được để trống',
            ] );
        }
        $data = $request->all();
        $update = $this->customer->updateCustomer($data);
        if ($update) {
            return redirect()->route('user.customer.index')->with('success', "Chỉnh sửa hồ sơ khách hàng thành công");
        } else {
           return redirect()->route('user.customer.edit', $data['id'])->with('fails', 'Chỉnh sửa hồ sơ khách hàng thất bại');
        }
    }

    public function deleteCustomer(Request $request)
    {
        $delete = $this->customer->deleteCustomer($request->id);
        if ($delete) {
            return json_encode(true);
        } else {
            return json_encode(false);
        }
    }

    public function detailCustomer($id)
    {
        $customer = $this->customer->detailCustomer($id);
        return view('users.customers.detail', compact('customer'));
    }


    public function list_order_with_customer($makh)
    {
        $orders = $this->customer->list_order_with_customer($makh);
        // dd($orders);
        return view('users.customers.list_order', compact('orders', 'makh'));
    }


}
