<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Factories\UserFactories;
use App\Http\Requests\RegisterValidate;
use App\Http\Requests\EmailValidate;
use App\Http\Requests\CreateNewPassRequest;
use App\Http\Requests\UpdateProfileValidate;
use App\Model\LogActivity;

use Illuminate\Mail\Mailable;
use Mail;
// thư viện mạng xã hội
use App\Services\SocialAccountService;
use Illuminate\Support\Facades\Log;
use Socialite;
use Illuminate\Contracts\Auth\Authenticatable;

class LoginController extends Controller
{
    protected $login;
    protected $log_activity;

    public function __construct()
    {
        $this->login = UserFactories::loginRepositories();
        $this->log_activity = new LogActivity();
    }

    public function index()
    {
        if (Auth::check()) {
            $user = $this->login->detail(Auth::user()->id);
            if (isset($user->meta)) {
                if ($user->mete->role == 'admin' || $user->mete->role == 'editor') {
                    return redirect()->route('admin.home');
                } else {
                    return redirect()->route('index');
                }
            } else {
                return redirect()->route('index');
            }
        } else {
            return view('users.auth.login');
        }
        return view('users.auth.login');
    }

    public function checkLogin(Request $request)
    {
        $email = $request->get('email');
        $password = $request->get('password');
        $remember = $request->has('remember') ? true : false;
        if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {
            // Tạo log
            $data_log = [
               'user_id' => Auth::user()->id,
               'action' => 'đăng nhập',
               'model' => 'User/Login',
               'description' => 'bằng email',
            ];
            $this->log_activity->create($data_log);
            // User Login
            try {
                $role = $this->login->detail(Auth::user()->id)->user_meta->role;
                if ($role == 'admin' || $role == 'editor') {
                    $request->session()->put('admin_id', Auth::user()->id);
                    $request->session()->put('admin_email', Auth::user()->email);
                    $request->session()->put('admin_passwd', Auth::user()->password);
                    return redirect()->route('admin.home');
                } else {
                    if ( !empty($request->session()->has('url_request')) ) {
                        $url = $request->session()->get('url_request');
                        $request->session()->forget('url_request');
                        return redirect($url);
                    }
                    return redirect(route('index'));
                }
            } catch (Exception $e) {
                report($e);
                return redirect()->route('login')->with('fails', 'Đăng nhập thất bại');
            }

        } else {
            return redirect()->route('login')->with('fails', 'Đăng nhập thất bại');
        }
    }

    public function logout(Request $request)
    {
        try {
          // Tạo log
          $data_log = [
             'user_id' => Auth::user()->id,
             'action' => 'đăng xuất',
             'model' => 'User/Login',
             'description' => '',
          ];
          $this->log_activity->create($data_log);
          $request->session()->forget('admin_id');
          $request->session()->forget('admin_email');
          $request->session()->forget('admin_passwd');
          Auth::logout();
          return redirect()->route('login');
        } catch (Exception $e) {
            report($e);
            return redirect()->route('login');
        }
    }

    public function register()
    {
        $capcha = config('capcha');
        return view('users.auth.register', compact('capcha'));
    }
    public function test_register()
    {
        $capcha = config('capcha');
        return view('users.auth.test_register', compact('capcha'));
    }
    public function forgotPasswordForm()
    {
        return view('users.auth.forgot-password-form');
    }

    public function sendEmailForgot(EmailValidate $request)
    {
        $email = $request->get('email');
        $result = $this->login->send_email_forgot($email);
		if ($result) {
    		return redirect()->route('forgot_password_form')->with('success', 'Thiết lập đặt lại mật khẩu thành công. Bạn hãy kiểm tra email và thực hiện xác thực theo hướng dẫn.');
    	} else {
    		return redirect()->route('forgot_password_form')->with('fails', 'Thiết lập đặt lại mật khẩu thất bại.');
    	}
    }
    public function resetPasswordForgotForm($token)
    {
        // $user = $this->login->view_forgot_password($token);
        if ($token) {
          return view('users.auth.reset-forgot-password-form', compact('token'));
        } else {
          return redirect()->route('login')->with('fails', 'Xác thực thất bại.');
        }
    }
    public function checkRegister(RegisterValidate $request)
    {
        $data = $request->all();
        $check_recapcha = $this->login->check_recapcha($data);
        // $check_recapcha = true;
        if ($check_recapcha) {
          $create = $this->login->createUser($data);
          if ($create["error"] == 0) {
              return redirect()->route('login')->with('success', 'Quý khách vừa đăng ký tạo tài khoản sử dụng Web Portal. Quý khách vui lòng kiểm tra email và thực hiện kích hoạt tài khoản theo hướng dẫn.');
          } else {
              if ( $create["error"] == 5 ) {
                return redirect()->route('login')->with('fails', 'Tạo tài khoản thất bại. Quý khách vui lòng thử tạo lại sau.');
              }
              elseif ( $create["error"] == 1 ) {
                return redirect()->route('login')->with('confirm', 'Quý khách vừa đăng ký tạo tài khoản sử dụng Web Portal. Quý khách vui lòng kiểm tra email và thực hiện kích hoạt tài khoản theo hướng dẫn.');
              }
              else {
                return redirect()->route('login')->with('fails', 'Tạo tài khoản thất bại.');
              }
          }
        } else {
          return redirect()->route('login')->with('fails', 'Tạo tài khoản thất bại. Form đăng ký không dành cho robot');
        }
    }
    public function testCheckRegister(RegisterValidate $request)
    {
        $data = $request->all();
        $check_recapcha = $this->login->check_recapcha($data);
        dd($check_recapcha);
        $create = $this->login->createUser($data);
        if ($create) {
            return redirect()->route('login')->with('success', 'Tạo tài khoản thành công. Vui lòng kiểm tra email và thực hiện xác thực theo hướng dẫn.');
        } else {
            return redirect()->route('login')->with('fails', 'Tạo tài khoản thất bại.');
        }
    }
    // Đăng nhập mạng xã hội
    public function redirect($social)
    {
       return Socialite::driver($social)->redirect();
    }
    // register user khi callback lại website
    public function callback($social, Request $request)
    {
      try {
          if (!empty($request->error_code)) {
              if ($request->error_code == 200) {
                return redirect()->route('login')->with('fails', 'Đăng nhập '.$social.' lỗi!');
              }
          }
          $socialLogin = $this->login->registerSocial(Socialite::driver($social)->stateless()->user(), $social);
          if ($socialLogin) {
            if ( $socialLogin['error'] ) {
                return redirect()->route('login')->with('fails', 'Hệ thống tạo tài khoản từ mạng xã hội đang bảo trì. Quý khách vui lòng bấm <a href="https://portal.cloudzone.vn/register">vào đây</a> để tạo tài khoản email.');
            } else {
                // Tạo log
                $user = $socialLogin['user'];
                $data_log = [
                    'user_id' => $user->id,
                    'action' => 'đăng nhập',
                    'model' => 'User/Login',
                    'description' => 'bằng ' . $social,
                ];
                $this->log_activity->create($data_log);
                // login bằng socical
                if (!empty($user->user_meta->phone)) {
                    Auth::loginUsingId($user->id);
                    if ( !empty($request->session()->has('url_request')) ) {
                        $url = $request->session()->get('url_request');
                        $request->session()->forget('url_request');
                        return redirect($url);
                    }
                    return redirect()->route('index');
                } else {
                    Auth::loginUsingId($user->id);
                    return redirect()->route('updateProfile');
                }
            }
          } else {
              return redirect()->route('login')->with('fails', 'Đăng nhập '.$social.' lỗi!');
          }
      } catch (Exception $e) {
          return redirect()->route('login')->with('fails', 'Đăng nhập '.$social.' lỗi!');
      }
    }
    // Test send Mail
    public function verify($token)
    {
        $verify = $this->login->verify($token);
        if ($verify) {
            return redirect()->route('login')->with('success', 'Kích hoạt tài khoản thành công.');
        } else {
            return redirect()->route('login')->with('fails', 'Kích hoạt tài khoản thất bại.');
        }

    }

    public function sendmail()
    {
        $this->login->send_mail();
    }

    public function changePassword (CreateNewPassRequest $request)
    {
        $data = $request->all();
        $check_token = $this->login->checkToken($data['token']);
        if ($check_token) {
          $result = $this->login->change_password($data);
          if ($result) {
            return redirect()->route('login')->with('success', 'Đặt lại mật khẩu thành công.');
          } else {
            return redirect()->route('login')->with('fails', 'Đặt lại mật khẩu thất bại.');
          }
        } else {
          return redirect()->route('login')->with('fails', 'Đường dẫn đã được sử dụng.');
        }
    }

    public function updateProfile()
    {
        return view('users.auth.updateProfile');
    }

    public function form_update_profile(UpdateProfileValidate $request)
    {
        $update = $this->login->updateProfile($request->all());
        // Tạo log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'cập nhật',
          'model' => 'User/Login',
          'description' => 'thông tin tài khoản',
        ];
        $this->log_activity->create($data_log);

        if ($update) {
            return redirect()->route('index')->with('success', 'Cập nhật thông tin thành công');
        } else {
            return redirect()->route('updateProfile')->with('fails', 'Cập nhật thông tin thất bại');
        }
    }

}
