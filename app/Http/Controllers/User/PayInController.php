<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\UserFactories;
use App\Http\Requests\PayInValidate;
use App\Services\Momo;

class PayInController extends Controller
{
    protected $payin;
    protected $momo;
    public function __construct()
    {
        $this->payin = UserFactories::payInRepositories();
        $this->momo = new Momo();
    }

    public function index()
    {
        $method_pay_in = config('pay_in');
        return view('users.pays.index', compact('method_pay_in'));
    }

    public function request(PayInValidate $request) {
        $method_pay = $request->get('method_pay');
        $check_han_muc = $this->payin->check_han_muc($request->get('money'));
        if ($check_han_muc) {
            $han_muc = 100000000 - $check_han_muc->value;
            return redirect()->route('user.pay_in_online')->with('fails' , 'Đăng ký nạp tiền thất bại. Hạn mức của tài khoản là 100.000.000 VNĐ. Quý khách chỉ có thể nạp thêm ' .number_format($han_muc,0,",",".") . ' VNĐ' );
        }

       // dd($method_pay, date('Y-m-d'));
        if ($method_pay == 'pay_in_office') {
            $pay_in_office = $this->payin->pay_in_office($request->all());
            if ($pay_in_office) {
                return redirect()->route('user.pay_in_online.pay_checkout', $pay_in_office->id);
            } else {
                return redirect()->route('user.pay_in_online')->with('fails' , 'Đăng ký nạp tiền thất bại. Vui lòng đăng ký lại hoặc chọn hình thức thanh toán khác.');
            }
        }
        elseif ($method_pay == 'bank_transfer_vietcombank') {
            $pay_in_office = $this->payin->pay_in_office($request->all());
            if ($pay_in_office) {
                return redirect()->route('user.pay_in_online.pay_checkout', $pay_in_office->id);
            } else {
                return redirect()->route('user.pay_in_online')->with('fails' , 'Đăng ký nạp tiền thất bại. Vui lòng đăng ký lại hoặc chọn hình thức thanh toán khác.');
            }
        }
        elseif ($method_pay == 'bank_transfer_techcombank') {
            $pay_in_office = $this->payin->pay_in_office($request->all());
            if ($pay_in_office) {
                return redirect()->route('user.pay_in_online.pay_checkout', $pay_in_office->id);
            } else {
                return redirect()->route('user.pay_in_online')->with('fails' , 'Đăng ký nạp tiền thất bại. Vui lòng đăng ký lại hoặc chọn hình thức thanh toán khác.');
            }
        }
        else if ($method_pay == 'momo') {
            $pay_in_office = $this->payin->pay_in_office($request->all());
            if ($pay_in_office) {
                return redirect()->route('user.pay_in_online.pay_checkout', $pay_in_office->id);
            } else {
                return redirect()->route('user.pay_in_online')->with('fails' , 'Đăng ký nạp tiền thất bại. Vui lòng đăng ký lại hoặc chọn hình thức thanh toán khác.');
            }
           // $pay_in_momo = $this->payin->pay_in_office($request->all());
           //      // $pay_in_momo = 1;
           //  if ($pay_in_momo) {
           //      $url = $this->momo->payment($pay_in_momo, $request->get('money'));
           //      return redirect($url);
           //  } else {
           //      return redirect()->route('user.pay_in_online')->with('fails' , 'Đăng ký nạp tiền thất bại. Vui lòng đăng ký lại hoặc chọn hình thức thanh toán khác.');
           //  }
        }
    }

    public function history_payment() {
        $payments = $this->payin->history_payment();
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        $momo_status = config('momo_status');
        return view('users.pays.history_payment', compact('payments', 'pay_in', 'type_payin', 'momo_status'));
    }

    public function check_invoice(Request $request)
    {
        $invoice_id = $request->get('id');
        $history_pay = $this->payin->check_invoice($invoice_id);
        return json_encode($history_pay);
    }

    public function payment_momo(Request $request)
    {
        $invoice_id = $request->get('invoice_id');
        $ma_gd = $request->get('ma_gd');
        $pay_in = $this->payin->detail_pay_in($ma_gd);
        $url = $this->momo->payment_momo($invoice_id, $pay_in);
        return redirect($url);
    }

    public function payment_order(Request $request)
    {
        // return json_encode(true);
        $invoice_id = $request->get('id');
        $invoice = $this->payin->detail_invoice($invoice_id);
        $data = [
            'pay' => $this->payin->check_invoice($invoice_id),
            'total' => $invoice->order->total,
            'credit' => $this->payin->check_credit($invoice_id),
        ];
        return json_encode($data);
    }

    public function payment_order_upgrade(Request $request)
    {
        // return json_encode(true);
        $invoice_id = $request->get('id');
        $invoice = $this->payin->detail_invoice($invoice_id);
        $data = [
            'pay' => $this->payin->check_invoice_upgrade($invoice_id),
            'total' => $invoice->order->total,
            'credit' => $this->payin->check_credit($invoice_id),
        ];
        return json_encode($data);
    }

    public function payment_order_upgrade_cloudzone_point(Request $request)
    {
        // return json_encode(true);
        $invoice_id = $request->get('id');
        $invoice = $this->payin->detail_invoice($invoice_id);
        $data = [
            'pay' => $this->payin->check_invoice_upgrade($invoice_id),
            'total' => $invoice->order->total,
            'point' => $this->payin->check_point(),
        ];
        return json_encode($data);
    }

    public function payment_order_cloudzone_point(Request $request)
    {
        // return json_encode(true);
        $invoice_id = $request->get('id');
        $invoice = $this->payin->detail_invoice($invoice_id);
        $data = [
            'pay' => $this->payin->check_invoice($invoice_id),
            'total' => $invoice->order->total,
            'point' => $this->payin->check_point(),
        ];
        return json_encode($data);
    }

    public function payment_domain_order(Request $request)
    {
        // return json_encode(true);
        $invoice_id = $request->get('id');
        $invoice = $this->payin->detail_invoice($invoice_id);
        $data = [
            'pay' => $this->payin->check_invoice_domain($invoice_id),
            'total' => $invoice->order->total,
            'credit' => $this->payin->check_credit($invoice_id),
        ];
        return json_encode($data);
    }
    public function payment_domain_expired_order(Request $request)
    {
        // return json_encode(true);
        $invoice_id = $request->get('id');
        $invoice = $this->payin->detail_invoice($invoice_id);
        $data = [
            'pay' => $this->payin->check_invoice_domain_expired($invoice_id),
            'total' => $invoice->order->total,
            'credit' => $this->payin->check_credit($invoice_id),
        ];
        return json_encode($data);
    }
    public function payment_domain_expired_order_cloudzone_point(Request $request)
    {
        // return json_encode(true);
        $invoice_id = $request->get('id');
        $invoice = $this->payin->detail_invoice($invoice_id);
        $data = [
            'pay' => $this->payin->check_invoice_domain_expired($invoice_id),
            'total' => $invoice->order->total,
            'point' => $this->payin->check_point(),
        ];
        return json_encode($data);
    }

    public function payment_expired(Request $request)
    {
        // return json_encode(true);
        $invoice_id = $request->get('id');
        $invoice = $this->payin->detail_invoice($invoice_id);
        $data = [
            'pay' => $this->payin->check_invoice_expired($invoice_id),
            'total' => $invoice->order->total,
            'credit' => $this->payin->check_credit($invoice_id),
        ];
        return json_encode($data);
    }

    public function payment_expired_cloudzone_point(Request $request)
    {
        // return json_encode(true);
        $invoice_id = $request->get('id');
        $invoice = $this->payin->detail_invoice($invoice_id);
        $data = [
            'pay' => $this->payin->check_invoice_expired($invoice_id),
            'total' => $invoice->order->total,
            'point' => $this->payin->check_point(),
        ];
        return json_encode($data);
    }

    public function payment_addon(Request $request)
    {
        // return json_encode(true);
        $invoice_id = $request->get('id');
        $invoice = $this->payin->detail_invoice($invoice_id);
        $data = [
            'pay' => $this->payin->check_invoice_addon($invoice_id),
            'total' => $invoice->order->total,
            'credit' => $this->payin->check_credit($invoice_id),
        ];
        return json_encode($data);
    }

    public function payment_addon_cloudzone_point(Request $request)
    {
        // return json_encode(true);
        $invoice_id = $request->get('id');
        $invoice = $this->payin->detail_invoice($invoice_id);
        $data = [
            'pay' => $this->payin->check_invoice_addon($invoice_id),
            'total' => $invoice->order->total,
            'point' => $this->payin->check_point(),
        ];
        return json_encode($data);
    }

    public function payment_change_ip_off(Request $request)
    {
        // return json_encode(true);
        $invoice_id = $request->get('id');
        $invoice = $this->payin->detail_invoice($invoice_id);
        $data = [
            'pay' => $this->payin->check_invoice_change_ip($invoice_id),
            'total' => $invoice->order->total,
            'credit' => $this->payin->check_credit($invoice_id),
        ];
        return json_encode($data);
    }

    public function payment_change_ip_off_cloudzone_point(Request $request)
    {
        // return json_encode(true);
        $invoice_id = $request->get('id');
        $invoice = $this->payin->detail_invoice($invoice_id);
        $data = [
            'pay' => $this->payin->check_invoice_change_ip($invoice_id),
            'total' => $invoice->order->total,
            'point' => $this->payin->check_point(),
        ];
        return json_encode($data);
    }

    public function pay_checkout($id)
    {
        $pay_in_office = $this->payin->detail_pay_in_with_id($id);
        return view('users.pays.finish', compact('pay_in_office'));
    }


}
