<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\UserFactories;
use App\Http\Requests\UpdatePassRequest;
use App\Http\Requests\ValidateUpdateProfile;
use Hash;
use Auth;

class HomeController extends Controller
{
    protected $home;
    protected $customer;
    protected $services;
    protected $user_repo;

    public function __construct()
    {
        $this->home = UserFactories::homeRepositories();
        $this->customer = UserFactories::customerRepositories();
        $this->user_repo = UserFactories::userRepositories();
        $this->user_notifica = UserFactories::notificationRepositories();
        $this->services = UserFactories::serviceRepositories();
    }

    public function index()
    {
        // dd('da den');
        // $service_nearly = $this->home->get_invoice_pending_home();
        // $services  = $this->home->get_total_sevices();
        // $tutorials = $this->home->get_tutorials();
        return view('users.index');
    }

    public function check_email(Request $request)
    {
        $user = $this->user_repo->detail_user_by_email($request->get('email'));
        return json_encode($user);
    }

    public function loadVpsPros()
    {
        return json_encode($this->services->qttVpsPros());
    }

    public function index2()
    {
        dd('da den');
        $service_nearly = $this->home->get_invoice_pending_home();
        $services  = $this->home->get_total_sevices();
        $tutorials = $this->home->get_tutorials();
        return view('users.index', compact('service_nearly' , 'services', 'tutorials'));
    }

    public function profile()
    {
        return view('users.auth.profile');
    }

    public function update_profile(ValidateUpdateProfile $request)
    {
        $data = $request->all();
        if (!empty($data['password'])) {
            $validatedData = $request->validate(
                [
                    'password' => 'bail|required|confirmed|min:6|max:64|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&"*()\-_=+{};:,<.>]).{8,255}+$/',
                ],
                [
                    'required'  => ':attribute Không được để trống',
                    'min'       => ':attribute Không được nhỏ hơn :min',
                    'max'       => ':attribute Không được lớn hơn :max',
                    'confirmed' => ':attribute không trùng khớp',
                    'regex'     => ':attribute phải có ký tự chữ số, chữ hoa, chữ thường và ký tự đặc biệt',
                ],
                [
                    'password' => 'Mật khẩu',
                ]
            );
        }
        if (!empty($data['avatar'])) {
            $validatedData = $request->validate(
                [
                    'avatar' => 'bail|mimes:jpeg,jpg,png,gif|required|max:10000',
                ],
                [
                    'required' => ':attribute Không được để trống',
                    'mimes'    => ':attribute Không đúng định dạng ảnh',
                    'max'      => ':attribute Không được lớn hơn :max',
                ]
            );
        }
        if (!empty($data['cmnd_before'])) {
            $validatedData = $request->validate(
                [
                    'cmnd_before' => 'bail|mimes:jpeg,jpg,png,gif|max:5120',
                ],
                [
                    'mimes' => 'Chứng minh nhân dân mặt trước không đúng định dạng ảnh',
                    'max' => 'Ảnh chứng minh mặt trước không được lớn hơn 5Mb',
                ]
            );
        }
        if (!empty($data['cmnd_after'])) {
            $validatedData = $request->validate(
                [
                    'cmnd_after' => 'bail|mimes:jpeg,jpg,png,gif|max:5120'
                ],
                [
                    'mimes' => 'Chứng minh nhân dân mặt sau không đúng định dạng ảnh',
                    'max' => 'Ảnh chứng minh mặt sau không được hơn 5Mb',
                ]
            );
        }
        $update = $this->home->update_profile($data);
        return redirect()->route('profile')->with($update);
    }


    public function listCustomer()
    {
        $customers = $this->customer->listCustomerHome();
        return json_encode($customers);
    }

    public function load_siderbar_top()
    {
        $siderbar_top = $this->user_repo->load_siderbar_top();
        $detail_order = $this->user_repo->total_detail_order_progressing();
        $total_ticket = $this->user_repo->total_ticket(Auth::user()->id);
        $siderbar_top['detail_order_progressing'] = $detail_order;
        $siderbar_top['total_ticket'] = $total_ticket;
        return json_encode($siderbar_top);
    }

    public function load_content_center()
    {
        $vps_nearly = $this->user_repo->total_vps_nearly();
        $vps_us_nearly = $this->user_repo->total_vps_us_nearly();
        $hosting_nearly = $this->user_repo->total_hosting_nearly();
        $server_nearly = $this->user_repo->total_server_nearly();
        $colocation_nearly = $this->user_repo->total_colocation_nearly();
        $email_hosting = $this->user_repo->total_email_hosting_nearly();
        $total_notication = $this->user_repo->total_notication();
        $tutorials = $this->user_repo->get_tutorials();
        $data = [
          'total_vps_nearly' => $vps_nearly,
          'total_vps_us_nearly' => $vps_us_nearly,
          'total_hosting_nearly' => $hosting_nearly,
          'total_notication' => $total_notication['total'],
          'notifications_reads' => $total_notication['notication'],
          'total_server_nearly' => $server_nearly,
          'total_colocation_nearly' => $colocation_nearly,
          'total_email_hosting_nearly' => $email_hosting,
          'total_tutorials' => $tutorials['total'],
          'tutorials' => $tutorials['tutorials'],
        ];
        return json_encode($data);
    }

    public function changePassForm()
    {
        return view('users.auth.changepass');
    }

    public function load_siderbar_right()
    {
        $data =[
          'services_right' => '',
        ];
        $siderbar_right = $this->user_repo->load_siderbar_right();
        $data['services_right'] = $siderbar_right;
        return json_encode($data);
    }

    public function updatePass(UpdatePassRequest $request)
    {
        $data = $request->all();

        if (!(Hash::check($request->get('old_password'), Auth::user()->password))) {
            return redirect()->route('change_pass_form')->with('fails', "Mật khẩu hiện tại không đúng");
        }
        if(strcmp($request->get('old_password'), $request->get('new_password')) == 0){
            return redirect()->route('change_pass_form')->with('fails', "Mật khẩu mới không được trùng với mật khẩu hiện tại");
        }
        $update = $this->home->update_pass($data);
        if ($update) {
            return redirect()->route('change_pass_form')->with('success', "Đổi mật khẩu thành công");
        } else {
            return redirect()->route('change_pass_form')->with('fails', "Đổi mật khẩu thất bại");
        }
    }
}
