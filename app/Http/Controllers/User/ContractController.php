<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\UserFactories;
use Auth;
use DomPDF;
use PDF;
use SnappyImage;
use JasperPHP;

class ContractController extends Controller
{
    // protected $userRepository;
    protected $contractRepository;

    public function __construct()
    {
        // $this->userRepository = UserFactories::userRepositories();
        $this->contractRepository = UserFactories::contractRepositories();
    }

    public function index()
    {
        $contracts = $this->contractRepository->get_all();
        return view('users.contract.list', compact('contracts'));
    }

    public function create()
    {
        $user_detail = Auth::user();
        $latest_contract_status =  $this->contractRepository->check_latest_contract_status($user_detail->id);
        // dd($latest_contract_status);
        if ($latest_contract_status) {
            return view('users.contract.create', compact('user_detail'));
        } else {
            return redirect(route('user.contract.index'))->with('fails', 'Một hợp đồng của bạn đang chờ xét duyệt, bạn chưa thể tạo thêm');
        }
    }

    public function create_data_contract(Request $request) 
    {
        $data = $request->all();
        $user_detail = Auth::user();
        $contract = $this->contractRepository->create_data_contract($user_detail, $data);
        if($contract) {
            $id = $contract->id;
            return redirect(route('user.contract.detail', ['id' => $contract->id]))->with('success', 'Tạo hợp đồng thành công, vui lòng thêm sản phẩm vào hợp đồng!');
        } else {
            return redirect(route('user.contract.index'))->with('fails', 'Tạo hợp đồng thất bại, vui lòng thử lại !');
        }
    }

    public function detail($id) 
    {
        $contract = $this->contractRepository->get_contract($id);
        if ($contract) {
            if($contract->status === 'pending') {
                $billings = config('billing');
                return view('users.contract.detail', compact('contract', 'billings'));
            } elseif ($contract->status === 'cancel') {
                return redirect(route('user.contract.index'))->with('fails', 'Hợp đồng đã hủy, bạn không thể chỉnh sửa');
            } else {
                return redirect(route('user.contract.index'))->with('fails', 'Hợp đồng đã được xét duyệt, bạn không thể chỉnh sửa');
            }
        } else {
            return redirect(route('user.contract.index'))->with('fails', 'Không tìm thấy hợp đồng');
        }
        

    }

    public function get_target_from_user(Request $request) 
    {
        $data = $request->all();
        $targets = $this->contractRepository->get_target_from_user($data);
        return $targets;
    }

    public function get_target_detail(Request $request)
    {
        $data = $request->all();
        $target_detail = $this->contractRepository->get_target_detail($data); 
        return json_encode($target_detail);
    }

    public function create_contract_detail(Request $request)
    {
        $data_contract_detail = $request->get('data');
        parse_str($data_contract_detail, $data_contract_detail);
        $result = $this->contractRepository->create_contract_detail($data_contract_detail);
        return $result;
    }

    public function update(Request $request, $id)
    {
        $data_update = $request->all();
        $contract = $this->contractRepository->get_contract($id);
        if ($contract->status === 'pending') {
            $update = $this->contractRepository->update($id, $data_update); 
            if ($update['status'] === true) {
                return redirect(route('user.contract.detail', ['id' => $id]))->with('success', $update['message']);
            } else {
                return redirect(route('user.contract.detail', ['id' => $id]))->with('fails', $update['message']);
            }
        } else {
            return redirect(route('user.contract.index'))->with('fails', 'Hợp đồng đã được xét duyệt, bạn không thể chỉnh sửa');
        }      
    }

    public function print_pdf($id)
    {
        $contract = $this->contractRepository->get_contract_with_id($id);
        if($contract) {
            if ($contract->status == 'active') {
                $billings = config('billing');
                $contract_details = $this->contractRepository->get_contract_details($id);
                $unicode = array(
                    'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
                    'd'=>'đ|Đ',
                    'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
                    'i'=>'í|ì|ỉ|ĩ|ị|I|Ì|Í|Ị|Ỉ|Ĩ',
                    'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ọ|Ó|Ò|Ỏ|Õ|Ô|Ồ|Ố|Ổ|Ỗ|Ộ|Ơ|Ờ|Ớ|Ở|Ỡ|Ợ',
                    'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ụ|Ũ|Ư|Ừ|Ứ|Ử|Ữ|Ự',
                    'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
                );
                $str_name = strtolower($contract->title);
                foreach($unicode as $nonUnicode => $uni){
                    $str_name = preg_replace( "/($uni)/i" , $nonUnicode , $str_name);
                }
                $str_name = str_replace( ' ', '_', $str_name );
                $path = '/storage/contracts/'. $contract->user_id;
                $sub_total = 0;
                foreach ($contract_details as $contract_detail) {
                    if ($contract_detail->amount) {
                        $sub_total += $contract_detail->amount;
                    }
                }
                $vat = ($sub_total*10)/100;
                $total = $sub_total + $vat;
                $pdf = DomPDF::loadView('admin.contract.contract-pdf', compact( 'contract', 'contract_details', 'billings', 'sub_total', 'vat', 'total'));
                return $pdf->stream($contract->title . ' (' . $contract->contract_number .').pdf'); 
            } elseif ($contract->status == 'cancel') {
                return redirect(route('user.contract.index'))->with('fails', 'Hợp đồng bị hủy');
            } else {
                return redirect(route('user.contract.index'))->with('fails', 'Hợp đồng đang xét duyệt, bạn chưa thể xem bản in');
            }
        } else {
            return redirect(route('user.contract.index'))->with('fails', 'Không tìm thấy hợp đồng');
        }
    }

}
