<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Factories\UserFactories;
use Illuminate\Http\Request;

class NotificationController extends Controller
{

    protected $notificationRepository;

    //Hàm khởi tạo
    public function __construct()
    {
        $this->notificationRepository = UserFactories::notificationRepositories();
    }

    public function index(Request $request)
    {
        $data = $request->all();
        $notifications = $this->notificationRepository->all($data);
        return view('users.notifications.index', compact('notifications'));
    }
    public function detail($id)
    {
        $notification_detail = $this->notificationRepository->get_notification($id);
        if ($notification_detail) {
          return view('users.notifications.detail', compact('notification_detail'));
        } else {
          return redirect()->route('user.notification.index')->with('fails', 'Thông báo không phải của quý khách');
        }
    }
    public function read_notification(Request $request)
    {
        $id = $request->get('id');
        $read_notification = $this->notificationRepository->read_notification($id);
        return $read_notification;
    }

    public function ajax_read(Request $request)
    {
        $ajax_read = $this->notificationRepository->ajax_read( $request->all() );
        return json_encode($ajax_read);
    }

}
