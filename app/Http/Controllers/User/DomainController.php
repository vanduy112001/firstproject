<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Factories\UserFactories;
use Illuminate\Http\Request;
use App\Model\Domain;
use Auth;
use Validator;
use App\Http\Requests\SearchDomainValidate;
use App\Services\DomainPA;

class DomainController extends Controller
{
    protected $domain;
    protected $domain_api;

    //Hàm khởi tạo
    public function __construct()
    {
        $this->domain = UserFactories::domainRepositories();
        $this->domain_api = new DomainPA();
    }

    public function index(Request $request)
    {
        $user_id = Auth::user()->id;
        $domains = $this->domain->get_domain($user_id);
        return view('users.domains.index', compact('domains'));
    }
    public function search_domain()
    {
        $domain_products = $this->domain->search_domain();
        return view('users.domains.user_search', compact('domain_products'));
    }

    public function domain_promotion($id)
    {
        $check_event = $this->domain->check_event($id);
        if ($check_event) {
           return view('users.domains.domain_promotion', compact('id'));
        } else {
           return redirect()->route('user.domain.search')->with('fails', 'Chương trình khuyến mãi này không dành cho quý khách hoặc quý khách đã sử dụng khuyến mãi. Quý khách vui lòng kiểm tra lại khuyến mãi hoặc liên hệ lại với chúng tôi.');
        }
    }

    public function domain_product_expired_price(Request $request)
    {
        $id = $request->get('id');
        $product_expired = $this->domain->get_product_domain_by_id($id);
        return $product_expired;
    }

    public function get_domain($user_id)
    {
        $domains = $this->domain->get_domain($user_id);
    }
    public function info(Request $request)
    {
        $id = $request->get('id');
        return $this->domain->info($id);
    }

    public function change_pass_domain(Request $request)
    {
        $validated = Validator::make(
            $request->all(),
            [
                'password_domain' => 'bail|required|min:8|max:15|regex: /^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).+$/',
            ],
            [
                'password_domain.required' => 'Mật khẩu không được để trống',
                'password_domain.min' => 'Mật khẩu không được nhỏ hơn 8 kí tự',
                'password_domain.max' => 'Mật khẩu  không được lớn hơn 15 kí tự',
                'password_domain.regex' => 'Mật khẩu  phải bao gồm số và chữ',
            ],
        );
        if ($validated->fails()) {
            return $data = [
                'status' => false,
                'message' => $validated->errors(),
            ];
        } else {
            $id = $request->get('domain-id');
            $pass = $request->get('password_domain');
            return $this->domain->change_pass_domain($id, $pass);
        }
    }

    public function search_domain_promotion(Request $request)
    {
        $exts = [ '.com', '.net' ];
        $data = [
            'domain' => $request->get('domain'),
            'ext'    => $request->get('ext'),
        ];
        return $this->domain_api->get_result_domain($data, $exts);
    }

    public function auto_fill_oder_domain_form() 
    {
        $user_id = Auth::user()->id;
        $user_data = $this->domain->get_data_user($user_id);
        return $user_data;
    }

}
