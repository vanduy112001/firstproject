<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\UserFactories;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    protected $product;
    public function __construct()
    {
        $this->product = UserFactories::productRepositories();
    }

    public function index($id)
    {
        $group_product = $this->product->get_detail_group_product($id);
        $products = $this->product->get_product_with_group_product($id);
        return view('users.products.index', compact('group_product', 'products'));
    }

    public function product_vps($id)
    {
        $check_group_product = $this->product->check_group_product($id);
        if ( $check_group_product ) {
            try {
                $group_product = $this->product->get_detail_group_product($id);
                $products = $this->product->get_product_with_group_product($id);
                return view('users.products.product_vps', compact('group_product', 'products'));
            } catch (Exception $e) {
                report($e);
                $group_product = $this->product->get_detail_group_product($id);
                $products = $this->product->get_product_with_group_product($id);
                return view('users.products.index', compact('group_product', 'products'));
            }
        } else {
            return redirect()->route('index')->with('fails', 'Sản phẩm VPS này không có hoặc không thuộc quyền truy cập của quý khách.');
        }
    }

    public function product_vps_us($id)
    {
        $check_group_product = $this->product->check_group_product($id);
        if ( $check_group_product ) {
            $group_product = $this->product->get_detail_group_product($id);
            $products = $this->product->get_product_with_group_product($id);
            return view('users.products.product_vps', compact('group_product', 'products'));
        } else {
            return redirect()->route('index')->with('fails', 'Sản phẩm VPS này không có hoặc không thuộc quyền truy cập của quý khách.');
        }
    }

    public function product_hosting($id)
    {
        $check_group_product = $this->product->check_group_product($id);
        if ( $check_group_product ) {
            $group_product = $this->product->get_detail_group_product($id);
            $products = $this->product->get_product_with_group_product($id);
            return view('users.products.product_hosting', compact('group_product', 'products'));
        } else {
            return redirect()->route('index')->with('fails', 'Sản phẩm Hosting này không có hoặc không thuộc quyền truy cập của quý khách.');
        }
    }

    public function product_server($id)
    {
        $user = Auth::user();
        $group_product = $this->product->get_detail_group_product($id);
        if ($user->can('checkGroupProduct', $group_product)) {
            $products = $this->product->get_product_with_group_product($id);
            return view('users.products.product_server', compact('group_product', 'products'));
        } else {
            return redirect()->route('index')->with('fails', 'Sản phẩm Server này không có hoặc không thuộc quyền truy cập của quý khách.');
        }
    }

    public function product_colocation($id)
    {
        $user = Auth::user();
        $group_product = $this->product->get_detail_group_product($id);
        if ($user->can('checkGroupProduct', $group_product)) {
            $products = $this->product->get_product_with_group_product($id);
            return view('users.products.product_colocation', compact('group_product', 'products'));
        } else {
            return redirect()->route('index')->with('fails', 'Sản phẩm Colocation này không có hoặc không thuộc quyền truy cập của quý khách.');
        }
    }

    public function product_proxy($id)
    {
        $user = Auth::user();
        $group_product = $this->product->get_detail_group_product($id);
        if ($user->can('checkGroupProduct', $group_product)) {
            $products = $this->product->get_product_with_group_product($id);
            return view('users.products.product_proxy', compact('group_product', 'products'));
        } else {
            return redirect()->route('index')->with('fails', 'Sản phẩm Proxy này không có hoặc không thuộc quyền truy cập của quý khách.');
        }
    }

    public function check_event(Request $request)
    {
       $event = $this->product->get_event_promotion( $request->get("id") , $request->get("billing_cycle") );
       return json_encode($event);
    }

}
