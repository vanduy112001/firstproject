<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\UserFactories;

class OrderController extends Controller
{
    protected $order;
    public function __construct()
    {
        $this->order = UserFactories::orderRepositories();
    }

    public function listOrder()
    {
        $orders = $this->order->list_order();
        $type_invoice = config('type_invoice');
        return view('users.orders.index', compact('orders', 'type_invoice'));
    }
    public function cancelOrder(Request $request)
    {
        $id = $request->get('id');
        $cancel = $this->order->cancel_order($id);
        if($cancel) {
            return json_encode(true);
        } else {
            return json_encode(false);
        }
    }

    public function detail_invoice(Request $request)
    {
      $data = $this->order->detail_invoice($request->get('id'));
      return json_encode($data);
    }

}
