<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\UserFactories;
use App\Services\Momo;

class MomoController extends Controller
{
    protected $payin;
    protected $momo;
    public function __construct()
    {
        $this->payin = UserFactories::payInRepositories();
        $this->momo = new Momo();
    }

    public function momoReturn(Request $request) {
        // dd($request->all());
        $result = $this->momo->payment_return($request->all());
        // dd('da den');
        if($result['status']) {
            return redirect()->route('order.check_out')->with('finish' , $result['message']);
        } else {
            return redirect()->route('user.pay_in_online')->with('fails' , $result['message']);
        }
    }

    public function momoNotify()
    {
        # code...
    }
}
