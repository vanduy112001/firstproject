<?php

namespace App\Http\Controllers\User;

use App\ReadNotification;
use Illuminate\Http\Request;

class ReadNotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReadNotification  $readNotification
     * @return \Illuminate\Http\Response
     */
    public function show(ReadNotification $readNotification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReadNotification  $readNotification
     * @return \Illuminate\Http\Response
     */
    public function edit(ReadNotification $readNotification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReadNotification  $readNotification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReadNotification $readNotification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReadNotification  $readNotification
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReadNotification $readNotification)
    {
        //
    }
}
