<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\UserFactories;
use App\Http\Requests\SearchDomainValidate;

class ClientController extends Controller
{
    protected $client;
    public function __construct()
    {
        // $this->product = UserFactories::productRepositories();
        $this->client = UserFactories::clientRepositories();
    }

    public function index()
    {
        return view('users.clients.index');
    }

    public function private_policy()
    {
        return view('users.clients.private_policy');
    }

    public function term_service()
    {
        return view('users.clients.term_service');
    }

    public function product($id)
    {
        // $id = $this->client->get_group_product_id($name);
        $group_product = $this->client->get_detail_group_product($id);
        $products = $this->client->get_product_with_group_product($id);
        return view('users.clients.product', compact('group_product', 'products'));
    }

    public function search_domain()
    {
        $domain_products = $this->client->get_domain();
        return view('users.domains.seach', compact('domain_products'));
    }

    public function search_domain_result(SearchDomainValidate $request)
    {
        $exts = [ '.vn', '.com.vn', '.net.vn', '.com', '.net', '.org', '.edu.vn'];
        $data = [
            'domain' => $request->get('domain'),
            'ext'    => $request->get('ext'),
        ];
        return $results = $this->client->get_result($data, $exts);
    }

    public function activity_log()
    {
        return view('users.activity_log.index');
    }

    public function list_activity_log()
    {
        $data =  $this->client->list_log();
        return json_encode($data);
    }

    public function list_log_with_action(Request $request)
    {
        $data =  $this->client->list_log_with_action($request->get('qtt') , $request->get('action') , $request->get('service') );
        return json_encode($data);
    }

    public function momo(Request $request)
    {
      $config = [

        'phone' => '0387586213', //sdt
        'otp' => '869202', //otp
        'password' => '761311', //pass

        'rkey' => 'JiqNaoc5yFLeLjz7jMAS', // 20 characters
        'setupKeyEncrypted' => "kysEfC775UPZf2E9vNdyQlBe9dPwc2/k4FPiVkX3dHKFa0ETVLtN3UFukwnM3BJP",
        'imei' => '0C9C8DF5-46DA-4779-A58F-0EDE31F044FE',
        'aaid' => '', //null
        'idfa' => "817E452F-579A-4C89-8A69-93CD95CA5A67",
        'token' => "f4tIFMYO1KA:APA91bFRSRcWG5ppPxmQ50FP5BZvhyKO7-7X54z8pZ1YEm2WhtKVFaRJXnfbuCBnVNGN0Nt5MThPtSseowD2nkS14LJLZSKcpRtS4sF2BT9qPdBKeJq7KYJ6yRVnM2DW91bpfLCQWzUD",
        'onesignalToken' => "6549f68e-0f57-4919-bab0-2732acac0e11",

        'csp' => 'Mobifone',
        'icc' => '',
        'mcc' => '452',
        'mnc' => '01',
        'cname' => 'Vietnam',
        'ccode' => '084',
        'channel' => 'APP',
        'lang' => 'vi',
        'deviceOS' => 'IOS',
        'device' => 'iPhone',
        'firmware' => '12.1.2',
        'manufacture' => 'Apple',
        'hardware' => 'iPhone',
        'simulator' => false,
        'appVer' => '21462',
        'appCode'   => "2.1.46",
        'deviceOS'  => "IOS",
      ];
      $config['ohash'] = hash('sha256', $config['phone'] . $config['rkey'] . $config['otp']);
      $config['setupKeyDecrypted'] = $this->encryptDecrypt($config['setupKeyEncrypted'], $config['ohash'], 'DECRYPT');
      $microTime = floor(microtime(true) * 1000);
      $msgType = ['type' => 'USER_LOGIN_MSG', '_class' => 'mservice.backend.entity.msg.LoginMsg'];
      $msgType['checkSumSyntax'] = $config['phone'] . $microTime . '000000' . $msgType['type'] . ($microTime / 1000000000000.0) . 'E12';
      $msgType['checkSum'] = $this->encryptDecrypt($msgType['checkSumSyntax'], $config['setupKeyDecrypted']);
      $msgType['pHashSyntax'] = $config['imei'] . '|' . $config['password'];
      $msgType['pHash'] = $this->encryptDecrypt($msgType['pHashSyntax'], $config['setupKeyDecrypted']);
      $data_body =  json_encode([
        'user'      => $config['phone'],
        'msgType'   => $msgType['type'],
        'cmdId'     => $microTime . '000000',
        'lang'      => $config['lang'],
        'channel'   => $config['channel'],
        'time'      => $microTime,
        'appVer'    => $config['appVer'],
        'appCode'   => $config['appCode'],
        'deviceOS'  => $config['deviceOS'],
        'result'    => true,
        'errorCode' => 0,
        'errorDesc' => '',
        'extra'     => [
          'checkSum' => $msgType['checkSum'],
          'pHash' => $msgType['pHash'],
          'AAID' => $config['aaid'],
          'IDFA' => $config['idfa'],
          'TOKEN' => $config['token'],
          'ONESIGNAL_TOKEN' => $config['onesignalToken'],
          'SIMULATOR' => $config['simulator']
        ],
        'pass'      => $config['password'],
        'momoMsg'   => ['_class' => $msgType['_class'], 'isSetup' => true]
        ]) ;
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://owa.momo.vn/public",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $data_body,
          CURLOPT_HTTPHEADER => array(
            'User-Agent' => "MoMoApp-Release/%s CFNetwork/978.0.7 Darwin/18.6.0",
            'Msgtype' => "USER_LOGIN_MSG",
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Msgtype' => $msgType['type'],
            'Userhash' => md5($config['phone'])  ,
          )
        ));
        $response = curl_exec($curl);
        $AUTH_TOKEN = json_decode($response)->extra->AUTH_TOKEN;
        //Trans
        $msgType = ['type' => 'QUERY_TRAN_HIS_MSG', '_class' => 'mservice.backend.entity.msg.QueryTranhisMsg'];
        $msgType['checkSumSyntax'] = $config['phone'] . $microTime . '000000' . $msgType['type'] . ($microTime / 1000000000000.0) . 'E12';
        $msgType['checkSum'] = $this->encryptDecrypt($msgType['checkSumSyntax'], $config['setupKeyDecrypted']);
        $msgType['pHashSyntax'] = $config['imei'] . '|' . $config['password'];
        $msgType['pHash'] = $this->encryptDecrypt($msgType['pHashSyntax'], $config['setupKeyDecrypted']);
        $data_post =  json_encode([
          'user'      => $config['phone'],
          'msgType'   => $msgType['type'],
          'cmdId'     => $microTime . '000000',
          'lang'      => $config['lang'],
          'channel'   => $config['channel'],
          'time'      => $microTime,
          'appVer'    => $config['appVer'],
          'appCode'   => $config['appCode'],
          'deviceOS'  => $config['deviceOS'],
          'result'    => true,
          'errorCode' => 0,
          'errorDesc' => '',
          'extra'     => ['checkSum' => $msgType['checkSum']],
          'momoMsg'   => ['_class' => $msgType['_class'], 'begin' => strtotime('360 hour ago') * 1000, 'end' => $microTime]
        ]);
        $ch = curl_init();
        curl_setopt_array($ch, array(
          CURLOPT_URL => "https://owa.momo.vn/api/sync/QUERY_TRAN_HIS_MSG",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => $data_post,
          CURLOPT_HTTPHEADER => array(
            'User-Agent' => "MoMoApp-Release/%s CFNetwork/978.0.7 Darwin/18.6.0",
            'Msgtype' => $msgType['type'],
            'Userhash' => md5($config['phone']),
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization: Bearer ' . trim($AUTH_TOKEN),
          )
        ));

        $result = curl_exec($ch);
        dd(json_decode($result,true));

      }

    public static function encryptDecrypt($data, $key, $mode = 'ENCRYPT')
  	{
  		if (strlen($key) < 32) {
  			$key = str_pad($key, 32, 'x');
  		}

  		$key = substr($key, 0, 32);
  		$iv = pack('C*', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

  		if ($mode === 'ENCRYPT') {
  			return base64_encode(openssl_encrypt($data, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv));
  		}
  		else {
  			return openssl_decrypt(base64_decode($data), 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);
  		}
  	}

}
