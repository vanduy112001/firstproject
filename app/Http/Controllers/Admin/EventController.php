<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;
use Auth;
use App\Model\EventPromotion;

class EventController extends Controller
{
    protected $event;
    protected $product;

    public function __construct()
    {
       $this->event = AdminFactories::eventRepository();
       $this->product = AdminFactories::productRepositories();
    }

    public function index()
    {
        $user = Auth::user();
        // dd( $user->can('indexEventPromotion', EventPromotion::class) );
        if ( $user->can('indexEventPromotion', EventPromotion::class) ) {
            $events = $this->event->list_event_with_index();
            $products = $this->product->get_products_in_event();
            $billings = config('billing');
            // dd($events);
            return view('admin.events.index', compact('products', 'billings', 'events') );
        } else {
            return redirect()->route('admin.home')->with('fails', 'Bạn không có quyền vào trang này.');
        }
        
    }

    public function actions(Request $request)
    {
        $user = Auth::user();
        if ( $user->can('actionsEventPromotion', EventPromotion::class) ) {
            $action = $request->get('action');
            if ($action == 'create') {
                $create = $this->event->create($request->all());
                if ($create == 1) {
                    return json_encode('Tạo khuyến mãi cho sản phẩm thành công');
                } elseif ($create == 2) {
                    return json_encode('Khuyến mãi cho sản phẩm và thời gian này đã có. Vui lòng kiểm tra lại các khuyến mãi.');
                }
                else {
                    return json_encode('Tạo khuyến mãi cho sản phẩm không thành công');
                }
            }
            else {
                $update = $this->event->update($request->all());
                if ($update == 1) {
                    return json_encode('Chỉnh sửa khuyến mãi cho sản phẩm thành công');
                } elseif ($update == 2) {
                    return json_encode('Khuyến mãi cho sản phẩm và thời gian này đã có. Vui lòng kiểm tra lại các khuyến mãi.');
                }
                else {
                    return json_encode('Chỉnh sửa khuyến mãi cho sản phẩm không thành công');
                }
            }
        } else {
            return json_encode('Bạn không có quyền tạo hoặc chỉnh sửa hệ thống khuyến mãi');
        }
        
    }

    public function list_event(Request $request)
    {
        $select = $request->get('select');
        $events = $this->event->list_event($select);
        return json_encode($events);
    }

    public function detail(Request $request)
    {
        $id = $request->get('id');
        $data = [
            'event' => $this->event->detail($id),
            'products' => $this->product->get_products_in_event(),
            'billings' => config('billing'),
        ];
        return json_encode($data);
    }

    public function delete(Request $request)
    {
        $user = Auth::user();
        if ( $user->can('deleteEventPromotion', EventPromotion::class) ) {
            $delete = $this->event->delete($request->get('id'));
            return json_encode([
                'error' => 0,
                'delete' => $delete
            ]);
        } else {
            return json_encode([
                'error' => 1,
                'delete' => ''
            ]);
        }
    }

}
