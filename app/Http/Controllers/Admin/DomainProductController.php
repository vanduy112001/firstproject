<?php

namespace App\Http\Controllers\Admin;

use App\Factories\AdminFactories;
use Illuminate\Http\Request;
use App\Http\Requests\DomainCreateValidate;

class DomainProductController extends Controller
{
    protected $domainProduct;

    //Hàm khởi tạo
    public function __construct()
    {
        $this->domainProduct = AdminFactories::domainProductRepositories();
    }

    public function index(Request $request)
    {
        // $notifications = Notification::orderBy('id', 'DESC')->get();
        $data = $request->all();
        $domainproducts = $this->domainProduct->all($data);
        $search = $request->get('keyword');
        return view('admin.domains.index', compact('domainproducts', 'search'));
    }

    public function create()
    {
        return view('admin.domains.create');
    }

    public function store(DomainCreateValidate $request)
    {
        $data = [
            'type' =>  $request->get('type'),
            'local_type' => $request->get('local_type'),
            'annually' => !empty($request->get('annually')) ? $request->get('annually') : '',
            'biennially' => !empty($request->get('biennially')) ? $request->get('biennially') : '',
            'triennially' => !empty($request->get('triennially')) ? $request->get('triennially') : '',
            'annually_exp' => !empty($request->get('annually_exp')) ? $request->get('annually_exp') : '',
            'biennially_exp' => !empty($request->get('biennially_exp')) ? $request->get('biennially_exp') : '',
            'triennially_exp' => !empty($request->get('triennially_exp')) ? $request->get('triennially_exp') : '',
            'promotion' => !empty($request->get('promotion')) ? true : false,
        ];
        // dd($data);
        $create_domain = $this->domainProduct->create($data);
        if( $create_domain ) {
            return redirect(route('admin.domain-products.index'))->with('success', 'Tạo sản phẩm domain thành công');
        } else {
            return redirect(route('admin.domain-products.index'))->with('error', 'Tạo sản phẩm domain thất bại');
        }
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        $delete = $this->domainProduct->delete($id);
        return $delete;
    }

    public function multidelete(Request $request)
    {
        $id = $request->get('id');
        $delete = $this->domainProduct->multidelete($id);
        return $delete;
    }

    public function edit($id)
    {
        $domainproduct = $this->domainProduct->detail($id);
        return view('admin.domains.edit', compact('domainproduct'));
    }
    public function update(DomainCreateValidate $request)
    {
        $id = $request->get('id');
        $data = [
            'type' =>  $request->get('type'),
            'local_type' => $request->get('local_type'),
            'annually' => $request->get('annually'),
            'biennially' => $request->get('biennially'),
            'triennially' => $request->get('triennially'),
            'annually_exp' => !empty($request->get('annually_exp')) ? $request->get('annually_exp') : '',
            'biennially_exp' => !empty($request->get('biennially_exp')) ? $request->get('biennially_exp') : '',
            'triennially_exp' => !empty($request->get('triennially_exp')) ? $request->get('triennially_exp') : '',
            'promotion' => !empty($request->get('promotion')) ? true : false,
        ];
        $domainproduct = $this->domainProduct->update($id, $data);
        if($domainproduct) {
            return redirect(route('admin.domain-products.index'))->with('success', 'Cập nhập thành công');
        } else {
            return redirect(route('admin.domain-products.index'))->with('error', 'Cập nhập thất bại');
        }
    }

}
