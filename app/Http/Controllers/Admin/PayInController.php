<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;
use App\Model\User;

class PayInController extends Controller
{
    protected $payment;
    protected $user;
    public function __construct()
    {
        $this->payment = AdminFactories::paymentRepositories();
        $this->user = new User;
    }

    public function index(Request $request) {
        $method = $request->get('method');
        $user_id = $request->get('user_id');
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        $payments = $this->payment->get_payments('pay', $user_id);
        $users = $this->user->get();
        return view('admin.payments.index', compact('payments', 'pay_in', 'type_payin', 'method', 'users'));
    }

    public function invoice(Request $request) {
        $method = $request->get('method');
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        $payments = $this->payment->get_payments($method);
        // dd($payments);
        $users = $this->user->get();
        return view('admin.payments.invoice', compact('payments', 'pay_in', 'type_payin', 'method', 'users'));
    }

    public function delete_payment(Request $request) {
        $id = $request->get('id');
        $delete = $this->payment->delete_payment($id);
        return json_encode($delete);
    }

    public function detail($id) {
        $payment = $this->payment->detail($id);
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        $momo_status = config('momo_status');
        $content = unserialize($payment->content);
        $billings = config('billing');
        $detail_order = $payment->detail_order;
        if ($payment->method_gd == 'domain') {
            return view('admin.payments.detail_domain', compact('payment', 'pay_in', 'type_payin', 'momo_status', 'content', 'billings'));
        } elseif ($payment->method_gd == 'domain_exp') {
            return view('admin.payments.detail_domain_exp', compact('payment', 'pay_in', 'type_payin', 'momo_status', 'content', 'billings'));
        } else {
            return view('admin.payments.detail', compact('payment', 'pay_in', 'type_payin', 'momo_status', 'content', 'billings'));
        }
    }

    public function confirm_payment(Request $request)
    {
        $id = $request->get('id');
        $confirm = $this->payment->confirm_payment($id);
        return json_encode($confirm);
    }

    public function select_user(Request $request)
    {
        $method = $request->get('type');
        $user_id = $request->get('user_id');

        return redirect()->route('admin.payments.select_with_user', [$user_id,$method]);

    }

    public function select_with_user($user_id , $method)
    {
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        $payments = $this->payment->get_payment_with_user($method, $user_id);
        // dd($user_id, $method, $payments);
        $users = $this->user->get();
        $user_select = $this->user->find($user_id);
        if ( $method == 'pay' || $method == 'all' || $method == null ) {
          return view('admin.payments.select_with_user_pay', compact('user_select','payments', 'pay_in', 'type_payin', 'method', 'users'));
        } else {
          return view('admin.payments.select_with_invoice', compact('user_select','payments', 'pay_in', 'type_payin', 'method', 'users'));
        }
    }

    public function payment_with_user(Request $request)
    {
       $data = $this->payment->payment_with_user($request->get('user_id'), $request->get('type'), $request->get('status'), $request->get('sl'));
       return json_encode($data);
    }

    public function list_payment_with_qtt(Request $request)
    {
       $data = $this->payment->list_payment_with_qtt($request->get('user_id'), $request->get('type'), $request->get('status'), $request->get('sl'), $request->get('q'));
       return json_encode($data);
    }

    public function list_payment_credit_of_personal()
    {
       $data = $this->payment->list_payment_credit_of_personal();
       return json_encode($data);
    }

    public function list_payment_credit_of_enterprise()
    {
       $data = $this->payment->list_payment_credit_of_enterprise();
       return json_encode($data);
    }

    public function list_payment_addon_of_personal()
    {
       $data = $this->payment->list_payment_addon_of_personal();
       return json_encode($data);
    }

    public function list_payment_addon_of_enterprise()
    {
       $data = $this->payment->list_payment_addon_of_enterprise();
       return json_encode($data);
    }

    public function list_payment_change_ip_of_personal()
    {
       $data = $this->payment->list_payment_change_ip_of_personal();
       return json_encode($data);
    }

    public function list_payment_change_ip_of_enterprise()
    {
       $data = $this->payment->list_payment_change_ip_of_enterprise();
       return json_encode($data);
    }

    public function list_payment_upgrade_hosting_of_personal()
    {
       $data = $this->payment->list_payment_upgrade_hosting_of_personal();
       return json_encode($data);
    }

    public function list_payment_upgrade_hosting_of_enterprise()
    {
       $data = $this->payment->list_payment_upgrade_hosting_of_enterprise();
       return json_encode($data);
    }

    public function list_payment_create_of_personal()
    {
       $data = $this->payment->list_payment_create_of_personal();
       return json_encode($data);
    }

    public function list_payment_create_of_enterprise()
    {
       $data = $this->payment->list_payment_create_of_enterprise();
       return json_encode($data);
    }

    public function list_payment_create_domain_of_personal()
    {
       $data = $this->payment->list_payment_create_domain_of_personal();
       return json_encode($data);
    }

    public function list_payment_domain_create_of_enterprise()
    {
       $data = $this->payment->list_payment_domain_create_of_enterprise();
       return json_encode($data);
    }

    public function list_payment_expire_domain_of_personal()
    {
       $data = $this->payment->list_payment_expire_domain_of_personal();
       return json_encode($data);
    }

    public function list_payment_domain_expire_of_enterprise()
    {
       $data = $this->payment->list_payment_domain_expire_of_enterprise();
       return json_encode($data);
    }

    public function list_payment_expired_of_personal()
    {
       $data = $this->payment->list_payment_expired_of_personal();
       return json_encode($data);
    }

    public function list_payment_expired_of_enterprise()
    {
       $data = $this->payment->list_payment_expired_of_enterprise();
       return json_encode($data);
    }

    public function list_payment_all_of_personal()
    {
       $data = $this->payment->list_payment_all_of_personal();
       return json_encode($data);
    }

    public function list_payment_all_of_enterprise()
    {
       $data = $this->payment->list_payment_all_of_enterprise();
       return json_encode($data);
    }

    public function list_payment_with_type(Request $request)
    {
       $data = $this->payment->list_payment_with_type($request->get('user_id'), $request->get('type'), $request->get('status'), $request->get('sl'));
       return json_encode($data);
    }

    public function list_payment_with_status(Request $request)
    {
       $data = $this->payment->list_payment_with_status($request->get('user_id'), $request->get('type'), $request->get('status'), $request->get('sl'));
       return json_encode($data);
    }

    public function invoice_create(Request $request) {
        $method = 'invoice';
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        $payments = $this->payment->get_payments('invoice');
        $users = $this->user->get();
        return view('admin.payments.invoice_create', compact('payments', 'pay_in', 'type_payin', 'method', 'users'));
    }

    public function invoice_addon(Request $request) {
        $method = 'addon';
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        $payments = $this->payment->get_payments('addon');
        $users = $this->user->get();
        return view('admin.payments.invoice_addon', compact('payments', 'pay_in', 'type_payin', 'method', 'users'));
    }

    public function invoice_upgrade_hosting(Request $request) {
        $method = 'upgrade_hosting';
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        $payments = $this->payment->get_payments('upgrade_hosting');
        $users = $this->user->get();
        return view('admin.payments.invoice_upgrade_hosting', compact('payments', 'pay_in', 'type_payin', 'method', 'users'));
    }

    public function invoice_change_id(Request $request) {
        $method = 'change_id';
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        $payments = $this->payment->get_payments('change_id');
        $users = $this->user->get();
        return view('admin.payments.invoice_change_id', compact('payments', 'pay_in', 'type_payin', 'method', 'users'));
    }

    public function invoice_expired(Request $request) {
        $method = 'expired';
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        $payments = $this->payment->get_payments('expired');
        $users = $this->user->get();
        return view('admin.payments.invoice_expired', compact('payments', 'pay_in', 'type_payin', 'method', 'users'));
    }

    public function invoice_domain(Request $request) {
        $method = 'domain';
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        $payments = $this->payment->get_payments('domain');
        $users = $this->user->get();
        return view('admin.payments.invoice_domain', compact('payments', 'pay_in', 'type_payin', 'method', 'users'));
    }

    public function invoice_domain_exp(Request $request) {
        $method = 'domain_exp';
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        $payments = $this->payment->get_payments('domain_exp');
        $users = $this->user->get();
        return view('admin.payments.invoice_domain_exp', compact('payments', 'pay_in', 'type_payin', 'method', 'users'));
    }

    public function all(Request $request) {
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        $payments = $this->payment->get_payments('');
        $users = $this->user->get();
        return view('admin.payments.all', compact('payments', 'pay_in', 'type_payin', 'users'));
    }

    public function list_payment_create_with_qtt(Request $request)
    {
      $data = $this->payment->list_payment_create_with_qtt($request->get('user_id'), $request->get('type'), $request->get('status'), $request->get('sl'));
      return json_encode($data);
    }

    public function list_payment_create_with_user(Request $request)
    {
      $data = $this->payment->list_payment_create_with_qtt($request->get('user_id'), $request->get('type'), $request->get('status'), $request->get('sl'));
      return json_encode($data);
    }

    public function list_payment_addon_with_qtt(Request $request)
    {
      $data = $this->payment->list_payment_addon_with_qtt($request->get('user_id'), $request->get('type'), $request->get('status'), $request->get('sl'));
      return json_encode($data);
    }

    public function list_upgrade_hosting_with_qtt(Request $request)
    {
      $data = $this->payment->list_upgrade_hosting_with_qtt($request->get('user_id'), $request->get('type'), $request->get('status'), $request->get('sl'));
      return json_encode($data);
    }

    public function list_change_ip_with_qtt(Request $request)
    {
      $data = $this->payment->list_change_ip_with_qtt($request->get('user_id'), $request->get('type'), $request->get('status'), $request->get('sl'));
      return json_encode($data);
    }

    public function list_expired_with_qtt(Request $request)
    {
      $data = $this->payment->list_expired_with_qtt($request->get('user_id'), $request->get('type'), $request->get('status'), $request->get('sl'));
      return json_encode($data);
    }

    public function list_domain_with_qtt(Request $request)
    {
      $data = $this->payment->list_domain_with_qtt($request->get('user_id'), $request->get('type'), $request->get('status'), $request->get('sl'));
      return json_encode($data);
    }

    public function list_domain_exp_with_qtt(Request $request)
    {
      $data = $this->payment->list_domain_exp_with_qtt($request->get('user_id'), $request->get('type'), $request->get('status'), $request->get('sl'));
      return json_encode($data);
    }

    public function list_all_with_qtt(Request $request)
    {
      $data = $this->payment->list_all_with_qtt($request->get('user_id'), $request->get('type'), $request->get('status'), $request->get('sl'), $request->get('method'));
      return json_encode($data);
    }


}
