<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;
use App\Http\Requests\AddVpsValidate;
use Excel;
use App\Exports\ExportVpsUs;

class VpsUSController extends Controller
{
    protected $vps;
    protected $product;
    protected $order;

    public function __construct()
    {
        $this->vps = AdminFactories::vpsRepositories();
        $this->product = AdminFactories::productRepositories();
        $this->order = AdminFactories::orderRepositories();
    }

    public function index()
    {
        $list_vps = $this->vps->get_vps_us();
        $billings = config('billing');
        // dd($list_vps);
        return view('admin.vps_us.index', compact('list_vps', 'billings'));
    }


    public function vps_us_detail($id)
    {
      $detail = $this->vps->detail_vps($id);
      $log_payments = $this->vps->log_payment_with_vps($id);
      $log_activities = $this->vps->log_activities_with_vps($id);
      $billings = config('billing');
      $users = $this->order->users();
      $products = $this->product->get_product_with_action($detail->detail_order->type, $detail->user_id);
      $status_vps = config('status_vps');
      return view('admin.vps_us.edit', compact('detail', 'products', 'billings', 'users', 'status_vps', 'log_payments', 'log_activities'));
    }

    // sevices update
    public function update_sevices(Request $request)
    {
        $validate = $request->validate([
            'user_id' => 'bail|required',
            'product_id' => 'bail|required|not_in:0',
            'ip' => 'bail|required',
            'sevices_username' => 'bail|required',
            'sevices_password' => 'bail|required',
        ],
        [
            'user_id.required' => 'Tên khách hàng không được để trống',
            'product_id.required' => 'Sản phẩm không được để trống',
            'product_id.required' => 'Sản phẩm không được để trống',
            'ip.required' => 'IP không được để trống',
            'sevices_username.required' => 'User không được để trống',
            'sevices_password.required' => 'Mật khẩu không được để trống',
        ] );
        $data = $request->all();
        // dd($data);
        if ($data['type'] == 'vps') {
            $update_sevice = $this->vps->update_sevice_vps_us($data);
            if ($update_sevice) {
                return redirect()->route('admin.vps_us.index')->with('success', 'Chỉnh sửa VPS thành công');
            } else {
                return redirect()->route('admin.vps_us.detail', $data['id'])->with('fails', 'Chỉnh sửa VPS thất bại');
            }
        }

    }


    public function select_status_vps(Request $request)
    {
      $q = $request->get('q');
      $status = $request->get('status');
      $qtt = $request->get('qtt');
      $data = $this->vps->select_status_vps_us($q, $status, 'desc', $qtt);
      return json_encode($data);
    }

    public function search(Request $request)
    {
      $q = $request->get('q');
      $status = $request->get('status');
      $qtt = $request->get('qtt');
      $data = $this->vps->search_vps_us($q, $status, $qtt);
      return json_encode($data);
    }

    public function list_vps_of_personal()
    {
        $data = $this->vps->list_vps_us_of_personal();
        return json_encode($data);
    }

    public function list_vps_of_enterprise()
    {
        $data = $this->vps->list_vps_us_of_enterprise();
        return json_encode($data);
    }

    public function delete_ajax_vps(Request $request)
    {
        $id = $request->get('id');
        $action = $request->get('action');
        // return json_encode(true);
        switch ($action) {
            case 'delete':
                $delete_vps = $this->vps->delete_vps($id);
                if($delete_vps) {
                    return json_encode(true);
                } else {
                    return json_encode(false);
                }
                break;
            case 'on':
                $off_vps = $this->vps->on_vps_us($id);
                if($off_vps) {
                    return json_encode(true);
                } else {
                    return json_encode(false);
                }
                break;
            case 'restart':
                $off_vps = $this->vps->restart_vps_us($id);
                if($off_vps) {
                    return json_encode(true);
                } else {
                    return json_encode(false);
                }
                break;
            case 'off':
                $off_vps = $this->vps->off_vps_us($id);
                if($off_vps) {
                    return json_encode(true);
                } else {
                    return json_encode(false);
                }
                break;
            default:
                return false;
                break;
        }
    }

    public function action(Request $request)
    {
        $vps_id = $request->get('vps_id');
        $action = $request->get('action');

        if ($action == 'on') {
            $result = $this->vps->us_on_action($vps_id);
        }
        elseif ($action == 'off') {
            $result = $this->vps->us_off_action($vps_id);
        }
        elseif ($action == 'restart') {
            $result = $this->vps->us_restart_action($vps_id);
        }
        elseif ($action == 'delete') {
            $result = $this->vps->delete_action($vps_id);
        } else {
            $result = $this->vps->cancel_action($vps_id);
        }
        return json_encode($result);
    }

    public function search_multi_vps_us(Request $request)
    {
        $q = explode(',', $request->get('q'));
        $data = $this->vps->search_multi_vps_us($q);
        return json_encode($data);
    }
    
    public function change_user(Request $request)
    {
        $list_id_vps = explode(',', $request->get('id'));
        if ( is_array($list_id_vps) ) {
            $list_vps = [];
            $check_vps = $this->vps->check_mutil_vps_with_change_user($list_id_vps);
            if ( $check_vps['error'] == 0 ) {
                $list_vps = $check_vps['content'];
                $list_user = $this->vps->list_all_user();
                return view('admin.vps_us.change_user', compact('list_vps', 'list_user'));
            }
            else {
                if ( $check_vps['error'] == 1 ) {
                    return redirect()->route('admin.vps_us.index')->with('fails', $check_vps['text_error']);
                }
                elseif ( $check_vps['error'] == 2 ) {
                    return redirect()->route('admin.vps_us.index')->with('fails', $check_vps['text_error']);
                }
                else {
                    return redirect()->route('admin.vps_us.index')->with('fails', $check_vps['text_error']);
                }
            }
        } else {
            $check_vps = $this->vps->check_vps_with_change_user($list_id_vps);
            if ( $check_vps['error'] == 0 ) {
                $list_vps = $check_vps['content'];
                $list_user = $this->vps->list_all_user();
                return view('admin.vps_us.change_user', compact('list_vps', 'list_user'));
            } else {
                if ( $check_vps['error'] == 1 ) {
                    return redirect()->route('admin.vps_us.index')->with('fails', 'Không có VPS được chọn trong dữ liệu.');
                }
                elseif ( $check_vps['error'] == 2 ) {
                    return redirect()->route('admin.vps_us.index')->with('fails', 'VPS chưa được tạo.');
                }
                else {
                    return redirect()->route('admin.vps_us.index')->with('fails', 'VPS được chọn không được ở trạng thái "đã xóa" - "đã chuyển" - "đã hủy". Vui lòng kiểm tra lại.');
                }
            }
        }
    }

    public function export_excel(Request $request)
    {
        $list_id_vps = explode(',', $request->get('id'));
        if ( is_array($list_id_vps) ) {
            $list_vps = [];
            $check_vps = $this->vps->check_mutil_vps_with_export_excel($list_id_vps);
            if ( $check_vps['error'] == 0 ) {
                $list_vps = $check_vps['content'];
                return view('admin.vps_us.export_excel', compact('list_vps'));
            }
            else {
                if ( $check_vps['error'] == 1 ) {
                    return redirect()->back()->with('fails', $check_vps['text_error']);
                }
                elseif ( $check_vps['error'] == 2 ) {
                    return redirect()->back()->with('fails', $check_vps['text_error']);
                }
                else {
                    return redirect()->back()->with('fails', $check_vps['text_error']);
                }
            }
        } else {
            return redirect()->back()->with('fails', 'Không có VPS được chọn trong dữ liệu.');
        }
    }

    public function form_export_excel(Request $request)
    {
        $list_id_vps = $request->get('id');
        $check_vps = $this->vps->check_mutil_vps_with_export_excel($list_id_vps);
        if ( $check_vps['error'] == 0 ) {
            $data = $this->vps->export_excel($list_id_vps, $request->all());
            
            if ( !empty($request->get('file_name')) ) {
                $unicode = array(
                    'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
                    'd'=>'đ|Đ',
                    'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
                    'i'=>'í|ì|ỉ|ĩ|ị|I|Ì|Í|Ị|Ỉ|Ĩ',
                    'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ọ|Ó|Ò|Ỏ|Õ|Ô|Ồ|Ố|Ổ|Ỗ|Ộ|Ơ|Ờ|Ớ|Ở|Ỡ|Ợ',
                    'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ụ|Ũ|Ư|Ừ|Ứ|Ử|Ữ|Ự',
                    'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
                );
                $str_name = strtolower( $request->get('file_name') );
                foreach($unicode as $nonUnicode => $uni){
                    $str_name = preg_replace( "/($uni)/i" , $nonUnicode , $str_name);
                }
                $str_name = str_replace( ' ', '_', $str_name );
            }
            else {
                $str_name = 'vps_us_' . date('m_d_Y');
            }
            $path = 'public/excel/' . $str_name . '.xlsx';
            $export = new ExportVpsUs($data);
            Excel::store($export, $path);
            return json_encode(
                [
                    'error' => 0,
                    'content' => '/storage/excel/' . $str_name . '.xlsx' , 
                    'file_name' => $str_name . '.xlsx',
                ]
            );
        } else {
            return json_encode(
                [
                    'error' => 1,
                    'content' => $check_vps['text_error'],
                    'file_name' => ''
                ]
            );
        }
    }

    public function form_change_user(Request $request)
    {
        // dd($request->all());
        $list_id_vps = $request->get('id');
        $check_vps = $this->vps->check_mutil_vps_with_change_user($list_id_vps);
        if ( $check_vps['error'] == 0 ) {
            $change = $this->vps->change_user_by_vps_us($request->all());
            if ( $change['error'] == 0 ) {
                return redirect()->route('admin.vps_us.index')->with('success', 'Chuyển VPS cho khách hàng ' . $change['user']['email'] . ' thành công.');
            } 
            elseif ( $change['error'] == 1 ) {
                return redirect()->back()->with('fails', 'Hệ thống đang bận. Quý khách vui lòng quay lại sau.');
            }
            else {
                return redirect()->back()->with('fails', 'Chuyển VPS cho khách hàng ' . $change['user']['email'] . ' thất bại.');
            }
        }
        else {
            if ( $check_vps['error'] == 1 ) {
                return redirect()->back()->with('fails', $check_vps['text_error']);
            }
            elseif ( $check_vps['error'] == 2 ) {
                return redirect()->back()->with('fails', $check_vps['text_error']);
            }
            else {
                return redirect()->back()->with('fails', $check_vps['text_error']);
            }
        }
    }

}
