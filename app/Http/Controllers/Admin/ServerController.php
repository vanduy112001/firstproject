<?php

namespace App\Http\Controllers\Admin;

use App\Model\Server;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;
use App\Http\Requests\CreateServerValidate;

class ServerController extends Controller
{
    protected $server;
    protected $product;
    protected $order;

    public function __construct()
    {
        $this->server = AdminFactories::serverRepositories();
        $this->product = AdminFactories::productRepositories();
        $this->order = AdminFactories::orderRepositories();
    }

    public function index()
    {
        $servers = $this->server->list_servers();
        $billings = config('billing');
        return view('admin.servers.index', compact('servers', 'billings'));
    }

    public function detail($id)
    {
        $detail = $this->server->detail_server($id);
        $billings = config('billing');
        $users = $this->order->users();
        $products = $this->product->get_product_with_action('Server', $detail->user_id);
        $config_os_server = config('listOsServer');
        $config_raid = config('raid');
        $config_datacenter = config('datacenter');
        $list_add_on_disk_server = $this->product->list_add_on_disk_server($detail->user_id);
        $list_add_on_ram_server = $this->product->list_add_on_ram_server($detail->user_id);
        $list_add_on_ip_server = $this->product->list_add_on_ip_server($detail->user_id);
        return view('admin.servers.edit', compact('detail', 'billings', 'users', 'config_os_server', 'config_raid',
        'config_datacenter', 'list_add_on_disk_server', 'list_add_on_ram_server', 'list_add_on_ip_server', 'products'));
    }

    public function update(Request $request)
    {
        // dd($request->all());
        $update = $this->server->update($request->all());
        if ( $update ) {
          return redirect()->route('admin.server.index')->with('success' , 'Chỉnh sửa Server thành công');
        } else {
          return redirect()->route('admin.server.edit', $request->get('id'))->with('fails' , 'Chỉnh sửa Server thất bại');
        }
    }

    public function delete_ajax_server(Request $request)
    {
        $action = $request->get('action');
        switch ($action) {
            case 'delete':
              if($request->filled('data_ids')){
                $delete = $this->server->delete($request->get('data_ids'),'multi');
              }else{
                $delete = $this->server->delete($request->get('id'),'single');
              }
                return json_encode($delete);
                break;

            default:
                return json_encode([
                    'error' => 1,
                    'action' => false,
                ]);
                break;
        }
    }

    public function create(Request $request)
    {
      $billings = config('billing');
      $users = $this->order->users();
      $config_os_server = config('listOsServer');
      $config_raid = config('raid');
      $config_datacenter = config('datacenter');
      return view('admin.servers.create', compact( 'billings', 'users', 'config_os_server', 'config_raid',
      'config_datacenter'));
    }

    public function list_product(Request $request)
    {
      $list_products = $this->server->listProductByUser($request->get('user_id'));
      return json_encode($list_products);
    }

    public function list_addon_server(Request $request)
    {
      $list_products = $this->server->listAddonServer($request->get('user_id'), $request->get('product_id'));
      return json_encode($list_products);
    }

    public function loadTotal(Request $request)
    {
      $loadTotal = $this->server->loadTotal($request->all());
      return json_encode($loadTotal);
    }

    public function store(CreateServerValidate $request)
    {
      $createServer = $this->server->createServer($request->all());
      if ( $createServer ) {
        return redirect()->route('admin.server.index')->with('success', 'Thêm server thành công');
      } else {
        return redirect()->back()->with('fails', 'Thêm server thất bại');
      }
    }

    public function sendMailFinishConfig($serverId)
    {
        $server = $this->server->detail_server($serverId);
        $sendMailFinishConfig = $this->server->loadMailFinishConfig($serverId);
        if ( $sendMailFinishConfig['error'] ) {
          return redirect()->route('admin.server.index')->with('fails', 'Chưa thiết lập Email hoàn thành cài đặt cho sản phẩm');
        }
        return view('admin.servers.send_mail', compact( 'server', 'sendMailFinishConfig'));
    }

    public function sendMail(Request $request)
    {
        $sendMail = $this->server->sendMail($request->all());
        if ( !$sendMail ) {
          return redirect()->route('admin.server.index')->with('fails', 'Gửi mail thất bại');
        }
        return redirect()->route('admin.server.index')->with('success', 'Gửi mail thành công');
    }

    public function listProxy()
    {
        return view('admin.proxies.index');
    }

    public function getProxy(Request $request)
    {
      $listProxy = $this->server->listProxy($request->all());
      return json_encode($listProxy);
    }

    public function proxy_detail(Request $request, $id)
    {
      $detail = $this->server->detail_proxy($id);
      $billings = config('billing');
      $users = $this->order->users();
      return view('admin.proxies.detail', compact('detail', 'billings', 'users'));
    }

    public function updateProxy(Request $request)
    {
        $update = $this->server->updateProxy($request->all());
        if ( $update ) {
          return redirect()->route('admin.proxy.listProxy')->with('success' , 'Chỉnh sửa Proxy thành công');
        } else {
          return redirect()->back()->with('fails' , 'Chỉnh sửa Proxy thất bại');
        }
    }

    public function action_proxy(Request $request)
    {
      $action = $request->get('action');
      switch ($action) {
        case 'delete':
            return json_encode( $this->server->deleteProxy($request->get('id')) );
          break;
        
        default:
          return false;
          break;
      }
    }

    public function getServer(Request $request)
    {
      $servers = $this->server->listServer($request->all());
      return json_encode($servers);
    }
}
