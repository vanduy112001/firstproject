<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;

class MessageController extends Controller
{
    protected $message;
    protected $user;

    public function __construct()
    {
        $this->message = AdminFactories::messageRepositories();
        $this->user = AdminFactories::userRepositories();
    }

    public function index()
    {
    	$list_message = $this->message->list_message();
    	$users = $this->user->all();
    	return view('admin.messages.index', compact('list_message', 'users'));
    }

    public function loadMessageByUser(Request $request)
    {
    	$list_message = $this->message->loadMessageByUser($request->get('user_id'));
    	return json_encode($list_message);
    }

    public function sendMessage(Request $request)
    {
        $sendMessage = $this->message->sendMessage($request->all());
        return json_encode($sendMessage);
    }

    public function userChat(Request $request)
    {
        try {
            $data_event = json_decode($request->get('message'))->data_event;
            $data_reponse = $this->message->userChat(json_decode($data_event));
            return json_encode($data_reponse);
        } catch (Exception $e) {
            report($e);
            return [
                "error" => 1,
                "content" => 'lỗi chat user'
            ];
        }
    }
}
