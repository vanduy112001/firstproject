<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;

class SendMailController extends Controller
{
    protected $email;

    public function __construct()
    {
        $this->email = AdminFactories::emailRepositories();
    }

    public function index()
    {
        return view('admin.send_mail.index');
    }


    public function load_mail()
    {
        return json_encode( $this->email->load_send_mail() );
    }

    public function detail_mail(Request $request)
    {
        return json_encode( $this->email->load_detail_send_mail($request->get('id')) );
    }

    public function loadFilterSendMail(Request $request)
    {
        $qtt = $request->get('qtt');
        $user = $request->get('user');
        $action = $request->get('action');
        $date = $request->get('date');
        return json_encode( $this->email->loadFilterSendMail( $qtt, $user, $action, $date ) );
    }

}
