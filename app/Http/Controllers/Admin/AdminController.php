<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;

class AdminController extends Controller
{
    protected $order;
    protected $home;
    protected $payment;

    public function __construct()
    {
        $this->order = AdminFactories::orderRepositories();
        $this->home = AdminFactories::homeRepositories();
        $this->payment = AdminFactories::paymentRepositories();
    }

    public function index()
    {
        $total_ticket_pending = $this->home->total_ticket_pending();
        return view('admin.home', compact('total_ticket_pending'));
    }

    public function load_siderbar_top()
    {
        $data = [
          'total_order_pending' => $this->order->total_order_pending(),
          'total_order_confirm' => $this->order->total_order_active(),
          'total_vps_before_termination' => $this->order->total_vps_before_termination(),
          'total_vps_us_before_termination' => $this->order->total_vps_us_before_termination(),
          'total_hosting_before_termination' => $this->order->total_hosting_before_termination(),
          'total_server_before_termination' => $this->order->total_server_before_termination(),
          'total_colo_before_termination' => $this->order->total_colo_before_termination(),
          'total_email_hosting_termination' => $this->order->total_email_hosting_termination(),
        ];
        return json_encode($data);
    }

    public function load_all_services()
    {
        $data_sevices = $this->home->data_sevices(); //list tổng số vps - hosting - server
        return json_encode($data_sevices);
    }

    public function load_tong_thu()
    {
        $data_sevices = $this->home->data_total_price(); //tổng thu
        return json_encode($data_sevices);
    }

    public function chart_dashboard()
    {
        $result = [
            'vps' => $this->home->total_vps(),
            'vps_us' => $this->home->total_vps_us(),
            'hosting' => $this->home->total_hosting(),
            'server' => $this->home->total_server(),
            'colocation' => $this->home->total_colocation(),
            'email_hosting' => $this->home->total_email_hosting(),
        ];
        return json_encode($result);
    }

    public function chart_total_price()
    {
      $result = [
          'vps' => $this->home->total_price_vps(),
          'vps_us' => $this->home->total_price_vps_us(),
          'hosting' => $this->home->total_price_hosting(),
          'server' => $this->home->total_price_server(),
          'colocation' => $this->home->total_price_colocation(),
          'email_hosting' => $this->home->total_price_email_hosting(),
      ];
      return json_encode($result);
    }
    // Biểu đồ user
    public function chart_user(Request $request)
    {
        // return json_encode(true);
        $type = $request->get("type");
        $sort1 = $request->get("sort1");
        $sort2 = $request->get("sort2");
        $sort3 = $request->get("sort3");
        $result = $this->home->chart_user($type, $sort1, $sort2,$sort3);
        return json_encode($result);
    }
    // Biểu đồ VPS
    public function chart_vps(Request $request)
    {
        // return json_encode(true);
        $type = $request->get("type");
        $sort1 = $request->get("sort1");
        $sort2 = $request->get("sort2");
        $sort3 = $request->get("sort3");
        $result = $this->home->chart_vps($type, $sort1, $sort2, $sort3);
        return json_encode($result);
    }
    // Biểu đồ Hosting
    public function chart_hosting(Request $request)
    {
        // return json_encode(true);
        $type = $request->get("type");
        $sort1 = $request->get("sort1");
        $sort2 = $request->get("sort2");
        $sort3 = $request->get("sort3");
        $result = $this->home->chart_hosting($type, $sort1, $sort2, $sort3);
        return json_encode($result);
    }
    // Biểu đồ Hosting
    public function chart_payment(Request $request)
    {
        // return json_encode(true);
        $type = $request->get("type");
        $sort1 = $request->get("sort1");
        $sort2 = $request->get("sort2");
        $sort3 = $request->get("sort3");
        $result = $this->home->chart_payment($type, $sort1, $sort2, $sort3);
        return json_encode($result);
    }

    public function momo(Request $request)
    {
        if($request->session()->has('login_momo') == 'success') {
            $list_momos = $this->home->list_momo();
            return view('admin.momo.index', compact('list_momos'));
        } else {
            return view('admin.momo.login_momo');
        }
    }

    public function login_momo(Request $request)
    {
        $check_login = $this->home->login_momo($request->all());
        // dd($check_login);
        if ($check_login == true) {
          $request->session()->put('login_momo', 'success');
          return redirect()->route('admin.momo.index');
        } else {
          return view('admin.momo.login_momo')->with('fails', 'Đăng nhập thất bại');
        }
    }

    public function search_momo(Request $request)
    {
        $data = $this->home->search_momo($request->all());
        return json_encode($data);
    }

    public function edit_momo($id_momo, Request $request)
    {
        if($request->session()->has('login_momo') == 'success') {
            $momo = $this->home->detail_momo($id_momo);
            if ( !empty($momo) ) {
                return view('admin.momo.edit', compact('momo'));
            } else {
                return redirect()->route('admin.momo.index')->with('fails', 'Không có giao dịch MoMo này trong dữ liệu');
            }
        } else {
            return view('admin.momo.login_momo');
        }
    }

    public function update_momo(Request $request)
    {
        $momo = $this->home->update_momo($request->all());
        $confirm_payment = $this->payment->update_confirm_payment_with_momo($momo);
        if ($confirm_payment) {
            return redirect()->route('admin.momo.index')->with('success', 'Cập nhật giao dịch MoMo và xác nhận lại thanh toán thành công');
        } else {
            return redirect()->route('admin.momo.index')->with('fails', 'Xác nhận giao dịch thất bại');
        }

    }

}
