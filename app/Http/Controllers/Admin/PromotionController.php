<?php

namespace App\Http\Controllers\Admin;

use App\Model\Promotion;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;
use App\Http\Requests\PromotionValidate;

class PromotionController extends Controller
{
    protected $promotion;

    public function __construct()
    {
        $this->promotion = AdminFactories::promotionRepositories();
        $this->product = AdminFactories::productRepositories();
    }

    public function index()
    {
        $promotions = $this->promotion->list();
        $type_promotion = config('type_promotion');
        return view('admin.promotions.index', compact('promotions', 'type_promotion'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type_promotion = config('type_promotion');
        $group_products = $this->product->get_all_group_product();
        $billings = config('billing');
        return view('admin.promotions.create', compact('type_promotion', 'group_products', 'billings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PromotionValidate $request)
    {
        $create = $this->promotion->create($request->all());
        if ($create) {
          return redirect()->route('admin.promotion.index')->with('success', 'Tạo mã khuyến mãi thành công');
        } else {
          return redirect()->route('admin.promotions.create')->with('fails', 'Tạo mã khuyến mãi thất bại');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function show(Promotion $promotion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $detail = $this->promotion->detail_promotion($id);
        $type_promotion = config('type_promotion');
        $group_products = $this->product->get_all_group_product();
        $billings = config('billing');
        return view('admin.promotions.edit', compact('type_promotion', 'detail', 'group_products', 'billings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function update(PromotionValidate $request)
    {
        $update = $this->promotion->update($request->all());
        if ($update) {
          return redirect()->route('admin.promotion.index')->with('success', 'Chỉnh sửa mã khuyến mãi thành công');
        } else {
          return redirect()->route('admin.promotions.update', $request->get('id'))->with('fails', 'Chỉnh sửa mã khuyến mãi thất bại');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        $delete = $this->promotion->delete($request->get('id'));
        return json_encode($delete);
    }
}
