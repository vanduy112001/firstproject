<?php

namespace App\Http\Controllers\Admin;

use App\Model\CmndVerify;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;

class CmndVerifyController extends Controller
{
    protected $userRepository;
    protected $cmndRepository;

    public function __construct()
    {
        $this->userRepository = AdminFactories::userRepositories();
        $this->cmndRepository = AdminFactories::cmndRepositories();
    }

    public function index()
    {
        // $users = $this->userRepository->all();
        $cmnds = $this->cmndRepository->get_all();
        return view('admin.cmnd_verifies.list', compact('cmnds'));
    }

    public function detail(Request $request)
    {
        $id = $request->get('id');
        $cmnd = $this->cmndRepository->detail($id);
        return $cmnd;
    }

    public function confirm(Request $request)
    {
        $id = $request->get('id');
        $cmnd = $this->cmndRepository->confirm($id);
        return redirect()->route('admin.users.cmnd_verifies')->with($cmnd);
    }

    public function delete(Request $request)
    {
        if (!empty($request->get('id'))) {
            $id = $request->get('id');
            $cmnd = $this->cmndRepository->delete($id);
            return $cmnd;
        }
    }

    public function multidelete(Request $request)
    {
        if (!empty($request->get('id'))) {
            $id = $request->get('id');
            $delete = $this->cmndRepository->multidelete($id);
            return $delete;
        }
    }

    public function deactive(Request $request)
    {
        $id = $request->get('id');
        $cmnd = $this->cmndRepository->deactive($id);
        return redirect()->route('admin.users.cmnd_verifies')->with($cmnd);
    }
}
