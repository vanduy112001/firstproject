<?php

namespace App\Http\Controllers\Admin;

use App\Factories\AdminFactories;
use Illuminate\Http\Request;
use App\Http\Requests\DomainCreateValidate;
use App\Http\Requests\GetDomainPaValidate;
use Validator;

class DomainController extends Controller
{
    protected $domain;
    protected $order;
    protected $user;
    protected $domain_product;
    //Hàm khởi tạo
    public function __construct()
    {
        $this->domain = AdminFactories::domainRepositories();
        $this->order = AdminFactories::orderRepositories();
        $this->user = AdminFactories::userRepositories();
        $this->domain_product = AdminFactories::domainProductRepositories();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $domains = $this->domain->get_all();
        return view('admin.domains.list', compact('domains'));
    }

    public function get_info_domain(Request $request)
    {
      $domain_name = $request->get('domain');
      $domain = $this->domain->get_info_domain( $domain_name );
      // $domain = [];
      $billings = config('billing');
      $provinces = config('city');
      $users = $this->order->users();
      $domain_products = $this->domain_product->all();
      return view('admin.domains.get_info_domain', compact('domain', 'users', 'billings', 'provinces', 'domain_products', 'domain_name'));
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        return $this->domain->delete($id);
    }
    public function multidelete(Request $request)
    {
        $id = $request->get('id');
        $delete = $this->domain->multidelete($id);
        return $delete;
    }
    public function info(Request $request)
    {
        $id = $request->get('id');
        return $this->domain->info($id);
    }

    public function detail_domain($id)
    {
        $billings = config('billing');
        $provinces = config('city');
        $users = $this->order->users();
        $detail = $this->domain->detail($id);
        return view('admin.domains.detail', compact('detail', 'users', 'billings', 'provinces'));
    }

    public function change_pass_domain(Request $request)
    {
        $validated = Validator::make(
            $request->all(),
            [
                'password_domain' => 'bail|required|min:8|max:15|regex: /^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).+$/',
            ],
            [
                'password_domain.required' => 'Mật khẩu không được để trống',
                'password_domain.min' => 'Mật khẩu không được nhỏ hơn 8 kí tự',
                'password_domain.max' => 'Mật khẩu  không được lớn hơn 15 kí tự',
                'password_domain.regex' => 'Mật khẩu  phải bao gồm số và chữ',
            ],
        );
        if ($validated->fails()) {
            return $data = [
                'status' => false,
                'message' => $validated->errors(),
            ];
        } else {
            $id = $request->get('domain-id');
            $pass = $request->get('password_domain');
            return $this->domain->change_pass_domain($id, $pass);
        }
    }

    public function check_account_still()
    {
        return $this->domain->check_account_still();
    }

    public function update_sevices(Request $request)
    {
        $data = $request->all();
        $id =  $data['id'];
        $update = $this->domain->update_domain($id, $data);
        if($update) {
            return redirect(route('admin.domain.detail', $id))->with('success', 'Chỉnh sửa thông tin tên miền thành công');
        } else {
            return redirect(route('admin.domain.detail', $id))->with('error', 'Chỉnh sửa thông tin tên miền thất bại');
        }
    }

    public function create_domain_form(Request $request) {
        $domain = $request->get('domain');
        $billing_cycle = $request->get('billing_cycle');
        $ext = ltrim(strstr($domain, '.'), '.');
        $product = $this->domain_product->get_domain_product_by_ext($ext);
        $billings = config('billing');
        $provinces = config('city');
        $users = $this->user->get_all_users();
        if ($product) {
            return view('admin.domains.create-domain', compact('billings', 'provinces', 'users', 'product', 'billing_cycle', 'domain'));
        } else {
            return redirect(route('admin.domain.index'))->with('error', 'Truy vấn lỗi, vui lòng thử lại');
        }
    }

    public function get_product_domain(Request $request)
    {
        $domain = $request->get('domain');
        $ext = ltrim(strstr($domain, '.'), '.');
        $product = $this->domain_product->get_domain_product_by_ext($ext);
        return $product;
    }

    public function create_domain(Request $request)
    {
        $data = $request->all();
        $domain = $request->get('domain');
        $ext = ltrim(strstr($domain, '.'), '.');
        $product = $this->domain_product->get_domain_product_by_ext($ext);
        $create = $this->domain->create_domain($data, $product);
        if ($create['status'] == true) {
            return redirect(route('admin.domain.index'))->with('success', $create['message']);
        } else {
            return redirect(route('admin.domain.index'))->with('error', $create['message']);
        }
    }

    public function get_domain(Request $request)
    {
        $id = $request->get('id');
        $domain = $this->domain->get_domain($id);
        $due_date = date('Y-m-d');
        $result = $this->domain->reOrderDomain($domain, $due_date);
        return $result;
    }

    public function update_next_due_date(Request $request)
    {
        $id = $request->get('id');
        $next_due_date = $request->get('next_due_date');
        $result = $this->domain->update_next_due_date($id, $next_due_date);
        return json_encode($result);
    }

    public function extend_domain_expired(Request $request)
    {
        $data = $request->all();
        $domain = $request->get('domain');
        $ext = ltrim(strstr($domain, '.'), '.');
        $product = $this->domain_product->get_domain_product_by_ext($ext);

        $extend = $this->domain->extend_domain_expired($data, $product);
        if ($extend['status'] == true) {
            return redirect(route('admin.domain.index'))->with('success', $extend['message']);
        } else {
            return redirect(route('admin.domain.index'))->with('error', $extend['message']);
        }
    }

    public function create_domain_with_PA(GetDomainPaValidate $request)
    {
        // dd($request->all());
        $create_domain = $this->domain->create_domain_with_PA($request->all());
        if ($create_domain) {
            return redirect(route('admin.domain.index'))->with('success', 'Thêm Domain từ PA thành công');
        } else {
            return redirect(route('admin.domain.index'))->with('error', 'Thêm Domain từ PA thất bại');
        }
    }

    public function auto_fill_domain_form(Request $request) 
    {
        $id = $request->get('id');
        $user_data = $this->domain->get_data_user($id);
        return $user_data;
    }

}
