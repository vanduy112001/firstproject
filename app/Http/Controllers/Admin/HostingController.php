<?php

namespace App\Http\Controllers\Admin;

use App\Model\Hosting;
use App\Model\Email;
use App\Model\User;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;
use Mail;
use App\Http\Requests\AddHostingDaPortalValidate;

class HostingController extends Controller
{
    protected $hosting;
    protected $product;
    protected $order;
    protected $invoice;
    protected $email;
    protected $user;
    // API
    protected $da;
    protected $da_si;
    // Send email
    protected $user_send_email;
    protected $subject;


    public function __construct()
    {
        $this->hosting = AdminFactories::hostingRepositories();
        $this->product = AdminFactories::productRepositories();
        $this->order = AdminFactories::orderRepositories();
        $this->da = AdminFactories::directAdminRepositories();
        $this->invoice = AdminFactories::invoiceRepositories();
        $this->email = new Email;
        $this->user = new User;
        $this->da = AdminFactories::directAdminRepositories();
        $this->da_si = AdminFactories::directAdminSingaporeRepositories();
    }

    public function index()
    {
        $hostings = $this->hosting->list_hostings();
        // dd($hostings);
        return view('admin.hostings.index', compact('hostings'));
    }

    public function detail($id)
    {
        $detail = $this->hosting->detail_hosting($id);
        $billings = config('billing');
        $users = $this->order->users();
        $products = $this->product->get_product_with_action('Hosting', $detail->user_id);
        return view('admin.hostings.edit', compact('detail', 'products', 'billings', 'users'));
    }

    public function update_sevices(Request $request)
    {
        $data = $request->all();
        $update_sevice = $this->hosting->update_sevice_hosting($data);
        if ($update_sevice) {
            if ($request->get('services_location') == 'vn') {
                return redirect()->route('admin.hostings.index')->with('success', 'Chỉnh sửa Hosting thành công');
            }
            elseif ($request->get('services_location') == 'si') {
                return redirect()->route('admin.hosting_singapore.index')->with('success', 'Chỉnh sửa Hosting thành công');
            }
        } else {
            return redirect()->route('admin.hosting.detail', $data['id'])->with('fails', 'Chỉnh sửa Hosting thất bại');
        }
    }

    public function action_hosting(Request $request)
    {
        $id  = $request->get('id');
        $type  = $request->get('type');
        // return json_encode($type);
        switch ($type) {
          case 'create':
              $hosting = $this->hosting->detail_hosting($id);
              if ( $hosting->status == 'Pending' ) {
                  $billing = [
                      'monthly' => '1 Month',
                      'quarterly' => '3 Month',
                      'semi_annually' => '6 Month',
                      'annually' => '1 Year',
                      'biennially' => '2 Year',
                      'triennially' => '3 Year'
                  ];
                  $invoice = $this->invoice->detail_invoice_with_services($hosting->detail_order_id);
                  $product = $this->product->detail_product($id);
                  $date_star = $invoice->created_at;
                  $due_date = date('Y-m-d', strtotime( $date_star . $billing[$hosting->billing_cycle] ));
                  if ($hosting->location == 'vn') {
                    $action = $this->da->createHosting($hosting, $due_date);
                  }
                  elseif ($hosting->location == 'si') {
                    $action = $this->da_si->createHosting($hosting, $due_date);
                  }
                  if (!$action) {
                      return json_encode(false);
                  }
                  $this->send_mail_finish($hosting);
                  return json_encode($type);
              }
            break;
          case 'suspend':
              $hosting = $this->hosting->detail_hosting($id);
              if ($hosting->status_hosting  == 'admin_off') {
                  return json_encode('admin-off');
              }
              if ($hosting->location == 'vn') {
                $action = $this->da->adminSuspend($hosting);
              }
              elseif ($hosting->location == 'si') {
                $action = $this->da_si->adminSuspend($hosting);
              }
              if (!$action) {
                  return json_encode(false);
              }
            break;
          case 'unsuspend':
              // return json_encode('da den');
              $hosting = $this->hosting->detail_hosting($id);
              if ($hosting->status_hosting  == 'on') {
                  return json_encode('admin-on');
              }
              if ($hosting->location == 'vn') {
                $action = $this->da->admin_unSuspend($hosting);
              }
              elseif ($hosting->location == 'si') {
                $action = $this->da_si->admin_unSuspend($hosting);
              }
              if (!$action) {
                  return json_encode(false);
              }
            break;
          case 'delete':
              $hosting = $this->hosting->detail_hosting($id);
              if ($hosting->location == 'vn') {
                $action = $this->da->deleteAccount($hosting);
              }
              elseif ($hosting->location == 'si') {
                $action = $this->da_si->deleteAccount($hosting);
              }
              if ( !$action ) {
                return json_encode(false);
              }
            break;
          default:
            return json_encode(false);
            break;
        }
        return json_encode($type);
    }

    public function multiple_action_hostings(Request $request)
    {
        $ids  = $request->get('id_hostings');
        $action  = $request->get('action');
        switch ($action) {
          case 'create':
              if (isset($ids)) {
                foreach ($ids as $key => $id) {
                    $hosting = $this->hosting->detail_hosting($id);
                    if ( $hosting->status == 'Pending' ) {
                        $billing = [
                            'monthly' => '1 Month',
                            'quarterly' => '3 Month',
                            'semi_annually' => '6 Month',
                            'annually' => '1 Year',
                            'biennially' => '2 Year',
                            'triennially' => '3 Year'
                        ];
                        $invoice = $this->invoice->detail_invoice_with_services($hosting->detail_order_id);
                        $product = $this->product->detail_product($id);
                        $date_star = $invoice->created_at;
                        $due_date = date('Y-m-d', strtotime( $date_star . $billing[$hosting->billing_cycle] ));
                        if ($hosting->location == 'vn') {
                          $action_hosting = $this->da->createHosting($hosting, $due_date);
                        }
                        elseif ($hosting->location == 'si') {
                          $action_hosting = $this->da_si->createHosting($hosting, $due_date);
                        }

                        if (!$action_hosting) {
                            return json_encode(false);
                        }
                        $this->send_mail_finish($hosting);
                    }
                }
              } else {
                return json_encode(false);
              }
            break;
          case 'suspend':
              if (isset($ids)) {
                foreach ($ids as $key => $id) {
                  $hosting = $this->hosting->detail_hosting($id);
                  if ($hosting->location == 'vn') {
                    $action_hosting = $this->da->adminSuspend($hosting);
                  }
                  elseif ($hosting->location == 'si') {
                    $action_hosting = $this->da_si->adminSuspend($hosting);
                  }
                  if (!$action_hosting) {
                      return json_encode(false);
                  }
                }
              } else {
                return json_encode(false);
              }
            break;
          case 'unsuspend':
              if ($ids) {
                foreach ($ids as $key => $id) {
                  $hosting = $this->hosting->detail_hosting($id);
                  if ($hosting->location == 'vn') {
                    $action_hosting = $this->da->admin_unSuspend($hosting);
                  }
                  elseif ($hosting->location == 'si') {
                    $action_hosting = $this->da_si->admin_unSuspend($hosting);
                  }
                  if (!$action_hosting) {
                      return json_encode(false);
                  }
                }
              } else {
                return json_encode(false);
              }

            break;
          case 'delete':
              if ($ids) {
                foreach ($ids as $key => $id) {
                  $hosting = $this->hosting->detail_hosting($id);
                  if ($hosting->location == 'vn') {
                    $action_hosting = $this->da->deleteAccount($hosting);
                  }
                  elseif ($hosting->location == 'si') {
                    $action_hosting = $this->da_si->deleteAccount($hosting);
                  }
                  if (!$action_hosting) {
                      return json_encode(false);
                  }
                  $hosting->delete();
                }
              } else {
                return json_encode(false);
              }

            break;
          case 'cancel':
              if ($ids) {
                foreach ($ids as $key => $id) {
                  $hosting = $this->hosting->detail_hosting($id);
                  if ($hosting->location == 'vn') {
                    $action_hosting = $this->da->admin_unSuspend($hosting);
                  }
                  elseif ($hosting->location == 'si') {
                    $action_hosting = $this->da_si->admin_unSuspend($hosting);
                  }
                  $hosting->status_hosting = 'cancel';
                  $hosting->save();
                  return json_encode($action);
                }
              } else {
                return json_encode(false);
              }
            break;
        }
        return json_encode($action);
    }

    public function send_mail_finish($hosting)
    {
          $billings = config('billing');
          $detail_order = $this->invoice->detail_invoice_with_services($hosting->detail_order_id);
          $order = $this->order->detail($detail_order->order_id);
          $product = $this->product->detail_product($hosting->product_id);
          $email = $this->email->find($product->meta_product->email_id);
          $url = url('');
          $url = str_replace(['http://','https://'], ['', ''], $url);
          if ($hosting->location == 'vn') {
            $url_control_panel = $this->da->url_control_panel();
          }
          elseif ($hosting->location == 'si') {
            $url_control_panel = $this->da_si->url_control_panel();
          }
          $add_on =  '';
          if (!empty($detail_order->addon_id)) {
              $add_on = $this->product->detail_product($hosting->addon_id);
          }
          $user = $this->user->find($hosting->user_id);
          $search = [ '{$url_control_panel}','{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
          $replace = [ $url_control_panel ,!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $hosting->domain, '' ,$billings[$hosting->billing_cycle], '', $order->created_at, $hosting->user, $hosting->password, number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' , $detail_order->quantity, '', '', $url];
          $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

          $this->user_send_email = $this->user->find($hosting->user_id)->email;
          $this->subject = $email->meta_email->subject;

          $data = [
                'content' => $content,
                'user_name' => $user->name,
                'user_id' => $user->id,
                'user_email' => $user->email,
                'user_addpress' => $user->user_meta->address,
                'user_phone' => $user->user_meta->user_phone,
                'order_id' => $order->id,
                'product_name' => $product->name,
                'domain' => '',
                'ip' => $hosting->ip,
                'billing_cycle' => $hosting->billing_cycle,
                'next_due_date' => $hosting->next_due_date,
                'order_created_at' => $order->created_at,
                'services_username' => $hosting->user,
                'services_password' => $hosting->password,
                'sub_total' => $detail_order->sub_total,
                'total' => $order->total,
                'qtt' => $detail_order->quantity,
                'os' => $hosting->os,
                'token' => '',
          ];

          $mail = Mail::send('users.mails.orders', $data, function($message){
              $message->from('uthienth92@gmail.com', 'Cloudzone');
              $message->to($this->user_send_email)->subject($this->subject);
          });
    }

    public function getHostingDaPortal($id)
    {
        return view('admin.hostings.getHostingDaPortal', compact('id'));
    }

    public function getHostingSingapore($id)
    {
        return view('admin.hostings.getHostingSingapore', compact('id'));
    }

    public function setHostingWithPortal(AddHostingDaPortalValidate $request)
    {
        // dd($request->all());
        $update_hosting = $this->hosting->setHostingWithPortal($request->all());
        if ($update_hosting) {
            if ($update_hosting['success']) {
                return redirect()->route('admin.hostings.index')->with('success', 'Thêm Hosting từ DAPortal vào Portal thành công');
            } else {
                return redirect()->route('admin.da.addHostingWithPortal', [$request->get('sevices_username'), $request->get('server_hosting_id')])->with('fails', 'Domain này đã có trong Portal');
            }
        } else {
           return redirect()->route('admin.da.addHostingWithPortal', [$request->get('sevices_username'), $request->get('server_hosting_id')])->with('fails', 'Thêm Hosting từ DAPortal vào Portal thất bại');
        }
    }

    public function setHostingSingaporeWithPortal(AddHostingDaPortalValidate $request)
    {
        // dd($request->all());
        $update_hosting = $this->hosting->setHostingWithPortal($request->all());
        if ($update_hosting) {
            if ($update_hosting['success']) {
                return redirect()->route('admin.hosting_singapore.index')->with('success', 'Thêm Hosting từ DA Singapore vào Portal thành công');
            } else {
                return redirect()->route('admin.da.addHostingSingaporeWithPortal', [$request->get('sevices_username'), $request->get('server_hosting_id')])->with('fails', 'Domain này đã có trong Portal');
            }
        } else {
           return redirect()->route('admin.da.addHostingSingaporeWithPortal', [$request->get('sevices_username'), $request->get('server_hosting_id')])->with('fails', 'Thêm Hosting từ DAPortal vào Portal thất bại');
        }
    }

    public function search(Request $request)
    {
        $q = $request->get('q');
        $status = $request->get('status');
        $sort = $request->get('sort');
        $data = $this->hosting->search($q, $status, $sort);
        return json_encode($data);
    }

    public function select_status_hosting(Request $request)
    {
        $q = $request->get('q');
        $status = $request->get('status');
        $sort = $request->get('sort');
        $data = $this->hosting->select_status_hosting($q, $status, $sort);
        return json_encode($data);
    }

    public function list_hosting_of_personal()
    {
        $data = $this->hosting->list_hosting_of_personal();
        return json_encode($data);
    }

    public function list_hosting_of_enterprise()
    {
        $data = $this->hosting->list_hosting_of_enterprise();
        return json_encode($data);
    }

    public function search_user(Request $request)
    {
        $q = $request->get('q');
        $data = $this->hosting->search_user($q);
        return json_encode($data);
    }

    public function hosting_singapore()
    {
        $hostings = $this->hosting->list_hosting_singapore();
        // dd($hostings);
        return view('admin.hostings.index', compact('hostings'));
    }

}
