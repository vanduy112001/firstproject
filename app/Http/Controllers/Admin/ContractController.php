<?php

namespace App\Http\Controllers\Admin;

use App\Model\Contract;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;
use DomPDF;
use PDF;
use SnappyImage;
use JasperPHP;

class ContractController extends Controller
{
    protected $userRepository;
    protected $contractRepository;

    public function __construct()
    {
        $this->userRepository = AdminFactories::userRepositories();
        $this->contractRepository = AdminFactories::contractRepositories();
    }

    public function index()
    {
        $contracts = $this->contractRepository->get_all();
        return view('admin.contract.list', compact('contracts'));
    }

    public function create_form()
    {
        $users = $this->contractRepository->get_users();
        $billings = config('billing');
        return view('admin.contract.create', compact('users', 'billings'));
    }

    public function add_data_contract_form(Request $request)
    {
        $id = $request->get('id');
        $data_contract_form = $this->contractRepository->add_data_contract_form($id);
        return $data_contract_form;
    }

    public function fill_amount_contract_form(Request $request)
    {
        $id = $request->get('id');
        $billing = $request->get('billing');
        $amount = $this->contractRepository->get_amount_contract_form($id, $billing);
        return json_encode($amount);
    }


    public function create_data_contract(Request $request)
    {
        $data = $request->all();
        // dd($data);
        $create = $this->contractRepository->create_data_contract($data);
        if ($create['status'] === true) {
            return redirect(route('admin.contract.update_form', ['id' => $create['id']]))->with('success', $create['message']);
        } else {
            return redirect(route('admin.contract.index'))->with('error', $create['message']);
        }
    }

    public function delete(Request $request)
    {
        $id = $request->get('id');
        $delete =  $this->contractRepository->delete($id);
        return $delete;
    }

    public function multidelete(Request $request)
    {
        $ids = $request->get('id');
        $multidelete =  $this->contractRepository->multidelete($ids);
        return $multidelete;
    }

    public function update_form($id)
    {
        // $data_contract_details = [];
        $contract = $this->contractRepository->get_contract_with_id($id);
        // $data_group_product = $this->contractRepository->add_data_contract_form($contract->user_id);
        // $group_products = $data_group_product['data_group_product'];
        $users = $this->contractRepository->get_users();
        $billings = config('billing');
        $contract_details = $this->contractRepository->get_contract_details($id);
        // foreach ($contract_details as $key => $contract_detail) {
        //     $data_contract_details[] = [
        //         'contract_detail' => $this->contractRepository->get_contract_detail_with_id($contract_detail->id),
        //         'product'         => $this->contractRepository->get_product($contract_detail->product_id)
        //     ];
        // }
        // return view('admin.contract.update', compact('contract', 'data_contract_details', 'users', 'billings', 'group_products'));
        return view('admin.contract.update', compact('contract', 'contract_details', 'users', 'billings'));
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();
        // dd($data);
        $update = $this->contractRepository->update($data, $id);
        if ($update['status'] === true) {
            return redirect(route('admin.contract.update_form', ['id' => $id]))->with('success', $update['message']);
        } else {
            return redirect(route('admin.contract.update_form', ['id' => $id]))->with('error', $update['message']);
        }
    }

    public function print_view($id)
    {
        // $data_contract_details = [];
        $contract = $this->contractRepository->get_contract_with_id($id);
        // $product = $this->contractRepository->get_product($contract->product_id);
        $billings = config('billing');
        $contract_details = $this->contractRepository->get_contract_details($id);
        // foreach ($contract_details as $key => $contract_detail) {
        //     $data_contract_details[] = [
        //         'contract_detail' => $this->contractRepository->get_contract_detail_with_id($contract_detail->id),
        //         'product'         => $this->contractRepository->get_product($contract_detail->product_id)
        //     ];
        // }
        // return view('admin.contract.print-view', compact('contract', 'billings', 'product', 'data_contract_details'));
        return view('admin.contract.print-view', compact('contract', 'billings', 'contract_details'));
    }

    public function print_pdf($id)
    {
        $contract = $this->contractRepository->get_contract_with_id($id);
        if($contract) {
            $billings = config('billing');
            $contract_details = $this->contractRepository->get_contract_details($id);
            $unicode = array(
                'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
                'd'=>'đ|Đ',
                'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
                'i'=>'í|ì|ỉ|ĩ|ị|I|Ì|Í|Ị|Ỉ|Ĩ',
                'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ọ|Ó|Ò|Ỏ|Õ|Ô|Ồ|Ố|Ổ|Ỗ|Ộ|Ơ|Ờ|Ớ|Ở|Ỡ|Ợ',
                'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ụ|Ũ|Ư|Ừ|Ứ|Ử|Ữ|Ự',
                'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
            );
            $str_name = strtolower($contract->title);
            foreach($unicode as $nonUnicode => $uni){
                $str_name = preg_replace( "/($uni)/i" , $nonUnicode , $str_name);
            }
            $str_name = str_replace( ' ', '_', $str_name );
            $path = '/storage/contracts/'. $contract->user_id;
            $sub_total = 0;
            foreach ($contract_details as $contract_detail) {
                if ($contract_detail->amount) {
                    $sub_total += $contract_detail->amount;
                    if ($contract_detail->qtt) {
                        $contract_detail->don_gia = $contract_detail->amount/$contract_detail->qtt;
                    }
                }
            }
            $vat = ($sub_total*10)/100;
            $total = $sub_total + $vat;
            if ($contract->vat) {
                $text_total = $this->convert_number_to_words($total);
            } else {
                $text_total = $this->convert_number_to_words($sub_total);
            }
            $contract->text_total = $text_total;
            $pdf = DomPDF::loadView('admin.contract.contract-pdf', compact( 'contract', 'contract_details', 'billings', 'sub_total', 'vat', 'total'));
            return $pdf->stream($contract->title . ' (' . $contract->contract_number .').pdf'); 
        } else {
            return redirect(route('admin.contract.index'))->with('error', 'Không tìm thấy hợp đồng');
        }
    }

    public function create_contract_detail(Request $request)
    {
        $data_contract_detail = $request->get('data');
        parse_str($data_contract_detail, $data_contract_detail);
        $result = $this->contractRepository->create_contract_detail($data_contract_detail);
        return $result;
    }

    public function delete_contract_detail(Request $request)
    {
        $id = $request->get('id');
        $result = $this->contractRepository->delete_contract_detail($id);
        return $result;
    }

    public function modal_update_contract_detail(Request $request)
    {
        $data = $request->all();
        $contract_detail = $this->contractRepository->get_contract_detail($data);
        return $contract_detail;
    }

    public function update_contract_detail(Request $request)
    {
        $id = $request->get('id');
        $data = $request->all();
        // dd($data);
        $result = $this->contractRepository->update_contract_detail($id, $data);
        if ( $result ) {
            return redirect()->back()->with('success', 'Chỉnh sửa sản phẩm thành công');
        } else {
            return redirect()->back()->with('success', 'Chỉnh sửa sản phẩm thất bại');
        }
        
    }
    
    public function get_target_from_user(Request $request) 
    {
        $data = $request->all();
        $targets = $this->contractRepository->get_target_from_user($data);
        return $targets;
    }
    
    public function get_target_from_user_action_update(Request $request) 
    {
        $data = $request->all();
        $targets = $this->contractRepository->get_target_from_user($data);
        $contract_detail = $this->contractRepository->contract_detail($data['contract_detail_id']);
        $data = [
            'contract_detail' => $contract_detail,
            'targets' => $targets
        ];
        return json_encode($data);
    }

    public function get_target_detail(Request $request)
    {
        $data = $request->all();
        $target_detail = $this->contractRepository->get_target_detail($data); 
        return json_encode($target_detail);
    }

    public function update_customer_info(Request $request) 
    {
        $data = $request->get('data');
        $update_result = $this->contractRepository->update_customer_info($data);
        return json_encode($update_result);
    }

    public function get_target_product_from_user(Request $request)
    {
        $data = $request->all();
        $targets = $this->contractRepository->get_target_product_from_user($data);
        return json_encode($targets);
    }

    public function get_target_detail_product(Request $request)
    {
        $data = $request->all();
        $product = $this->contractRepository->get_target_detail_product($data);
        return json_encode($product);
    }

    public function create_new_contract_number(Request $request)
    {
        $contract_number = $request->get('contract_number');
        $new_contract_number = $this->contractRepository->create_new_contract_number($contract_number);
        return json_encode($new_contract_number);
    }

    public function add_product_to_contract(Request $request)
    {
        $ids = $request->get('ids');
        $product_type = $request->get('product_type');
        $users = $this->contractRepository->get_users();
        $owner = $this->contractRepository->get_owner($ids[0], $product_type);
        $products = $this->contractRepository->get_products($ids, $product_type);
        $billings = config('billing');
        if($product_type == 'vps' || $product_type == 'vps_us') {
            $contract_type = 'VPS';
        } elseif ($product_type == 'hosting') {
            $contract_type = 'HOST';
        }
        return view('admin.contract.add-product-to-contract', compact('users', 'owner', 'product_type', 'contract_type', 'products', 'billings'));
    }

    public function save_product_to_contract(Request $request)
    {
        $data = $request->all();
        $create = $this->contractRepository->save_product_to_contract($data);
        if ($create['status'] === true) {
            return redirect(route('admin.contract.update_form', ['id' => $create['id']]))->with('success', $create['message']);
        } else {
            return redirect(route('admin.contract.index'))->with('error', $create['message']);
        }
    }

    public function userContract($id) 
    {
        $user = $this->contractRepository->getUser($id);
        $user_contracts = $this->contractRepository->userContract($id);
        return view('admin.contract.user-contracts', compact('user', 'user_contracts'));
    }

    public function userContractCreate($id)
    {
        $user_detail = $this->contractRepository->getUser($id);
        $users = $this->contractRepository->get_users();
        $billings = config('billing');
        return view('admin.contract.create-user-contract', compact('user_detail', 'users', 'billings'));
    }

    function convert_number_to_words($number) {
        // dd($number);
        $convert = [
            0 => 'không',
            1 => 'một',
            2 => 'hai',
            3 => 'ba',
            4 => 'bốn',
            5 => 'năm',
            6 => 'sáu',
            7 => 'bảy',
            8 => 'tám',
            9 => 'chín',
            10 => 'mười',
            11 => 'mười một',
            12 => 'mười hai',
            13 => 'mười ba',
            14 => 'mười bốn',
            15 => 'mười lăm',
            16 => 'mười sáu',
            17 => 'mười bảy',
            18 => 'mười tám',
            19 => 'mười chín',
        ];
        $term = $number;
        $string = '';
            // number lớn hơn 1 tỷ
        if ( $number > 999999999 ) {
            $sub_string = '';
            $sub_number = (int) floor( $number / 1000000000 );
            $number %= 1000000000;
              // sub_number lớn hơn 100
            if ( $sub_number > 100 ) {
                $number_h = $sub_number;
                $sub_number_h = (int) floor( $sub_number / 100 );
                $number_h %= 100;
                $sub_string = $convert[$sub_number_h] . ' trăm ';
                // sub_number_h lớn hơn 10
                if ( $number_h > 20 ) {
                    $number_0 = $number_h;
                    $sub_number_t = (int) floor( $number_h / 10 );
                    $number_0 %= 10;
                    $sub_string .= $convert[$sub_number_t];
                    $sub_string .= ' mươi ';
                    if ( $number_0 > 0 ) {
                        $sub_string .= $convert[$number_0];
                        $sub_string .= ' ';
                    }
                } else {
                    if ( $sub_number_t > 0 && $sub_number_t < 10 ) {
                        $sub_string .= 'lẻ ';
                        $sub_string .= $convert[$sub_number_t];
                        $sub_string .= ' ';
                    } else {
                        $sub_string .= $convert[$sub_number_t];
                        $sub_string .= ' ';
                    }
                }
                $string = $sub_string . 'tỷ ';
            }
            elseif ( $sub_number > 10 ) {
                $number_0 = $sub_number;
                $sub_number_t = (int) floor( $sub_number / 10 );
                $number_0 %= 10;
                $sub_string .= $convert[$sub_number_t];
                $sub_string .= ' mươi ';
                if ( $number_0 ) {
                    $sub_string .= $convert[$number_0];
                    $sub_string .= ' ';
                }
                $string = $sub_string . 'tỷ ';
            }
            else {
                $sub_string .= $convert[$sub_number];
                $sub_string .= ' ';
                $string = $sub_string . 'tỷ ';
            }
        }
            // number lớn hơn 1 triệu
        if ( $number > 999999 ) {
            $sub_string = '';
            $sub_number = (int) floor( $number / 1000000 );
            $number %= 1000000;
              // sub_number lớn hơn 100
            if ( $sub_number >= 100 ) {
                $number_h = $sub_number;
                $sub_number_h = (int) floor( $sub_number / 100 );
                $number_h %= 100;
                $sub_string = $convert[$sub_number_h] . ' trăm ';
                // sub_number_h lớn hơn 10
                if ($number_h) {
                  if ( $number_h > 10 ) {
                    $number_0 = $number_h;
                    $sub_number_t = (int) floor( $number_h / 10 );
                    $number_0 %= 10;
                    $sub_string .= $convert[$sub_number_t];
                    $sub_string .= ' mươi ';
                    if ( $number_0 > 0 ) {
                        $sub_string .= $convert[$number_0];
                        $sub_string .= ' ';
                    }
                  } else {
                      if ($sub_number_t > 0 && $sub_number_t < 10 ) {
                          $sub_string .= 'lẻ ';
                          $sub_string .= $convert[$sub_number_t];
                          $sub_string .= ' ';
                      } else {
                          $sub_string .= $convert[$sub_number_t];
                          $sub_string .= ' ';
                      }
                  }
                }
                $string .= $sub_string . 'triệu ';
            }
            elseif ( $sub_number > 20 ) {
                $number_0 = $sub_number;
                $sub_number_t = (int) floor( $sub_number / 10 );
                $number_0 %= 10;
                $sub_string .= $convert[$sub_number_t];
                $sub_string .= ' mươi ';
                if ( $number_0 > 0 ) {
                    $sub_string .= $convert[$number_0];
                    $sub_string .= ' ';
                }
                $string .= $sub_string . 'triệu ';
            }
            else {
                if ( $term < 10000000 ) {
                    $sub_string .= $convert[$sub_number];
                    $sub_string .= ' ';
                    $string .= $sub_string . 'triệu ';
                } else {
                    if ( $sub_number > 0 ) {
                        $sub_string .= $convert[$sub_number];
                        $sub_string .= ' ';
                        $string .= $sub_string . 'triệu ';
                    }
                }
                
            }
        }
            // number lớn hơn 1 ngàn
        if ( $number > 999 ) {
            $sub_string = '';
            $sub_number = (int) floor( $number / 1000 );
            $number %= 1000;
              // sub_number lớn hơn 100
            if ( $sub_number >= 100 ) {
                $number_h = $sub_number;
                $sub_number_h = (int) floor( $sub_number / 100 );
                $number_h %= 100;
                $sub_string = $convert[$sub_number_h] . ' trăm ';
                // sub_number_h lớn hơn 10
                if ($number_h) {
                  if ( $number_h > 19 ) {
                      $number_0 = $number_h;
                      $sub_number_t = (int) floor( $number_h / 10 );
                      $number_0 %= 10;
                      $sub_string .= $convert[$sub_number_t];
                      $sub_string .= ' mươi ';
                      if ( $number_0 > 0 ) {
                          $sub_string .= $convert[$number_0];
                          $sub_string .= ' ';
                      }
                  } else {
                      if ($number_h) {
                          $sub_string .= ' lẻ ';
                          $sub_string .= $convert[$number_h];
                          $sub_string .= ' ';
                      }
                  }
                }
                $string .= $sub_string . 'ngàn ';
            }
            elseif ( $sub_number > 20 ) {
                $number_0 = $sub_number;
                $sub_number_t = (int) floor( $sub_number / 10 );
                $number_0 %= 10;
                $sub_string .= $convert[$sub_number_t];
                $sub_string .= ' mươi ';
                if ( $number_0 > 0 ) {
                    $sub_string .= $convert[$number_0];
                    $sub_string .= ' ';
                }
                $string .= $sub_string . 'ngàn ';
            }
            else {
                $sub_string .= $convert[$sub_number];
                $sub_string .= ' ';
                $string .= $sub_string . 'ngàn ';
            }
        }
            // dd($number);
            // number lớn hơn bé hơn 1000
        if ( $number < 1000 &&  $number > 0) {
            $sub_string = '';
            $sub_number = $number;
              // sub_number lớn hơn 100
            if ( $sub_number >= 100 ) {
                $number_h = $sub_number;
                $sub_number_h = (int) floor( $sub_number / 100 );
                $number_h %= 100;
                $sub_string = $convert[$sub_number_h] . ' trăm ';
                // sub_number_h lớn hơn 10
                if ($number_h) {
                  if ( $number_h > 10 ) {
                      $number_0 = $sub_number;
                      $sub_number_t = (int) floor( $number_h / 10 );
                      $number_0 %= 10;
                      $sub_string .= $convert[$sub_number_t];
                      $sub_string .= ' mươi ';
                      if ( $number_0 ) {
                          $sub_string .= $convert[$number_0];
                          $sub_string .= ' ';
                      }
                  } else {
                      if ( $number_h ) {
                          $sub_string .= ' lẻ ';
                          $sub_string .= $convert[$number_h];
                          $sub_string .= ' ';
                      }
                  }
                }
                $string .= $sub_string;
            }
            elseif ( $sub_number > 20 ) {
                $number_0 = $sub_number;
                $sub_number_t = (int) floor( $sub_number / 10 );
                $number_0 %= 10;
                $sub_string .= $convert[$sub_number_t];
                $sub_string .= ' mươi ';
                if ( $number_0 ) {
                    $sub_string .= $convert[$number_0];
                    $sub_string .= ' ';
                }
                $string .= $sub_string;
            }
            else {
                if ( $sub_number ) {
                    $sub_string .= $convert[$sub_number];
                    $sub_string .= ' ';
                    $string .= $sub_string;
                }
            }
        }
        
        return ucfirst($string)  . 'đồng';
      }

}
