<?php

namespace App\Http\Controllers\Admin;

use App\Factories\AdminFactories;
use App\Http\Controllers\Controller;
use App\Mail\SendMailTicketUser;
use App\Mail\SendMailUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class TicketController extends Controller
{
    protected $ticketRepository;

    public function __construct()
    {
        $this->ticketRepository = AdminFactories::ticketRepository();
    }

    public function getList(Request $request)
    {
        $list = $this->ticketRepository->getList([]);

        return view('admin.tickets.index', compact('list'));
    }

    public function detail($id)
    {
        $detail = $this->ticketRepository->getDetail($id);

        return view('admin.tickets.detail', compact('detail'));
    }

    public function message(Request $request)
    {
        $data = $request->all();
        $validatedData = $request->validate([
            'content' => 'required',
            'ticket_id' => 'required',
        ]);
        $ticketId = $data['ticket_id'];
        $ticketDetail = $this->ticketRepository->getDetail($ticketId);
        if (!empty($ticketDetail)) {
            $data['user_id'] = Auth::id();
            $user = $ticketDetail->user;

            $save = $this->ticketRepository->saveMessage($data);

            if ($save) {
                Mail::to($user->email)->send(new SendMailTicketUser($user, $data['ticket_id'], $data['content']));

                return redirect(route('admin.ticket.detail', ['id' => $ticketId]))->with('success',
                    'Gửi phản hồi thành công.');
            }
        }

        return redirect(route('admin.ticket.detail', ['id' => $ticketId]))->with('fails',
            'Có lỗi xảy ra, vui lòng thử lại.');
    }

    public function updateStatusHandle($id, Request $request)
    {
        try {
            $data = $request->all();
            $validatedData = $request->validate([
                'status' => 'required',
            ]);

            $save = $this->ticketRepository->save($id, $data);
            if ($save) {
                $saveMessage = $this->ticketRepository->saveMessage([
                    'ticket_id' => $id,
                    'user_id' => Auth::id(),
                    'content' => 'Admin đã chuyển trạng thái ticket thành : <b>'.$data['status'].'</b>',
                ]);

                return redirect(route('admin.ticket.detail', ['id' => $id]))->with('success',
                    'Cập nhật trạng thái thành công.');
            } else {
                return redirect(route('admin.ticket.detail', ['id' => $id]))->with('fails',
                    'Có lỗi xảy ra, vui lòng thử lại.');
            }
        } catch (\Exception $exception) {
            return redirect(route('admin.ticket.detail', ['id' => $id]))->with('fails',
                $exception->getMessage());
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->ticketRepository->delete($request->get('id'));
        return json_encode($delete);
    }

    public function deleteAll(Request $request)
    {
      // dd('da den');
        $listTicket = $this->ticketRepository->deleteAll($request->get('list'));
        return json_encode($listTicket);
    }

}
