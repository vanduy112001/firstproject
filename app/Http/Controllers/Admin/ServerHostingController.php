<?php

namespace App\Http\Controllers\Admin;

use App\Model\ServerHosting;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;
use App\Http\Requests\CreateServerHostingValidate;

class ServerHostingController extends Controller
{

    protected $server_hosting;

    public function __construct()
    {
        $this->server_hosting = AdminFactories::serverHostingRepositories();
    }

    public function index()
    {
        $default = config('da');
        // dd($default);
        $server_hostings = $this->server_hosting->getServerHosting();
        return view('admin.server_hostings.index', compact('default', 'server_hostings'));
    }

    public function server_si()
    {
        $default = config('da_si');
        // dd($default);
        $server_hostings = $this->server_hosting->getServerHostingSingapore();
        return view('admin.server_hostings.server_si', compact('default', 'server_hostings'));
    }


    public function create_server_hosting()
    {
        $type_server_hosting = config('type_server_hosting');
        $location_hosting = config('location_hosting');
        return view('admin.server_hostings.create', compact('type_server_hosting', 'location_hosting'));
    }

    public function check_connection(Request $request)
    {
        if ( empty($request->get('host')) || empty($request->get('post')) || empty($request->get('user_name')) || empty($request->get('password')) || empty($request->get('type')) ) {
            return json_encode(1);
        }
        $check_connection = $this->server_hosting->check_connection($request->all());
        if ($check_connection) {
          return json_encode($check_connection);
        } else {
          return json_encode(1);
        }

    }

    public function store(CreateServerHostingValidate $request)
    {
        $create = $this->server_hosting->create_server_hosting($request->all());
        if ($create) {
            return redirect()->route('admin.server_hosting.index')->with('success' , 'Thêm server hosting thành công');
        } else {
            return redirect()->route('admin.server_hosting.create_server_hosting')->with('fails', 'Thêm server hosting thất bại');
        }
    }

    public function edit_server_hosting($id)
    {
        $server_hosting = $this->server_hosting->detail($id);
        $type_server_hosting = config('type_server_hosting');
        return view('admin.server_hostings.edit', compact('type_server_hosting', 'server_hosting'));
    }

    public function update(CreateServerHostingValidate $request)
    {
        $update = $this->server_hosting->update_server_hosting($request->all());
        if ($update) {
            return redirect()->route('admin.server_hosting.index')->with('success' , 'Chỉnh sửa server hosting thành công');
        } else {
            return redirect()->route('admin.server_hosting.edit_server_hosting', $request->get('id'))->with('fails', 'Thêm server hosting thất bại');
        }
    }

    public function delete(Request $request)
    {
        // return json_encode(true);
        $id  = $request->get('id');
        $delete =  $this->server_hosting->delete($id);
        if ($delete) {
          return json_encode(true);
        } else {
          return json_encode(false);
        }
    }

    public function active(Request $request)
    {
        // dd('da den');
        $id  = $request->get('id');
        $type  = $request->get('type');
        if ($type == 'active') {
            $update = $this->server_hosting->active($id);
        } else {
            $update = $this->server_hosting->deactive($id);
        }
        if ($update) {
            return redirect()->route('admin.server_hosting.index')->with('success' , 'Kích hoạt server hosting thành công');
        } else {
            return redirect()->route('admin.server_hosting.index')->with('fails' , 'Kích hoạt server hosting thất bại');
        }
    }

}
