<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;

class VpsController extends Controller
{
    protected $vps;


    public function __construct()
    {
        $this->vps = AdminFactories::vpsRepositories();
    }

    public function index(Request $request)
    {
        $data = $request->all();
        $data_reponse = [];
        foreach ($data as $key => $value) {
            if ($value['status'] == 'on' || $value['status'] == 'change_user' || $value['status'] == 'off' || $value['status'] == 'expire' || $value['status'] == 'delete_vps' || $value['status'] == 'suspend' ) {
                $update = $this->vps->update_status_vps_form_dashboard($value);
                if ($update) {
                    $data_reponse[] = [
                        "vm_id" => $value['vm_id'],
                        "error" => 0,
                        "status" => "Chỉnh sửa trạng thái vps thành công"
                    ];
                } else {
                    $data_reponse[] = [
                        "vm_id" => $value['vm_id'],
                        "error" => 1,
                        "status" => "Chỉnh sửa trạng thái vps thất bại"
                    ];
                }
            } else {
                $data_reponse[] = [
                    "vm_id" => $value['vm_id'],
                    "error" => 1,
                    "status" => "Trạng thái của vps không hợp lệ"
                ];
            }
        }
        return response()->json($data_reponse , 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        // return json_encode($data, 200, ['Content-type'=> 'application/json; charset=utf-8']);
    }

    public function change_ip_vps(Request $request)
    {
        $data = $request->all();
        $data_reponse = [];
        foreach ($data as $key => $value) {
            $update = $this->vps->change_ip_vps($value);
            if ($update) {
                $data_reponse[] = [
                    "vm_id" => $value['vm_id'],
                    "error" => 0,
                    "status" => "Đổi ip thành công"
                ];
            } else {
                $data_reponse[] = [
                    "vm_id" => $value['vm_id'],
                    "error" => 1,
                    "status" => "Không có vps này"
                ];
            }
        }
        return response()->json($data_reponse , 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }

    public function update_reset(Request $request)
    {
        $data = $request->all();
        $data_reponse = [];
        if ( !empty($data['type']) ) {
          if ( $data['type'] == 'rebuild' ) {
            foreach ($data['data'] as $key => $value) {
                $update = $this->vps->update_rebuild_vps($value);
                if ($update) {
                    $data_reponse[] = [
                        "vm_id" => $value['vm_id'],
                        "error" => 0,
                        "status" => "Cập nhật thành công"
                    ];
                } else {
                    $data_reponse[] = [
                        "vm_id" => $value['vm_id'],
                        "error" => 1,
                        "status" => "Không có vps này"
                    ];
                }
            }
          }
          elseif ( $data['type'] == 'reset_password' ) {
            foreach ($data['data'] as $key => $value) {
                $update = $this->vps->update_reset_password($value);
                if ($update) {
                    $data_reponse[] = [
                        "vm_id" => $value['vm_id'],
                        "error" => 0,
                        "status" => "Cập nhật thành công"
                    ];
                } else {
                    $data_reponse[] = [
                        "vm_id" => $value['vm_id'],
                        "error" => 1,
                        "status" => "Không có vps này"
                    ];
                }
            }
          }
        } else {
          $data_reponse[] = [
              "vm_id" => $value['vm_id'],
              "error" => 1,
              "status" => "Không có trường \"type\" trong body"
          ];
        }
        return response()->json($data_reponse , 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }

    public function add_vps(Request $request)
    {
        $data = $request->all();
        $add_vps = $this->vps->add_vps_with_dashboard($data);
        if ($add_vps) {
            if ( empty($add_vps['error']) ) {
                $data = [
                    "error" => 0,
                    "status" => "Thêm VPS thành công"
                ];
            } else {
                return $add_vps;
            }
        } else {
            $data = [
                "error" => 1,
                "status" => "Thêm VPS thất bại"
            ];
        }
        return $data;
    }

    public function get_product_vps_with_user(Request $request)
    {
        $customer_id = $request->get('customer_id');
        if (empty($customer_id)) {
          $data = [
              "error" => 1,
              "status" => "Không có customer_id",
              "product" => null,
          ];
          return response()->json($data , 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        }
        $products = $this->vps->get_product_vps_with_user($customer_id);
        if ($products) {
            $data = [
                "error" => 0,
                "status" => "Lấy sản phẩm cho khách hàng thành công",
                "product" => $products,
            ];
        } else {
            $data = [
                "error" => 2,
                "status" => "Khách hàng này không có trong Portal",
                "product" => null,
            ];
        }
        return response()->json($data , 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }

    public function change_vps(Request $request)
    {
        $status = $request->get('status');
        if ( !empty($status) ) {
            $data = $request->all();
            if ( $status == 'change_ip' ) {
                $change_ip = $this->vps->change_ip($data);
                return $change_ip;
            }
            elseif ( $status == 'next_due_date' ) {
              $data = $request->all();
              $change_next_due_date = $this->vps->change_next_due_date($data);
              return $change_next_due_date;
            }
            elseif ( $status == 'change_user' ) {
              $data = $request->all();
              $change_user = $this->vps->change_user($data);
              return $change_user;
            }
            elseif ( $status == 'delete_vps' ) {
              $data = $request->all();
              $delete_vps = $this->vps->api_delete_vps($data);
              return $delete_vps;
            }
            elseif ( $status == 'change_user_dashboard' ) {
              $data = $request->all();
              $delete_vps = $this->vps->api_change_user($data);
              return $delete_vps;
            }
        } else {
            $restore = [
                'error' => 1,
                'status' => 'Lỗi không có trạng thái của kết nối thay đổi VPS',
            ];
        }
        return response()->json($restore , 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
    }

    public function get_all_vps()
    {
        $list_vps = $this->vps->api_get_vps();
        return $list_vps;
    }
    // link https://portal.cloudzone.vn/api/get_vps_us
    public function get_all_vps_us()
    {
        $list_vps = $this->vps->get_all_vps_us();
        return $list_vps;
    }

    // link https://portal.cloudzone.vn/api/get_vps_by_vmid
    public function get_vps_by_vmid(Request $request)
    {
        $list_vps = $this->vps->get_vps_by_vmid($request->all());
        return $list_vps;
    }

    // link https://portal.cloudzone.vn/api/get_vps_us_by_vmid
    public function get_vps_us_by_vmid(Request $request)
    {
        $list_vps = $this->vps->get_vps_us_by_vmid($request->all());
        return $list_vps;
    }

    public function add_proxy(Request $request)
    {
        $data = $request->all();
        $add_vps = $this->vps->add_proxy_with_dashboard($data);
        if ($add_vps) {
            if ( empty($add_vps['error']) ) {
                $data = [
                    "error" => 0,
                    "status" => "Thêm Proxy thành công"
                ];
            } else {
                return $add_vps;
            }
        } else {
            $data = [
                "error" => 1,
                "status" => "Thêm Proxy thất bại"
            ];
        }
        return $data;
    }

}
