<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;

class OrderController extends Controller
{
    protected $order;

    public function __construct()
    {
        $this->order = AdminFactories::apiOrderRepositories();
    }

    // Link tạo order (post)
    public function order(Request $request)
    {
        $order = $this->order->order($request->all());
        return $order;
    }
    //error: 0, Tạo order thành công
    //error: 1, lỗi user và password không đúng
    //error: 2, lỗi token không đúng
    //error: 3, lỗi hệ thống
    // error: 4, số dư không đủ

    // Link tạo gia hạn (put)
    public function expire(Request $request)
    {
        $expire = $this->order->expire($request->all());
        return $expire;
    }
    //error: 0, Tạo gia hạn thành công, trả về các dịch vụ ngày kết thúc và số dư
    //error: 1, lỗi user và password không đúng
    //error: 2, lỗi token không đúng
    //error: 3, lỗi hệ thống
    // error: 4, số dư không đủ



}
