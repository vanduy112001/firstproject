<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    protected $user;

    public function __construct()
    {
        $this->user = AdminFactories::apiUserRepository();
    }
/**USER*/
    public function listUser(Request $request)
    {
        try {
            $q = !empty($request->get('q')) ? $request->get('q') : "";
            return response()->json([
                'error' => 0,
                'message' => 'Lấy danh sách khách hàng thành công',
                'content' => $this->user->listUser($q),
            ]);
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn khách hàng'
            ]);
        }
        
    }

    public function createUser(Request $request)
    {
        try {
            $response = $this->user->createUser($request->all());
            return response()->json( $response );
        } catch (Exception $e) {
            report($e, $request->all());
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn khách hàng'
            ]);
        }
    }

    public function editUser(Request $request)
    {
        try {
            $response = $this->user->editUser($request->all());
            return response()->json( $response );
        } catch (Exception $e) {
            report($e, $request->all());
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn khách hàng'
            ]);
        }
    }

    public function getInfoUser(Request $request, $userId)
    {
        try {
            $response = $this->user->detailUser($userId);
            return response()->json( $response );
        } catch (Exception $e) {
            report($e, $userId);
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn khách hàng'
            ]);
        }
    }

    public function getEditUser(Request $request, $userId)
    {
        try {
            $response = $this->user->detailUser($userId);
            $response['groupUser'] = $this->user->listGroupUser();
            return response()->json( $response );
        } catch (Exception $e) {
            report($e, $userId);
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn khách hàng'
            ]);
        }
    }

    public function updateCredit(Request $request)
    {
        try {
            $response = $this->user->updateCredit($request->all());
            return response()->json( $response );
        } catch (Exception $e) {
            report($e, $request->all());
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn khách hàng'
            ]);
        }
    }

    public function historyPayment(Request $request, $userId)
    {
        try {
            $response = $this->user->historyPayment($userId);
            return response()->json([
                'error' => 0,
                'message' => 'Lấy danh sách khách hàng thành công',
                'content' => $response,
            ]);
        } catch (Exception $e) {
            report($e, $request->all());
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn khách hàng'
            ]);
        }
    }

    public function historyOrder(Request $request, $userId)
    {
        try {
            $response = $this->user->historyOrder($userId);
            return response()->json([
                'error' => 0,
                'message' => 'Lấy danh sách khách hàng thành công',
                'content' => $response,
            ]);
        } catch (Exception $e) {
            report($e, $request->all());
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn khách hàng'
            ]);
        }
    }

    public function deleteUser(Request $request, $userId)
    {
        try {
            $response = $this->user->deleteUser($userId);
            return response()->json( $response );
        } catch (Exception $e) {
            report($e, $request->all());
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn khách hàng'
            ]);
        }
    }
/**GROUP USER*/
    public function listGroupUser(Request $request)
    {
        try {
            return response()->json([
                'error' => 0,
                'message' => 'Lấy danh sách nhóm khách hàng thành công',
                'content' => $this->user->listGroupUser(),
            ]);
        } catch (Exception $e) {
            report($e, $request->all());
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn nhóm khách hàng'
            ]);
        }
    }

    public function getlistGroupUser(Request $request)
    {
        try {
            return response()->json([
                'error' => 0,
                'message' => 'Lấy danh sách nhóm khách hàng thành công',
                'content' => $this->user->getlistGroupUser(),
            ]);
        } catch (Exception $e) {
            report($e, $request->all());
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn nhóm khách hàng'
            ]);
        }
    }

    public function getDetailGroupUser(Request $request, $groupUserId)
    {
        $response = $this->user->detailGroupUser($groupUserId);
        return response()->json( $response );
    }

    public function createGroupUser(Request $request)
    {
        $response = $this->user->createGroupUser($request->all());
        return response()->json( $response );
    }
    
    public function editGroupUser(Request $request)
    {
        $response = $this->user->editGroupUser($request->all());
        return response()->json( $response );
    }

    public function listUserByGroupUser(Request $request, $groupUserId)
    {
        try {
            $q = !empty($request->get('q')) ? $request->get('q') : "";
            return response()->json([
                'error' => 0,
                'message' => 'Lấy danh sách khách hàng thành công',
                'content' => $this->user->listUserByGroupUser($q, $groupUserId),
                'groupUser' => $this->user->detailGroupUser($groupUserId),
            ]);
        } catch (\Throwable $th) {
            report($th);
            return response()->json([
                'error' => 1,
                'message' => 'Lỗi truy vấn khách hàng'
            ]);
        }
    }

}
