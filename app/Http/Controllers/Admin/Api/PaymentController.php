<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;

class PaymentController extends Controller
{
    protected $payment;
    public function __construct()
    {
        $this->payment = AdminFactories::paymentRepositories();
    }

    public function get_payment_unpaid()
    {
        try {
            $payments = $this->payment->get_payment_unpaid();
            return response()->json($payments , 200, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        } catch (Exception $e) {
            report($e);
            return response()->json('error' , 500, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }

    public function confirm_payment(Request $request)
    {
        $data = $request->all();
        $confirm_payment = $this->payment->api_confirm_payment($data);
        return $confirm_payment;
    }

}
