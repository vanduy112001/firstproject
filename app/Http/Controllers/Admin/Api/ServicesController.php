<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;

class ServicesController extends Controller
{
    protected $service;

    public function __construct()
    {
        $this->service = AdminFactories::apiServiceRepositories();
    }

    // hành động của vps
    public function vps_action(Request $request)
    {
        $service = $this->service->vps_action($request);
        return $service;
    }
    /** LIST VPS **/
    //error: 0, List vps thành công và trả về 1 list vps của khách hàng
    //error: 1, lỗi user và password không đúng
    //error: 2, lỗi token không đúng
    //error: 3, lỗi hệ thống

    // hành động của NAT vps
    public function nat_vps_action(Request $request)
    {
        $service = $this->service->nat_vps_action($request);
        return $service;
    }
    /** LIST NAT VPS **/
    //error: 0, List vps thành công và trả về 1 list vps của khách hàng
    //error: 1, lỗi user và password không đúng
    //error: 2, lỗi token không đúng
    //error: 3, lỗi hệ thống

    // hành động của hosting
    public function hosting_action(Request $request)
    {
        $service = $this->service->hosting_action($request);
        return $service;
    }
    /** LIST Hosting **/
    //error: 0, List hosting thành công và trả về 1 list vps của khách hàng
    //error: 1, lỗi user và password không đúng
    //error: 2, lỗi token không đúng
    //error: 3, lỗi hệ thống


}
