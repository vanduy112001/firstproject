<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;
use JWTAuth;
use JWTAuthException;
use App\Model\LogActivity;

class LoginController extends Controller
{
    protected $login;
    protected $log_activity;

    public function __construct()
    {
        $this->login = AdminFactories::agencyRepositories();
        $this->log_activity = new LogActivity();
    }
    // Admin Login
    public function adminLogin(Request $request)
    {
      $credentials = $request->only('email', 'password');
      $token = null;
      try {

        if (!$token = JWTAuth::attempt($credentials)) {
          return response()->json([
            'error' => 2,
            'message' => 'Tài khoản và mật khẩu không đúng.'
          ]);
        }
      } catch (JWTAuthException $e) {
        report($e);
        return response()->json([
          'error' => 1,
          'message' => 'Lỗi truy vấn.'
        ]);
      }
      if ( JWTAuth::user()->user_meta->role == 'admin' ) {
        // Tạo log
            $data_log = [
               'user_id' => JWTAuth::user()->id,
               'action' => 'đăng nhập admin mobile',
               'model' => 'admin',
               'description' => '',
            ];
            $this->log_activity->create($data_log);
      
        return response()->json([
          'error' => 0,
          'message' => 'Đăng nhập thành công',
          'token' => $token,
        ]);
      } else {
        return response()->json([
          'error' => 3,
          'message' => 'Tài khoản không có quyền đăng nhập vào hệ thống.'
        ]); 
      }
      
    }
    // login với user và password cho trước (post)
    public function login(Request $request)
    {
        $login = $this->login->login($request->all());
        return $login;
    }
    // trả lại: error: 0, token: token
    // token: tạo 1 token kết nối
    //error: 1, lỗi user và password không đúng
    //error: 2, lỗi hệ thống

    // login với user và password cho trước va token từ link login (post)
    public function test_login(Request $request)
    {
      $test_login = $this->login->test_login($request, $request->all());
      return $test_login;
    }
    //error: 0, kết nối thành công
    //error: 1, lỗi user và password không đúng
    //error: 2, lỗi token không đúng
    //error: 3, lỗi hệ thống

    // kiểm tra số dư tài khoản (get)
    public function check_credit(Request $request)
    {
      $credit = $this->login->check_credit($request->all());
      return $credit;
    }
    //error: 0, kết nối thành công trả về số dư hiện tại
    //error: 1, lỗi user và password không đúng
    //error: 2, lỗi token không đúng
    //error: 3, lỗi hệ thống

    // lấy sản phẩm từ portal của các đại lý (get)
    public function get_product(Request $request)
    {
      $products = $this->login->get_group_product_private($request->all());
      return $products;
    }
    //error: 0, kết nối thành công trả về nhóm sản phẩm của đại lý
    //error: 1, lỗi user và password không đúng
    //error: 2, lỗi token không đúng
    //error: 3, lỗi hệ thống

}
