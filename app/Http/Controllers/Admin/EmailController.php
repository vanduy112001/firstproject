<?php

namespace App\Http\Controllers\Admin;

use App\Model\Email;
use App\Factories\AdminFactories;
use Illuminate\Http\Request;
use App\Http\Requests\EditEmailValidate;
use App\Http\Requests\CreateEmailMarketingValidate;
use App\Http\Requests\CreateEmailServiceValidate;
use App\Model\GroupEmail;
use App\Model\MetaEmail;

class EmailController extends Controller
{
    protected $email;
    protected $user;

    public function __construct()
    {
        $this->email = AdminFactories::emailRepositories();
        $this->user = AdminFactories::userRepositories();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $emails = $this->email->getEmail();
        $group_emails = $this->email->getGroupEmail();
        return view('admin.emails.index', compact('emails', 'group_emails')); 
    }
    // Get group email va email template
    public function listGroupEmail()
    {
        $group_emails = $this->email->getGroupEmail();
        return response([
            'groupEmail' => $group_emails
        ], 200);
    }
    // Tao group email
    public function CreateGroupEmail(Request $request)
    {
        $store = $this->email->storeGroupEmail($request->get('name'));
        return response([
            'groupEmail' => $store,
        ], 200);
    }
    // Sửa group email
    public function editGroupEmail(Request $request)
    {
        $id = $request->get('id');
        $name = $request->get('name');
        $edit = $this->email->editGroupEmail($id, $name);
        return response([
            'editGroup' => $edit,
        ], 200);
    }
    // Tạo email teamplate
    public function createEmailTemplate(Request $request)
    {        
        $this->validate($request, [
            'name' => 'required|min:3',
            'idGroupEmail' => 'required',
        ]);

        $name = $request->get('name');
        $idGroupEmail = $request->get('idGroupEmail');
        $create = $this->email->createEmail($name, $idGroupEmail);
        return response([
            'email' => $create
        ], 200);

    }
    // Sua email template
    public function editEmail(Request $request)
    {
        $id = $request->get('id');
        $email = $this->email->detailEmail($id);
        return view('admin.emails.edit', compact('email'));
    }
    // chỉnh sửa nội dung email template
    public function updateTemplate(EditEmailValidate $request)
    {
        $id = $request->get('id');
        $name = $request->get('name');
        $data_email_meta = [
            'subject' => $request->get('subject'),
            'content' => $request->get('content'),
            'email_id' => $request->get('id'),
            'discription' => '',
        ];
        $update = $this->email->updateEmail($id, $name, $data_email_meta);
        if($update) {
            return redirect()->route('email.index')->with('success', 'Chỉnh sửa email template thành công!');
        } else {
            return redirect()->route('email.index')->with('fails', 'Chỉnh sửa email template thất bại!');
        }
    }
    // Xóa email template
    public function deleteEmail($id)
    {
        $delete = $this->email->deleteEmail($id);
        return response([
            'delete' => $id
        ], 200);
    }

/* Email marketing */
    public function list_email_marketing()
    {
        return view('admin.emails.list_email_marketing');
    }

    public function create_email_marketing()
    {
        $list_group_user = $this->user->getGroupUser();
        return view('admin.emails.create_email_marketing', compact('list_group_user'));
    }

    public function edit_email_marketing(Request $request, $id)
    {
        $list_group_user = $this->user->getGroupUser();
        $email = $this->email->detailEmail($id);
        return view('admin.emails.edit_email_marketing', compact('list_group_user', 'email'));
    }

    public function clone_email_marketing(Request $request, $id)
    {
        $list_group_user = $this->user->getGroupUser();
        $email = $this->email->detailEmail($id);
        return view('admin.emails.clone_email_marketing', compact('list_group_user', 'email'));
    }

    public function send_email_marketing(Request $request, $id)
    {
        $email = $this->email->detailEmail($id);
        $list_user = $this->email->list_user();
        return view('admin.emails.send_email_marketing', compact('email', 'list_user'));
    }

    public function store_email_marketing(CreateEmailMarketingValidate $request)
    {
        // dd($request->all());
        $create_email = $this->email->create_email_marketing($request->all());
        if ( $create_email ) {
            return redirect()->route('email.email_marketing.list')->with('success', 'Tạo Email Marketing thành công');
        } else {
            return redirect()->back()->with('false', 'Tạo Email Marketing thất bại');
        }
    }

    public function get_list_email_marketing()
    {
        $list_email = $this->email->get_list_email_marketing();
        return json_encode($list_email);
    }

    public function update_email_marketing(CreateEmailMarketingValidate $request)
    {
        // dd($request->all());
        $create_email = $this->email->update_email_marketing($request->all());
        if ( $create_email ) {
            return redirect()->route('email.email_marketing.list')->with('success', 'Chỉnh sửa Email Marketing thành công');
        } else {
            return redirect()->back()->with('false', 'Chỉnh sửa Email Marketing thất bại');
        }
    }

    public function form_send_email_marketing(Request $request)
    {
        $create_email = $this->email->send_email_marketing($request->all());
        if ( $create_email ) {
            return redirect()->route('email.email_marketing.list')->with('success', 'Gửi Email Marketing thành công');
        } else {
            return redirect()->back()->with('false', 'Gửi Email Marketing thất bại');
        }
    }

    public function action_email_marketing(Request $request)
    {
        $action = $request->get('action');
        // return json_encode($request->get('id'));
        switch ($action) {
            case 'delete':
                $delete = $this->email->delete_email_marketing($request->get('id'));
                return json_encode($delete);
                break;
            case 'on':
                $on = $this->email->on_email_marketing($request->get('id'));
                return json_encode($on);
                break;
            case 'off':
                $off = $this->email->off_email_marketing($request->get('id'));
                return json_encode($off);
                break;
            default:
                return json_encode([
                    'error' => 2
                ]);
                break;
        }
    }

/* Email service */
    public function list_email_service()
    {
        return view('admin.emails.service.list');
    }

    public function create_email_service()
    {
        $type_email_service = config("type_email_service");
        return view('admin.emails.service.create', compact('type_email_service'));
    }

    public function store_email_service(CreateEmailServiceValidate $request)
    {
        // dd($request->all());
        $create_email = $this->email->create_email_service($request->all());
        if ( $create_email ) {
            return redirect()->route('email.service.list')->with('success', 'Tạo Email Service thành công');
        } else {
            return redirect()->back()->with('false', 'Tạo Email Service thất bại');
        }
    }

    public function get_list_email_service()
    {
        $list_email = $this->email->get_list_email_service();
        return json_encode($list_email);
    }

    public function edit_email_service(Request $request, $id)
    {
        $email = $this->email->detailEmail($id);
        $type_email_service = config("type_email_service");
        return view('admin.emails.service.edit', compact('email', 'type_email_service'));
    }

    public function update_email_service(CreateEmailServiceValidate $request)
    {
        $create_email = $this->email->update_email_service($request->all());
        if ( $create_email ) {
            return redirect()->route('email.service.list')->with('success', 'Chỉnh sửa Email Service thành công');
        } else {
            return redirect()->back()->with('false', 'Chỉnh sửa Email Service thất bại');
        }
    }

    public function action_email_service(Request $request)
    {
        $action = $request->get('action');
        // return json_encode($request->get('id'));
        switch ($action) {
            case 'delete':
                $delete = $this->email->delete_email_marketing($request->get('id'));
                return json_encode($delete);
                break;
            default:
                return json_encode([
                    'error' => 2
                ]);
                break;
        }
    }

}
