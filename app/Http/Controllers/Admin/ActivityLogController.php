<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Factories\AdminFactories;

class ActivityLogController extends Controller
{
    protected $log;

    public function __construct()
    {
        $this->log = AdminFactories::authRepositories();
    }

    public function index()
    {
        return view('admin.logs.index');
    }

    public function danh_sach_log()
    {
        $data = $this->log->list_log();
        return json_encode($data);
    }

    public function danh_sach_log_theo_hanh_dong(Request $request)
    {
        $data = $this->log->list_log_with_action($request->get('qtt'), $request->get('user'), $request->get('action'), $request->get('service'));
        return json_encode($data);
    }

}
