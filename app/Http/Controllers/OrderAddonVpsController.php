<?php

namespace App\Http\Controllers;

use App\Model\OrderAddonVps;
use Illuminate\Http\Request;

class OrderAddonVpsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\OrderAddonVps  $orderAddonVps
     * @return \Illuminate\Http\Response
     */
    public function show(OrderAddonVps $orderAddonVps)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\OrderAddonVps  $orderAddonVps
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderAddonVps $orderAddonVps)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\OrderAddonVps  $orderAddonVps
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderAddonVps $orderAddonVps)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\OrderAddonVps  $orderAddonVps
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderAddonVps $orderAddonVps)
    {
        //
    }
}
