<?php

namespace App\Http\Controllers;

use App\Model\Vietcombank;
use Illuminate\Http\Request;

class VietcombankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Vietcombank  $vietcombank
     * @return \Illuminate\Http\Response
     */
    public function show(Vietcombank $vietcombank)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Vietcombank  $vietcombank
     * @return \Illuminate\Http\Response
     */
    public function edit(Vietcombank $vietcombank)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Vietcombank  $vietcombank
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vietcombank $vietcombank)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Vietcombank  $vietcombank
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vietcombank $vietcombank)
    {
        //
    }
}
