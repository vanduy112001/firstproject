<?php

namespace App\Http\Controllers;

use App\Model\OrderExpired;
use Illuminate\Http\Request;

class OrderExpiredController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\OrderExpired  $orderExpired
     * @return \Illuminate\Http\Response
     */
    public function show(OrderExpired $orderExpired)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\OrderExpired  $orderExpired
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderExpired $orderExpired)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\OrderExpired  $orderExpired
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderExpired $orderExpired)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\OrderExpired  $orderExpired
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderExpired $orderExpired)
    {
        //
    }
}
