<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|min:2|max:200',
            'group_product_id' => 'bail|required|not_in:0',
            'type_product' => 'bail|required|not_in:0',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên sản phẩm không được để trống',
            'name.min' => 'Số ký tự của tên sản phẩm phải nhỏ hơn :min',
            'name.max' => 'Số ký tự của tên sản phẩm phải lớn hơn :max',
            'group_product_id.required' => 'Nhóm sản phẩm không được để trống',
            'group_product_id.not_in' => 'Nhóm sản phẩm không được để trống',
            'type_product.required' => 'Nhóm sản phẩm không được để trống',
            'type_product.not_in' => 'Nhóm sản phẩm không được để trống',
        ];
    }
}
