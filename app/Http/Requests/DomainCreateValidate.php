<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DomainCreateValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'bail|required|unique:domain_products,type,'.$this->id,
            'local_type' => 'bail|required',
            'annually' => 'bail|numeric',
            'biennially' => 'bail|numeric',
            'triennially' => 'bail|numeric'
        ];
    }

    public function messages()
    {
        return [
            'type.required' => 'Tên sản phẩm không được để trống',
            'type.unique' => 'Loại tên miền đã được tạo',
            'local_type.required' => 'Vùng tên miền không được để trống',
            'annually.numeric' => 'Giá tiền phải là ký tự số',
            'biennially.numeric' => 'Giá tiền phải là ký tự số',
            'triennially.numeric' => 'Giá tiền phải là ký tự số',
        ];
    }
}
