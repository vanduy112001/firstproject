<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchDomainValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'domain' => 'bail|required|min:2|regex:/^[a-zA-Z0-9.]+$/',
        ];
    }

    public function messages()
    {
        return [
            'domain.required' => 'Vui lòng nhập tên miền',
            'domain.min' => 'Tên domain có ít nhất 2 kí tự',
            'domain.regex' => 'Tên domain không được có kí tự đặt biệt',
        ];
    }
}
