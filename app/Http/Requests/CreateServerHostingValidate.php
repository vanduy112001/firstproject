<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateServerHostingValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ip' => 'bail|required',
            'host' => 'bail|required',
            'user_name' => 'bail|required',
            'password' => 'bail|required',
            'port' => 'bail|required',
            'type_server_hosting' => 'bail|required',
            'name' => 'bail|required',
        ];
    }

    public function messages()
    {
        return [
            'ip.required' => 'Trường IP không được để trống',
            'host.required' => 'Trường Domain không được để trống',
            'user.required' => 'Trường Username không được để trống',
            'password.required' => 'Trường password không được để trống',
            'type_server_hosting.required' => 'Trường loại server hosting không được để trống',
            'port.required' => 'Trường port không được để trống',
            'name.required' => 'Trường tên server không được để trống',
        ];
    }
}
