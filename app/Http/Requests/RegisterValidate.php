<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name' => 'bail|required|min:2|max:20|regex:/(^[\pL0-9 ]+)$/u',
            'first_name' => 'bail|required|min:1|max:20|regex:/(^[\pL0-9 ]+)$/u',
            'gender' => 'bail|required|not_in:0',
            'date' => 'bail|required|nullable',
            'phone' => 'bail|required|numeric',
            'address' => 'bail|required|max:200',
            'email' => 'bail|required|email|max:255|unique:users,email,'.$this->id,
            'password' => 'bail|required|confirmed|min:6|max:64|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&"*()\-_=+{};:,<.>]).{8,255}+$/',
            'password_confirmation' => 'bail|required|same:password',
        ];
    }

    public function messages()
    {
        return [
            'last_name.required' => 'Last Name không được để trống',
            'last_name.min' => 'Last Name không được nhỏ hơn :min ký tự',
            'last_name.max' => 'Last Name không được lớn hơn :max ký tự',
            'last_name.regex' => 'Không được đặt tên với ký tự đặt biệt',
            'first_name.required' => 'Tên không được để trống',
            'first_name.min' => 'Tên không được nhỏ hơn :min ký tự',
            'first_name.max' => 'Tên không được lớn hơn :max ký tự',
            'first_name.regex' => 'Không được đặt tên với ký tự đặt biệt',
            'phone.required' => 'Số điện thoại không được để trống',
            'phone.numeric' => 'Số điện thoại phải là ký tự số',
            'address.required' => 'Địa chỉ không được để trống',
            'address.max' => 'Địa chỉ không được lớn hơn :max ký tự',
            'gender.required' => 'Giới tính không được để trống',
            'gender.not_in' => 'Giới tính không được để trống',
            'date.required' => 'Ngày tháng không được để trống',
            'date.nullable' => 'Ngày tháng không được để trống',
            'email.required' => 'Email không được để trống',
            'email.email' => 'Email không đúng định dạng',
            'email.max' => 'Email không được lớn hơn :max ký tự',
            'email.unique' => 'Email đã được đăng ký',
            'password.required' => 'Mật khẩu không được để trống',
            'password.confirmed' => 'Xác nhận mật khẩu không đúng',
            'password.min' => 'Mật khẩu không được nhỏ hơn :min ký tự',
            'password.max' => 'Mật khẩu không được lớn hơn :max ký tự',
            'password.regex' => 'Mật khẩu phải có ký tự chữ số, chữ hoa, chữ thường và ký tự đặc biệt',
            'password_confirmation.required' => 'Xác nhận mật khẩu không được để trống',
            'password_confirmation.same' => 'Xác nhận mật khẩu không đúng',
        ];
    }

}
