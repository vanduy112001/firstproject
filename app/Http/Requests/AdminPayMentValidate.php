<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminPayMentValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'amount' => 'bail|required|numeric',
            'method_pay' => 'bail|required|not_in:0',
            'user_id' => 'bail|required|not_in:0',
        ];
    }

    public function messages()
    {
        return [
            'amount.required' => 'Số tiền nạp không được để trống',
            'amount.numeric' => 'Số tiền nạp không phải là ký tự số',
            'method_pay.not_in' => 'Hình thức thanh toán không được để trống',
            'method_pay.required' => 'Hình thức thanh toán không được để trống',
            'user_id.not_in' => 'Khách hàng không được để trống',
            'user_id.required' => 'Khách hàng không được để trống',
        ];
    }
}
