<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterDomainValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'password-domain' => 'bail|required|min:8|max:15|regex: /^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9]).+$/',
            'customer-domain' => 'bail|required',
            'owner-name' => 'bail|required',
            // 'ownerid-number' => 'bail|required|numeric',
            // 'owner-taxcode' => 'bail|required|numeric',
            'owner-address' => 'bail|required',

            'owner-phone' => 'bail|required|numeric',

            'owner-email' => 'bail|required|email',

            'ui-name' => 'bail|required',

            // 'uiid-number' => 'bail|required|numeric',

            'ui-address' => 'bail|required',

            // 'ui-birthdate' => 'bail|required',

            'ui-phone' => 'bail|required|numeric',

            // 'adminid-number' => 'bail|required|numeric',

            // 'admin-phone' => 'bail|required|numeric',
            // 'cmnd_after'        =>  'bail|required|mimes:jpeg,jpg,png|max:24048',
            // 'cmnd_before'        =>  'bail|required|mimes:jpeg,jpg,png|max:24048',

        ];
    }
    public function messages()
    {
        return [
            'password-domain.required' => 'Mật khẩu domain không được để trống',
            'password-domain.min' => 'Mật khẩu domain không được nhỏ hơn 8 kí tự',
            'password-domain.max' => 'Mật khẩu domain không được lớn hơn 15 kí tự',
            'password-domain.regex' => 'Mật khẩu domain phải bao gồm số và chữ',

            'customer-domain.required' => 'Chưa chọn nhóm khách hàng (cá nhân hoặc công ty)',

            'owner-name.required' => 'Tên khách hàng không được để trống',

            // 'cmnd_before.required' => 'Ảnh chứng minh nhân mặt trước không được để trống',
            // 'cmnd_after.required' => 'Ảnh chứng minh nhân mặt sau không được để trống',
            // 'cmnd_after.mimes' => 'Định dạng ảnh chứng minh nhân dân mặt sau không chính xác (Các định dạng ảnh cho phép jpeg,jpg,png)',
            // 'cmnd_before.mimes' => 'Định dạng ảnh chứng minh nhân dân mặt trước không chính xác (Các định dạng ảnh cho phép jpeg,jpg,png)',
            // 'cmnd_before.max' => 'Ảnh chứng minh nhân mặt trước không vượt quá 24MB',
            // 'cmnd_after.max' => 'Ảnh chứng minh nhân mặt sau không vượt quá 24MB',

            // 'ownerid-number.required' => 'Chứng minh thư khách hàng không được để trống',
            // 'ownerid-number.numeric' => 'Chứng minh thư khách hàng phải là kiểu số',

            // 'owner-taxcode.required' => 'Chứng minh thư khách hàng không được để trống',
            // 'owner-taxcode.numeric' => 'Mã số thuế khách hàng phải là kiểu số',

            'owner-address.required' => 'Địa chỉ khách hàng không được để trống',

            'owner-phone.required' => 'Số điện thoại khách hàng không được để trống',
            'owner-phone.numeric' => 'Số điện thoại khách hàng phải là kiểu số',

            'owner-email.required' => 'Email khách hàng không được để trống',
            'owner-email.email' => 'Email khách hàng không đúng định dạng',

            'ui-name.required' => 'Tên chủ thể không được để trống',

            // 'uiid-number.required' => 'Chứng minh thư chủ thể không được để trống',
            // 'uiid-number.numeric' => 'Chứng minh thư chủ thể phải là kiểu số',

            'ui-address.required' => 'Địa chỉ chủ thể không được để trống',

            // 'ui-birthdate.required' => 'Ngày sinh chủ thể không được để trống',

            'ui-phone.required' => 'Ngày sinh chủ thể không được để trống',
            'ui-phone.numeric' => 'Số điện thoại chủ thể phải là kiểu số',

            // 'adminid-number.required' => 'Chứng minh thư người quản lí không được để trống',
            // 'adminid-number.numeric' => 'Chứng minh thư người quản lí phải là kiểu số',

            // 'admin-phone.required' => 'Số điện thoại người quản lí không được để trống',
            // 'admin-phone.numeric' => 'Số điện thoại người quản lí phải là kiểu số',


        ];
    }
}
