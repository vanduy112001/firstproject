<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProfileValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
     {
         return [
             'name' => 'bail|required',
             'phone'  => 'bail|required|min:10|numeric',
             'gender'  => 'bail|not_in:0',
             'address'  => 'bail|required',
             'date'  => 'bail|required',
         ];
     }

     public function messages()
     {
         return [
             'name.required'     =>  'Họ và Tên không được để trống',
             'date.required'    =>  'Ngày sinh không được để trống',
             'address.required'    =>  'Địa chỉ không được để trống',
             'phone.required'    =>  'Số điện thoại không được để trống',
             'phone.numeric'     =>  'Số điện thoại phải là ký tự số',
             'phone.min'     =>  'Số điện thoại không đúng',
             'gender.not_in'     =>  'Giới tình không được chọn',
         ];
     }
}
