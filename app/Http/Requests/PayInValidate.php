<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PayInValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'money' => 'bail|required|numeric|min:200000,max:100000000',
            'method_pay' => 'bail|required|not_in:0',
        ];
    }

    public function messages()
    {
        return [
            'money.required' => 'Số tiền nạp không được để trống',
            'money.numeric' => 'Số tiền nạp không phải là ký tự số',
            'money.min' => 'Số tiền nạp không được nhỏ hơn 200.000 VNĐ hoặc lớn hơn 100.000.000 VNĐ',
            'money.max' => 'Số tiền nạp không được nhỏ hơn 200.000 VNĐ hoặc lớn hơn 100.000.000 VNĐ',
            'method_pay.not_in' => 'Hình thức thanh toán không được để trống',
            'method_pay.required' => 'Hình thức thanh toán không được để trống',
        ];
    }
}
