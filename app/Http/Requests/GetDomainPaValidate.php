<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetDomainPaValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'bail|required|numeric',
            'product_id' => 'bail|required|numeric',
            'domain' => 'bail|required',
            'customer_domain' => 'bail|required',
            'date_create' => 'bail|required',
            'next_due_date' => 'bail|required',
            'date_paid' => 'bail|required',
            'owner-name' => 'bail|required',
            'owner-address' => 'bail|required',
            'owner-email' => 'bail|required',
            'owner-phone' => 'bail|required',
            'ui_name' => 'bail|required',
            'uiid_number' => 'bail|required',
            'ui_phone' => 'bail|required',
            'ui_email' => 'bail|required',
            'ui_gender' => 'bail|required',
            'ui_birthdate' => 'bail|required',
            'ui_province' => 'bail|required',
            'ui_address' => 'bail|required',
            'admin-name' => 'bail|required',
            'adminid-number' => 'bail|required',
            'admin-gender' => 'bail|required',
            'admin-birthdate' => 'bail|required',
            'admin-address' => 'bail|required',
            'admin-province' => 'bail|required',
            'admin-email' => 'bail|required',
            'admin-phone' => 'bail|required',
        ];
    }

    public function messages()
    {
        return [
            'user_id.required' => 'Khách hàng không được để trống',
            'user_id.numeric' => 'Khách hàng phải là ký tự số',
            'product_id.required' => 'Sản phẩm không được để trống',
            'product_id.numeric' => 'Sản phẩm phải là ký tự số',
            'status.required' => 'Trạng thái không được để trống',
            'domain.required' => 'Tên Domain không được để trống',
            'customer_domain.required' => 'Khách hàng sử dụng không được để trống',
            'date_create.required' => 'Ngày tạo không được để trống',
            'next_due_date.required' => 'Ngày kết thúc không được để trống',
            'date_paid.required' => 'Ngày thanh toán không được để trống',

            'owner-name.required' => 'Thông tin khách hàng (owner) - Tên (cá nhân hoặc công ty) không được để trống',
            'owner-email.address' => 'Thông tin khách hàng (owner) - Địa chỉ không được để trống',
            'owner-email.required' => 'Thông tin khách hàng (owner) - Email không được để trống',
            'owner-phone.required' => 'Thông tin khách hàng (owner) - Phone không được để trống',

            'ui_name.required' => 'Thông tin chủ thể (ui) - Tên chủ thể(cá nhân hoặc công ty) không được để trống',
            'uiid_number.required' => 'Thông tin chủ thể (ui) - Chứng minh nhân dân không được để trống',
            'ui_phone.required' => 'Thông tin chủ thể (ui) - Số điện thoại không được để trống',
            'ui_email.required' => 'Thông tin chủ thể (ui) - Email không được để trống',
            'ui_gender.required' => 'Thông tin chủ thể (ui) - Giới tính không được để trống',
            'ui_birthdate.required' => 'Thông tin chủ thể (ui) - Ngày sinh không được để trống',
            'ui_province.required' => 'Thông tin chủ thể (ui) - Tỉnh/Thành phố không được để trống',
            'ui_address.required' => 'Thông tin chủ thể (ui) - Địa chỉ không được để trống',

            'admin-name.required' => 'Thông tin người quản lý (admin) - Tên không được để trống',
            'admin-number.required' => 'Thông tin người quản lý (admin) - Chứng minh thư không được để trống',
            'admin-gender.required' => 'Thông tin người quản lý (admin) - Giới tính không được để trống',
            'admin-birthdate.required' => 'Thông tin người quản lý (admin) - Ngày sinh nhật không được để trống',
            'admin-address.required' => 'Thông tin người quản lý (admin) - Địa chỉ không được để trống',
            'admin-province.required' => 'Thông tin người quản lý (admin) - Tình / Thành phố không được để trống',
            'admin-email.required' => 'Thông tin người quản lý (admin) - Email không được để trống',
            'admin-phone.required' => 'Thông tin người quản lý (admin) - Phone không được để trống',
        ];
    }

}
