<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'name' => 'bail|required',
            // 'email'    => 'bail|required|email|unique:users',
            // 'password' => 'bail|required|min:6|confirmed',
            // 'phone'  => 'bail|min:10|numeric',
        ];
    }

    public function messages()
    {
        return [
            // 'name.required'     =>  'Tên không được để trống',
            // 'email.required'    =>  'Email không được để trống',
            // 'email.email'       =>  'Email không đúng định dạng',
            // 'email.unique' =>  'Email đã có người sử dụng',
            // 'password.min'    =>  'Mật khẩu không được ngắn hơn 6 ký tự',
            // 'phone.numeric'     =>  'Số điện thoại phải là ký tự số',
            // 'phone.min'     =>  'Số điện thoại không đúng',
        ];
    }
}
