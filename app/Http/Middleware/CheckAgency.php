<?php

namespace App\Http\Middleware;

use Hash;
use Closure;
use App\Model\Agency;

class CheckAgency
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $agency = Agency::where('user_api', $request->get('user') )->where('password_api', $request->get('password') )->first();
        $data_restore = [];
        try {
          if ( isset($agency) ) {
              $token = $request->header('Authorization');
              if (Hash::check($token , $agency->token_api))
              {
                  return $next($request);
              }
              else {
                  $data_restore = [
                    'error' => 2,
                    'status' => 'Lỗi token không đúng',
                  ];
                  return $data_restore;
              }
          }
          else {
            $data_restore = [
              'error' => 1,
              'status' => 'Kết nối thất bại',
            ];
            return $data_restore;
          }
        } catch (\Exception $e) {
          report($e);
          $data_restore = [
            'error' => 2,
            'status' => 'Kết nối thất bại',
          ];
          return $data_restore;
        }
        return $next($request);
    }
}
