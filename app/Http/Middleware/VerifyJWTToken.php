<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class VerifyJWTToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            // $user = JWTAuth::toUser($request->header('Authorization'));
            $token = $request->header('Authorization');
            $user = JWTAuth::setToken($token)->toUser();
            if ( $user->user_meta->role != 'admin' ) {
                return response()->json([
                    'error' => 4,
                    'message' => 'Không có quyền truy cập'
                ]);
            }
        }
        catch (JWTException $e) {
            report($e);
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException) {
                return response()->json([
                    'error' => 9999,
                    'message' => 'Token hết hạn'
                ]);
            }
            else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException) {
                return response()->json([
                    'error' => 9998,
                    'message' => 'Token không hợp lệ.'
                ]);
            }
            else {
                return response()->json([
                    'error' => 9997,
                    'message' => 'Token không tồn tại.'
                ]);
            }
        }
        return $next($request);
    }
}
