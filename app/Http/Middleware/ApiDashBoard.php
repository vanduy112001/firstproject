<?php

namespace App\Http\Middleware;

use Closure;

class ApiDashBoard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {   
        try {
            if ($request->header('Authorization') == 'Token orCq7S2m^2!8!pT*V%TtBxZSBqU9B%@XOnlUCZR^h&EOUx9az%uemtFTFE8LuY%xJA66@$xm1QXjjrNd5C31ht9VwbDU1jS1uBskh6o5cUP3FRVtH%*mxSWCrGdSGDncg&ZtA7v3LYC6J&jMtbavkFmJ8Poe&D*rKv3Dnh!izh9L3OCmoUX1#S8RTxm@BFF&Yi*gRMFWaFmEGvJXf^#VpvIV%^&DM@LMK!QjEUGWw$tSXbrNPq*pRqOHdNU04I42Pwz8sMqyO0IPJW%eyHXYlUT1RBJnE187zw&vlWgha5IW8g#tOPHA*rL$y8Lr6ADBFBlsJ4dyvOAVKj&V^dR#vdC#8sMsE#r1k0IyqTjl8ms1%0%m*bnaIBBdn8eszMY3AKAXryD62udOKiuIm!H&W6wu*1pV8Gj$3AfegwhO9QFU7n!rLrooUY!bxymxmvilqsPX&2Dxx03!1yB&2G2Ar5PMX4qaf5SZqUYNh4ntQVmCx7m2oPrU3Oo6WudpN*W8') {
                return $next($request);
            }else {
                $data = [
                    'error' => 1,
                    'status' => 'Lỗi kết nối',
                ];
                return response()->json($data , 403, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
            }
        } catch (\Throwable $th) {
            $data = [
                'error' => 1,
                'status' => 'Lỗi kết nối',
            ];
            return response()->json($data , 403, ['Content-type'=> 'application/json; charset=utf-8'], JSON_UNESCAPED_UNICODE);
        }
    }
}
