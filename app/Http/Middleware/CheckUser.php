<?php

namespace App\Http\Middleware;

use App\Factories\AdminFactories;
use Closure;
use Illuminate\Support\Facades\Auth;

class CheckUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
            // dd($request->url());
        try {
            if(Auth::check()) {
                $userRepo = AdminFactories::loginRepositories();
                $user = $userRepo->detail(Auth::user()->id);
                if(!empty($user->email_verified_at)) {
                    return $next($request);
                } else {
                    Auth::logout();
                    return redirect(route('login'))->with('fails', 'Tài khoản chưa kích hoạt email. Vui lòng kích hoạt email để đăng nhập.');
                }
                
            } else {
                $request->session()->put('url_request', $request->fullUrl());
                return redirect(route('login'));
            }
        } catch (\Throwable $th) {
            $request->session()->put('url_request', $request->fullUrl());
            return redirect(route('login'));
        }
    }
}
