<?php

namespace App\Helpers;

use App\Factories\UserFactories;
use App\Factories\AdminFactories;

Class User {
	public static function get_user($id)
	{
		$repo = UserFactories::userRepositories();
		$user = $repo->detail($id);
        return $user;
	}

	public static function get_cmnd_unactive_number()
	{
		$repo = AdminFactories::cmndRepositories();
		$cmnd_unactive_number = $repo->get_cmnd_unactive_number();
		return $cmnd_unactive_number;
	}

	public static function get_addon_product_private($id)
	{
		$repo = UserFactories::homeRepositories();
		$add_on_products = $repo->get_addon_product_private($id);
		return $add_on_products;
	}

	public static function admin_get_qtt_ticket_pending()
	{
			$repo = AdminFactories::ticketRepository();
			$ticket = $repo->get_qtt_ticket_pending();
			return $ticket;
	}

	public static function admin_get_qtt_payment_confirm()
	{
			$repo = AdminFactories::paymentRepositories();
			$order = $repo->get_qtt_payment_confirm();
			return $order;
	}

	public static function get_contract_detail_vps($id, $product_type)
	{
			$repo = AdminFactories::contractRepositories();
			$vps = $repo->get_contract_detail_vps($id, $product_type);
			return $vps;
	}

	public static function get_contract_detail_vps_us($id, $product_type)
	{
			$repo = AdminFactories::contractRepositories();
			$vps = $repo->get_contract_detail_vps_us($id, $product_type);
			return $vps;
	}

    public static function get_contract_detail_hosting($id, $product_type)
	{
			$repo = AdminFactories::contractRepositories();
			$hosting = $repo->get_contract_detail_hosting($id, $product_type);
			return $hosting;
	}

    public static function get_contract_detail_email_hosting($id, $product_type)
	{
			$repo = AdminFactories::contractRepositories();
			$email_hosting = $repo->get_contract_detail_email_hosting($id, $product_type);
			return $email_hosting;
	}

	public static function get_contract_detail_server($id)
	{
			$repo = AdminFactories::contractRepositories();
			$server = $repo->get_contract_detail_server($id);
			return $server;
	}

	public static function get_contract_detail_colocation($id)
	{
			$repo = AdminFactories::contractRepositories();
			$server = $repo->get_contract_detail_colocation($id);
			return $server;
	}

	public static function get_contract_detail_domain($id)
	{
			$repo = AdminFactories::contractRepositories();
			$domain = $repo->get_contract_detail_domain($id);
			return $domain;
	}

	public static function get_contract_pending()
	{
			$repo = AdminFactories::contractRepositories();
			$contract_pendings = $repo->get_contract_pending();
			return $contract_pendings;
	}

	public static function getGroupUser()
	{
		$repo = AdminFactories::userRepositories();
		return $repo->getGroupUser();
	}

}
