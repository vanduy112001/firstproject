<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GroupEmail extends Model
{
    protected $fillable = ['name'];

    public function emails()
    {
      return $this->hasMany('App\Model\Email');
    }
}
