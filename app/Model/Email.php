<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $fillable = [ 'group_email_id', 'name', 'type', 'status', 'type_service' ];

    public function group_email()
    {
      return $this->belongsTo('App\Model\GroupEmail');
    }

    public function meta_email()
    {
      return $this->hasOne('App\Model\MetaEmail');
    }

    public function mail_marketing()
    {
      return $this->hasOne('App\Model\MailMarketing');
    }

    public function email_group_users()
    {
      return $this->hasMany('App\Model\EmailGroupUser');
    }

}
