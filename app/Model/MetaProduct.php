<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MetaProduct extends Model
{
    protected $fillable = [
        'product_id', 'email_id', 'hidden', 'cpu', 'memory', 'disk', 'os', 'ip', 'bandwidth', 'storage',
        'domain', 'sub_domain', 'alias_domain', 'database', 'ftp', 'panel', 'name_server', 'email_create',
        'email_expired', 'email_expired_finish', 'type_addon', 'product_special', 'girf', 'promotion', 'qtt_email',
        'chassis', 'raid', 'datacenter', 'server_management', 'backup', 'cores', 'port_network', 'email_config_finish'
    ];

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }


}
