<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MetaEmail extends Model
{
    protected $fillable =
      [
        'subject', 'content', 'discription', 'email_id', 'type_group', 'time', 'date'
      ];

    public function email()
    {
      return belongsTo('App\Model\Email');
    }
}
