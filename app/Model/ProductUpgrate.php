<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductUpgrate extends Model
{
    protected $fillable = [
        'product_default_id', 'product_upgrate_id',
    ];

    public function product_default()
    {
       return $this->belongsTo('App\Model\Product', 'product_default_id', 'id');
    }

    public function product_upgrate()
    {
       return $this->belongsTo('App\Model\Product', 'product_upgrate_id', 'id');
    }

}
