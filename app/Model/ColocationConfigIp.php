<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ColocationConfigIp extends Model
{
    protected $fillable = [
        'colocation_id', 'product_id'
    ];

    public function colocation()
    {
        return $this->belongsTo('App\Model\Colocation');
    }

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }


}
