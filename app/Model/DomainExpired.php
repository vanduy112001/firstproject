<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class DomainExpired extends Model
{
    protected $fillable = ['detail_order_id', 'domain_id', 'billing_cycle'];

    public function domain()
    {
        return $this->belongsTo('App\Model\Domain');
    }
    public function detail_order()
    {
        return $this->belongsTo('App\Model\DetailOrder');
    }
}
