<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VpsConfig extends Model
{
    protected $fillable = [
        'vps_id', 'ip', 'disk', 'ram', 'cpu',  
    ];

    public function vps()
    {
        return $this->belongsTo('App\Model\Vps');
    }

}
