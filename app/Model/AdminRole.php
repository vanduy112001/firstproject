<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdminRole extends Model
{
    protected $fillable = [
        'name', 'description'
    ];

    public function user()
    {
        return $this->hasMany('App\Model\User');
    }

    public function admin_role_meta()
    {
        return $this->hasMany('App\Model\AdminRoleMeta');
    }

}