<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PromotionMeta extends Model
{
    protected $fillable = [
        'user_id','promotion_id', 'use',
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function promotion()
    {
        return $this->belongsTo('App\Model\Promotion');
    }




}
