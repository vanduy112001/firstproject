<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServerConfigIp extends Model
{
    protected $fillable = [
        'server_id', 'product_id', 'ips'
    ];

    public function server()
    {
        return $this->belongsTo('App\Model\Server');
    }

    public function product()
    {
       return $this->belongsTo('App\Model\Product');
    }

}
