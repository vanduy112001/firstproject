<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmailGroupUser extends Model
{
    protected $fillable = [ 'email_id', 'group_user_id' ];

    public function email()
    {
        return $this->belongsTo('App\Model\Email');
    }

    public function group_user()
    {
        return $this->belongsTo('App\Model\GroupUser');
    }
}
