<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
    	'user_id', 'status'
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function message_metas()
    {
        return $this->hasMany('App\Model\MessageMeta');
    }

}
