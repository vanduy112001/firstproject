<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderExpired extends Model
{
    protected $fillable = [ 'detail_order_id', 'expired_id' , 'billing_cycle', 'type'];

    public function detail_order()
    {
        return $this->belongsTo('App\Model\DetailOrder');
    }

    public function vps()
    {
        return $this->belongsTo('App\Model\VPS', 'expired_id' , 'id');
    }

    public function proxy()
    {
        return $this->belongsTo('App\Model\Proxy', 'expired_id' , 'id');
    }

    public function hosting()
    {
        return $this->belongsTo('App\Model\Hosting', 'expired_id' , 'id');
    }

    public function email_hosting()
    {
        return $this->belongsTo('App\Model\EmailHosting', 'expired_id' , 'id');
    }

    public function server()
    {
        return $this->belongsTo('App\Model\Server', 'expired_id' , 'id');
    }

    public function colocation()
    {
        return $this->belongsTo('App\Model\Colocation', 'expired_id' , 'id');
    }

}
