<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderAddonVps extends Model
{
    protected $fillable = [
        'detail_order_id', 'vps_id', 'ram', 'cpu', 'disk', 'ip', 'month', 'day'
    ];

    public function detail_order()
    {
        return $this->belongsTo('App\Model\DetailOrder');
    }

    public function vps()
    {
        return $this->belongsTo('App\Model\Vps');
    }

}
