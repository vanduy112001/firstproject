<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MetaGroupProduct extends Model
{
    protected $fillable = [
        'group_product_id', 'user_id', 'group_product_duplicate',
    ];

    public function group_product()
    {
        return $this->belongsTo('App\Model\GroupProduct');
    }

}
