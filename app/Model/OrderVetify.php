<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderVetify extends Model
{
    protected $fillable = [ 'order_id', 'token' ];

    public function order()
    {
        return $this->belongsTo('App\Model\Order');
    }
}
