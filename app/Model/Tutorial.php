<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tutorial extends Model
{
  protected $fillable =
    [
      'title', 'descripton', 'note', 'link'
    ];
}
