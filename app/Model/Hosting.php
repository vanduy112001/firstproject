<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Hosting extends Model
{
    protected $fillable = [
        'detail_order_id', 'user_id', 'product_id', 'domain', 'billing_cycle', 'next_due_date',
        'status', 'user', 'password','date_create', 'paid', 'status_hosting','addon_id', 'addon_qtt',
          'expired_id', 'server_hosting_id', 'location', 'price_override', 'promotion', 'expire_billing_cycle',
    ];

    public function detail_order()
    {
        return $this->belongsTo('App\Model\DetailOrder');
    }
    public function user_hosting()
    {
        return $this->belongsTo('App\Model\User', 'user_id', 'id');
    }
    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }
    public function order_expireds()
    {
        return $this->hasMany('App\Model\OrderExpired');
    }
    public function addon()
    {
        return $this->belongsTo('App\Model\Product', 'addon_id', 'id');
    }

    public function expired_detail_order()
    {
        return $this->belongsTo('App\Model\DetailOrder', 'expired_id', 'id');
    }

    public function server_hosting()
    {
        return $this->belongsTo('App\Model\ServerHosting', 'server_hosting_id', 'id');
    }

    public function order_upgrade_hosting()
    {
       return $this->hasMany('App\Model\OrderUpgradeHosting');
    }

}
