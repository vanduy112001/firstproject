<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ColocationIp extends Model
{
    protected $fillable = [
        'colocation_id', 'ip'
    ];

    public function colocation()
    {
        return $this->belongsTo('App\Model\Colocation');
    }

}
