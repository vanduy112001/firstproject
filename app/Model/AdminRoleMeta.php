<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdminRoleMeta extends Model
{
    protected $fillable = [
        'admin_role_id', 'page'
    ];
    
    public function admin_role()
    {
        return $this->belongsTo('App\Model\AdminRole');
    }

}
