<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderAddonServer extends Model
{
    protected $fillable = [
      'detail_order_id', 'server_id', 'list_ip', 'ip', 'ram', 'disk'
    ];

    public function server()
    {
        return $this->belongsTo('App\Model\Server');
    }

    public function detail_order()
    {
        return $this->belongsTo('App\Model\DetailOrder');
    }

}
