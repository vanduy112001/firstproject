<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'type_product', 'group_product_id', 'name', 'module_id', 'hidden', 
        'stt', 'package','description', 'duplicate'
    ];

    public function group_product()
    {
        return $this->belongsTo('App\Model\GroupProduct');
    }

    public function module()
    {
        return $this->hasOne('App\Model\Module');
    }

    public function pricing()
    {
        return $this->hasOne('App\Model\Pricing');
    }

    public function quotation_details()
    {
        return $this->hasMany('App\Model\QuotationDetail');
    }

    public function product_datacenters()
    {
        return $this->hasMany('App\Model\ProductDatacenter');
    }

    public function product_raids()
    {
        return $this->hasMany('App\Model\ProductRaid');
    }

    public function server_managements()
    {
        return $this->hasMany('App\Model\ServerManagement');
    }

    public function meta_product()
    {
        return $this->hasOne('App\Model\MetaProduct');
    }

    public function product_drive()
    {
        return $this->hasOne('App\Model\ProductDrive');
    }

    public function vps()
    {
        return $this->hasMany('App\Model\Vps');
    }
    public function proxies()
    {
        return $this->hasMany('App\Model\Proxy');
    }
    public function hostings()
    {
        return $this->hasMany('App\Model\Hosting');
    }
    public function servers()
    {
        return $this->hasMany('App\Model\Server');
    }
    public function event_promotion()
    {
        return $this->hasMany('App\Model\EventPromotion');
    }
    public function vps_os()
    {
        return $this->hasMany('App\Model\VpsOs');
    }

    public function product_upgrates()
    {
        return $this->hasMany('App\Model\ProductUpgrate', 'product_default_id', 'id');
    }

    public function order_upgrade_hosting()
    {
       return $this->hasMany('App\Model\OrderUpgradeHosting');
    }

    public function product_promotions()
    {
        return $this->hasMany('App\Model\ProductPromotion');
    }

    public function server_config_rams()
    {
        return $this->hasMany('App\Model\ServerConfigRam');
    }

    public function server_config_ips()
    {
        return $this->hasMany('App\Model\ServerConfigIp');
    }
    
}
