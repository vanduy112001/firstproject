<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderUpgradeHosting extends Model
{
    protected $fillable = [
        'detail_order_id', 'hosting_id', 'product_id',
    ];

    public function detail_order()
    {
        return $this->belongsTo('App\Model\DetailOrder');
    }

    public function hosting()
    {
        return $this->belongsTo('App\Model\Hosting');
    }

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }

}
