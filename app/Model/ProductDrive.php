<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductDrive extends Model
{
    protected $fillable = [
        'product_id', 'first', 'second', 'third', 'four ', 'five', 'six', 'seven', 'eight'
    ];

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }
}
