<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServerManagement extends Model
{
    protected $fillable = [
        'product_id', 'server_management',
    ];

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }
}
