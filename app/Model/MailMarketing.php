<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MailMarketing extends Model
{
    protected $fillable = [ 'email_id', 'qtt' ];

    public function email()
    {
        return $this->belongsTo('App\Model\Email');
    }
    
}
