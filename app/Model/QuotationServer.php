<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QuotationServer extends Model
{
    protected $fillable = [
        'quotation_detail_id', 'addon_ram', 'addon_ip', 'addon_disk2', 'addon_disk3',
        'addon_disk4', 'addon_disk5', 'addon_disk6', 'addon_disk7', 'addon_disk8', 
    ];

    public function quotation_detail()
    {
        return $this->belongsTo('App\Model\QuotationDetail');
    }

    public function product_addon_ram()
    {
       return $this->belongsTo('App\Model\Product', 'addon_ram', 'id');
    }

    public function product_addon_ip()
    {
       return $this->belongsTo('App\Model\Product', 'addon_ip', 'id');
    }

    public function product_disk2()
    {
       return $this->belongsTo('App\Model\Product', 'addon_disk2', 'id');
    }

    public function product_disk3()
    {
       return $this->belongsTo('App\Model\Product', 'addon_disk3', 'id');
    }

    public function product_disk4()
    {
       return $this->belongsTo('App\Model\Product', 'addon_disk4', 'id');
    }

    public function product_disk5()
    {
       return $this->belongsTo('App\Model\Product', 'addon_disk5', 'id');
    }

    public function product_disk6()
    {
       return $this->belongsTo('App\Model\Product', 'addon_disk6', 'id');
    }

    public function product_disk7()
    {
       return $this->belongsTo('App\Model\Product', 'addon_disk7', 'id');
    }

    public function product_disk8()
    {
       return $this->belongsTo('App\Model\Product', 'addon_disk8', 'id');
    }
}
