<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GroupProduct extends Model
{
    protected $fillable = [
        'name', 'title', 'private', 'group_user_id', 'type', 'stt', 'hidden', 'link'
    ];

    public function products()
    {
        return $this->hasMany('App\Model\Product');
    }

    public function meta_group_products()
    {
        return $this->hasMany('App\Model\MetaGroupProduct');
    }

    public function group_user()
    {
        return $this->belongsTo('App\Model\GroupUser');
    }

}
