<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CmndVerify extends Model
{
    protected $table = 'cmnd_verifies';
    public $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'cmnd_after', 'cmnd_before', 'active', 'active_at'
    ];
    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }
}
