<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class GroupUser extends Model
{
    protected $fillable = ['name'];

    public function users()
    {
        return $this->hasMany('App\Model\User');
    }

    public function group_products()
    {
        return $this->hasMany('App\Model\GroupProduct');
    }

    public function email_group_users()
    {
      return $this->hasMany('App\Model\EmailGroupUser');
    }

}
