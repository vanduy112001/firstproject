<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServerConfig extends Model
{
    protected $fillable = [
        'server_id', 'ip', 'disk', 'ram', 'disk2', 'disk3', 'disk4', 'disk5',
        'disk6', 'disk7', 'disk8'
    ];

    public function server()
    {
        return $this->belongsTo('App\Model\Server');
    }

    public function product_disk2()
    {
       return $this->belongsTo('App\Model\Product', 'disk2', 'id');
    }

    public function product_disk3()
    {
       return $this->belongsTo('App\Model\Product', 'disk3', 'id');
    }

    public function product_disk4()
    {
       return $this->belongsTo('App\Model\Product', 'disk4', 'id');
    }

    public function product_disk5()
    {
       return $this->belongsTo('App\Model\Product', 'disk5', 'id');
    }

    public function product_disk6()
    {
       return $this->belongsTo('App\Model\Product', 'disk6', 'id');
    }

    public function product_disk7()
    {
       return $this->belongsTo('App\Model\Product', 'disk7', 'id');
    }

    public function product_disk8()
    {
       return $this->belongsTo('App\Model\Product', 'disk8', 'id');
    }

}
