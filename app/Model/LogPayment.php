<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LogPayment extends Model
{
    protected $fillable = [
      'history_pay_id', 'before', 'after',
    ];

    public function history_pay()
    {
        return $this->belongsTo('App\Model\HistoryPay');
    }

}
