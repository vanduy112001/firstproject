<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $fillable = [
        'name','code', 'type', 'value', 'start_date', 'end_date', 'max_uses', 'uses',
        'type_order', 'apply_one', 'upgrade', 'life_time',
    ];

    public function orders()
    {
        return $this->hasMany('App\Model\Order');
    }

    public function promotion_meta()
    {
        return $this->hasMany('App\Model\PromotionMeta');
    }

    public function product_promotions()
    {
        return $this->hasMany('App\Model\ProductPromotion');
    }

    public function billing_promotions()
    {
        return $this->hasMany('App\Model\BillingPromotion');
    }

}
