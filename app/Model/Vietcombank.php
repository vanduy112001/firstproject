<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vietcombank extends Model
{
    protected $fillable = [
        'sothamchieu', 'total', 'mota', 'ngaygd',
    ];
}
