<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Proxy extends Model
{
    protected $fillable = [
        'detail_order_id', 'user_id', 'product_id', 'ip', 'port',
        'status', 'username', 'password', 'billing_cycle', 'next_due_date',
        'proxy_id', 'description', 'price_override', 
        'location', 'state', 'auto_refurn', 'date_create'
    ];

    public function detail_order()
    {
        return $this->belongsTo('App\Model\DetailOrder');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }

    public function order_expireds()
    {
        return $this->hasMany('App\Model\OrderExpired');
    }

}
