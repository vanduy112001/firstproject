<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ReadNotification extends Model
{
    protected $fillable = [ 'user_id', 'notification_id', 'read_at' ];

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }
    public function notification()
    {
        return $this->belongsTo('App\Model\Notification');
    }
}
