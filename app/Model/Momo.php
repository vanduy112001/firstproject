<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Momo extends Model
{
    protected $fillable = [
        'number_phone', 'amount', 'content', 'date_trade', 'id_momo',
    ];
}
