<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $fillable = [ 'name' ];
    
    public function notification() {
        return $this->hasMany('App\Model\Notification');
    }
}
