<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Agency extends Model
{
    protected $fillable = [
        'user_id', 'user_api', 'password_api', 'token_api',
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

}
