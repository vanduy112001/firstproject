<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EventPromotion extends Model
{
    protected $fillable = [ 'product_id', 'billing_cycle', 'point' ];

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }

}
