<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProductRaid extends Model
{
    protected $fillable = [
        'product_id', 'raid',
    ];

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }
}
