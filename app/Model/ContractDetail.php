<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ContractDetail extends Model
{
    protected $table = 'contract_details';
    public $primaryKey = 'id';
    protected $fillable = [
        'contract_id', 'qtt', 'amount', 'note', 'date_end', 'date_create', 'billing_cycle', 'target_id', 'product_type', 'product_option'
    ];
    public function contract()
    {
        return $this->belongsTo('App\Model\Contract');
    }
    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }
    
}
