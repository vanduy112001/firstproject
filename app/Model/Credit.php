<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    protected $fillable = [ 'user_id', 'value', 'total' ];

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }
}
