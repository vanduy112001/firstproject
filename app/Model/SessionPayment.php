<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SessionPayment extends Model
{
    protected $fillable = ['user_id', 'payment'];
}
