<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ProxyState extends Model
{
    protected $fillable = [
        'name', 'id_state', 'hidden'
    ];
}
