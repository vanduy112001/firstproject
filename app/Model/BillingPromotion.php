<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BillingPromotion extends Model
{
    protected $fillable = [
        'promotion_id', 'billing',
    ];

    public function promotion()
    {
        return $this->belongsTo('App\Model\Promotion');
    }
}
