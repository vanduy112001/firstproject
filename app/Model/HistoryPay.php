<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class HistoryPay extends Model
{
    protected $fillable =
    [
        'user_id', 'ma_gd', 'detail_order_id' , 'discription', 'type_gd', 'method_gd',
        'date_gd', 'money', 'status', 'content', 'method_gd_invoice', 'magd_api', 'type_gd_api',
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function detail_order()
    {
        return $this->belongsTo('App\Model\DetailOrder');
    }

    public function log_payment()
    {
        return $this->hasOne('App\Model\LogPayment');
    }

}
