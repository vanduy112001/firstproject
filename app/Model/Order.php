<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable  = [
        'user_id', 'promotion_id', 'total', 'status', 'description', 'verify', 'makh', 'type'
    ];

    public function promotion()
    {
        return $this->belongsTo('App\Model\Promotion');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function detail_orders()
    {
        return $this->hasMany('App\Model\DetailOrder');
    }

    public function order_vetify()
    {
        return $this->hasOne('App\Model\OrderVetify');
    }

    public function order_expireds()
    {
        return $this->hasManyThrough( 'App\Model\OrderExpired', 'App\Model\DetailOrder', 'order_id', 'detail_order_id', 'id' );
    }

    public function send_mails()
    {
        return $this->hasMany('App\Model\SendMail');
    }

}
