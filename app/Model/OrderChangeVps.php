<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class OrderChangeVps extends Model
{
    protected $fillable = [ 'detail_order_id', 'vps_id' , 'ip' , 'changed_ip'];

    public function detail_order()
    {
        return $this->belongsTo('App\Model\DetailOrder');
    }


    public function vps()
    {
        return $this->belongsTo('App\Model\VPS', 'vps_id' , 'id');
    }
}
