<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $fillable = [
      'user_id', 'link', 'date_create', 'title', 
      'total', 'text_total', 'payment_cycle', 'vat', 'name_replace'
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User', 'user_id', 'id');
    }

    public function quotation_details()
    {
        return $this->hasMany('App\Model\QuotationDetail');
    }

}
