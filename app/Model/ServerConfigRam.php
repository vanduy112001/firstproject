<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServerConfigRam extends Model
{
    protected $fillable = [
        'server_id', 'product_id',
    ];

    public function server()
    {
        return $this->belongsTo('App\Model\Server');
    }

    public function product()
    {
       return $this->belongsTo('App\Model\Product');
    }

}
