<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'tickets';
    protected $fillable = [
        'user_id',
        'user_handle_id',
        'title',
        'content',
        'status',
    ];

    public function messages()
    {
        return $this->hasMany('App\Model\TicketMessage', 'ticket_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }
}
