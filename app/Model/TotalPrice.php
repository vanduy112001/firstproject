<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TotalPrice extends Model
{
    protected $fillable = [
        'vps' , 'hosting', 'server'
    ];
}
