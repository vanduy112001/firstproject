<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ColocationConfig extends Model
{
    protected $fillable = [
        'colocation_id', 'disk', 'ram', 'cpu', 'ip'
    ];

    public function colocation()
    {
        return $this->belongsTo('App\Model\Colocation');
    }

}
