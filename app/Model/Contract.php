<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    protected $table = 'contracts';
    public $primaryKey = 'id';
    protected $fillable = [
        'user_id', 'contract_number', 'company', 'deputy', 'deputy_position', 'address', 'phone', 'email', 'mst', 'date_create', 'date_end', 'title', 'status', 'vat', 'contract_type', 'domain_website', 'abbreviation_name'
    ];
    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }
    // public function product()
    // {
    //     return $this->belongsTo('App\Model\Product');
    // }
    public function contract_details()
    {
        return $this->hasMany('App\Model\ContractDetail');
    }
    
}
