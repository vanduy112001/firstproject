<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{
    protected $fillable = [
        'product_id', 'type', 'monthly', 'quarterly', 'semi_annually', 'annually', 'biennially', 'triennially',
         'one_time_pay', 'twomonthly'
    ];

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }


}
