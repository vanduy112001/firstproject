<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TicketMessage extends Model
{
    protected $table = 'ticket_messages';
    protected $fillable = [
        'ticket_id',
        'user_id',
        'content',
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

}
