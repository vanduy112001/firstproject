<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SendMail extends Model
{
    protected $fillable = [
        'type', 'type_service', 'user_id', 'order_id', 'service_id', 'status', 'content', 'send_time'
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function order()
    {
        return $this->belongsTo('App\Model\Order');
    }

}
