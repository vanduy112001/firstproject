<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class EmailHosting extends Model
{
    protected $fillable = [
        'detail_order_id', 'user_id', 'product_id', 'domain', 'billing_cycle', 'next_due_date',
        'status', 'user', 'password','date_create', 'paid', 'status_hosting', 'price_override', 'expire_billing_cycle',
    ];

    public function detail_order()
    {
        return $this->belongsTo('App\Model\DetailOrder');
    }

    public function user_email_hosting()
    {
        return $this->belongsTo('App\Model\User', 'user_id', 'id');
    }

    public function product()
    {
        return $this->belongsTo('App\Model\Product');
    }

    public function order_expireds()
    {
        return $this->hasMany('App\Model\OrderExpired');
    }
}
