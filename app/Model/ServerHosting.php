<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ServerHosting extends Model
{
    protected $fillable = ['ip', 'host', 'user', 'password', 'port', 'active', 'type_server_hosting', 'name' , 'location' ];

    public function hostings()
    {
        return $this->hasMany('App\Model\Hosting');
    }

}
