<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'user_id', 'type_customer', 'ma_customer', 'customer_name', 'customer_date', 'customer_gender', 'customer_cmnd', 'customer_phone', 'customer_email',
        'customer_address', 'customer_city', 'customer_tc_name', 'customer_tc_mst', 'customer_tc_dctc', 'customer_tc_sdt', 'customer_tc_city', 'customer_dk_name',
        'customer_dk_date', 'customer_dk_gender', 'customer_dk_phone', 'customer_dk_cv', 'customer_dd_name', 'customer_dd_cv', 'customer_dd_gender', 'customer_dd_date',
        'customer_dd_cmnd', 'customer_dd_email', 'customer_tt_name', 'customer_tt_gender', 'customer_tt_date', 'customer_tt_cmnd', 'customer_tt_email', 'customer_dk_cmnd'
    ];

    public function user()
    {
        return $this->belongsTo('App\Model\User');
    }

    public function vps()
    {
        return $this->hasMany('App\Model\Vps', 'ma_customer', 'ma_kh');
    }

}
