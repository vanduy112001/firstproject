<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailable;
use Mail;

class SendVerifyEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $user;
    public $token;
    public function __construct($user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = [
            'name' => $this->user->name,
            'email' => $this->user->email,
            'token' => $this->token,
        ];
        Mail::send('users.mails.verify', $user, function($message){
            $message->from('support@cloudzone.com.vn', 'Cloudzone');
            $message->to($this->user->mail)->subject('Xác thực tài khoản Cloudzone');
        });
    }
}
