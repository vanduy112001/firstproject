<?php
namespace App\Services;

use App\Factories\AdminFactories;
use GuzzleHttp\Client;
use http\Env\Request;
use Mail;
use App\Model\DomainProduct;
use App\Model\Domain;
/**
 *
 */
class DomainPA
{
  protected $domain;
  protected $domain_product;

  public function  __construct()
  {
      $config_domain = config('domain');
      $this->user_name = $config_domain['USERNAME'];
      $this->api_key   = $config_domain['API_KEY'];
      $this->api_url   = $config_domain['API_URL'];
      $this->domain_product = new DomainProduct;
      $this->domain = new Domain;
  }
//   Lấy thông tin domain
  public function get_result_domain($data, $exts)
  {
    //tách tên domain khi gõ kèm đuôi mở rộng (ví dụ: sinh.com), chỉ lấy sinh, bỏ .com
    if (strpos($data['domain'],'.') == true) {
        $domain_name = strstr( $data['domain'], ".", true);
      } else {
       $domain_name = $data['domain'];
     }
    //  $result = 1;
    //  $info   = 'đây là thông tin whois';
     //tạo mảng rỗng để thêm giá trị vào sau khi truy vấn
    $show_result = [];
    // $result = file_get_contents($this->api_url."?username=".$this->user_name."&apikey=".$this->api_key."&cmd=get_whois&domain=".$domain);//Gọi link thực thi thật
    // kiểm tra tất cả các đuôi mở rộng của domain khi chọn radio all
    if ($data['ext'] == 'all') {
      foreach ($exts as $key => $ext) {
        $ext_sub = substr($ext, 1, strlen($ext));
        $domain = $domain_name.$ext;
        $price = $this->domain_product->where('type', $ext_sub)->first();
        $result = file_get_contents($this->api_url."?cmd=check_whois&apikey=".$this->api_key."&username=".$this->user_name."&domain=".$domain);
        $info   = file_get_contents($this->api_url."?cmd=get_whois&apikey=".$this->api_key."&username=".$this->user_name."&domain=".$domain); //Thông tin whois
        $show_result[] = [
          'domain' => $domain,
          'result' => $result,//$result
          'info'   => $info,
          'price'  => isset($price) ? $price->annually : 0,
          // 'id'     => isset($price) ? $price->id : '',
          'ext'    => $ext,
        ];

      }
    // kiểm tra  chọn đuôi mở rộng của domain khiradio cụ thể
    } else {
          $ext = substr($data['ext'], 1, strlen($data['ext']));
          $domain = $domain_name.$data['ext'];
          $price = $this->domain_product->where('type', $ext)->first();
          $result = file_get_contents($this->api_url."?cmd=check_whois&apikey=".$this->api_key."&username=".$this->user_name."&domain=".$domain);
          $info   = file_get_contents($this->api_url."?cmd=get_whois&apikey=".$this->api_key."&username=".$this->user_name."&domain=".$domain); //Thông tin whois
          $show_result[] = [
            'domain' => $domain,
            'result' => $result,
            'info'   => $info,
            'price'  => isset($price) ? $price->annually : 0,
            // 'id'     => isset($price) ? $price->id : '',
            'ext'    => $ext,
          ];
    }
    return $show_result;
  }

  public function registerDomainPA($domain, $due_date)
  {
      $product_domain_id = $domain->product_domain_id;
      $product_domain = $this->domain_product->find($product_domain_id);
      $local_type = $product_domain->local_type;
      if ($local_type == 'Việt Nam') {
          $data_reg_domain = [
            'cmd' 				    => 'register_domain_vietnam',//Lệnh đăng ký domain
            'username'			  => $this->user_name,//Username đại lý
            'apikey'			    => $this->api_key,//API KEY
            'domainName' 	  	=> $domain['domain_name'],//Tên domain(Chỉ nhập phần tên, không nhập phần đuôi.Ví dụ: abc.vn thì chỉ nhập abc)
            'domainExt' 		  => $domain['domain_ext'],//Nhập phần đuôi của domain
            'domainYear' 		  => $domain['domain_year'],//Số năm đăng ký
            'passwordDomain' 	=> $domain['password_domain'],//Password domain phải có độ dài từ 8 đến 15 ký tự (phải bao gồm cả số và chữ)
            'for' 				    => $domain['customer_domain'],//Đăng ký cho cá nhân hay công ty(canhan/congty)
            'domainDNS1' 		  => 'ns1.pavietnam.vn',//Tên DNS Primary
            'domainDNS2' 		  => 'ns2.pavietnam.vn',//Tên DNS Secondary
            'domainDNS3' 		  => 'nsbak.pavietnam.net',//Tên DNS Secondary2

            /* THÔNG TIN KHÁCH HÀNG(Thông tin dùng để tạo mã support PA-xxx) */
            'ownerName'	  		=> $domain['owner_name'],//Tên cá nhân hoặc công ty
            'ownerID_Number'	=> $domain['ownerid_number'],//Chứng minh nhân dân: bắt buộc đối với cá nhân
            'ownerTaxCode'		=> $domain['owner_taxcode'],//Mã số thuế: bắt buộc đối với công ty
            'ownerAddress'		=> $domain['owner_address'],//Địa chỉ liên hệ
            'ownerEmail1'		  => $domain['owner_email'],//Email chính của khách hàng
            'ownerEmail2'		  => $domain['owner_email'],//Email phụ của khách hàng
            'ownerPhone'		  => $domain['owner_phone'],//Điện thoại di động
            'ownerPhone2' 		=> $domain['owner_phone'],//Điện thoại bàn
            'ownerFax'			  => '',//Fax

            /* THÔNG TIN CHỦ THỂ (Bắt buộc nhập đúng thông tin cá nhân, hoặc tổ chức)*/
            'uiName' 		  	=> $domain['ui_name'],//Họ tên cá nhân hoặc tên công ty
            'uiID_Number'		=> $domain['uiid_number'],//Số chứng minh nhân dân của chủ thể (Nếu là cá nhân)
            'uiAddress' 		=> $domain['ui_address'],//Địa chỉ chủ thể
            'uiProvince' 		=> $domain['ui_province'],//Tỉnh thành(Chú ý: Nhập đúng theo file "Tinh thanh pho Viet Nam.xls")
            'uiCountry' 		=> 'Viet Nam',//Quốc gia(Chú ý: Nhập đúng theo file "Quoc gia ISO3166.xls")
            'uiEmail' 			=> $domain['ui_email'],//Email chủ thể
            'uiPhone' 			=> $domain['ui_phone'],//Điện thoại di động chủ thể
            'uiFax' 			  => '',//Fax chủ thể
            'uiGender'			=> $domain['ui_gender'],//Giới tính: Nam/Nữ
            'uiBirthdate'		=> $domain['ui_birthdate'],//Ngày sinh (format YYYY-mm-dd)
            'uiCompany'			=> '',//Tên tổ chức của chủ thể(Nếu có)
            'uiPosition' 		=> '',//Chức vụ

            /* THÔNG TIN NGƯỜI QUẢN LÝ (Bắt buộc nhập đúng thông tin cá nhân)*/
            'adminName' 		  => $domain['admin_name'],//Họ tên người quản lý
            'adminID_Number'	=> $domain['adminid_number'],//Số chứng minh nhân dân của người quản lý
            'adminAddress' 		=> $domain['admin_address'],//Địa chỉ người quản lý
            'adminProvince' 	=> $domain['admin_province'],//Tỉnh thành (Chú ý: Nhập đúng theo file "Tinh thanh pho Viet Nam.xls")
            'adminCountry' 		=> 'Viet Nam',//Quốc gia  (Chú ý: Nhập đúng theo file "Quoc gia ISO3166.xls")
            'adminEmail' 		  => $domain['admin_email'],//Email người quản lý
            'adminPhone' 	  	=> $domain['admin_phone'],//Điện thoại người quản lý
            'adminFax' 			  => '',//Fax người quản lý
            'adminGender'		  => $domain['admin_gender'],//Giới tính: Nam/Nữ
            'adminBirthdate'	=> $domain['admin_birthdate'],//Ngày sinh (format YYYY-mm-dd)
            'adminCompany'		=> '',//Tên tổ chức (Nếu có)
            'adminPosition' 	=> '',//Chức vụ người quản lý

            'sendmail'			  => '1',//Gửi mail thông báo đến khách hàng: (1: Có gửi, 0: Không gửi)
            'responsetype'		=> 'json'//Dữ liệu trả về kiểu json, nếu để rỗng thì trả về kiểu plan text
          ];

          // dd($data_reg_domain);
          	//Mã hoá url trước khi gọi link thực thi
          $param = '';
          foreach($data_reg_domain as $k=>$v) $param .= $k.'='.urlencode($v).'&';
          $result = file_get_contents($this->api_url."?$param");//Gọi link đăng ký
          $result = json_decode($result);
          // if($result->{'ReturnCode'} == 200)//Thành công
          // {
          //   //Xử lý trường hợp đăng ký domain thành công
          //   //...
          //   //Xem toàn bộ thông tin trả về
          //   // echo "<pre>".print_r($result,true)."</pre>";
          //   return true;
          // }
          // else//Thất bại
          // {
          //   //Xử lý trường hợp đăng ký domain thất bại
          //   //...
          //   //Xem toàn bộ thông tin trả về
          //   // echo "<pre>".print_r($result,true)."</pre>";
          //   return $result;
          // }
          return $result;

      } elseif ($local_type == 'Quốc tế') {
          $data_reg_domain = [
            'cmd' 				    => 'register_domain_quocte',//Lệnh đăng ký domain
            'username'			  => $this->user_name,//Username đại lý
            'apikey'			    => $this->api_key,//API KEY
            'domainName' 		  => $domain['domain_name'],//Tên domain(Chỉ nhập phần tên, không nhập phần đuôi.Ví dụ: abc.vn thì chỉ nhập abc)
            'domainExt' 		  => $domain['domain_ext'],//Nhập phần đuôi của domain
            'domainYear' 		  => $domain['domain_year'],//Số năm đăng ký
            'passwordDomain' 	=> $domain['password_domain'],//Password domain phải có độ dài từ 8 đến 15 ký tự (phải bao gồm cả số và chữ)
            'domainDNS1' 		  => 'ns1.pavietnam.vn',//Tên DNS Primary
            'domainDNS2' 		  => 'ns2.pavietnam.vn',//Tên DNS Secondary

            /* THÔNG TIN KHÁCH HÀNG(Thông tin dùng để tạo mã support PA-xxx) */
            'ownerName'	  		=> $domain['owner_name'],//Tên cá nhân hoặc công ty
            'ownerID_Number'	=> $domain['ownerid_number'],//Chứng minh nhân dân: bắt buộc đối với cá nhân
            'ownerTaxCode'		=> $domain['owner_taxcode'],//Mã số thuế: bắt buộc đối với công ty
            'ownerAddress'		=> $domain['owner_address'],//Địa chỉ liên hệ
            'ownerEmail1'		  => $domain['owner_email'],//Email chính của khách hàng
            'ownerEmail2'		  => $domain['owner_email'],//Email phụ của khách hàng
            'ownerPhone'		  => $domain['owner_phone'],//Điện thoại di động
            'ownerPhone2' 		=> $domain['owner_phone'],//Điện thoại bàn
            'ownerFax'			  => '',//Fax

            /* THÔNG TIN CHỦ THỂ (Bắt buộc nhập đúng thông tin cá nhân, hoặc tổ chức)*/
            'uiName' 			  => $domain['ui_name'],//Họ tên cá nhân hoặc tên công ty
            'uiID_Number'		=> !empty($domain['uiid_number']) ? $domain['uiid_number'] : '',//Số chứng minh nhân dân của chủ thể (Nếu là cá nhân)
            'uiTaxCode'			=> !empty($domain['ui_taxcode']) ? $domain['ui_taxcode'] : '' ,
            'uiAddress' 		=> $domain['ui_address'],//Địa chỉ chủ thể
            'uiProvince' 		=> $domain['ui_province'],//Tỉnh thành(Chú ý: Nhập đúng theo file "Tinh thanh pho Viet Nam.xls")
            'uiCountry' 		=> 'Viet Nam',//Quốc gia(Chú ý: Nhập đúng theo file "Quoc gia ISO3166.xls")
            'uiEmail' 			=> $domain['ui_email'],//Email chủ thể
            'uiPhone' 			=> $domain['ui_phone'],//Điện thoại di động chủ thể
            'uiFax' 			  => '',//Fax chủ thể

            'sendmail'			=> '1',//Gửi mail thông báo đến khách hàng: (1: Có gửi, 0: Không gửi)
            'responsetype'		=> 'json'//Dữ liệu trả về kiểu json, nếu để rỗng thì trả về kiểu plan text
          ];
          // dd($data_reg_domain);
          //Mã hoá url trước khi gọi link thực thi
          $param = '';
          foreach($data_reg_domain as $k=>$v) $param .= $k.'='.urlencode($v).'&';
          $result = file_get_contents($this->api_url."?$param");//Gọi link đăng ký
          // dd($param);
          $result = json_decode($result);
          // if($result->{'ReturnCode'} == 200)//Thành công
          //     {
          //       //Xử lý trường hợp đăng ký domain thành công
          //       //...
          //       //Xem toàn bộ thông tin trả về
          //       // echo "<pre>".print_r($result,true)."</pre>";
          //       return true;
          //   }
          // else//Thất bại
          //     {
          //       //Xử lý trường hợp đăng ký domain thất bại
          //       //...
          //       //Xem toàn bộ thông tin trả về
          //       // echo "<pre>".print_r($result,true)."</pre>";
          //       return $result;
          //  }
          return $result;
      }

  }

  public function renewDomain($domain, $year)
    {
      $data_renew = [
        'cmd' 				    => 'renew_domain',//Lệnh gia hạn domain
        'username'			  => $this->user_name,//Username đại lý
        'apikey'			    => $this->api_key,//API KEY
        'domain' 			    => $domain['domain'],//domain cần gia hạn(Bao gồm cả phần tên domain và phần mở rộng)
        'year' 				    => $year,//Số năm gia hạn
        'sendmail'			  => '1',//Gửi mail thông báo đến khách hàng: (1: Có gửi, 2: Không gửi)
        'responsetype'		=> 'json'//Dữ liệu trả về kiểu json, nếu để rỗng thì trả về kiểu
      ];
      $param = '';
      foreach($data_renew as $k=>$v) $param .= $k.'='.urlencode($v).'&';
      $result = file_get_contents($this->api_url."?$param");//Gọi link đăng ký
      $result = json_decode($result);
      return $result;
    }

    public function change_pass_domain($domain, $pass)
    {
      $data_change_pass = [
        'cmd' 				    => 'change_password_domain',//Lệnh thay đổi mật khẩu domain
        'username'		    =>  $this->user_name,//Username đại lý
        'apikey'		     	=>  $this->api_key,//API KEY
        'domain' 			    =>  $domain,//domain cần thay đổi password(Bao gồm cả phần tên domain và phần mở rộng)
        'passwordDomain'	=>  $pass,//Độ dài password từ 8 đến 15 ký tự (phải bao gồm cả số và chữ)
        'responsetype'		=> 'json'//Dữ liệu trả về kiểu json, nếu để rỗng thì trả về kiểu plan text
      ];
      $param = '';
      foreach($data_change_pass as $k=>$v) $param .= $k.'='.urlencode($v).'&';
      $result = file_get_contents($this->api_url."?$param");//Gọi link đăng ký
      $result = json_decode($result);
      return $result;
    }

    public function check_account_still()
    {
      $data_check = [
        'cmd' 			    => 'check_account_still',//(check_account_still: Xem tài khoản còn lại, check_account_total: Kiểm tra tổng nạp)
        'username'	  	=> $this->user_name,//Username đại lý
        'apikey'		    => $this->api_key,//API KEY
        'responsetype'	=> 'json'//Dữ liệu trả về kiểu json, nếu để rỗng thì trả về kiểu plan text
      ];
      $param = '';
      foreach($data_check as $k=>$v) $param .= $k.'='.urlencode($v).'&';
      $result = file_get_contents($this->api_url."?$param");//Gọi link đăng ký
      $result = json_decode($result);
      return $result;
    }

  // TODO: Lấy thông tin Domain
  public function get_info_domain($q)
  {
      $data = [
        "cmd" => "get_info_domain",
        "username" => "daivietso",
        "apikey" => "cbdaffb1b3a77091faccc4e140e54a00",
        "domain" => $q,
        "responsetype" => "json",
      ];
      try {
          $client = new Client([
             'headers' => [
                'Content-Type' => 'application/json',
              ],
          ]);
          $response = $client->get(
                $this->api_url ,
                [
                  'query' => $data
                ]
          );
          if ($response->getStatusCode() == 200) {
              $response = json_decode($response->getBody()->getContents());
              if ( $response->{'ReturnCode'} == 200 ) {
                return $response->{'ReturnText'};
              }
          }
      } catch (\Exception $e) {
          report($e);
          // dd('false');
          return false;
      }

  }

}
