<?php
namespace App\Services;


class ClassMoMo{
    private $config;
    public function __construct($phone, $password, $otp, $rkey, $setupKeyEncrypted, $imei, $token, $onesignalToken){
        $ohash = hash('sha256', $phone . $rkey . $otp);
        $this->config = [
            'phone'                 => $phone,
            'otp'                   => $otp,
            'password'              => $password,
            'rkey'                  => $rkey,
            'setupKeyEncrypted'     => $setupKeyEncrypted,
            'imei'                  => $imei,
            'token'                 => $token,
            'onesignalToken'        => $onesignalToken,
            'aaid'                  => '',
            'idfa'                  => '',
            'csp'                   => 'Mobifone',
            'icc'                   => '',
            'mcc'                   => '452',
            'mnc'                   => '01',
            'cname'                 => 'Vietnam',
            'ccode'                 => '084',
            'channel'               => 'APP',
            'lang'                  => 'vi',
            'device'                => 'iPhone',
            'firmware'              => '13.5.1',
            'manufacture'           => 'Apple',
            'hardware'              => 'iPhone',
            'simulator'             => false,
            'appVer'                => '21462',
            'appCode'               => "2.1.46",
            'deviceOS'              => "IOS",
            'setupKeyDecrypted'     => $this->encryptDecrypt($setupKeyEncrypted, $ohash, 'DECRYPT')
        ];
    }

    private function encryptDecrypt($data, $key, $mode = 'ENCRYPT'){
        if (strlen($key) < 32) {
            $key = str_pad($key, 32, 'x');
        }
        $key = substr($key, 0, 32);
        $iv = pack('C*', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        if ($mode === 'ENCRYPT') {
            return base64_encode(openssl_encrypt($data, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv));
        }
        else {
            return openssl_decrypt(base64_decode($data), 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);
        }
    }

    private function get_microtime(){
        return floor(microtime(true) * 1000);
    }

    private function get_checksum($type){
        $config         = $this->config;
        $checkSumSyntax = $config['phone'] . $this->get_microtime() . '000000' . $type . ($this->get_microtime() / 1000000000000.0) . 'E12';
        return $this->encryptDecrypt($checkSumSyntax, $config['setupKeyDecrypted']);
    }

    private function get_pHash(){
        $config         = $this->config;
        $pHashSyntax    = $config['imei'] . '|' . $config['password'];
        return $this->encryptDecrypt($pHashSyntax, $config['setupKeyDecrypted']);
    }

    public function get_auth(){
        $config         = $this->config;
        $type           = 'USER_LOGIN_MSG';
        $data_body = [
            'user'      => $config['phone'],
            'pass'      => $config['password'],
            'msgType'   => $type,
            'cmdId'     => $this->get_microtime() . '000000',
            'lang'      => $config['lang'],
            'channel'   => $config['channel'],
            'time'      => $this->get_microtime(),
            'appVer'    => $config['appVer'],
            'appCode'   => $config['appCode'],
            'deviceOS'  => $config['deviceOS'],
            'result'    => true,
            'errorCode' => 0,
            'errorDesc' => '',
            'extra'     => [
                'checkSum'          => $this->get_checksum($type),
                'pHash'             => $this->get_pHash(),
                'AAID'              => $config['aaid'],
                'IDFA'              => $config['idfa'],
                'TOKEN'             => $config['token'],
                'ONESIGNAL_TOKEN'   => $config['onesignalToken'],
                'SIMULATOR'         => $config['simulator']
            ],
            'momoMsg'   => [
                '_class'    => 'mservice.backend.entity.msg.LoginMsg'
                , 'isSetup' => true
            ]
        ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL             => "https://owa.momo.vn/public",
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => "",
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 30,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => "POST",
            CURLOPT_POSTFIELDS      => json_encode($data_body),
            CURLOPT_HTTPHEADER      => array(
                'User-Agent'    => "MoMoApp-Release/%s CFNetwork/978.0.7 Darwin/18.6.0",
                'Msgtype'       => "USER_LOGIN_MSG",
                'Accept'        => 'application/json',
                'Content-Type'  => 'application/json',
                'Userhash'      => md5($config['phone'])  ,
            )
        ));
        $response = curl_exec($curl);
        if(!$response){
            return false;
        }
        if ( !empty(json_decode($response)->extra->AUTH_TOKEN) ) {
          return json_decode($response)->extra->AUTH_TOKEN;
        }
        return false;
    }

    public function history($day = 1){
        $config = $this->config;
        $type   = 'QUERY_TRAN_HIS_MSG';
        $data_post =  [
            'user'      => $config['phone'],
            'msgType'   => $type,
            'cmdId'     => $this->get_microtime() . '000000',
            'lang'      => $config['lang'],
            'channel'   => $config['channel'],
            'time'      => $this->get_microtime(),
            'appVer'    => $config['appVer'],
            'appCode'   => $config['appCode'],
            'deviceOS'  => $config['deviceOS'],
            'result'    => true,
            'errorCode' => 0,
            'errorDesc' => '',
            'extra'     => [
                'checkSum' => $this->get_checksum($type)
            ],
            'momoMsg'   => [
                '_class'    => 'mservice.backend.entity.msg.QueryTranhisMsg',
                'begin'     => (time() - (86400 * $day)) * 1000,
                'end'       => $this->get_microtime()
            ]
        ];
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL             => "https://owa.momo.vn/api/sync/$type",
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => "",
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 30,
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST   => "POST",
            CURLOPT_POSTFIELDS      => json_encode($data_post),
            CURLOPT_HTTPHEADER      => array(
                'User-Agent'    => "MoMoApp-Release/%s CFNetwork/978.0.7 Darwin/18.6.0",
                'Msgtype'       => $type,
                'Userhash'      => md5($config['phone']),
                'Accept'        => 'application/json',
                'Content-Type'  => 'application/json',
                'Authorization: Bearer ' . trim($this->get_auth()),
            )
        ));
        $result = curl_exec($ch);
        if(!$result){
            return false;
        }
        return $result;
    }

    public function Quang_CreatTransfersOutMomo($amount, $comment,$dataBank) {
        $config = $this->config;
        $type   = 'TRAN_HIS_INIT_MSG';
        $data_body = [
            'user' => $config['phone'],
            'msgType' => $type,
            'cmdId'     => $this->get_microtime() . '000000',
            'lang'      => $config['lang'],
            'channel'   => $config['channel'],
            'time'      => $this->get_microtime(),
            'appVer'    => $config['appVer'],
            'appCode'   => $config['appCode'],
            'deviceOS'  => $config['deviceOS'],
            'result' => true,
            'errorCode' => 0,
            'errorDesc' => '',
            'extra' => [
                'checkSum' => $this->get_checksum($type)
            ],
            'momoMsg' => [
                'user' => $config['phone'],
                'clientTime' => $this->get_microtime(),
                'tranType' => 8,
                'comment' => $comment,
                'amount' => $amount,
                'moneySource' => 1,
                'partnerCode' => 'momo',
                'partnerId' => $dataBank['bankCode'],
                'partnerName' => $dataBank['bankName'],
                'serviceId' => "transfer_p2b",
                'rowCardNum' => $dataBank['rowCardNum'],
                'rowCardId' => null,
                'ownerName' => "LE THANH TRUNG",
                'partnerRef' => $config['phone'],
                'extras' => json_encode(array(
                    "saveCard" => false,
                    "vpc_CardType" => "SML",
                    "vpc_TicketNo" => "210.245.32.243")),
                '_class' => 'mservice.backend.entity.msg.TranHisMsg',
                'giftId' => "",
            ],
        ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://owa.momo.vn/api/TRAN_HIS_INIT_MSG/8/transfer_p2b",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data_body),
            CURLOPT_HTTPHEADER => array(
                'User-Agent' => "MoMoApp-Release/%s CFNetwork/978.0.7 Darwin/18.6.0",
                'Msgtype' => "TRAN_HIS_INIT_MSG",
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Userhash' => md5($config['phone']),
                'Authorization: Bearer ' . trim($this->get_auth()),
            ),
        ));
        $response = curl_exec($curl);
        if (!$response) {
            return false;
        }
        return $response;
    }

    public function Quang_ComfirmTransfersOutMomo($id, $amount, $fee, $comment, $extras,$dataBank) {
        $config = $this->config;
        $type   = 'TRAN_HIS_CONFIRM_MSG';
        $data_body = [
            'user' => $config['phone'],
            'msgType' => $type,
            'cmdId'     => $this->get_microtime() . '000000',
            'lang'      => $config['lang'],
            'channel'   => $config['channel'],
            'time'      => $this->get_microtime(),
            'appVer'    => $config['appVer'],
            'appCode'   => $config['appCode'],
            'deviceOS'  => $config['deviceOS'],
            'result' => true,
            'errorCode' => 0,
            'errorDesc' => '',
            'extra' => [
                'checkSum' => $this->get_checksum($type),
                "cvn" => "",
            ],
            'momoMsg' => [
                "ID" => $id,
                'user' => $config['phone'],
                "commandInd" => $this->get_microtime() . '000000',
                'tranId' => $this->get_microtime(),
                'clientTime' => $this->get_microtime(),
                'ackTime' => $this->get_microtime(),
                'tranType' => 8,
                'io' => -1,
                'partnerId' => $dataBank['bankCode'],
                'partnerCode' => 'momo',
                'partnerName' => $dataBank['bankName'],
                'partnerRef' => $config['phone'],
                'amount' => intval($amount),
                'comment' => $comment,
                'status' => 4,
                'ownerNumber' => $config['phone'],
                'ownerName' => "LE THANH TRUNG",
                'moneySource' => 1,
                'desc' => "Thành Công",
                'fee' => $fee,
                'originalAmount' => $amount - $fee,
                'serviceId' => "transfer_p2b",
                "quantity" => 1,
                "lastUpdate" => $this->get_microtime(),
                "share" => "0.0",
                "receiverType" => 2,
                'extras' => json_encode($extras),
                "rowCardNum" => $dataBank['rowCardNum'],
                '_class' => 'mservice.backend.entity.msg.TranHisMsg',
                "channel" => "END_USER",
                "otpType" => "NA",
            ],
            "pass" => $config['password'],
        ];

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://owa.momo.vn/api/TRAN_HIS_CONFIRM_MSG/8/transfer_p2b",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($data_body),
            CURLOPT_HTTPHEADER => array(
                'User-Agent' => "MoMoApp-Release/%s CFNetwork/978.0.7 Darwin/18.6.0",
                'Msgtype' => "TRAN_HIS_CONFIRM_MSG",
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Userhash' => md5($config['phone']),
                'Authorization: Bearer ' . trim($this->get_auth()),
            ),
        ));
        $response = curl_exec($curl);
        if (!$response) {
            return false;
        }
        return $response;
    }

    // public function Quang_TransfersOutMomo($from_phone, $id_bank, $accId, $partnerRef, $amount, $comment, $data_user = false) {
    //     global $db, $quang;
    //     $data = array();
    //     if (empty($data_user)) {
    //         $data_user = $quang['user'];
    //     }
    //     if (!Quang_IsAdmin($data_user['user_id'])) {
    //         $db->where("user_id", $data_user['user_id']);
    //         $db->where("transfers", 1);
    //     }
    //     $data_momo = $db->where("phone", $from_phone)->where("status", 1)->getOne(T_MOMO);
    //     if ($data_momo) {
    //         $bank = json_decode($quang['config']['bank_data_momo'], true)[$id_bank];
    //         $data_checkbank = json_decode(Quang_CheckBank($data_momo, $bank, $accId), true);
    //         if ($data_checkbank['resultCode'] === 0 && !empty($data_checkbank['benfAccount']['accName'])) {
    //             $data_creat = json_decode(Quang_CreatTransfersOutMomo($data_momo, $bank, $accId, $data_checkbank['benfAccount']['accName'], $amount, $partnerRef, $comment), true);
    //             if ($data_creat['result'] == true) {
    //                 $data_comfirm = json_decode(Quang_ComfirmTransfersOutMomo($data_momo, $data_creat['momoMsg']['ID'], $bank, $accId, $data_checkbank['benfAccount']['accName'], $data_creat['momoMsg']['tranHisMsg']['amount'], $data_creat['momoMsg']['tranHisMsg']['fee'], $partnerRef, $comment, $data_creat['momoMsg']['tranHisMsg']['extras']), true);
    //                 if ($data_comfirm['result'] == true) {
    //                     $data_insert = array(
    //                         "user_id" => $data_user['user_id'],
    //                         "from_phone" => $data_momo->phone,
    //                         "to_phone" => "{$id_bank} - {$accId}",
    //                         "balance" => $amount,
    //                         "comment" => $comment,
    //                         "type" => "outmomo",
    //                         "status" => 1,
    //                         "log_request" => json_encode($data_comfirm),
    //                         "time" => time(),
    //                     );
    //                     $db->insert(T_TRANSFERS_LOG, $data_insert);
    //                     $data['status'] = 200;
    //                     $data['message'] = "Chuyển tiền thành công. Mã giao dịch momo: " . $data_comfirm['momoMsg']['transId'];
    //                 } else {
    //                     $data['message'] = $data_comfirm['errorDesc'];
    //                 }
    //             } else {
    //                 $data['status'] = 102;
    //                 $data['message'] = !empty($data_creat['errorDesc']) ? $data_creat['errorDesc'] : "Có lỗi xảy ra khi tạo yêu cầu chuyển tiền vui lòng thử lại";
    //             }
    //         } else {
    //             $data['status'] = $data_checkbank['resultCode'];
    //             $data['message'] = !empty($data_checkbank['description']) ? $data_checkbank['description'] : "Có lỗi xảy ra khi lấy thông tin người nhận vui lòng thử lại";
    //         }
    //     } else {
    //         $data['status'] = 414;
    //         $data['message'] = "Tài khoản nguồn không khả dụng. [FROM_PHONE_NOT_AVAILABLE]";
    //     }
    //     return $data;
    // }

}
