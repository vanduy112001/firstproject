<?php

namespace App\Services;

use App\Factories\UserFactories;
use GuzzleHttp\Client;
use http\Env\Request;

class Momo
{
    protected $pay_in;
    public function __construct()
    {
        $this->pay_in = UserFactories::payInRepositories();
    }


    public function payment($pay_in, $amount) {
       try {
           $config = config('momo');
           $url = $config['url'];
           $secret_key = $config['secret_key'];

           $pagram = [
               "partnerCode" => $config['partner_code'],
               'accessKey' => $config['access_key'],
               'requestId' => $pay_in->ma_gd,
            //    'requestId' => time(),
               'amount' => $amount,
               'orderId' => $pay_in->ma_gd,
            //    'orderId' => time(),
               'orderInfo' => 'Nạp tiền vào tài khoản',
               'returnUrl' => route('user.momo.return'),
               'notifyUrl' => route('user.momo.notify'),
               'extraData' => 'merchantName=Cloudzone',
           ];

           $md5HashData = '';
           $signature = '';
           foreach ($pagram as $key => $item) {
               $md5HashData .= $key . '=' . $item . '&';
           }
           $md5HashData = rtrim($md5HashData,'&');
           if (strlen($secret_key) > 0 ) {
               $signature = hash_hmac('SHA256' , $md5HashData, $secret_key);
           }
           $pagram['signature'] = $signature;
           $pagram['requestType'] = 'captureMoMoWallet';

           $client = new Client([
              'headers' => ['Content-Type' => 'application/json'],
           ]);

           $response = $client->post(
               $url,
               [
                   'body' => json_encode($pagram),
               ]
           );
           $result  = json_decode($response->getBody());
           // dd($pagram,$result);
           if ($result->errorCode == 0) {
               $payUrl = $result->payUrl;
               return $payUrl;
           }


       } catch (\Exception $exception) {
       }
       return false;
    }

    public function payment_return($data)
    {
        $config = config('momo');
        $status_config = config('momo_status');
        $payment_result = [
            'status' => false,
            'message' => 'ERROR: An error, please contact admin.',
        ];
        if (!empty($data['requestId'])) {
            $secret_key = $config['secret_key'];
            $error_code = isset($data['errorCode']) ? $data['errorCode']: '';
            $pay_in_msgd = $data['orderId'];
            if (strlen($secret_key) > 0) {
                $md5HashData = '';
                $signature = '';
                $pagram_check = $data;
                unset($pagram_check['signature']);
                foreach ($pagram_check as $key => $value) {
                    $md5HashData .= $key . '=' . $value . '&';
                }
                $md5HashData = rtrim($md5HashData, '&');
                $signature = hash_hmac('SHA256', $md5HashData, $secret_key);
                if ($signature === $data['signature']) {
                    if ($error_code == 0) {
                        $update = $this->pay_in->update_history_pay($pay_in_msgd,$data);
                        if ($update) {
                            $payment_result = [
                                'status' => true,
                                'message' => 'Giao dịch thành công. Quý khách vui lòng kiểm tra lại tài khoản của mình.',
                            ];
                        }
                    } else {
                        $update = $this->pay_in->update_false_history_pay($pay_in_msgd,$data);
                        $payment_result = [
                            'status' => false,
                            'message' => array_key_exists($error_code, $status_config) ? $status_config[$error_code] : 'Giao dịch thất bại - Failured',
                        ];
                    }
                }
            }

        }
        return $payment_result;
    }

    public function payment_momo($invoice_id, $pay_in) {
        try {
            $config = config('momo');
            $url = $config['url'];
            $secret_key = $config['secret_key'];

            $pagram = [
                "partnerCode" => $config['partner_code'],
                'accessKey' => $config['access_key'],
                'requestId' => $pay_in->ma_gd,
             //    'requestId' => time(),
                'amount' => $pay_in->money,
                'orderId' => $pay_in->ma_gd,
             //    'orderId' => time(),
                'orderInfo' => 'Thanh toán dịch vụ / sản phẩm',
                'returnUrl' => route('user.momo.return'),
                'notifyUrl' => route('user.momo.notify'),
                'extraData' => 'merchantName=Cloudzone',
            ];
            // dd($pagram);
            $md5HashData = '';
            $signature = '';
            foreach ($pagram as $key => $item) {
                $md5HashData .= $key . '=' . $item . '&';
            }
            $md5HashData = rtrim($md5HashData,'&');
            if (strlen($secret_key) > 0 ) {
                $signature = hash_hmac('SHA256' , $md5HashData, $secret_key);
            }
            $pagram['signature'] = $signature;
            $pagram['requestType'] = 'captureMoMoWallet';
            // dd($pagram);
            $client = new Client([
               'headers' => ['Content-Type' => 'application/json'],
            ]);

            $response = $client->post(
                $url,
                [
                    'body' => json_encode($pagram),
                ]
            );
            $result  = json_decode($response->getBody());
            // dd($result);
            if ($result->errorCode == 0) {
                $payUrl = $result->payUrl;
                return $payUrl;
            }


        } catch (\Exception $exception) {
        }
        return false;
    }


}
