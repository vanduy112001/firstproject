<?php
namespace App\Services;

use App\Factories\AdminFactories;
use GuzzleHttp\Client;
use http\Env\Request;
use Mail;
use App\Model\Vps;
use App\Model\SendMail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
/**
 *
 */
class DashBoard
{
  protected $dashboard;
  protected $token;
  protected $url_create_vps;
  protected $url_create_customer;
  protected $vps;
  protected $send_mail;

  public function  __construct()
  {
      $config_dashboard = config('dashboard');
      $this->token = $config_dashboard['Authorization'];
      $this->url_create_vps = $config_dashboard['url_create_vps'];
      $this->url_create_vps_us = $config_dashboard['url_create_vps_us'];
      $this->url_create_customer = $config_dashboard['url_create_customer'];
      $this->url_status = $config_dashboard['url_status'];
      $this->url_proxy = $config_dashboard['url_proxy'];
      $this->vps = new Vps;
      $this->send_mail = new SendMail;
  }
//   Tạo VPS
  public function createVPS($data, $vps)
  {
      $data_json = json_encode($data);
      // dd($data_json);
      $client = new Client([
         'headers' => [
            'Authorization' => $this->token, //$this->token ('Token 25f3362d23d770184c811dcd3a72ba2781f6d170' -- token local)
            'Content-Type' => 'application/json',
          ],
      ]);
      // dd($client , $this->url_create_vps ,  $data_json);

      try {
          $response = $client->post(
                $this->url_create_vps , //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                [
                    'body' => $data_json,
                ]
          );
          // dd($response);
          if ($response->getStatusCode() == 200) {
              $response = json_decode($response->getBody()->getContents());
              return $response;
          }
          elseif ($response->getStatusCode() == 201) {
              $response = json_decode($response->getBody()->getContents());
              return $response;
          }
      } catch (\Exception $e) {
          // dd($response);
          // dd('da den loi');
          report($e);
          try {
            Log::info($data_json);
            $data['vps'] = $vps;
            $data['subject'] = 'Lỗi khi tạo vps';
            $data_tb_send_mail = [
                'type' => 'mail_create_error_vps',
                'type_service' => 'vps',
                'user_id' => $vps->user_id,
                'status' => false,
                'content' => serialize($data),
            ];
            $this->send_mail->create($data_tb_send_mail);
            return false;
          } catch (\Exception $e) {
              report($e);
              return false;
          }

      }

  }
  //   Tạo VpsUs
  public function createVpsUs($data, $vps)
  {
      $data_json = json_encode($data);
      $dataResponse['error'] = 2;
    //   dd($data_json);
      $client = new Client([
         'headers' => [
            'Authorization' => $this->token, //$this->token ('Token 25f3362d23d770184c811dcd3a72ba2781f6d170' -- token local)
            'Content-Type' => 'application/json',
          ],
      ]);
      // dd($client , $this->url_create_vps ,  $data_json);

      try {
          $response = $client->post(
                $this->url_create_vps_us , //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                [
                    'body' => $data_json,
                ]
          );
          // dd($response);
          if ($response->getStatusCode() == 200) {
              $dataResponse['error'] = 1;
              $dataResponse['content'] = json_decode($response->getBody()->getContents());
              return $dataResponse;
          }
          elseif ($response->getStatusCode() == 201) {
              $dataResponse['error'] = 0;
              $dataResponse['content'] = json_decode($response->getBody()->getContents());
              return $dataResponse;
          }
      } catch (\Exception $e) {
          // dd($response);
          // dd('da den loi');
          report($e);
          $dataResponse['error'] = 2;
          try {
                Log::info($data_json);
                $data['vps'] = $vps;
                $data['subject'] = 'Lỗi khi tạo vps us';
                $data_tb_send_mail = [
                    'type' => 'mail_create_error_vps_us',
                    'type_service' => 'vps_us',
                    'user_id' => $vps->user_id,
                    'status' => false,
                    'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);
              return $dataResponse;
          } catch (\Exception $e) {
              report($e);
              return $dataResponse;
          }

      }

  }
// Gữi user qua bên dashboard và lấy vm_id lưu vào user
  public function sendUserDashBoard($data_create)
  {
        try {
            $data_json = json_encode($data_create);
            $client = new Client([
                'headers' => [
                'Authorization' => $this->token, //$this->token
                'Content-Type' => 'application/json',
                ],
            ]);
            $response = $client->post(
                $this->url_create_customer, //$this->url_create ('http://192.168.1.111:8000/api/customers/' - url local)
                [
                    'body' => $data_json,
                ]
            );
            $response = json_decode($response->getBody()->getContents());
            return $response;
        } catch (\Exception $e) {
            try {
                $mail = Mail::send('users.mails.error_create_user', $data_create, function($message){
                    $message->from('support@cloudzone.vn', 'Cloudzone');
                    $message->to('uthienth92@gmail.com')->subject('Lỗi khi tạo user');
                    $message->cc('support@cloudzone.vn')->subject('Lỗi khi tạo user');
                });
                return false;
            } catch (\Exception $e) {
                report($e);
                return false;
            }

        }

  }
  //Get User Dashboard
  public function getUser()
  {
        $client = new Client([
            'headers' => [
            'Authorization' => 'Token 25f3362d23d770184c811dcd3a72ba2781f6d170', //$this->token
            'Content-Type' => 'application/json',
            ],
        ]);
        $response = $client->get('http://192.168.1.111:8000/api/customers/');
        $response = json_decode($response->getBody()->getContents());
        dd($response);
        return $response;
  }
  // Bật VPS
    public function onVPS($vps)
    {
        // return true;
            if ($vps->status_vps == 'rebuild') {
                $data_action['error'] = 4;
                return $data_action;
            }
            if (empty($vps->vm_id)) {
                    $data_action['error'] = 1;
                    return $data_action;
            }
            $data = [
                "mob" => "on",
                "name" => $vps->vm_id,
                'type' => $vps->type_vps == 'vps' ? 0 : 5,
            ];
            // dd($data);
            $data_json = json_encode($data);
            $client = new Client([
                'headers' => [
                'Authorization' => $this->token, //$this->token
                'Content-Type' => 'application/json',
                ],
            ]);
            try {
              $response = $client->put(
                  $this->url_status .  $vps->vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                  [
                      'body' => $data_json,
                  ]
              );
              if ($response->getStatusCode() == 200) {
                  $data_action['success'] = true;
                  return $data_action['success'];
              } else {
                  $data_action['error'] = 400;
                  return $data_action;
              }
            } catch (\Exception $e) {
              Log::info($data);
              report($e);
              return false;
            }

    }

  // Tắt VPS
    public function offVPS($vps)
    {
        // return true;
        if ($vps->status_vps == 'off') {
            $data_action['error'] = 3;
            return $data_action;
        }
        if ($vps->status_vps == 'rebuild') {
            $data_action['error'] = 4;
            return $data_action;
        }
        if (empty($vps->vm_id)) {
                $data_action['error'] = 1;
                return $data_action;
        }
        $data = [
            "mob" => "off",
            "name" => $vps->vm_id,
            'type' => $vps->type_vps == 'vps' ? 0 : 5,
        ];
        // dd($data);
        $data_json = json_encode($data);
        try {
            $client = new Client([
                'headers' => [
                'Authorization' => $this->token, //$this->token
                'Content-Type' => 'application/json',
                ],
            ]);
            $response = $client->put(
                $this->url_status .  $vps->vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                [
                    'body' => $data_json,
                ]
            );
            if ($response->getStatusCode() == 200) {
                $data_action['success'] = true;
                return $data_action['success'];
            } else {
                $data_action['error'] = 400;
                return $data_action;
            }
        } catch (Exception $e) {
            report($e);
            Log::info($data_json);
            return false;
        }
    }
    // Bật VPS US
    public function onVPSUS($vps)
    {
        // return true;
        if (empty($vps->vm_id)) {
            $data_action['error'] = 1;
            return $data_action;
        }
        if ($vps->status_vps == 'rebuild') {
            $data_action['error'] = 4;
            return $data_action;
        }
      $data = [
        "status" => "reset",
      ];
      // dd($data);
      $data_json = json_encode($data);
      try {
          $client = new Client([
            'headers' => [
              'Authorization' => $this->token, //$this->token
              'Content-Type' => 'application/json',
            ],
          ]);
          $url_vps_us = 'https://dashboard.cloudzone.vn/api/usrs/';
          $response = $client->put(
            $url_vps_us .  $vps->vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
            [
              'body' => $data_json,
            ]
          );
          if ($response->getStatusCode() == 200) {
            $data_action['success'] = true;
            return $data_action['success'];
          } else {
            $data_action['error'] = 400;
            return $data_action;
          }
      } catch (Exception $e) {
          report($e);
          Log::info($data_json);
          return false;
      }
    }

    // Tắt VPS
    public function offVPSUS($vps)
    {
      // return true;
      if ($vps->status_vps == 'off') {
        $data_action['error'] = 3;
        return $data_action;
      }
      if (empty($vps->vm_id)) {
        $data_action['error'] = 1;
        return $data_action;
      }
        if ($vps->status_vps == 'rebuild') {
            $data_action['error'] = 4;
            return $data_action;
        }
      $data = [
        "status" => "off",
      ];
      // dd($data);
      $data_json = json_encode($data);
      try {
          $client = new Client([
            'headers' => [
                'Authorization' => $this->token, //$this->token
                'Content-Type' => 'application/json',
            ],
          ]);
          $url_vps_us = 'https://dashboard.cloudzone.vn/api/usrs/';
          $response = $client->put(
            $url_vps_us .  $vps->vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
            [
              'body' => $data_json,
            ]
          );
          if ($response->getStatusCode() == 200) {
            $data_action['success'] = true;
            return $data_action['success'];
          } else {
            $data_action['error'] = 400;
            return $data_action;
          }
      } catch (Exception $e) {
          report($e);
          Log::info($data_json);
          return false;
      }
    }
  // Xóa VPS
  public function deleteVPS($vps)
  {
      return true;
  }

  // Khởi động lại VPS
    public function restartVPS($vps)
    {
      // Log::info('da den');
        if (empty($vps->vm_id)) {
            $data_action['error'] = 1;
            return $data_action;
        }
        $data = [
            "mob" => "reset",
            "name" => $vps->vm_id,
            'type' => $vps->type_vps == 'vps' ? 0 : 5,
        ];
        // dd($data);
        $data_json = json_encode($data);
        try {

            $client = new Client([
                'headers' => [
                'Authorization' => $this->token, //$this->token
                'Content-Type' => 'application/json',
                ],
            ]);
            $response = $client->put(
                $this->url_status .  $vps->vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                [
                    'body' => $data_json,
                ]
            );
            if ($response->getStatusCode() == 200) {
                $data_action['success'] = true;
                return $data_action['success'];
            } else {
                $data_action['error'] = 400;
                return $data_action;
            }
        } catch (Exception $e) {
              report($e);
              Log::info($data_json);
              return false;
        }
    }

    // gia han vps
    public function expired_vps($data_expired, $vm_id, $vps)
    {
        try {
            $data_json = json_encode($data_expired);
            // dd($data_json);
            $client = new Client([
                'headers' => [
                'Authorization' => $this->token, //$this->token
                'Content-Type' => 'application/json',
                ],
            ]);
            $url_expired = "https://dashboard.cloudzone.vn/api/vms_multi/";
            $response = $client->put(
                $url_expired, //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                [
                    'body' => $data_json,
                ]
            );
            if ($response->getStatusCode() == 200) {
                $response = json_decode($response->getBody()->getContents());
                return $response;
            }
        } catch (\Exception $e) {
            report($e);
            try {
                Log::info($data_expired);
                $str = "";
                foreach ($data_expired as $key => $expired) {
                  $vps_error = $this->vps->where('vm_id', $expired['vm_id'])->first();
                  $str .= "VPS số " . $key . "<br>";
                  $str .= " - VM_ID: " . $expired['vm_id'] . " <br>";
                  $str .= " - IP: " . $vps_error->ip . " <br> ";
                  $str .= "<br>";
                }
                // dd($str);
                $data_mail['error_vps'] = $str;
                $data_mail['vps'] = $vps;
                $data_mail['subject'] = 'Lỗi khi gia hạn vps';
                $data_tb_send_mail = [
                    'type' => 'mail_expire_error_vps',
                    'type_service' => 'vps_us',
                    'user_id' => $vps->user_id,
                    'status' => false,
                    'content' => serialize($data_mail),
                ];
                $this->send_mail->create($data_tb_send_mail);
                return false;
            } catch (\Exception $e) {
                report($e);
                return false;
            }

        }

    }

    // gia han vps us
    public function expired_vps_us($data_expired)
    {
        try {
            // return true;
            $data_json = json_encode($data_expired);
            // dd($data_json);
            $client = new Client([
                'headers' => [
                'Authorization' => $this->token, //$this->token
                'Content-Type' => 'application/json',
                ],
            ]);
            $url_vps_us = 'https://dashboard.cloudzone.vn/api/usrs_multi/';
            $response = $client->put(
                $url_vps_us, //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                [
                    'body' => $data_json,
                ]
            );
            if ($response->getStatusCode() == 200) {
                $response = json_decode($response->getBody()->getContents());
                return $response;
            }
        } catch (\Exception $e) {
            report($e);
            Log::info($data_expired);
            return false;

        }

    }

    // nâng cấp cấu hình VPS
    public function upgrade_vps($data_addon, $vm_id, $vps)
    {
        try {
            $data_json = json_encode($data_addon);
            // dd($data_json);
            $client = new Client([
                'headers' => [
                'Authorization' => $this->token, //$this->token
                'Content-Type' => 'application/json',
                ],
            ]);
            $response = $client->put(
                $this->url_status .  $vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                [
                    'body' => $data_json,
                ]
            );
            if ($response->getStatusCode() == 200) {
                $data_action['success'] = true;
                return $data_action['success'];
            } else {
                $data_action['error'] = 400;
                return $data_action;
            }
        } catch (\Exception $e) {
            report($e);
            try {
                Log::info($data_addon);
                $data_addon['vm_id'] = $vm_id;
                $data_addon['subject'] = 'Lỗi khi cập nhật cấu hình vps';
                $data_tb_send_mail = [
                    'type' => 'mail_upgrade_error_vps',
                    'type_service' => 'vps_us',
                    'user_id' => $vps->user_id,
                    'status' => false,
                    'content' => serialize($data_addon),
                ];
                $this->send_mail->create($data_tb_send_mail);
                return false;
            } catch (\Exception $e) {
                report($e);
                return false;
            }

        }

    }

    // nâng cấp cấu hình VPS
    public function rebuild_vps($data_rebuild, $vm_id)
    {
        try {
            $data_json = json_encode($data_rebuild);
            // dd($data_json);
            $client = new Client([
                'headers' => [
                'Authorization' => $this->token, //$this->token
                'Content-Type' => 'application/json',
                ],
            ]);
            $response = $client->put(
                $this->url_status .  $vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                [
                    'body' => $data_json,
                ]
            );
            if ($response->getStatusCode() == 200) {
                return true;
            } elseif ($response->getStatusCode() == 201) {
                return true;
            }
        } catch (\Exception $e) {
            report($e);
            Log::info($data_rebuild);
            return false;

        }

    }


    // rebuild_vps_us
    public function rebuild_vps_us($vps)
    {
      // return true;
      if (empty($vps->vm_id)) {
        $data_action['error'] = 1;
        return $data_action;
      }
        if ($vps->status_vps == 'rebuild') {
            $data_action['error'] = 4;
            return $data_action;
        }
      $data = [
        "status" => "rebuild",
      ];
      // dd($data);
      $data_json = json_encode($data);
      try {
          $client = new Client([
            'headers' => [
                'Authorization' => $this->token, //$this->token
                'Content-Type' => 'application/json',
            ],
          ]);
          $url_vps_us = 'https://dashboard.cloudzone.vn/api/usrs/';
          $response = $client->put(
            $url_vps_us .  $vps->vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
            [
              'body' => $data_json,
            ]
          );
          if ($response->getStatusCode() == 200) {
            return true;
          }
          return false;
      } catch (Exception $e) {
            report($e);
            $data_rebuild = [
              "mob" => "rebuild",
              "name" => $vps->vm_id,
              "template" => 2,
              'type' => 0,
            ];
            try {
                Log::info($data_rebuild);
                $data_rebuild['vm_id'] = $vm_id;
                $data_rebuild['subject'] = 'Lỗi khi rebuild vps';
                $data_tb_send_mail = [
                    'type' => 'mail_rebuild_error_vps',
                    'type_service' => 'vps_us',
                    'user_id' => $vps->user_id,
                    'status' => false,
                    'content' => serialize($data_rebuild),
                ];
                $this->send_mail->create($data_tb_send_mail);
                return false;
            } catch (\Exception $e) {
                report($e);
                return false;
            }
      }
    }

    // reset_password
    public function reset_password($vps)
    {
        // return true;
        if (empty($vps->vm_id)) {
            $data_action['error'] = 1;
            return $data_action;
        }
        if ( $vps->vm_id < 1000000000 ) {
            $data = [
                "mob" => "resetpass",
            ];
            // dd($data);
            $data_json = json_encode($data);
            try {
                $client = new Client([
                    'headers' => [
                        'Authorization' => $this->token, //$this->token
                        'Content-Type' => 'application/json',
                    ],
                ]);
                $url_vps_us = 'https://dashboard.cloudzone.vn/api/vms/';
                $response = $client->put(
                    $url_vps_us .  $vps->vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                    [
                        'body' => $data_json,
                    ]
                );
                if ($response->getStatusCode() == 200) {
                    Log::info($response->getBody()->getContents());
                    // $reset_password = json_decode($response->getBody()->getContents());
                    // if ( $reset_password->reset_pass_done == '0' ) {
                    //     $vps->status_vps = 'reset_password';
                    //     $vps->save();
                    // }
                    return true;
                }
                return false;
            } catch (Exception $e) {
                report($e);
                Log::info($data_json);
                return false;
            }
        }
        else {
            $data = [
                "status" => "resetpass",
            ];
            // dd($data);
            $data_json = json_encode($data);
            try {
                $client = new Client([
                    'headers' => [
                        'Authorization' => $this->token, //$this->token
                        'Content-Type' => 'application/json',
                    ],
                ]);
                $url_vps_us = 'https://dashboard.cloudzone.vn/api/usrs/';
                $response = $client->put(
                    $url_vps_us .  $vps->vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                    [
                        'body' => $data_json,
                    ]
                );
                if ($response->getStatusCode() == 200) {
                    return true;
                }
                return false;
            } catch (Exception $e) {
                report($e);
                Log::info($data_json);
                return false;
            }
        }
    }

    // change ip
    public function change_ip_vps_us($vps)
    {
      // return true;
      if (empty($vps->vm_id)) {
        $data_action['error'] = 1;
        return $data_action;
      }
      $data = [
        "status" => "changeip",
      ];
      // dd($data);
      $data_json = json_encode($data);
      try {
          $client = new Client([
            'headers' => [
                'Authorization' => $this->token, //$this->token
                'Content-Type' => 'application/json',
            ],
          ]);
          $url_vps_us = 'https://dashboard.cloudzone.vn/api/usrs/';
          $response = $client->put(
            $url_vps_us .  $vps->vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
            [
              'body' => $data_json,
            ]
          );
          if ($response->getStatusCode() == 200) {
            return true;
          }
          return false;
      } catch (Exception $e) {
            report($e);
            Log::info($data_json);
            return false;
      }
    }

    // thay đổi IP VPS
    public function change_ip($data_change_vps, $vm_id)
    {
        // return true;
        try {
            $data_json = json_encode($data_change_vps);
            // dd($data_json);
            $client = new Client([
                'headers' => [
                'Authorization' => $this->token, //$this->token
                'Content-Type' => 'application/json',
                ],
            ]);
            $response = $client->put(
                $this->url_status .  $vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                [
                    'body' => $data_json,
                ]
            );
            if ($response->getStatusCode() == 200) {
                $response = json_decode($response->getBody()->getContents());
                return $response;
            } elseif ($response->getStatusCode() == 201) {
                $response = json_decode($response->getBody()->getContents());
                return $response;
            }
        } catch (\Exception $e) {
            report($e);
            return false;
        }
    }

    // Đổi user cho VPS
    public function update_sevice_vps($data_update_sevice_vps, $vm_id)
    {
        try {
            $data_json = json_encode($data_update_sevice_vps);
            // dd($data_json);
            $client = new Client([
                'headers' => [
                'Authorization' => $this->token, //$this->token
                'Content-Type' => 'application/json',
                ],
            ]);
            $response = $client->put(
                $this->url_status .  $vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                [
                    'body' => $data_json,
                ]
            );
            if ($response->getStatusCode() == 200) {
                $response = json_decode($response->getBody()->getContents());
                return $response;
            } elseif ($response->getStatusCode() == 201) {
                $response = json_decode($response->getBody()->getContents());
                return $response;
            }
        } catch (\Exception $e) {
            report($e);
            $data_json = json_encode($data_update_sevice_vps);
            Log::info($data_json);
            return false;
        }
    }

    public function vps_console($vm_id)
    {
      $url_console = $this->url_status . $vm_id .'/';
      $client = new Client([
        'headers' => [
              'Authorization' => $this->token, //$this->token
              'Content-Type' => 'application/json',
            ],
          ]);
      $option = [
        'timeout' => 10,
        'connect_timeout' => 10
      ];
      try {
        $response = $client->get($url_console, $option);
        $response = json_decode($response->getBody()->getContents());
        return $response;
      } catch (Exception $e) {
        report($e);
        return false;
      }
    }

    public function update_leased_time($vm_id, $data_update_leased_time, $ip)
    {
        try {
            $data_json = json_encode($data_update_leased_time);
            // dd($data_json);
            $client = new Client([
                'headers' => [
                'Authorization' => $this->token, //$this->token
                'Content-Type' => 'application/json',
                ],
            ]);
            $response = $client->put(
                $this->url_status .  $vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                [
                    'body' => $data_json,
                ]
            );
            if ($response->getStatusCode() == 200) {
                $response = json_decode($response->getBody()->getContents());
                return $response;
            } elseif ($response->getStatusCode() == 201) {
                $response = json_decode($response->getBody()->getContents());
                return $response;
            }
        } catch (\Exception $e) {
            report($e);
            return false;
        }
    }

    // đổi user cho vps
    public function change_user($vps, $customer_id)
    {
      if ( $vps->location == 'cloudzone' ) {
          $data = [
            "mob" => "change_customer",
            "customer" => $customer_id,
          ];
          try {
            $data_json = json_encode($data);
            $client = new Client([
              'headers' => [
              'Authorization' => $this->token, //$this->token
              'Content-Type' => 'application/json',
            ],
          ]);
            $response = $client->put(
            $this->url_status .  $vps->vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
            [
              'body' => $data_json,
            ]
          );
            if ($response->getStatusCode() == 200) {
              return true;
            }
            elseif ( $response->getStatusCode() == 201 ) {
              return true;
            }
          } catch (Exception $e) {
            report($e);
            $data_json = json_encode($data);
            Log::info($data_json);
            return false;
          }
      }
      else {
        if ( $vps->vm_id < 1000000000 ) {
            $data = [
                "mob" => "change_customer",
                "customer" => $customer_id,
            ];
            try {
                $data_json = json_encode($data);
                $client = new Client([
                      'headers' => [
                      'Authorization' => $this->token, //$this->token
                      'Content-Type' => 'application/json',
                    ],
                ]);
                $response = $client->put(
                    $this->url_status .  $vps->vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                    [
                        'body' => $data_json,
                    ]
                );
                if ($response->getStatusCode() == 200) {
                      return true;
                }
                elseif ( $response->getStatusCode() == 201 ) {
                      return true;
                }
            } catch (Exception $e) {
                report($e);
                $data_json = json_encode($data);
                Log::info($data_json);
                return false;
            }
        } else {
            $data = [
              "status" => "change_customer",
              "customer" => $customer_id,
            ];
            $data_json = json_encode($data);

            $client = new Client([
              'headers' => [
              'Authorization' => $this->token, //$this->token
              'Content-Type' => 'application/json',
              ],
            ]);

            try {
                $url_vps_us = 'https://dashboard.cloudzone.vn/api/usrs/';
                $response = $client->put(
                  $url_vps_us .  $vps->vm_id . '/', //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                  [
                    'body' => $data_json,
                  ]
                );
                // dd($response->getStatusCode());
                if ($response->getStatusCode() == 200) {
                  return true;
                }
                elseif ($response->getStatusCode() == 201) {
                  return true;
                }
                else {
                  return false;
                }
            } catch (Exception $e) {
                report($e);
                $data_json = json_encode($data);
                Log::info($data_json);
                return false;
            }
        }
      }
    }

    // kiểm tra sl khả dụng proxy
    public function check_qtt_proxy($data_check)
    {
        try {
            $data_json = json_encode($data_check);
            $client = new Client([
                  'headers' => [
                  'Authorization' => $this->token, //$this->token
                  'Content-Type' => 'application/json',
                ],
            ]);
            $response = $client->get(
                $this->url_proxy, //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                [
                    'body' => $data_json,
                ]
            );
            if ($response->getStatusCode() == 200) {
                $response = json_decode($response->getBody()->getContents());
                return $response;
            }
            elseif ( $response->getStatusCode() == 201 ) {
                $response = json_decode($response->getBody()->getContents());
                return $response;
            }
        } catch (Exception $e) {
            report($e);
            $data_json = json_encode($data_check);
            Log::info($data_json);
            $user = Auth::user();
            $user->error = 2;
            $user->qtt = 0;
            return $user;
        }
    }

    public function createProxy($data)
    {
        try {
            $data_json = json_encode($data);
            $client = new Client([
                  'headers' => [
                  'Authorization' => $this->token, //$this->token
                  'Content-Type' => 'application/json',
                ],
            ]);
            $response = $client->post(
                $this->url_proxy, //$this->url_create ('http://192.168.1.111:8000/api/test/' -- url local)
                [
                    'body' => $data_json,
                ]
            );
            if ($response->getStatusCode() == 200) {
                $response = json_decode($response->getBody()->getContents());
                return $response;
            }
            elseif ( $response->getStatusCode() == 201 ) {
                $response = json_decode($response->getBody()->getContents());
                return $response;
            }
        } catch (Exception $e) {
            report($e);
            $data_json = json_encode($data);
            Log::info($data_json);
            $user = Auth::user();
            $user->error = 2;
            $user->qtt = 0;
            return $user;
        }
    }

    public function expired_proxy($data)
    {
        // dd($data);
        try {
            $data_json = json_encode($data);
            $client = new Client([
                  'headers' => [
                  'Authorization' => $this->token, //$this->token
                  'Content-Type' => 'application/json',
                ],
            ]);
            $response = $client->put(
                'https://dashboard.cloudzone.vn/api/proxy_multi/',
                [
                    'body' => $data_json,
                ]
            );
            if ($response->getStatusCode() == 200) {
                $response = json_decode($response->getBody()->getContents());
                // dd($response);
                return $response;
            }
            elseif ( $response->getStatusCode() == 201 ) {
                $response = json_decode($response->getBody()->getContents());
                return $response;
            }
        } catch (Exception $e) {
            report($e);
            $data_json = json_encode($data);
            Log::info($data_json);
            $user = Auth::user();
            $user->error = 2;
            $user->qtt = 0;
            return $user;
        }
    }

}
