<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Factories\UserFactories;

class MarketingUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'marketing:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'mail marketing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $repo = UserFactories::cronTab();
      // $repo->send_mail_marketing();
      // $repo->send_mail_marketing_tiny();
      // $repo->send_mail_marketing_vps_uk();
    }
}
