<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Factories\UserFactories;

class SendMailQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendmailqueue:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mail queue cho hệ thống';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $repo = UserFactories::cronTab();
        $repo->send_mail_once_minute();
    }
}
