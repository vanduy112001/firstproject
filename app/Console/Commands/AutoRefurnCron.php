<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Factories\UserFactories;

class AutoRefurnCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autorefurn:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Tự động gia hạn dịch vụ VPS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $repo = UserFactories::cronTab();
        // $repo->check_gd_vcb();
        $repo->autoRefurn();
    }
}
