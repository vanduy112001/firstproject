<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Factories\UserFactories;

class ResetMailMarketing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'resetMarketing:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset Mail Marketing';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $repo = UserFactories::cronTab();
      $repo->reset_mail_marketing();
    }
}
