<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Factories\UserFactories;

class TutorialUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'tutorial:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hướng dẫn sử dụng tài khoản Portal Cloudzone (Tạo User)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $repo = UserFactories::cronTab();
        // $repo->send_mail_tutorial();
        $repo->send_mail_update_feature();
    }
}
