<?php

namespace App\Policies;

use App\Model\User;
use App\Model\GroupProduct;
use Illuminate\Auth\Access\HandlesAuthorization;

class GroupProductPolicy
{
    use HandlesAuthorization;

    public function checkGroupProduct(User $user, GroupProduct $groupProduct)
    {
        return $user->group_user_id === $groupProduct->group_user_id;
    }
}
