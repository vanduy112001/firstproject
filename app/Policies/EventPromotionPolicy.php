<?php

namespace App\Policies;

use App\Model\User;
use App\Model\EventPromotion;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventPromotionPolicy
{
    use HandlesAuthorization;

    public function indexEventPromotion(User $user)
    {
        // dd($user->user_meta->role);
        return true;
    }

    public function actionsEventPromotion(User $user)
    {
        return true;
    }

    public function deleteEventPromotion(User $user)
    {
        return $user->user_meta->role == 'admin' ? true : false;
        // return false;
    }
}
