<?php
namespace App\Repositories\Api;

use Hash;
use App\Model\User;
use App\Model\Agency;
use App\Model\Product;
use App\Model\GroupProduct;
use App\Model\MetaProduct;
use App\Model\Pricing;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\HistoryPay;
use Mail;
use App\Model\Vps;
use App\Model\Hosting;
use App\Model\Email;
use App\Model\OrderAddonVps;
use App\Model\OrderAddonServer;
use App\Model\ProductUpgrate;
use App\Model\OrderUpgradeHosting;
use App\Model\OrderChangeVps;
use App\Model\OrderExpired;
use Carbon\Carbon;
use App\Model\LogActivity;
use App\Model\Credit;
use App\Factories\AdminFactories;

class ApiServiceRepositories {

  protected $user;
  protected $agency;
  protected $product;
  protected $group_product;
  protected $meta_product;
  protected $pricing;
  protected $order;
  protected $detail_order;
  protected $vps;
  protected $server;
  protected $hosting;
  protected $email;
  protected $user_email;
  protected $subject;
  protected $history_pay;
  protected $order_addon_vps;
  protected $order_addon_server;
  protected $product_upgrate;
  protected $order_upgrade_hosting;
  protected $order_change_ip;
  protected $log_activity;
  protected $order_expired;
  protected $credit;

  protected $dashboard;
  protected $da;



  public function __construct()
  {
    $this->user = new User;
    $this->agency = new Agency;
    $this->product = new Product;
    $this->group_product = new GroupProduct;
    $this->meta_product = new MetaProduct;
    $this->pricing = new Pricing;
    $this->order = new Order;
    $this->detail_order = new DetailOrder;
    $this->vps = new Vps;
    $this->hosting = new Hosting;
    $this->email = new Email;
    $this->history_pay = new HistoryPay;
    $this->order_addon_vps = new OrderAddonVps;
    $this->order_addon_server = new OrderAddonServer;
    $this->product_upgrate = new ProductUpgrate;
    $this->order_upgrade_hosting = new OrderUpgradeHosting;
    $this->order_change_ip = new OrderChangeVps;
    $this->log_activity = new LogActivity();
    $this->order_expired = new OrderExpired;
    $this->credit = new Credit;

    $this->dashboard = AdminFactories::dashBoardRepositories();
    $this->da = AdminFactories::directAdminRepositories();
  }

  public function vps_action($data)
  {
      $agency = $this->agency->where('user_api', $data['user'])->where('password_api', $data['password'])->first();
      $data_restore = [];
      switch ( $data['action'] ) {
        case 'list_vps':
            $list_vps = $this->list_vps_with_api($agency->user_id);
            $data_restore = [
              "error" => 0,
              "list_vps" => $list_vps
            ];
            return $data_restore;
          break;
        case 'off':
            if ( !is_array($data['list_vps']) ) {
                $data_restore = [
                  "error" => 11,
                  "status" => 'Danh sách VPS không phải là mảng.',
                ];
                return $data_restore;
            }
            $check_vps = $this->check_list_vps($data['list_vps'] , $agency->user_id);
            if ( $check_vps ) {
                $off_list_vps = $this->off_list_vps($data['list_vps'], $agency->user_id);
                $data_restore = [
                  "error" => 0,
                  "status" => 'Tắt danh sách vps thành công',
                  "list_vps" => $off_list_vps
                ];
                return $data_restore;
            } else {
                $data_restore = [
                  "error" => 4,
                  "status" => 'Lỗi, trong danh sách VPS cần tắt có một hoặc nhiều VPS không thuộc quyền sở hửu của quý khách.'
                ];
                return $data_restore;
            }
          break;
        case 'on':
            if ( !is_array($data['list_vps']) ) {
                $data_restore = [
                  "error" => 11,
                  "status" => 'Danh sách VPS không phải là mảng.',
                ];
                return $data_restore;
            }
            $check_vps = $this->check_list_vps($data['list_vps'] , $agency->user_id);
            if ( $check_vps ) {
                $on_list_vps = $this->on_list_vps($data['list_vps'], $agency->user_id);
                if ( empty($on_list_vps['error']) ) {
                    $data_restore = [
                      "error" => 0,
                      "status" => 'Bật danh sách vps thành công',
                      "list_vps" => $on_list_vps
                    ];
                }
                else {
                    if ( $on_list_vps['error'] == 4 ) {
                      $data_restore = [
                        "error" => 5,
                        "status" => 'Lỗi truy vấn, trong danh sách VPS cần bật có một hoặc nhiều VPS bị khóa bởi Admin.',
                      ];
                    }
                    elseif ( $on_list_vps['error'] == 5 ) {
                      $data_restore = [
                        "error" => 6,
                        "status" => 'Lỗi truy vấn, trong danh sách VPS cần bật có một hoặc nhiều VPS bị khóa bởi Admin.',
                      ];
                    }
                    elseif ( $on_list_vps['error'] == 6 ) {
                      $data_restore = [
                        "error" => 7,
                        "status" => 'Lỗi truy vấn, trong danh sách VPS cần bật có một hoặc nhiều VPS bị hủy.',
                      ];
                    }
                }
                return $data_restore;
            } else {
                $data_restore = [
                  "error" => 4,
                  "status" => 'Lỗi truy vấn, trong danh sách VPS cần bật có một hoặc nhiều VPS không thuộc quyền sở hửu của quý khách.'
                ];
                return $data_restore;
            }
          break;
        case 'restart':
            if ( !is_array($data['list_vps']) ) {
                $data_restore = [
                  "error" => 11,
                  "status" => 'Danh sách VPS không phải là mảng.',
                ];
                return $data_restore;
            }
            $check_vps = $this->check_list_vps($data['list_vps'] , $agency->user_id);
            if ( $check_vps ) {
                $restart_list_vps = $this->restart_list_vps($data['list_vps'], $agency->user_id);
                if ( empty($restart_list_vps['error']) ) {
                    $data_restore = [
                      "error" => 0,
                      "status" => 'Khởi động lại danh sách vps thành công',
                      "list_vps" => $restart_list_vps
                    ];
                }
                else {
                    if ( $restart_list_vps['error'] == 4 ) {
                      $data_restore = [
                        "error" => 5,
                        "status" => 'Lỗi truy vấn, trong danh sách VPS cần khởi động lại có một hoặc nhiều VPS bị khóa bởi Admin.',
                      ];
                    }
                    elseif ( $restart_list_vps['error'] == 5 ) {
                      $data_restore = [
                        "error" => 6,
                        "status" => 'Lỗi truy vấn, trong danh sách VPS cần khởi động lại có một hoặc nhiều VPS bị khóa bởi Admin.',
                      ];
                    }
                    elseif ( $restart_list_vps['error'] == 6 ) {
                      $data_restore = [
                        "error" => 7,
                        "status" => 'Lỗi truy vấn, trong danh sách VPS cần khởi động lại có một hoặc nhiều VPS bị hủy.',
                      ];
                    }
                }
                return $data_restore;
            } else {
                $data_restore = [
                  "error" => 4,
                  "status" => 'Lỗi truy vấn, trong danh sách VPS cần khởi động lại có một hoặc nhiều VPS không thuộc quyền sở hửu của quý khách.'
                ];
                return $data_restore;
            }
          break;
        case 'rebuild':
            if ( !is_array($data['list_vps']) ) {
                $data_restore = [
                  "error" => 11,
                  "status" => 'Danh sách VPS không phải là mảng.',
                ];
                return $data_restore;
            }
            $check_vps = $this->check_list_vps($data['list_vps'] , $agency->user_id);
            if ( empty($data['os']) ) {
                $data_restore = [
                  "error" => 8,
                  "status" => 'Lỗi truy vấn, trong Data Request không có thông tin hệ điều hành',
                ];
            }
            if ( empty($data['security']) ) {
                $data_restore = [
                  "error" => 9,
                  "status" => 'Lỗi truy vấn, trong Data Request không có thông tin bảo mật',
                ];
            }
            if ( $check_vps ) {
                $rebuild_list_vps = $this->rebuild_list_vps($data['list_vps'], $agency->user_id, $data['os'], $data['security']);
                if ( empty($rebuild_list_vps['error']) ) {
                    $data_restore = [
                      "error" => 0,
                      "status" => 'Cài đặt lại danh sách vps thành công',
                      "list_vps" => $rebuild_list_vps
                    ];
                }
                else {
                    if ( $rebuild_list_vps['error'] == 4 ) {
                      $data_restore = [
                        "error" => 5,
                        "status" => 'Lỗi truy vấn, trong danh sách VPS cần cài đặt lại có một hoặc nhiều VPS bị khóa bởi Admin.',
                      ];
                    }
                    elseif ( $rebuild_list_vps['error'] == 5 ) {
                      $data_restore = [
                        "error" => 6,
                        "status" => 'Lỗi truy vấn, trong danh sách VPS cần cài đặt lại có một hoặc nhiều VPS bị khóa bởi Admin.',
                      ];
                    }
                    elseif ( $rebuild_list_vps['error'] == 6 ) {
                      $data_restore = [
                        "error" => 7,
                        "status" => 'Lỗi truy vấn, trong danh sách VPS cần cài đặt lại có một hoặc nhiều VPS bị hủy.',
                      ];
                    }
                }
                return $data_restore;
            } else {
                $data_restore = [
                  "error" => 4,
                  "status" => 'Lỗi truy vấn, trong danh sách VPS cần cài đặt lại có một hoặc nhiều VPS không thuộc quyền sở hửu của quý khách.'
                ];
                return $data_restore;
            }
          break;
        case 'cancel':
            if ( !is_array($data['list_vps']) ) {
                $data_restore = [
                  "error" => 11,
                  "status" => 'Danh sách VPS không phải là mảng.',
                ];
                return $data_restore;
            }
            $check_vps = $this->check_list_vps($data['list_vps'] , $agency->user_id);
            if ( $check_vps ) {
                $terminated_vps = $this->terminated_vps($data['list_vps'], $agency->user_id);
                $data_restore = [
                  "error" => 0,
                  "status" => 'Hủy danh sách vps thành công',
                  "list_vps" => $terminated_vps
                ];
                return $data_restore;
            } else {
                $data_restore = [
                  "error" => 4,
                  "status" => 'Lỗi, trong danh sách VPS cần hủy có một hoặc nhiều VPS không thuộc quyền sở hửu của quý khách.'
                ];
                return $data_restore;
            }
          break;
        case 'get_status_vps':
            if ( !is_array($data['list_vps']) ) {
                $data_restore = [
                  "error" => 11,
                  "status" => 'Danh sách VPS không phải là mảng.',
                ];
                return $data_restore;
            }
            $check_vps = $this->check_list_vps($data['list_vps'] , $agency->user_id);
            if ( $check_vps ) {
                $get_status_vps = $this->get_status_vps($data['list_vps'], $agency->user_id);
                $data_restore = [
                  "error" => 0,
                  "status" => 'Lấy trạng thái danh sách vps thành công',
                  "list_vps" => $get_status_vps
                ];
                return $data_restore;
            } else {
                $data_restore = [
                  "error" => 4,
                  "status" => 'Lỗi, trong danh sách VPS cần hủy có một hoặc nhiều VPS không thuộc quyền sở hửu của quý khách.'
                ];
                return $data_restore;
            }
          break;
        default:
            $data_restore = [
              "error" => 10,
              "status" => 'Lỗi, không có yêu cầu truy vấn.'
            ];
            return $data_restore;
          break;
      }
  }

  public function list_vps_with_api($user_id)
  {
      $list_vps = $this->vps->where('user_id', $user_id)->where('status', 'Active')
                        ->where('type_vps', 'vps')
                        ->where('location', 'cloudzone')
                        ->where(function($query)
                        {
                            return $query
                              ->orwhere('status_vps', 'on')
                              ->orWhere('status_vps', 'off')
                              ->orWhere('status_vps', 'admin_off')
                              ->orWhere('status_vps', 'progressing')
                              ->orWhere('status_vps', 'change_ip')
                              ->orWhere('status_vps', 'rebuild');
                        })
                        ->orderBy('id', 'desc')->with('product')->get();
      $data = [];
      if ($list_vps->count() > 0) {
          foreach ($list_vps as $key => $vps) {
              $product = $vps->product;
              $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
              $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
              $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
              // Addon
              if (!empty($vps->vps_config)) {
                  $vps_config = $vps->vps_config;
                  if (!empty($vps_config)) {
                      $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                      $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                      $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                  }
              }

              $total = 0;
              if ( !empty($vps->price_override) ) {
                 $total = $vps->price_override;
              } else {
                  $product = $vps->product;
                  $total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                  if (!empty($vps->vps_config)) {
                    $addon_vps = $vps->vps_config;
                    $add_on_products = UserHelper::get_addon_product_private($vps->user_id);
                    $pricing_addon = 0;
                    foreach ($add_on_products as $key => $add_on_product) {
                      if (!empty($add_on_product->meta_product->type_addon)) {
                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                          $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                          $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                          $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                        }
                      }
                    }
                    $total += $pricing_addon;
                  }
              }

              $data[] = [
                'portal_vps_id' => $vps->id,
                'ip' => $vps->ip,
                'user' => $vps->user,
                'password' => $vps->password,
                'product' => $product->name,
                'billing_cycle' => $vps->billing_cycle,
                'config' => $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk',
                'date_create' => date('d-m-Y', strtotime($vps->date_create)),
                'next_due_date' => date('d-m-Y', strtotime($vps->next_due_date)),
                'status' => $vps->status_vps,
                'amount' => $total,
              ];
          }
      }
      return $data;
  }

  public function off_list_vps($list_vps, $user_id)
  {
      $data_vps_off = [];
      foreach ($list_vps as $key => $vps_id) {
          $vps = $this->vps->find($vps_id['portal_vps_id']);
          $off = $this->dashboard->offVPS($vps);
          $data_log = [
            'user_id' => $user_id,
            'action' => 'đại lý API tắt',
            'model' => 'User/Services_VPS',
            'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
            'description_user' => ' dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
            'service' =>  $vps->ip,
          ];
          $this->log_activity->create($data_log);
          $data_vps_off[] = [
            'portal_vps_id' => $vps->id,
            'ip' => $vps->ip,
            'status' => $vps->status_vps,
          ];
      }
      return $data_vps_off;
  }

  public function on_list_vps($list_vps, $user_id)
  {
      $data_vps_off = [];
      foreach ($list_vps as $key => $vps_id) {
          $vps = $this->vps->find($vps_id['portal_vps_id']);

          if ($vps->status_vps == 'admin_off') {
                $data = [
                  'error' => 4
                ];
                return $data;
          }

          if ($vps->status_vps == 'suspend') {
                $data = [
                  'error' => 6
                ];
                return $data;
          }

          if ($vps->status_vps == 'cancel') {
                $data = [
                  'error' => 5
                ];
                return $data;
          }

          $on = $this->dashboard->onVPS($vps);

          $data_log = [
            'user_id' => $user_id,
            'action' => 'đại lý API bật',
            'model' => 'User/Services_VPS',
            'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
            'description_user' => ' dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> ',
            'service' =>  $vps->ip,
          ];
          $this->log_activity->create($data_log);

          $data_vps_off[] = [
            'portal_vps_id' => $vps->id,
            'ip' => $vps->ip,
            'status' => $vps->status_vps,
          ];
      }
      return $data_vps_off;
  }

  public function restart_list_vps($list_vps, $user_id)
  {
      $data_vps = [];
      foreach ($list_vps as $key => $vps_id) {
          $vps = $this->vps->find($vps_id['portal_vps_id']);

          if ($vps->status_vps == 'admin_off') {
                $data = [
                  'error' => 4
                ];
                return $data;
          }

          if ($vps->status_vps == 'suspend') {
                $data = [
                  'error' => 6
                ];
                return $data;
          }

          if ($vps->status_vps == 'cancel') {
                $data = [
                  'error' => 5
                ];
                return $data;
          }

          $on = $this->dashboard->restartVPS($vps);
          $data_log = [
            'user_id' => $user_id,
            'action' => 'đại lý API khởi động lại',
            'model' => 'User/Services_VPS',
            'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
            'description_user' => ' dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
            'service' =>  $vps->ip,
          ];
          $this->log_activity->create($data_log);

          $data_vps[] = [
            'portal_vps_id' => $vps->id,
            'ip' => $vps->ip,
            'status' => $vps->status_vps,
          ];
      }
      return $data_vps;
  }

  public function rebuild_list_vps($list_vps, $user_id, $os, $security)
  {
      $data_vps = [];
      if ( empty($security) ) {
          if ($os == 1) {
              $os = 11;
          }
          else if ($os == 2) {
              $os = 12;
          }
          else if ($os == 3) {
              $os = 13;
          }
          else {
              $os = 14;
          }
      }
      foreach ($list_vps as $key => $vps_id) {
          $vps = $this->vps->find($vps_id['portal_vps_id']);

          if ($vps->status_vps == 'admin_off') {
                $data = [
                  'error' => 4
                ];
                return $data;
          }

          if ($vps->status_vps == 'suspend') {
                $data = [
                  'error' => 6
                ];
                return $data;
          }

          if ($vps->status_vps == 'cancel') {
                $data = [
                  'error' => 5
                ];
                return $data;
          }

          $data_rebuild = [
            "mob" => "rebuild",
            "name" => $vps->vm_id,
            "template" => $os,
            'type' => 0,
          ];

          $rebuild = $this->dashboard->rebuild_vps($data_rebuild, $vps->vm_id, $vps);

          if ($rebuild) {
              $vps->status_vps = 'rebuild';
              $vps->os = $os;
              $vps->security = !empty($security) ? true : false;
              $vps->save();
              try {
                  // ghi log
                  $data_log = [
                    'user_id' => $user_id,
                    'action' => 'đại lý API cài đặt lại',
                    'model' => 'User/Order',
                    'description' => ' VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                    'description_user' => ' VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
                    'service' =>  $vps->ip,
                  ];
                  $this->log_activity->create($data_log);
              } catch (\Exception $e) {
                  continue;
              }
          }

          $data_vps[] = [
            'portal_vps_id' => $vps->id,
            'ip' => $vps->ip,
            'status' => $vps->status_vps,
          ];
      }
      return $data_vps;
  }

  public function terminated_vps($list_vps, $user_id)
  {
      $data_vps_off = [];
      foreach ($list_vps as $key => $vps_id) {
          $vps = $this->vps->find($vps_id['portal_vps_id']);
          $data_log = [
            'user_id' => $user_id,
            'action' => 'đại lý API hủy',
            'model' => 'User/Services_VPS',
            'description' => ' dịch vụ VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ',
            'description_user' => ' dịch vụ VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> ',
            'service' => $vps->ip,
          ];
          $this->log_activity->create($data_log);

          $terminated = $this->dashboard->terminatedVps($vps);

          $data_vps_off[] = [
            'portal_vps_id' => $vps->id,
            'ip' => $vps->ip,
            'status' => $vps->status_vps,
          ];
      }
      return $data_vps_off;
  }

  public function get_status_vps($list_vps, $user_id)
  {
      $data_vps_off = [];
      foreach ($list_vps as $key => $vps_id) {
          $vps = $this->vps->find($vps_id['portal_vps_id']);

          $data_vps_off[] = [
            'portal_vps_id' => $vps->id,
            'ip' => $vps->ip,
            'status' => $vps->status_vps,
          ];
      }
      return $data_vps_off;
  }

  public function check_list_vps($list_vps, $user_id)
  {
      foreach ($list_vps as $key => $vps_id) {
          $vps = $this->vps->where("id", $vps_id)->where('user_id' , $user_id)->first();
          if ( !isset($vps) ) {
              return false;
          }
      }
      return true;
  }

  public function nat_vps_action($data)
  {
      $agency = $this->agency->where('user_api', $data['user'])->where('password_api', $data['password'])->first();
      $data_restore = [];
      switch ( $data['action'] ) {
        case 'list_nat_vps':
            $list_vps = $this->list_nat_vps_with_api($agency->user_id);
            $data_restore = [
              "error" => 0,
              "list_nat_vps" => $list_vps
            ];
            return $data_restore;
          break;
        case 'off':
            if ( !is_array($data['list_vps']) ) {
                $data_restore = [
                  "error" => 11,
                  "status" => 'Danh sách VPS không phải là mảng.',
                ];
                return $data_restore;
            }
            $check_vps = $this->check_list_vps($data['list_vps'] , $agency->user_id);
            if ( $check_vps ) {
              $off_list_vps = $this->off_list_vps($data['list_vps'], $agency->user_id);
              $data_restore = [
                "error" => 0,
                "status" => 'Tắt danh sách NAT VPS thành công',
                "list_vps" => $off_list_vps
              ];
              return $data_restore;
            } else {
              $data_restore = [
                "error" => 4,
                "status" => 'Lỗi, trong danh sách NAT VPS cần tắt có một hoặc nhiều VPS không thuộc quyền sở hửu của quý khách.'
              ];
              return $data_restore;
            }
          break;
        case 'on':
            if ( !is_array($data['list_vps']) ) {
                $data_restore = [
                  "error" => 11,
                  "status" => 'Danh sách VPS không phải là mảng.',
                ];
                return $data_restore;
            }
            $check_vps = $this->check_list_vps($data['list_vps'] , $agency->user_id);
            if ( $check_vps ) {
              $on_list_vps = $this->on_list_vps($data['list_vps'], $agency->user_id);
              if ( empty($on_list_vps['error']) ) {
                $data_restore = [
                  "error" => 0,
                  "status" => 'Bật danh sách NAT VPS thành công',
                  "list_vps" => $on_list_vps
                ];
              }
              else {
                if ( $on_list_vps['error'] == 4 ) {
                  $data_restore = [
                    "error" => 5,
                    "status" => 'Lỗi truy vấn, trong danh sách VPS cần bật có một hoặc nhiều NAT VPS bị khóa bởi Admin.',
                  ];
                }
                elseif ( $on_list_vps['error'] == 5 ) {
                  $data_restore = [
                    "error" => 6,
                    "status" => 'Lỗi truy vấn, trong danh sách VPS cần bật có một hoặc nhiều NAT VPS bị khóa bởi Admin.',
                  ];
                }
                elseif ( $on_list_vps['error'] == 6 ) {
                  $data_restore = [
                    "error" => 7,
                    "status" => 'Lỗi truy vấn, trong danh sách VPS cần bật có một hoặc nhiều NAT VPS bị hủy.',
                  ];
                }
              }
              return $data_restore;
            } else {
              $data_restore = [
                "error" => 4,
                "status" => 'Lỗi truy vấn, trong danh sách NAT VPS cần bật có một hoặc nhiều NAT VPS không thuộc quyền sở hửu của quý khách.'
              ];
              return $data_restore;
            }
          break;
        case 'restart':
            if ( !is_array($data['list_vps']) ) {
                $data_restore = [
                  "error" => 11,
                  "status" => 'Danh sách VPS không phải là mảng.',
                ];
                return $data_restore;
            }
            $check_vps = $this->check_list_vps($data['list_vps'] , $agency->user_id);
            if ( $check_vps ) {
              $restart_list_vps = $this->restart_list_vps($data['list_vps'], $agency->user_id);
              if ( empty($restart_list_vps['error']) ) {
                $data_restore = [
                  "error" => 0,
                  "status" => 'Khởi động lại danh sách NAT VPS thành công',
                  "list_vps" => $restart_list_vps
                ];
              }
              else {
                if ( $restart_list_vps['error'] == 4 ) {
                  $data_restore = [
                    "error" => 5,
                    "status" => 'Lỗi truy vấn, trong danh sách VPS cần khởi động lại có một hoặc nhiều NAT VPS bị khóa bởi Admin.',
                  ];
                }
                elseif ( $restart_list_vps['error'] == 5 ) {
                  $data_restore = [
                    "error" => 6,
                    "status" => 'Lỗi truy vấn, trong danh sách VPS cần khởi động lại có một hoặc nhiều NAT VPS bị khóa bởi Admin.',
                  ];
                }
                elseif ( $restart_list_vps['error'] == 6 ) {
                  $data_restore = [
                    "error" => 7,
                    "status" => 'Lỗi truy vấn, trong danh sách VPS cần khởi động lại có một hoặc nhiều NAT VPS bị hủy.',
                  ];
                }
              }
              return $data_restore;
            } else {
              $data_restore = [
                "error" => 4,
                "status" => 'Lỗi truy vấn, trong danh sách VPS cần khởi động lại có một hoặc nhiều NAT VPS không thuộc quyền sở hửu của quý khách.'
              ];
              return $data_restore;
            }
          break;
        case 'rebuild':
            if ( !is_array($data['list_vps']) ) {
                $data_restore = [
                  "error" => 11,
                  "status" => 'Danh sách VPS không phải là mảng.',
                ];
                return $data_restore;
            }
            $check_vps = $this->check_list_vps($data['list_vps'] , $agency->user_id);
            if ( empty($data['os']) ) {
              $data_restore = [
                "error" => 8,
                "status" => 'Lỗi truy vấn, trong Data Request không có thông tin hệ điều hành',
              ];
            }
            if ( $check_vps ) {
              $rebuild_list_vps = $this->rebuild_list_nat_vps($data['list_vps'], $agency->user_id, $data['os'], $data['security']);
              if ( empty($rebuild_list_vps['error']) ) {
                $data_restore = [
                  "error" => 0,
                  "status" => 'Cài đặt lại danh sách NAT VPS thành công',
                  "list_vps" => $rebuild_list_vps
                ];
              }
              else {
                if ( $rebuild_list_vps['error'] == 4 ) {
                  $data_restore = [
                    "error" => 5,
                    "status" => 'Lỗi truy vấn, trong danh sách VPS cần cài đặt lại có một hoặc nhiều NAT VPS bị khóa bởi Admin.',
                  ];
                }
                elseif ( $rebuild_list_vps['error'] == 5 ) {
                  $data_restore = [
                    "error" => 6,
                    "status" => 'Lỗi truy vấn, trong danh sách VPS cần cài đặt lại có một hoặc nhiều NAT VPS bị khóa bởi Admin.',
                  ];
                }
                elseif ( $rebuild_list_vps['error'] == 6 ) {
                  $data_restore = [
                    "error" => 7,
                    "status" => 'Lỗi truy vấn, trong danh sách VPS cần cài đặt lại có một hoặc nhiều NAT VPS bị hủy.',
                  ];
                }
              }
              return $data_restore;
            } else {
              $data_restore = [
                "error" => 4,
                "status" => 'Lỗi truy vấn, trong danh sách VPS cần cài đặt lại có một hoặc nhiều NAT VPS không thuộc quyền sở hửu của quý khách.'
              ];
              return $data_restore;
            }
          break;
        case 'cancel':
            if ( !is_array($data['list_vps']) ) {
                $data_restore = [
                  "error" => 11,
                  "status" => 'Danh sách VPS không phải là mảng.',
                ];
                return $data_restore;
            }
            $check_vps = $this->check_list_vps($data['list_vps'] , $agency->user_id);
            if ( $check_vps ) {
              $terminated_vps = $this->terminated_vps($data['list_vps'], $agency->user_id);
              $data_restore = [
                "error" => 0,
                "status" => 'Hủy danh sách NAT VPS thành công',
                "list_vps" => $terminated_vps
              ];
              return $data_restore;
            } else {
              $data_restore = [
                "error" => 4,
                "status" => 'Lỗi, trong danh sách VPS cần hủy có một hoặc nhiều NAT VPS không thuộc quyền sở hửu của quý khách.'
              ];
              return $data_restore;
            }
          break;
        case 'get_status_vps':
            if ( !is_array($data['list_vps']) ) {
                $data_restore = [
                  "error" => 11,
                  "status" => 'Danh sách VPS không phải là mảng.',
                ];
                return $data_restore;
            }
            $check_vps = $this->check_list_vps($data['list_vps'] , $agency->user_id);
            if ( $check_vps ) {
              $get_status_vps = $this->get_status_vps($data['list_vps'], $agency->user_id);
              $data_restore = [
                "error" => 0,
                "status" => 'Lấy trạng thái danh sách NAT VPS thành công',
                "list_vps" => $get_status_vps
              ];
              return $data_restore;
            } else {
              $data_restore = [
                "error" => 4,
                "status" => 'Lỗi, trong danh sách NAT VPS cần hủy có một hoặc nhiều VPS không thuộc quyền sở hửu của quý khách.'
              ];
              return $data_restore;
            }
          break;
        default:
            $data_restore = [
              "error" => 10,
              "status" => 'Lỗi, không có yêu cầu truy vấn.'
            ];
            return $data_restore;
          break;
      }
  }

  public function rebuild_list_nat_vps($list_vps, $user_id, $os, $security)
  {
      $data_vps = [];
      foreach ($list_vps as $key => $vps_id) {
          $vps = $this->vps->find($vps_id['portal_vps_id']);

          if ($vps->status_vps == 'admin_off') {
                $data = [
                  'error' => 4
                ];
                return $data;
          }

          if ($vps->status_vps == 'suspend') {
                $data = [
                  'error' => 6
                ];
                return $data;
          }

          if ($vps->status_vps == 'cancel') {
                $data = [
                  'error' => 5
                ];
                return $data;
          }

          $data_rebuild = [
            "mob" => "rebuild",
            "name" => $vps->vm_id,
            "template" => $os,
            'type' => 5,
          ];

          $rebuild = $this->dashboard->rebuild_vps($data_rebuild, $vps->vm_id, $vps);

          if ($rebuild) {
              $vps->status_vps = 'rebuild';
              $vps->os = $os;
              $vps->security = !empty($security) ? true : false;
              $vps->save();
              try {
                  // ghi log
                  $data_log = [
                    'user_id' => $user_id,
                    'action' => 'đại lý API cài đặt lại',
                    'model' => 'User/Order',
                    'description' => ' VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                    'description_user' => ' VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
                    'service' =>  $vps->ip,
                  ];
                  $this->log_activity->create($data_log);
              } catch (\Exception $e) {
                  continue;
              }
          }

          $data_vps[] = [
            'portal_vps_id' => $vps->id,
            'ip' => $vps->ip,
            'status' => $vps->status_vps,
          ];
      }
      return $data_vps;
  }

  public function list_nat_vps_with_api($user_id)
  {
      $list_vps = $this->vps->where('user_id', $user_id)->where('status', 'Active')
                        ->where('type_vps', 'nat_vps')
                        ->where('location', 'cloudzone')
                        ->where(function($query)
                        {
                            return $query
                              ->orwhere('status_vps', 'on')
                              ->orWhere('status_vps', 'off')
                              ->orWhere('status_vps', 'admin_off')
                              ->orWhere('status_vps', 'progressing')
                              ->orWhere('status_vps', 'change_ip')
                              ->orWhere('status_vps', 'rebuild');
                        })
                        ->orderBy('id', 'desc')->with('product')->get();
      $data = [];
      if ($list_vps->count() > 0) {
          foreach ($list_vps as $key => $vps) {
              $product = $vps->product;
              $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
              $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
              $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
              // Addon
              if (!empty($vps->vps_config)) {
                  $vps_config = $vps->vps_config;
                  if (!empty($vps_config)) {
                      $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                      $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                      $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                  }
              }

              $total = 0;
              if ( !empty($vps->price_override) ) {
                 $total = $vps->price_override;
              } else {
                  $product = $vps->product;
                  $total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                  if (!empty($vps->vps_config)) {
                    $addon_vps = $vps->vps_config;
                    $add_on_products = UserHelper::get_addon_product_private($vps->user_id);
                    $pricing_addon = 0;
                    foreach ($add_on_products as $key => $add_on_product) {
                      if (!empty($add_on_product->meta_product->type_addon)) {
                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                          $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                          $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                          $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                        }
                      }
                    }
                    $total += $pricing_addon;
                  }
              }

              $data[] = [
                'portal_vps_id' => $vps->id,
                'ip' => $vps->ip,
                'user' => $vps->user,
                'password' => $vps->password,
                'product' => $product->name,
                'billing_cycle' => $vps->billing_cycle,
                'config' => $cpu . ' CPU - ' . $ram . ' RAM - ' . $disk . ' Disk',
                'date_create' => date('d-m-Y', strtotime($vps->date_create)),
                'next_due_date' => date('d-m-Y', strtotime($vps->next_due_date)),
                'status' => $vps->status_vps,
                'amount' => $total,
              ];
          }
      }
      return $data;
  }

  public function hosting_action($data)
  {
      $agency = $this->agency->where('user_api', $data['user'])->where('password_api', $data['password'])->first();
      $data_restore = [];
      switch ( $data['action'] ) {
        case 'list_hosting':
            $list_hosting = $this->list_hosting($agency->user_id);
            $data_restore = [
              "error" => 0,
              "list_hosting" => $list_hosting
            ];
            return $data_restore;
          break;
        case 'off':
            if ( !is_array($data['list_hosting']) ) {
                $data_restore = [
                  "error" => 11,
                  "status" => 'Danh sách Hosting không phải là mảng.',
                ];
                return $data_restore;
            }
            $check_hosting = $this->check_hostings_with_user($data['list_hosting'] , $agency->user_id);
            if ( $check_hosting ) {
              $off_list_hosting = $this->off_list_hosting($data['list_hosting'], $agency->user_id);
              $data_restore = [
                "error" => 0,
                "status" => 'Tắt danh sách Hosting thành công',
                "list_hosting" => $off_list_hosting
              ];
              return $data_restore;
            } else {
              $data_restore = [
                "error" => 4,
                "status" => 'Lỗi, trong danh sách Hosting cần tắt có một hoặc nhiều Hosting không thuộc quyền sở hửu của quý khách.'
              ];
              return $data_restore;
            }
          break;
        case 'on':
            if ( !is_array($data['list_hosting']) ) {
                $data_restore = [
                  "error" => 11,
                  "status" => 'Danh sách Hosting không phải là mảng.',
                ];
                return $data_restore;
            }
            $check_hosting = $this->check_hostings_with_user($data['list_hosting'] , $agency->user_id);
            if ( $check_hosting ) {
              $on_list_hosting = $this->on_list_hosting($data['list_hosting'], $agency->user_id);
              if ( empty($on_list_hosting['error']) ) {
                $data_restore = [
                  "error" => 0,
                  "status" => 'Bật danh sách Hosting thành công',
                  "list_hosting" => $on_list_hosting
                ];
              }
              else {
                if ( $on_list_hosting['error'] == 4 ) {
                  $data_restore = [
                    "error" => 5,
                    "status" => 'Lỗi truy vấn, trong danh sách Hosting cần bật có một hoặc nhiều Hosting bị khóa bởi Admin.',
                  ];
                }
                elseif ( $on_list_hosting['error'] == 5 ) {
                  $data_restore = [
                    "error" => 6,
                    "status" => 'Lỗi truy vấn, trong danh sách Hosting cần bật có một hoặc nhiều Hosting bị khóa bởi Admin.',
                  ];
                }
                elseif ( $on_list_hosting['error'] == 6 ) {
                  $data_restore = [
                    "error" => 7,
                    "status" => 'Lỗi truy vấn, trong danh sách Hosting cần bật có một hoặc nhiều Hosting bị hủy.',
                  ];
                }
              }
              return $data_restore;
            } else {
              $data_restore = [
                "error" => 4,
                "status" => 'Lỗi truy vấn, trong danh sách Hosting cần bật có một hoặc nhiều Hosting không thuộc quyền sở hửu của quý khách.'
              ];
              return $data_restore;
            }
          break;
        case 'cancel':
            if ( !is_array($data['list_hosting']) ) {
                $data_restore = [
                  "error" => 11,
                  "status" => 'Danh sách Hosting không phải là mảng.',
                ];
                return $data_restore;
            }
            $check_hosting = $this->check_hostings_with_user($data['list_hosting'] , $agency->user_id);
            if ( $check_hosting ) {
              $terminated_hosting = $this->terminated_hosting($data['list_hosting'], $agency->user_id);
              $data_restore = [
                "error" => 0,
                "status" => 'Hủy danh sách Hosting thành công',
                "list_hosting" => $terminated_hosting
              ];
              return $data_restore;
            } else {
              $data_restore = [
                "error" => 4,
                "status" => 'Lỗi, trong danh sách Hosting cần hủy có một hoặc nhiều Hosting không thuộc quyền sở hửu của quý khách.'
              ];
              return $data_restore;
            }
          break;
        case 'get_status_hosting':
            if ( !is_array($data['list_hosting']) ) {
                $data_restore = [
                  "error" => 11,
                  "status" => 'Danh sách Hosting không phải là mảng.',
                ];
                return $data_restore;
            }
            $check_hosting = $this->check_hostings_with_user($data['list_hosting'] , $agency->user_id);
            if ( $check_hosting ) {
              $get_status_hosting = $this->get_status_hosting($data['list_hosting'], $agency->user_id);
              $data_restore = [
                "error" => 0,
                "status" => 'Lấy trạng thái danh sách Hosting thành công',
                "list_hosting" => $get_status_hosting
              ];
              return $data_restore;
            } else {
              $data_restore = [
                "error" => 4,
                "status" => 'Lỗi, trong danh sách Hosting cần hủy có một hoặc nhiều Hosting không thuộc quyền sở hửu của quý khách.'
              ];
              return $data_restore;
            }
          break;
        default:
            $data_restore = [
              "error" => 10,
              "status" => 'Lỗi, không có yêu cầu truy vấn.'
            ];
            return $data_restore;
          break;
      }
  }

  public function list_hosting($user_id)
  {
        $list_hosting = $this->hosting->where('user_id', $user_id)->with('product')
            ->where(function($query)
            {
                return $query
                  ->orwhere('status_hosting', 'on')
                  ->orwhere('status_hosting', 'expire')
                  ->orWhere('status_hosting', 'off');
            })
            ->orderBy('id', 'desc')->get();
        $data = [];
        if ($list_hosting->count() > 0) {
            foreach ($list_hosting as $key => $hosting) {
                $product = $hosting->product;

                $total = 0;
                if ( !empty($hosting->price_override) ) {
                   $total = $hosting->price_override;
                } else {
                    $product = $hosting->product;
                    $total = !empty( $product->pricing[$hosting->billing_cycle] ) ? $product->pricing[$hosting->billing_cycle] : 0;
                }

                $data[] = [
                  'portal_hosting_id' => $hosting->id,
                  'domain' => $hosting->ip,
                  'user' => $hosting->user,
                  'password' => $hosting->password,
                  'product' => $product->name,
                  'billing_cycle' => $hosting->billing_cycle,
                  'date_create' => date('d-m-Y', strtotime($hosting->date_create)),
                  'next_due_date' => date('d-m-Y', strtotime($hosting->next_due_date)),
                  'status' => $hosting->status_hosting,
                  'amount' => $total,
                ];
            }
        }
        return $data;
  }

  public function off_list_hosting($list_hosting, $user_id)
  {
      $data_hosting_off = [];
      foreach ($list_hosting as $key => $hosting_id) {
          $hosting = $this->hosting->find($hosting_id['portal_hosting_id']);
          $off = $this->da->off($hosting);
          $data_log = [
            'user_id' => $user_id,
            'action' => 'đại lý API tắt',
            'model' => 'User/Services_Hosting',
            'description' => ' dịch vụ Hosting <a target="_blank" href="/admin/hostings/detail/' . $hosting->id . '">' .$hosting->domain . '</a>',
            'description_user' => ' dịch vụ Hosting <a target="_blank" href="/service/detail/' . $hosting->id . '?type=vps">' . $hosting->domain . '</a>',
            'service' =>  $hosting->domain,
          ];
          $this->log_activity->create($data_log);
          $data_hosting_off[] = [
            'portal_hosting_id' => $hosting->id,
            'domain' => $hosting->domain,
            'status' => $hosting->status_hosting,
          ];
      }
      return $data_hosting_off;
  }

  public function on_list_hosting($list_hosting, $user_id)
  {
      $data_hosting_off = [];
      foreach ($list_hosting as $key => $hosting_id) {
          $hosting = $this->hosting->find($hosting_id['portal_hosting_id']);
          if ($hosting->status_hosting == 'admin_off') {
              $data = [
                'error' => 5
              ];
              return $data;
          }
          if ($hosting->status_hosting == 'cancel') {
              $data = [
                'error' => 7
              ];
              return $data;
          }

          $off = $this->da->unSuspend($hosting);
          $data_log = [
            'user_id' => $user_id,
            'action' => 'đại lý API bật',
            'model' => 'User/Services_Hosting',
            'description' => ' dịch vụ Hosting <a target="_blank" href="/admin/hostings/detail/' . $hosting->id . '">' .$hosting->domain . '</a>',
            'description_user' => ' dịch vụ Hosting <a target="_blank" href="/service/detail/' . $hosting->id . '?type=vps">' . $hosting->domain . '</a>',
            'service' =>  $hosting->domain,
          ];
          $this->log_activity->create($data_log);
          $data_hosting_off[] = [
            'portal_hosting_id' => $hosting->id,
            'domain' => $hosting->domain,
            'status' => $hosting->status_hosting,
          ];
      }
      return $data_hosting_off;
  }

  public function terminated_hosting($list_hosting, $user_id)
  {
      $data_hosting_off = [];
      foreach ($list_hosting as $key => $hosting_id) {
          $hosting = $this->hosting->find($hosting_id['portal_hosting_id']);
          $off = $this->da->suspendHosting($hosting);
          $data_log = [
            'user_id' => $user_id,
            'action' => 'đại lý API hủy',
            'model' => 'User/Services_Hosting',
            'description' => ' dịch vụ Hosting <a target="_blank" href="/admin/hostings/detail/' . $hosting->id . '">' .$hosting->domain . '</a>',
            'description_user' => ' dịch vụ Hosting <a target="_blank" href="/service/detail/' . $hosting->id . '?type=vps">' . $hosting->domain . '</a>',
            'service' =>  $hosting->domain,
          ];
          $this->log_activity->create($data_log);
          $data_hosting_off[] = [
            'portal_hosting_id' => $hosting->id,
            'domain' => $hosting->domain,
            'status' => $hosting->status_hosting,
          ];
      }
      return $data_hosting_off;
  }

  public function check_hostings_with_user($list_id, $user_id)
  {
      foreach ($list_id as $key => $id) {
         $hosting = $this->hosting->where('user_id', $user_id)->where('id', $id)->first();
         if (!isset($hosting)) {
            return false;
         }
      }
     return true;
  }

  public function get_status_hosting($list_hosting, $user_id)
  {
      $data_hosting_off = [];
      foreach ($list_hosting as $key => $hosting_id) {
          $hosting = $this->hosting->find($hosting_id['portal_hosting_id']);

          $data_hosting_off[] = [
            'portal_hosting_id' => $hosting->id,
            'domain' => $hosting->domain,
            'status' => $hosting->status_hosting,
          ];
      }
      return $data_hosting_off;
  }

  function rand_string() {
      $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*";
      $size = strlen( $chars );
      $str = '';
      for( $i = 0; $i < 16; $i++ ) {
          $str .= $chars[ rand( 0, $size - 1 ) ];
      }
      return $str;
  }

}

?>
