<?php
namespace App\Repositories\Api;

use Hash;
use App\Model\User;
use App\Model\Agency;
use App\Model\Product;
use App\Model\GroupProduct;
use App\Model\MetaProduct;
use App\Model\Pricing;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\HistoryPay;
use Mail;
use App\Model\Vps;
use App\Model\Hosting;
use App\Model\Email;
use App\Model\OrderAddonVps;
use App\Model\OrderAddonServer;
use App\Model\ProductUpgrate;
use App\Model\OrderUpgradeHosting;
use App\Model\OrderChangeVps;
use App\Model\OrderExpired;
use Carbon\Carbon;
use App\Model\LogActivity;
use App\Model\Credit;
use App\Factories\AdminFactories;

class ApiOrderRepositories {

  protected $user;
  protected $agency;
  protected $product;
  protected $group_product;
  protected $meta_product;
  protected $pricing;
  protected $order;
  protected $detail_order;
  protected $vps;
  protected $server;
  protected $hosting;
  protected $email;
  protected $user_email;
  protected $subject;
  protected $history_pay;
  protected $order_addon_vps;
  protected $order_addon_server;
  protected $product_upgrate;
  protected $order_upgrade_hosting;
  protected $order_change_ip;
  protected $log_activity;
  protected $order_expired;
  protected $credit;

  protected $dashboard;
  protected $da;



  public function __construct()
  {
    $this->user = new User;
    $this->agency = new Agency;
    $this->product = new Product;
    $this->group_product = new GroupProduct;
    $this->meta_product = new MetaProduct;
    $this->pricing = new Pricing;
    $this->order = new Order;
    $this->detail_order = new DetailOrder;
    $this->vps = new Vps;
    $this->hosting = new Hosting;
    $this->email = new Email;
    $this->history_pay = new HistoryPay;
    $this->order_addon_vps = new OrderAddonVps;
    $this->order_addon_server = new OrderAddonServer;
    $this->product_upgrate = new ProductUpgrate;
    $this->order_upgrade_hosting = new OrderUpgradeHosting;
    $this->order_change_ip = new OrderChangeVps;
    $this->log_activity = new LogActivity();
    $this->order_expired = new OrderExpired;
    $this->credit = new Credit;

    $this->dashboard = AdminFactories::dashBoardRepositories();
    $this->da = AdminFactories::directAdminRepositories();
  }

  public function order($data)
  {
      $data_restore = [];
      $agency = $this->agency->where('user_api', $data['user'])->where('password_api', $data['password'])->first();
      if ( $data['type_product'] == 'vps' ) {
          $list_vps = $this->order_vps($data);
          if ( !empty($list_vps) ) {
            $data_vps = [];
            foreach ($list_vps as $key => $vps) {
              $data_vps[] = [
                'portal_vps_id' => $vps->id,
                'ip' => $vps->ip,
                'user' => $vps->user,
                'password' => $vps->password,
                'next_due_date' => $vps->next_due_date,
                'date_create' => $vps->date_create,
                'status_vps' => $vps->status_vps,
              ];
            }
            $data_restore = [
              'error' => 0,
              'list_vps' => $data_vps,
            ];
          }
          else {
            $data_restore = [
              'error' => 4,
              'status' => 'Số dư tài khoản của quý khách không đủ để thực hiện giao dịch này.',
            ];
          }
      }
      elseif ( $data['type_product'] == 'nat_vps' ) {
          $list_vps = $this->order_nat_vps($data);
          if ( !empty($list_vps) ) {
            $data_vps = [];
            foreach ($list_vps as $key => $vps) {
              $data_vps = [
                'portal_vps_id' => $vps->id,
                'ip' => $vps->ip,
                'user' => $vps->user,
                'password' => $vps->password,
                'next_due_date' => $vps->next_due_date,
                'date_create' => $vps->date_create,
                'status_vps' => $vps->status_vps,
              ];
            }
            $data_restore = [
              'error' => 0,
              'list_vps' => $data_vps,
            ];
          }
          else {
            $data_restore = [
              'error' => 4,
              'status' => 'Số dư tài khoản của quý khách không đủ để thực hiện giao dịch này.',
            ];
          }
      }
      elseif ( $data['type_product'] == 'hosting' ) {
          $list_hosting = $this->order_hosting($data);
          if ( !empty($hosting) ) {
            $data_hosting = [];
            foreach ($list_hosting as $key => $hosting) {
              $url_control_panel = '';
              if (!empty($hosting->server_hosting_id)) {
                  $server_hosting = $hosting->server_hosting;
                  $url_control_panel = "https://" . $server_hosting->host . ':' . $server_hosting->port;
                  // dd($url);
              } else {
                  $url_control_panel = redirect('https://daportal.cloudzone.vn:2222/');
              }
              $data_hosting = [
                'hosting_id' => $hosting->id,
                'domain' => $hosting->domain,
                'user' => $hosting->user,
                'password' => $hosting->password,
                'next_due_date' => $hosting->next_due_date,
                'date_create' => $hosting->date_create,
                'status_hosting' => $hosting->status_hosting,
                'url_control_panel' => $url_control_panel,
              ];
            }
            $data_restore = [
              'error' => 0,
              'hosting' => $data_hosting,
            ];
          }
          else {
            $data_restore = [
              'error' => 4,
              'status' => 'Số dư tài khoản của quý khách không đủ để thực hiện giao dịch này.',
            ];
          }
      }
      return $data_restore;
  }

  public function order_vps($data)
  {
      $agency = $this->agency->where('user_api', $data['user'])->where('password_api', $data['password'])->first();
      $product = $this->product->find($data['product_id']);
      if ( !empty($product->meta_product->product_special) ) {
        $add_on = !empty($data['addon'])? 1 : '';
      }
      $sub_total = $product->pricing[$data['billing_cycle']];
      $total = $product->pricing[$data['billing_cycle']] * $data['quantity'];

      if (!empty($add_on)) {
          $add_on_products = $this->get_addon_product_private($agency->user_id);
          $pricing_addon = 0;
          if ( $data['addon_cpu'] == 0  && $data['addon_ram'] == 0 && $data['addon_disk'] ==0 ) {
            $add_on = 0;
          }
          foreach ($add_on_products as $key => $add_on_product) {
              if (!empty($add_on_product->meta_product->type_addon)) {
                  if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                    $sub_total += $data['addon_cpu'] * $add_on_product->pricing[$data['billing_cycle']];
                    $pricing_addon += $data['addon_cpu'] * $add_on_product->pricing[$data['billing_cycle']] * $data['quantity'];
                  }
                  if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                    $sub_total += $data['addon_ram'] * $add_on_product->pricing[$data['billing_cycle']];
                    $pricing_addon += $data['addon_ram'] * $add_on_product->pricing[$data['addon_ram']] * $data['quantity'];
                  }
                  if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                    $sub_total += ($data['addon_disk']/10) * $add_on_product->pricing[$data['billing_cycle']];
                    $pricing_addon += $addon_vps->disk * ($add_on_product->pricing[$data['billing_cycle']] / 10) * $data['quantity'];
                  }
              }
          }
          $total += $pricing_addon;
      }
      $check_credit = $this->check_credit($agency->user_id, $total);
      if ( $check_credit == false ) {
          return false;
      }
      $credit = $this->credit->where('user_id', $agency->user_id)->first();
      if (!isset($credit)) {
          return false;
      }
      $credit->value -= $total;
      $credit->save();
      // tao order
      $data_order = [
          'user_id' => $agency->user_id,
          'total' => $total,
          'status' => 'Finish',
          'description' => !empty($data['description'])? $data['description'] : '',
          'verify' => true,
      ];
      $create_order = $this->order->create($data_order);

      $billing = [
          'monthly' => '1 Month',
          'twomonthly' => '2 Month',
          'quarterly' => '3 Month',
          'semi_annually' => '6 Month',
          'annually' => '1 Year',
          'biennially' => '2 Year',
          'triennially' => '3 Year'
      ];
      $date = date('Y-m-d');
      $due_date = date('Y-m-d', strtotime( $date . $billing[$data['billing_cycle']] ));
      $data_order_detail = [
          'order_id' => $create_order->id,
          'type' => $product->type_product,
          'due_date' => $due_date,
          'description' => !empty($data['description'])? $data['description'] : '',
          'payment_method' => 1,
          'status' => 'paid',
          'sub_total' => $sub_total,
          'quantity' => $data['quantity'],
          'user_id' => $agency->user_id,
          'paid_date' => date('Y-m-d'),
          'addon_id' => !empty($data['addon'])? '1' : '0',
      ];
      $detail_order = $this->detail_order->create($data_order_detail);
      $user = $this->user->find($agency->user_id);
      $data_history = [
          'user_id' => $user->id,
          'ma_gd' => 'GDODV' . strtoupper(substr(sha1(time()), 34, 39)),
          'discription' => 'Hóa đơn số ' . $detail_order->id,
          'type_gd' => '2',
          'method_gd' => 'invoice',
          'date_gd' => date('Y-m-d'),
          'money'=> $total,
          'status' => 'Active',
          'method_gd_invoice' => 'credit',
          'detail_order_id' => $detail_order->id,
      ];
      $this->history_pay->create($data_history);
      $qtt = $data['quantity'];
      $list_vps = [];
      for ($i=0; $i < $qtt; $i++) {
          $data_create_vps = [
              'detail_order_id' => $detail_order->id,
              'user_id' => $user->id,
              'product_id' => $data['product_id'],
              'next_due_date' => $due_date,
              'ip' => !empty($data['ip'])? $data['ip'] : '',
              'os' => $data['os_id'],
              'billing_cycle' => $data['billing_cycle'],
              'status' => 'Active',
              'date_create' => date('Y-m-d'),
              'paid' => 'paid',
              'addon_id' => !empty($data['addon'])? 1 : '0',
              'security' => !empty($data['check_security']) ? true : false,
              'type_vps' => 'vps',
          ];
          // return $data_create_vps;
          $create_vps = $this->vps->create($data_create_vps);
          $list_vps[] = $create_vps;
          if (!empty($data['addon'])) {
              if ($data['addon_ram'] > 0 || $data['addon_cpu'] > 0 || $data['addon_disk'] > 0  ) {
                  $data_vps_config = [
                      'vps_id' => $create_vps->id,
                      'ip' => '0',
                      'cpu' => !empty($data['addon_cpu']) ? $data['addon_cpu'] : 0,
                      'ram' => !empty($data['addon_ram']) ? $data['addon_ram'] : 0,
                      'disk' => !empty($data['addon_disk']) ? $data['addon_disk'] : 0,
                  ];
                  $this->vps_config->create($data_vps_config);
              }
          }
      }
      $reques_vcenter = $this->dashboard->createVpsWithApi($list_vps, $due_date); //request đên Vcenter
      // $reques_vcenter = true; //request đên Vcenter
      $create_order->status = 'Finish';
      $create_order->save();
      try {
        $this->send_mail_finish('vps',$create_order->id, $detail_order ,$list_vps);
        $billings = config('billing');
        // Tạo log
        if ($product->group_product->private == 0) {
          $data_log = [
            'user_id' => $user->id,
            'action' => 'đại lý đặt hàng',
            'model' => 'User/Order',
            'description' => $data['quantity'] . ' VPS <a target="_blank" href="/admin/products/edit/' . $product->id . '">' . $product->name . '</a> ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']] . ' qua API',
            'description_user' => $data['quantity'] . '  ' . $product->name . '  ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']] . ' qua API',
          ];
        } else {
          $data_log = [
            'user_id' => $user->id,
            'action' => 'đại lý đặt hàng',
            'model' => 'User/Order',
            'description' => $data['quantity'] . ' VPS <a target="_blank" target="_blank" href="/admin/product_privates/edit/' . $product->id . '">' . $product->name . '</a> ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']] . ' qua API',
            'description_user' => $data['quantity'] . '  ' . $product->name . '  ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']] . ' qua API',
          ];
        }
        $this->log_activity->create($data_log);
      }
      catch (\Exception $e) {
          report($e);
          if($create_vps) {
              return $reques_vcenter;
          } else {
              return $reques_vcenter;
          }
      }
      return $reques_vcenter;
  }

  public function order_nat_vps($data)
  {
      $agency = $this->agency->where('user_api', $data['user'])->where('password_api', $data['password'])->first();
      $product = $this->product->find($data['product_id']);
      if ( !empty($product->meta_product->product_special) ) {
        $add_on = !empty($data['addon'])? 1 : '';
      }
      $sub_total = $product->pricing[$data['billing_cycle']];
      $total = $product->pricing[$data['billing_cycle']] * $data['quantity'];

      if (!empty($add_on)) {
          $add_on_products = $this->get_addon_product_private($agency->user_id);
          $pricing_addon = 0;
          if ( $data['addon_cpu'] == 0  && $data['addon_ram'] == 0 && $data['addon_disk'] ==0 ) {
            $add_on = 0;
          }
          foreach ($add_on_products as $key => $add_on_product) {
              if (!empty($add_on_product->meta_product->type_addon)) {
                  if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                    $sub_total += $data['addon_cpu'] * $add_on_product->pricing[$data['billing_cycle']];
                    $pricing_addon += $data['addon_cpu'] * $add_on_product->pricing[$data['billing_cycle']] * $data['quantity'];
                  }
                  if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                    $sub_total += $data['addon_ram'] * $add_on_product->pricing[$data['billing_cycle']];
                    $pricing_addon += $data['addon_ram'] * $add_on_product->pricing[$data['addon_ram']] * $data['quantity'];
                  }
                  if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                    $sub_total += ($data['addon_disk']/10) * $add_on_product->pricing[$data['billing_cycle']];
                    $pricing_addon += $addon_vps->disk * ($add_on_product->pricing[$data['billing_cycle']] / 10) * $data['quantity'];
                  }
              }
          }
          $total += $pricing_addon;
      }
      $check_credit = $this->check_credit($agency->user_id, $total);
      if ( $check_credit == false ) {
          return false;
      }
      $credit = $this->credit->where('user_id', $agency->user_id)->first();
      if (!isset($credit)) {
          return false;
      }
      $credit->value -= $total;
      $credit->save();
      // tao order
      $data_order = [
          'user_id' => $agency->user_id,
          'total' => $total,
          'status' => 'Finish',
          'description' => !empty($data['description'])? $data['description'] : '',
          'verify' => true,
      ];
      $create_order = $this->order->create($data_order);

      $billing = [
          'monthly' => '1 Month',
          'twomonthly' => '2 Month',
          'quarterly' => '3 Month',
          'semi_annually' => '6 Month',
          'annually' => '1 Year',
          'biennially' => '2 Year',
          'triennially' => '3 Year'
      ];
      $date = date('Y-m-d');
      $due_date = date('Y-m-d', strtotime( $date . $billing[$data['billing_cycle']] ));
      $data_order_detail = [
          'order_id' => $create_order->id,
          'type' => $product->type_product,
          'due_date' => $due_date,
          'description' => !empty($data['description'])? $data['description'] : '',
          'payment_method' => 1,
          'status' => 'paid',
          'sub_total' => $sub_total,
          'quantity' => $data['quantity'],
          'user_id' => $agency->user_id,
          'paid_date' => date('Y-m-d'),
          'addon_id' => !empty($data['addon'])? '1' : '0',
      ];
      $detail_order = $this->detail_order->create($data_order_detail);
      $user = $this->user->find($agency->user_id);
      $data_history = [
          'user_id' => $user->id,
          'ma_gd' => 'GDONV' . strtoupper(substr(sha1(time()), 34, 39)),
          'discription' => 'Hóa đơn số ' . $detail_order->id,
          'type_gd' => '2',
          'method_gd' => 'invoice',
          'date_gd' => date('Y-m-d'),
          'money'=> $total,
          'status' => 'Active',
          'method_gd_invoice' => 'credit',
          'detail_order_id' => $detail_order->id,
      ];
      $this->history_pay->create($data_history);
      $qtt = $data['quantity'];
      $list_vps = [];
      for ($i=0; $i < $qtt; $i++) {
          $data_create_vps = [
              'detail_order_id' => $detail_order->id,
              'user_id' => $user->id,
              'product_id' => $data['product_id'],
              'next_due_date' => $due_date,
              'ip' => !empty($data['ip'])? $data['ip'] : '',
              'os' => $data['os_id'],
              'billing_cycle' => $data['billing_cycle'],
              'status' => 'Active',
              'date_create' => date('Y-m-d'),
              'paid' => 'paid',
              'addon_id' => !empty($data['addon'])? 1 : '0',
              'security' => !empty($data['check_security']) ? true : false,
              'type_vps' => 'nat_vps',
          ];
          // return $data_create_vps;
          $create_vps = $this->vps->create($data_create_vps);
          $list_vps[] = $create_vps;
          if (!empty($data['addon'])) {
              if ($data['addon_ram'] > 0 || $data['addon_cpu'] > 0 || $data['addon_disk'] > 0  ) {
                  $data_vps_config = [
                      'vps_id' => $create_vps->id,
                      'ip' => '0',
                      'cpu' => !empty($data['addon_cpu']) ? $data['addon_cpu'] : 0,
                      'ram' => !empty($data['addon_ram']) ? $data['addon_ram'] : 0,
                      'disk' => !empty($data['addon_disk']) ? $data['addon_disk'] : 0,
                  ];
                  $this->vps_config->create($data_vps_config);
              }
          }
      }
      $reques_vcenter = $this->dashboard->createNatVPSWithApi($list_vps, $due_date); //request đên Vcenter
      // $reques_vcenter = true; //request đên Vcenter
      $create_order->status = 'Finish';
      $create_order->save();
      try {
        $this->send_mail_finish('vps',$create_order->id, $detail_order ,$list_vps);
        $billings = config('billing');
        // Tạo log
        if ($product->group_product->private == 0) {
          $data_log = [
            'user_id' => $user->id,
            'action' => 'đại lý đặt hàng',
            'model' => 'User/Order',
            'description' => $data['quantity'] . ' VPS <a target="_blank" href="/admin/products/edit/' . $product->id . '">' . $product->name . '</a> ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']] . ' qua API',
            'description_user' => $data['quantity'] . '  ' . $product->name . '  ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']] . ' qua API',
          ];
        } else {
          $data_log = [
            'user_id' => $user->id,
            'action' => 'đại lý đặt hàng',
            'model' => 'User/Order',
            'description' => $data['quantity'] . ' VPS <a target="_blank" target="_blank" href="/admin/product_privates/edit/' . $product->id . '">' . $product->name . '</a> ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']] . ' qua API',
            'description_user' => $data['quantity'] . '  ' . $product->name . '  ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']] . ' qua API',
          ];
        }
        $this->log_activity->create($data_log);
      }
      catch (\Exception $e) {
          report($e);
          if($create_vps) {
              return $reques_vcenter;
          } else {
              return $reques_vcenter;
          }
      }
      return $reques_vcenter;
  }

  public function order_hosting($data)
  {
      $agency = $this->agency->where('user_api', $data['user'])->where('password_api', $data['password'])->first();
      $product = $this->product->find($data['product_id']);

      $total = $product->pricing[$data['billing_cycle']];

      $check_credit = $this->check_credit($agency->user_id, $total);
      if ( $check_credit == false ) {
          return false;
      }
      $credit = $this->credit->where('user_id', $agency->user_id)->first();
      if (!isset($credit)) {
          return false;
      }
      $credit->value -= $total;
      $credit->save();
      // tao order
      $data_order = [
          'user_id' => $agency->user_id,
          'total' => $total,
          'status' => 'Finish',
          'description' => !empty($data['description'])? $data['description'] : '',
          'verify' => true,
      ];
      $create_order = $this->order->create($data_order);

      $billing = [
          'monthly' => '1 Month',
          'twomonthly' => '2 Month',
          'quarterly' => '3 Month',
          'semi_annually' => '6 Month',
          'annually' => '1 Year',
          'biennially' => '2 Year',
          'triennially' => '3 Year'
      ];
      $date = date('Y-m-d');
      if ($data['billing_cycle'] == 'one_time_pay') {
        $due_date = '2099-12-31';
      } else {
        $due_date = date('Y-m-d', strtotime( $date . $billing[$data['billing_cycle']] ));
      }
      $data_order_detail = [
          'order_id' => $create_order->id,
          'type' => $product->type_product,
          'due_date' => $due_date,
          'description' => !empty($data['description'])? $data['description'] : '',
          'payment_method' => 1,
          'status' => 'paid',
          'sub_total' => $total,
          'quantity' => 1,
          'user_id' => $agency->user_id,
          'paid_date' => date('Y-m-d'),
          'addon_id' => !empty($data['addon'])? '1' : '0',
      ];
      $detail_order = $this->detail_order->create($data_order_detail);
      $user = $this->user->find($agency->user_id);
      $data_history = [
          'user_id' => $user->id,
          'ma_gd' => 'GDODH' . strtoupper(substr(sha1(time()), 34, 39)),
          'discription' => 'Hóa đơn số ' . $detail_order->id,
          'type_gd' => '2',
          'method_gd' => 'invoice',
          'date_gd' => date('Y-m-d'),
          'money'=> $total,
          'status' => 'Active',
          'detail_order_id' => $detail_order->id,
          'method_gd_invoice' => 'credit',
      ];
      $this->history_pay->create($data_history);
      // Tạo hosting
      $domain_hosting = strtolower($data['domain']);
      $user_hosting = str_replace('.', '', $domain_hosting); //uthien.com -> uthiencom
      $check_user_hosting = substr($user_hosting, 0, 1);
      if ( is_numeric($check_user_hosting) ) {
        $user_hosting = 'a' . substr($user_hosting, 0, 6); //123uthiencom -> a123uthienco
      }
      else {
        $user_hosting = substr($user_hosting, 0, 7); //uthiencom -> uthienco
      }
      // Tao order Hosting
      $data_hosting = [
          'detail_order_id' => $detail_order->id,
          'user_id' => $user->id,
          'product_id' => $data['product_id'],
          'next_due_date' => $due_date,
          'domain' => !empty($data['domain'])? $data['domain'] : '',
          'billing_cycle' => $data['billing_cycle'],
          'status' => 'Active',
          'user' => $user_hosting,
          'password' => $this->rand_string(),
          'paid' => 'paid',
          'server_hosting_id' => !empty($data['server_hosting'])? $data['server_hosting'] : 0,
          'location' => 'vn',
          'date_create' => date('Y-m-d'),
      ];
      $create_hosting = $this->hosting->create($data_hosting);
      if ($data['billing_cycle'] == 'one_time_pay') {
        $request_da = $this->da->orderCreateAccountWithApi($create_hosting, 'one_time_pay');
      } else {
        $request_da = $this->da->orderCreateAccountWithApi($create_hosting, $due_date);
      }
      $list_hosting[] = $create_hosting;
      // $reques_vcenter = true; //request đên Vcenter
      $create_order->status = 'Finish';
      $create_order->save();
      try {
        $this->send_mail_finish('hosting',$create_order->id, $detail_order ,$list_hosting);
        $billings = config('billing');
        // Tạo log
        if ($product->group_product->private == 0) {
          $data_log = [
            'user_id' => $user->id,
            'action' => 'đại lý đặt hàng',
            'model' => 'User/Order',
            'description' => '1 Hosting <a target="_blank" href="/admin/products/edit/' . $product->id . '">' . $product->name . '</a> ' . $billings[$data['billing_cycle']] . ' qua API',
            'description_user' => '1  ' . $product->name  . ' ' . $billings[$data['billing_cycle']] . ' qua API',
          ];
        } else {
          $data_log = [
            'user_id' => $user->id,
            'action' => 'đại lý đặt hàng',
            'model' => 'User/Order',
            'description' =>  '1 Hosting <a target="_blank" target="_blank" href="/admin/product_privates/edit/' . $product->id . '">' . $product->name . '</a> ' . $billings[$data['billing_cycle']] . ' qua API',
            'description_user' => '1 ' . $product->name . '  ' . $billings[$data['billing_cycle']] . ' qua API',
          ];
        }
        $this->log_activity->create($data_log);
      }
      catch (\Exception $e) {
          report($e);
          return $request_da;
      }
      return $request_da;
  }

  public function check_credit($user_id , $total)
  {
      $credit = $this->credit->where('user_id', $user_id)->first();
      if (!isset($credit)) {
          return false;
      }
      // dd($total);
      if ($credit->value >= $total) {
          return true;
      } else {
          return false;
      }

  }

  public function send_mail_finish($type, $order_id , $detail_order, $list_sevices)
  {
      $billings = config('billing');
      $order = $this->order->find($order_id);
      $product = $this->product->find($list_sevices[0]->product_id);
      $email = $this->email->find($product->meta_product->email_id);
      $url = url('');
      $url = str_replace(['http://','https://'], ['', ''], $url);
      $add_on =  '';
      if (!empty($detail_order->addon_id)) {
          $add_on = $this->product->find($list_sevices[0]->addon_id);
      }
      if ($type == 'vps') {
          foreach ($list_sevices as $key => $services) {
              $user = $this->user->find($services->user_id);
              $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
              $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], '', $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, $services->os, '', $url];
              $content = str_replace($search, $replace, $email->meta_email->content);

              $this->user_email = $this->user->find($services->user_id)->email;
              $this->subject = $email->meta_email->subject;

              $data = [
                  'content' => $content,
                  'user_name' => $user->name,
                  'user_id' => $user->id,
                  'user_email' => $user->email,
                  'user_addpress' => $user->user_meta->address,
                  'user_phone' => $user->user_meta->user_phone,
                  'order_id' => $order->id,
                  'product_name' => $product->name,
                  'domain' => '',
                  'ip' => $services->ip,
                  'billing_cycle' => $services->billing_cycle,
                  'next_due_date' => $services->next_due_date,
                  'order_created_at' => $order->created_at,
                  'services_username' => $services->user,
                  'services_password' => $services->password,
                  'sub_total' => $detail_order->sub_total,
                  'total' => $order->total,
                  'qtt' => $detail_order->quantity,
                  'os' => $services->os,
                  'token' => '',
              ];

              $mail = Mail::send('users.mails.orders', $data, function($message){
                  $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                  $message->to($this->user_email)->subject($this->subject);
              });
          }
      }
      elseif ($type == 'hosting') {
          foreach ($list_sevices as $key => $services) {
                if (!empty($services->server_hosting_id)) {
                   $link_control_panel = '<a href="https://' . $services->server_hosting->host . ':' . $services->server_hosting->port . '" target="_blank" >https://'. $services->server_hosting->host . ':' . $services->server_hosting->port . '</a>' ;
                } else {
                   $link_control_panel = '<a href="https://daportal.cloudzone.vn:2222/" target="_blank">https://daportal.cloudzone.vn:2222/</a>';
                }
                $user = $this->user->find($services->user_id);
                $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}', '{$url_control_panel}'];
                $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], '', $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url, $link_control_panel];
                $content = str_replace($search, $replace, $email->meta_email->content);

                $this->user_email = $this->user->find($services->user_id)->email;
                $this->subject = $email->meta_email->subject;

                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'user_id' => $user->id,
                    'user_email' => $user->email,
                    'user_addpress' => $user->user_meta->address,
                    'user_phone' => $user->user_meta->user_phone,
                    'order_id' => $order->id,
                    'product_name' => $product->name,
                    'domain' => '',
                    'ip' => $services->ip,
                    'billing_cycle' => $services->billing_cycle,
                    'next_due_date' => $services->next_due_date,
                    'order_created_at' => $order->created_at,
                    'services_username' => $services->user,
                    'services_password' => $services->password,
                    'sub_total' => $detail_order->sub_total,
                    'total' => $order->total,
                    'qtt' => $detail_order->quantity,
                    'os' => $services->os,
                    'token' => '',
                ];

                $mail = Mail::send('users.mails.orders', $data, function($message){
                    $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                    $message->to($this->user_email)->subject($this->subject);
                });
          }
      }
      elseif ($type == 'email') {
          $services = $list_sevices;
          $user = $this->user->find($services->user_id);
          $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}',];
          $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], '', $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url, ];
          $content = str_replace($search, $replace, $email->meta_email->content);

          $this->user_email = $this->user->find($services->user_id)->email;
          $this->subject = $email->meta_email->subject;

          $data = [
                'content' => $content,
                'user_name' => $user->name,
                'user_id' => $user->id,
                'user_email' => $user->email,
                'user_addpress' => $user->user_meta->address,
                'user_phone' => $user->user_meta->user_phone,
                'order_id' => $order->id,
                'product_name' => $product->name,
                'domain' => '',
                'ip' => $services->ip,
                'billing_cycle' => $services->billing_cycle,
                'next_due_date' => $services->next_due_date,
                'order_created_at' => $order->created_at,
                'services_username' => $services->user,
                'services_password' => $services->password,
                'sub_total' => $detail_order->sub_total,
                'total' => $order->total,
                'qtt' => $detail_order->quantity,
                'os' => $services->os,
                'token' => '',
          ];

          $mail = Mail::send('users.mails.orders', $data, function($message){
            $message->from('support@cloudzone.vn', 'Cloudzone Portal');
            $message->to($this->user_email)->subject($this->subject);
          });
      }
  }

  public function get_addon_product_private($user_id)
  {
      $products = [];
      $user = $this->user->find($user_id);
      // dd($user);
      if ($user->group_user_id != 0) {
          $group_user = $user->group_user;
          $group_products = $group_user->group_products;
          foreach ($group_products as $key => $group_product) {
              if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                  $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
              }
          }
      } else {
          $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->get();
          foreach ($group_products as $key => $group_product) {
              if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                  $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
              }
          }
      }
      return $products;
  }

  public function expire($data)
  {
      $data_restore = [];
      $agency = $this->agency->where('user_api', $data['user'])->where('password_api', $data['password'])->first();
      if ( $data['type_product'] == 'vps' ) {
          $list_vps = $this->expire_vps($data);
          if ( !empty($list_vps) ) {
            $data_vps = [];
            foreach ($list_vps as $key => $vps) {
              $data_vps[] = [
                'portal_vps_id' => $vps->id,
                'ip' => $vps->ip,
                'next_due_date' => date('d-m-Y', strtotime($vps->next_due_date)),
                'status_vps' => $vps->status_vps,
              ];
            }
            $data_restore = [
              'error' => 0,
              'list_vps' => $data_vps,
              'credit' => $agency->user->credit->value,
            ];
          }
          else {
            $data_restore = [
              'error' => 4,
              'status' => 'Số dư tài khoản của quý khách không đủ để thực hiện giao dịch này.',
            ];
          }
      }
      elseif ( $data['type_product'] == 'nat_vps' ) {
          $list_vps = $this->expire_nat_vps($data);
          if ( !empty($list_vps) ) {
            $data_vps = [];
            foreach ($list_vps as $key => $vps) {
              $data_vps[] = [
                'portal_vps_id' => $vps->id,
                'ip' => $vps->ip,
                'next_due_date' => date('d-m-Y', strtotime($vps->next_due_date)),
                'status_vps' => $vps->status_vps,
              ];
            }
            $data_restore = [
              'error' => 0,
              'list_vps' => $data_vps,
              'credit' => $agency->user->credit->value,
            ];
          }
          else {
            $data_restore = [
              'error' => 4,
              'status' => 'Số dư tài khoản của quý khách không đủ để thực hiện giao dịch này.',
            ];
          }
      }
      elseif ( $data['type_product'] == 'hosting' ) {
          $list_hosting = $this->expire_hosting($data);
          // return $list_hosting;
          $data_hosting = [];
          if ( !empty($list_hosting) ) {
            foreach ($list_hosting as $key => $hosting) {
              $data_hosting[] = [
                'portal_hosting_id' => $hosting->id,
                'domain' => $hosting->domain,
                'next_due_date' => date('d-m-Y', strtotime($hosting->next_due_date)),
                'status_hosting' => $hosting->status_hosting,
              ];
            }
            $data_restore = [
              'error' => 0,
              'hosting' => $data_hosting,
              'credit' => $agency->user->credit->value,
            ];
          }
          else {
            $data_restore = [
              'error' => 4,
              'status' => 'Số dư tài khoản của quý khách không đủ để thực hiện giao dịch này.',
            ];
          }
      }
      return $data_restore;
  }

  public function expire_vps($data)
  {
      $agency = $this->agency->where('user_api', $data['user'])->where('password_api', $data['password'])->first();
      $user = $this->user->find($agency->user_id);
      $billing_cycle = $data['billing_cycle'];
      $total = 0;
      foreach ($data['list_vps'] as $key => $vps_id) {
          $vps = $this->vps->find($vps_id['portal_vps_id']);
          $product = $this->product->find($vps->product_id);
          $invoiced = $vps->detail_order;
          if (!empty($vps)) {
            if ( !empty($vps->price_override) ) {
                if ( $vps->billing_cycle == $billing_cycle ) {
                  $total += $vps->price_override;
                } else {
                  $total += $product->pricing[$billing_cycle];
                  if (!empty($vps->vps_config)) {
                      $addon_vps = $vps->vps_config;
                      $add_on_products = $this->get_addon_product_private($user->id);
                      $pricing_addon = 0;
                      foreach ($add_on_products as $key => $add_on_product) {
                          if (!empty($add_on_product->meta_product->type_addon)) {
                              if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                              }
                              if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                              }
                              if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                              }
                          }
                      }
                      $total += $pricing_addon;
                  }
                }
            }
            else {
                $total += $product->pricing[$billing_cycle];
                if (!empty($vps->vps_config)) {
                  $addon_vps = $vps->vps_config;
                  $add_on_products = $this->get_addon_product_private($user->id);
                  $pricing_addon = 0;
                  foreach ($add_on_products as $key => $add_on_product) {
                    if (!empty($add_on_product->meta_product->type_addon)) {
                      if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                        $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                        $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                        $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                      }
                    }
                  }
                  $total += $pricing_addon;
                }
            }
          }
          else {
              return false;
          }
      }
      $check_credit = $this->check_credit($agency->user_id, $total);
      if ( $check_credit == false ) {
          return false;
      }
      $credit = $this->credit->where('user_id', $agency->user_id)->first();
      if (!isset($credit)) {
          return false;
      }
      $credit->value -= $total;
      $credit->save();
      // create order
      $data_order = [
          'user_id' => $user->id,
          'total' => $total,
          'status' => 'Finish',
          'description' => 'expired vps',
          'type' => 'expired',
          'makh' => '',
      ];
      $order = $this->order->create($data_order);
      //create order detail
      $date = date('Y-m-d');
      $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
      $data_order_detail = [
          'order_id' => $order->id,
          'type' => 'VPS',
          'due_date' => $date,
          'paid_date' => date('Y-m-d'),
          'description' => 'Gia hạn VPS',
          'status' => 'paid',
          'sub_total' => $total,
          'quantity' => count($data['list_vps']),
          'user_id' => $user->id,
          'paid_date' => date('Y-m-d'),
          'addon_id' => '0',
          'payment_method' => 1,
      ];
      $detail_order = $this->detail_order->create($data_order_detail);
      $data_history = [
          'user_id' => $user->id,
          'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
          'discription' => 'Hóa đơn số ' . $detail_order->id,
          'type_gd' => '3',
          'method_gd' => 'invoice',
          'date_gd' => date('Y-m-d'),
          'money'=> $total,
          'status' => 'Active',
          'method_gd_invoice' => 'credit',
          'detail_order_id' => $detail_order->id,
      ];
      $this->history_pay->create($data_history);
      // invoice
      $billing = [
          'monthly' => '1 Month',
          'twomonthly' => '2 Month',
          'quarterly' => '3 Month',
          'semi_annually' => '6 Month',
          'annually' => '1 Year',
          'biennially' => '2 Year',
          'triennially' => '3 Year'
      ];
      $data_restore = [];
      foreach ($data['list_vps'] as $key => $vps_id) {
          $vps = $this->vps->find($vps_id['portal_vps_id']);
          // Tạo lưu trữ khi gia hạn
          $data_order_expired = [
              'detail_order_id' => $detail_order->id,
              'expired_id' => $vps->id,
              'billing_cycle' => $billing_cycle,
              'type' => 'vps',
          ];
          $order_expired_vps = $this->order_expired->create($data_order_expired);
          $date_star = $vps->next_due_date;
          $due_date = date('Y-m-d', strtotime( $date_star . $billing[$billing_cycle] ));

          if ( !empty($vps->price_override) ) {
              if ( $vps->billing_cycle ==  $billing_cycle ) {
                  $vps->next_due_date = $due_date;
                  $vps->save();
              }
          } else {
              $vps->price_override = 0;
              $vps->next_due_date = $due_date;
              $vps->billing_cycle = $billing_cycle;
              $vps->save();
          }
          $config_billing_DashBoard = config('billingDashBoard');
          $data_expired = [
              'mob' => 'renew',
              'package' => (int) $detail_order->sub_total,
              'leased_time' => $config_billing_DashBoard[$billing_cycle],
              'end_date' => $due_date,
              'type' => 0,
          ];
          $reques_vcenter = $this->dashboard->expired_vps($data_expired, $vps->vm_id); //request đên Vcenter

          $data_restore[] = $vps;

          try {
              $billings = config('billing');
              // ghi log
              $data_log = [
                'user_id' => $user->id,
                'action' => 'đại lý API',
                'model' => 'Admin/Order',
                'description' => '  hoàn thành gia hạn VPS <a href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                'service' =>  $vps->ip,
              ];
              $this->log_activity->create($data_log);
              // gui mail
              $this->send_mail_finish_expired('vps',$order, $detail_order ,$order_expired_vps);
          } catch (\Exception $e) {
              report($e);
              return true;
          }
      }
      return $data_restore;
  }

  public function expire_nat_vps($data)
  {
      $agency = $this->agency->where('user_api', $data['user'])->where('password_api', $data['password'])->first();
      $user = $this->user->find($agency->user_id);
      $billing_cycle = $data['billing_cycle'];
      $total = 0;
      foreach ($data['list_vps'] as $key => $vps_id) {
          $vps = $this->vps->find($vps_id['portal_vps_id']);
          $product = $this->product->find($vps->product_id);
          $invoiced = $vps->detail_order;
          if (!empty($vps)) {
            if ( !empty($vps->price_override) ) {
              if ( $vps->billing_cycle == $billing_cycle ) {
                $total += $vps->price_override;
              } else {
                $total += $product->pricing[$billing_cycle];
                if (!empty($vps->vps_config)) {
                    $addon_vps = $vps->vps_config;
                    $add_on_products = $this->get_addon_product_private($user->id);
                    $pricing_addon = 0;
                    foreach ($add_on_products as $key => $add_on_product) {
                        if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                            }
                        }
                    }
                    $total += $pricing_addon;
                }
              }
            }
            else {
                $total += $product->pricing[$billing_cycle];
                if (!empty($vps->vps_config)) {
                  $addon_vps = $vps->vps_config;
                  $add_on_products = $this->get_addon_product_private($user->id);
                  $pricing_addon = 0;
                  foreach ($add_on_products as $key => $add_on_product) {
                    if (!empty($add_on_product->meta_product->type_addon)) {
                      if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                        $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                        $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                        $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                      }
                    }
                  }
                  $total += $pricing_addon;
                }
            }
          }
          else {
              return false;
          }
      }
      $check_credit = $this->check_credit($agency->user_id, $total);
      if ( $check_credit == false ) {
          return false;
      }
      $credit = $this->credit->where('user_id', $agency->user_id)->first();
      if (!isset($credit)) {
          return false;
      }
      $credit->value -= $total;
      $credit->save();
      // create order
      $data_order = [
          'user_id' => $user->id,
          'total' => $total,
          'status' => 'Finish',
          'description' => 'expired vps',
          'type' => 'expired',
          'makh' => '',
      ];
      $order = $this->order->create($data_order);
      //create order detail
      $date = date('Y-m-d');
      $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
      $data_order_detail = [
          'order_id' => $order->id,
          'type' => 'VPS',
          'due_date' => $date,
          'paid_date' => date('Y-m-d'),
          'description' => 'Gia hạn VPS',
          'status' => 'paid',
          'sub_total' => $total,
          'quantity' => count($data['list_vps']),
          'user_id' => $user->id,
          'paid_date' => date('Y-m-d'),
          'addon_id' => '0',
          'payment_method' => 1,
      ];
      $detail_order = $this->detail_order->create($data_order_detail);
      $data_history = [
          'user_id' => $user->id,
          'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
          'discription' => 'Hóa đơn số ' . $detail_order->id,
          'type_gd' => '3',
          'method_gd' => 'invoice',
          'date_gd' => date('Y-m-d'),
          'money'=> $total,
          'status' => 'Active',
          'method_gd_invoice' => 'credit',
          'detail_order_id' => $detail_order->id,
      ];
      $this->history_pay->create($data_history);
      // invoice
      $billing = [
          'monthly' => '1 Month',
          'twomonthly' => '2 Month',
          'quarterly' => '3 Month',
          'semi_annually' => '6 Month',
          'annually' => '1 Year',
          'biennially' => '2 Year',
          'triennially' => '3 Year'
      ];
      $data_restore = [];
      foreach ($data['list_vps'] as $key => $vps_id) {
          $vps = $this->vps->find($vps_id['portal_vps_id']);
          // Tạo lưu trữ khi gia hạn
          $data_order_expired = [
              'detail_order_id' => $detail_order->id,
              'expired_id' => $vps->id,
              'billing_cycle' => $billing_cycle,
              'type' => 'vps',
          ];
          $order_expired_vps = $this->order_expired->create($data_order_expired);
          $date_star = $vps->next_due_date;
          $due_date = date('Y-m-d', strtotime( $date_star . $billing[$billing_cycle] ));

          if ( !empty($vps->price_override) ) {
              if ( $vps->billing_cycle ==  $billing_cycle ) {
                  $vps->next_due_date = $due_date;
                  $vps->save();
              }
          } else {
              $vps->price_override = 0;
              $vps->next_due_date = $due_date;
              $vps->billing_cycle = $billing_cycle;
              $vps->save();
          }
          $config_billing_DashBoard = config('billingDashBoard');
          $data_expired = [
              'mob' => 'renew',
              'package' => (int) $detail_order->sub_total,
              'leased_time' => $config_billing_DashBoard[$billing_cycle],
              'end_date' => $due_date,
              'type' => 5,
          ];
          $reques_vcenter = $this->dashboard->expired_vps($data_expired, $vps->vm_id); //request đên Vcenter

          $data_restore[] = $vps;

          try {
              $billings = config('billing');
              // ghi log
              $data_log = [
                'user_id' => $user->id,
                'action' => 'đại lý API',
                'model' => 'Admin/Order',
                'description' => '  hoàn thành gia hạn NAT VPS <a href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                'service' =>  $vps->ip,
              ];
              $this->log_activity->create($data_log);
              // gui mail
              $this->send_mail_finish_expired('vps',$order, $detail_order ,$order_expired_vps);
          } catch (\Exception $e) {
              report($e);
              return true;
          }
      }
      return $data_restore;
  }

  public function expire_hosting($data)
  {
      $agency = $this->agency->where('user_api', $data['user'])->where('password_api', $data['password'])->first();
      $user = $this->user->find($agency->user_id);
      $billing_cycle = $data['billing_cycle'];
      $total = 0;
      foreach ($data['list_hosting'] as $key => $hosting_id) {
          $hosting = $this->hosting->find($hosting_id['portal_hosting_id']);
          $product = $this->product->find($hosting->product_id);
          $invoiced = $hosting->detail_order;
          if (!empty($hosting)) {
              if ( !empty($hosting->price_override) ) {
                  if ( $vps->billing_cycle == $billing_cycle ) {
                    $total += $hosting->price_override;
                  }
                  else {
                    $total += $product->pricing[$billing_cycle];
                  }
              }
              else {
                  $total += $product->pricing[$billing_cycle];
              }
          }
      }
      $check_credit = $this->check_credit($agency->user_id, $total);
      if ( $check_credit == false ) {
          return false;
      }
      $credit = $this->credit->where('user_id', $agency->user_id)->first();
      if (!isset($credit)) {
          return false;
      }
      $credit->value -= $total;
      $credit->save();
      // create order
      $data_order = [
          'user_id' => $user->id,
          'total' => $total,
          'status' => 'Finish',
          'description' => 'expired vps',
          'type' => 'expired',
          'makh' => '',
      ];
      $order = $this->order->create($data_order);
      //create order detail
      $date = date('Y-m-d');
      $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
      $data_order_detail = [
          'order_id' => $order->id,
          'type' => 'hosting',
          'due_date' => $date,
          'paid_date' => date('Y-m-d'),
          'description' => 'Gia hạn Hosting',
          'status' => 'paid',
          'sub_total' => $total,
          'quantity' => count($data['list_hosting']),
          'user_id' => $user->id,
          'paid_date' => date('Y-m-d'),
          'addon_id' => '0',
          'payment_method' => 1,
      ];
      $detail_order = $this->detail_order->create($data_order_detail);
      $data_history = [
          'user_id' => $user->id,
          'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
          'discription' => 'Hóa đơn số ' . $detail_order->id,
          'type_gd' => '3',
          'method_gd' => 'invoice',
          'date_gd' => date('Y-m-d'),
          'money'=> $total,
          'status' => 'Active',
          'method_gd_invoice' => 'credit',
          'detail_order_id' => $detail_order->id,
      ];
      $this->history_pay->create($data_history);
      // invoice
      $billing = [
          'monthly' => '1 Month',
          'twomonthly' => '2 Month',
          'quarterly' => '3 Month',
          'semi_annually' => '6 Month',
          'annually' => '1 Year',
          'biennially' => '2 Year',
          'triennially' => '3 Year'
      ];
      $data_restore = [];
      foreach ($data['list_hosting'] as $key => $hosting_id) {
          $hosting = $this->hosting->find($hosting_id['portal_hosting_id']);
          // Tạo lưu trữ khi gia hạn
          $data_order_expired = [
              'detail_order_id' => $detail_order->id,
              'expired_id' => $hosting->id,
              'billing_cycle' => $billing_cycle,
              'type' => 'hosting',
          ];
          $order_expired_hosting = $this->order_expired->create($data_order_expired);
          $date_star = $hosting->next_due_date;
          $due_date = date('Y-m-d', strtotime( $date_star . $billing[$billing_cycle] ));

          if ( !empty($hosting->price_override) ) {
              if ( $hosting->billing_cycle ==  $billing_cycle ) {
                  $hosting->next_due_date = $due_date;
                  $hosting->save();
              }
          } else {
              $hosting->price_override = 0;
              $hosting->next_due_date = $due_date;
              $hosting->billing_cycle = $billing_cycle;
              $hosting->save();
          }
          $data_restore[] = $hosting;

          try {
              $billings = config('billing');
              // ghi log
              $data_log = [
                'user_id' => $user->id,
                'action' => 'đại lý API',
                'model' => 'Admin/Order',
                'description' => '  hoàn thành gia hạn Hosting <a href="/admin/hostings/detail/' . $hosting->id . '">' . $hosting->domain . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                'service' =>  $hosting->domain,
              ];
              $this->log_activity->create($data_log);
              // gui mail
              $this->send_mail_finish_expired('hosting',$order, $detail_order ,$order_expired_hosting);
          } catch (\Exception $e) {
              report($e);
              return $data_restore;
          }
      }
      return $data_restore;
  }

  public function send_mail_finish_expired($type, $order , $detail_order, $order_expired)
  {
      $billings = config('billing');
      $url = url('');
      $url = str_replace(['http://','https://'], ['', ''], $url);
      $add_on =  '';
      if ($type == 'vps') {
          $services = $order_expired->vps;
          $product = $this->product->find($services->product_id);
          $email = $this->email->find($product->meta_product->email_expired_finish);

          $user = $this->user->find($services->user_id);
          $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
          $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' , $detail_order->quantity, $services->os, '', $url];
          $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

          $this->user_send_email = $this->user->find($services->user_id)->email;
          $this->subject = $email->meta_email->subject;

          $data = [
              'content' => $content,
              'user_name' => $user->name,
              'user_id' => $user->id,
              'user_email' => $user->email,
              'user_addpress' => $user->user_meta->address,
              'user_phone' => $user->user_meta->user_phone,
              'order_id' => $order->id,
              'product_name' => $product->name,
              'domain' => '',
              'ip' => $services->ip,
              'billing_cycle' => $services->billing_cycle,
              'next_due_date' => $services->next_due_date,
              'order_created_at' => $order->created_at,
              'services_username' => $services->user,
              'services_password' => $services->password,
              'sub_total' => $detail_order->sub_total,
              'total' => $order->total,
              'qtt' => $detail_order->quantity,
              'os' => $services->os,
              'token' => '',
          ];

          $mail = Mail::send('users.mails.orders', $data, function($message){
              $message->from('support@cloudzone.vn', 'Cloudzone Portal');
              $message->to($this->user_send_email)->subject($this->subject);
          });
      }
      elseif ($type == 'hosting') {
          $services = $order_expired->hosting;
          $product = $this->product->find($services->product_id);
          $email = $this->email->find($product->meta_product->email_expired_finish);
          $user = $this->user->find($services->user_id);
          $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
          $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url];
          $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

          $this->user_send_email = $this->user->find($services->user_id)->email;
          $this->subject = !empty($email->meta_email->subject) ? $email->meta_email->subject : 'Gia hạn dịch vụ';

          $data = [
              'content' => $content,
              'user_name' => $user->name,
              'user_id' => $user->id,
              'user_email' => $user->email,
              'user_addpress' => $user->user_meta->address,
              'user_phone' => $user->user_meta->user_phone,
              'order_id' => $order->id,
              'product_name' => $product->name,
              'domain' => $services->domain,
              'ip' => '',
              'billing_cycle' => $services->billing_cycle,
              'next_due_date' => $services->next_due_date,
              'order_created_at' => $order->created_at,
              'services_username' => $services->user,
              'services_password' => $services->password,
              'sub_total' => $detail_order->sub_total,
              'total' => $order->total,
              'qtt' => $detail_order->quantity,
              'os' => $services->os,
              'token' => '',
          ];

          $mail = Mail::send('users.mails.orders', $data, function($message){
              $message->from('support@cloudzone.vn', 'Cloudzone Portal');
              $message->to($this->user_send_email)->subject($this->subject);
          });
      }
      elseif ($type == 'email_hosting') {
          $services = $order_expired->email_hosting;
          $product = $this->product->find($services->product_id);
          $email = $this->email->find($product->meta_product->email_expired_finish);
          $user = $this->user->find($services->user_id);
          $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
          $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url];
          $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

          $this->user_send_email = $this->user->find($services->user_id)->email;
          $this->subject = !empty($email->meta_email->subject) ? $email->meta_email->subject : 'Gia hạn dịch vụ Email Hosting';

          $data = [
              'content' => $content,
              'user_name' => $user->name,
              'user_id' => $user->id,
              'user_email' => $user->email,
              'user_addpress' => $user->user_meta->address,
              'user_phone' => $user->user_meta->user_phone,
              'order_id' => $order->id,
              'product_name' => $product->name,
              'domain' => $services->domain,
              'ip' => '',
              'billing_cycle' => $services->billing_cycle,
              'next_due_date' => $services->next_due_date,
              'order_created_at' => $order->created_at,
              'services_username' => $services->user,
              'services_password' => $services->password,
              'sub_total' => $detail_order->sub_total,
              'total' => $order->total,
              'qtt' => $detail_order->quantity,
              'os' => $services->os,
              'token' => '',
          ];

          $mail = Mail::send('users.mails.orders', $data, function($message){
              $message->from('support@cloudzone.vn', 'Cloudzone Portal');
              $message->to($this->user_send_email)->subject($this->subject);
          });
      }
  }

  function rand_string() {
      $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*";
      $size = strlen( $chars );
      $str = '';
      for( $i = 0; $i < 16; $i++ ) {
          $str .= $chars[ rand( 0, $size - 1 ) ];
      }
      return $str;
  }

}

?>
