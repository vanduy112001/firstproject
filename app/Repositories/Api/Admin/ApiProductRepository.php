<?php
namespace App\Repositories\Api\Admin;

use App\Model\User;
use App\Model\GroupUser;
use Illuminate\Support\Facades\Auth;
use App\Model\LogActivity;
use App\Model\HistoryPay;
use App\Model\LogPayment;
use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\MetaProduct;
use App\Model\Pricing;
use App\Model\VpsOs;
use App\Model\ProductUpgrate;
use App\Model\State;
use App\Factories\AdminFactories;

class ApiProductRepository {
	protected $user;
    protected $groupUser;
    protected $group_product;
    protected $log_activity;
    protected $state;
    protected $product;
    protected $product_upgrate;
    protected $meta_product;
    protected $pricing;
    protected $vps_os;
    protected $email;

    public function __construct()
    {
        $this->user = new User;
        $this->groupUser = new GroupUser;
        $this->group_product = new GroupProduct;
        $this->log_activity = new LogActivity();
        $this->state = new State();
        $this->product = new Product;
        $this->product_upgrate = new ProductUpgrate;
        $this->meta_product = new MetaProduct;
        $this->pricing = new Pricing;
        $this->vps_os = new VpsOs;
        $this->email = AdminFactories::emailRepositories();
    }

    public function listGroupProductByGroupUser($groupUserId)
    {
    	$group_products = $this->group_product->where('group_user_id', $groupUserId)->orderBy('id', 'desc')->with('products')->get();
    	$data = [];
    	foreach ($group_products as $key => $group_product) {
            $group_product->countProduct = $group_product->products->count();
    		foreach ($group_product->products as $key => $product) {
    			$info = '';
                if ( $product->type_product == 'VPS' || $product->type_product == 'VPS-US' || $product->type_product == 'NAT-VPS'  ) {
                    $info .= 'Cấu hình: ';
                    if ( !empty( $product->meta_product->cpu ) ) {
                        $info .= $product->meta_product->cpu . ' cpu';
                    }
                    if ( !empty( $product->meta_product->memory ) ) {
                        $info .= '-' . $product->meta_product->memory . ' ram';
                    }
                    if ( !empty( $product->meta_product->disk ) ) {
                        $info .= '-' . $product->meta_product->disk . ' disk';
                    }
                    if ( !empty( $product->meta_product->ip ) ) {
                        $info .= '-' . $product->meta_product->ip . ' ip';
                    }
                    $info .= ' - HĐH: ';
                    if ( !empty( $product->meta_product->os ) ) {
                        $info .= $product->meta_product->os;
                    }
                    $info .= ' - Băng thông: ';
                    if ( !empty( $product->meta_product->bandwidth ) ) {
                        $info .= $product->meta_product->bandwidth;
                    }
                }
                elseif ( $product->type_product == 'Hosting' || $product->type_product == 'Hosting-Singapore' || $product->type_product == 'Email Hosting' ) {
                    $info .= 'Cấu hình: ';
                    if ( !empty( $product->meta_product->storage ) ) {
                        $info .= $product->meta_product->storage . 'MB';
                    }
                    if ( !empty( $product->meta_product->domain ) ) {
                        $info .= '-' . $product->meta_product->domain . ' domain ';
                    }
                    if ( !empty( $product->meta_product->database ) ) {
                        $info .= '-' . $product->meta_product->database . ' db ';
                    }
                    if ( !empty( $product->meta_product->ftp ) ) {
                        $info .= '-' . $product->meta_product->ftp . ' ftp ';
                    }
                    $info .= ' - Control panel: ';
                    if ( !empty( $product->meta_product->panel ) ) {
                        $info .= $product->meta_product->panel;
                    }
                }
                elseif ( $product->type_product == 'Addon-VPS' ) {
                    $info .= 'Cấu hình: ';
                    if ( !empty( $product->meta_product->cpu ) ) {
                        $info .= $product->meta_product->cpu . ' cpu';
                    }
                    if ( !empty( $product->meta_product->memory ) ) {
                        $info .= $product->meta_product->memory . ' ram';
                    }
                    if ( !empty( $product->meta_product->disk ) ) {
                        $info .= $product->meta_product->disk . ' disk';
                    }
                    if ( !empty( $product->meta_product->ip ) ) {
                        $info .= $product->meta_product->ip . ' ip';
                    }
                }
                $product->info = $info;
                $price = '';
                if ( $product->pricing->type == 'one_time' ) {
                	$price = number_format( $product->pricing->one_time_pay ,0,",",".") . '/Vĩnh viễn';
                } else {
                	if ( !empty($product->pricing->monthly) ) {
                		$price = number_format( $product->pricing->monthly ,0,",",".") . '/1 Tháng';
                	}
                	elseif ( !empty($product->pricing->twomonthly) ) {
                		$price = number_format( $product->pricing->twomonthly ,0,",",".") . '/2 Tháng';
                	}
                	elseif ( !empty($product->pricing->quarterly) ) {
                		$price = number_format( $product->pricing->quarterly ,0,",",".") . '/3 Tháng';
                	}
                	elseif ( !empty($product->pricing->semi_annually) ) {
                		$price = number_format( $product->pricing->semi_annually ,0,",",".") . '/6 Tháng';
                	}
                	elseif ( !empty($product->pricing->annually) ) {
                		$price = number_format( $product->pricing->monthly ,0,",",".") . '/1 Năm';
                	}
                	elseif ( !empty($product->pricing->biennially) ) {
                		$price = number_format( $product->pricing->biennially ,0,",",".") . '/2 Năm';
                	}
                	elseif ( !empty($product->pricing->triennially) ) {
                		$price = number_format( $product->pricing->triennially ,0,",",".") . '/3 Năm';
                	}
                }
                $product->price;
    		}
    		$data[] = $group_product;
    	}
    	return $data;
    }

    public function createGroupProduct($request)
    {
    	try {
    		$data = [
	            'name' => $request['name'],
	            'title' => $request['title'],
	            'type' => $request['type'],
	            'stt' => !empty($request['stt']) ? $request['stt'] : 20,
	            'hidden' => !empty($request['hidden']) ? true : false,
	            'link' => !empty($request['link']) ? $request['link'] : '',
	            'group_user_id' => !empty($request['groupUserId']) ? $request['groupUserId'] : 0,
                'private' => !empty($request['groupUserId']) ? true : false,
	        ];
	        $group_product = $this->group_product->create($data);
	        // ghi log
	        $data_log = [
	          'user_id' => Auth::user()->id,
	          'action' => 'tạo',
	          'model' => 'Admin/Product',
	          'description' => ' nhóm sản phẩm ' . $group_product->name,
	        ];
	        $this->log_activity->create($data_log);

	        return [
                'error' => 0,
                'message' => 'Tạo nhóm sản phẩm thành công',
            ];
    	} catch (Exception $e) {
    		report($e);
            return [
                'error' => 1,
                'message' => 'Tạo nhóm sản phẩm thất bại',
            ];
    	}
    }

    public function detailGroupProduct($groupProductId)
    {
        $group_product =  $this->group_product->find($groupProductId);
        if ( isset( $group_product ) ) {
            return [
                'error' => 0,
                'message' => '',
                'groupProduct' => $group_product
            ];
        } else {
            return [
                'error' => 1,
                'message' => 'Nhóm sản phẩm không tồn tại'
            ];
        }
    }

    public function updateGroupProduct($request)
    {
    	try {
    		$group_product = $this->group_product->find($request['groupProductId']);
    		if ( isset($group_product) ) {
	    		$data = [
		            'name' => $request['name'],
		            'title' => $request['title'],
		            'type' => $request['type'],
		            'stt' => !empty($request['stt']) ? $request['stt'] : 20,
		            'hidden' => !empty($request['hidden']) ? true : false,
		            'link' => !empty($request['link']) ? $request['link'] : '',
		        ];
		        $group_product->update($data);
		        // ghi log
		        $data_log = [
		          'user_id' => Auth::user()->id,
		          'action' => 'chỉnh sửa',
		          'model' => 'Admin/Mobile/Product',
		          'description' => ' nhóm sản phẩm ' . $group_product->name,
		        ];
		        $this->log_activity->create($data_log);

    			return [
	                'error' => 0,
	                'message' => 'Chỉnh sửa nhóm sản phẩm thành công',
	            ];
    		} else {
    			return [
	                'error' => 1,
	                'message' => 'Nhóm sản phẩm không tồn tại',
	            ];
    		}
    	} catch (Exception $e) {
    		report($e);
    		return [
                'error' => 1,
                'message' => 'Chỉnh sửa nhóm sản phẩm thất bại',
            ];
    	}
    }

    public function deleteGroupProduct($groupProductId)
    {
    	try {
    		$group_product = $this->group_product->find($request['groupProductId']);
    		if ( isset($group_product) ) {
    			$this->product->where('group_product_id', $groupProductId)->delete();
		        // ghi log
		        $data_log = [
		          'user_id' => Auth::user()->id,
		          'action' => 'xóa',
		          'model' => 'Admin/Mobile/Product',
		          'description' => ' nhóm sản phẩm ' . $group_product->name,
		        ];
		        $this->log_activity->create($data_log);

    			$group_product->delete();
    		} else {
    			return [
	                'error' => 1,
	                'message' => 'Nhóm sản phẩm không tồn tại',
	            ];
	        }
    	} catch (Exception $e) {
    		report($e);
    		return [
                'error' => 1,
                'message' => 'Chỉnh sửa nhóm sản phẩm thất bại',
            ];
    	}
    }
/* Product */
    public function createProduct($request)
    {
        try {
            // chi tiết product
            $data_product = [
                'name' => $request['name'],
                'group_product_id' => $request['groupProductId'],
                'type_product' => $request['typeProduct'],
                'module' => !empty($request['module']) ? $request['module'] : '' ,
                'hidden' => !empty($request['hidden']) ? $request['hidden'] : 0 ,
                'stt' => !empty($request['stt']) ? $request['stt'] : 10,
                'package' => !empty($request['package']) ? $request['package'] : '',
                'description' => !empty($request['description']) ? $request['description'] : '',
            ];
            $product = $this->product->create($data_product);
            // chi tiết meta_product
            $data_meta_product = [
                'product_id' => $product->id,
                'cpu' => !empty($request['cpu']) ? $request['cpu'] : '',
                'memory' => !empty($request['ram']) ? $request['ram'] : '',
                'disk' => !empty($request['disk']) ? $request['disk'] : '',
                'bandwidth' => !empty($request['bandwidth']) ? $request['bandwidth'] : '',
                'ip' => !empty($request['ip']) ? $request['ip'] : '',
                'os' => !empty($request['os']) ? $request['os'] : '',
                'email_id' => !empty($request['emailFinishOrder']) ? $request['emailFinishOrder'] : '',
                'email_create' => !empty($request['emailCreateOrder']) ? $request['emailCreateOrder'] : '',
                'storage' => !empty($request['storage']) ? $request['storage'] : '',
                'domain' => !empty($request['domain']) ? $request['domain'] : '',
                'sub_domain' => !empty($request['sub_domain']) ? $request['sub_domain'] : '',
                'alias_domain' => !empty($request['alias_domain']) ? $request['alias_domain'] : '',
                'database' => !empty($request['database']) ? $request['database'] : '',
                'ftp' => !empty($request['ftp']) ? $request['ftp'] : '',
                'panel' => !empty($request['panel']) ? $request['panel'] : '',
                'name_server' => !empty($request['name_server']) ? $request['name_server'] : '',
                'hidden' => '',
                'email_expired' => $request['emailCreateOrderExpire'],
                'email_expired_finish' => $request['emailFinishOrderExpire'],
                'type_addon' =>!empty($request['type_addon']) ? $request['type_addon'] : '',
                'product_special' =>!empty($request['product_special']) ? $request['product_special'] : 0,
                'girf' =>!empty($request['girf']) ? $request['girf'] : null,
                'promotion' =>!empty($request['promotion']) ? 1 : 0,
                'qtt_email' =>!empty($request['qtt_email']) ? $request['qtt_email'] : '',
                'chassis' =>!empty($request['chassis']) ? $request['chassis'] : '',
                'raid' =>!empty($request['raid']) ? $request['raid'] : '',
                'datacenter' =>!empty($request['datacenter']) ? $request['datacenter'] : '',
                'server_management' =>!empty($request['server_management']) ? $request['server_management'] : '',
                'backup' =>!empty($request['backup']) ? $request['backup'] : '',
                'cores' =>!empty($request['cores']) ? $request['cores'] : '',
                'email_config_finish' => !empty($request['email_config_finish']) ? $request['email_config_finish'] : 0,
            ];
            $meta_product = $this->meta_product->create($data_meta_product);
            // chi tiết pricing
            $data_pricing = [
                'product_id' => $product->id,
                'type' => !empty($request['type']) ? $request['typePricing'] : 0,
                'monthly' => !empty($request['monthly']) ? $request['monthly'] : 0,
                'twomonthly' => !empty($request['twomonthly']) ? $request['twomonthly'] : 0,
                'quarterly' => !empty($request['quarterly']) ? $request['quarterly'] : 0,
                'semi_annually' => !empty($request['semi_annually']) ? $request['semi_annually'] : 0,
                'annually' => !empty($request['annually']) ? $request['annually'] : 0,
                'biennially' => !empty($request['biennially']) ? $request['biennially'] : 0,
                'triennially' => !empty($request['triennially']) ? $request['triennially'] : 0,
                'one_time_pay' => !empty($request['one_time_pay']) ? $request['one_time_pay'] : 0,
            ];
            $pricing = $this->pricing->create($data_pricing);
            // data vps_os
            $list_vps_os = !empty($request['listOsVps']) ? $request['listOsVps'] : [];
            $list_upgrate = !empty($request['upgrate']) ? $request['upgrate'] : [];
            if (isset($list_vps_os)) {
                foreach ($list_vps_os as $key => $vps_os) {
                    $data_vps_on = [
                        'product_id' => $id,
                        'os' => $vps_os->value,
                    ];
                    $this->vps_os->create($data_vps_on);
                }
            }
            // upgrate
            if (isset($list_upgrate)) {
               foreach ($list_upgrate as $key => $upgrate) {
                  $data_upgrate = [
                    'product_default_id' => $id,
                    'product_upgrate_id' => $upgrate,
                  ];
                  $this->product_upgrate->create($data_upgrate);
               }
            }
            return [
                'error' => 0,
                'message' => 'Tạo sản phẩm thành công',
            ];
        } catch (Exception $e) {
            report($e);
            return [
                'error' => 1,
                'message' => 'Tạo sản phẩm thất bại',
            ];
        }
    }

    public function updateProduct($request)
    {
        try {
            $product = $this->product->find($request['productId']);
            if ( isset($product) ) {
                // chi tiết product
                $data_product = [
                    'name' => $request['name'],
                    'group_product_id' => $request['group_product_id'],
                    'type_product' => $request['type_product'],
                    'module' => !empty($request['module']) ? $request['module'] : '' ,
                    'hidden' => !empty($request['hidden']) ? $request['hidden'] : 0 ,
                    'stt' => !empty($request['stt']) ? $request['stt'] : 10,
                    'package' => !empty($request['package']) ? $request['package'] : '',
                    'description' => !empty($request['description']) ? $request['description'] : '',
                ];
                $product->update($data_product);
                // chi tiết meta_product
                $data_meta_product = [
                    'cpu' => !empty($request['cpu']) ? $request['cpu'] : '',
                    'memory' => !empty($request['ram']) ? $request['ram'] : '',
                    'disk' => !empty($request['disk']) ? $request['disk'] : '',
                    'bandwidth' => !empty($request['bandwidth']) ? $request['bandwidth'] : '',
                    'ip' => !empty($request['ip']) ? $request['ip'] : '',
                    'os' => !empty($request['os']) ? $request['os'] : '',
                    'email_id' => !empty($request['email_id']) ? $request['email_id'] : '',
                    'email_create' => !empty($request['email_create']) ? $request['email_create'] : '',
                    'storage' => !empty($request['storage']) ? $request['storage'] : '',
                    'domain' => !empty($request['domain']) ? $request['domain'] : '',
                    'sub_domain' => !empty($request['sub_domain']) ? $request['sub_domain'] : '',
                    'alias_domain' => !empty($request['alias_domain']) ? $request['alias_domain'] : '',
                    'database' => !empty($request['database']) ? $request['database'] : '',
                    'ftp' => !empty($request['ftp']) ? $request['ftp'] : '',
                    'panel' => !empty($request['panel']) ? $request['panel'] : '',
                    'name_server' => !empty($request['name_server']) ? $request['name_server'] : '',
                    'hidden' => '',
                    'email_expired' => $request['email_expired'],
                    'email_expired_finish' => $request['email_expired_finish'],
                    'type_addon' =>!empty($request['type_addon']) ? $request['type_addon'] : '',
                    'product_special' =>!empty($request['product_special']) ? $request['product_special'] : 0,
                    'girf' =>!empty($request['girf']) ? $request['girf'] : null,
                    'promotion' =>!empty($request['promotion']) ? 1 : 0,
                    'qtt_email' =>!empty($request['qtt_email']) ? $request['qtt_email'] : '',
                ];
                $product->meta_product->update($data_meta_product);
                // chi tiết pricing
                $data_pricing = [
                    'type' => !empty($request['typePricing']) ? $request['typePricing'] : 0,
                    'twomonthly' => !empty($request['monthly']) ? $request['monthly'] : 0,
                    'twomonthly' => !empty($request['twomonthly']) ? $request['twomonthly'] : 0,
                    'quarterly' => !empty($request['quarterly']) ? $request['quarterly'] : 0,
                    'semi_annually' => !empty($request['semi_annually']) ? $request['semi_annually'] : 0,
                    'annually' => !empty($request['annually']) ? $request['annually'] : 0,
                    'biennially' => !empty($request['biennially']) ? $request['biennially'] : 0,
                    'triennially' => !empty($request['triennially']) ? $request['triennially'] : 0,
                    'one_time_pay' => !empty($request['one_time_pay']) ? $request['one_time_pay'] : 0,
                ];
                $product->pricing->update($data_pricing);
                // data vps_os
                $list_vps_os = !empty($request['os_vps']) ? $request['os_vps'] : [];
                $list_upgrate = !empty($request['upgrate']) ? $request['upgrate'] : [];
                if (isset($list_vps_os)) {
                    $this->vps_os->where('product_id', $id)->delete();
                    foreach ($list_vps_os as $key => $vps_os) {
                        $data_vps_on = [
                            'product_id' => $id,
                            'os' => $vps_os,
                        ];
                        $this->vps_os->create($data_vps_on);
                    }
                }
                // upgrate
                if (isset($list_upgrate)) {
                    $this->product_upgrate->where('product_default_id', $id)->delete();
                    foreach ($list_upgrate as $key => $upgrate) {
                      $data_upgrate = [
                        'product_default_id' => $id,
                        'product_upgrate_id' => $upgrate,
                      ];
                      $this->product_upgrate->create($data_upgrate);
                    }
                }
                return [
                    'error' => 0,
                    'message' => 'Chỉnh sửa sản phẩm thành công',
                ];
            } else {
                return [
                    'error' => 1,
                    'message' => 'Sản phẩm không tồn tại',
                ];
            }

        } catch (Exception $e) {
            report($e);
            return [
                'error' => 1,
                'message' => 'Chỉnh sửa sản phẩm thất bại',
            ];
        }
    }

    public function detailProduct($productId)
    {
        $product = $this->product->find($request['productId']);
        if ( isset($product) ) {
			$list_config_os = config('os');
			$listVpsOs = $product->vps_os;
			$list_os = [];
			$list_responce_os = [];
			foreach ($list_config_os as $key => $os) {
				$isChecked = false;
				if (!empty($listVpsOs)) {
					foreach ($listVpsOs as $vpsOs) {
						if ( $vpsOs->os == $key ) {
							$isChecked = true;
							$list_responce_os[] = [
								'label' => $os,
								'value' => $key,
								'isChecked' => $isChecked
							];
						}
					}
				}
				$list_os[] = [
					'label' => $os,
					'value' => $key,
					'isChecked' => $isChecked
				];
			}
            return [
                'error' => 0,
                'message' => '',
								'type_products' => config('type_product'),
                'emails' => $this->email->getTemplateEmail(),
                'config_os' => $list_os,
                'product' => $product,
                'metaProduct' => $product->meta_product,
                'pricing' => $product->pricing,
								'listVpsOs' => $list_responce_os,
            ];
        } else {
            return [
                'error' => 1,
                'message' => 'Sản phẩm không tồn tại',
            ];
        }

    }

    public function listProductByGroupProduct($groupProductId)
    {
        $groupProduct = $this->group_product->find($groupProductId);
        if ( isset($groupProduct) ) {
            $data = [];
            $products = $this->product->where('group_product_id', $groupProductId)->get();
            foreach ($products as $key => $product) {
                $info = '';
                if ( $product->type_product == 'VPS' || $product->type_product == 'VPS-US' || $product->type_product == 'NAT-VPS'  ) {
                    $info .= 'Cấu hình: ';
                    if ( !empty( $product->meta_product->cpu ) ) {
                        $info .= $product->meta_product->cpu . ' cpu';
                    }
                    if ( !empty( $product->meta_product->memory ) ) {
                        $info .= '-' . $product->meta_product->memory . ' ram';
                    }
                    if ( !empty( $product->meta_product->disk ) ) {
                        $info .= '-' . $product->meta_product->disk . ' disk';
                    }
                    if ( !empty( $product->meta_product->ip ) ) {
                        $info .= '-' . $product->meta_product->ip . ' ip';
                    }
                    $info .= ' - HĐH: ';
                    if ( !empty( $product->meta_product->os ) ) {
                        $info .= $product->meta_product->os;
                    }
                    $info .= ' - Băng thông: ';
                    if ( !empty( $product->meta_product->bandwidth ) ) {
                        $info .= $product->meta_product->bandwidth;
                    }
                }
                elseif ( $product->type_product == 'Hosting' || $product->type_product == 'Hosting-Singapore' || $product->type_product == 'Email Hosting' ) {
                    $info .= 'Cấu hình: ';
                    if ( !empty( $product->meta_product->storage ) ) {
                        $info .= $product->meta_product->storage . 'MB';
                    }
                    if ( !empty( $product->meta_product->domain ) ) {
                        $info .= '-' . $product->meta_product->domain . ' domain ';
                    }
                    if ( !empty( $product->meta_product->database ) ) {
                        $info .= '-' . $product->meta_product->database . ' db ';
                    }
                    if ( !empty( $product->meta_product->ftp ) ) {
                        $info .= '-' . $product->meta_product->ftp . ' ftp ';
                    }
                    $info .= ' - Control panel: ';
                    if ( !empty( $product->meta_product->panel ) ) {
                        $info .= $product->meta_product->panel;
                    }
                }
                elseif ( $product->type_product == 'Addon-VPS' ) {
                    $info .= 'Cấu hình: ';
                    if ( !empty( $product->meta_product->cpu ) ) {
                        $info .= $product->meta_product->cpu . ' cpu';
                    }
                    if ( !empty( $product->meta_product->memory ) ) {
                        $info .= $product->meta_product->memory . ' ram';
                    }
                    if ( !empty( $product->meta_product->disk ) ) {
                        $info .= $product->meta_product->disk . ' disk';
                    }
                    if ( !empty( $product->meta_product->ip ) ) {
                        $info .= $product->meta_product->ip . ' ip';
                    }
                }
                $product->info = $info;
                $price = '';
                if ( $product->pricing->type == 'one_time' ) {
                    $price = number_format( $product->pricing->one_time_pay ,0,",",".") . '/Vĩnh viễn';
                } else {
                    if ( !empty($product->pricing->monthly) ) {
                        $price = number_format( $product->pricing->monthly ,0,",",".") . '/1 Tháng';
                    }
                    elseif ( !empty($product->pricing->twomonthly) ) {
                        $price = number_format( $product->pricing->twomonthly ,0,",",".") . '/2 Tháng';
                    }
                    elseif ( !empty($product->pricing->quarterly) ) {
                        $price = number_format( $product->pricing->quarterly ,0,",",".") . '/3 Tháng';
                    }
                    elseif ( !empty($product->pricing->semi_annually) ) {
                        $price = number_format( $product->pricing->semi_annually ,0,",",".") . '/6 Tháng';
                    }
                    elseif ( !empty($product->pricing->annually) ) {
                        $price = number_format( $product->pricing->monthly ,0,",",".") . '/1 Năm';
                    }
                    elseif ( !empty($product->pricing->biennially) ) {
                        $price = number_format( $product->pricing->biennially ,0,",",".") . '/2 Năm';
                    }
                    elseif ( !empty($product->pricing->triennially) ) {
                        $price = number_format( $product->pricing->triennially ,0,",",".") . '/3 Năm';
                    }
                }
                $product->price;
                $data[] = $product;
            }
            return [
                'error' => 0,
                'message' => '',
                'groupProduct' => $groupProduct,
                'products' => $data,
            ];
        } else {
            return [
                'error' => 1,
                'message' => 'Nhóm sản phẩm không tồn tại',
            ];
        }

    }
}

?>
