<?php

namespace App\Repositories\Admin;

use App\Factories\AdminFactories;
use App\Model\Notification;
use App\Model\Status;
use App\Model\ReadNotification;
use App\Model\User;
use App\Model\LogActivity;
use Illuminate\Support\Facades\Auth;

class NotificationRepositories
{
  protected $model;
  protected $user;
  protected $readnotification;
  protected $log_activity;

  function __construct()
  {
    $this->model = new Notification();
    $this->user = new User();
    $this->readnotification = new ReadNotification();
    $this->log_activity = new LogActivity();
  }

  public function all($params = [])
  {
    $query = $this->model;
    if (!empty($params['keyword'])) {
      $query = $query->where(function ($query) use ($params) {
        $query->orWhere('name', 'LIKE', '%' . $params['keyword'] . '%');
      });
    }
    $result = $query->orderBy('updated_at', 'desc')->paginate(10);
    return $result;
  }

  public function create($data_notification)
  {
    $infoNotification = $this->model->create($data_notification);
    // ghi log
    $data_log = [
      'user_id' => Auth::user()->id,
      'action' => 'tạo',
      'model' => 'Admin/Notification',
      'description' => ' thông báo <a href="/admin/notifications/edit/' . $infoNotification->id . '">' . $infoNotification->name . '</a> ',
    ];
    $this->log_activity->create($data_log);

    $users = $this->user->get();
    if ($infoNotification) {
      foreach ($users as $user) {
        if($user->name != 'admin') {
          $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
        }
      }
      return true;
    } else {
      return false;
    }
  }

  public function detail($notification)
  {
    $infoNotification = $this->model->find($notification);
    return $infoNotification;
  }

  public function update($id, $data_notification)
  {
    // dd($data_notification->status);
    if ($data_notification['status'] == "Cancel") {
      $update_read = $this->readnotification->where('notification_id', $id)->update(['read_at' => NULL]);
    }
    $infoNotification = $this->model->find($id);
    // ghi log
    $data_log = [
      'user_id' => Auth::user()->id,
      'action' => 'chỉnh sửa',
      'model' => 'Admin/Notification',
      'description' => ' thông báo <a href="/admin/notifications/edit/' . $infoNotification->id . '">' . $infoNotification->name . '</a> ',
    ];
    $this->log_activity->create($data_log);
    $infoNotification = $infoNotification->update($data_notification);
    if ($infoNotification) {
      return true;
    } else {
      return false;
    }
  }

  public function delete($id)
  {
    try {
      $result = [
        'status' => false,
        'message' => 'Có lỗi xảy ra, vui lòng thử lại.',
      ];
      // ghi log
      $infoNotification = $this->model->find($id);
      $data_log = [
        'user_id' => Auth::user()->id,
        'action' => 'chỉnh sửa',
        'model' => 'Admin/Notification',
        'description' => ' thông báo ' . $infoNotification->name ,
      ];
      $this->log_activity->create($data_log);
      // Xóa tất cả các table có liên quan trên user
      $delete = $this->model->where('id', $id)->delete();

      if ($delete) {
        $this->readnotification->where('notification_id', $id)->delete();
        $result = [
          'status' => true,
          'message' => 'Xóa thành công.',
        ];
      }
      return $result;
    } catch (\Exception $exception) {
      return [
        'status' => false,
        'message' => $exception->getMessage(),
      ];
    }
  }
  public function multidelete($id)
  {
    try {
      $result = [
        'status' => false,
        'message' => 'Có lỗi xảy ra, vui lòng thử lại.',
      ];
      // ghi log
      $infoNotification = $this->model->find($id);
      $data_log = [
        'user_id' => Auth::user()->id,
        'action' => 'chỉnh sửa',
        'model' => 'Admin/Notification',
        'description' => ' thông báo ' . $infoNotification->name ,
      ];
      $this->log_activity->create($data_log);
      // Xóa tất cả các table có liên quan trên user
      $delete = $this->model->whereIn('id', $id)->delete();

      if ($delete) {
        $this->readnotification->whereIn('notification_id', $id)->delete();
        $result = [
          'status' => true,
          'message' => 'Xóa thành công.',
        ];
      }
      return $result;
    } catch (\Exception $exception) {
      return [
        'status' => false,
        'message' => $exception->getMessage(),
      ];
    }
  }
}
