<?php


namespace App\Repositories\Admin;

use App\Model\User;
use App\Model\UserMeta;
use App\Model\Verify;
use App\Model\Credit;
use App\Model\Email;
use App\Model\HistoryPay;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\Server;
use App\Model\Hosting;
use App\Model\Product;
use App\Model\GroupProduct;
use App\Model\VpsConfig;
use App\Model\Colocation;
use App\Model\EmailHosting;
use App\Model\LogPayment;
use App\Model\OrderExpired;
use App\Model\SendMail;
use Mail;
use App\Factories\AdminFactories;
use App\Http\Controllers\Admin\DirectAdminController;
use App\Model\TotalPrice;
use App\Model\DomainProduct;
use App\Model\Domain;
use App\Model\DomainExpired;
use App\Model\LogActivity;
use Illuminate\Support\Facades\Auth;

class PaymentRepositories
{
    protected $user;
    protected $user_meta;
    protected $credit;
    protected $email;
    protected $history_pay;
    protected $group_product;
    protected $order;
    protected $detail_order;
    protected $vps;
    protected $vps_config;
    protected $server;
    protected $hosting;
    protected $user_email;
    protected $subject;
    protected $product;
    protected $total_price;
    protected $domain_product;
    protected $domain;
    protected $domain_exp;
    protected $colocation;
    protected $email_hosting;
    protected $log_activity;
    protected $log_payment;
    protected $order_expired;
    protected $send_mail;
    // API
    protected $da;
    protected $da_si;
    protected $dashboard;
    protected $domain_pa;

    public function __construct()
    {
        $this->user = new User;
        $this->user_meta = new UserMeta;
        $this->credit = new Credit;
        $this->email = new Email;
        $this->history_pay = new HistoryPay;
        $this->order = new Order;
        $this->detail_order = new DetailOrder;
        $this->vps = new Vps;
        $this->server = new Server;
        $this->hosting = new Hosting;
        $this->product = new Product;
        $this->total_price = new TotalPrice;
        $this->group_product = new GroupProduct;
        $this->domain_product = new DomainProduct;
        $this->vps_config = new VpsConfig;
        $this->domain = new Domain;
        $this->domain_exp = new DomainExpired;
        $this->colocation = new Colocation;
        $this->email_hosting = new EmailHosting;
        $this->log_activity = new LogActivity();
        $this->log_payment = new LogPayment;
        $this->order_expired = new OrderExpired;
        $this->send_mail = new SendMail;
        $this->dashboard = AdminFactories::dashBoardRepositories();
        $this->da = AdminFactories::directAdminRepositories();
        $this->domain_pa = AdminFactories::domainRepositories();
        $this->da_si = AdminFactories::directAdminSingaporeRepositories();
    }

    public function get_payments($method, $user_id = '') {
        // dd($method);
        if (!empty($method)) {
            if ($method == 'pay') {
                $payments = $this->history_pay->where('type_gd', 1)->with('user')->orderBy('id', 'desc')->paginate(20);
            }
            elseif ($method == 'invoice') {
                $payments = $this->history_pay->where('type_gd', 2)->with('user')->orderBy('id', 'desc')->paginate(20);
            }
            elseif ($method == 'expired') {
                $payments = $this->history_pay->where('type_gd', 3)->with('user')->orderBy('id', 'desc')->paginate(20);
            }
            elseif ($method == 'addon') {
                $payments = $this->history_pay->where('type_gd', 4)->with('user')->orderBy('id', 'desc')->paginate(20);
            } elseif ($method == 'change_id') {
                $payments = $this->history_pay->where('type_gd', 5)->with('user')->orderBy('id', 'desc')->paginate(20);
            } elseif ($method == 'domain') {
                $payments = $this->history_pay->where('type_gd', 6)->with('user')->orderBy('id', 'desc')->paginate(20);
            } elseif ($method == 'domain_exp') {
                $payments = $this->history_pay->where('type_gd', 7)->with('user')->orderBy('id', 'desc')->paginate(20);
            } elseif ($method == 'upgrade_hosting') {
                $payments = $this->history_pay->where('type_gd', 8)->with('user')->orderBy('id', 'desc')->paginate(20);
            }
        } else {
            $payments = $this->history_pay->with('user')->orderBy('id', 'desc')->paginate(20);
        }
        return $payments;
    }


    public function delete_payment($id) {
        $delete = $this->history_pay->find($id)->delete();
        return $delete;
    }

    public function detail($id) {
        return $this->history_pay->find($id);
    }

    public function get_qtt_payment_confirm()
    {
        $payments = $this->history_pay->where('status', 'confirm')
                    ->orderBy('id', 'desc')->get();
        $data = [
          'qtt' => 0,
          'last_time' => false,
        ];
        if ( $payments->count() > 0 ) {
            $data = [
              'qtt' => $payments->count(),
              'last_time' => $payments[0]->created_at,
            ];
        }
        return $data;
    }

    // TODO: api lấy giao dich chưa thanh toán
    public function get_payment_unpaid()
    {
        $payments = $this->history_pay->where('status', 'confirm')
                    ->where(function($query)
                          {
                              return $query
                                ->orWhere('method_gd', 'bank_transfer_techcombank')
                                ->orWhere('method_gd', 'bank_transfer_vietcombank')
                                ->orWhere('method_gd_invoice', 'bank_transfer_vietcombank')
                                ->orWhere('method_gd_invoice', 'bank_transfer_techcombank');
                          })
                    ->orderBy('id', 'desc')->get();
        return $payments;
    }

    public function api_confirm_payment($list)
    {
        try {
          foreach ($list as $key => $data) {
              $payment = $this->history_pay->find($data['id']);
              if (!empty($payment)) {
                if ( $payment->money <= $data['amount'] ) {
                    $payment->magd_api = $data['stc'];
                    $payment->type_gd_api = $data['type'];
                    $payment->save();

                    // cộng tiền vào tài khoản
                    if ( $payment->money < $data['amount'] ) {
                      $user = $payment->user;
                      $so_du = (int) str_replace(',', '' , $data['amount'] ) - (int) $payment->money;
                      $data_history = [
                        'user_id' => $user->id,
                        'ma_gd' => 'ADPL' . strtoupper(substr(sha1(time()), 34, 39)),
                        'discription' => 'Cộng tiền vào tài khoản',
                        'type_gd' => '98',
                        'method_gd' => 'credit',
                        'date_gd' => date('Y-m-d'),
                        'money'=> $so_du,
                        'status' => 'Active',
                      ];
                      $create = $this->history_pay->create($data_history);

                      $user_credit = $user->credit;
                      $user_credit->value += (int) str_replace(',', '' , $data['amount'] ) - (int) $payment->money;
                      $user_credit->save();

                      $data_log = [
                         'user_id' => $user->id,
                         'action' => 'Cộng tiền vào tài khoản',
                         'model' => 'Admin/VCB',
                         'description_user' => ' cộng ' . number_format($so_du,0,",",".") . ' VNĐ vào số dư tài khoản do thanh toán dịch vụ dư',
                       ];
                       $this->log_activity->create($data_log);
                       $data_log = [
                         'user_id' => 999999,
                         'action' => 'Cộng tiền vào tài khoản',
                         'model' => 'Admin/VCB',
                         'description' => ' cộng ' . number_format($so_du,0,",",".") . ' VNĐ vào số dư tài khoản <a href="/admin/users/detail/' . $user->id . '">' . $user->name . '</a> do thanh toán dịch vụ dư' ,
                       ];
                       $this->log_activity->create($data_log);

                    }
                    try {
                      $this->confirm_payment($payment->id);
                    } catch (\Exception $e) {
                      $data_response = [
                          'error' => 1,
                          'status' => 'Lỗi không xác nhận giao dịch',
                          'data' => $data,
                      ];
                      return $data_response;
                    }
                    $data_log = [
                      'user_id' => 999999,
                      'action' => 'tự động xác nhận dịch vụ',
                      'model' => 'CronTab/Confirm_payment',
                      'description' => ' <a target="_blank" href="/admin/users/detail/' . $payment->id . '">#' . $payment->id . '</a> bằng vietcombank',
                    ];
                    $this->log_activity->create($data_log);
                }
                else {
                    if ($payment->type_gd == 1) {
                        $payment->money = $data['amount'];
                        $payment->save();
                        try {
                          $this->confirm_payment($payment->id);
                        } catch (\Exception $e) {
                          $data_response = [
                              'error' => 1,
                              'status' => 'Lỗi không xác nhận giao dịch',
                              'data' => $data,
                          ];
                          return $data_response;
                        }
                        $data_log = [
                          'user_id' => 999999,
                          'action' => 'tự động xác nhận dịch vụ',
                          'model' => 'CronTab/Confirm_payment',
                          'description' => ' <a target="_blank" href="/admin/users/detail/' . $payment->id . '">#' . $payment->id . '</a> bằng vietcombank',
                        ];
                        $this->log_activity->create($data_log);
                    } else {
                      $user = $payment->user;
                      $data_history = [
                        'user_id' => $user->id,
                        'ma_gd' => 'GDVC' . strtoupper(substr(sha1(time()), 34, 39)),
                        'discription' => 'Cộng tiền vào tài khoản',
                        'type_gd' => '1',
                        'method_gd' => 'credit',
                        'date_gd' => date('Y-m-d'),
                        'money'=> $data['amount'],
                        'status' => 'Active',
                      ];
                      $create = $this->history_pay->create($data_history);

                      $user_credit = $user->credit;
                      $user_credit->value += (int) $data['amount'];
                      $user_credit->total += (int) $data['amount'];
                      $user_credit->save();

                      $data_log = [
                         'user_id' => $user->id,
                         'action' => 'Cộng tiền vào tài khoản',
                         'model' => 'Admin/VCB',
                         'description_user' => ' cộng ' . number_format($data['amount'],0,",",".") . ' VNĐ vào số dư tài khoản do thanh toán dịch vụ thiếu ' . number_format( (int) $payment->money - (int) $data['amount'] ,0,",",".") . ' VNĐ',
                       ];
                      $this->log_activity->create($data_log);
                      $data_log = [
                         'user_id' => 999999,
                         'action' => 'Cộng tiền vào tài khoản',
                         'model' => 'Admin/VCB',
                         'description' => ' cộng ' . number_format($data['amount'],0,",",".") . ' VNĐ vào số dư tài khoản <a href="/admin/users/detail/' . $user->id . '">' . $user->name . '</a> do thanh toán dịch vụ thiếu ' . number_format( (int) $payment->money - (int) $data['amount'] ,0,",",".") . ' VNĐ',
                      ];
                      $this->log_activity->create($data_log);

                      // gữi mail
                      try {
                        $data = [
                          'name' => $user->name,
                          'amount' => $data['amount'],
                          'ma_gd' => $create->ma_gd,
                          'credit' => $user->credit->value,
                          'payment' => $payment,
                          'method' => $data['type'],
                          'subject' => 'Nạp tiền vào tài khoản tại CLOUDZONE thành công'
                        ];

                        $data_tb_send_mail = [
                          'type' => 'finish_pay_error',
                          'user_id' => $user->id,
                          'status' => false,
                          'content' => serialize($data),
                        ];
                        $this->send_mail->create($data_tb_send_mail);

                      } catch (\Exception $e) {
                        report($e);
                        continue;
                      }
                    }
                }
              } else {
                $data_response = [
                    'error' => 1,
                    'status' => 'ID payment không phù hợp',
                    'data' => $data,
                ];
                return $data_response;
              }
          }
          $data_response = [
              'error' => 0,
              'status' => '',
              'data' => '',
          ];
          return $data_response;
        } catch (\Exception $e) {
            report($e);
            $data_response = [
                'error' => 2,
                'status' => 'Lỗi hệ thống',
                'data' => $list,
            ];
            return $data_response;
        }

    }
    // TODO: api lấy các thanh toán link: https://portal.cloudzone.vn/api/get_payment_unpaid (get)
    // TODO: thành phần api xác nhận thanh toán
    // link: https://portal.cloudzone.vn/api/confirm_payment (put)
    // [
    //   {
    //       "id" : 1 tỷ 100, // id của thanh toán
    //       "amount" : 100000, //Số tiền thanh toán
    //       "stc" : 34243242342, //Số tham chiếu
    //       "type" : "vietcombank" // loại ngân hàng
    //   },
    // ]

    public function update_confirm_payment_with_momo($momo)
    {
        $list_payment = $this->history_pay->where('status', 'Pending')->orWhere('status' , 'confirm')->orderBy('id', 'desc')->get();
        $check = false;
        foreach ($list_payment as $key => $payment) {
            $magd = $payment->ma_gd;
            if ( is_numeric(strpos( strtoupper($momo->content), $magd )) ) {
               if ( (int) $payment->money <= (int) $momo->amount ) {
                   $payment->magd_api = $momo->id_momo;
                   $payment->type_gd_api = 'momo';
                   $payment->save();
                   $user = $payment->user;
                   $user_credit = $user->credit;
                   $user_credit->value += (int) $momo->amount - (int) $payment->money;
                   $user_credit->save();
                   $this->confirm_payment($payment->id);
                   $data_log = [
                        'user_id' => 999999,
                        'action' => 'chỉnh sửa và tự động xác nhận dịch vụ',
                        'model' => 'CronTab/Confirm_payment',
                        'description' => ' <a target="_blank" href="/admin/users/detail/' . $payment->id . '">#' . $payment->id . '</a> bằng momo',
                    ];
                    $this->log_activity->create($data_log);
                    $check = true;
                }
                else {
                    if ($payment->type_gd == 1) {
                        $payment->money = $momo->amount;
                        $payment->save();
                        try {
                          $this->confirm_payment($payment->id);
                        } catch (\Exception $e) {
                          return false;
                        }
                        $data_log = [
                          'user_id' => 999999,
                          'action' => 'chỉnh sửa và tự động xác nhận dịch vụ',
                          'model' => 'CronTab/Confirm_payment',
                          'description' => ' <a target="_blank" href="/admin/users/detail/' . $payment->id . '">#' . $payment->id . '</a> bằng vietcombank',
                        ];
                        $this->log_activity->create($data_log);
                    }
                    $check = true;
                }
            }
        }
        return $check;
    }


    public function confirm_payment($id)
    {
        $payment = $this->history_pay->find($id);
        // ghi log
        $data_log = [
          'user_id' => !empty(Auth::user()->id) ? Auth::user()->id : 999999,
          'action' => 'xác nhận',
          'model' => 'Admin/HistoryPay',
          'description' => ' thanh toán <a target="_blank" href="/admin/payment/detail/' . $payment->id . '">#' . $payment->id . '</a> ',
        ];
        $this->log_activity->create($data_log);
        if ($payment->type_gd == 1) {
            $credit = $this->credit->where('user_id', $payment->user_id)->first();
            if (isset($credit)) {
                // ghi log giao dịch
                $data_log_payment = [
                  'history_pay_id' => $payment->id,
                  'before' => $credit->value,
                  'after' => $credit->value + $payment->money,
                ];
                $this->log_payment->create($data_log_payment);

                $credit->value += $payment->money;
                $credit->total += $payment->money;
                $credit->save();
            } else {
                // ghi log giao dịch
                $data_log_payment = [
                  'history_pay_id' => $payment->id,
                  'before' => 0,
                  'after' => $payment->money,
                ];
                $this->log_payment->create($data_log_payment);

                $data_credit = [
                    'user_id' => $payment->user_id,
                    'value' => $payment->money,
                    'total' => $payment->money,
                ];
                $this->credit->create($data_credit);
            }
            $payment->status = 'Active';
            $update = $payment->save();
            // ghi log
            $data_log = [
              'user_id' => !empty($payment->user_id) ? $payment->user_id : 999999,
              'action' => 'xác nhận',
              'model' => 'Admin/HistoryPay',
              'description' => ' nạp tiền thành công',
            ];
            $this->log_activity->create($data_log);
            try {
              $user = $this->user->find($payment->user_id);
              $type = '';
              if ( $payment->method_gd == 'pay_in_office' ) {
                $type = 'Trực tiếp tại văn phòng';
              }
              elseif ( $payment->method_gd == 'bank_transfer_vietcombank' ) {
                $type = 'ngân hàng Vietcombank';
              }
              elseif ( $payment->method_gd == 'bank_transfer_techcombank' ) {
                $type = 'ngân hàng Techcombank';
              }
              elseif ( $payment->method_gd == 'momo' ) {
                $type = 'MoMo';
              } else {
                $type = 'loại khác';
              }

              $mail_system = $this->email->where('type', 3)->where('type_service', 'finish_payment')->first();
              if ( isset($mail_system) ) {
                $search = [ '{$user_id}', '{$user_name}',  '{$user_email}', '{$user_credit}', '{$url}', '{$token}', '{$payment_code}', '{$payment_amount}', '{$payment_time}' ];
                $replace = [ $user->id, $user->name, $user->email, !empty($user->credit->value) ? number_format($user->credit->value,0,",",".") . ' VNĐ' : 0, url(''), '', $payment->ma_gd, number_format($payment->money,0,",",".") . ' VNĐ', date('d-m-Y') ];
                $content = str_replace($search, $replace, !empty($mail_system->meta_email->content)? $mail_system->meta_email->content : '');
                $data = [
                  'content' => $content,
                  'subject' => !empty($mail_system->meta_email->subject) ? $mail_system->meta_email->subject : 'Nạp tiền vào tài khoản tại CLOUDZONE thành công',
                ];
              } else {
                $data = [
                  'name' => $user->name,
                  'amount' => $payment->money,
                  'ma_gd' => $payment->ma_gd,
                  'credit' => $user->credit->value,
                  'payment' => $payment,
                  'method' => $type,
                  'subject' => 'Nạp tiền vào tài khoản tại CLOUDZONE thành công'
                ];
              }

              $data_tb_send_mail = [
                'type' => 'finish_payment',
                'user_id' => $user->id,
                'status' => false,
                'content' => serialize($data),
              ];
              $this->send_mail->create($data_tb_send_mail);

            } catch (\Exception $e) {
              report($e);
              return $update;
            }
            return $update;
        }
        elseif ($payment->type_gd == 2) {
            $payment->status = 'Active';
            $update = $payment->save();
            $update_invoice = $this->confirm_type_invoice($payment);
            try {
              $this->set_log_payment($payment);
            } catch (\Exception $e) {
              report($e);
              return $update_invoice;
            }
            return $update_invoice;
        }
        elseif ($payment->type_gd == 3) {
            $payment->status = 'Active';
            $update = $payment->save();
            $update_expired = $this->payment_expired($payment);
            try {
              $this->set_log_payment($payment);
            } catch (\Exception $e) {
              report($e);
              return $update_expired;
            }
            return $update_expired;
        }
        elseif ($payment->type_gd == 4) {
            $payment->status = 'Active';
            $update = $payment->save();
            $update_expired = $this->payment_addon($payment->detail_order_id);
            try {
              $this->set_log_payment($payment);
            } catch (\Exception $e) {
              report($e);
              return $update_expired;
            }
            return $update_expired;
        }
        elseif ($payment->type_gd == 5) {
            $payment->status = 'Active';
            $update = $payment->save();
            $update_change_ip = $this->payment_change_ip($payment->detail_order_id);
            try {
              $this->set_log_payment($payment);
            } catch (\Exception $e) {
              report($e);
              return $update_change_ip;
            }
            return $update_change_ip;
        }
        elseif ($payment->type_gd == 6) {
            $payment->status = 'Active';
            $update = $payment->save();
            $update_domain = $this->payment_domain($payment);
            try {
              $this->set_log_payment($payment);
            } catch (\Exception $e) {
              report($e);
              return $update_domain;
            }
            return $update_domain;
        }
        elseif ($payment->type_gd == 7) {
            $payment->status = 'Active';
            $update = $payment->save();
            $update_domain = $this->payment_domain_expired($payment);
            try {
              $this->set_log_payment($payment);
            } catch (\Exception $e) {
              report($e);
              return $update_domain;
            }
            return $update_domain;
        }
        elseif ($payment->type_gd == 8) {
            $payment->status = 'Active';
            $update = $payment->save();
            $update_domain = $this->payment_upgrade_hosting($payment->detail_order_id);
            try {
              $this->set_log_payment($payment);
            } catch (\Exception $e) {
              report($e);
              return $update_domain;
            }
            return $update_domain;
        }
    }

    public function set_log_payment($payment)
    {
        $credit = $this->credit->where('user_id', $payment->user_id)->first();
        if (isset($credit)) {
            // ghi log giao dịch
            $data_log_payment = [
              'history_pay_id' => $payment->id,
              'after' => $credit->value,
              'before' => $credit->value + $payment->money,
            ];
            $this->log_payment->create($data_log_payment);
        } else {
            // ghi log giao dịch
            $data_log_payment = [
              'history_pay_id' => $payment->id,
              'after' => 0,
              'before' => $payment->money,
            ];
            $this->log_payment->create($data_log_payment);
        }
    }
    // Confirm order
    public function confirm_type_invoice($payment)
    {
        $invoice = $this->detail_order->find($payment->detail_order_id);
        $order = $this->order->find($invoice->order->id);
        $total = $order->total;
        $order->status = 'Active';
        $order->save();
        $credit = $this->credit->where('user_id', $payment->user_id)->first();
        if (!isset($credit)) {
            return false;
        }
        // HistoryPay
        $payment->status = 'Active';
        $payment->save();
        // invoice
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        if ($invoice->type == 'VPS') {
            $list_vps = $this->vps->where('detail_order_id', $invoice->id)->get();
            $billing_cycle = $list_vps[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$list_vps[0]->billing_cycle] ));
            // dd($due_date);

            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            $reques_vcenter = $this->dashboard->createVPS($list_vps, $due_date); //request đên Vcenter
            // $reques_vcenter = true;
            if ($reques_vcenter) {
                $order->status = 'Finish';
                $order->save();
                $this->send_mail_finish('vps',$order, $invoice ,$list_vps);
                // ghi vào tổng thu
                if (!empty($this->total_price->first())) {
                    $total_price = $this->total_price->first();
                    $total_price->vps += $total;
                    $total_price->save();
                } else {
                    $data_total_price = [
                        'vps' => $total,
                        'hosting' => '0',
                        'server' => '0',
                    ];
                    $this->total_price->create($data_total_price);
                }
            } else {
                return false;
            }

        }
        elseif ($invoice->type == 'NAT-VPS') {
            $list_vps = $this->vps->where('detail_order_id', $invoice->id)->get();
            $billing_cycle = $list_vps[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$list_vps[0]->billing_cycle] ));
            // dd($due_date);

            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            $reques_vcenter = $this->dashboard->createNatVPS($list_vps, $due_date); //request đên Vcenter
            // $reques_vcenter = true;
            if ($reques_vcenter) {
                $order->status = 'Finish';
                $order->save();
                $this->send_mail_finish('vps',$order, $invoice ,$list_vps);
                // ghi vào tổng thu
                if (!empty($this->total_price->first())) {
                    $total_price = $this->total_price->first();
                    $total_price->vps += $total;
                    $total_price->save();
                } else {
                    $data_total_price = [
                        'vps' => $total,
                        'hosting' => '0',
                        'server' => '0',
                    ];
                    $this->total_price->create($data_total_price);
                }
            } else {
                return false;
            }
        }
        elseif ($invoice->type == 'VPS-US') {
            $list_vps = $invoice->vps;
            $billing_cycle = $list_vps[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$list_vps[0]->billing_cycle] ));
            // dd($due_date);

            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();

            $reques_vcenter = $this->dashboard->createVpsUs($list_vps); //request đên Vcenter
            // $reques_vcenter["error"] = 0;
            if ( $reques_vcenter["error"] = 0 || $reques_vcenter["error"] = 1 ) {
                $order->status = 'Finish';
                $order->save();
                $this->send_mail_finish('vps',$order, $invoice ,$list_vps);
                // ghi vào tổng thu
                if (!empty($this->total_price->first())) {
                    $total_price = $this->total_price->first();
                    $total_price->vps += $total;
                    $total_price->save();
                } else {
                    $data_total_price = [
                        'vps' => $total,
                        'hosting' => '0',
                        'server' => '0',
                    ];
                    $this->total_price->create($data_total_price);
                }
            } else {
                return false;
            }
        }
        elseif ($invoice->type == 'Server') {
            $servers = $this->server->where('detail_order_id', $invoice->id)->get();
            $billing_cycle = $servers[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$servers[0]->billing_cycle] ));
            foreach ($servers as $key => $server) {
                $server->next_due_date = $due_date;
                $server->paid = 'paid';
                $server->status = 'Active';
                $server->status_server = 'progressing';
                $server->date_create = date('Y-m-d');
                $server->save();
            }
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();

            $order->status = 'Finish';
            $order->save();
            // gữi mail
            // ghi vào tổng thu
            $this->send_mail_finish('server',$order, $invoice ,$servers);
        }
        elseif ($invoice->type == 'Colocation') {
            $colocations = $this->colocation->where('detail_order_id', $invoice->id)->get();
            $billing_cycle = $colocations[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$colocations[0]->billing_cycle] ));
            foreach ($colocations as $key => $colocation) {
                $colocation->next_due_date = $due_date;
                $colocation->paid = 'paid';
                $colocation->status = 'Active';
                $colocation->status_colo = 'on';
                $colocation->date_create = date('Y-m-d');
                $colocation->save();
            }
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();

            $order->status = 'Finish';
            $order->save();
            // gữi mail
            // ghi vào tổng thu
            if (!empty($this->total_price->first())) {
                $total_price = $this->total_price->first();
                $total_price->server += $total;
                $total_price->save();
            } else {
                $data_total_price = [
                    'vps' => '0',
                    'hosting' => '0',
                    'server' => $total,
                ];
                $this->total_price->create($data_total_price);
            }
        }
        elseif ($invoice->type == 'Hosting-Singapore') {
            $hostings = $this->hosting->where('detail_order_id', $invoice->id)->get();

            $billing_cycle = $hostings[0]->billing_cycle;
            // Dành cho những hosting one_time_pay
            if ($billing_cycle == 'one_time_pay') {
                $date_paid = date('Y-m-d');
                $invoice->due_date = '2099-12-31';
                $invoice->paid_date = $date_paid;
                $invoice->status = 'paid';
                $invoice->payment_method = 1;

                $request_da = $this->da_si->orderCreateAccount($hostings, 'one_time_pay'); //request đên Direct Admin
                $paid = $invoice->save();

                if ($request_da) {
                    $order->status = 'Finish';
                    $order->save();
                    $this->send_mail_finish('email_hosting',$order, $invoice ,$hostings);
                    // ghi vào tổng thu
                    if (!empty($this->total_price->first())) {
                        $total_price = $this->total_price->first();
                        $total_price->hosting += $total;
                        $total_price->save();
                    } else {
                        $data_total_price = [
                            'vps' => '0',
                            'hosting' => $total,
                            'server' => '0',
                        ];
                        $this->total_price->create($data_total_price);
                    }
                }
                return true;
            }
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$hostings[0]->billing_cycle] ));
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;

            $request_da = $this->da_si->orderCreateAccount($hostings,$due_date); //request đên Direct Admin
            $paid = $invoice->save();
            if ($request_da) {
                $order->status = 'Finish';
                $order->save();
                $this->send_mail_finish('hosting',$order, $invoice ,$hostings);
                // ghi vào tổng thu
                if (!empty($this->total_price->first())) {
                    $total_price = $this->total_price->first();
                    $total_price->hosting += $total;
                    $total_price->save();
                } else {
                    $data_total_price = [
                        'vps' => '0',
                        'hosting' => $total,
                        'server' => '0',
                    ];
                    $this->total_price->create($data_total_price);
                }
            }
        }
        elseif($invoice->type == 'addon_vps') {
            return false;
        }
        elseif ( $invoice->type == 'Email Hosting' ) {
          $email_hostings = $this->email_hosting->where('detail_order_id', $invoice->id)->get();

          $billing_cycle = $email_hostings[0]->billing_cycle;
          $date_star = $invoice->created_at;
          $date_paid = date('Y-m-d');
          $due_date = date('Y-m-d', strtotime( $date_star . $billing[$email_hostings[0]->billing_cycle] ));
          $invoice->due_date = $due_date;
          $invoice->paid_date = $date_paid;
          $invoice->status = 'paid';
          $invoice->payment_method = 1;
          foreach ($email_hostings as $key => $email_hosting) {
              $email_hosting->next_due_date = $due_date;
              $email_hosting->paid = 'paid';
              $email_hosting->status = 'Active';
              $email_hosting->status_hosting = 'on';
              $email_hosting->date_create = date('Y-m-d');
              $email_hosting->save();
          }
          $request_da = true; //request đên Direct Admin
          $paid = $invoice->save();
          if ($request_da) {
              $order->status = 'Finish';
              $order->save();
              $this->send_mail_finish('email_hosting',$order, $invoice ,$email_hostings);
              // ghi vào tổng thu
              if (!empty($this->total_price->first())) {
                  $total_price = $this->total_price->first();
                  $total_price->hosting += $total;
                  $total_price->save();
              } else {
                  $data_total_price = [
                      'vps' => '0',
                      'hosting' => $total,
                      'server' => '0',
                  ];
                  $this->total_price->create($data_total_price);
              }
          }
        }
        else {
            $hostings = $this->hosting->where('detail_order_id', $invoice->id)->get();

            $billing_cycle = $hostings[0]->billing_cycle;
            // Dành cho những hosting one_time_pay
            if ($billing_cycle == 'one_time_pay') {
                $date_paid = date('Y-m-d');
                $invoice->due_date = '2099-12-31';
                $invoice->paid_date = $date_paid;
                $invoice->status = 'paid';
                $invoice->payment_method = 1;

                $request_da = $this->da->orderCreateAccount($hostings, 'one_time_pay'); //request đên Direct Admin
                $paid = $invoice->save();

                if ($request_da) {
                    $order->status = 'Finish';
                    $order->save();
                    $this->send_mail_finish('hosting',$order, $invoice ,$hostings);
                    // ghi vào tổng thu
                    if (!empty($this->total_price->first())) {
                        $total_price = $this->total_price->first();
                        $total_price->hosting += $total;
                        $total_price->save();
                    } else {
                        $data_total_price = [
                            'vps' => '0',
                            'hosting' => $total,
                            'server' => '0',
                        ];
                        $this->total_price->create($data_total_price);
                    }
                }
                return true;
            }
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$hostings[0]->billing_cycle] ));
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;

            $request_da = $this->da->orderCreateAccount($hostings,$due_date); //request đên Direct Admin
            $paid = $invoice->save();
            // $request_da = true;
            if ($request_da) {
                $order->status = 'Finish';
                $order->save();
                $this->send_mail_finish('hosting',$order, $invoice ,$hostings);
                // ghi vào tổng thu
                if (!empty($this->total_price->first())) {
                    $total_price = $this->total_price->first();
                    $total_price->hosting += $total;
                    $total_price->save();
                } else {
                    $data_total_price = [
                        'vps' => '0',
                        'hosting' => $total,
                        'server' => '0',
                    ];
                    $this->total_price->create($data_total_price);
                }
            }


        }
        return $paid;
    }
    // Gữi email khi hoàn thành tạo các services trong order
    public function send_mail_finish($type, $order , $detail_order, $list_sevices)
    {
        $billings = config('billing');
        $product = $this->product->find($list_sevices[0]->product_id);
        $email = $this->email->find($product->meta_product->email_id);
        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $add_on =  '';
        if (!empty($detail_order->addon_id)) {
            $add_on = $this->product->find($list_sevices[0]->addon_id);
        }
        if ($type == 'vps') {
            foreach ($list_sevices as $key => $services) {
                $user = $this->user->find($services->user_id);
                $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' , $detail_order->quantity, $services->os, '', $url];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'subject' => $email->meta_email->subject,
                ];

                $data_tb_send_mail = [
                  'type' => 'mail_finish_order_vps',
                  'type_service' => 'vps',
                  'user_id' => $user->id,
                  'status' => false,
                  'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);
            }
        }
        elseif ($type == 'hosting') {
            foreach ($list_sevices as $key => $services) {
              if (!empty($services->server_hosting_id)) {
                 $link_control_panel = '<a href="https://' . $services->server_hosting->host . ':' . $services->server_hosting->port . '" target="_blank" >https://'. $services->server_hosting->host . ':' . $services->server_hosting->port . '</a>' ;
              } else {
                 $link_control_panel = '<a href="https://daportal.cloudzone.vn:2222/" target="_blank">https://daportal.cloudzone.vn:2222/</a>';
              }
              $user = $this->user->find($services->user_id);
                  $search = ['{$url_control_panel}' , '{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' ,
                  '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}',
                  '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}',
                  '{$url}'];
                  $replace = [
                      $link_control_panel, !empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address ,
                      $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)),
                      $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,
                      number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url ,
                  ];



                  $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                  $this->user_send_email = $this->user->find($services->user_id)->email;
                  $this->subject = $email->meta_email->subject;

                  $data = [
                      'content' => $content,
                      'user_name' => $user->name,
                      'subject' => $email->meta_email->subject,
                  ];

                  $data_tb_send_mail = [
                    'type' => 'mail_finish_order_hosting',
                    'type_service' => 'hosting',
                    'user_id' => $user->id,
                    'status' => false,
                    'content' => serialize($data),
                  ];
                  $this->send_mail->create($data_tb_send_mail);
            }
        }
        elseif ($type == 'email_hosting') {
            foreach ($list_sevices as $key => $services) {
              $link_control_panel = '';
              $user = $this->user->find($services->user_id);
              $search = ['{$url_control_panel}' , '{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' ,
              '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}',
              '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}',
              '{$url}'];
              $replace = [
                $link_control_panel, !empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address ,
                $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)),
                $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,
                number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url ,
              ];

              $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

              $this->user_send_email = $this->user->find($services->user_id)->email;
              $this->subject = $email->meta_email->subject;

              $data = [
                'content' => $content,
                'user_name' => $user->name,
                'user_id' => $user->id,
                'user_email' => $user->email,
                'user_addpress' => $user->user_meta->address,
                'user_phone' => $user->user_meta->user_phone,
                'order_id' => $order->id,
                'product_name' => $product->name,
                'domain' => '',
                'ip' => $services->ip,
                'billing_cycle' => $services->billing_cycle,
                'next_due_date' => $services->next_due_date,
                'order_created_at' => $order->created_at,
                'services_username' => $services->user,
                'services_password' => $services->password,
                'sub_total' => $detail_order->sub_total,
                'total' => $order->total,
                'qtt' => $detail_order->quantity,
                'os' => $services->os,
                'token' => '',
              ];

              $mail = Mail::send('users.mails.orders', $data, function($message){
                $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                $message->to($this->user_send_email)->subject($this->subject);
              });
            }
        }
        elseif ($type == 'server') {
          $user = $order->user;
          $server_cpu = $product->meta_product->cpu . ' (' . $product->meta_product->cores . ')';
          $server_disk = $product->meta_product->disk . ' ';
          $server_disk .= $this->get_addon_disk_server($list_sevices[0]->id);
          $search = ['{$user_name}',  '{$user_email}' , '{$order_id}',  '{$product_name}', '{$billing_cycle}',
            '{$order_created_at}', '{$sub_total}', '{$total}', '{$qtt}', '{$server_os}', '{$url}', '{$server_cpu}', '{$server_ram}',
            '{$server_disk}', '{$server_qtt_ip}', '{$server_raid}', '{$server_datacenter}', '{$server_management}', '{$next_due_date}'
          ];
          $replace = [ $user->name,  $user->email , $order->id,  $product->name ,$billings[$list_sevices[0]->billing_cycle], $order->created_at,
            number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' ,
            $detail_order->quantity, $list_sevices[0]->os, $url, $server_cpu, $product->meta_product->memory, $server_disk,
            $product->meta_product->ip, $list_sevices[0]->raid, $list_sevices[0]->location, $list_sevices[0]->server_management,
            date('d-m-Y', strtotime($list_sevices[0]->next_due_date))
          ];
          $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
          try {
              $data = [
                  'content' => $content,
                  'user_name' => $user->name,
                  'subject' => $email->meta_email->subject,
              ];
              $data_tb_send_mail = [
                  'type' => 'mail_finish_order_server',
                  'type_service' => 'server',
                  'user_id' => $user->id,
                  'status' => false,
                  'content' => serialize($data),
              ];
              $this->send_mail->create($data_tb_send_mail);
          } catch (\Exception $e) {
              report($e);
          }
        }
    }

    public function get_addon_disk_server($serverId)
    {
        $server = $this->server->find($serverId);
        if ( !empty($server->server_config) ) {
          $text_config = '';
          $server_config = $server->server_config;
          $data_config = [];
          if ( !empty($server_config->disk2) ) {
            $data_config[] = [
              'id' => $server_config->disk2,
              'name' => $server_config->product_disk2->name,
              'qtt' => 1
            ];
          }
          if ( !empty($server_config->disk3) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk3) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk3,
                'name' => $server_config->product_disk3->name,
                'qtt' => 1
              ];
            }
          }
          if ( !empty($server_config->disk4) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk4) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk4,
                'name' => $server_config->product_disk4->name,
                'qtt' => 1
              ];
            }
          }
          if ( !empty($server_config->disk5) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk5) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk5,
                'name' => $server_config->product_disk5->name,
                'qtt' => 1
              ];
            }
          }
          if ( !empty($server_config->disk6) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk6) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk6,
                'name' => $server_config->product_disk6->name,
                'qtt' => 1
              ];
            }
          }
          if ( !empty($server_config->disk7) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk7) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk7,
                'name' => $server_config->product_disk7->name,
                'qtt' => 1
              ];
            }
          }
          if ( !empty($server_config->disk8) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk8) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk8,
                'name' => $server_config->product_disk8->name,
                'qtt' => 1
              ];
            }
          }
          if ( count($data_config) ) {
            $text_config = '(Addon ';
            foreach ($data_config as $key => $data) {
              $text_config .= ( $data['qtt'] . ' x ' . $data['name']  );
              if ( count( $data_config ) - 1 > $key ) {
                $text_config .= ' - ';
              }
            }
            $text_config .= ')';
          }
          return $text_config;
        } else {
          return '';
        }
    }
    // Gia hạn
    // Thanh toán khi gia hạn
    public function payment_expired($payment)
    {
        $invoice = $this->detail_order->find($payment->detail_order_id);
        $order = $this->order->find($invoice->order->id);
        $total = $order->total;
        $order->status = 'Active';
        $order->save();
        // invoice
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        $order_expireds = $invoice->order_expireds;
        if ($invoice->type == 'VPS') {
            //
            $billing_cycle = $order_expireds[0]->billing_cycle;
            foreach ($order_expireds as $key => $order_expired) {
                $vps = $order_expired->vps;
                $date_star = $vps->next_due_date;
                $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expired->billing_cycle] ));

                // $vps->next_due_date = $due_date;
                // $vps->billing_cycle = $order_expired->billing_cycle;
                // $vps->save();

                $config_billing_DashBoard = config('billingDashBoard');
                $data_expired[] = [
                    'package' => (int) $invoice->sub_total,
                    'leased_time' => $config_billing_DashBoard[$order_expired->billing_cycle],
                    'type' => ($vps->type_vps == 'nat_vps') ? 5 : 0,
                    'vm_id' => $vps->vm_id,
                    'end_date' => $due_date
                ];
            }
            $reques_vcenter = $this->dashboard->expired_vps($data_expired, $vps->vm_id); //request đên Vcenter
            // $reques_vcenter = true;
            if ( $reques_vcenter ) {
                // dd($reques_vcenter); $reques_vcenter->error == 0
                if ($reques_vcenter->error == 0) {
                    foreach ($order_expireds as $key => $order_expired) {
                      $vps = $order_expired->vps;
                      $date_star = $vps->next_due_date;
                      $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expired->billing_cycle] ));

                      $vps->next_due_date = $due_date;
                      $vps->billing_cycle = $order_expired->billing_cycle;
                      if ( $vps->status_vps == 'expire' ) {
                          $vps->status_vps = 'on';
                      }
                      $vps->save();
                    }
                    $date_star = $invoice->created_at;
                    $date_paid = date('Y-m-d');
                    $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expireds[0]->billing_cycle] ));
                    // dd($due_date)
                    $payment->status = 'Active';
                    $payment->save();
                    // Invoice
                    $invoice->due_date = $due_date;
                    $invoice->paid_date = $date_paid;
                    $invoice->status = 'paid';
                    $invoice->payment_method = 1;
                    $paid = $invoice->save();
                    $order->status = 'Finish';
                    $order->save();
                    $this->send_mail_finish_expired('vps',$order, $invoice ,$order_expireds);
                    // ghi vào tổng thu
                    if (!empty($this->total_price->first())) {
                        $total_price = $this->total_price->first();
                        $total_price->vps += $total;
                        $total_price->save();
                    } else {
                        $data_total_price = [
                            'vps' => $total,
                            'hosting' => '0',
                            'server' => '0',
                        ];
                        $this->total_price->create($data_total_price);
                    }
                } else {
                    $list_success = $reques_vcenter->list_success;
                    $list_error = $reques_vcenter->list_error;
                    $this->expired_success($list_success, $invoice->order_expireds[0]->billing_cycle);
                    $this->expired_error($list_error, $invoice->order_expireds[0]->billing_cycle);
                    try {
                      $invoice->order->delete();
                      foreach ($invoice->history_pay as $key => $history_pay) {
                          $history_pay->delete();
                      }
                      foreach ($order_expireds as $key => $order_expired) {
                          $order_expired->delete();
                      }
                      $invoice->delete();
                    } catch (\Exception $e) {
                      return true;
                    }
                }
            } else {
              return false;
            }

        }
        elseif ($invoice->type == 'VPS US') {
            $billing_cycle = $order_expireds[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expireds[0]->billing_cycle] ));
            // dd($due_date)
            $payment->status = 'Active';
            $payment->save();
            // Invoice
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            //
            foreach ($order_expireds as $key => $order_expired) {
                $vps = $order_expired->vps;
                $config_billing_DashBoard = config('billingDashBoard');
                $data_expired = [
                    'mob' => 'renew',
                    'package' => (int) $invoice->sub_total,
                    'leased_time' => $config_billing_DashBoard[$order_expired->billing_cycle],
                    'end_date' => $due_date,
                ];
            }
            $reques_vcenter = $this->dashboard->expired_vps_us($data_expired, $vps->vm_id); //request đên Vcenter
            // $reques_vcenter = true;
            if ( $reques_vcenter ) {
              // dd($reques_vcenter); $reques_vcenter->error == 0
              if ($reques_vcenter->error == 0) {
                  foreach ($order_expireds as $key => $order_expired) {
                    $vps = $order_expired->vps;
                    $date_star = $vps->next_due_date;
                    $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expired->billing_cycle] ));

                    $vps->next_due_date = $due_date;
                    $vps->billing_cycle = $order_expired->billing_cycle;
                    if ( $vps->status_vps == 'expire' ) {
                        $vps->status_vps = 'on';
                    }
                    $vps->save();
                  }
                  $date_star = $invoice->created_at;
                  $date_paid = date('Y-m-d');
                  $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expireds[0]->billing_cycle] ));
                  // dd($due_date)
                  $payment->status = 'Active';
                  $payment->save();
                  // Invoice
                  $invoice->due_date = $due_date;
                  $invoice->paid_date = $date_paid;
                  $invoice->status = 'paid';
                  $invoice->payment_method = 1;
                  $paid = $invoice->save();
                  $order->status = 'Finish';
                  $order->save();
                  $this->send_mail_finish_expired('vps',$order, $invoice ,$order_expireds);
                  // ghi vào tổng thu
                  if (!empty($this->total_price->first())) {
                      $total_price = $this->total_price->first();
                      $total_price->vps += $total;
                      $total_price->save();
                  } else {
                      $data_total_price = [
                          'vps' => $total,
                          'hosting' => '0',
                          'server' => '0',
                      ];
                      $this->total_price->create($data_total_price);
                  }
              } else {
                  $list_success = $reques_vcenter->list_success;
                  $list_error = $reques_vcenter->list_error;
                  $this->expired_success($list_success, $invoice->order_expireds[0]->billing_cycle);
                  $this->expired_error($list_error, $invoice->order_expireds[0]->billing_cycle);
                  try {
                    $invoice->order->delete();
                    foreach ($invoice->history_pay as $key => $history_pay) {
                        $history_pay->delete();
                    }
                    foreach ($order_expireds as $key => $order_expired) {
                        $order_expired->delete();
                    }
                    $invoice->delete();
                  } catch (\Exception $e) {
                    return true;
                  }
              }
            } else {
              return false;
            }
        }
        elseif ($invoice->type == 'Server') {
            $billing_cycle = $order_expireds[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expireds[0]->billing_cycle] ));
            foreach ($order_expireds as $key => $order_expired) {
                $server = $order_expired->server;
                $date_start_server = $server->next_due_date;
                $due_date_server = date('Y-m-d', strtotime( $date_start_server . $billing[$order_expired->billing_cycle] ));
                $server->next_due_date = $due_date_server;
                $server->billing_cycle = $order_expired->billing_cycle;
                if ( $server->status_server == 'expire' ) {
                    $server->status_server = 'on';
                }
                $server->save();
            }
            // HistoryPay
            $payment->status = 'Active';
            $payment->save();
            // invoice
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            $order->status = 'Finish';
            $order->save();
            // ghi vào tổng thu
            $this->send_mail_finish_expired('server',$order, $invoice ,$order_expireds);
        }
        elseif ($invoice->type == 'Colocation') {
            $billing_cycle = $order_expireds[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expireds[0]->billing_cycle] ));
            foreach ($order_expireds as $key => $order_expired) {
                $colocation = $order_expired->colocation;
                $date_start_colocation = $colocation->next_due_date;
                $due_date_colocation = date('Y-m-d', strtotime( $date_start_colocation . $billing[$order_expired->billing_cycle] ));
                $amount = $colocation->amount;
                if ( $colocation->billing_cycle == $order_expired->billing_cycle ) {
                    $sub_total_colocation = $amount;
                } else {
                    $sub_total_colocation = $amount *  ( $billingDashBoard[$order_expired->billing_cycle] ) / ( $billingDashBoard[$colocation->billing_cycle] ) ;
                }
                $colocation->next_due_date = $due_date_colocation;
                $colocation->billing_cycle = $order_expired->billing_cycle;
                $colocation->amount = $sub_total_colocation;
                $colocation->save();
            }
            // HistoryPay
            $payment->status = 'Active';
            $payment->save();
            // invoice
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            $order->status = 'Finish';
            $order->save();
            // ghi vào tổng thu
            if (!empty($this->total_price->first())) {
                $total_price = $this->total_price->first();
                $total_price->server += $total;
                $total_price->save();
            } else {
                $data_total_price = [
                    'vps' => '0',
                    'hosting' => '0',
                    'server' => $total,
                ];
                $this->total_price->create($data_total_price);
            }
        }
        elseif ($invoice->type == 'Email Hosting') {
          // HistoryPay\
          $payment->status = 'Active';
          $payment->save();
          // dd($hostings);
          $billing_cycle = $order_expireds[0]->billing_cycle;
          // Dành cho những hosting one_time_pay
          // invoice
          $date_star = $invoice->created_at;
          $date_paid = date('Y-m-d');
          $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expireds[0]->billing_cycle] ));
          $invoice->due_date = $due_date;
          $invoice->paid_date = $date_paid;
          $invoice->status = 'paid';
          $invoice->payment_method = 1;
          // $request_da = $this->da->orderCreateAccount($hostings,$due_date); //request đên Direct Admin
          $paid = $invoice->save();

          foreach ($order_expireds as $key => $order_expired) {
              $email_hosting = $order_expired->email_hosting;
              $date_star = $email_hosting->next_due_date;
              $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expired->billing_cycle] ));

              $email_hosting->next_due_date = $due_date;
              $email_hosting->save();
          }

          $order->status = 'Finish';
          $order->save();
          $this->send_mail_finish_expired('email_hosting',$order, $invoice ,$order_expireds);
          // ghi vào tổng thu
          if (!empty($this->total_price->first())) {
              $total_price = $this->total_price->first();
              $total_price->hosting += $total;
              $total_price->save();
          } else {
              $data_total_price = [
                  'vps' => '0',
                  'hosting' => $total,
                  'server' => '0',
              ];
              $this->total_price->create($data_total_price);
          }

        }
        else {
            // HistoryPay\
            $payment->status = 'Active';
            $payment->save();
            // dd($hostings);
            $billing_cycle = $order_expireds[0]->billing_cycle;
            // Dành cho những hosting one_time_pay
            // invoice
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expireds[0]->billing_cycle] ));
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            // $request_da = $this->da->orderCreateAccount($hostings,$due_date); //request đên Direct Admin
            $paid = $invoice->save();

            foreach ($order_expireds as $key => $order_expired) {
                $hosting = $order_expired->hosting;
                $date_star = $hosting->next_due_date;
                $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expired->billing_cycle] ));

                $hosting->next_due_date = $due_date;
                $hosting->save();
            }

            $order->status = 'Finish';
            $order->save();
            $this->send_mail_finish_expired('hosting',$order, $invoice ,$order_expireds);
            // ghi vào tổng thu
            if (!empty($this->total_price->first())) {
                $total_price = $this->total_price->first();
                $total_price->hosting += $total;
                $total_price->save();
            } else {
                $data_total_price = [
                    'vps' => '0',
                    'hosting' => $total,
                    'server' => '0',
                ];
                $this->total_price->create($data_total_price);
            }


        }
        return true;
    }
    // tạo 1 order khi gia hạn thành công
    public function expired_success($list_vm_id, $billing_cycle)
    {
        $total = 0;
        $quantity = 0;
        $billing_price = config('billingDashBoard');
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        foreach ($list_vm_id as $key => $vm_id) {
            $vps = $this->vps->where('vm_id', $vm_id)->first();
            $user = $vps->user_vps;
            if (!empty($vps)) {
                $product = $this->product->find($vps->product_id);
                if (!empty($vps->price_override)) {
                    if ($vps->billing_cycle == $billing_cycle) {
                        $total += $vps->price_override;
                    } else {
                      $total_price_1 = ($vps->price_override * $billing_price[$billing_cycle]) / $billing_price[$vps->billing_cycle];
                      $total += (int) round( $total_price_1 , -3);
                    }
                } else {
                    $total += $product->pricing[$billing_cycle];
                    if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($user->id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                            if (!empty($add_on_product->meta_product->type_addon)) {
                                if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                  $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                  $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                  $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                                }
                            }
                        }
                        $total += $pricing_addon;
                    }
                }
            }
            $quantity++;
        }
        $data_order = [
            'user_id' => $user->id,
            'total' => $total,
            'status' => 'Finish',
            'description' => 'expired vps',
            'type' => 'expired',
            'makh' => '',
        ];
        $order = $this->order->create($data_order);
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => 'VPS' . (($vps->location == 'us') ? " US" : ""),
            'due_date' => $date,
            'paid_date' => date('Y-m-d'),
            'description' => 'Gia hạn VPS' . (($vps->location == 'us') ? " US" : ""),
            'status' => 'paid',
            'sub_total' => $total / $quantity,
            'quantity' => $quantity,
            'user_id' => $user->id,
            'paid_date' => date('Y-m-d'),
            'addon_id' => '0',
            'payment_method' => 1,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '3',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Active',
            'method_gd_invoice' => 'credit',
            'detail_order_id' => $detail_order->id,
        ];
        $history_pay = $this->history_pay->create($data_history);
        // ghi log giao dịch
        $credit = $this->credit->where('user_id', $user->id)->first();
        if (!isset($credit)) {
            return false;
        }
        $data_log_payment = [
          'history_pay_id' => $history_pay->id,
          'before' => $credit->value,
          'after' => $credit->value - $total,
        ];
        $this->log_payment->create($data_log_payment);
        // credit
        $credit->value = $credit->value - $total;
        $credit->save();
        $credit = $this->credit->where('user_id', $user->id)->first();
        foreach ($list_vm_id as $key => $vm_id) {
            $vps = $this->vps->where('vm_id', $vm_id)->first();
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $vps->id,
                'billing_cycle' => $billing_cycle,
                'type' => 'vps',
            ];
            $this->order_expired->create($data_order_expired);

            $date_star = $vps->next_due_date;
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$billing_cycle] ));

            $vps->next_due_date = $due_date;
            $vps->billing_cycle = $billing_cycle;
            $vps->save();
            // ghi log
            $billings = config('billing');
            $data_log = [
              'user_id' => $user->id,
              'action' => 'thanh toán',
              'model' => 'User/HistoryPay',
              'description' => ' gia hạn VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$billing_cycle],
              'description_user' => ' gia hạn VPS <a target="_blank" href="/order/check-invoices/' . $detail_order->id . '">' . $detail_order->id . '</a> '  . $billings[$billing_cycle] . ' thành công',
              'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log);
            // ghi log user
            $data_log_user = [
              'user_id' => $vps->user_id,
              'action' => 'gia hạn',
              'model' => 'User/Services',
              'description_user' => ' VPS <a target="_blank" href="/service/detail/' . $vps->id  . '?type=vps">' . $vps->ip . '</a>',
              'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log_user);
        }
        try {
          $this->send_mail_finish_expired('vps',$order, $detail_order , $detail_order->order_expireds);
        } catch (\Exception $e) {
          return $detail_order->id;
        }
        return $detail_order->id;
    }
    // tạo 1 order khi gia hạn thất bại
    public function expired_error($list_vm_id, $billing_cycle)
    {
        $total = 0;
        $quantity = 0;
        $billing_price = config('billingDashBoard');
        foreach ($list_vm_id as $key => $vm_id) {
            $vps = $this->vps->where('vm_id', $vm_id)->first();
            $user = $vps->user_vps;
            if (!empty($vps)) {
                $product = $this->product->find($vps->product_id);
                if (!empty($vps->price_override)) {
                    if ($vps->billing_cycle == $billing_cycle) {
                        $total += $vps->price_override;
                    } else {
                      $total_price_1 = ($vps->price_override * $billing_price[$billing_cycle]) / $billing_price[$vps->billing_cycle];
                      $total += (int) round( $total_price_1 , -3);
                    }
                } else {
                    $total += $product->pricing[$billing_cycle];
                    if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($user->id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                            if (!empty($add_on_product->meta_product->type_addon)) {
                                if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                  $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                  $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                  $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                                }
                            }
                        }
                        $total += $pricing_addon;
                    }
                }
            }
            $quantity++;
        }

        $data_order = [
            'user_id' => $user->id,
            'total' => $total,
            'status' => 'Pending',
            'description' => 'Gia hạn',//thiếu status gia hạn
            'type' => 'expired',
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => 'VPS' . (($vps->location == 'us') ? " US" : ""),
            'due_date' => $date,
            'description' => 'Gia hạn',
            'status' => 'unpaid',
            'sub_total' => $total / $quantity,
            'quantity' => $quantity,
            'user_id' => $user->id,
            'addon_id' => 0,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        foreach ($list_vm_id as $key => $vm_id) {
            $vps = $this->vps->where('vm_id', $vm_id)->first();
            // Tạo lưu trữ khi gia hạn
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $vps->id,
                'billing_cycle' => $billing_cycle,
                'type' => 'vps',
            ];
            $this->order_expired->create($data_order_expired);

            // if (!empty($vps)) {
            //     $vps->expired_id = $detail_order->id;
            //     $vps->billing_cycle = $billing_cycle;
            //     $vps->save();
            // }
        }
        // create history pay
        $user = $this->user->find($user->id);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '3',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $history_pay = $this->history_pay->create($data_history);
        // gui mail
        if ($order && $detail_order && $data_history) {

            $email = $this->email->find($product->meta_product->email_expired);

            $billings = config('billing');
            // ghi log
            $data_log = [
              'user_id' => $user->id,
              'action' => 'đặt hàng',
              'model' => 'User/Order_Services',
              'description' => ' gia hạn VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
              'description_user' => ' gia hạn VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
              'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log);

            $url = url('');
            $url = str_replace(['http://','https://'], ['', ''], $url);
            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', '' ,$billings[$data_order_expired['billing_cycle']], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , $quantity, '', 'token', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
            $data = [
                'content' => $content,
                'user_name' => $user->name,
            ];
            try {
              $this->send_mail(!empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận gia hạn VPS', $data, $user);
            } catch (\Exception $e) {
              return $detail_order->id;
            }

            return $detail_order->id;
        } else {
            return false;
        }
    }
    public function send_mail($subject, $data, $user)
    {
        try {
          $data['subject'] = $subject;
          $data_tb_send_mail = [
            'type' => 'mail_order_vps',
            'type_service' => 'vps',
            'user_id' => $user->id,
            'status' => false,
            'content' => serialize($data),
          ];
          $this->send_mail->create($data_tb_send_mail);
        } catch (\Throwable $th) {
          //throw $th;
          report($th);
        }
    }
    // gữi mail khi hoàn thành gia hạn
    public function send_mail_finish_expired($type, $order , $detail_order, $order_expireds)
    {
        $billings = config('billing');
        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $add_on =  '';
        if ($type == 'vps') {
            foreach ($order_expireds as $key => $order_expired) {
                $services = $order_expired->vps;
                $product = $this->product->find($services->product_id);
                $email = $this->email->find($product->meta_product->email_expired_finish);
                $user = $this->user->find($services->user_id);
                $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' , $detail_order->quantity, $services->os, '', $url];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                $data = [
                  'content' => $content,
                  'user_name' => $user->name,
                  'subject' => $email->meta_email->subject
                ];
                $data_tb_send_mail = [
                  'type' => 'mail_finish_order_expire_vps',
                  'type_service' => 'vps',
                  'user_id' => $user->id,
                  'status' => false,
                  'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);
            }
        }
        elseif ($type == 'hosting') {
            foreach ($order_expireds as $key => $order_expired) {
                  $services = $order_expired->hosting;
                  $product = $this->product->find($services->product_id);
                  $email = $this->email->find($product->meta_product->email_expired_finish);
                  $user = $this->user->find($services->user_id);
                  $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                  $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)) , $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url];
                  $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                  $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'subject' => $email->meta_email->subject
                  ];
                  $data_tb_send_mail = [
                    'type' => 'mail_finish_order_expire_hosting',
                    'type_service' => 'hosting',
                    'user_id' => $user->id,
                    'status' => false,
                    'content' => serialize($data),
                  ];
                  $this->send_mail->create($data_tb_send_mail);
            }
        }
        elseif ($type == 'email_hosting') {
            foreach ($order_expireds as $key => $order_expired) {
                  $services = $order_expired->email_hosting;
                  $product = $this->product->find($services->product_id);
                  $email = $this->email->find($product->meta_product->email_expired_finish);
                  $user = $this->user->find($services->user_id);
                  $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                  $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)) , $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url];
                  $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                  $this->user_send_email = $this->user->find($services->user_id)->email;
                  $this->subject = $email->meta_email->subject;
            }
        }
        elseif ($type == 'server') {
          $user = $order->user;
          foreach ($order_expireds as $key => $order_expired) {
            $server = $order_expired->server;
            $product = $server->product;
            $email = $this->email->find($product->meta_product->email_expired_finish);
            $server_cpu = $product->meta_product->cpu . ' (' . $product->meta_product->cores . ')';
            $server_disk = $product->meta_product->disk . ' ';
            $server_disk .= $this->get_addon_disk_server($server->id);
            $search = ['{$user_name}',  '{$user_email}' , '{$order_id}',  '{$product_name}', '{$billing_cycle}',
              '{$order_created_at}', '{$sub_total}', '{$total}', '{$qtt}', '{$server_os}', '{$url}', '{$server_cpu}', '{$server_ram}',
              '{$server_disk}', '{$server_qtt_ip}', '{$server_raid}', '{$server_datacenter}', '{$server_management}', '{$server_ip}',
              '{$server_ip2}', '{$next_due_date}'
            ];
            $replace = [ $user->name,  $user->email , $order->id,  $product->name ,$billings[$server->billing_cycle], $order->created_at,
              number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' ,
              $detail_order->quantity, $server->os, $url, $server_cpu, $product->meta_product->memory, $server_disk,
              $product->meta_product->ip, $server->raid, $server->location, $server->server_management,
              $server->ip, $server->ip2, date('d-m-Y', strtotime($server->next_due_date))
            ];
            $content = str_replace($search, $replace, !empty($email->meta_email->content) ? $email->meta_email->content : '');
            try {
                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'subject' => $email->meta_email->subject,
                ];
                $data_tb_send_mail = [
                    'type' => 'mail_finish_order_expire_server',
                    'type_service' => 'server',
                    'user_id' => $user->id,
                    'status' => false,
                    'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);
            } catch (\Exception $e) {
                report($e);
            }
          }
        }
    }

    // Thanh toán addon
    // Thanh toán khi order addon
    public function payment_addon($invoice_id)
    {
        $invoice = $this->detail_order->find($invoice_id);
        $order = $this->order->find($invoice->order->id);
        $total = $order->total;
        $order->status = 'Active';
        $order->save();
        // invoice
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];

        foreach ($invoice->order_addon_vps as $key => $order_addon_vps) {
            $vps = $order_addon_vps->vps;
            $billing_cycle = $vps->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$vps->billing_cycle] ));
            // dd($due_date);
            // Invoice
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            // ghi vào tổng thu
            if (!empty($this->total_price->first())) {
                $total_price = $this->total_price->first();
                $total_price->vps += $total;
                $total_price->save();
            } else {
                $data_total_price = [
                    'vps' => $total,
                    'hosting' => '0',
                    'server' => '0',
                ];
                $this->total_price->create($data_total_price);
            }

            $data_addon = [
                'mob' => 'upgrade', // action
                'name' => $vps->vm_id,  // vm_id
                'extra_cpu' => !empty($order_addon_vps->cpu) ? $order_addon_vps->cpu : 0, // so CPU them
                'extra_ram' => !empty($order_addon_vps->ram) ? $order_addon_vps->ram : 0, // so RAM them
                'extra_disk' => !empty($order_addon_vps->disk) ? $order_addon_vps->disk : 0, // so DISK them
                'package' => $total, // tong tien
            ];
            try {
              $reques_vcenter = $this->dashboard->upgrade_vps($data_addon, $vps->vm_id); //request đên Vcenter
            } catch (\Exception $e) {
              return false;
              // $reques_vcenter= true;
            }
            // $reques_vcenter= true;
            if ($reques_vcenter) {
                $order->status = 'Finish';
                $order->save();
                if ( !empty($vps->price_override) ) {
                    $pricing_addon = 0;
                    $add_on_products = $this->get_addon_product_private(Auth::user()->id);
                    foreach ($add_on_products as $key => $add_on_product) {
                        if (!empty($add_on_product->meta_product->type_addon)) {
                          if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                            $pricing_addon += $order_addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                          }
                          if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                            $pricing_addon += $order_addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                          }
                          if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $order_addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                          }
                        }
                    }
                    $vps->price_override += $pricing_addon;
                    $vps->save();
                }
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {
                    $vps_config->ram += $order_addon_vps->ram;
                    $vps_config->cpu += $order_addon_vps->cpu;
                    $vps_config->disk += $order_addon_vps->disk;
                    $vps_config->save();
                } else {
                    $data_config = [
                        'vps_id' => $vps->id,
                        'ram' => $order_addon_vps->ram,
                        'cpu' => $order_addon_vps->cpu,
                        'disk' => $order_addon_vps->disk,
                        'ip' => 0,
                    ];
                    $this->vps_config->create($data_config);
                }

                // Gữi email khi hoàn thành order addon
                $this->user_email = $vps->user_vps->email;
                $this->subject = 'Hoàn thành nâng cấp cấu hình VPS';
                $product_addons = $this->get_addon_product_private($vps->user_vps->id);
                foreach ($product_addons as $key => $product_addon) {
                    if (!empty($product_addon->meta_product->type_addon)) {
                        if ($product_addon->meta_product->type_addon == 'addon_cpu') {
                            $product_addon_cpu = $product_addon;
                        }
                        if ($product_addon->meta_product->type_addon == 'addon_ram') {
                            $product_addon_ram = $product_addon;
                        }
                        if ($product_addon->meta_product->type_addon == 'addon_disk') {
                            $product_addon_disk = $product_addon;
                        }
                    }
                }

                if (!empty($order_addon_vps->month)) {
                    $time = $order_addon_vps->month . ' Tháng ' . $order_addon_vps->day . ' Ngày';
                } else {
                    $time = $order_addon_vps->day . ' Ngày';
                }

                $data = [
                    'name' => $vps->user_vps->name,
                    'email' => $vps->user_vps->email,
                    'amount' => $total,
                    'ip' => $vps->ip,
                    'cpu' => !empty($order_addon_vps->cpu) ? $order_addon_vps->cpu : 0,
                    'ram' => !empty($order_addon_vps->ram) ? $order_addon_vps->ram : 0,
                    'disk' => !empty($order_addon_vps->disk) ? $order_addon_vps->disk : 0,
                    'product_cpu' => !empty($product_addon_cpu) ? $product_addon_cpu : 0,
                    'product_ram' => !empty($product_addon_ram) ? $product_addon_ram : 0,
                    'product_disk' => !empty($product_addon_disk) ? $product_addon_disk : 0,
                    'billing_cycle' => $billing_cycle,
                    'time' =>  $time,
                    'subject' => 'Hoàn thành đặt hàng nâng cấp cấu hình VPS'
                ];

                $data_tb_send_mail = [
                  'type' => 'finish_order_addon_vps',
                  'type_service' => 'vps',
                  'user_id' => $vps->user_id,
                  'status' => false,
                  'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);

            }
        }
        return $paid;
    }

    public function get_addon_product_private($user_id)
    {
        $products = [];
        $group_products = [];
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_user = $user->group_user;
            $group_products = $group_user->group_products;
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->get();
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        }
        return $products;
    }

    // Thanh toán khi order addon
    public function payment_change_ip($invoice_id)
    {
        $invoice = $this->detail_order->find($invoice_id);
        $order = $this->order->find($invoice->order->id);
        $total = $order->total;
        $order->status = 'Active';
        $order->save();
        // invoice
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];

        foreach ($invoice->order_change_vps as $key => $order_change_vps) {
            $vps = $order_change_vps->vps;
            $billing_cycle = $vps->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$vps->billing_cycle] ));
            // dd($due_date);
            // Invoice
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            // ghi vào tổng thu
            if (!empty($this->total_price->first())) {
                $total_price = $this->total_price->first();
                $total_price->vps += $total;
                $total_price->save();
            } else {
                $data_total_price = [
                    'vps' => $total,
                    'hosting' => '0',
                    'server' => '0',
                ];
                $this->total_price->create($data_total_price);
            }

            $ip = $vps->ip;
            try {
              $reques_vcenter = $this->dashboard->change_ip($vps); //request đên Vcenter
            } catch (\Exception $e) {
              return false;
              // $reques_vcenter= true;
            }
            // $reques_vcenter= true;
            if ($reques_vcenter) {
                $order->status = 'Finish';
                $order->save();
                // Gữi email khi hoàn thành order addon
                $this->user_email = $vps->user_vps->email;
                $this->subject = 'Hoàn thành thay đổi IP cho VPS';

                $data = [
                    'name' => $vps->user_vps->name,
                    'email' => $vps->user_vps->email,
                    'amount' => $invoice->sub_total,
                    'ip' => $ip,
                    'ip_change' => $vps->ip,
                    'amount' => $invoice->sub_total,
                ];

                $mail = Mail::send('users.mails.finish_order_change_ip_vps', $data, function($message){
                    $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                    $message->to($this->user_email)->subject($this->subject);
                });

            }
        }
        return $paid;
    }

    public function get_payment_with_user($method, $user_id)
    {
        if ($method == 'pay') {
            $payments = $this->history_pay->where('type_gd', 1)->where('user_id', $user_id)->with('user')->orderBy('id', 'desc')->paginate(20);
        }
        elseif ($method == 'invoice') {
            $payments = $this->history_pay->where('type_gd', 2)->where('user_id', $user_id)->with('user')->orderBy('id', 'desc')->paginate(20);
        }
        elseif ($method == 'expired') {
            $payments = $this->history_pay->where('type_gd', 3)->where('user_id', $user_id)->with('user')->orderBy('id', 'desc')->paginate(20);
        }
        elseif ($method == 'addon') {
            $payments = $this->history_pay->where('type_gd', 4)->where('user_id', $user_id)->with('user')->orderBy('id', 'desc')->paginate(20);
        } elseif ($method == 'change_id') {
            $payments = $this->history_pay->where('type_gd', 5)->where('user_id', $user_id)->with('user')->orderBy('id', 'desc')->paginate(20);
        }
        else {
            $payments = $this->history_pay->where('user_id', $user_id)->with('user')->orderBy('id', 'desc')->paginate(20);
        }
        return $payments;
    }

    public function payment_domain($payment)
    {
        $invoice = $this->detail_order->find($payment->detail_order_id);
        $order = $this->order->find($invoice->order->id);
        $total = $order->total;
        $order->status = 'Finish';
        $order->save();
        $user = $this->user->find($payment->user_id);
        $credit = $this->credit->where('user_id', $payment->user_id)->first();
        if (!isset($credit)) {
            return false;
        }
        // HistoryPay
        $payment->status = 'Active';
        $payment->save();
        // invoice
        $billing = [
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        if ($invoice->type == 'Domain') {
            $domains = $this->domain->where('detail_order_id', $invoice->id)->get();
            $billing_cycle = $domains[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$domains[0]->billing_cycle] ));
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            foreach ($domains as $key => $domain) {
                $request_panhanhoa = $this->domain_pa->registerDomain($domain, $due_date); //Request API PAVietNam
                //Ut Hiển làm tổng thu ở đây nha <3
                $domain->update(['status' => 'Active']);
                $domain->next_due_date = $due_date;
                $domain->save();
                if($request_panhanhoa->{'ReturnCode'} == 200) {
                    // $order->status = 'Finish';
                    // $order->save();
                    // $this->send_mail_finish('hosting',$order, $invoice ,$hostings);
                    // $this->send_mail_finish('domain',$order, $invoice ,$domain);
                    $data_reg = [
                        'user_id' => $domain['user_id'],
                        'domain' => $domain['domain'],
                        'due_date' => date('d/m/Y', strtotime( $due_date )),
                        'total' => $invoice->sub_total,
                        'subject' => 'Hoàn thành đăng ký domain',
                    ];
                    $this->user_email = $user->email;

                    $mail = Mail::send('users.mails.finish_reg_domain', $data_reg, function($message){
                        $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                        $message->to($this->user_email)->subject('Hoàn thành đăng ký domain');
                    });
                    $mail_to_admin = Mail::send('users.mails.finish_reg_domain_to_admin', $data_reg, function($message){
                        $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                        $message->to('tuansinhtk@gmail.com')->subject('Hoàn thành đăng ký domain');
                    });
                    return true;
                } else {
                    $data_error = [
                        'user_id' => $domain['user_id'],
                        'domain' => $domain['domain'],
                        'error_code' => $request_panhanhoa->{'ReturnCode'},
                        'error' => $request_panhanhoa->{'ReturnText'},
                    ];
                    $mail = Mail::send('users.mails.error_reg_domain', $data_error, function($message){
                        $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                        $message->to('tuansinhtk@gmail.com')->subject('Lỗi khi tạo domain');
                    });
                    return true;
                }
            }
            return $paid;
        }
    }

    public function payment_domain_expired($payment)
    {
        $invoice = $this->detail_order->find($payment->detail_order_id);
        $domain_exp = $this->domain_exp->where('detail_order_id', $invoice->id)->first();
        $domain = $this->domain->where('id', $domain_exp->domain_id)->first();
        $next_due_date = $domain->next_due_date;
        if(!$next_due_date) {
            return false;
        }
        $order = $this->order->find($invoice->order->id);
        $total = $order->total;
        $order->status = 'Finish';
        $order->save();
        $credit = $this->credit->where('user_id', $payment->user_id)->first();
        if (!isset($credit)) {
            return false;
        }
        // HistoryPay
        $payment->status = 'Active';
        $payment->save();
        // invoice
        $billing = config('billing_domain_exp');
        if ($invoice->type == 'Domain_exp') {
            $domain_expireds = $invoice->domain_expireds;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $next_due_date . $billing[$domain_expireds[0]->billing_cycle] . ' Year'));
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            foreach ($domain_expireds as $key => $domain_expired) {
                $domain = $domain_expired->domain;
                // $date_star = $domain->next_due_date;
                $date_star = date('Y-m-d');
                $due_date = date('Y-m-d', strtotime( $next_due_date . $billing[$domain_expired->billing_cycle] . ' Year'));
                $domain->next_due_date = $due_date;
                $domain->save();
                $year = $billing[$domain_expired->billing_cycle];
                try {
                  $request_panhanhoa = $this->domain_pa->renewDomain($domain, $year);
                } catch (\Exception $e) {
                  report($e);
                  if($request_panhanhoa->{'ReturnCode'} == 200) {
                      // $order->status = 'Finish';
                      // $order->save();
                      $data_reg = [
                          'user_id' => $domain['user_id'],
                          'domain' => $domain['domain'],
                          'due_date' => date('d/m/Y', strtotime( $due_date )),
                          'total' => $invoice->sub_total
                      ];
                      $mail = Mail::send('users.mails.finish_renew_domain', $data_reg, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to(Auth::user()->email)->subject('Hoàn thành gia hạn domain');
                      });
                      $mail_to_admin = Mail::send('users.mails.finish_renew_domain_to_admin', $data_reg, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to('tuansinhtk@gmail.com')->subject('Hoàn thành gia hạn domain');
                      });
                      return true;
                  } else {
                      $data_error = [
                          'user_id' => $domain['user_id'],
                          'domain' => $domain['domain'],
                          'error_code' => $request_panhanhoa->{'ReturnCode'},
                          'error' => $request_panhanhoa->{'ReturnText'},
                      ];
                      // dd($data_error);
                      $mail = Mail::send('users.mails.error_renew_domain', $data_error, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to('tuansinhtk@gmail.com')->subject('Lỗi khi gia hạn domain');
                      });
                      return true;
                  }
                }
                try {
                  if($request_panhanhoa->{'ReturnCode'} == 200) {
                      // $order->status = 'Finish';
                      // $order->save();
                      $data_reg = [
                          'user_id' => $domain['user_id'],
                          'domain' => $domain['domain'],
                          'due_date' => date('d/m/Y', strtotime( $due_date )),
                          'total' => $invoice->sub_total
                      ];
                      $mail = Mail::send('users.mails.finish_renew_domain', $data_reg, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to(Auth::user()->email)->subject('Hoàn thành gia hạn domain');
                      });
                      $mail_to_admin = Mail::send('users.mails.finish_renew_domain_to_admin', $data_reg, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to('tuansinhtk@gmail.com')->subject('Hoàn thành gia hạn domain');
                      });
                      return true;
                  } else {
                      $data_error = [
                          'user_id' => $domain['user_id'],
                          'domain' => $domain['domain'],
                          'error_code' => $request_panhanhoa->{'ReturnCode'},
                          'error' => $request_panhanhoa->{'ReturnText'},
                      ];
                      // dd($data_error);
                      $mail = Mail::send('users.mails.error_renew_domain', $data_error, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to('tuansinhtk@gmail.com')->subject('Lỗi khi gia hạn domain');
                      });
                      return true;
                  }
                } catch (\Exception $e) {
                  continue;
                }
            }
            return $paid;
        }
    }

    // Thanh toán khi order addon
    public function payment_upgrade_hosting($invoice_id)
    {
        $invoice = $this->detail_order->find($invoice_id);
        $order = $this->order->find($invoice->order->id);
        $total = $order->total;
        $order->status = 'Active';
        $order->save();
        // invoice
        $invoice->status = 'paid';
        $invoice->payment_method = 1;
        $paid = $invoice->save();
        // upgrade hosting
        $upgrade = $invoice->order_upgrade_hosting;
        $hosting = $upgrade->hosting;
        if ($hosting->location == 'vn') {
          $this->da->upgrade($hosting, $upgrade->product->package);
        }
        elseif ($hosting->location == 'si') {
          $this->da_si->upgrade($hosting, $upgrade->product->package);
        }
        $hosting->product_id = $upgrade->product_id;
        if (!empty($hosting->price_override)) {
           $hosting->price_override += $total;
        }
        $hosting->save();

        try {
          $data = [
            'name' => $invoice->user->name,
            'email' => $invoice->user->email,
            'domain' => $hosting->domain,
            'product_name' => $upgrade->product->name,
            'subject' => 'Hoàn thành nâng cấp dịch vụ Hosting'
          ];
          $data_tb_send_mail = [
            'type' => 'finish_order_upgarde_hosting',
            'type_service' => 'hosting',
            'user_id' => $user->id,
            'status' => false,
            'content' => serialize($data),
          ];
          $this->send_mail->create($data_tb_send_mail);
        } catch (\Throwable $th) {
          report($th);
        }

        return $paid;
    }

    public function payment_with_user($user_id, $type, $status, $sl)
    {
        if (!empty($type) && !empty($status)) {
          $payments = $this->history_pay->where('type_gd', 1)->where('user_id', $user_id)->where('method_gd', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type)) {
          $payments = $this->history_pay->where('type_gd', 1)->where('user_id', $user_id)->where('method_gd', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($status)) {
          $payments = $this->history_pay->where('type_gd', 1)->where('user_id', $user_id)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else {
          $payments = $this->history_pay->where('type_gd', 1)->where('user_id', $user_id)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        foreach ($payments as $key => $payment) {
           $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
           $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
           $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
           if ( $payment->status == 'Active' ) {
             if (!empty( $payment->log_payment->created_at )) {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
             } else {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
             }
           } else {
             $payment->text_date_gd = '';
           }
           $data['data'][] = $payment;
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_with_type($user_id, $type, $status, $sl)
    {
        if (!empty($user_id) && !empty($status)) {
          $payments = $this->history_pay->where('type_gd', 1)->where('user_id', $user_id)->where('method_gd', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 1)->where('user_id', $user_id)->where('method_gd', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($status)) {
          $payments = $this->history_pay->where('type_gd', 1)->where('method_gd', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else {
          $payments = $this->history_pay->where('type_gd', 1)->where('method_gd', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        foreach ($payments as $key => $payment) {
           $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
           $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
           $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
           if ( $payment->status == 'Active' ) {
             if (!empty( $payment->log_payment->created_at )) {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
             } else {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
             }
           } else {
             $payment->text_date_gd = '';
           }
           $data['data'][] = $payment;
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_with_status($user_id, $type, $status, $sl)
    {
        if (!empty($user_id) && !empty($type)) {
          $payments = $this->history_pay->where('type_gd', 1)->where('user_id', $user_id)->where('method_gd', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 1)->where('user_id', $user_id)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($type)) {
          $payments = $this->history_pay->where('type_gd', 1)->where('method_gd', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else {
          $payments = $this->history_pay->where('type_gd', 1)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        foreach ($payments as $key => $payment) {
           $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
           $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
           $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
           if ( $payment->status == 'Active' ) {
             if (!empty( $payment->log_payment->created_at )) {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
             } else {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
             }
           } else {
             $payment->text_date_gd = '';
           }
           $data['data'][] = $payment;
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_with_qtt($user_id, $type, $status, $sl, $q)
    {
        if (!empty($type) && !empty($status) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 1)->where('user_id', $user_id)->where('ma_gd', 'LIKE', '%'.$q.'%')->where('method_gd', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 1)->where('user_id', $user_id)->where('ma_gd', 'LIKE', '%'.$q.'%')->where('method_gd', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($status) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 1)->where('user_id', $user_id)->where('ma_gd', 'LIKE', '%'.$q.'%')->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type)) {
          $payments = $this->history_pay->where('type_gd', 1)->where('method_gd', $type)->where('ma_gd', 'LIKE', '%'.$q.'%')->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($status)) {
          $payments = $this->history_pay->where('type_gd', 1)->where('status', $status)->where('ma_gd', 'LIKE', '%'.$q.'%')->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 1)->where('user_id', $user_id)->where('ma_gd', 'LIKE', '%'.$q.'%')->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else {
          $payments = $this->history_pay->where('type_gd', 1)->where('ma_gd', 'LIKE', '%'.$q.'%')->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        foreach ($payments as $key => $payment) {
           $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
           $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
           $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
           if ( $payment->status == 'Active' ) {
             if (!empty( $payment->log_payment->created_at )) {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
             } else {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
             }
           } else {
             $payment->text_date_gd = '';
           }
           $data['data'][] = $payment;
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_credit_of_enterprise()
    {
        $payments = $this->history_pay->from('history_pays as o')->where('o.type_gd', 1)->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', true)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        foreach ($payments as $key => $payment) {
           $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
           $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
           $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
           if ( $payment->status == 'Active' ) {
             if (!empty( $payment->log_payment->created_at )) {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
             } else {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
             }
           } else {
             $payment->text_date_gd = '';
           }
           $data['data'][] = $payment;
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_credit_of_personal()
    {
        $payments = $this->history_pay->from('history_pays as o')->where('o.type_gd', 1)->select(['o.*'])->join('users as u','o.user_id','=','u.id')->where('u.enterprise', false)->with('user')->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        foreach ($payments as $key => $payment) {
           $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
           $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
           $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
           if ( $payment->status == 'Active' ) {
             if (!empty( $payment->log_payment->created_at )) {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
             } else {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
             }
           } else {
             $payment->text_date_gd = '';
           }
           $data['data'][] = $payment;
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_addon_of_enterprise()
    {
        $payments = $this->history_pay->from('history_pays as o')->where('o.type_gd', 4)->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', true)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        if ( $payments->count() > 0 ) {
            foreach ($payments as $key => $payment) {
                $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
                $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
                if ( $payment->status == 'Active' ) {
                  if (!empty( $payment->log_payment->created_at )) {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
                  } else {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
                  }
                } else {
                  $payment->text_date_gd = '';
                }
                $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
                if ($payment->status == 'confirm' || $payment->status == 'Active') {
                  $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
                } else {
                  $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
                }
                $data['data'][] = $payment;
            }
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_addon_of_personal()
    {
        $payments = $this->history_pay->from('history_pays as o')->where('o.type_gd', 4)->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', false)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        if ( $payments->count() > 0 ) {
            foreach ($payments as $key => $payment) {
                $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
                $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
                if ( $payment->status == 'Active' ) {
                  if (!empty( $payment->log_payment->created_at )) {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
                  } else {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
                  }
                } else {
                  $payment->text_date_gd = '';
                }
                $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
                if ($payment->status == 'confirm' || $payment->status == 'Active') {
                  $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
                } else {
                  $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
                }
                $data['data'][] = $payment;
            }
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_change_ip_of_enterprise()
    {
        $payments = $this->history_pay->from('history_pays as o')->where('o.type_gd', 5)->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', true)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        if ( $payments->count() > 0 ) {
            foreach ($payments as $key => $payment) {
                $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
                $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
                if ( $payment->status == 'Active' ) {
                  if (!empty( $payment->log_payment->created_at )) {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
                  } else {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
                  }
                } else {
                  $payment->text_date_gd = '';
                }
                $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
                if ($payment->status == 'confirm' || $payment->status == 'Active') {
                  $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
                } else {
                  $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
                }
                $data['data'][] = $payment;
            }
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
        // dd($data);
        return $data;
    }

    public function list_payment_change_ip_of_personal()
    {
        $payments = $this->history_pay->from('history_pays as o')->where('o.type_gd', 5)->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', false)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        if ( $payments->count() > 0 ) {
            foreach ($payments as $key => $payment) {
                $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
                $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
                if ( $payment->status == 'Active' ) {
                  if (!empty( $payment->log_payment->created_at )) {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
                  } else {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
                  }
                } else {
                  $payment->text_date_gd = '';
                }
                $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
                if ($payment->status == 'confirm' || $payment->status == 'Active') {
                  $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
                } else {
                  $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
                }
                $data['data'][] = $payment;
            }
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_upgrade_hosting_of_enterprise()
    {
        $payments = $this->history_pay->from('history_pays as o')->where('o.type_gd', 8)->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', true)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        if ( $payments->count() > 0 ) {
            foreach ($payments as $key => $payment) {
                $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
                $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
                if ( $payment->status == 'Active' ) {
                  if (!empty( $payment->log_payment->created_at )) {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
                  } else {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
                  }
                } else {
                  $payment->text_date_gd = '';
                }
                $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
                if ($payment->status == 'confirm' || $payment->status == 'Active') {
                  $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
                } else {
                  $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
                }
                $data['data'][] = $payment;
            }
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_upgrade_hosting_of_personal()
    {
        $payments = $this->history_pay->from('history_pays as o')->where('o.type_gd', 8)->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', false)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        if ( $payments->count() > 0 ) {
            foreach ($payments as $key => $payment) {
                $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
                $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
                if ( $payment->status == 'Active' ) {
                  if (!empty( $payment->log_payment->created_at )) {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
                  } else {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
                  }
                } else {
                  $payment->text_date_gd = '';
                }
                $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
                if ($payment->status == 'confirm' || $payment->status == 'Active') {
                  $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
                } else {
                  $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
                }
                $data['data'][] = $payment;
            }
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_domain_create_of_enterprise()
    {
        $payments = $this->history_pay->from('history_pays as o')->where('o.type_gd', 6)->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', true)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        if ( $payments->count() > 0 ) {
            foreach ($payments as $key => $payment) {
                $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
                $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
                if ( $payment->status == 'Active' ) {
                  if (!empty( $payment->log_payment->created_at )) {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
                  } else {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
                  }
                } else {
                  $payment->text_date_gd = '';
                }
                $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
                if ($payment->status == 'confirm' || $payment->status == 'Active') {
                  $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
                } else {
                  $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
                }
                $data['data'][] = $payment;
            }
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_create_domain_of_personal()
    {
        $payments = $this->history_pay->from('history_pays as o')->where('o.type_gd', 6)->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', false)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        if ( $payments->count() > 0 ) {
            foreach ($payments as $key => $payment) {
                $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
                $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
                if ( $payment->status == 'Active' ) {
                  if (!empty( $payment->log_payment->created_at )) {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
                  } else {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
                  }
                } else {
                  $payment->text_date_gd = '';
                }
                $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
                if ($payment->status == 'confirm' || $payment->status == 'Active') {
                  $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
                } else {
                  $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
                }
                $data['data'][] = $payment;
            }
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_domain_expire_of_enterprise()
    {
        $payments = $this->history_pay->from('history_pays as o')->where('o.type_gd', 7)->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', true)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        if ( $payments->count() > 0 ) {
            foreach ($payments as $key => $payment) {
                $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
                $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
                if ( $payment->status == 'Active' ) {
                  if (!empty( $payment->log_payment->created_at )) {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
                  } else {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
                  }
                } else {
                  $payment->text_date_gd = '';
                }
                $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
                if ($payment->status == 'confirm' || $payment->status == 'Active') {
                  $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
                } else {
                  $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
                }
                $data['data'][] = $payment;
            }
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_expire_domain_of_personal()
    {
        $payments = $this->history_pay->from('history_pays as o')->where('o.type_gd', 7)->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', false)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        if ( $payments->count() > 0 ) {
            foreach ($payments as $key => $payment) {
                $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
                $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
                if ( $payment->status == 'Active' ) {
                  if (!empty( $payment->log_payment->created_at )) {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
                  } else {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
                  }
                } else {
                  $payment->text_date_gd = '';
                }
                $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
                if ($payment->status == 'confirm' || $payment->status == 'Active') {
                  $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
                } else {
                  $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
                }
                $data['data'][] = $payment;
            }
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_expired_of_enterprise()
    {
        $payments = $this->history_pay->from('history_pays as o')->where('o.type_gd', 3)->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', true)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        if ( $payments->count() > 0 ) {
            foreach ($payments as $key => $payment) {
                $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
                $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
                if ( $payment->status == 'Active' ) {
                  if (!empty( $payment->log_payment->created_at )) {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
                  } else {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
                  }
                } else {
                  $payment->text_date_gd = '';
                }
                $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
                if ($payment->status == 'confirm' || $payment->status == 'Active') {
                  $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
                } else {
                  $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
                }
                $data['data'][] = $payment;
            }
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_expired_of_personal()
    {
        $payments = $this->history_pay->from('history_pays as o')->where('o.type_gd', 3)->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', false)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        if ( $payments->count() > 0 ) {
            foreach ($payments as $key => $payment) {
                $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
                $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
                if ( $payment->status == 'Active' ) {
                  if (!empty( $payment->log_payment->created_at )) {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
                  } else {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
                  }
                } else {
                  $payment->text_date_gd = '';
                }
                $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
                if ($payment->status == 'confirm' || $payment->status == 'Active') {
                  $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
                } else {
                  $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
                }
                $data['data'][] = $payment;
            }
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_create_of_enterprise()
    {
        $payments = $this->history_pay->from('history_pays as o')->where('o.type_gd', 2)->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', true)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        if ( $payments->count() > 0 ) {
            foreach ($payments as $key => $payment) {
                $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
                $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
                if ( $payment->status == 'Active' ) {
                  if (!empty( $payment->log_payment->created_at )) {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
                  } else {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
                  }
                } else {
                  $payment->text_date_gd = '';
                }
                $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
                if ($payment->status == 'confirm' || $payment->status == 'Active') {
                  $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
                } else {
                  $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
                }
                $data['data'][] = $payment;
            }
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_create_of_personal()
    {
        $payments = $this->history_pay->from('history_pays as o')->where('o.type_gd', 2)->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', false)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        if ( $payments->count() > 0 ) {
            foreach ($payments as $key => $payment) {
                $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
                $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
                if ( $payment->status == 'Active' ) {
                  if (!empty( $payment->log_payment->created_at )) {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
                  } else {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
                  }
                } else {
                  $payment->text_date_gd = '';
                }
                $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
                if ($payment->status == 'confirm' || $payment->status == 'Active') {
                  $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
                } else {
                  $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
                }
                $data['data'][] = $payment;
            }
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_all_of_enterprise()
    {
        $payments = $this->history_pay->from('history_pays as o')->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', true)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        if ( $payments->count() > 0 ) {
            foreach ($payments as $key => $payment) {
                $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
                $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
                if ( $payment->status == 'Active' ) {
                  if (!empty( $payment->log_payment->created_at )) {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
                  } else {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
                  }
                } else {
                  $payment->text_date_gd = '';
                }
                if ($payment->type_gd == 1) {
                  $payment->loai_gd = 'Nạp tiền vào tài khoản';
                }
                else {
                    if ($payment->type_gd == 2) {
                      $payment->loai_gd = 'Tạo ';
                      if (!empty($payment->detail_order->type)) {
                        $payment->loai_gd .= $payment->detail_order->type;
                      }
                    }
                    elseif ($payment->type_gd == 3){
                      $payment->loai_gd = 'Gia hạn ';
                      if (!empty($payment->detail_order->type)) {
                        $payment->loai_gd .= $payment->detail_order->type;
                      }
                    }
                    elseif ($payment->type_gd == 4) {
                      $payment->loai_gd = 'Addon VPS';
                    }
                    elseif ($payment->type_gd == 5) {
                      $payment->loai_gd = 'Đổi IP';
                    }
                    elseif ($payment->type_gd == 6) {
                      $payment->loai_gd = 'Tạo Domain';
                    }
                    elseif ($payment->type_gd == 7) {
                      $payment->loai_gd = 'Gia hạn Domain';
                    }
                    elseif ($payment->type_gd == 8) {
                      $payment->loai_gd = 'Nâng cấp Hosting';
                    }
                }
                if ($payment->type_gd == 1) {
                  $payment->hinh_thuc_gd = !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                } else {
                  if ($payment->status == 'confirm' || $payment->status == 'Active') {
                    $payment->hinh_thuc_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
                  } else {
                    $payment->hinh_thuc_gd = '<span class="text-danger">Chưa thanh toán</span>';
                  }
                }
                $data['data'][] = $payment;
            }
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_all_of_personal()
    {
        $payments = $this->history_pay->from('history_pays as o')->select(['o.*'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', false)->with('user')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        if ( $payments->count() > 0 ) {
            foreach ($payments as $key => $payment) {
                $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
                $payment->loai_gd =  !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
                if ( $payment->status == 'Active' ) {
                  if (!empty( $payment->log_payment->created_at )) {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
                  } else {
                    $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
                  }
                } else {
                  $payment->text_date_gd = '';
                }
                if ($payment->type_gd == 1) {
                  $payment->loai_gd = 'Nạp tiền vào tài khoản';
                }
                else {
                    if ($payment->type_gd == 2) {
                      $payment->loai_gd = 'Tạo ';
                      if (!empty($payment->detail_order->type)) {
                        $payment->loai_gd .= $payment->detail_order->type;
                      }
                    }
                    elseif ($payment->type_gd == 3){
                      $payment->loai_gd = 'Gia hạn ';
                      if (!empty($payment->detail_order->type)) {
                        $payment->loai_gd .= $payment->detail_order->type;
                      }
                    }
                    elseif ($payment->type_gd == 4) {
                      $payment->loai_gd = 'Addon VPS';
                    }
                    elseif ($payment->type_gd == 5) {
                      $payment->loai_gd = 'Đổi IP';
                    }
                    elseif ($payment->type_gd == 6) {
                      $payment->loai_gd = 'Tạo Domain';
                    }
                    elseif ($payment->type_gd == 7) {
                      $payment->loai_gd = 'Gia hạn Domain';
                    }
                    elseif ($payment->type_gd == 8) {
                      $payment->loai_gd = 'Nâng cấp Hosting';
                    }
                }
                if ($payment->type_gd == 1) {
                  $payment->hinh_thuc_gd = !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
                } else {
                  if ($payment->status == 'confirm' || $payment->status == 'Active') {
                    $payment->hinh_thuc_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
                  } else {
                    $payment->hinh_thuc_gd = '<span class="text-danger">Chưa thanh toán</span>';
                  }
                }
                $data['data'][] = $payment;
            }
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_create_with_qtt($user_id, $type, $status, $sl)
    {
        if (!empty($type) && !empty($status) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 2)->where('user_id', $user_id)->where('method_gd_invoice', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 2)->where('user_id', $user_id)->where('method_gd_invoice', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($status) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 2)->where('user_id', $user_id)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($status) && !empty($type)) {
          $payments = $this->history_pay->where('type_gd', 2)->where('method_gd_invoice', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type)) {
          $payments = $this->history_pay->where('type_gd', 2)->where('method_gd_invoice', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($status)) {
          $payments = $this->history_pay->where('type_gd', 2)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 2)->where('user_id', $user_id)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else {
          $payments = $this->history_pay->where('type_gd', 2)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        foreach ($payments as $key => $payment) {
           $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
           $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
           if ($payment->status == 'confirm' || $payment->status == 'Active') {
             $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : $payment->method_gd_invoice;
           } else {
             $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
           }
           $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
           if ( $payment->status == 'Active' ) {
             if (!empty( $payment->log_payment->created_at )) {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
             } else {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
             }
           } else {
             $payment->text_date_gd = '';
           }
           $data['data'][] = $payment;
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_payment_addon_with_qtt($user_id, $type, $status, $sl)
    {
        if (!empty($type) && !empty($status) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 4)->where('user_id', $user_id)->where('method_gd_invoice', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 4)->where('user_id', $user_id)->where('method_gd_invoice', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($status) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 4)->where('user_id', $user_id)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($status) && !empty($type)) {
          $payments = $this->history_pay->where('type_gd', 4)->where('method_gd_invoice', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type)) {
          $payments = $this->history_pay->where('type_gd', 4)->where('method_gd_invoice', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($status)) {
          $payments = $this->history_pay->where('type_gd', 4)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 4)->where('user_id', $user_id)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else {
          $payments = $this->history_pay->where('type_gd', 4)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        foreach ($payments as $key => $payment) {
           $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
           $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
           if ($payment->status == 'confirm' || $payment->status == 'Active') {
             $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
           } else {
             $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
           }
           $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
           if ( $payment->status == 'Active' ) {
             if (!empty( $payment->log_payment->created_at )) {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
             } else {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
             }
           } else {
             $payment->text_date_gd = '';
           }
           $data['data'][] = $payment;
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_upgrade_hosting_with_qtt($user_id, $type, $status, $sl)
    {
        if (!empty($type) && !empty($status) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 8)->where('user_id', $user_id)->where('method_gd_invoice', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 8)->where('user_id', $user_id)->where('method_gd_invoice', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($status) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 8)->where('user_id', $user_id)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($status) && !empty($type)) {
          $payments = $this->history_pay->where('type_gd', 8)->where('method_gd_invoice', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type)) {
          $payments = $this->history_pay->where('type_gd', 8)->where('method_gd_invoice', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($status)) {
          $payments = $this->history_pay->where('type_gd', 8)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 8)->where('user_id', $user_id)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else {
          $payments = $this->history_pay->where('type_gd', 8)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        foreach ($payments as $key => $payment) {
           $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
           $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
           if ($payment->status == 'confirm' || $payment->status == 'Active') {
             $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
           } else {
             $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
           }
           $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
           if ( $payment->status == 'Active' ) {
             if (!empty( $payment->log_payment->created_at )) {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
             } else {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
             }
           } else {
             $payment->text_date_gd = '';
           }
           $data['data'][] = $payment;
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_change_ip_with_qtt($user_id, $type, $status, $sl)
    {
        if (!empty($type) && !empty($status) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 5)->where('user_id', $user_id)->where('method_gd_invoice', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 5)->where('user_id', $user_id)->where('method_gd_invoice', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($status) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 5)->where('user_id', $user_id)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($status) && !empty($type)) {
          $payments = $this->history_pay->where('type_gd', 5)->where('method_gd_invoice', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type)) {
          $payments = $this->history_pay->where('type_gd', 5)->where('method_gd_invoice', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($status)) {
          $payments = $this->history_pay->where('type_gd', 5)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 5)->where('user_id', $user_id)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else {
          $payments = $this->history_pay->where('type_gd', 5)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        foreach ($payments as $key => $payment) {
           $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
           $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
           if ($payment->status == 'confirm' || $payment->status == 'Active') {
             $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
           } else {
             $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
           }
           $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
           if ( $payment->status == 'Active' ) {
             if (!empty( $payment->log_payment->created_at )) {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
             } else {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
             }
           } else {
             $payment->text_date_gd = '';
           }
           $data['data'][] = $payment;
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_expired_with_qtt($user_id, $type, $status, $sl)
    {
        if (!empty($type) && !empty($status) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 3)->where('user_id', $user_id)->where('method_gd_invoice', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 3)->where('user_id', $user_id)->where('method_gd_invoice', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($status) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 3)->where('user_id', $user_id)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($status) && !empty($type)) {
          $payments = $this->history_pay->where('type_gd', 3)->where('method_gd_invoice', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type)) {
          $payments = $this->history_pay->where('type_gd', 3)->where('method_gd_invoice', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($status)) {
          $payments = $this->history_pay->where('type_gd', 3)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 3)->where('user_id', $user_id)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else {
          $payments = $this->history_pay->where('type_gd', 3)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        foreach ($payments as $key => $payment) {
           $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
           $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
           if ($payment->status == 'confirm' || $payment->status == 'Active') {
             $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
           } else {
             $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
           }
           $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
           if ( $payment->status == 'Active' ) {
             if (!empty( $payment->log_payment->created_at )) {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
             } else {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
             }
           } else {
             $payment->text_date_gd = '';
           }
           $data['data'][] = $payment;
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_domain_with_qtt($user_id, $type, $status, $sl)
    {
        if (!empty($type) && !empty($status) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 6)->where('user_id', $user_id)->where('method_gd_invoice', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 6)->where('user_id', $user_id)->where('method_gd_invoice', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($status) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 6)->where('user_id', $user_id)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($status) && !empty($type)) {
          $payments = $this->history_pay->where('type_gd', 6)->where('method_gd_invoice', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type)) {
          $payments = $this->history_pay->where('type_gd', 6)->where('method_gd_invoice', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($status)) {
          $payments = $this->history_pay->where('type_gd', 6)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 6)->where('user_id', $user_id)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else {
          $payments = $this->history_pay->where('type_gd', 6)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        foreach ($payments as $key => $payment) {
           $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
           $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
           if ($payment->status == 'confirm' || $payment->status == 'Active') {
             $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
           } else {
             $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
           }
           $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
           if ( $payment->status == 'Active' ) {
             if (!empty( $payment->log_payment->created_at )) {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
             } else {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
             }
           } else {
             $payment->text_date_gd = '';
           }
           $data['data'][] = $payment;
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_domain_exp_with_qtt($user_id, $type, $status, $sl)
    {
        if (!empty($type) && !empty($status) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 7)->where('user_id', $user_id)->where('method_gd_invoice', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 7)->where('user_id', $user_id)->where('method_gd_invoice', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($status) && !empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 7)->where('user_id', $user_id)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($status) && !empty($type)) {
          $payments = $this->history_pay->where('type_gd', 7)->where('method_gd_invoice', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type)) {
          $payments = $this->history_pay->where('type_gd', 7)->where('method_gd_invoice', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($status)) {
          $payments = $this->history_pay->where('type_gd', 7)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($user_id)) {
          $payments = $this->history_pay->where('type_gd', 7)->where('user_id', $user_id)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else {
          $payments = $this->history_pay->where('type_gd', 7)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        foreach ($payments as $key => $payment) {
           $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
           $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
           if ($payment->status == 'confirm' || $payment->status == 'Active') {
             $payment->loai_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
           } else {
             $payment->loai_gd = '<span class="text-danger">Chưa thanh toán</span>';
           }
           $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
           if ( $payment->status == 'Active' ) {
             if (!empty( $payment->log_payment->created_at )) {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
             } else {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
             }
           } else {
             $payment->text_date_gd = '';
           }
           $data['data'][] = $payment;
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function list_all_with_qtt($user_id, $type, $status, $sl, $method)
    {
        if (!empty($type) && !empty($status) && !empty($user_id) && !empty($method)) {
          $payments = $this->history_pay->where('user_id', $user_id)->where('method_gd_invoice', $method)->where('type_gd', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type) && !empty($user_id)  && !empty($method)) {
          if ($type == 1) {
            $payments = $this->history_pay->where('user_id', $user_id)->where('type_gd', $type)->where('method_gd', $method)->with('user')->orderBy('id', 'desc')->paginate($sl);
          } else {
            $payments = $this->history_pay->where('user_id', $user_id)->where('type_gd', $type)->where('method_gd_invoice', $method)->with('user')->orderBy('id', 'desc')->paginate($sl);
          }
        }
        else if (!empty($type) && !empty($user_id)  && !empty($status)) {
          $payments = $this->history_pay->where('user_id', $user_id)->where('type_gd', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type) && !empty($method)  && !empty($status)) {
          if ($type == 1) {
            $payments = $this->history_pay->where('method_gd', $method)->where('type_gd', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
          } else {
            $payments = $this->history_pay->where('method_gd_invoice', $method)->where('type_gd', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
          }
        }
        else if (!empty($user_id) && !empty($method)  && !empty($status)) {
          $payments = $this->history_pay->where('user_id', $user_id)->where('method_gd_invoice', $method)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type) && !empty($user_id)) {
          $payments = $this->history_pay->where('user_id', $user_id)->where('type_gd', $type)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type) && !empty($status)) {
          $payments = $this->history_pay->where('type_gd', $type)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type)  && !empty($method)) {
          if ($type == 1) {
            $payments = $this->history_pay->where('type_gd', $type)->where('method_gd', $method)->with('user')->orderBy('id', 'desc')->paginate($sl);
          } else {
            $payments = $this->history_pay->where('type_gd', $type)->where('method_gd_invoice', $method)->with('user')->orderBy('id', 'desc')->paginate($sl);
          }
        }
        else if (!empty($user_id) && !empty($status)) {
          $payments = $this->history_pay->where('user_id', $user_id)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($user_id) && !empty($method) ) {
          $payments = $this->history_pay->where('user_id', $user_id)->where('method_gd_invoice', $method)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($method)  && !empty($status)) {
          $payments = $this->history_pay->where('method_gd_invoice', $method)->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else if (!empty($type)) {
          $payments = $this->history_pay->where('type_gd', $type)->where('method_gd_invoice', $method)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($status)) {
          $payments = $this->history_pay->where('status', $status)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($user_id)) {
          $payments = $this->history_pay->where('user_id', $user_id)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        elseif (!empty($method)) {
          $payments = $this->history_pay->where('method_gd_invoice', $method)->with('user')->orderBy('id', 'desc')->paginate($sl);
        }
        else {
          $payments = $this->history_pay->with('user')->orderBy('id', 'desc')->paginate($sl);
        }

        $data = [];
        $data['data']  = [];
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        foreach ($payments as $key => $payment) {
           $payment->user_name = !empty($payment->user->name) ? $payment->user->name : 'Đã xóa';
           $payment->text_type = !empty($payment->detail_order->type) ? $payment->detail_order->type : '';
           if ($payment->type_gd == 1) {
             $payment->loai_gd = 'Nạp tiền vào tài khoản';
           }
           else {
               if ($payment->type_gd == 2) {
                 $payment->loai_gd = 'Tạo ';
                 if (!empty($payment->detail_order->type)) {
                   $payment->loai_gd .= $payment->detail_order->type;
                 }
               }
               elseif ($payment->type_gd == 3){
                 $payment->loai_gd = 'Gia hạn ';
                 if (!empty($payment->detail_order->type)) {
                   $payment->loai_gd .= $payment->detail_order->type;
                 }
               }
               elseif ($payment->type_gd == 4) {
                 $payment->loai_gd = 'Addon VPS';
               }
               elseif ($payment->type_gd == 5) {
                 $payment->loai_gd = 'Đổi IP';
               }
               elseif ($payment->type_gd == 6) {
                 $payment->loai_gd = 'Tạo Domain';
               }
               elseif ($payment->type_gd == 7) {
                 $payment->loai_gd = 'Gia hạn Domain';
               }
               elseif ($payment->type_gd == 8) {
                 $payment->loai_gd = 'Nâng cấp Hosting';
               }
               elseif ($payment->type_gd == 9) {
                 $payment->loai_gd = 'Addon Server';
               }
               elseif ($payment->type_gd == 98) {
                 $payment->loai_gd = 'Cộng tiền vào tài khoản';
               }
               elseif ($payment->type_gd == 99) {
                 $payment->loai_gd = 'Trừ tiền tài khoản';
               }
           }
           if ($payment->type_gd == 1) {
             $payment->hinh_thuc_gd = !empty($pay_in[$payment->method_gd]) ? $pay_in[$payment->method_gd] : $payment->method_gd;
           } else {
             if ($payment->status == 'confirm' || $payment->status == 'Active') {
               $payment->hinh_thuc_gd =  !empty($pay_in[$payment->method_gd_invoice]) ? $pay_in[$payment->method_gd_invoice] : 'Số dư tài khoản';
             } else {
               $payment->hinh_thuc_gd = '<span class="text-danger">Chưa thanh toán</span>';
             }
           }
           $payment->text_created_at = date('H:i:s d-m-Y', strtotime($payment->created_at));
           if ( $payment->status == 'Active' ) {
             if (!empty( $payment->log_payment->created_at )) {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
             } else {
               $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
             }
           } else {
             $payment->text_date_gd = '';
           }
           $data['data'][] = $payment;
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

    public function history_pay_with_user($id)
    {
        $payments = $this->history_pay->where('user_id', $id)->where('status', 'Active')->with('detail_order')
                    ->orderByRaw('updated_at desc')->paginate(30);
        return $payments;
    }

    public function search_history_pay($id, $q)
    {
        $data = ['data' => []];
        $payments = $this->history_pay->where('user_id', $id)->where('ma_gd', 'LIKE', '%'.$q.'%')->where('status', 'Active')
          ->with('detail_order')->orderByRaw('updated_at desc')->paginate(30);
        $pay_in = config('pay_in');
        $type_payin = config('type_payin');
        $momo_status = config('momo_status');
        foreach ($payments as $payment) {
            if ($payment->method_gd == 'invoice') {
              if(!empty($pay_in[$payment->method_gd_invoice])) {
                $payment->text_method_gd_invoice = $pay_in[$payment->method_gd_invoice];
              }
              else {
                if ($payment->status == 'Active') {
                  $payment->text_method_gd_invoice = 'Admin xác nhận';
                } else {
                  $payment->text_method_gd_invoice = 'Chưa thanh toán';
                }
              }
            }
            else {
              if(!empty($pay_in[$payment->method_gd])) {
                // $payment->text_method_gd_invoice = $pay_in[$payment->method_gd];
                $payment->text_method_gd_invoice = 'Vietcombank';
              } else {
                $payment->text_method_gd_invoice = 'Thanh toán';
              }
            }
            // loai giao dich
            if ( $payment->type_gd == 2 ) {
              $payment->text_type_gd = 'Thanh toán ' . $payment->detail_order->type;
            } elseif ( $payment->type_gd == 3 ) {
              $payment->text_type_gd = 'Gia hạn ' . $payment->detail_order->type;
            } else {
              $payment->text_type_gd = $type_payin[$payment->type_gd];
            }
            //List dịch vụ
            $payment->text_list_service = '';
            if ( $payment->type_gd == 2 || $payment->type_gd == 6 ) {
              if ( !empty($payment->detail_order->type) ) {
                if ( $payment->detail_order->type == 'VPS' || $payment->detail_order->type == 'VPS-US' ||
                  $payment->detail_order->type == 'VPS US' || $payment->detail_order->type == 'NAT-VPS' ) {
                  foreach ( $payment->detail_order->vps as $vps ) {
                    if ( !empty($vps->ip) ) {
                      $payment->text_list_service .= $vps->ip . '<br>';
                    } else {
                      $payment->text_list_service .= '<span class="text-danger">Đang chờ tạo</span> <br>';
                    }
                  }
                }
                elseif ( $payment->detail_order->type == 'Hosting' ) {
                  foreach ( $payment->detail_order->hostings as $hosting ) {
                    if( !empty($hosting->domain) ) {
                      $payment->text_list_service .= $hosting->domain . '<br>';
                    } else {
                      $payment->text_list_service .= '<span class="text-danger">Đã xóa</span> <br>';
                    }
                  }
                }
                elseif ( $payment->detail_order->type == 'Domain' ) {
                  foreach ( $payment->detail_order->domains as $domain ) {
                    if( !empty($domain->domain) ) {
                      $payment->text_list_service .= $domain->domain . '<br>';
                    } else {
                      $payment->text_list_service .= '<span class="text-danger">Đã xóa</span> <br>';
                    }
                  }
                }
              }
            }
            elseif ( $payment->type_gd == 3 || $payment->type_gd == 7 ) {
              if ( !empty($payment->detail_order->type) ) {
                if ( $payment->detail_order->type == 'VPS' || $payment->detail_order->type == 'VPS-US' ||
                  $payment->detail_order->type == 'VPS US' || $payment->detail_order->type == 'NAT-VPS' ) {
                  if ( !empty($payment->detail_order->order_expireds) ) {
                    foreach ( $payment->detail_order->order_expireds as $order_expired ) {
                      if ( !empty( $order_expired->vps->ip ) ) {
                        $payment->text_list_service .= $order_expired->vps->ip . '<br>';
                      } else {
                        $payment->text_list_service .= '<span class="text-danger">Đã xóa</span> <br>';
                      }
                    }
                  }
                }
                elseif ( $payment->detail_order->type == 'hosting' ) {
                  if ( !empty($payment->detail_order->order_expireds) ) {
                    foreach ( $payment->detail_order->order_expireds as $order_expired ) {
                      if ( !empty( $order_expired->hosting->domain ) ) {
                        $payment->text_list_service .= $order_expired->hosting->domain . '<br>';
                      } else {
                        $payment->text_list_service .= '<span class="text-danger">Đã xóa</span> <br>';
                      }
                    }
                  }
                }
                elseif ( $payment->detail_order->type == 'Domain' ) {
                  if ( !empty($payment->detail_order->order_expireds) ) {
                    foreach ( $payment->detail_order->order_expireds as $order_expired ) {
                      if ( !empty( $order_expired->domain->domain ) ) {
                        $payment->text_list_service .= $order_expired->domain->domain . '<br>';
                      } else {
                        $payment->text_list_service .= '<span class="text-danger">Đã xóa</span> <br>';
                      }
                    }
                  }
                }
              }
            }
            elseif ( $payment->type_gd == 4 ) {
              if ( !empty($payment->detail_order) ) {
                if ($payment->detail_order->order_addon_vps) {
                  foreach ($payment->detail_order->order_addon_vps as $order_addon_vps) {
                    if ( !empty($order_addon_vps->vps->ip) ) {
                      $payment->text_list_service .= $order_addon_vps->vps->ip . '<br>';
                    } else {
                      $payment->text_list_service .= '<span class="text-danger">Đã xóa</span> <br>';
                    }
                  }
                }
              }
            }
            elseif ( $payment->type_gd == 5 ) {
              if ( !empty($payment->detail_order) ) {
                if ( !empty($payment->detail_order->order_change_vps) ) {
                  foreach ($payment->detail_order->order_change_vps as $order_change_vps) {
                    if ( !empty($order_change_vps->vps->ip) ) {
                      $payment->text_list_service .= $order_addon_vps->vps->ip . '<br>';
                    } else {
                      $payment->text_list_service .= '<span class="text-danger">Đã xóa</span> <br>';
                    }
                  }
                }
              }
            }
            elseif ( $payment->type_gd == 8 ) {
              if ( !empty($payment->detail_order) ) {
                if ( !empty($detail_order->order_upgrade_hosting->hosting) ) {
                  $payment->text_list_service .= $detail_order->order_upgrade_hosting->hosting;
                } else {
                  $payment->text_list_service .= '<span class="text-danger">Đã xóa</span> <br>';
                }
              }
            }
            // log_payment_before
            if ( !empty($payment->log_payment->before) ) {
              $payment->log_payment_before = number_format($payment->log_payment->before,0,",",".") . ' VNĐ';
            } else {
              $payment->log_payment_before = '';
            }
            // log_payment_after
            if ( !empty($payment->log_payment->after) ) {
              $payment->log_payment_after = number_format($payment->log_payment->after,0,",",".") . ' VNĐ';
            } else {
              $payment->log_payment_after = '';
            }
            // text_date_gd
            if ( $payment->status == 'Active' ) {
              if (!empty( $payment->log_payment->created_at )) {
                $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->log_payment->created_at));
              } else {
                $payment->text_date_gd = date('H:i:s d-m-Y', strtotime($payment->updated_at));
              }
            } else {
              $payment->text_date_gd = '';
            }
            // text_status
            if($payment->status == 'Active') {
              $payment->text_status = '<span class="text-success">Đã thanh toán</span>';
            } elseif ($payment->status == 'confirm') {
              $payment->text_status = '<span class="text-info">Đã xác nhận</span>';
            } else {
              $payment->text_status = '<span class="text-danger">Chưa thanh toán</span>';
            }
            $data['data'][] = $payment;
        }
        $data['total'] = $payments->total();
        $data['perPage'] = $payments->perPage();
        $data['current_page'] = $payments->currentPage();
        // dd($data);
        return $data;
    }

}
