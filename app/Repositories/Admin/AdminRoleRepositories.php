<?php

namespace App\Repositories\Admin;
use App\Model\AdminRole;
use App\Model\AdminRoleMeta;
use App\Model\LogActivity;
use App\Model\User;
use App\Model\UserMeta;
use Illuminate\Support\Facades\Auth;

class AdminRoleRepositories {

	protected $adminRoler;
	protected $adminRoleMeta;
	protected $log_activity;
	protected $user_meta;

	public function __construct()
    {
        $this->adminRoler = new AdminRole;
        $this->adminRoleMeta = new AdminRoleMeta;
        $this->log_activity = new LogActivity();
        $this->user = new User();
        $this->user_meta = new UserMeta();
    }

    public function login($data)
    {
    	$login_momo = config('login_admin_momo');
    	if ( $data['user'] == $login_momo['user'] && $data['password'] == $login_momo['password'] ) {
          return true;
        }
        return false;
    }

    public function listGroup($qtt , $q)
    {
    	if ( !empty($qtt) ) {
    		$qtt = 20;
    	}
    	$listGroup = $this->adminRoler->where('name' , 'like', '%'.$q.'%')->orderBy('id', 'desc')->paginate($qtt);
    	return $listGroup;
    }

    public function createGroup($data)
    {
    	$data_create = [
    		'name' => $data['name'],
    		'description' => $data['description'],
    	];
    	$group = $this->adminRoler->create($data_create);
    	// ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tạo',
          'model' => 'Admin/AdminRole',
          'description' => ' nhóm <a href="/admin/admin-roles/'. $group->id .'/chi-tiet-nhóm-vai-tro-quan-tri-vien.html">'. $group->name .'</a>',
        ];
        $this->log_activity->create($data_log);
        return $group;
    }

    public function editGroup($data)
    {
        if ( !empty($data['name']) || !empty($data['id']) ) {
            $dataGroup = [
                'name' => $data['name'],
                'description' => $data['description'],
            ];
            $group = $this->adminRoler->find($data['id']);
            // ghi log
            $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'sửa',
                'model' => 'Admin/AdminRole',
                'admin' => ' nhóm <a href="/admin/admin-roles/'. $group->id .'/chi-tiet-nhóm-vai-tro-quan-tri-vien.html">'. $group->name .'</a>',
            ];
            $this->log_activity->create($data_log);

            return $group->update($dataGroup);
        }
        return false;
    }

    public function deleteGroup($id)
    {
        if ( !empty($id) ) {
            $group = $this->adminRoler->find($id);
            if ( !empty($group) ) {
                // ghi log
                $data_log = [
                    'user_id' => Auth::user()->id,
                    'action' => 'xóa',
                    'model' => 'Admin/AdminRole',
                    'admin' => ' nhóm quản trị ' . $group->name,
                ];
                $this->log_activity->create($data_log);

                return $group->delete();
            }
            return false;
        }
        return false;
    }

    public function loadDetailGroup($id)
    {
    	return $this->adminRoler->find($id);
    }

    public function loadUsers($id)
    {
    	return $this->user->where('admin_role_id', $id)->get();
    }

    public function loadUserAdmin($id)
    {
    	$list_user_meta = $this->user_meta->where('role', 'admin')->get();
    	$list_user = [];
    	foreach ($list_user_meta as $key => $userMeta) {
    		if ( $userMeta->user_id != 1 ) {
    			if ( !empty($userMeta->user ) ) {
    				if ( $userMeta->user->admin_role_id != $id) {
		    			$list_user[] = $userMeta->user;
		    		}
    			}
    		}
    	}
    	return $list_user;
    }

    // update user vào admin role  admin_role_id
    public function update_user_with_group($data)
    {
        $user_ids = $data['user_id'];
        $adminRoler = $this->adminRoler->find($data['id']);
        foreach ($user_ids as $key => $user_id) {
            // ghi log
            $user = $this->user->find($user_id);
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'update',
              'model' => 'Admin/AdminRole',
              'description' => $user->name . ' vào Admin Role ' . $adminRoler->name,
            ];
            $this->log_activity->create($data_log);
            $user->admin_role_id = $adminRoler->id;
            $update = $user->save();
            if (!$update) {
                return false;
            }
        }
        return true;
    }

    // Xóa user ra nhóm quản trị
    public function delete_user_with_group($data)
    {
        // ghi log
        $user = $this->user->find($data['id']);
        $adminRoler = $this->adminRoler->find($data['role_id']);
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'delete',
          'model' => 'Admin/AdminRole',
          'description' => $user->name . ' khỏi Admin Role ' . $adminRoler->name,
        ];
        $this->log_activity->create($data_log);

        $user->admin_role_id = 0;
        return $user->save();
    }

    //   lấy đặc quyền của nhóm
    public function load_admin_roles($id, $role)
    {
    	$data = ['data' => []];
    	if ( empty($role) ) {
    		$adminRoleMeta = $this->adminRoleMeta->where('admin_role_id', $id)->paginate(40);
    	} else {
    		$adminRoleMeta = $this->adminRoleMeta->where('admin_role_id', $id)->where('page', $role)->paginate(40);
    	}
    	foreach ($adminRoleMeta as $key => $role) {
    		$role_config = config('role');
    		$role->page = !empty($role_config[ $role->page ]) ? $role_config[ $role->page ] : '<span class="text-danger">Không xác định</span>';
    		$data['data'][] = $role;
    	}
    	$data['total'] = $adminRoleMeta->total();
        $data['perPage'] = $adminRoleMeta->perPage();
        $data['current_page'] = $adminRoleMeta->currentPage();
    	return $data;
    }

    public function load_roles($id)
    {
    	$group_admin_roles = $this->adminRoleMeta->where('admin_role_id', $id)->get();
    	$role_config = config('role');
    	$data = [];
    	foreach ($role_config as $key => $role) {
    		$check = false;
    		foreach ($group_admin_roles as $key2 => $group_admin_role) {
    			if ( $key == $group_admin_role->page ) {
    				$check = true;
    			}
    		}
    		if ( $check ) {
    			continue;
    		}
    		$data[$key] =  $role;
    	}
    	return $data;
    }

    // cập nhật admin role meta
    public function update_admin_role_meta($data)
    {
    	if ( isset($data['page']) ) {
    		$role_config = config('role');
    		$adminRoler = $this->adminRoler->find($data['id']);
    		foreach ($data['page'] as $key => $page) {
    			$data_create = [
    				'admin_role_id' => $adminRoler->id,
    				'page' => $page
    			];
    			$adminRoleMeta = $this->adminRoleMeta->create($data_create);
		        $data_log = [
		          'user_id' => Auth::user()->id,
		          'action' => 'update',
		          'model' => 'Admin/AdminRole',
		          'description' => 'Trang ' . $role_config[$page] . ' vào Admin Role ' . $adminRoler->name,
		        ];
		        $this->log_activity->create($data_log);
    		}
    		return true;
    	} 
    	return false;
    }

    // Xóa user ra nhóm quản trị
    public function delete_admin_role_meta($data)
    {
        // ghi log
        $adminRoleMeta = $this->adminRoleMeta->find($data['id']);
    	$role_config = config('role');
        $adminRoler = $this->adminRoler->find($data['role_id']);

        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'delete',
          'model' => 'Admin/AdminRole',
          'description' => 'Trang ' . $role_config[$adminRoleMeta->page] . ' khỏi Admin Role ' . $adminRoler->name,
        ];
        $this->log_activity->create($data_log);

        return $adminRoleMeta->delete();
    }

    public function checkRole($user_id, $page)
    {
    	return true;
    	$user = $this->user->find($user_id);
    	if ( $user->id == 1 || $user->name == 'admin' ) {
    		return true;
    	}
    	if ( !empty($user->admin_role) ) {
    		$admin_role = $user->admin_role;
	    	foreach ($admin_role->admin_role_meta as $key => $admin_role_meta) {
	    		if ( $admin_role_meta->page == $page ) {
	    			return true;
	    		}
	    	}
    	}
	    return false;
    }

}

?>