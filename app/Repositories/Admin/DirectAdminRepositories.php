<?php

namespace App\Repositories\Admin;

use App\Model\User;
use App\Services\DirectAdmin;
use App\Model\Product;
use App\Model\Hosting;
use App\Model\ServerHosting;
use App\Model\ReadNotification;
use App\Model\Notification;
use App\Model\EventPromotion;
use App\Model\LogActivity;
use Illuminate\Support\Facades\Auth;

class DirectAdminRepositories {

    protected $user;
    protected $da;
    protected $da_host;
    protected $da_port;
    protected $da_user;
    protected $da_password;
    protected $product;
    protected $hosting;
    protected $ip;
    protected $server_hosting;
    protected $type_server_hosting;
    protected $readnotification;
    protected $notification;
    protected $event_promotion;
    protected $log_activity;

    public function __construct()
    {
        $this->user = new User;
        $this->server_hosting = new ServerHosting;
        $this->product = new Product;
        $this->hosting = new Hosting;
        $this->notification = new Notification();
        $this->readnotification = new ReadNotification();
        $this->event_promotion = new EventPromotion();
        $this->log_activity = new LogActivity();
        // kiểm tra server hosting
        $server_hosting_active = $this->server_hosting->where('location', 'vn')->where('active', true)->first();
        if (isset($server_hosting_active)) {
            $this->da_host = $server_hosting_active['host'];
            $this->ip = $server_hosting_active['ip'];
            $this->da_port = $server_hosting_active['port'];
            $this->da_user = $server_hosting_active['user'];
            $this->da_password = $server_hosting_active['password'];
            $this->type_server_hosting = $server_hosting_active->id;
        } else {
            $da_config = config('da');
            $this->da_host = $da_config['host'];
            $this->ip = $da_config['ip'];
            $this->da_port = $da_config['port'];
            $this->da_user = $da_config['user'];
            $this->da_password = $da_config['password'];
            $this->type_server_hosting = false;
        }
    }

    public function url_control_panel()
    {
       $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
       $url = '<a href="' . $host_connect .'" target="_blank">' . $host_connect . '</a>';
       return $url;
    }

    public function orderCreateAccount($hostings, $due_date)
    {
        foreach ($hostings as $key => $hosting) {
            $user = $this->user->find($hosting->user_id);
            $product = $this->product->find($hosting->product_id);
            $event = $this->event_promotion->where('product_id', $hosting->product_id)->where('billing_cycle', $hosting->billing_cycle)->first();
            if ( isset($event) ) {
               $meta_user = $user->user_meta;
               $meta_user->point += $event->point;
               $meta_user->save();
               // Tạo thông báo cho user
               $content = '';
               $content .= '<p>Bạn được tặng ' . $event->point . ' Cloudzone Point vào tài khoản khi đăng ký gói dịch vụ ' . $hosting->product->name . '</p>';

               $data_notification = [
                   'name' => "Chúc mừng bạn đã nhận được KHUYẾN MÃI từ Cloudzone!",
                   'content' => $content,
                   'status' => 'Active',
               ];
               $infoNotification = $this->notification->create($data_notification);
               $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
            }

            $da = $this->createAccount($hosting->domain, $hosting->user, $hosting->password, $hosting->password, $user->email, $product->package);
            // $da['error'] = 0;
            if ($da['error'] == 0) {
                if ($due_date == 'one_time_pay') {
                    $due_date = '2099-12-31';
                }
                $data_hosting = [
                    'status' => 'Active',
                    'paid' => 'paid',
                    'status_hosting' => 'on',
                    'date_create' => date('Y-m-d'),
                    'next_due_date' => $due_date,
                    'server_hosting_id' => $this->type_server_hosting,
                ];
                $hosting->update($data_hosting);
                try {
                    // ghi log
                    $text_vps = '';
                    if (!empty($hosting->domain)) {
                       $text_vps = ' <a target="_blank" href="/admin/hostings/detail/' . $hosting->id  . '">' . $hosting->domain . '</a>';
                    }
                    $data_log = [
                      'user_id' => 999997,
                      'action' => 'tạo',
                      'model' => 'Admin/DirectAdmin',
                      'description' => ' Hosting ' . $text_vps . ' thành công ',
                      'service' =>  $hosting->domain,
                    ];
                    $this->log_activity->create($data_log);
                } catch (\Exception $e) {
                    continue;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    public function adminCreateHosting($hosting, $due_date)
    {
        $user = $this->user->find($hosting->user_id);
        $product = $this->product->find($hosting->product_id);
        $event = $this->event_promotion->where('product_id', $hosting->product_id)->where('billing_cycle', $hosting->billing_cycle)->first();
        // if ( isset($event) ) {
        //    $meta_user = $user->user_meta;
        //    $meta_user->point += $event->point;
        //    $meta_user->save();
        //    // Tạo thông báo cho user
        //    $content = '';
        //    $content .= '<p>Bạn được tặng ' . $event->point . ' Cloudzone Point vào tài khoản.</p>';
        //
        //    $data_notification = [
        //        'name' => "Chúc mừng bạn đã nhận được KHUYẾN MÃI từ Cloudzone!",
        //        'content' => $content,
        //        'status' => 'Active',
        //    ];
        //    $infoNotification = $this->notification->create($data_notification);
        //    $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
        // }

        $da = $this->adminCreateAccount($hosting->domain, $hosting->user, $hosting->password, $hosting->password, $user->email, $product->package, $hosting->server_hosting_id);
        // $da['error'] = 0;
        if ($da['error'] == 0) {
            if ($due_date == 'one_time_pay') {
                $due_date = '2099-12-31';
            }
            $data_hosting = [
                'status' => 'Active',
                'paid' => 'paid',
                'status_hosting' => 'on',
                'date_create' => date('Y-m-d'),
                'next_due_date' => $due_date,
                'server_hosting_id' => $this->type_server_hosting,
            ];
            $hosting->update($data_hosting);
        } else {
            return false;
        }
    }

    public function orderCreateAccountWithApi($hosting, $due_date)
    {
        $user = $this->user->find($hosting->user_id);
        $product = $this->product->find($hosting->product_id);
        $event = $this->event_promotion->where('product_id', $hosting->product_id)->where('billing_cycle', $hosting->billing_cycle)->first();
        // if ( isset($event) ) {
        //    $meta_user = $user->user_meta;
        //    $meta_user->point += $event->point;
        //    $meta_user->save();
        //    // Tạo thông báo cho user
        //    $content = '';
        //    $content .= '<p>Bạn được tặng ' . $event->point . ' Cloudzone Point vào tài khoản.</p>';
        //
        //    $data_notification = [
        //        'name' => "Chúc mừng bạn đã nhận được KHUYẾN MÃI từ Cloudzone!",
        //        'content' => $content,
        //        'status' => 'Active',
        //    ];
        //    $infoNotification = $this->notification->create($data_notification);
        //    $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
        // }

        $da = $this->adminCreateAccount($hosting->domain, $hosting->user, $hosting->password, $hosting->password, $user->email, $product->package, $hosting->server_hosting_id);
        // $da['error'] = 0;
        if ($da['error'] == 0) {
            if ($due_date == 'one_time_pay') {
                $due_date = '2099-12-31';
            }
            $data_hosting = [
                'status' => 'Active',
                'paid' => 'paid',
                'status_hosting' => 'on',
                'date_create' => date('Y-m-d'),
                'next_due_date' => $due_date,
                'server_hosting_id' => $this->type_server_hosting,
            ];
            $hosting->update($data_hosting);
        } else {
            return false;
        }
        return $hosting;
    }

    public function orderCreateAccountWithPoint($hostings, $due_date)
    {
        foreach ($hostings as $key => $hosting) {
            $user = $this->user->find($hosting->user_id);
            $product = $this->product->find($hosting->product_id);
            $da = $this->createAccount($hosting->domain, $hosting->user, $hosting->password, $hosting->password, $user->email, $product->package);
            // $da['error'] = 0;
            if ($da['error'] == 0) {
                if ($due_date == 'one_time_pay') {
                    $due_date = '2099-12-31';
                }
                $data_hosting = [
                    'status' => 'Active',
                    'paid' => 'paid',
                    'status_hosting' => 'on',
                    'date_create' => date('Y-m-d'),
                    'next_due_date' => $due_date,
                    'server_hosting_id' => $this->type_server_hosting,
                ];
                $hosting->update($data_hosting);
                try {
                    // ghi log
                    $text_vps = '';
                    if (!empty($hosting->domain)) {
                       $text_vps = ' <a target="_blank" href="/admin/hostings/detail/' . $hosting->id  . '">' . $hosting->domain . '</a>';
                    }
                    $data_log = [
                      'user_id' => 999997,
                      'action' => 'tạo',
                      'model' => 'Admin/DirectAdmin',
                      'description' => ' Hosting ' . $text_vps . ' thành công bằng Cloudzone Point',
                      'service' =>  $hosting->domain,
                    ];
                    $this->log_activity->create($data_log);
                } catch (\Exception $e) {
                    continue;
                }
            } else {
                return false;
            }
        }
        return true;
    }
    // tạo 1 hosting
    public function createHosting($hosting, $due_date)
    {
        $user = $this->user->find($hosting->user_id);
        $product = $this->product->find($hosting->product_id);
        $da = $this->createAccount($hosting->domain, $hosting->user, $hosting->password, $hosting->password, $user->email, $product->package);
        if ($da['error'] == 0) {
            $data_hosting = [
                'status' => 'Active',
                'paid' => 'paid',
                'status_hosting' => 'on',
                'date_create' => date('Y-m-d'),
                'next_due_date' => $due_date,
                'server_hosting_id' => $this->type_server_hosting,
            ];
            $hosting->update($data_hosting);
        } else {
            return false;
        }
        return true;
    }
    // User yêu cầu xóa hosting
    public function suspendHosting($hosting)
    {
        try {
            $da = $this->suspendAccount($hosting);
        } catch (Exception $e) {
            report($e);
        }
        if ($da['error'] == 0) {
            $hosting->status_hosting = 'cancel';
            $hosting->save();
            try {
                // ghi log
                $text_vps = '';
                if (!empty($hosting->domain)) {
                   $text_vps = ' <a target="_blank" href="/admin/hostings/detail/' . $hosting->id  . '">' . $hosting->domain . '</a>';
                }
                $data_log = [
                  'user_id' => 999997,
                  'action' => 'tắt',
                  'model' => 'Admin/DirectAdmin',
                  'description' => ' Hosting ' . $text_vps . ' thành công',
                  'service' =>  $hosting->domain,
                ];
                $this->log_activity->create($data_log);
            } catch (\Exception $e) {
                return true;
            }
            return true;
        } else {
            return false;
        }
    }
    // User yêu cầu tat hosting
    public function off($hosting)
    {
        $da = $this->suspendAccount($hosting);
        if ($da['error'] == 0) {
            $hosting->status_hosting = 'off';
            $hosting->save();
            try {
                // ghi log
                $text_vps = '';
                if (!empty($hosting->domain)) {
                   $text_vps = ' <a target="_blank" href="/admin/hostings/detail/' . $hosting->id  . '">' . $hosting->domain . '</a>';
                }
                $data_log = [
                  'user_id' => 999997,
                  'action' => 'tắt',
                  'model' => 'Admin/DirectAdmin',
                  'description' => ' Hosting ' . $text_vps . ' thành công',
                  'service' =>  $hosting->domain,
                ];
                $this->log_activity->create($data_log);
            } catch (\Exception $e) {
                report($e);
                return true;
            }
            return true;
        } else {
            return false;
        }
    }
    // User yêu cầu bật hosting bị suppen
    public function unSuspend($hosting)
    {
        if ($hosting->status_hosting == 'admin_off') {
            $data = [
              'error' => 2223
            ];
            return $data;
        }
        if ($hosting->status_hosting == 'cancel') {
            $data = [
              'error' => 2224
            ];
            return $data;
        }
        $da = $this->unSuspendAccount($hosting);
        if ($da['error'] == 0) {
            $hosting->status_hosting = 'on';
            $hosting->save();
            try {
                // ghi log
                $text_vps = '';
                if (!empty($hosting->domain)) {
                   $text_vps = ' <a target="_blank" href="/admin/hostings/detail/' . $hosting->id  . '">' . $hosting->domain . '</a>';
                }
                $data_log = [
                  'user_id' => 999997,
                  'action' => 'bật',
                  'model' => 'Admin/DirectAdmin',
                  'description' => ' Hosting ' . $text_vps . ' thành công',
                  'service' =>  $hosting->domain,
                ];
                $this->log_activity->create($data_log);
            } catch (\Exception $e) {
                report($e);
                return true;
            }
            return true;
        } else {
            return false;
        }
    }
    // User yêu cầu bật hosting bị suppen
    public function admin_unSuspend($hosting)
    {
        $da = $this->unSuspendAccount($hosting);
        if ($da['error'] == 0) {
            $hosting->status_hosting = 'on';
            $hosting->save();
            return true;
        } else {
            return false;
        }
    }
    /*
    * Admin
    */
    public function adminSuspend($hosting)
    {
        $da = $this->suspendAccount($hosting);
        if ($da['error'] == 0) {
            $hosting->status_hosting = 'admin_off';
            $hosting->save();
            return true;
        } else {
            return false;
        }
    }
    /*
    * Admin
    */
    public function expire_hosting($hosting)
    {
        $da = $this->suspendAccount($hosting);
        if ($da['error'] == 0) {
            $hosting->status_hosting = 'expire';
            $hosting->save();
            return true;
        } else {
            return false;
        }
    }

    public function delete_hosting($hosting)
    {
        $da = $this->suspendAccount($hosting);
        if ($da['error'] == 0) {
            $hosting->status_hosting = 'delete';
            $hosting->save();
            return true;
        } else {
            return false;
        }
    }
    // Module directAdmin
    public function showPackage()
    {
        // $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $result = $da->query("CMD_API_PACKAGES_USER");
        return $result;
    }

    public function createAccount($domain, $username, $passwd, $passwd2, $email, $package = 'basic')
    {
        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $result = $da->query("CMD_API_ACCOUNT_USER", [
            'action' => 'create',
            'add' => 'Submit',
            'domain' => $domain, //uthientest.cloudzone.vn
            'username' => $username,
            'passwd' => $passwd,
            'passwd2' => $passwd2,
            'email' => $email,
            'package' => !empty($package)? $package : 'basic',
            'ip' => $this->ip,
        ], "POST");
        return $result;
    }

    public function adminCreateAccount($domain, $username, $passwd, $passwd2, $email, $package = 'basic', $server_hosting_id)
    {
        if ( !empty($server_hosting_id) ) {
            $server_hosting = $this->server_hosting->find($server_hosting_id);

            $host_connect = 'https://' . $server_hosting->host . ':' . $server_hosting->port;
            // dd($host_connect);
            $da = new DirectAdmin($host_connect, $server_hosting->user, $server_hosting->password);
            $ip = $server_hosting->ip;
        } else {
            $da_config = config('da');

            $host_connect = 'https://' . $da_config['host'] . ':' . $da_config['port'];
            // dd($host_connect);
            $da = new DirectAdmin($host_connect, $da_config['user'] , $da_config['password']);
            $ip = $da_config['ip'];
        }
        $result = $da->query("CMD_API_ACCOUNT_USER", [
            'action' => 'create',
            'add' => 'Submit',
            'domain' => $domain, //uthientest.cloudzone.vn
            'username' => $username,
            'passwd' => $passwd,
            'passwd2' => $passwd2,
            'email' => $email,
            'package' => !empty($package)? $package : 'basic',
            'ip' => $ip,
        ], "POST");
        return $result;
    }

    public function deleteAccount($hosting)
    {
        if ( $hosting->status == 'Active' ) {
            // if (!empty($hosting->server_hosting_id)) {
            //     $server_hosting = $hosting->server_hosting;
            //     $this->da_host = $server_hosting->host;
            //     $this->ip = $server_hosting->ip;
            //     $this->da_port = $server_hosting->port;
            //     $this->da_user = $server_hosting->user;
            //     $this->da_password = $server_hosting->password;
            // } else {
            //     $da_config = config('da');
            //     $this->da_host = $da_config['host'];
            //     $this->ip = $da_config['ip'];
            //     $this->da_port = $da_config['port'];
            //     $this->da_user = $da_config['user'];
            //     $this->da_password = $da_config['password'];
            // }
            // $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
            // dd($host_connect, $this->da_user, $this->da_password);
            // $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
            // $result = $da->query("CMD_API_SELECT_USERS", [
            //     'confirmed' => 'Confirm',
            //     'delete' => 'yes',
            //     'select0' => $hosting->user,
            // ], "POST");
            $da = $this->suspendAccount($hosting);
            $hosting->status_hosting = 'delete';
            $hosting->save();
            return true;
        } else {
            $hosting->delete();
            return true;
        }
    }

    public function showIP()
    {
        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect, $this->da_user, $this->da_password);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $result = $da->query("CMD_API_SHOW_RESELLER_IPS");
        return $result;
    }

    public function suspendAccount($hosting)
    {
        if (!empty($hosting->server_hosting_id)) {
            $server_hosting = $hosting->server_hosting;
            $this->da_host = $server_hosting->host;
            $this->ip = $server_hosting->ip;
            $this->da_port = $server_hosting->port;
            $this->da_user = $server_hosting->user;
            $this->da_password = $server_hosting->password;
        } else {
            $da_config = config('da');
            $this->da_host = $da_config['host'];
            $this->ip = $da_config['ip'];
            $this->da_port = $da_config['port'];
            $this->da_user = $da_config['user'];
            $this->da_password = $da_config['password'];
        }
        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $result = $da->query("CMD_API_SELECT_USERS", [
            'location' => 'CMD_SELECT_USERS',
            'suspend' => 'Suspend',
            'select0' => $hosting->user,
        ], "POST");
        return $result;
    }

    public function unSuspendAccount($hosting)
    {
        if (!empty($hosting->server_hosting_id)) {
            $server_hosting = $hosting->server_hosting;
            $this->da_host = $server_hosting->host;
            $this->ip = $server_hosting->ip;
            $this->da_port = $server_hosting->port;
            $this->da_user = $server_hosting->user;
            $this->da_password = $server_hosting->password;
        } else {
            $da_config = config('da');
            $this->da_host = $da_config['host'];
            $this->ip = $da_config['ip'];
            $this->da_port = $da_config['port'];
            $this->da_user = $da_config['user'];
            $this->da_password = $da_config['password'];
        }
        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $result = $da->query("CMD_API_SELECT_USERS", [
            'location' => 'CMD_SELECT_USERS',
            'suspend' => 'Unsuspend',
            'select0' => $hosting->user,
        ], "POST");
        return $result;
    }

    public function login_as($username, $passwd)
    {
    // dd($username);
        // $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // $da =  new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        // dd($username);
        // $result = $da->query("CMD_API_LOGIN_KEYS", [
        //     'keyname' => $username,
        //     'key' => '3xKsz45iz6ts65G0X666HjaD4PU5U227Q99427bwvk8O4861PN1RBYY7uO95e609',
        //     'key2' => '3xKsz45iz6ts65G0X666HjaD4PU5U227Q99427bwvk8O4861PN1RBYY7uO95e609',
        //     'never_expires' => 'no',
        //     'expiry_timestamp' => 100000,
        //     'max_uses' => 1,
        //     'clear_key' => 'yes',
        //     'allow_html' => 'no',
        //     'passwd' => $passwd,
        // ], 'POST');
        // $result = $da->login_as('letanhi');
        // $key = $this->rand_string(64);
        // $result2 = $da->query("CMD_API_LOGIN_KEYS", [
        //   'action' =>'create',
        //     'type' => 'one_time_url',
        //     'keyname' => 'letanhi',
        //     'key' => $key,
        //     'key2' => $key,
        //     'type' => 'url',
        //     'never_expires' => 'no',
        //     'clear_key' => 'yes',
        //     'allow_html' => 'yes',
        //     'max_uses' => 1,
        //     'passwd' => $this->da_password,
        // ], 'POST');
        // $result2 = $da->query("CMD_API_LOGIN_KEYS", [
        //     'action' => 'show_modify',
        //     'keyname' => 'letanhi',
        // ]);
        // dd($result,$result2,$key);
        // if ($result2['error'] == 0) {
        //   $da->logout();
        //     $da->logout();
        //     return $key;
        // }
        return false;
    }

    public function getHostingDaPortal($id)
    {
        $data  = [];
        $data_search = [];
        // kiểm tra server hosting
        $server_hosting_active = $this->server_hosting->where('id', $id)->first();
        if (isset($server_hosting_active)) {
            $this->da_host = $server_hosting_active['host'];
            $this->ip = $server_hosting_active['ip'];
            $this->da_port = $server_hosting_active['port'];
            $this->da_user = $server_hosting_active['user'];
            $this->da_password = $server_hosting_active['password'];
            $this->type_server_hosting = $server_hosting_active->id;
        } else {
            $da_config = config('da');
            $this->da_host = $da_config['host'];
            $this->ip = $da_config['ip'];
            $this->da_port = $da_config['port'];
            $this->da_user = $da_config['user'];
            $this->da_password = $da_config['password'];
            $this->type_server_hosting = false;
        }
        // $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $results = $da->query("CMD_API_SHOW_USERS");
        foreach ($results as $key => $result) {
            $data[] = $da->query("CMD_API_SHOW_USER_CONFIG" , [
                'user' => $result,
            ]);
        }
        foreach ($data as $key => $hosting) {
            $check = $this->hosting->where('domain', $hosting['domain'])->first();
            if (isset($check)) {
                $hosting['user_pt'] = $check->user_hosting;
            } else {
                $hosting['user_pt'] = 0;
            }
            $data_search[] = $hosting;
        }
        return $data_search;
    }

    public function getHostingDaSingapore()
    {
        $data  = [];
        $data_search = [];
        // kiểm tra server hosting
        $server_hosting_active = $this->server_hosting->where('id', $id)->first();
        if (isset($server_hosting_active)) {
            $this->da_host = $server_hosting_active['host'];
            $this->ip = $server_hosting_active['ip'];
            $this->da_port = $server_hosting_active['port'];
            $this->da_user = $server_hosting_active['user'];
            $this->da_password = $server_hosting_active['password'];
            $this->type_server_hosting = $server_hosting_active->id;
        } else {
            $da_config = config('da_si');
            $this->da_host = $da_config['host'];
            $this->ip = $da_config['ip'];
            $this->da_port = $da_config['port'];
            $this->da_user = $da_config['user'];
            $this->da_password = $da_config['password'];
            $this->type_server_hosting = false;
        }
        // $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $results = $da->query("CMD_API_SHOW_USERS");
        foreach ($results as $key => $result) {
            $data[] = $da->query("CMD_API_SHOW_USER_CONFIG" , [
                'user' => $result,
            ]);
        }
        foreach ($data as $key => $hosting) {
            $check = $this->hosting->where('domain', $hosting['domain'])->first();
            if (isset($check)) {
                $hosting['user_pt'] = $check->user_hosting;
            } else {
                $hosting['user_pt'] = 0;
            }
            $data_search[] = $hosting;
        }
        return $data_search;
    }

    public function getUserHostingDaPortal($username, $id)
    {
        $server_hosting_active = $this->server_hosting->where('id', $id)->first();
        if (isset($server_hosting_active)) {
            $this->da_host = $server_hosting_active['host'];
            $this->ip = $server_hosting_active['ip'];
            $this->da_port = $server_hosting_active['port'];
            $this->da_user = $server_hosting_active['user'];
            $this->da_password = $server_hosting_active['password'];
            $this->type_server_hosting = $server_hosting_active->id;
        } else {
            $da_config = config('da');
            $this->da_host = $da_config['host'];
            $this->ip = $da_config['ip'];
            $this->da_port = $da_config['port'];
            $this->da_user = $da_config['user'];
            $this->da_password = $da_config['password'];
            $this->type_server_hosting = false;
        }
        // $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $result = $da->query("CMD_API_SHOW_USER_CONFIG" , [
            'user' => $username,
        ]);
        return $result;
    }

    function rand_string($n) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen( $chars );
        $str = '';
        for( $i = 0; $i < $n; $i++ ) {
            $str .= $chars[ rand( 0, $size - 1 ) ];
        }
        return $str;
    }

    public function upgrade($hosting, $package)
    {
        $user = $this->user->find($hosting->user_id);
        $product = $this->product->find($hosting->product_id);
        // $da = $this->createAccount($hosting->domain, $hosting->user, $hosting->password, $hosting->password, $user->email, $product->package);
        if (!empty($hosting->server_hosting_id)) {
            $server_hosting = $hosting->server_hosting;
            $this->da_host = $server_hosting->host;
            $this->ip = $server_hosting->ip;
            $this->da_port = $server_hosting->port;
            $this->da_user = $server_hosting->user;
            $this->da_password = $server_hosting->password;
        } else {
            $da_config = config('da');
            $this->da_host = $da_config['host'];
            $this->ip = $da_config['ip'];
            $this->da_port = $da_config['port'];
            $this->da_user = $da_config['user'];
            $this->da_password = $da_config['password'];
        }

        $host_connect = 'https://' . $this->da_host . ':' . $this->da_port;
        // dd($host_connect);
        $da = new DirectAdmin($host_connect, $this->da_user, $this->da_password);
        $result = $da->query("CMD_API_MODIFY_USER", [
            'action' => 'package',
            'user' => $hosting->user,
            'package' => $package, //uthientest.cloudzone.vn
        ], "POST");
        try {
            // ghi log
            $text_vps = '';
            if (!empty($hosting->domain)) {
               $text_vps = ' <a target="_blank" href="/admin/hostings/detail/' . $hosting->id  . '">' . $hosting->domain . '</a>';
            }
            $data_log = [
              'user_id' => 999997,
              'action' => 'nâng cấp',
              'model' => 'Admin/DirectAdmin',
              'description' => ' Hosting ' . $text_vps . ' thành công',
              'service' =>  $hosting->domain,
            ];
            $this->log_activity->create($data_log);
        } catch (\Exception $e) {
            return $result;
        }
        return $result;
    }

}
