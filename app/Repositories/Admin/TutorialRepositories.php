<?php

namespace App\Repositories\Admin;

use App\Model\Tutorial;
use App\Model\LogActivity;
use Illuminate\Support\Facades\Auth;

class TutorialRepositories {

    protected $tutorial;
    protected $log_activity;

    public function __construct()
    {
        $this->tutorial = new Tutorial;
        $this->log_activity = new LogActivity();
    }

    public function get_all_tutorial()
    {
        return $this->tutorial->orderBy('id', 'desc')->paginate(30);
    }

    public function list_tutorial()
    {
        return $this->tutorial->orderBy('id', 'desc')->paginate(30);
    }

    public function detail_tutorial($id)
    {
        return $this->tutorial->find($id);
    }

    public function create($data)
    {
       $data_create = [
          'title' => $data['title'],
          'link' => $data['link'],
          'descripton' => !empty($data['descripton']) ? $data['descripton'] : '',
       ];
       $create =  $this->tutorial->create($data_create);
       // ghi log
       $data_log = [
         'user_id' => Auth::user()->id,
         'action' => 'tạo',
         'model' => 'Admin/Tutorial',
         'description' => ' liên kết hướng dẫn ' . $create->title,
       ];
       $this->log_activity->create($data_log);
       return $create;
    }

    public function update($data)
    {
        $data_update = [
           'title' => $data['title'],
           'link' => $data['link'],
           'descripton' => !empty($data['descripton']) ? $data['descripton'] : '',
        ];
        $update =  $this->tutorial->find($data['tutorial_id'])->update($data_update);
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'chỉnh sửa',
          'model' => 'Admin/Tutorial',
          'description' => ' liên kết hướng dẫn ' . $data['title'],
        ];
        $this->log_activity->create($data_log);
        return $update;
    }

    public function delete_tutorial($id)
    {
        $delete =  $this->tutorial->find($id);
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'xóa',
          'model' => 'Admin/Tutorial',
          'description' => ' liên kết hướng dẫn ' . $delete->title,
        ];
        $this->log_activity->create($data_log);
        return $delete->delete();
    }

}

?>
