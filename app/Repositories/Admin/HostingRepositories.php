<?php

namespace App\Repositories\Admin;

use App\Model\User;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\Server;
use App\Model\Hosting;
use App\Model\Product;
use App\Model\HistoryPay;
use App\Model\Credit;
use App\Model\LogActivity;
use Illuminate\Support\Facades\Auth;

class HostingRepositories {

    protected $user;
    protected $order;
    protected $detail_order;
    protected $vps;
    protected $server;
    protected $hosting;
    protected $product;
    protected $history_pay;
    protected $credit;
    protected $event;
    protected $log_activity;

    public function __construct()
    {
        $this->user = new User;
        $this->order = new Order;
        $this->detail_order = new DetailOrder;
        $this->vps = new Vps;
        $this->server = new Server;
        $this->hosting = new Hosting;
        $this->product = new Product;
        $this->history_pay = new HistoryPay;
        $this->credit = new Credit;
        $this->log_activity = new LogActivity();
    }

    public function list_hostings()
    {
        return $this->hosting->where('location', 'vn')->with('user_hosting','detail_order')->orderBy('id', 'desc')->paginate(30);
    }

    public function list_hosting_singapore()
    {
        return $this->hosting->where('location', 'si')->with('user_hosting','detail_order')->orderBy('id', 'desc')->paginate(30);
    }

    public function listHostingByPageDetailUser($userId)
    {
        return $this->hosting->where('user_id',$userId)->with('product')->orderBy('id', 'desc')->paginate(30);
    }

    public function listHostingByUser($userId)
    {
        $hostings = $this->hosting->where('user_id',$userId)->with('product')->orderBy('id', 'desc')->paginate(30);
        $data = [];
        $data['data'] = [];
        $billing = config('billing');
        foreach ($hostings as $key => $hosting) {
            $hosting->product_name = $hosting->product->name;
            $hosting->start_date = date('h:i:s d-m-Y' , strtotime($hosting->created_at));
            $hosting->end_date = date('h:i:s d-m-Y' , strtotime($hosting->next_due_date));
            $hosting->billing_cycle = !empty($billing[$hosting->billing_cycle]) ? $billing[$hosting->billing_cycle] : '1 Tháng';
            if ( $hosting->status_hosting == 'off' ) {
                $hosting->status_hosting = '<td><span class="text-danger">Đã tắt</span></td>';
            }
            elseif ($hosting->status_hosting == 'on') {
                $hosting->status_hosting = '<td><span class="text-success">Đang bật</span></td>';
            }
            elseif ($hosting->status_hosting == 'admin_off') {
                $hosting->status_hosting = '<td><span class="text-success">Admin tắt</span></td>';
            }
            elseif ($hosting->status_hosting == 'expire') {
                $hosting->status_hosting = '<td><span class="text-danger">Hết hạn</span></td>';
            }
            elseif ($hosting->status_hosting == 'cancel') {
                $hosting->status_hosting = '<td><span class="text-danger">Đã hủy</span></td>';
            }
            $data['data'][] = $hosting;
        }
        $data['total'] = $hostings->total();
        $data['perPage'] = $hostings->perPage();
        $data['current_page'] = $hostings->currentPage();
        return $data;
    }
    public function detail_hosting($id)
    {
        return $this->hosting->find($id);
    }

    public function totalByUser($userId){
        return $this->hosting->where('user_id',$userId)->where('status','Active')->count();
    }

    public function countUsed($userId){
        return $this->hosting->where('user_id',$userId)->where('status','Active')
        ->whereIn('status_hosting', [ 'on', 'off', 'admin_off' ])
        ->count();
    }

    public function countExpire($userId)
    {
        return $this->hosting->where('user_id',$userId)->where('status','Active')
            ->where('status_hosting', 'expire')
            ->count();
    }

    public function countDelete($userId)
    {
        return $this->hosting->where('user_id',$userId)->where('status','Active')
            ->whereIn('status_hosting', [ 'delete', 'change_user', 'cancel' ])
            ->count();
    }

    public function update_sevice_hosting($data)
    {
        $hosting = $this->hosting->find($data['id']);
        $data_hosting = [
            'user_id' => $data['user_id'],
            'product_id' => !empty($data['product_id']) ? $data['product_id'] : $hosting->product_id,
            'domain' => !empty($data['domain']) ? $data['domain'] : '',
            'user' => !empty($data['sevices_username']) ? $data['sevices_username'] : '',
            'password' => !empty($data['sevices_password']) ? $data['sevices_password'] : '',
            'status' => !empty($data['status']) ? $data['status'] : '',
            'price_override' => !empty($data['price_override']) ? $data['sub_total'] : '',
            'expire_billing_cycle' => !empty($data['expire_billing_cycle']) ? $data['expire_billing_cycle'] : 0,
        ];
        if (!empty($data['next_due_date'])) {
            $data_hosting['next_due_date'] = date('Y-m-d', strtotime($data['next_due_date']));
        }

        if (!empty($data['date_create'])) {
            $data_hosting['date_create'] = date('Y-m-d', strtotime($data['date_create']));
        }

        if (!empty($data['sub_total'])) {
            $invoice = $this->detail_order->find($data['invoice_id']);
            $invoice->sub_total = $data['sub_total'];
            $invoice->save();
        }
        if (!empty($data['billing_cycle'])) {
            $hosting = $this->hosting->find($data['id']);
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $date_start = date( 'd-m-Y', strtotime( '-'.$billing[$hosting->billing_cycle] , strtotime($hosting->next_due_date) ) );
            $due_date = date('Y-m-d', strtotime( $date_start . $billing[ $data['billing_cycle'] ] ));
            $data_hosting['next_due_date'] = $due_date;
            $data_hosting['billing_cycle'] = $data['billing_cycle'];
            $invoice = $this->detail_order->find($data['invoice_id']);
            if ($invoice->quantity == 1) {
                $invoice->due_date = $due_date;
                $invoice->save();
            }
        }
        if (!empty($data['paid_date'])) {
          $invoice = $this->detail_order->find($data['invoice_id']);
          $invoice->paid_date = date('Y-m-d', strtotime( $data['paid_date'] ));
          $invoice->save();
        }
        $hosting = $this->hosting->find($data['id']);
        $update = $hosting->update($data_hosting);
        // Ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'chỉnh sửa',
          'model' => 'Admin/Hosting',
          'description' => ' Hosting <a target="_blank" href="/admin/hostings/detail/' . $hosting->id . '">' . $hosting->domain . '</a>',
          'service' =>  $hosting->domain,
        ];
        $this->log_activity->create($data_log);
        return $update;
    }

    public function setHostingWithPortal($data)
    {

        $hosting = $this->hosting->where('domain', $data['domain'])->first();
        if (!isset($hosting)) {
            $next_due_date = $data['next_due_date'];
            if (empty($next_due_date)) {
              $billing = [
                  'monthly' => '1 Month',
                  'twomonthly' => '2 Month',
                  'quarterly' => '3 Month',
                  'semi_annually' => '6 Month',
                  'annually' => '1 Year',
                  'biennially' => '2 Year',
                  'triennially' => '3 Year'
              ];
              $date_start = date( 'Y-m-d' );
              $next_due_date = date('Y-m-d', strtotime( $date_start . $billing[ $data['billing_cycle'] ] ));
            }
            else {
              $next_due_date = date('Y-m-d', strtotime($next_due_date));
            }

            $product = $this->product->find($data['product_id']);

            $data_order = [
                'user_id' => $data['user_id'],
                'total' => !empty($data['price_override']) ? $data['total'] : $product->pricing[$data['billing_cycle']],
                'status' => 'Finish',
                'description' => '',
                'type' => 'order',
            ];
            $order = $this->order->create($data_order);

            $data_detail_order = [
                'order_id' => $order->id,
                'user_id' => $data['user_id'],
                'type' => 'Hosting',
                'paid_date' => date('Y-m-d'),
                'sub_total' => !empty($data['price_override']) ? $data['total'] : $product->pricing[$data['billing_cycle']],
                'quantity' => 1,
                'due_date' => $next_due_date,
                'status' => 'paid',
                'payment_method' => 1,
                'date_paid' => date('Y-m-d'),
                'purpose' => 'create',
            ];
            $detail_order = $this->detail_order->create($data_detail_order);

            $data_history = [
                'user_id' => $data['user_id'],
                'ma_gd' => 'GDODH' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '2',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> !empty($data['price_override']) ? $data['total'] : $product->pricing[$data['billing_cycle']],
                'status' => 'Active',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);

            $data_hosting = [
               'detail_order_id' => $detail_order->id,
               'user_id' => $data['user_id'],
               'product_id' => $data['product_id'],
               'domain' => $data['domain'],
               'billing_cycle' => $data['billing_cycle'],
               'status' => $data['status'],
               'user' => $data['sevices_username'],
               'date_create' => date('Y-m-d'),
               'paid' => 'paid',
               'status_hosting' => 'on',
               'next_due_date' => $next_due_date,
               'server_hosting_id' => !empty($data['server_hosting_id']) ? $data['server_hosting_id'] : 0,
               'location' => $data['location'],
               'price_override' => !empty($data['price_override']) ? $data['total'] : '',
               'price_override' => !empty($data['expire_billing_cycle']) ? $data['expire_billing_cycle'] : 0,
            ];
            $create_hosting = $this->hosting->create($data_hosting);

            if (!empty($data['sub_value'])) {
              $credit = $this->credit->where('user_id' , $data['user_id'])->first();
              if (isset($credit)) {
                if (!empty($data['price_override'])) {
                   $credit->value = $credit->value - $data['total'];
                   $credit->save();
                } else {
                   $credit->value = $product->pricing[$data['billing_cycle']];
                   $credit->save();
                }
              }
            }
            // Ghi log
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'thêm',
              'model' => 'Admin/Hosting',
              'description' => ' Hosting  <a target="_blank" href="/admin/hostings/detail/' . $create_hosting->id . '">' . $create_hosting->domain . '</a> từ <a href="/admin/hostings/get-hosting-daportal/' . $data['server_hosting_id'] . '">#' . $data['server_hosting_id'] . '</a> ',
              'service' =>  $create_hosting->domain,
            ];
            $this->log_activity->create($data_log);
            if ($create_hosting) {
                $data['success'] = true;
                return $data;
            } else {
                return false;
            }

        } else {
            $data = ['success' => '' , 'error' => true];
            return $data;
        }
    }

    public function search($q, $status, $sort)
    {
        $data = [];
        $data['data'] = [];

        if ( empty($status) ) {
          if ( empty($sort) ) {
              $list_hosting = $this->hosting->where('domain', 'like', '%'.$q.'%')
                ->with('user_hosting', 'product')
                ->orderBy('id', 'desc')->paginate(30);
          }
          else {
              $list_hosting = $this->hosting->where('domain', 'like', '%'.$q.'%')
                ->with('user_hosting', 'product')
                ->where('next_due_date' , '!=', null)
                ->orderByRaw('next_due_date ' . $sort)
                ->where(function($query)
                {
                    return $query
                      ->orwhere('status_vps', 'on')
                      ->orWhere('status_vps', 'off')
                      ->orWhere('status_vps', 'admin_off')
                      ->orWhere('status_vps', 'progressing');
                })->paginate(30);
          }
          if ($list_hosting->count() > 0) {
              foreach ($list_hosting as $key => $hosting) {
                  $product = $hosting->product;
                  if (!empty($product)) {
                      if ($product->group_product->private != 0) {
                          $hosting->private = true;
                      } else {
                          $hosting->private = false;
                      }
                      $hosting->id_product = $product->id;
                      $hosting->name_product = $product->name;
                  }
                  if ( !empty($hosting->price_override) ) {
                     $price_hosting = $hosting->price_override;
                     if (!empty($hosting->order_upgrade_hosting)) {
                       foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                          $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                       }
                     }
                  } else {
                     $price_hosting = !empty($hosting->detail_order->sub_total) ? $hosting->detail_order->sub_total : 0;
                     if (!empty($hosting->order_upgrade_hosting)) {
                       foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                          $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                       }
                     }
                  }
                  $hosting->price_hosting = $price_hosting;
                  $data['data'][] = $hosting;
              }

          }
          else {
            $list_hosting = $this->hosting->from('hostings as o')->select(['o.*'])->join('users as u','o.user_id','=','u.id')->where('u.name' , 'like', '%'.$q.'%')->with('user_hosting', 'product')->orderBy('id', 'desc')->paginate(30);
            foreach ($list_hosting as $key => $hosting) {
                  $product = $hosting->product;
                  if (!empty($product)) {
                      if ($product->group_product->private != 0) {
                          $hosting->private = true;
                      } else {
                          $hosting->private = false;
                      }
                      $hosting->id_product = $product->id;
                      $hosting->name_product = $product->name;
                  }
                  if ( !empty($hosting->price_override) ) {
                     $price_hosting = $hosting->price_override;
                     if (!empty($hosting->order_upgrade_hosting)) {
                       foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                          $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                       }
                     }
                  } else {
                     $price_hosting = !empty($hosting->detail_order->sub_total) ? $hosting->detail_order->sub_total : 0;
                     if (!empty($hosting->order_upgrade_hosting)) {
                       foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                          $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                       }
                     }
                  }
                  $hosting->price_hosting = $price_hosting;
                  $data['data'][] = $hosting;
              }
          }
          $data['total'] = $list_hosting->total();
          $data['perPage'] = $list_hosting->perPage();
          $data['current_page'] = $list_hosting->currentPage();
        }
        else {
            if ( empty($sort) ) {
                $list_hosting = $this->hosting->where('domain', 'like', '%'.$q.'%')
                ->with('user_hosting', 'product')
                ->orderBy('id', 'desc')->paginate(30);
            }
            else {
                $list_hosting = $this->hosting->where('domain', 'like', '%'.$q.'%')
                  ->with('user_hosting', 'product')
                  ->where('next_due_date' , '!=', null)
                  ->orderByRaw('next_due_date ' . $sort)
                  ->where(function($query)
                  {
                      return $query
                        ->orwhere('status_vps', 'on')
                        ->orWhere('status_vps', 'off')
                        ->orWhere('status_vps', 'admin_off')
                        ->orWhere('status_vps', 'progressing');
                  })->paginate(30);
            }
            if ($list_hosting->count() > 0) {
                foreach ($list_hosting as $key => $hosting) {
                    $product = $hosting->product;
                    if (!empty($product)) {
                        if ($product->group_product->private != 0) {
                            $hosting->private = true;
                        } else {
                            $hosting->private = false;
                        }
                        $hosting->id_product = $product->id;
                        $hosting->name_product = $product->name;
                    }
                    if ( !empty($hosting->price_override) ) {
                       $price_hosting = $hosting->price_override;
                       if (!empty($hosting->order_upgrade_hosting)) {
                         foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                            $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                         }
                       }
                    } else {
                       $price_hosting = !empty($hosting->detail_order->sub_total) ? $hosting->detail_order->sub_total : 0;
                       if (!empty($hosting->order_upgrade_hosting)) {
                         foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                            $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                         }
                       }
                    }
                    $hosting->price_hosting = $price_hosting;
                    $data['data'][] = $hosting;
                }
            }
            else {
              $list_hosting = $this->hosting->from('hosting as o')->select(['o.*'])->where('o.status_hosting', $status)->join('users as u','o.user_id','=','u.id')->where('u.name' , 'like', '%'.$q.'%')->with('user_hosting', 'product')->orderBy('id', 'desc')->paginate(30);
              foreach ($list_hosting as $key => $hosting) {
                  $product = $hosting->product;
                  if (!empty($product)) {
                      if ($product->group_product->private != 0) {
                          $hosting->private = true;
                      } else {
                          $hosting->private = false;
                      }
                      $hosting->id_product = $product->id;
                      $hosting->name_product = $product->name;
                  }
                  if ( !empty($hosting->price_override) ) {
                     $price_hosting = $hosting->price_override;
                     if (!empty($hosting->order_upgrade_hosting)) {
                       foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                          $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                       }
                     }
                  } else {
                     $price_hosting = !empty($hosting->detail_order->sub_total) ? $hosting->detail_order->sub_total : 0;
                     if (!empty($hosting->order_upgrade_hosting)) {
                       foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                          $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                       }
                     }
                  }
                  $hosting->price_hosting = $price_hosting;
                  $data['data'][] = $hosting;
              }
            }
            $data['total'] = $list_hosting->total();
            $data['perPage'] = $list_hosting->perPage();
            $data['current_page'] = $list_hosting->currentPage();
        }
        return $data;
    }

    public function select_status_hosting($q, $status)
    {
        $data = [];
        $data['data'] = [];
        if ( empty($q) ) {
          if ( empty($sort) ) {
              $list_hosting = $this->hosting->where('status_hosting', $status)
                ->with('user_hosting', 'product')
                ->orderBy('id', 'desc')->paginate(30);
          }
          else {
              $list_hosting = $this->hosting->where('status_hosting', $status)
                ->with('user_hosting', 'product')
                ->where('next_due_date' , '!=', null)
                ->orderByRaw('next_due_date ' . $sort)
                ->where(function($query)
                {
                    return $query
                      ->orwhere('status_vps', 'on')
                      ->orWhere('status_vps', 'off')
                      ->orWhere('status_vps', 'admin_off')
                      ->orWhere('status_vps', 'progressing');
                })->paginate(30);
          }
          if ($list_hosting->count() > 0) {
              foreach ($list_hosting as $key => $hosting) {
                  $product = $hosting->product;
                  if (!empty($product)) {
                      if ($product->group_product->private != 0) {
                          $hosting->private = true;
                      } else {
                          $hosting->private = false;
                      }
                      $hosting->id_product = $product->id;
                      $hosting->name_product = $product->name;
                  }
                  if ( !empty($hosting->price_override) ) {
                     $price_hosting = $hosting->price_override;
                     if (!empty($hosting->order_upgrade_hosting)) {
                       foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                          $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                       }
                     }
                  } else {
                     $price_hosting = !empty($hosting->detail_order->sub_total) ? $hosting->detail_order->sub_total : 0;
                     if (!empty($hosting->order_upgrade_hosting)) {
                       foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                          $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                       }
                     }
                  }
                  $hosting->price_hosting = $price_hosting;
                  $data['data'][] = $hosting;
              }
          }
          $data['total'] = $list_hosting->total();
          $data['perPage'] = $list_hosting->perPage();
          $data['current_page'] = $list_hosting->currentPage();
        }
        else {
          if ( empty($sort) ) {
              $list_hosting = $this->hosting->where('domain', 'like', '%'.$q.'%')
                ->where('status_hosting', $status)->with('user_hosting', 'product')
                ->orderBy('id', 'desc')->paginate(30);
          }
          else {
              $list_hosting = $this->hosting->where('domain', 'like', '%'.$q.'%')
                ->where('status_hosting', $status)->with('user_hosting', 'product')
                ->where('next_due_date' , '!=', null)
                ->orderByRaw('next_due_date ' . $sort)
                ->where(function($query)
                {
                    return $query
                      ->orwhere('status_vps', 'on')
                      ->orWhere('status_vps', 'off')
                      ->orWhere('status_vps', 'admin_off')
                      ->orWhere('status_vps', 'progressing');
                })->paginate(30);
          }
          // $list_hosting = $this->hosting->hosting->from('hosting as o')->select(['o.*'])->where('o.domain', 'like', '%'.$q.'%')->where('o.status_hosting', $status)->join('users as u','o.user_id','=','u.id')->orWhere('u.name' , 'like', '%'.$q.'%')->with('user_hosting', 'product')->orderBy('id', 'desc')->paginate(30);
          if ($list_hosting->count() > 0) {
              foreach ($list_hosting as $key => $hosting) {
                  $product = $hosting->product;
                  if (!empty($product)) {
                      if ($product->group_product->private != 0) {
                          $hosting->private = true;
                      } else {
                          $hosting->private = false;
                      }
                      $hosting->id_product = $product->id;
                      $hosting->name_product = $product->name;
                  }
                  if ( !empty($hosting->price_override) ) {
                     $price_hosting = $hosting->price_override;
                     if (!empty($hosting->order_upgrade_hosting)) {
                       foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                          $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                       }
                     }
                  } else {
                     $price_hosting = !empty($hosting->detail_order->sub_total) ? $hosting->detail_order->sub_total : 0;
                     if (!empty($hosting->order_upgrade_hosting)) {
                       foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                          $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                       }
                     }
                  }
                  $hosting->price_hosting = $price_hosting;
                  $data['data'][] = $hosting;
              }
              $data['total'] = $list_hosting->total();
              $data['perPage'] = $list_hosting->perPage();
              $data['current_page'] = $list_hosting->currentPage();
          }
          else {
              $list_hosting = $this->hosting->from('hosting as o')->select(['o.*'])->where('o.status_hosting', $status)->join('users as u','o.user_id','=','u.id')->where('u.name' , 'like', '%'.$q.'%')->with('user_hosting', 'product')->orderBy('id', 'desc')->paginate(30);
              foreach ($list_hosting as $key => $hosting) {
                  $product = $hosting->product;
                  if (!empty($product)) {
                      if ($product->group_product->private != 0) {
                          $hosting->private = true;
                      } else {
                          $hosting->private = false;
                      }
                      $hosting->id_product = $product->id;
                      $hosting->name_product = $product->name;
                  }
                  if ( !empty($hosting->price_override) ) {
                     $price_hosting = $hosting->price_override;
                     if (!empty($hosting->order_upgrade_hosting)) {
                       foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                          $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                       }
                     }
                  } else {
                     $price_hosting = !empty($hosting->detail_order->sub_total) ? $hosting->detail_order->sub_total : 0;
                     if (!empty($hosting->order_upgrade_hosting)) {
                       foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                          $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                       }
                     }
                  }
                  $hosting->price_hosting = $price_hosting;
                  $data['data'][] = $hosting;
              }
              $data['total'] = $list_hosting->total();
              $data['perPage'] = $list_hosting->perPage();
              $data['current_page'] = $list_hosting->currentPage();
          }
        }
        return $data;
    }

    public function search_user($q)
    {
        $list_users = $this->user->where('name', 'like', '%'.$q.'%')->with('hostings')->get();
        $list_hosting =  [];
        foreach ($list_users as $key => $user) {
          if (!empty($user->hostings)) {
            foreach ($user->hostings as $key => $hosting) {
              $product = $hosting->product;
              if (!empty($product)) {
                if ($product->group_product->private != 0) {
                  $hosting->private = true;
                } else {
                  $hosting->private = false;
                }
                $hosting->id_product = $product->id;
                $hosting->name_product = $product->name;
              }
              $list_hosting[$key] = $hosting;
              $list_hosting[$key]['user_hosting']['id'] = $user->id;
              if ( !empty($hosting->price_override) ) {
                 $price_hosting = $hosting->price_override;
                 if (!empty($hosting->order_upgrade_hosting)) {
                   foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                      $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                   }
                 }
              } else {
                 $price_hosting = !empty($hosting->detail_order->sub_total) ? $hosting->detail_order->sub_total : 0;
                 if (!empty($hosting->order_upgrade_hosting)) {
                   foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                      $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                   }
                 }
              }
              $hosting->price_hosting = $price_hosting;
            }
          }
        }
        return $list_hosting;
    }

    public function list_hosting_of_personal()
    {
        $hostings = $this->hosting->from('hostings as o')->select(['o.*', 'u.name'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', false)->with('user_hosting', 'product')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data =  [];
        $data['data'] =  [];
        if ( $hostings->count() > 0 ) {
          foreach ($hostings as $key => $hosting) {
            $product = $hosting->product;
            if (!empty($product)) {
              if ($product->group_product->private != 0) {
                $hosting->private = true;
              } else {
                $hosting->private = false;
              }
              $hosting->id_product = $product->id;
              $hosting->name_product = $product->name;
            }
            if ( !empty($hosting->price_override) ) {
               $price_hosting = $hosting->price_override;
               if (!empty($hosting->order_upgrade_hosting)) {
                 foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                    $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                 }
               }
            } else {
               $price_hosting = !empty($hosting->detail_order->sub_total) ? $hosting->detail_order->sub_total : 0;
               if (!empty($hosting->order_upgrade_hosting)) {
                 foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                    $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                 }
               }
            }
            $hosting->price_hosting = $price_hosting;
            $data['data'][] = $hosting;
          }
        }
        $data['total'] = $hostings->total();
        $data['perPage'] = $hostings->perPage();
        $data['current_page'] = $hostings->currentPage();
        return $data;
    }

    public function list_hosting_of_enterprise()
    {
        $hostings = $this->hosting->from('hostings as o')->select(['o.*', 'u.name'])
                    ->join('users as u','o.user_id','=','u.id')->where('u.enterprise', true)->with('user_hosting', 'product')
                    ->orderBy('o.id', 'desc')->paginate(30);
        $data =  [];
        $data['data'] =  [];
        if ( $hostings->count() > 0 ) {
          foreach ($hostings as $key => $hosting) {
            $product = $hosting->product;
            if (!empty($product)) {
              if ($product->group_product->private != 0) {
                $hosting->private = true;
              } else {
                $hosting->private = false;
              }
              $hosting->id_product = $product->id;
              $hosting->name_product = $product->name;
            }
            if ( !empty($hosting->price_override) ) {
               $price_hosting = $hosting->price_override;
               if (!empty($hosting->order_upgrade_hosting)) {
                 foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                    $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                 }
               }
            } else {
               $price_hosting = !empty($hosting->detail_order->sub_total) ? $hosting->detail_order->sub_total : 0;
               if (!empty($hosting->order_upgrade_hosting)) {
                 foreach ($hosting->order_upgrade_hosting as $key => $order_upgrade_hosting) {
                    $price_hosting += !empty($order_upgrade_hosting->detail_order->sub_total) ? $order_upgrade_hosting->detail_order->sub_total : 0;
                 }
               }
            }
            $hosting->price_hosting = $price_hosting;
            $data['data'][] = $hosting;
          }
        }
        $data['total'] = $hostings->total();
        $data['perPage'] = $hostings->perPage();
        $data['current_page'] = $hostings->currentPage();
        return $data;
    }

}
