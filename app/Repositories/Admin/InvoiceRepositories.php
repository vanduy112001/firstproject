<?php

namespace App\Repositories\Admin;

use App\Model\User;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\Server;
use App\Model\Hosting;
use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\Colocation;
use App\Model\LogActivity;
use Illuminate\Support\Facades\Auth;


class InvoiceRepositories {

    protected $user;
    protected $order;
    protected $detail_order;
    protected $vps;
    protected $server;
    protected $hosting;
    protected $group_product;
    protected $product;
    protected $colocation;
    protected $log_activity;

    public function __construct()
    {
        $this->user = new User;
        $this->order = new Order;
        $this->detail_order = new DetailOrder;
        $this->vps = new Vps;
        $this->server = new Server;
        $this->hosting = new Hosting;
        $this->group_product = new GroupProduct;
        $this->product = new Product;
        $this->colocation = new Colocation;
        $this->log_activity = new LogActivity();
    }

    public function get_detail_orders()
    {
        return $this->detail_order->orderBy('id', 'desc')->with('vps', 'servers', 'hostings', 'user')->paginate(20);
    }

    public function get_detail_order($id)
    {
        return $this->detail_order->find($id);
    }

    public function delete_vps($id)
    {
        $vps = $this->vps->find($id);
        $detail_order = $this->detail_order->find($vps->detail_order_id);
        $order = $this->order->find($detail_order->order_id);
        $order->total = $order->total - ($order->total / $detail_order->quantity);
        $order->save();

        $detail_order->sub_total = $detail_order->sub_total - ($detail_order->sub_total/$detail_order->quantity);
        $detail_order->quantity = $detail_order->quantity - 1;
        $detail_order->save();

        $delete = $vps->delete();

        return $delete;
    }

    public function delete_server($id)
    {
        $server = $this->server->find($id);
        $detail_order = $this->detail_order->find($server->detail_order_id);
        $order = $this->order->find($detail_order->order_id);
        $order->total = $order->total - ($order->total / $detail_order->quantity);
        $order->save();

        $detail_order->sub_total = $detail_order->sub_total - ($detail_order->sub_total/$detail_order->quantity);
        $detail_order->quantity = $detail_order->quantity - 1;
        $detail_order->save();

        $delete = $server->delete();
        return $delete;
    }

    public function delete_hosting($id)
    {
        $hosting = $this->hosting->find($id);
        $detail_order = $this->detail_order->find($hosting->detail_order_id);
        $order = $this->order->find($detail_order->order_id);
        $order->total = $order->total - ($order->total / $detail_order->quantity);
        $order->save();

        $detail_order->sub_total = $detail_order->sub_total - ($detail_order->sub_total/$detail_order->quantity);
        $detail_order->quantity = $detail_order->quantity - 1;
        $detail_order->save();

        $delete = $hosting->delete();
        return $delete;
    }

    public function detail_invoice_with_services($detail_order_id)
    {
        return $this->detail_order->find($detail_order_id);
    }

    public function mark_paid($id)
    {
        $detail_order = $this->detail_order->find($id);
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        if ($detail_order->type == 'VPS') {
            $vps = $this->vps->where('detail_order_id', $id)->get();
            foreach ($vps as $detail_vps) {
                $detail_vps->next_due_date = date('Y-m-d', strtotime( $detail_vps->created_at . '+'. $billing[$detail_vps->billing_cycle] ) );
                $detail_vps->save();

                $detail_order->paid_date = date('Y-m-d');
                $detail_order->due_date = date('Y-m-d', strtotime( $detail_order->created_at . '+'. $billing[$detail_vps->billing_cycle] ) );
                $detail_order->status = 'paid';

            }
        } elseif ($detail_order->type == 'Server') {
            $servers = $this->server->where('detail_order_id', $id)->get();
            foreach ($servers as $server) {
                $server->next_due_date = date('Y-m-d', strtotime( $server->created_at . '+'. $billing[$server->billing_cycle] ));
                $server->save();

                $detail_order->paid_date = date('Y-m-d');
                $detail_order->due_date = date('Y-m-d', strtotime( $detail_order->created_at . '+'. $billing[$server->billing_cycle] ));
                $detail_order->status = 'paid';

            }
        } else {
            $hostings = $this->hosting->where('detail_order_id', $id)->get();
            foreach ($hostings as $hosting) {
                $hosting->next_due_date = date('m-d-Y', strtotime( $hosting->created_at . '+'. $billing[$hosting->billing_cycle] ));
                $hosting->save();

                $detail_order->paid_date = date('Y-m-d');
                $detail_order->due_date = date('m-d-Y', strtotime( $detail_order->created_at . '+'. $billing[$hosting->billing_cycle] ));
                $detail_order->status = 'paid';

            }
        }

        $save = $detail_order->save();
        return $save;

    }


    public function unpaid($id)
    {
        $detail_order = $this->detail_order->find($id);

        if ($detail_order->type == 'VPS') {
            $vps = $this->vps->where('detail_order_id', $id)->get();
            foreach ($vps as $detail_vps) {
                $detail_vps->next_due_date = null;
                $detail_vps->save();

                $detail_order->paid_date = null;
                $detail_order->due_date = date('Y-m-d', strtotime( $server->created_at . '+3 Day'));
                $detail_order->status = 'unpaid';

            }
        } elseif ($detail_order->type == 'Server') {
            $servers = $this->server->where('detail_order_id', $id)->get();
            foreach ($servers as $server) {
                $server->next_due_date = null;
                $server->save();

                $detail_order->paid_date = null;
                $detail_order->due_date = date('Y-m-d', strtotime( $server->created_at . '+3 Day'));
                $detail_order->status = 'unpaid';

            }
        } else {
            $hostings = $this->hosting->where('detail_order_id', $id)->get();
            foreach ($hostings as $hosting) {
                $hosting->next_due_date = null;
                $hosting->save();

                $detail_order->paid_date = null;
                $detail_order->due_date = date('Y-m-d', strtotime( $server->created_at . '+3 Day'));
                $detail_order->status = 'unpaid';

            }
        }
    }

    public function cancel($id)
    {
        $detail_order = $this->detail_order->find($id);
        $detail_order->status = 'cancel';

        $save = $detail_order->save();
        return $save;

    }



    public function invoices_mark_paid($data)
    {
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        foreach ($data as $key => $id) {
            $detail_order = $this->detail_order->find($id);
            if($detail_order->status != 'paid') {
                if ($detail_order->type == 'VPS') {
                    $vps = $this->vps->where('detail_order_id', $detail_order->id)->get();
                    foreach ($vps as $detail_vps) {
                        $detail_vps->next_due_date = date('Y-m-d', strtotime( $detail_vps->created_at . '+'. $billing[$detail_vps->billing_cycle] ) );
                        $detail_vps->paid = 'paid';
                        $detail_vps->save();

                        $detail_order->paid_date = date('Y-m-d');
                        $detail_order->due_date = date('Y-m-d', strtotime( $detail_order->created_at . '+'. $billing[$detail_vps->billing_cycle] ) );
                        $detail_order->status = 'paid';
                        $save = $detail_order->save();
                        if (!$save) {
                            return false;
                        }
                    }
                } elseif($detail_order->type == 'Server') {
                    $servers = $this->server->where('detail_order_id', $detail_order->id)->get();
                    foreach ($servers as $server) {
                        $server->next_due_date = date('Y-m-d', strtotime( $server->created_at . '+'. $billing[$server->billing_cycle] ) );
                        $server->paid ='paid';
                        $server->save();

                        $detail_order->paid_date = date('Y-m-d');
                        $detail_order->due_date = date('Y-m-d', strtotime( $detail_order->created_at . '+'. $billing[$server->billing_cycle] ) );
                        $detail_order->status = 'paid';
                        $save = $detail_order->save();
                        if (!$save) {
                            return false;
                        }
                    }
                }  else {
                    $hostings = $this->hosting->where('detail_order_id', $detail_order->id)->get();
                    foreach ($hostings as $hosting) {
                        $hosting->next_due_date = date('Y-m-d', strtotime( $hosting->created_at . '+'. $billing[$hosting->billing_cycle] ) );
                        $hosting->paid = 'paid';
                        $hosting->save();

                        $detail_order->paid_date = date('Y-m-d');
                        $detail_order->due_date = date('Y-m-d', strtotime( $detail_order->created_at . '+'. $billing[$hosting->billing_cycle] ) );
                        $detail_order->status = 'paid';
                        $save = $detail_order->save();
                        if (!$save) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    public function invoices_unpaid($data)
    {
        foreach ($data as $key => $id) {
            $detail_order = $this->detail_order->find($id);
            $detail_order->status = 'unpaid';
            $save = $detail_order->save();
            if (!$save) {
                return false;
            }
        }
        return true;
    }

    public function invoices_cancel($data)
    {
        foreach ($data as $key => $id) {
            $detail_order = $this->detail_order->find($id);
            $detail_order->status = 'cancel';
            $save = $detail_order->save();
            if (!$save) {
                return false;
            }
        }
        return true;
    }

    public function invoices_delete($data)
    {
        foreach ($data as $key => $id) {
            $detail_order = $this->detail_order->find($id);
            if ($detail_order->type == 'VPS') {
                $vps = $this->vps->where('detail_order_id', $detail_order->id)->delete();
            } elseif($detail_order->type == 'Server') {
                $servers = $this->server->where('detail_order_id', $detail_order->id)->delete();
            }  else {
                $hostings = $this->server->where('detail_order_id', $detail_order->id)->delete();
            }
            $delete = $detail_order->delete();
            // ghi log
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'xóa',
              'model' => 'Admin/DetailOrder',
              'description' => ' detail order #' . $id,
            ];
            $this->log_activity->create($data_log);
            if (!$delete) {
                return false;
            }
        }
        return true;
    }

    public function delete($id)
    {
        $this->vps->where('detail_order_id', $id)->delete();
        $this->server->where('detail_order_id', $id)->delete();
        $this->hosting->where('detail_order_id', $id)->delete();
        $delete = $this->detail_order->where('id', $id)->delete();
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'xóa',
          'model' => 'Admin/DetailOrder',
          'description' => ' detail order #' . $id,
        ];
        $this->log_activity->create($data_log);
        return $delete;
    }

    public function filter_type($type,$user_id, $status)
    {
       $data = [];
       if ($type == 'VPS') {
          $detail_orders = $this->detail_order->where('type', 'VPS')->with('order', 'user')->orderBy('id', 'desc')->get();
          if (!empty($user_id)) {
            $detail_orders = $detail_orders->where('user_id', $user_id);
          }
          if ($status != 'all') {
            if ($status == 'paid') {
              $detail_orders = $detail_orders->where('status', 'paid');
            } elseif ($status == 'unpaid') {
              $detail_orders = $detail_orders->where('status', 'unpaid');
            } else {
              $detail_orders = $detail_orders->where('status', 'cancel');
            }
          }
          foreach ($detail_orders as $key => $detail_order) {
             if ($detail_order->order->type == 'order') {
                  $detail_order->username = $detail_order->user->name;
                  $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                  $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                  if (!empty($detail_order->paid_date)) {
                    $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                  } else {
                    $detail_order->paid_date = '';
                  }
                  $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                  $data[] = $detail_order;
             }
          }
       }
       elseif ($type == 'NAT-VPS') {
           $detail_orders = $this->detail_order->where('type', 'NAT-VPS')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'order') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'Server') {
           $detail_orders = $this->detail_order->where('type', 'Server')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'order') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'Colocation') {
           $detail_orders = $this->detail_order->where('type', 'Colocation')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'order') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'Hosting') {
           $detail_orders = $this->detail_order->where('type', 'Hosting')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'order') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'Email Hosting') {
           $detail_orders = $this->detail_order->where('type', 'Hosting')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'order') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'Hosting-Singapore') {
           $detail_orders = $this->detail_order->where('type', 'Hosting-Singapore')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'order') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'Domain') {
           $detail_orders = $this->detail_order->where('type', 'Domain')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'order') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'addon_vps') {
           $detail_orders = $this->detail_order->where('type', 'addon_vps')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'order') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'addon_server') {
           $detail_orders = $this->detail_order->where('type', 'addon_server')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'order') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'upgrade_hosting') {
           $detail_orders = $this->detail_order->where('type', 'upgrade_hosting')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'order') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'change_ip') {
           $detail_orders = $this->detail_order->where('type', 'change_ip')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'order') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'expired_vps') {
           $detail_orders = $this->detail_order->where('type', 'VPS')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'expired') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'expired_server') {
           $detail_orders = $this->detail_order->where('type', 'Server')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'expired') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'expired_colocation') {
           $detail_orders = $this->detail_order->where('type', 'Colocation')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'expired') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'expired_nat_vps') {
           $detail_orders = $this->detail_order->where('type', 'NAT-VPS')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'expired') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'expired_hosting') {
           $detail_orders = $this->detail_order->where('type', 'Hosting')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'expired') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'expired_email_hosting') {
           $detail_orders = $this->detail_order->where('type', 'Email Hosting')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'expired') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       elseif ($type == 'Domain_exp') {
           $detail_orders = $this->detail_order->where('type', 'Domain_exp')->with('order', 'user')->orderBy('id', 'desc')->get();
           if (!empty($user_id)) {
             $detail_orders = $detail_orders->where('user_id', $user_id);
           }
           if ($status != 'all') {
             if ($status == 'paid') {
               $detail_orders = $detail_orders->where('status', 'paid');
             } elseif ($status == 'unpaid') {
               $detail_orders = $detail_orders->where('status', 'unpaid');
             } else {
               $detail_orders = $detail_orders->where('status', 'cancel');
             }
           }
           foreach ($detail_orders as $key => $detail_order) {
              if ($detail_order->order->type == 'expired') {
                   $detail_order->username = $detail_order->user->name;
                   $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                   $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                   if (!empty($detail_order->paid_date)) {
                     $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                   } else {
                     $detail_order->paid_date = '';
                   }
                   $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                   $data[] = $detail_order;
              }
           }
       }
       return $data;
    }

    public function filter_user($type,$user_id, $status)
    {
        $detail_orders = $this->detail_order->where('user_id', $user_id)->with('order', 'user')->orderBy('id', 'desc')->get();
        $data = [];
        if ($detail_orders->count() > 0) {
          if ($status != 'all') {
            if ($status == 'paid') {
              $detail_orders = $detail_orders->where('status', 'paid');
            } elseif ($status == 'unpaid') {
              $detail_orders = $detail_orders->where('status', 'unpaid');
            } else {
              $detail_orders = $detail_orders->where('status', 'cancel');
            }
          }
          if (!empty($type)) {
              if ($type == 'VPS') {
                 $detail_orders = $detail_orders->where('type', 'VPS');
                 foreach ($detail_orders as $key => $detail_order) {
                    if ($detail_order->order->type == 'order') {
                         $detail_order->username = $detail_order->user->name;
                         $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                         $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                         if (!empty($detail_order->paid_date)) {
                           $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                         } else {
                           $detail_order->paid_date = '';
                         }
                         $detail_order->type = 'Tạo ' . $detail_order->type;
                         $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                         $data[] = $detail_order;
                    }
                 }
              }
              elseif ($type == 'NAT-VPS') {
                 $detail_orders = $detail_orders->where('type', 'NAT-VPS');
                 foreach ($detail_orders as $key => $detail_order) {
                    if ($detail_order->order->type == 'order') {
                         $detail_order->username = $detail_order->user->name;
                         $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                         $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                         if (!empty($detail_order->paid_date)) {
                           $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                         } else {
                           $detail_order->paid_date = '';
                         }
                         $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                         $detail_order->type = 'Tạo ' . $detail_order->type;
                         $data[] = $detail_order;
                    }
                 }
              }
              elseif ($type == 'Server') {
                 $detail_orders = $detail_orders->where('type', 'Server');
                 foreach ($detail_orders as $key => $detail_order) {
                    if ($detail_order->order->type == 'order') {
                         $detail_order->username = $detail_order->user->name;
                         $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                         $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                         if (!empty($detail_order->paid_date)) {
                           $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                         } else {
                           $detail_order->paid_date = '';
                         }
                         $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                         $detail_order->type = 'Tạo ' . $detail_order->type;
                         $data[] = $detail_order;
                    }
                 }
              }
              elseif ($type == 'Colocation') {
                 $detail_orders = $detail_orders->where('type', 'Colocation');
                 foreach ($detail_orders as $key => $detail_order) {
                    if ($detail_order->order->type == 'order') {
                         $detail_order->username = $detail_order->user->name;
                         $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                         $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                         if (!empty($detail_order->paid_date)) {
                           $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                         } else {
                           $detail_order->paid_date = '';
                         }
                         $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                         $detail_order->type = 'Tạo ' . $detail_order->type;
                         $data[] = $detail_order;
                    }
                 }
              }
              elseif ($type == 'Hosting') {
                $detail_orders = $detail_orders->where('type', 'Hosting');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'order') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Tạo ' . $detail_order->type;
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'Email Hosting') {
                $detail_orders = $detail_orders->where('type', 'Email Hosting');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'order') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Tạo ' . $detail_order->type;
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'Hosting-Singapore') {
                $detail_orders = $detail_orders->where('type', 'Hosting-Singapore');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'order') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Tạo ' . $detail_order->type;
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'Domain') {
                $detail_orders = $detail_orders->where('type', 'Domain');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'order') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Tạo ' . $detail_order->type;
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'addon_vps') {
                $detail_orders = $detail_orders->where('type', 'addon_vps');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'order') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Nâng cấp VPS';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'addon_server') {
                $detail_orders = $detail_orders->where('type', 'addon_server');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'order') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Nâng cấp Server';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'upgrade_hosting') {
                $detail_orders = $detail_orders->where('type', 'upgrade_hosting');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'order') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Nâng cấp Hosting';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'change_ip') {
                $detail_orders = $detail_orders->where('type', 'change_ip');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'order') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Đổi IP VPS';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'expired_vps') {
                $detail_orders = $detail_orders->where('type', 'VPS');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'expired') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Gia hạn VPS';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'expired_server') {
                $detail_orders = $detail_orders->where('type', 'Server');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'expired') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Gia hạn Server';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'expired_colocation') {
                $detail_orders = $detail_orders->where('type', 'Colocation');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'expired') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Gia hạn Colocation';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'expired_email_hosting') {
                $detail_orders = $detail_orders->where('type', 'Email Hosting');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'expired') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Gia hạn Email Hosting';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'expired_nat_vps') {
                $detail_orders = $detail_orders->where('type', 'NAT-VPS');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'expired') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Gia hạn NAT VPS';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'expired_hosting') {
                $detail_orders = $detail_orders->where('type', 'hosting');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'expired') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Gia hạn Hosting';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'Domain_exp') {
                $detail_orders = $detail_orders->where('type', 'Domain_exp');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'expired') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Gia hạn VPS';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
          }
          else {
              foreach ($detail_orders as $key => $detail_order) {
                  $detail_order->username = $detail_order->user->name;
                  $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                  $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                  if (!empty($detail_order->paid_date)) {
                    $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                  } else {
                    $detail_order->paid_date = '';
                  }
                  if ($detail_order->type == 'VPS' || $detail_order->type == 'NAT-VPS' || $detail_order->type == 'Hosting' || $detail_order->type == 'Hosting-Singapore' || $detail_order->type == 'Domain' || $detail_order->type == 'Server' || $detail_order->type == 'Email Hosting' || $detail_order->type == 'Colocation' ) {
                    if ($detail_order->order->type == 'order') {
                       $detail_order->type = 'Tạo ' . $detail_order->type;
                    } else {
                        $detail_order->type = 'Gia Hạn ' . $detail_order->type;
                    }
                  }
                  $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                  $data[] = $detail_order;
              }
          }
        }
        else {
          return $data;
        }
        return $data;
    }

    public function filter_status($type,$user_id, $status)
    {
        $detail_orders = $this->detail_order->where('status', $status)->with('order', 'user')->orderBy('id', 'desc')->get();
        $data = [];
        if ($detail_orders->count() > 0) {
          if (!empty($user_id)) {
            $detail_orders = $detail_orders->where('user_id', $user_id);
          }
          if (!empty($type)) {
              if ($type == 'VPS') {
                 $detail_orders = $detail_orders->where('type', 'VPS');
                 foreach ($detail_orders as $key => $detail_order) {
                    if ($detail_order->order->type == 'order') {
                         $detail_order->username = $detail_order->user->name;
                         $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                         $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                         if (!empty($detail_order->paid_date)) {
                           $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                         } else {
                           $detail_order->paid_date = '';
                         }
                         $detail_order->type = 'Tạo ' . $detail_order->type;
                         $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                         $data[] = $detail_order;
                    }
                 }
              }
              elseif ($type == 'NAT-VPS') {
                 $detail_orders = $detail_orders->where('type', 'NAT-VPS');
                 foreach ($detail_orders as $key => $detail_order) {
                    if ($detail_order->order->type == 'order') {
                         $detail_order->username = $detail_order->user->name;
                         $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                         $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                         if (!empty($detail_order->paid_date)) {
                           $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                         } else {
                           $detail_order->paid_date = '';
                         }
                         $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                         $detail_order->type = 'Tạo ' . $detail_order->type;
                         $data[] = $detail_order;
                    }
                 }
              }
              elseif ($type == 'Server') {
                 $detail_orders = $detail_orders->where('type', 'Server');
                 foreach ($detail_orders as $key => $detail_order) {
                    if ($detail_order->order->type == 'order') {
                         $detail_order->username = $detail_order->user->name;
                         $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                         $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                         if (!empty($detail_order->paid_date)) {
                           $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                         } else {
                           $detail_order->paid_date = '';
                         }
                         $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                         $detail_order->type = 'Tạo ' . $detail_order->type;
                         $data[] = $detail_order;
                    }
                 }
              }
              elseif ($type == 'Colocation') {
                 $detail_orders = $detail_orders->where('type', 'Colocation');
                 foreach ($detail_orders as $key => $detail_order) {
                    if ($detail_order->order->type == 'order') {
                         $detail_order->username = $detail_order->user->name;
                         $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                         $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                         if (!empty($detail_order->paid_date)) {
                           $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                         } else {
                           $detail_order->paid_date = '';
                         }
                         $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                         $detail_order->type = 'Tạo ' . $detail_order->type;
                         $data[] = $detail_order;
                    }
                 }
              }
              elseif ($type == 'Hosting') {
                $detail_orders = $detail_orders->where('type', 'Hosting');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'order') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Tạo ' . $detail_order->type;
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'Email Hosting') {
                $detail_orders = $detail_orders->where('type', 'Email Hosting');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'order') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Tạo ' . $detail_order->type;
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'Hosting-Singapore') {
                $detail_orders = $detail_orders->where('type', 'Hosting-Singapore');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'order') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Tạo ' . $detail_order->type;
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'Domain') {
                $detail_orders = $detail_orders->where('type', 'Domain');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'order') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Tạo ' . $detail_order->type;
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'addon_vps') {
                $detail_orders = $detail_orders->where('type', 'addon_vps');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'order') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Nâng cấp VPS';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'addon_server') {
                $detail_orders = $detail_orders->where('type', 'addon_server');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'order') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Nâng cấp Server';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'upgrade_hosting') {
                $detail_orders = $detail_orders->where('type', 'upgrade_hosting');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'order') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Nâng cấp Hosting';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'change_ip') {
                $detail_orders = $detail_orders->where('type', 'change_ip');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'order') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Đổi IP VPS';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'expired_vps') {
                $detail_orders = $detail_orders->where('type', 'VPS');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'expired') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Gia hạn VPS';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'expired_server') {
                $detail_orders = $detail_orders->where('type', 'Server');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'expired') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Gia hạn Server';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'expired_colocation') {
                $detail_orders = $detail_orders->where('type', 'Colocation');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'expired') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Gia hạn Colocation';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'expired_email_hosting') {
                $detail_orders = $detail_orders->where('type', 'Email Hosting');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'expired') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Gia hạn Email Hosting';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'expired_nat_vps') {
                $detail_orders = $detail_orders->where('type', 'NAT-VPS');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'expired') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Gia hạn NAT VPS';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'expired_hosting') {
                $detail_orders = $detail_orders->where('type', 'hosting');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'expired') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Gia hạn Hosting';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
              elseif ($type == 'Domain_exp') {
                $detail_orders = $detail_orders->where('type', 'Domain_exp');
                foreach ($detail_orders as $key => $detail_order) {
                   if ($detail_order->order->type == 'expired') {
                        $detail_order->username = $detail_order->user->name;
                        $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                        $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                        if (!empty($detail_order->paid_date)) {
                          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                        } else {
                          $detail_order->paid_date = '';
                        }
                        $detail_order->type = 'Gia hạn VPS';
                        $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                        $data[] = $detail_order;
                   }
                }
              }
          }
          else {
              foreach ($detail_orders as $key => $detail_order) {
                  $detail_order->username = $detail_order->user->name;
                  $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
                  $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
                  if (!empty($detail_order->paid_date)) {
                    $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
                  } else {
                    $detail_order->paid_date = '';
                  }
                  if ($detail_order->type == 'VPS' || $detail_order->type == 'NAT-VPS' || $detail_order->type == 'Hosting' || $detail_order->type == 'Hosting-Singapore' || $detail_order->type == 'Domain' || $detail_order->type == 'Server' || $detail_order->type == 'Email Hosting' || $detail_order->type == 'Colocation' ) {
                    if ($detail_order->order->type == 'order') {
                       $detail_order->type = 'Tạo ' . $detail_order->type;
                    } else {
                        $detail_order->type = 'Gia Hạn ' . $detail_order->type;
                    }
                  }
                  $detail_order->sub_total = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
                  $data[] = $detail_order;
              }
          }
        }
        return $data;
    }

    public function detail_invoice($id)
    {
       $billings = config('billing');
       $detail_order = $this->detail_order->find($id);
       if ($detail_order->status == 'paid') {
          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
       }
       else {
          $detail_order->paid_date = '';
       }
       $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
       $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
       $detail_order->username = $detail_order->user->name;
       if ($detail_order->order->type == 'expired') {
           $detail_order->type_order = 'expired';
           if ($detail_order->type == 'VPS') {
              $order_expireds = $detail_order->order_expireds;
              $services = [];
              foreach ($order_expireds as $key => $order_expired) {
                $services[$key]['sevice_id'] = $order_expired->vps->id;
                $services[$key]['sevice_name'] = $order_expired->vps->ip;
                $product = $order_expired->vps->product;
                $vps = $order_expired->vps;
                if(!empty($product->meta_product)) {
                  $cpu = $product->meta_product->cpu;
                  $ram = $product->meta_product->memory;
                  $disk = $product->meta_product->disk;
                } else {
                  $cpu = 1;
                  $ram = 1;
                  $disk = 20;
                }
                // Addon
                if (!empty($vps->vps_config)) {
                    $vps_config = $vps->vps_config;
                    if (!empty($vps_config)) {
                        $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                        $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                        $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                    }
                }
                $services[$key]['config'] = $cpu . ' CPU - ' . $ram . ' GB RAM - ' . $disk . ' GB DISK';
                $services[$key]['billing_cycle'] = !empty($order_expired->billing_cycle) ? $billings[$order_expired->billing_cycle] : '1 Tháng';
                $services[$key]['amount'] = !empty($vps->price_override) ? number_format($vps->price_override,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
              }
              $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'VPS US' || $detail_order->type == ' US' ) {
              $order_expireds = $detail_order->order_expireds;
              $services = [];
              foreach ($order_expireds as $key => $order_expired) {
                $services[$key]['sevice_id'] = $order_expired->vps->id;
                $services[$key]['sevice_name'] = $order_expired->vps->ip;
                $product = $order_expired->vps->product;
                $vps = $order_expired->vps;
                if(!empty($product->meta_product)) {
                  $cpu = $product->meta_product->cpu;
                  $ram = $product->meta_product->memory;
                  $disk = $product->meta_product->disk;
                } else {
                  $cpu = 1;
                  $ram = 1;
                  $disk = 20;
                }
                // Addon
                if (!empty($vps->vps_config)) {
                    $vps_config = $vps->vps_config;
                    if (!empty($vps_config)) {
                        $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                        $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                        $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                    }
                }
                $services[$key]['config'] = $cpu . ' CPU - ' . $ram . ' GB RAM - ' . $disk . ' GB DISK';
                $services[$key]['billing_cycle'] = !empty($order_expired->billing_cycle) ? $billings[$order_expired->billing_cycle] : '1 Tháng';
                $services[$key]['amount'] = !empty($vps->price_override) ? number_format($vps->price_override,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
              }
              $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'Server') {
              $order_expireds = $detail_order->order_expireds;
              $services = [];
              foreach ($order_expireds as $key => $order_expired) {
                $services[$key]['sevice_id'] = $order_expired->server->id;
                $services[$key]['sevice_name'] = $order_expired->server->ip;
                $server = $order_expired->server;
                $chip = $server->chip;
                $ram = $server->ram;
                $disk = $server->disk;
                $ip = 1;
                // Addon
                if (!empty($server->server_config)) {
                    $server_config = $server->server_config;
                    if (!empty($server_config)) {
                        $ip += (int) !empty($server_config->ip) ? $server_config->ip : 0;
                        $ram += (int) !empty($server_config->ram) ? $server_config->ram : 0;
                        $disk += (int) !empty($server_config->disk) ? $server_config->disk : 0;
                    }
                }
                $services[$key]['config'] = $chip . ' - ' . $ip . ' IP - ' . $ram . ' GB RAM - ' . $disk . ' GB DISK';
                $services[$key]['billing_cycle'] = !empty($order_expired->billing_cycle) ? $billings[$order_expired->billing_cycle] : '1 Tháng';
                $services[$key]['amount'] = !empty($server->amount) ? number_format($server->amount,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
              }
              $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'Colocation') {
              $order_expireds = $detail_order->order_expireds;
              $services = [];
              foreach ($order_expireds as $key => $order_expired) {
                $services[$key]['sevice_id'] = $order_expired->colocation->id;
                $services[$key]['sevice_name'] = $order_expired->colocation->ip;
                $colocation = $order_expired->colocation;
                $services[$key]['config'] = $colocation->type_colo;
                $services[$key]['billing_cycle'] = !empty($order_expired->billing_cycle) ? $billings[$order_expired->billing_cycle] : '1 Tháng';
                $services[$key]['amount'] = !empty($colocation->amount) ? number_format($colocation->amount,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
              }
              $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'hosting') {
               $order_expireds = $detail_order->order_expireds;
               $services = [];
               foreach ($order_expireds as $key => $order_expired) {
                 $services[$key]['sevice_id'] = $order_expired->hosting->id;
                 $services[$key]['sevice_name'] = $order_expired->hosting->domain;
                 $product = $order_expired->hosting->product;
                 $hosting = $order_expired->hosting;
                 $services[$key]['config'] = $product->name;
                 $services[$key]['billing_cycle'] = !empty($order_expired->billing_cycle) ? $billings[$order_expired->billing_cycle] : '1 Tháng';
                 $services[$key]['amount'] = !empty($hosting->price_override) ? number_format($hosting->price_override,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
               }
               $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'Email Hosting') {
               $order_expireds = $detail_order->order_expireds;
               $services = [];
               foreach ($order_expireds as $key => $order_expired) {
                 $services[$key]['sevice_id'] = $order_expired->email_hosting->id;
                 $services[$key]['sevice_name'] = $order_expired->email_hosting->domain;
                 $product = $order_expired->email_hosting->product;
                 $hosting = $order_expired->email_hosting;
                 $services[$key]['config'] = $product->name;
                 $services[$key]['billing_cycle'] = !empty($order_expired->billing_cycle) ? $billings[$order_expired->billing_cycle] : '1 Tháng';
                 $services[$key]['amount'] = !empty($hosting->price_override) ? number_format($hosting->price_override,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
               }
               $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'Domain_exp') {
               $billing_domain_exp = [
                   'monthly_exp' => '1 Tháng',
                   'twomonthly_exp' => '2 Tháng',
                   'quarterly_exp' => '3 Tháng',
                   'semi_annually_exp' => '6 Tháng',
                   'annually_exp' => '1 Năm',
                   'biennially_exp' => '2 Năm',
                   'triennially_exp' => '3 Năm',
               ];
               $detail_order->type = 'Domain';
               $order_expireds = $detail_order->domain_expireds;
               $services = [];
               foreach ($order_expireds as $key => $order_expired) {
                 $services[$key]['sevice_id'] = $order_expired->domain->id;
                 $services[$key]['sevice_name'] = $order_expired->domain->domain;
                 $product = $order_expired->domain->domain_product;
                 $domain = $order_expired->domain;
                 $services[$key]['config'] = 'Domain .' . $product->type;
                 $services[$key]['billing_cycle'] = !empty($order_expired->billing_cycle) ? $billing_domain_exp[$order_expired->billing_cycle] : '1 Tháng';
                 $services[$key]['amount'] = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
               }
               $detail_order->services = $services;
           }
       }
       else {
         $detail_order->type_order = 'order';
           if ($detail_order->type == 'VPS' || $detail_order->type == 'NAT-VPS' || $detail_order->type == 'VPS-US') {
              $list_vps = $detail_order->vps;
              $services = [];
              foreach ($list_vps as $key => $vps) {
                $services[$key]['sevice_id'] = $vps->id;
                $services[$key]['sevice_name'] = $vps->ip;
                $product = $vps->product;
                if(!empty($product->meta_product)) {
                  $cpu = $product->meta_product->cpu;
                  $ram = $product->meta_product->memory;
                  $disk = $product->meta_product->disk;
                } else {
                  $cpu = 1;
                  $ram = 1;
                  $disk = 20;
                }
                // Addon
                if (!empty($vps->vps_config)) {
                    $vps_config = $vps->vps_config;
                    if (!empty($vps_config)) {
                        $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                        $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                        $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                    }
                }
                $services[$key]['config'] = $cpu . ' CPU - ' . $ram . ' GB RAM - ' . $disk . ' GB DISK';
                $services[$key]['billing_cycle'] = !empty($vps->billing_cycle) ? $billings[$vps->billing_cycle] : '1 Tháng';
                $services[$key]['amount'] = !empty($vps->price_override) ? number_format($vps->price_override,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
              }
              $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'Server') {
              $list_servers = $detail_order->servers;
              $services = [];
              foreach ($list_servers as $key => $server) {
                $services[$key]['sevice_id'] = $server->id;
                $services[$key]['sevice_name'] = $server->ip;
                $chip = $server->chip;
                $ram = $server->ram;
                $disk = $server->disk;
                // Addon
                if (!empty($server->server_config)) {
                    $server_config = $server->vps_config;
                    if (!empty($server_config)) {
                        $ram += (int) !empty($server_config->ram) ? $server_config->ram : 0;
                        $disk += (int) !empty($server_config->disk) ? $server_config->disk : 0;
                    }
                }
                $services[$key]['config'] = $chip . ' - ' . $ram . ' GB RAM - ' . $disk . ' GB DISK';
                $services[$key]['billing_cycle'] = !empty($server->billing_cycle) ? $billings[$server->billing_cycle] : '1 Tháng';
                $services[$key]['amount'] = !empty($server->amount) ? number_format($server->amount,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
              }
              $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'Colocation') {
              $list_colocations = $detail_order->colocations;
              $services = [];
              foreach ($list_colocations as $key => $colocation) {
                $services[$key]['sevice_id'] = $colocation->id;
                $services[$key]['sevice_name'] = $colocation->ip;
                $services[$key]['config'] = $colocation->type_colo;
                $services[$key]['billing_cycle'] = !empty($colocation->billing_cycle) ? $billings[$colocation->billing_cycle] : '1 Tháng';
                $services[$key]['amount'] = !empty($colocation->amount) ? number_format($colocation->amount,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
              }
              $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'Hosting' || $detail_order->type == 'Hosting-Singapore') {
               $list_hosting = $detail_order->hostings;
               $services = [];
               foreach ($list_hosting as $key => $hosting) {
                 $services[$key]['sevice_id'] = $hosting->id;
                 $services[$key]['sevice_name'] = $hosting->domain;
                 $product = $hosting->product;
                 if (!empty($product)) {
                   $services[$key]['config'] = $product->name;
                 } else {
                   $services[$key]['config'] = '<span class="text-danger">Đã xóa</span>';
                 }
                 $services[$key]['billing_cycle'] = !empty($hosting->billing_cycle) ? $billings[$hosting->billing_cycle] : '1 Tháng';
                 $services[$key]['amount'] = !empty($hosting->price_override) ? number_format($hosting->price_override,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
               }
               $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'Email Hosting') {
               $list_hosting = $detail_order->email_hostings;
               $services = [];
               foreach ($list_hosting as $key => $hosting) {
                 $services[$key]['sevice_id'] = $hosting->id;
                 $services[$key]['sevice_name'] = $hosting->domain;
                 $product = $hosting->product;
                 if (!empty($product)) {
                   $services[$key]['config'] = $product->name;
                 } else {
                   $services[$key]['config'] = '<span class="text-danger">Đã xóa</span>';
                 }
                 $services[$key]['billing_cycle'] = !empty($hosting->billing_cycle) ? $billings[$hosting->billing_cycle] : '1 Tháng';
                 $services[$key]['amount'] = !empty($hosting->price_override) ? number_format($hosting->price_override,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
               }
               $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'Domain') {
               $list_domain = $detail_order->domains;
               $services = [];
               foreach ($list_domain as $key => $domain) {
                 $services[$key]['sevice_id'] = $domain->id;
                 $services[$key]['sevice_name'] = $domain->domain;
                 $product = $domain->product;
                 if (!empty($product)) {
                   $services[$key]['config'] = $product->name;
                 } else {
                   $services[$key]['config'] = '<span class="text-danger">Đã xóa</span>';
                 }
                 $services[$key]['billing_cycle'] = !empty($domain->billing_cycle) ? $billings[$domain->billing_cycle] : '1 Tháng';
                 $services[$key]['amount'] = !empty($domain->price_override) ? number_format($domain->price_override,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
               }
               $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'addon_vps') {
              $order_addon_vps = $detail_order->order_addon_vps;
              $services = [];
              $billing_cycle = 'monthly';
              $detail_order->type_order = 'addon_vps';
              foreach ($order_addon_vps as $key => $addon_vps) {
                  $services[$key]['sevice_id'] = $addon_vps->vps->id;
                  $services[$key]['sevice_name'] = $addon_vps->vps->ip;
                  $services[$key]['cpu'] = $addon_vps->cpu;
                  $services[$key]['ram'] = $addon_vps->ram;
                  $services[$key]['disk'] = $addon_vps->disk;
                  $services[$key]['time'] = !empty($addon_vps->month) ? $addon_vps->month . ' Tháng ' : ' ';
                  $services[$key]['time'] .= !empty($addon_vps->day) ? $addon_vps->day .' Ngày' : '';
                  $add_on_products = $this->get_addon_product_private($detail_order->user_id);
                  $pricing_addon = 0;
                  foreach ($add_on_products as $key1 => $add_on_product) {
                      if (!empty($add_on_product->meta_product->type_addon)) {
                          if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                            $pricing_addon += $addon_vps->month * $addon_vps->cpu * $add_on_product->pricing[$billing_cycle] + (int) round($addon_vps->day  * $addon_vps->cpu * $add_on_product->pricing[$billing_cycle] / 30, -3);
                          }
                      }
                  }
                  foreach ($add_on_products as $key2 => $add_on_product) {
                      if (!empty($add_on_product->meta_product->type_addon)) {
                          if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                            $pricing_addon += $addon_vps->month * $addon_vps->ram * $add_on_product->pricing[$billing_cycle] + (int) round($addon_vps->day  * $addon_vps->ram * $add_on_product->pricing[$billing_cycle] / 30, -3);
                          }
                      }
                  }
                  foreach ($add_on_products as $key3 => $add_on_product) {
                      if (!empty($add_on_product->meta_product->type_addon)) {
                          if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                            $pricing_addon += $addon_vps->month * $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10 + (int) round($addon_vps->day  * $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 30, -3)/10;
                          }
                      }
                  }
                  $services[$key]['amount'] = number_format( $pricing_addon,0,",",".") . ' VNĐ';
              }
              $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'addon_server') {
              $order_addon_servers = $detail_order->order_addon_servers;
              $services = [];
              $billing_cycle = 'monthly';
              $detail_order->type_order = 'addon_server';
              foreach ($order_addon_servers as $key => $addon_server) {
                  $services[$key]['sevice_id'] = $addon_server->server->id;
                  $services[$key]['sevice_name'] = $addon_server->server->ip;
                  $services[$key]['ip'] = $addon_server->ip;
                  $services[$key]['ram'] = $addon_server->ram;
                  $services[$key]['disk'] = $addon_server->disk;
                  $services[$key]['amount'] = number_format( $addon_server->detail_order->sub_total ,0,",",".") . ' VNĐ';
              }
              $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'change_ip') {
              $order_change_vps = $detail_order->order_change_vps;
              $services = [];
              $detail_order->type_order = 'change_ip';
              foreach ($order_change_vps as $key => $change_vps) {
                  $services[$key]['sevice_id'] = $change_vps->vps->id;
                  $services[$key]['sevice_name'] = $change_vps->vps->ip;
                  $services[$key]['amount'] = number_format( $detail_order->sub_total,0,",",".") . ' VNĐ';
              }
              $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'upgrade_hosting') {
              $order_upgrade_hosting = $detail_order->order_upgrade_hosting;
              $services = [];
              $detail_order->type_order = 'upgrade_hosting';
              $services['sevice_id'] = $order_upgrade_hosting->hosting->id;
              $services['sevice_name'] = $order_upgrade_hosting->hosting->domain;
              $product = $order_upgrade_hosting->product;
              if ($product->group_product->private) {
                $services['upgrade'] = '<a href="/admin/product_privates/edit/' . $product->id . '" target="_blank">' . $product->name . '</a>';
              } else {
                $services['upgrade'] = '<a href="/admin/products/edit/' . $product->id . '" target="_blank">' . $product->name . '</a>';
              }
              $services['amount'] = number_format( $detail_order->sub_total,0,",",".") . ' VNĐ';

              $detail_order->services = $services;
           }
       }
       return $detail_order;
    }

    public function get_addon_product_private($user_id)
    {
        $products = [];
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_user = $user->group_user;
            $group_products = $group_user->group_products;
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->get();
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        }
        return $products;
    }

}
