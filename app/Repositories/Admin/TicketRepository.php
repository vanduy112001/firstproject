<?php

namespace App\Repositories\Admin;

use App\Model\Ticket;
use App\Model\TicketMessage;

class TicketRepository
{

    protected $model;
    protected $modelMessage;

    public function __construct()
    {
        $this->model = new Ticket();
        $this->modelMessage = new TicketMessage();
    }

    public function getList($params)
    {
        $limit = !empty($params['limit']) ? $params['limit'] : 20;

        $query = $this->model->orderBy('id', 'desc');

        $result = $query->paginate($limit);

        return $result;
    }

    public function getDetail($id)
    {
        $result = $this->model->find($id);

        return $result;
    }

    public function save($id, $data)
    {
        $find = [];
        if (!empty($id)) {
            $find = $this->model->find($id);
        }
        if (empty($find)) {
            $data['id'] = $id;

            $save = $this->model->create($data);
        } else {
            $save = $find->update($data);
        }

        return $save;
    }


    public function saveMessage($data)
    {
        $save = $this->modelMessage->create($data);

        return $save;
    }

    public function delete($id)
    {
        $ticket = $this->model->find($id);
        foreach ($ticket->messages as $key => $message) {
          $message->delete();
        }
        $delete = $ticket->delete();
        return $delete;
    }

    public function deleteAll($listId)
    {
      foreach ($listId as $key => $id) {
          $ticket = $this->model->find($id);
          foreach ($ticket->messages as $key => $message) {
            $message->delete();
          }
          $delete = $ticket->delete();
      }
      return true;
    }

    public function get_qtt_ticket_pending()
    {
        $ticket_pending = $this->model->where('status' , 'pending')->orderBy('id', 'desc')->get();
        $data = [
          'qtt' => 0,
          'last_time' => false,
        ];
        if ( $ticket_pending->count() > 0 ) {
            $data = [
              'qtt' => $ticket_pending->count(),
              'last_time' => $ticket_pending[0]->created_at,
            ];
        }
        return $data;
    }

}
