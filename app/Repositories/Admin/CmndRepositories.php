<?php

namespace App\Repositories\Admin;

use App\Model\CmndVerify;
use App\Model\User;
use App\Model\UserMeta;

class CmndRepositories {

    protected $user;
    protected $cmnd;
    protected $user_meta;

    public function __construct()
    {
        $this->user = new User;
        $this->user_meta = new UserMeta;
        $this->cmnd = new CmndVerify;
    }

    public function get_all()
    {
        return $this->cmnd->with('user')->orderBy('id', 'desc')->get();
    }
    public function detail($id)
    {
        return $this->cmnd->where('id', $id)->with('user')->first();
    }

    public function confirm($id)
    {

        $cmnd = $this->cmnd->where('id', $id)->first();
        $cmnd->update( [ 'active' => 1, 'active_at' => date('Y-m-d') ] );
        $user_meta = $this->user_meta->where('user_id', $cmnd->user_id)->first();
        $data_cmnd = [
            'cmnd_after' => $cmnd->cmnd_after,
            'cmnd_before' => $cmnd->cmnd_before,
        ];
        $confirm = $user_meta->update($data_cmnd);

        if($confirm === true) {
            return $result = [
                'success' => 'Xác nhận thông tin chứng minh nhân dân thành công'
            ];
        } else {
            return $result = [
                'fail' => 'Xác nhận thông tin chứng minh nhân dân thất bại'
            ];
        }
    }

    public function delete($id)
    {
      try {
        $result = [
          'status' => false,
          'message' => 'Có lỗi xảy ra, vui lòng thử lại.',
        ];
        $cmnd =$this->cmnd->find($id); 
        $delete = $cmnd->delete();
        if ($delete) {
          $result = [
            'status' => true,
            'message' => 'Xóa thành công.',
          ];
        }
        return $result;
      } catch (\Exception $exception) {
        return [
          'status' => false,
          'message' => $exception->getMessage(),
        ];
      }
    }

    public function multidelete($id)
    {
      try {
        $result = [
          'status' => false,
          'message' => 'Có lỗi xảy ra, vui lòng thử lại.',
        ];
        $delete = $this->cmnd->whereIn('id', $id)->delete();
        if ($delete) {
          $result = [
            'status' => true,
            'message' => 'Xóa thành công.',
          ];
        }
        return $result;
      } catch (\Exception $exception) {
        return [
          'status' => false,
          'message' => $exception->getMessage(),
        ];
      }
    }

    public function get_cmnd_unactive_number()
    {
      return $this->cmnd->where('active_at', NULL)->count();
    }

    public function deactive($id)
    {
        $cmnd = $this->cmnd->where('id', $id)->first();
        $cmnd->update( [ 'active' => 0, 'active_at' => NULL ] );
        $user_meta = $this->user_meta->where('user_id', $cmnd->user_id)->first();
        $data_cmnd = [
            'cmnd_after' => NULL,
            'cmnd_before' => NULL,
        ];
        $confirm = $user_meta->update($data_cmnd);

        if($confirm === true) {
            return $result = [
                'success' => 'Hủy kích hoạt chứng minh nhân dân thành công'
            ];
        } else {
            return $result = [
                'fail' => 'Hủy kích hoạt chứng minh nhân dân thất bại'
            ];
        }
    }

}
