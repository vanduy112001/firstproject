<?php

namespace App\Repositories\Admin;

use App\Model\Promotion;
use App\Model\ProductPromotion;
use App\Model\BillingPromotion;


class PromotionRepositories {

    protected $promotion;
    protected $product_promotion;
    protected $billing_promotion;

    public function __construct()
    {
        $this->promotion = new Promotion;
        $this->product_promotion = new ProductPromotion;
        $this->billing_promotion = new BillingPromotion;
    }

    public function list()
    {
        return $this->promotion->orderBy('id', 'desc')->get();
    }

    public function create($data)
    {
        $data_create = [
            'name' => $data['name'],
            'code' => $data['code'],
            'type' => $data['type'],
            'value' => $data['value'],
            'start_date' => date('Y-m-d', strtotime($data['start_date'])),
            'end_date' => date('Y-m-d', strtotime($data['end_date'])),
            'max_uses' => $data['max_uses'],
            'type_order' => !empty($data['type_order']) ? $data['type_order'] : 'order',
            'life_time' => !empty($data['life_time']) ? true : false,
            'apply_one' => !empty($data['apply_one']) ? true : false,
            'upgrade' => !empty($data['upgrade']) ? true : false,
        ];
        $create = $this->promotion->create($data_create);
        if ( !empty($data['product']) ) {
            foreach ($data['product'] as $key => $product_id) {
                $data_product = [
                    'promotion_id' => $create->id,
                    'product_id' => $product_id,
                ];
                $this->product_promotion->create($data_product);
            }
        }
        if ( !empty($data['billing']) ) {
            foreach ($data['billing'] as  $billing) {
                $data_billing = [
                    'promotion_id' =>  $create->id,
                    'billing' =>  $billing,
                ];
                $this->billing_promotion->create($data_billing);
            }
        }
        return $create;
    }

    public function detail_promotion($id)
    {
        return $this->promotion->find($id);
    }

    public function update($data)
    {
        $data_update = [
            'name' => $data['name'],
            'code' => $data['code'],
            'type' => $data['type'],
            'value' => $data['value'],
            'start_date' => date('Y-m-d', strtotime($data['start_date'])),
            'end_date' => date('Y-m-d', strtotime($data['end_date'])),
            'max_uses' => $data['max_uses'],
            'type_order' => !empty($data['type_order']) ? $data['type_order'] : 'order',
            'life_time' => !empty($data['life_time']) ? true : false,
            'apply_one' => !empty($data['apply_one']) ? true : false,
            'upgrade' => !empty($data['upgrade']) ? true : false,
        ];
        $update = $this->promotion->find($data['id'])->update($data_update);
        $this->product_promotion->where('promotion_id', $data['id'])->delete();
        if ( !empty($data['product']) ) {
            foreach ($data['product'] as $key => $product_id) {
                $data_product = [
                    'promotion_id' => $data['id'],
                    'product_id' => $product_id,
                ];
                $this->product_promotion->create($data_product);
            }
        }
        $this->billing_promotion->where('promotion_id', $data['id'])->delete();
        if ( !empty($data['billing']) ) {
            foreach ($data['billing'] as  $billing) {
                $data_billing = [
                    'promotion_id' =>  $data['id'],
                    'billing' =>  $billing,
                ];
                $this->billing_promotion->create($data_billing);
            }
        }
        return $update;
    }

    public function delete($id)
    {
        return $this->promotion->find($id)->delete();
    }

}
