<?php

namespace App\Repositories\Admin;

use App\Model\User;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\Server;
use App\Model\ServerConfig;
use App\Model\Hosting;
use App\Model\LogActivity;
use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\HistoryPay;
use App\Model\Email;
use App\Model\SendMail;
use Illuminate\Support\Facades\Auth;
use App\Model\Proxy;
use Carbon\Carbon;
use App\Model\ServerConfigRam;
use App\Model\ServerConfigIp;


class ServerRepositories
{

  protected $user;
  protected $order;
  protected $detail_order;
  protected $vps;
  protected $server;
  protected $server_config;
  protected $hosting;
  protected $log_activity;
  protected $group_product;
  protected $product;
  protected $history_pay;
  protected $email;
  protected $send_mail;
  protected $proxy;
  protected $server_config_ram;
  protected $server_config_ip;

  public function __construct()
  {
    $this->user = new User;
    $this->order = new Order;
    $this->detail_order = new DetailOrder;
    $this->vps = new Vps;
    $this->server = new Server;
    $this->server_config = new ServerConfig;
    $this->hosting = new Hosting;
    $this->log_activity = new LogActivity();
    $this->group_product = new GroupProduct;
    $this->product = new Product;
    $this->history_pay = new HistoryPay;
    $this->email = new Email;
    $this->send_mail = new SendMail;
    $this->proxy = new Proxy;
    $this->server_config_ram = new ServerConfigRam();
    $this->server_config_ip = new ServerConfigIp();
  }

  public function list_servers()
  {
    return $this->server->with('user', 'detail_order')->orderBy('id', 'desc')->paginate(30);
  }

  public function listServersByUser($userId)
  {
    return $this->server->where('user_id', $userId)->orderBy('id', 'desc')->paginate(30);
  }

  public function detail_server($id)
  {
    return $this->server->find($id);
  }

  public function countUsed($userId)
  {
    return $this->server->where('user_id', $userId)->where('status', 'Active')->count();
  }

  public function listProductByUser($userId)
  {
    $user = $this->user->find($userId);
    $list_group_product = $this->group_product->where('group_user_id', $user->group_user_id)
      ->where('type', 'server')->get();
    $products = [];
    foreach ($list_group_product as $key => $group_product) {
      foreach ($group_product->products as $key => $product) {
        $products[] = $product;
      }
    }
    return $products;
  }

  public function listAddonServer($userId, $productId)
  {
    $product = $this->product->find($productId);
    $user = $this->user->find($userId);
    $list_group_product = $this->group_product->where('group_user_id', $user->group_user_id)
      ->where('type', 'addon_server')->get();
    $listAddonServer = [];
    foreach ($list_group_product as $key => $group_product) {
      foreach ($group_product->products as $key => $product) {
        $listAddonServer[] = $product;
      }
    }
    return [
      'product' => $listAddonServer,
      'disk2' => !empty($product->product_drive->second) ? 1 : 0,
    ];
  }

  public function loadTotal($request)
  {
    $product = $this->product->find($request['productId']);
    $total = 0;
    if (!empty($product->pricing[$request['billing_cycle']])) {
      $product_disk_2 = $this->product->find($request['disk2']);
      $amount_disk2 = !empty($product_disk_2->pricing[$request['billing_cycle']]) ? $product_disk_2->pricing[$request['billing_cycle']] : 0;
      // disk3
      $product_disk_3 = $this->product->find($request['disk3']);
      $amount_disk3 = !empty($product_disk_3->pricing[$request['billing_cycle']]) ? $product_disk_3->pricing[$request['billing_cycle']] : 0;
      // disk4
      $product_disk_4 = $this->product->find($request['disk4']);
      $amount_disk4 = !empty($product_disk_4->pricing[$request['billing_cycle']]) ? $product_disk_4->pricing[$request['billing_cycle']] : 0;
      // disk5
      $product_disk_5 = $this->product->find($request['disk5']);
      $amount_disk5 = !empty($product_disk_5->pricing[$request['billing_cycle']]) ? $product_disk_5->pricing[$request['billing_cycle']] : 0;
      // disk6
      $product_disk_6 = $this->product->find($request['disk6']);
      $amount_disk6 = !empty($product_disk_6->pricing[$request['billing_cycle']]) ? $product_disk_6->pricing[$request['billing_cycle']] : 0;
      // disk4
      $product_disk_7 = $this->product->find($request['disk7']);
      $amount_disk7 = !empty($product_disk_7->pricing[$request['billing_cycle']]) ? $product_disk_7->pricing[$request['billing_cycle']] : 0;
      // disk4
      $product_disk_8 = $this->product->find($request['disk8']);
      $amount_disk8 = !empty($product_disk_8->pricing[$request['billing_cycle']]) ? $product_disk_8->pricing[$request['billing_cycle']] : 0;
      //product
      $amount = $product->pricing[$request['billing_cycle']];

      $total = $amount + $amount_disk3 + $amount_disk4 + $amount_disk5 + $amount_disk6 + $amount_disk7
        + $amount_disk8 + $amount_disk2;
      return [
        'error' => 0,
        'total' => $total,
      ];
    } else {
      return [
        'error' => 1,
        'amount' => 0,
      ];
    }
  }

  public function createServer($request)
  {
    $billing = [
      'monthly' => '1 Month',
      'twomonthly' => '2 Month',
      'quarterly' => '3 Month',
      'semi_annually' => '6 Month',
      'annually' => '1 Year',
      'biennially' => '2 Year',
      'triennially' => '3 Year'
    ];
    $product = $this->product->find($request['product_id']);
    // total
    $total = 0;
    if (!empty($request['total'])) {
      $total = $request['total'];
    } else {
      if (!empty($product->pricing[$request['billing_cycle']])) {
        $product_disk_2 = $this->product->find($request['disk2']);
        $amount_disk2 = !empty($product_disk_2->pricing[$request['billing_cycle']]) ? $product_disk_2->pricing[$request['billing_cycle']] : 0;
        // disk3
        $product_disk_3 = $this->product->find($request['disk3']);
        $amount_disk3 = !empty($product_disk_3->pricing[$request['billing_cycle']]) ? $product_disk_3->pricing[$request['billing_cycle']] : 0;
        // disk4
        $product_disk_4 = $this->product->find($request['disk4']);
        $amount_disk4 = !empty($product_disk_4->pricing[$request['billing_cycle']]) ? $product_disk_4->pricing[$request['billing_cycle']] : 0;
        // disk5
        $product_disk_5 = $this->product->find($request['disk5']);
        $amount_disk5 = !empty($product_disk_5->pricing[$request['billing_cycle']]) ? $product_disk_5->pricing[$request['billing_cycle']] : 0;
        // disk6
        $product_disk_6 = $this->product->find($request['disk6']);
        $amount_disk6 = !empty($product_disk_6->pricing[$request['billing_cycle']]) ? $product_disk_6->pricing[$request['billing_cycle']] : 0;
        // disk4
        $product_disk_7 = $this->product->find($request['disk7']);
        $amount_disk7 = !empty($product_disk_7->pricing[$request['billing_cycle']]) ? $product_disk_7->pricing[$request['billing_cycle']] : 0;
        // disk4
        $product_disk_8 = $this->product->find($request['disk8']);
        $amount_disk8 = !empty($product_disk_8->pricing[$request['billing_cycle']]) ? $product_disk_8->pricing[$request['billing_cycle']] : 0;
        //product
        $amount = $product->pricing[$request['billing_cycle']];

        $total = $amount + $amount_disk3 + $amount_disk4 + $amount_disk5 + $amount_disk6 + $amount_disk7
          + $amount_disk8 + $amount_disk2;
        return [
          'error' => 0,
          'total' => $total,
        ];
      } else {
        return false;
      }
    }
    // order
    $data_order = [
      'user_id' => $request['user_id'],
      'total' => $total,
      'status' => 'Finish',
    ];
    $create_order = $this->order->create($data_order);
    $date = date('Y-m-d');
    $due_date = date('Y-m-d', strtotime($date . $billing[$request['billing_cycle']]));
    // order_detail
    $data_order_detail = [
      'order_id' => $create_order->id,
      'type' => 'Server',
      'due_date' => $due_date,
      'description' => !empty($request['description']) ? $request['description'] : '',
      'payment_method' => 1,
      'status' => 'paid',
      'sub_total' => $total,
      'quantity' => 1,
      'user_id' => $request['user_id'],
      'paid_date' => date('Y-m-d', strtotime($request['payment_date'])),
    ];
    $detail_order = $this->detail_order->create($data_order_detail);
    // history_pay
    $data_history = [
      'user_id' => $request['user_id'],
      'ma_gd' => 'GDODS' . strtoupper(substr(sha1(time()), 34, 39)),
      'discription' => 'Hóa đơn số ' . $detail_order->id,
      'type_gd' => '2',
      'method_gd' => 'invoice',
      'date_gd' => date('Y-m-d'),
      'money' => $total,
      'status' => 'Active',
      'detail_order_id' => $detail_order->id,
      'method_gd_invoice' => 'credit',
    ];
    $this->history_pay->create($data_history);
    // server
    $data_server = [
      'detail_order_id' => $detail_order->id,
      'user_id' => $request['user_id'],
      'type' => 'Server',
      'product_id' => $request['product_id'],
      'os' => $request['os_server'],
      'billing_cycle' => $request['billing_cycle'],
      'status' => 'Active',
      'location' => $request['datacenter'],
      'raid' => $request['raid'],
      'server_management' => $request['server_management'],
      'status_server' => $request['status_server'],
      'user_name' => !empty($request['user_name']) ? $request['user_name'] : '',
      'password' => !empty($request['password']) ? $request['password'] : '',
      'paid' => 'paid',
      'date_create' => date('Y-m-d'),
      'description' => '',
      'rack' => $request['rack'],
      'ip' => $request['ip'],
      'ip2' => $request['ip2'],
      'date_create' => date('Y-m-d', strtotime($request['date_create'])),
      'next_due_date' => date('Y-m-d', strtotime($request['next_due_date'])),
    ];
    if (!empty($request['price_override'])) {
      $data_server['amount'] = $total;
    }
    $server = $this->server->create($data_server);
    // addon server
    if (
      !empty($request['disk3']) || !empty($request['disk4']) || !empty($request['disk5'])
      || !empty($request['disk6']) || !empty($request['disk7']) || !empty($request['disk8'])
    ) {
      $data_server_config = [
        'server_id' => $server->id,
        'disk2' => !empty($request['disk2']) ? $request['disk2'] : 0,
        'disk3' => !empty($request['disk3']) ? $request['disk3'] : 0,
        'disk4' => !empty($request['disk4']) ? $request['disk4'] : 0,
        'disk5' => !empty($request['disk5']) ? $request['disk5'] : 0,
        'disk6' => !empty($request['disk6']) ? $request['disk6'] : 0,
        'disk7' => !empty($request['disk7']) ? $request['disk7'] : 0,
        'disk8' => !empty($request['disk8']) ? $request['disk8'] : 0,
      ];
      $this->server_config->create($data_server_config);
    }
    // log
    $data_log = [
      'user_id' => Auth::id(),
      'action' => 'tạo',
      'model' => 'Admin/Server',
      'description' => ' Server ' . $server->ip . ' ' . $server->ip2,
      'service' =>  $server->ip,
    ];
    $this->log_activity->create($data_log);

    return $server;
  }

  public function update($data)
  {
    // dd($data);
    $server = $this->server->find($data['id']);
    $data_server = [
      'user_id' => $data['user_id'],
      'server_name' => $data['server_name'],
      'ip' => $data['ip'],
      'ip2' => $data['ip2'],
      'user_name' => $data['user_name'],
      'password' => $data['password'],
      'status_server' => $data['status_server'],
      'status' => 'Active',
      'os' => $data['os_server'],
      'raid' => $data['raid'],
      'location' => $data['datacenter'],
      'rack' => $data['rack'],
      'product_id' => !empty($data['product_id']) ? $data['product_id'] : '',
      'config_text' => !empty($data['config_text']) ? $data['config_text'] : '',
    ];
    if (!empty($data['date_create']) && !empty($data['billing_cycle'])) {
      // Tính ngày kết thúc cho VPS
      $billing = [
        'monthly' => '1 Month',
        'twomonthly' => '2 Month',
        'quarterly' => '3 Month',
        'semi_annually' => '6 Month',
        'annually' => '1 Year',
        'biennially' => '2 Year',
        'triennially' => '3 Year'
      ];
      $date_create = $data['date_create'];
      $billing_cycle = $data['billing_cycle'];
      $next_due_date = date('Y-m-d', strtotime($date_create . $billing[$billing_cycle]));
      $data_server['date_create'] = date('Y-m-d', strtotime($data['date_create']));
      $data_server['next_due_date'] = $next_due_date;
      $data_server['billing_cycle'] = $data['billing_cycle'];
    } elseif (!empty($data['date_create'])) {
      // Tính ngày kết thúc cho VPS
      $billing = [
        'monthly' => '1 Month',
        'twomonthly' => '2 Month',
        'quarterly' => '3 Month',
        'semi_annually' => '6 Month',
        'annually' => '1 Year',
        'biennially' => '2 Year',
        'triennially' => '3 Year'
      ];
      $date_create = $data['date_create'];
      $billing_cycle = $server->billing_cycle;
      $next_due_date = date('Y-m-d', strtotime($date_create . $billing[$billing_cycle]));

      $data_server['date_create'] = date('Y-m-d', strtotime($data['date_create']));
      $data_server['next_due_date'] = $next_due_date;
    } elseif (!empty($data['billing_cycle'])) {
      $billing = [
        'monthly' => '1 Month',
        'twomonthly' => '2 Month',
        'quarterly' => '3 Month',
        'semi_annually' => '6 Month',
        'annually' => '1 Year',
        'biennially' => '2 Year',
        'triennially' => '3 Year'
      ];
      $date_create = $server->date_create;
      $billing_cycle = $data['billing_cycle'];
      $next_due_date = date('Y-m-d', strtotime($date_create . $billing[$billing_cycle]));

      $data_server['next_due_date'] = $next_due_date;
      $data_server['billing_cycle'] = $data['billing_cycle'];
    }

    if (!empty($data['next_due_date'])) {
      $data_server['next_due_date'] = date('Y-m-d', strtotime($data['next_due_date']));
    }
    if (!empty($data['price'])) {
      $data_server['amount'] = $data['price'];
    }
    if ( !empty($data['disk2']) || !empty($data['disk3']) || !empty($data['disk4']) || !empty($data['disk5']) || !empty($data['disk6']) 
      || !empty($data['disk7']) || !empty($data['disk8'])
    ) {
      if (!empty($server->server_config)) {
        $server_config = $server->server_config;
        $data_config = [
          'disk2' => !empty($data['disk2']) ? $data['disk2'] : 0,
          'disk3' => !empty($data['disk3']) ? $data['disk3'] : 0,
          'disk4' => !empty($data['disk4']) ? $data['disk4'] : 0,
          'disk5' => !empty($data['disk5']) ? $data['disk5'] : 0,
          'disk6' => !empty($data['disk6']) ? $data['disk6'] : 0,
          'disk7' => !empty($data['disk7']) ? $data['disk7'] : 0,
          'disk8' => !empty($data['disk8']) ? $data['disk8'] : 0,
        ];
        $server_config->update($data_config);
      } else {
        $data_server_config = [
          'server_id' => $server->id,
          'disk2' => !empty($data['disk2']) ? $data['disk2'] : 0,
          'disk3' => !empty($data['disk3']) ? $data['disk3'] : 0,
          'disk4' => !empty($data['disk4']) ? $data['disk4'] : 0,
          'disk5' => !empty($data['disk5']) ? $data['disk5'] : 0,
          'disk6' => !empty($data['disk6']) ? $data['disk6'] : 0,
          'disk7' => !empty($data['disk7']) ? $data['disk7'] : 0,
          'disk8' => !empty($data['disk8']) ? $data['disk8'] : 0,
        ];
        $this->server_config->create($data_server_config);
      }
    }
    // addon ram
    if ( !empty($data['addon_ram']) ) {
      $this->server_config_ram->where('server_id', $server->id)->delete();
      $check_addon_ram = $this->check_addon($data['addon_ram']);
      if ( $check_addon_ram ) {
        if (!empty($server->server_config)) {
          $server_config = $server->server_config;
          $server_config->ram = 1;
          $server_config->save();
        } else {
          $data_server_config = [
            'server_id' => $server->id,
            'ram' => 1,
          ];
          $this->server_config->create($data_server_config);
        }
        foreach ($data['addon_ram'] as $key => $addon_ram) {
          $data_server_config_ram = [
            'server_id' => $server->id,
            'product_id' => $addon_ram,
          ];
          $this->server_config_ram->create($data_server_config_ram);
        }
      }
    }
    // addon ip
    if ( !empty($data['addon_ip']) ) {
      $this->server_config_ip->where('server_id', $server->id)->delete();
      $check_addon_ip = $this->check_addon($data['addon_ip']);
      if ( $check_addon_ip ) {
        if (!empty($server->server_config)) {
          $server_config = $server->server_config;
          $server_config->ip = 1;
          $server_config->save();
        } else {
          $data_server_config = [
            'server_id' => $server->id,
            'ip' => 1,
          ];
          $this->server_config->create($data_server_config);
        }
        foreach ($data['addon_ip'] as $key => $addon_ip) {
          $data_server_config_ip = [
            'server_id' => $server->id,
            'product_id' => $addon_ip,
            'ips' => !empty($data['addon_ip_text'][$key]) ? $data['addon_ip_text'][$key] : '',
          ];
          $this->server_config_ip->create($data_server_config_ip);
        }
      }
    }
    // dd($data_server, $data);
    $text_vps = '';
    $text_server = '#' . $server->id;
    if (!empty($server->ip)) {
      $text_vps .= ' Ip: <a target="_blank" href="/admin/servers/detail/' . $server->id  . '">' . $server->ip . '</a>';
    }
    $data_log = [
      'user_id' => Auth::user()->id,
      'action' => 'chỉnh sửa',
      'model' => 'Admin/Server',
      'description' => ' Server ' . $text_vps,
      'service' =>  $server->ip,
    ];
    $this->log_activity->create($data_log);

    return $server->update($data_server);
  }

  public function check_addon($list_addon)
  {
    $check = false;
    foreach ($list_addon as $key => $addon) {
      if ( !empty($addon) ) {
        $check = true;
      }
    }
    return $check;
  }

  public function delete($id, $type)
  {
    switch ($type) {
      case ('multi'):
        $data_ids = $id;
          foreach ($data_ids as $id) {
            $server = $this->server->find($id);
            if (isset($server)) {
              $data_log = [
                'user_id' => Auth::id(),
                'action' => 'xóa',
                'model' => 'Admin/Server',
                'description' => ' Server ' . $server->ip . ' ' . $server->ip2,
                'service' =>  $server->ip,
              ];
              $this->log_activity->create($data_log);

              if ( $server->status == 'Active' ) {
                $server->status_server = 'delete_server';
                // $server->server_id = -1;
                $server->save();
              } else {
                if (!empty($server->server_config)) {
                  $this->server_config->where('server_id', $id)->delete();
                }
                $server->delete();
              }
            } else {
              return [
                'error' => 0,
                'action' => false,
              ];
            }
          }
          return [
            'error' => 0,
            'action' => true,
          ];
     
        break;
      case ('single'):
        try {
          $server = $this->server->find($id);
          if (isset($server)) {
            $data_log = [
              'user_id' => Auth::id(),
              'action' => 'xóa',
              'model' => 'Admin/Server',
              'description' => ' Server ' . $server->ip . ' ' . $server->ip2,
              'service' =>  $server->ip,
            ];
            $this->log_activity->create($data_log);
            if ( $server->status == 'Active' ) {
              $server->status_server = 'delete_server';
              // $server->server_id = -1;
              $server->save();
            } else {
              if (!empty($server->server_config)) {
                $this->server_config->where('server_id', $id)->delete();
              }
              $server->delete();
            }
            return [
              'error' => 0,
              'action' => true,
            ];
          } else {
            return [
              'error' => 0,
              'action' => false,
            ];
          }
        } catch (\Throwable $th) {
          //throw $th;
          report($th);
          return [
            'error' => 0,
            'action' => false,
          ];
        }
        break;
    }
  }

  public function loadMailFinishConfig($serverId)
  {
    $billings = config('billing');
    $server = $this->server->find($serverId);
    $product = $server->product;
    if (!isset($product)) {
      return ['error' => 1];
    }
    $emailFinishConfig = $this->email->find($product->meta_product->email_config_finish);
    // dd($emailFinishConfig);
    if (isset($emailFinishConfig)) {
      $url = url('');
      $url = str_replace(['http://', 'https://'], ['', ''], $url);
      $user = $server->user;
      $server_cpu = $product->meta_product->cpu . ' (' . $product->meta_product->cores . ')';
      $server_disk = $product->meta_product->disk . ' ';
      $server_disk .= $this->get_addon_disk_server($server->id);
      // status
      $status = '';
      if ($server->status_server == 'on') {
        $status = 'Đang bật';
      } elseif ($server->status_server == 'off') {
        $status = 'Đã tắt';
      } elseif ($server->status_server == 'progressing') {
        $status = 'Đang cài đặt';
      } elseif ($server->status_server == 'cancel') {
        $status = 'Đã hủy';
      } elseif ($server->status_server == 'delete_server') {
        $status = 'Đã xóa';
      }
      $search = [
        '{$user_name}',  '{$user_email}', '{$order_id}',  '{$product_name}', '{$billing_cycle}',
        '{$order_created_at}', '{$sub_total}', '{$total}', '{$qtt}', '{$server_os}', '{$url}', '{$server_cpu}', '{$server_ram}',
        '{$server_disk}', '{$server_qtt_ip}', '{$server_raid}', '{$server_datacenter}', '{$server_management}', '{$next_due_date}',
        '{$server_user_name}', '{$server_password}', '{$server_status}', '{$server_ip}', '{$server_ip2}'
      ];
      $replace = [
        $user->name,  $user->email, $server->detail_order->order->id,  $product->name, $billings[$server->billing_cycle], $server->detail_order->created_at,
        number_format($server->detail_order->sub_total, 0, ",", ".") . ' VNĐ', number_format($server->detail_order->order->total, 0, ",", ".") . ' VNĐ',
        $server->detail_order->quantity, $server->os, $url, $server_cpu, $product->meta_product->memory, $server_disk,
        $product->meta_product->ip, $server->raid, $server->location, $server->server_management,
        date('d-m-Y', strtotime($server->next_due_date)), $server->user_name, $server->password,
        $status, $server->ip, $server->ip2
      ];
      $content = str_replace($search, $replace, !empty($emailFinishConfig->meta_email->content) ? $emailFinishConfig->meta_email->content : '');
      return [
        'error' => 0,
        'subject' =>  $emailFinishConfig->meta_email->subject,
        'content' => $content
      ];
    } else {
      return ['error' => 1];
    }
  }

  public function get_addon_disk_server($serverId)
  {
    $server = $this->server->find($serverId);
    if (!empty($server->server_config)) {
      $text_config = '';
      $server_config = $server->server_config;
      $data_config = [];
      if (!empty($server_config->disk2)) {
        $data_config[] = [
          'id' => $server_config->disk2,
          'name' => $server_config->product_disk2->name,
          'qtt' => 1
        ];
      }
      if (!empty($server_config->disk3)) {
        $check = true;
        foreach ($data_config as $key => $data) {
          if ($data['id'] == $server_config->disk3) {
            $data_config[$key]['qtt'] = $data['qtt'] + 1;
            $check = false;
          }
        }
        if ($check) {
          $data_config[] = [
            'id' => $server_config->disk3,
            'name' => $server_config->product_disk3->name,
            'qtt' => 1
          ];
        }
      }
      if (!empty($server_config->disk4)) {
        $check = true;
        foreach ($data_config as $key => $data) {
          if ($data['id'] == $server_config->disk4) {
            $data_config[$key]['qtt'] = $data['qtt'] + 1;
            $check = false;
          }
        }
        if ($check) {
          $data_config[] = [
            'id' => $server_config->disk4,
            'name' => $server_config->product_disk4->name,
            'qtt' => 1
          ];
        }
      }
      if (!empty($server_config->disk5)) {
        $check = true;
        foreach ($data_config as $key => $data) {
          if ($data['id'] == $server_config->disk5) {
            $data_config[$key]['qtt'] = $data['qtt'] + 1;
            $check = false;
          }
        }
        if ($check) {
          $data_config[] = [
            'id' => $server_config->disk5,
            'name' => $server_config->product_disk5->name,
            'qtt' => 1
          ];
        }
      }
      if (!empty($server_config->disk6)) {
        $check = true;
        foreach ($data_config as $key => $data) {
          if ($data['id'] == $server_config->disk6) {
            $data_config[$key]['qtt'] = $data['qtt'] + 1;
            $check = false;
          }
        }
        if ($check) {
          $data_config[] = [
            'id' => $server_config->disk6,
            'name' => $server_config->product_disk6->name,
            'qtt' => 1
          ];
        }
      }
      if (!empty($server_config->disk7)) {
        $check = true;
        foreach ($data_config as $key => $data) {
          if ($data['id'] == $server_config->disk7) {
            $data_config[$key]['qtt'] = $data['qtt'] + 1;
            $check = false;
          }
        }
        if ($check) {
          $data_config[] = [
            'id' => $server_config->disk7,
            'name' => $server_config->product_disk7->name,
            'qtt' => 1
          ];
        }
      }
      if (!empty($server_config->disk8)) {
        $check = true;
        foreach ($data_config as $key => $data) {
          if ($data['id'] == $server_config->disk8) {
            $data_config[$key]['qtt'] = $data['qtt'] + 1;
            $check = false;
          }
        }
        if ($check) {
          $data_config[] = [
            'id' => $server_config->disk8,
            'name' => $server_config->product_disk8->name,
            'qtt' => 1
          ];
        }
      }
      if (count($data_config)) {
        $text_config = '(Addon ';
        foreach ($data_config as $key => $data) {
          $text_config .= ($data['qtt'] . ' x ' . $data['name']);
          if (count($data_config) - 1 > $key) {
            $text_config .= ' - ';
          }
        }
        $text_config .= ')';
      }
      return $text_config;
    } else {
      return '';
    }
  }

  public function sendMail($request)
  {
    $user = $this->user->find($request['userId']);
    $server = $this->server->find($request['serverId']);
    try {
      // gửi mail
      $data = [
        'content' => $request['content'],
        'user_name' => $user->name,
        'subject' => $request['subject'],
      ];
      $data_tb_send_mail = [
        'type' => 'mail_finish_config_server',
        'type_service' => 'server',
        'user_id' => $user->id,
        'status' => false,
        'content' => serialize($data),
      ];
      $this->send_mail->create($data_tb_send_mail);
      // log
      $data_log = [
        'user_id' => Auth::id(),
        'action' => 'gửi mail',
        'model' => 'Admin/Server',
        'description' => ' Server ' . $server->ip . ' ' . $server->ip2,
        'service' =>  $server->ip,
      ];
      $this->log_activity->create($data_log);
      return true;
    } catch (\Exception $e) {
      report($e);
      return false;
    }
  }

  public function listProxy($request)
  {
    $data = [];
    $data['data'] = [];
    $status_vps = config('status_vps');
    $billings = config('billing');
    $qtt = !empty($request['qtt']) ? $request['qtt'] : 30;
    if (!empty($request['status'])) {
      $listProxy = $this->proxy->where('status', $request['status'])->where('ip', 'like', '%' . $request['q'] . '%')
        ->orderBy('id', 'desc')->paginate($qtt);
    } else {
      $listProxy = $this->proxy->where('status', '!=', 'Pending')->where('ip', 'like', '%' . $request['q'] . '%')
        ->orderBy('id', 'desc')->paginate($qtt);
    }

    foreach ($listProxy as $key => $proxy) {
      // khách hàng
      $proxy->text_user_name = !empty($proxy->user->name) ? $proxy->user->name : '<span class="text-danger">Đã xóa</span>';
      // sản phẩm
      $product = $proxy->product;
      if (!empty($proxy->product)) {
        if (!empty($proxy->product->duplicate)) {
          $proxy->text_product = '<a href="' . route('admin.product.edit', $proxy->product->id) . '">' . $proxy->product->name . '</a>';
        } else {
          if (!empty($proxy->product->group_product->private)) {
            $proxy->text_product = '<a href="' . route('admin.product_private.editPrivate', [$proxy->product->group_product->id, $proxy->product->id]) . '">' . $proxy->product->name . '</a>';
          } else {
            $proxy->text_product = '<a href="' . route('admin.product.edit', $proxy->product->id) . '">' . $proxy->product->name . '</a>';
          }
        }
      } else {
        $proxy->text_product = '<span class="text-danger">Đã xóa</span>';
      }
      // ngày tạo
      $proxy->date_create = date('H:i:m d-m-Y', strtotime($proxy->created_at));
      // ngày kết thúc
      $proxy->text_next_due_date = date('d-m-Y', strtotime($proxy->next_due_date));
      // tổng thời gian thuê 
      $total_time = '';
      $create_date = new Carbon($proxy->date_create);
      $next_due_date = new Carbon($proxy->next_due_date);
      if ($next_due_date->diffInYears($create_date)) {
        $year = $next_due_date->diffInYears($create_date);
        $total_time = $year . ' Năm ';
        $create_date = $create_date->addYears($year);
        $month = $next_due_date->diffInMonths($create_date);
        //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
        if ($create_date != $next_due_date) {
          $total_time .= $month . ' Tháng';
        }
      } else {
        $diff_month = $next_due_date->diffInMonths($create_date);
        if ($create_date != $next_due_date) {
          $total_time = $diff_month . ' Tháng';
        } else {
          $total_time = $diff_month . ' Tháng';
        }
      }
      $proxy->total_time = $total_time;
      // chu kỳ thanh toán
      $proxy->text_billing_cycle = !empty($billings[$proxy->billing_cycle]) ? $billings[$proxy->billing_cycle] : '1 Tháng';
      // chi phí
      if (!empty($proxy->price_override)) {
        $amount = $proxy->price_override;
      } else {
        $amount = !empty($product->pricing[$proxy->billing_cycle]) ? $product->pricing[$proxy->billing_cycle] : 0;
      }
      $proxy->amount = number_format($amount, 0, ",", ".") . ' VNĐ';
      // trạng thái
      if ($proxy->status != 'Pending') {
        if ($proxy->status == 'on') {
          $proxy->text_status = '<span class="text-success">Đang bật</span>';
        } elseif ($proxy->status == 'off') {
          $proxy->text_status = '<span class="text-danger">Đã tắt</span>';
        } elseif ($proxy->status == 'progressing') {
          $proxy->text_status = '<span class="text-info">Đang tạo</span>';
        } elseif ($proxy->status == 'expire') {
          $proxy->text_status = '<span class="text-danger">Hết hạn</span>';
        } elseif ($proxy->status == 'reset_password') {
          $proxy->text_status = '<span class="text-primary">Đang đổi mật khâu</span>';
        } elseif ($proxy->status == 'cancel') {
          $proxy->text_status = '<span class="text-danger">Đã hủy</span>';
        } elseif ($proxy->status == 'delete') {
          $proxy->text_status = '<span class="text-danger">Đã xóa</span>';
        } elseif ($proxy->status == 'change_user') {
          $proxy->text_status = '<span class="text-danger">Đã chuyển</span>';
        } else {
          $proxy->text_status = '<span class="text-danger">Trạng thái lỗi!</span>';
        }
      } else {
        $proxy->text_status = '<span class="text-danger">Chưa thanh toán</span>';
      }
      $data['data'][] = $proxy;
    }
    $data['total'] = $listProxy->total();
    $data['perPage'] = $listProxy->perPage();
    $data['current_page'] = $listProxy->currentPage();
    return $data;
  }

  public function detail_proxy($id)
  {
    return $this->proxy->find($id);
  }

  public function updateProxy($request)
  {
    $proxy = $this->proxy->find($request['id']);
    $dataProxy = [
      'ip' => $request['ip'],
      'port' => $request['port'],
      'username' => $request['user_name'],
      'password' => $request['password'],
      'status' => $request['status'],
      'vm_id' => $request['vm_id'],
    ];
    if ($proxy->user_id != $request['user_id']) {
      $dataProxy['user_id'] = $request['user_id'];
      $user = $this->user->find($request['user_id']);
      $dataDashboard = [
        'vm_id' => $proxy->vm_id,
        'customer_id' => $user->customer_id,
      ];
      // Gửi đổi User cho proxy
      $request_vcenter = true;
      if ($request_vcenter) {
        $detail_order = $proxy->detail_order;
        $detail_order->user_id = $request['user_id'];
        $detail_order->save();
        $order = $detail_order->order;
        $order->user_id = $request['user_id'];
        $order->save();
      }
    }
    if (!empty($request['date_create']) && !empty($request['billing_cycle'])) {
      // Tính ngày kết thúc cho VPS
      $billing = [
        'monthly' => '1 Month',
        'twomonthly' => '2 Month',
        'quarterly' => '3 Month',
        'semi_annually' => '6 Month',
        'annually' => '1 Year',
        'biennially' => '2 Year',
        'triennially' => '3 Year'
      ];
      $date_create = $request['date_create'];
      $billing_cycle = $request['billing_cycle'];
      $next_due_date = date('Y-m-d', strtotime($date_create . $billing[$billing_cycle]));
      $dataProxy['created_at'] = date('Y-m-d', strtotime($request['date_create'])) . ' ' . date('H:i:s', strtotime($proxy->created_at));
      $dataProxy['next_due_date'] = $next_due_date;
      $dataProxy['billing_cycle'] = $request['billing_cycle'];
    } elseif (!empty($request['date_create'])) {
      // Tính ngày kết thúc cho VPS
      $billing = [
        'monthly' => '1 Month',
        'twomonthly' => '2 Month',
        'quarterly' => '3 Month',
        'semi_annually' => '6 Month',
        'annually' => '1 Year',
        'biennially' => '2 Year',
        'triennially' => '3 Year'
      ];
      $date_create = $request['date_create'];
      $billing_cycle = $proxy->billing_cycle;
      $next_due_date = date('Y-m-d', strtotime($date_create . $billing[$billing_cycle]));

      $dataProxy['created_at'] = date('Y-m-d', strtotime($request['date_create'])) . ' ' . date('H:i:s', strtotime($proxy->created_at));
      $dataProxy['next_due_date'] = $next_due_date;
    } elseif (!empty($request['billing_cycle'])) {
      $billing = [
        'monthly' => '1 Month',
        'twomonthly' => '2 Month',
        'quarterly' => '3 Month',
        'semi_annually' => '6 Month',
        'annually' => '1 Year',
        'biennially' => '2 Year',
        'triennially' => '3 Year'
      ];
      $date_create = $proxy->date_create;
      $billing_cycle = $request['billing_cycle'];
      $next_due_date = date('Y-m-d', strtotime($date_create . $billing[$billing_cycle]));

      $dataProxy['next_due_date'] = $next_due_date;
      $dataProxy['billing_cycle'] = $request['billing_cycle'];
    }
    if (!empty($request['next_due_date'])) {
      $dataProxy['next_due_date'] = date('Y-m-d', strtotime($request['next_due_date']));
    }
    if (!empty($request['price'])) {
      $dataProxy['price_override'] = $request['price'];
    }
    $text_vps = '';
    $text_server = '#' . $proxy->id;
    if (!empty($proxy->ip)) {
      $text_vps .= ' Ip: <a target="_blank" href="/admin/proxy/detail/' . $proxy->id  . '">' . $proxy->ip . '</a>';
    }
    // dd($dataProxy);
    $data_log = [
      'user_id' => Auth::user()->id,
      'action' => 'chỉnh sửa',
      'model' => 'Admin/Proxy',
      'description' => ' Proxy ' . $text_vps,
      'service' =>  $proxy->ip,
    ];
    $this->log_activity->create($data_log);

    return $proxy->update($dataProxy);
  }

  public function deleteProxy($id)
  {
    try {
      $proxy = $this->proxy->find($id);
      if (isset($proxy)) {
        $data_log = [
          'user_id' => Auth::id(),
          'action' => 'xóa',
          'model' => 'Admin/Proxy',
          'description' => ' Proxy ' . $proxy->ip,
          'service' =>  $proxy->ip,
        ];
        $this->log_activity->create($data_log);
        if ( $proxy->status != 'Pending' ) {
          $proxy->proxy_id = -1;
          $proxy->status = 'delete';
          return $proxy->save();
        } else {
          return $proxy->delete();
        }
      } else {
        return false;
      }
    } catch (\Throwable $th) {
      report($th);
      return false;
    }
  }

  public function get_config_server($serverId)
  {
    $server = $this->server->find($serverId);
    if ( !empty($server->server_config) ) {
      $text_config = '';
      $server_config = $server->server_config;
      $data_config = [];
      if ( !empty($server_config->disk2) ) {
        $data_config[] = [
          'id' => $server_config->disk2,
          'name' => $server_config->product_disk2->name,
          'qtt' => 1
        ];
      }
      if ( !empty($server_config->disk3) ) {
        $check = true;
        foreach ($data_config as $key => $data) {
          if ($data['id'] == $server_config->disk3) {
            $data_config[$key]['qtt'] = $data['qtt'] + 1;
            $check = false;
          }
        }
        if ( $check ) {
          $data_config[] = [
            'id' => $server_config->disk3,
            'name' => $server_config->product_disk3->name,
            'qtt' => 1
          ];
        }
      }
      if ( !empty($server_config->disk4) ) {
        $check = true;
        foreach ($data_config as $key => $data) {
          if ($data['id'] == $server_config->disk4) {
            $data_config[$key]['qtt'] = $data['qtt'] + 1;
            $check = false;
          }
        }
        if ( $check ) {
          $data_config[] = [
            'id' => $server_config->disk4,
            'name' => $server_config->product_disk4->name,
            'qtt' => 1
          ];
        }
      }
      if ( !empty($server_config->disk5) ) {
        $check = true;
        foreach ($data_config as $key => $data) {
          if ($data['id'] == $server_config->disk5) {
            $data_config[$key]['qtt'] = $data['qtt'] + 1;
            $check = false;
          }
        }
        if ( $check ) {
          $data_config[] = [
            'id' => $server_config->disk5,
            'name' => $server_config->product_disk5->name,
            'qtt' => 1
          ];
        }
      }
      if ( !empty($server_config->disk6) ) {
        $check = true;
        foreach ($data_config as $key => $data) {
          if ($data['id'] == $server_config->disk6) {
            $data_config[$key]['qtt'] = $data['qtt'] + 1;
            $check = false;
          }
        }
        if ( $check ) {
          $data_config[] = [
            'id' => $server_config->disk6,
            'name' => $server_config->product_disk6->name,
            'qtt' => 1
          ];
        }
      }
      if ( !empty($server_config->disk7) ) {
        $check = true;
        foreach ($data_config as $key => $data) {
          if ($data['id'] == $server_config->disk7) {
            $data_config[$key]['qtt'] = $data['qtt'] + 1;
            $check = false;
          }
        }
        if ( $check ) {
          $data_config[] = [
            'id' => $server_config->disk7,
            'name' => $server_config->product_disk7->name,
            'qtt' => 1
          ];
        }
      }
      if ( !empty($server_config->disk8) ) {
        $check = true;
        foreach ($data_config as $key => $data) {
          if ($data['id'] == $server_config->disk8) {
            $data_config[$key]['qtt'] = $data['qtt'] + 1;
            $check = false;
          }
        }
        if ( $check ) {
          $data_config[] = [
            'id' => $server_config->disk8,
            'name' => $server_config->product_disk8->name,
            'qtt' => 1
          ];
        }
      }
      if ( !empty($server_config->ram) ) {
        $add_ram = 0;
        foreach ($server->server_config_rams as $server_config_ram) {
          $add_ram += !empty($server_config_ram->product->meta_product->memory) ? $server_config_ram->product->meta_product->memory : 0;
        }
        $text_config .= 'Addon RAM: '. $add_ram .' GB RAM <br>';
      }
      if ( count($data_config) ) {
        $text_config .= 'Addon Disk (';
        foreach ($data_config as $key => $data) {
          $text_config .= ( $data['qtt'] . ' &#215; ' . $data['name']  );
          if ( count( $data_config ) - 1 > $key ) {
            $text_config .= ' - ';
          }
        }
        $text_config .= ')';
      }
      if ( !empty($server_config->ip) ) {
        $text_config .= '<br>';
        $add_ram = 0;
        foreach ($server->server_config_ips as $server_config_ip) {
          $add_ram += !empty($server_config_ip->product->meta_product->ip) ? $server_config_ip->product->meta_product->ip : 0;
        }
        $text_config .= 'Addon IP: '. $add_ram .' IP Public';
      }
      return $text_config;
    } else {
      return '';
    }
  }

  public function listServer($request)
  {
    $data = [];
    $data['data'] = [];
    $status_vps = config('status_vps'); // get các loại status lên
    $billings = config('billing'); // get billing lên
    $qtt = !empty($request['qtt']) ? $request['qtt'] : 30; // nếu qtt trống thì mặc định là 30
    if (!empty($request['status'])) {
      // nếu trạng thái đc chọn thì get list server theo trạng thái
      $listServer = $this->server->where('status_server', $request['status'])->where('ip', 'like', '%' . $request['q'] . '%')
        ->orderBy('id', 'desc')->paginate($qtt);
    } else {
      // ngược lại get list server bất kì trạng thái nào
      if ( !empty($request['q']) ) {
        $listServer = $this->server->where('status_server', '!=', null)->where('ip', 'like', '%' . $request['q'] . '%')
        ->orderBy('id', 'desc')->paginate($qtt);
      } else {
        $listServer = $this->server->orderBy('id', 'desc')->paginate($qtt);
      }
    }

    foreach ($listServer as $key => $server) {
      // khách hàng
      $server->text_user_name = !empty($server->user->name) ? $server->user->name : '<span class="text-danger">Đã xóa</span>';
      // sản phẩm
      $product = $server->product;
      if (!empty($server->product)) {
        if (!empty($server->product->duplicate)) {
          $server->text_product = '<a href="' . route('admin.product.edit', $server->product->id) . '">' . $server->product->name . '</a>';
        } else {
          if (!empty($server->product->group_product->private)) {
            $server->text_product = '<a href="' . route('admin.product_private.editPrivate', [$server->product->group_product->id, $server->product->id]) . '">' . $proxy->product->name . '</a>';
          } else {
            $server->text_product = '<a href="' . route('admin.product.edit', $server->product->id) . '">' . $server->product->name . '</a>';
          }
        }
      } else {
        $server->text_product = '<span class="text-danger">Đã xóa</span>';
      }

      // Cấu hình
      if ( !empty($server->config_text) ) {
        $server->specs = $server->config_text;
      } else {
        $product = $server->product;
        $cpu = !empty($product->meta_product->cpu) ? $product->meta_product->cpu : 0;
        $cores = !empty($product->meta_product->cores) ? $product->meta_product->cores : 0;
        $ram = !empty($product->meta_product->memory) ? $product->meta_product->memory : 0;
        $disk = !empty($product->meta_product->disk) ? $product->meta_product->disk : 0;
        $server->specs = "$cpu ($cores), $ram, $disk ($server->raid)";
      }
      
      $addonConfig = '';
      $addonConfig = $this->get_config_server($server->id);
      $server->specs .= "<br>HĐH: $server->os <br> $addonConfig";

      // ngày tạo
      $server->date_create = date('H:i:m d-m-Y', strtotime($server->created_at));
      // ngày kết thúc
      $server->text_next_due_date = date('d-m-Y', strtotime($server->next_due_date));
      // tổng thời gian thuê 
      $total_time = '';
      $create_date = new Carbon($server->created_at);
      $next_due_date = new Carbon($server->next_due_date);
      if ($next_due_date->diffInYears($create_date)) {
        $year = $next_due_date->diffInYears($create_date);
        $total_time = $year . ' Năm ';
        $create_date = $create_date->addYears($year);
        $month = $next_due_date->diffInMonths($create_date);
        //dd($create_date, $next_due_date, $next_due_date->diffInMonths($create_date));
        if ($create_date != $next_due_date) {
          $total_time .= ($month + 1) . ' Tháng';
        }
      } else {
        $diff_month = $next_due_date->diffInMonths($create_date);
        if ($create_date != $next_due_date) {
          $total_time = ($diff_month + 1) . ' Tháng';
        } else {
          $total_time = $diff_month . ' Tháng';
        }
      }
      $server->total_time = $total_time;
      // chu kỳ thanh toán
      $server->text_billing_cycle = !empty($billings[$server->billing_cycle]) ? $billings[$server->billing_cycle] : '1 Tháng';
      // chi phí
      if (!empty($server->amount)) {
        $total = $server->amount;
      } else {
        $total = 0;
        $product = $server->product;
        $total = !empty( $product->pricing[$server->billing_cycle] ) ? $product->pricing[$server->billing_cycle] : 0;
        if (!empty($server->server_config)) {
           $server_config = $server->server_config;
           $pricing_addon = 0;
           if ( !empty( $server_config->ram ) ) {
               foreach ($server->server_config_rams as $server_config_ram) {
                   $pricing_addon += !empty($server_config_ram->product->pricing[$server->billing_cycle]) ? $server_config_ram->product->pricing[$server->billing_cycle] : 0;
               }
           }
           if ( !empty( $server_config->ip ) ) {
               foreach ($server->server_config_ips as $server_config_ip) {
                   $pricing_addon += !empty($server_config_ip->product->pricing[$server->billing_cycle]) ? $server_config_ip->product->pricing[$server->billing_cycle] : 0;
               }
           }
           if ( !empty($server_config->disk2) ) {
               $pricing_addon += $server_config->product_disk2->pricing[$server->billing_cycle];
           }
           if ( !empty($server_config->disk3) ) {
               $pricing_addon += $server_config->product_disk3->pricing[$server->billing_cycle];
           }
           if ( !empty($server_config->disk4) ) {
               $pricing_addon += $server_config->product_disk4->pricing[$server->billing_cycle];
           }
           if ( !empty($server_config->disk5) ) {
               $pricing_addon += $server_config->product_disk5->pricing[$server->billing_cycle];
           }
           if ( !empty($server_config->disk6) ) {
               $pricing_addon += $server_config->product_disk6->pricing[$server->billing_cycle];
           }
           if ( !empty($server_config->disk7) ) {
               $pricing_addon += $server_config->product_disk7->pricing[$server->billing_cycle];
           }
           if ( !empty($server_config->disk8) ) {
               $pricing_addon += $server_config->product_disk8->pricing[$server->billing_cycle];
           }
           $total += $pricing_addon;
        }
      }
      $server->total = number_format($total, 0, ",", ".");
      // trạng thái
      if ($server->status != 'Pending') {
        if ($server->status_server == 'on') {
          $server->text_status = '<span class="text-success">Đang bật</span>';
        } elseif ($server->status_server == 'off') {
          $server->text_status = '<span class="text-danger">Đã tắt</span>';
        } elseif ($server->status_server == 'progressing') {
          $server->text_status = '<span class="text-info">Đang tạo</span>';
        } elseif ($server->status_server == 'expire') {
          $server->text_status = '<span class="text-danger">Hết hạn</span>';
        } elseif ($server->status_server == 'reset_password') {
          $server->text_status = '<span class="text-primary">Đang đổi mật khâu</span>';
        } elseif ($server->status_server == 'cancel') {
          $server->text_status = '<span class="text-danger">Đã hủy</span>';
        } elseif ($server->status_server == 'delete') {
          $server->text_status = '<span class="text-danger">Đã xóa</span>';
        } elseif ($server->status_server == 'change_user') {
          $server->text_status = '<span class="text-danger">Đã chuyển</span>';
        } else {
          $server->text_status = '<span class="text-danger">Trạng thái lỗi!</span>';
        }
      } else {
        $server->text_status = '<span class="text-danger">Chưa thanh toán</span>';
      }
      $data['data'][] = $server;
    }
    $data['total'] = $listServer->total();
    $data['perPage'] = $listServer->perPage();
    $data['current_page'] = $listServer->currentPage();
    return $data;
  }
}
