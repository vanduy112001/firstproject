<?php

namespace App\Repositories\Admin;

use App\Model\User;
use App\Model\ServerHosting;
use App\Factories\AdminFactories;
use App\Services\DirectAdmin;
use App\Model\LogActivity;
use Illuminate\Support\Facades\Auth;

class ServerHostingRepositories
{

    protected $user;
    protected $server_hosting;
    protected $da;
    protected $log_activity;

    public function __construct()
    {
        $this->user = new User;
        $this->server_hosting = new ServerHosting;
        $this->da = AdminFactories::directAdminRepositories();
        $this->log_activity = new LogActivity();
    }

    public function getServerHosting()
    {
        $server_hostings = $this->server_hosting->where('location', 'vn')->orderBy('id', 'desc')->paginate(20);
        return $server_hostings;
    }

    public function list_server_hosting_vn()
    {
        $server_hostings = $this->server_hosting->where('location', 'vn')->orderBy('id', 'desc')->get();
        return $server_hostings;
    }

    public function getServerHostingSingapore()
    {
        $server_hostings = $this->server_hosting->where('location', 'si')->orderBy('id', 'desc')->paginate(20);
        return $server_hostings;
    }

    public function check_connection($data)
    {
        $type  = $data['type'];
        if ($type == 'DirectAdmin') {
          $host_connect = 'https://' . $data['host'] . ':' . $data['post'];
          $da = new DirectAdmin($host_connect, $data['user_name'], $data['password']);
          $result = $da->query("CMD_API_PACKAGES_USER");
          if (is_array($result)) {
            return true;
          } else {
            return false;
          }
        }
    }

    public function create_server_hosting($data)
    {
        $data_create = [
            'ip' => $data['ip'],
            'host' => $data['host'],
            'user' => $data['user_name'],
            'password' => $data['password'],
            'port' => $data['port'],
            'type_server_hosting' => $data['type_server_hosting'],
            'name' => $data['name'],
            'location' => !empty($data['location']) ? $data['location'] : 'vn',
        ];
        $create = $this->server_hosting->create($data_create);
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tạo',
          'model' => 'Admin/ServerHosting',
          'description' => ' server hosting <a target="_blank" href="/admin/hostings/get-hosting-daportal/'. $create->id .'">' . $create->host . '</a>',
        ];
        $this->log_activity->create($data_log);
        return $create;
    }

    public function detail($id)
    {
        return $this->server_hosting->find($id);
    }

    public function update_server_hosting($data)
    {
        $data_create = [
            'ip' => $data['ip'],
            'host' => $data['host'],
            'user' => $data['user_name'],
            'password' => $data['password'],
            'port' => $data['port'],
            'type_server_hosting' => $data['type_server_hosting'],
            'name' => $data['name'],
            'location' => !empty($data['location']) ? $data['location'] : 'vn',
        ];
        $server_hosting = $this->server_hosting->find($data['id']);
        $update = $server_hosting->update($data_create);
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'chỉnh sửa',
          'model' => 'Admin/ServerHosting',
          'description' => ' server hosting <a target="_blank" href="/admin/hostings/get-hosting-daportal/'. $server_hosting->id .'">' . $server_hosting->host . '</a>',
        ];
        $this->log_activity->create($data_log);
        return $update;
    }

    public function delete($id)
    {
        $server_hosting = $this->server_hosting->find($id);
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'xóa',
          'model' => 'Admin/ServerHosting',
          'description' => ' server hosting ' .  $server_hosting->host,
        ];
        $this->log_activity->create($data_log);
        return $server_hosting->delete();
    }

    public function active($id)
    {
        if (!empty($id)) {
            $server_hostings = $this->server_hosting->where('id' , '!=', $id)->get();
            if (!empty($server_hostings)) {
              foreach ($server_hostings as $key => $server_hosting) {
                  $server_hosting->active == false;
                  $server_hosting->save();
              }
            }
            $detail = $this->server_hosting->find($id);
            // ghi log
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'kích hoạt',
              'model' => 'Admin/ServerHosting',
              'description' => ' server hosting <a target="_blank" href="/admin/hostings/get-hosting-daportal/'. $detail->id .'">' . $detail->host . '</a>',
            ];
            $this->log_activity->create($data_log);

            $detail->active = true;
            $active = $detail->save();
            return $active;
        } else {
            $server_hostings = $this->server_hosting->get();
            foreach ($server_hostings as $key => $server_hosting) {
                $server_hosting->active = false;
                $server_hosting->save();
            }
            // ghi log
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'kích hoạt',
              'model' => 'Admin/ServerHosting',
              'description' => ' server hosting mặc định',
            ];
            $this->log_activity->create($data_log);
            return true;
        }
    }

    public function deactive($id)
    {
        $detail = $this->server_hosting->find($id);
        $detail->active = false;
        $active = $detail->save();
        return true;
    }

}
