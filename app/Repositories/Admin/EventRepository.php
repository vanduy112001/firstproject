<?php
namespace App\Repositories\Admin;

use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\MetaProduct;
use App\Model\Pricing;
use App\Model\MetaGroupProduct;
use App\Model\User;
use App\Model\EventPromotion;
use App\Model\LogActivity;
use Illuminate\Support\Facades\Auth;

class EventRepository {

    protected $group_product;
    protected $product;
    protected $meta_product;
    protected $meta_group_product;
    protected $pricing;
    protected $user;
    protected $product_point;
    protected $event;
    protected $log_activity;

    public function __construct()
    {
        $this->group_product = new GroupProduct;
        $this->product = new Product;
        $this->meta_product = new MetaProduct;
        $this->meta_group_product = new MetaGroupProduct;
        $this->pricing = new Pricing;
        $this->user = new User;
        $this->event = new EventPromotion;
        $this->log_activity = new LogActivity();
    }

    public function list_event_with_index() {
        $events = $this->event->orderBy('id', 'desc')->with('product')->get();
        return $events;
    }

    public function list_event($select)
    {
        if (!empty($select)) {
            $events = $this->event->where('product_id', $select)->orderBy('id', 'desc')->with('product')->paginate(30);
        } else {
            $events = $this->event->with('product')->orderBy('id', 'desc')->paginate(30);
        }
        $billing = config('billing');
        foreach ($events as $key => $event) {
            $product = $event->product;
            $event->name =  $product->name . ' - ' . $product->group_product->name;
            $event->billing_cycle = $billing[$event->billing_cycle];
        }
        return $events;
    }

    public function create($data)
    {
       $event  = $this->event->where('product_id', $data['product_id'])->where('billing_cycle', $data['billing_cycle'])->first();
       if ( isset($event) ) {
          return 2;
       } else {
           $data_event = [
              'product_id' => $data['product_id'],
              'billing_cycle' => $data['billing_cycle'],
              'point' => $data['point'],
           ];
           $create = $this->event->create($data_event);
           // ghi log
           $data_log = [
             'user_id' => Auth::user()->id,
             'action' => 'tạo',
             'model' => 'Admin/Event',
             'description' => ' Event ' . $create->id,
           ];
           $this->log_activity->create($data_log);
           if ($create) {
              return 1;
           } else {
             return 0;
           }
       }
    }

    public function detail($id)
    {
        return $this->event->find($id);
    }

    public function update($data)
    {
        $event = $this->event->find($data['event_id']);
        if ( $event->product_id == $data['product_id'] ) {
           $update = $event->update($data);
           // ghi log
           $data_log = [
             'user_id' => Auth::user()->id,
             'action' => 'chỉnh sửa',
             'model' => 'Admin/Event',
             'description' => ' Event #'. $event->id . ' thời gian ' . $data['billing_cycle'] . ' Điểm ' . $data['point'],
           ];
           $this->log_activity->create($data_log);
        } else {
           $check_event  = $this->event->where('product_id', $data['product_id'])->where('billing_cycle', $data['billing_cycle'])->first();
           if ( isset($event) ) {
              return 2;
           } else {
              $update = $event->update($data);
           }
        }
        if ($update) {
           return 1;
        } else {
          return 0;
        }
    }

    public function delete($id)
    {
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'xóa',
          'model' => 'Admin/Event',
          'description' => ' Event ' . $id,
        ];
        $this->log_activity->create($data_log);
        return $this->event->find($id)->delete();
    }

}

?>
