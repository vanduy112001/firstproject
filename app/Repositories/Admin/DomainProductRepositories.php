<?php

namespace App\Repositories\Admin;

use App\Model\DomainProduct;
use App\Model\LogActivity;
use Illuminate\Support\Facades\Auth;

class DomainProductRepositories
{
  protected $model;
  protected $log_activity;

  public function __construct()
  {
      $this->model = new DomainProduct();
      $this->log_activity = new LogActivity();
  }

  public function all($params = [])
  {
    $query = $this->model;
    if (!empty($params['keyword'])) {
      $query = $query->where(function ($query) use ($params) {
        $query->orWhere('type', 'LIKE', '%' . $params['keyword'] . '%');
      });
    }
    $result = $query->orderBy('id', 'desc')->paginate(10);
    return $result;
  }

  public function create($data)
  {
    $infoDomain = $this->model->create($data);
    // ghi log
    $data_log = [
      'user_id' => Auth::user()->id,
      'action' => 'tạo',
      'model' => 'Admin/DomainProduct',
      'description' => ' sản phẩm domain <a target="_blank" href="/admin/domain-products/edit/'. $infoDomain->id .'">'. $infoDomain->type .'</a>',
    ];
    $this->log_activity->create($data_log);
    if ($infoDomain) {
      return true;
    } else {
      return false;
    }
  }

  public function delete($id)
  {
    try {
      $result = [
        'status' => false,
        'message' => 'Có lỗi xảy ra, vui lòng thử lại.',
      ];
      $product = $this->model->find($id);
      // ghi log
      $data_log = [
        'user_id' => Auth::user()->id,
        'action' => 'Xóa',
        'model' => 'Admin/DomainProduct',
        'description' => ' sản phẩm domain ' . $product->title,
      ];
      $this->log_activity->create($data_log);
      // Xóa tất cả các table có liên quan trên user
      $delete = $this->model->where('id', $id)->delete();

      if ($delete) {
        $result = [
          'status' => true,
          'message' => 'Xóa thành công.',
        ];
      }
      return $result;
    } catch (\Exception $exception) {
      return [
        'status' => false,
        'message' => $exception->getMessage(),
      ];
    }
  }

  public function multidelete($id)
  {
    try {
      $result = [
        'status' => false,
        'message' => 'Có lỗi xảy ra, vui lòng thử lại.',
      ];

      $delete = $this->model->whereIn('id', $id)->delete();

      if ($delete) {
        $result = [
          'status' => true,
          'message' => 'Xóa thành công.',
        ];
      }
      return $result;
    } catch (\Exception $exception) {
      return [
        'status' => false,
        'message' => $exception->getMessage(),
      ];
    }
  }

  public function detail($id)
  {
    return $infoDomain = $this->model->find($id);
  }

  public function update($id, $data)
  {
    $infoDomain = $this->model->where('id', $id)->update($data);
    $product = $this->model->find($id);
    // ghi log
    $data_log = [
      'user_id' => Auth::user()->id,
      'action' => 'chỉnh sửa',
      'model' => 'Admin/DomainProduct',
      'description' => ' sản phẩm domain <a target="_blank" href="/admin/domain-products/edit/'. $product->id .'">'. $product->type .'</a>',
    ];
    $this->log_activity->create($data_log);
    if ($infoDomain) {
      return true;
    } else {
      return false;
    }
  }
  public function get_domain_product_by_ext($ext)
  {
    return $this->model->where('type', $ext)->first();
  }

}
