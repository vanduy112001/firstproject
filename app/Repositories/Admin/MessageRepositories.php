<?php

namespace App\Repositories\Admin;

use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\User;
use App\Model\UserMeta;
use App\Model\GroupUser;
use App\Model\LogActivity;
use App\Model\Message;
use App\Model\MessageMeta;
use Illuminate\Support\Facades\Auth;

use Carbon\Carbon;

class MessageRepositories
{

    protected $group_product;
    protected $product;
    protected $user;
    protected $user_meta;
    protected $group_user;
    protected $domain;
    protected $domain_product;
    protected $log;
    protected $message;
    protected $message_meta;

    public function __construct()
    {
        $this->group_product = new GroupProduct;
        $this->product = new Product;
        $this->log = new LogActivity;
        $this->message = new Message;
        $this->message_meta = new MessageMeta;
    }

    public function list_message()
    {
        $messages = $this->message->orderByRaw('updated_at desc')->paginate(20);
        Carbon::setLocale('vi');
        $now = Carbon::now();
        if ( $messages->count() > 0 ) {
            foreach ($messages as $key => $message) {
                $message->content = '';
                $message->diffForHumans = $message->updated_at->diffForHumans($now);
                if ( count($message->message_metas) > 0 ) {
                    $n =  count($message->message_metas) - 1;
                    $message->content = $message->message_metas[$n]->content;
                    $message->status_meta = $message->message_metas[$n]->status;
                    $message->qtt_pending = $message->message_metas->where('status', 'pending')->count();
                }
            }
        }
        return $messages;
    }

    public function loadMessageByUser($user_id)
    {
        $message = $this->message->where('user_id', $user_id)->first();
        $data = [];
        if ( !isset( $message ) ) {
            $this->message->create([
                'user_id' => $user_id,
                'status' => 'active'
            ]);
            $data = [
                'error' => 2,
                'content' => null,
            ];
        }
        else {
            $data['error'] = 0;
            $message->status = 'active';
            $message->save();
            $data['avatar'] = !empty($message->user->user_meta->avatar) ? $message->user->user_meta->avatar : url('/images/user2-160x160.jpg');
            $data['user_name'] = !empty($message->user->name) ? $message->user->name : 'Đã xóa';
            $data['user_email'] = !empty($message->user->email) ? $message->user->email : 'Đã xóa';
            // lấy list message
            $list_message_metas = $this->message_meta->where('message_id', $message->id)->with('user')->orderBy('id', 'desc')->paginate(20);
            foreach ($list_message_metas as $key => $message_meta) {
                $message_meta->avatar = !empty($message_meta->user->user_meta->avatar) ? $message_meta->user->user_meta->avatar : url('/images/user2-160x160.jpg');
                $message_meta->timeSend = date('H:i', strtotime($message_meta->created_at));
                $message_meta->dateSend = date('d-m-Y', strtotime($message_meta->created_at));
            }
            $data['content'] = $list_message_metas;
            // đổi trạng thái cho message meta
            $list_message_metas_status_peding = $this->message_meta->where('message_id', $message->id)->where('status', 'pending')->get();
            foreach ($list_message_metas_status_peding as $key => $message_meta_status) {
                $message_meta_status->status = 'active';
                $message_meta_status->save();
            }
        }
        return $data;
    }


    public function sendMessage($data)
    {
        try {
            $message = $this->message->where('user_id', $data['msgUserId'])->first();
            if ( !isset( $message ) ) {
                $data_message = [
                    "user_id" => $data['msgUserId'],
                    "status" => 'pending'
                ];
                $message = $this->message->create($data_message);
            } 

            $data_message_meta = [
                "message_id" => $message->id,
                "content" => $data['msgText'],
                "user_id" => Auth::id(),
                "status" => 'pending'
            ];

            $this->message_meta->create($data_message_meta);
            // event
            return [
                "error" => 0
            ];
        } catch (Exception $e) {
            report($e);
            return [
                "error" => 1
            ];
        }
    }

    public function userChat($data_event)
    {
        try {
            $message_meta = $this->message_meta->where('user_id', $data_event->user_id)->where('id', $data_event->message_meta_id)->with('user')->first();
            Carbon::setLocale('vi');
            $now = Carbon::now();

            if ( isset( $message_meta ) ) {
            
                $message_meta->timeSend = date('H:i', strtotime($message_meta->created_at));
                $message_meta->dateSend = date('d-m-Y', strtotime($message_meta->created_at));
                $message_meta->diffForHumans = $message_meta->created_at->diffForHumans($now);
                $message_meta->avatar = !empty($message_meta->user->user_meta->avatar) ? $message_meta->user->user_meta->avatar : url('/images/user2-160x160.jpg');
                $message_meta->user_name = !empty($message_meta->user->name) ? $message_meta->user->name : 'Đã xóa';
                return [
                    "error" => 0,
                    "content" => $message_meta
                ];

            } else {
                return [
                    "error" => 3,
                    "content" => 'không có tin nhắn'
                ];
            }

        } catch (Exception $e) {
            report($e);
            return [
                "error" => 2,
                "content" => 'lỗi hệ thống'
            ];
        }
    }


}
