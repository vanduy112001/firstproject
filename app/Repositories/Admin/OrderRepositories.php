<?php

namespace App\Repositories\Admin;

use App\Model\User;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\Server;
use App\Model\Hosting;
use App\Model\Email;
use App\Model\Product;
use App\Model\VpsConfig;
use App\Model\Credit;
use App\Model\ServerConfig;
use App\Model\GroupProduct;
use App\Model\HistoryPay;
use App\Model\TotalPrice;
use App\Model\OrderAddonVps;
use App\Model\OrderAddonServer;
use App\Model\ProductUpgrate;
use App\Model\OrderUpgradeHosting;
use App\Model\OrderChangeVps;
use App\Model\OrderExpired;
use App\Model\Colocation;
use App\Model\EmailHosting;
use App\Model\LogPayment;
use App\Model\SendMail;
use Mail;
use App\Factories\AdminFactories;
use App\Http\Controllers\Admin\DirectAdminController;
use Carbon\Carbon;
use App\Model\LogActivity;
use Illuminate\Support\Facades\Auth;

class OrderRepositories {

    protected $user;
    protected $order;
    protected $detail_order;
    protected $vps;
    protected $server;
    protected $hosting;
    protected $email;
    protected $user_email;
    protected $subject;
    protected $product;
    protected $history_pay;
    protected $vps_config;
    protected $server_config;
    protected $total_price;
    protected $group_product;
    protected $order_addon_vps;
    protected $order_addon_server;
    protected $product_upgrate;
    protected $order_upgrade_hosting;
    protected $order_change_ip;
    protected $log_activity;
    protected $order_expired;
    protected $colocation;
    protected $email_hosting;
    protected $credit;
    protected $log_payment;
    protected $send_mail;
    // API
    protected $da;
    protected $da_si;
    protected $dashboard;

    public function __construct()
    {
        $this->user = new User;
        $this->order = new Order;
        $this->detail_order = new DetailOrder;
        $this->vps = new Vps;
        $this->vps_config = new VpsConfig;
        $this->server_config = new ServerConfig;
        $this->server = new Server;
        $this->hosting = new Hosting;
        $this->email_hosting = new EmailHosting;
        $this->email = new Email;
        $this->product = new Product;
        $this->credit = new Credit;
        $this->history_pay = new HistoryPay;
        $this->total_price = new TotalPrice;
        $this->group_product = new GroupProduct;
        $this->order_addon_vps = new OrderAddonVps;
        $this->order_addon_server = new OrderAddonServer;
        $this->product_upgrate = new ProductUpgrate;
        $this->order_upgrade_hosting = new OrderUpgradeHosting;
        $this->order_change_ip = new OrderChangeVps;
        $this->log_activity = new LogActivity();
        $this->order_expired = new OrderExpired;
        $this->colocation = new Colocation;
        $this->log_payment = new LogPayment;
        $this->send_mail = new SendMail;
        $this->dashboard = AdminFactories::dashBoardRepositories();
        $this->da = AdminFactories::directAdminRepositories();
        $this->da_si = AdminFactories::directAdminSingaporeRepositories();
    }

    public function list_orders()
    {
        return $this->order->orderBy('id', 'desc')->with('user', 'detail_orders')->paginate('30');
    }

    public function total_order_pending()
    {
        return $this->order->where('status', 'Pending')->count();
    }
    public function total_order_active()
    {
        return $this->order->where('status', 'Active')->count();
    }
    // VPS hết hạn
    public function total_vps_termination()
    {
        $list_vps = $this->vps->get();
        $n = 0;
        foreach ($list_vps as $key => $vps) {
          $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
          $next_due_date = new Carbon($vps->next_due_date);
          if ( $next_due_date->diffInDays($now) < 0 ) {
             $n++;
          }
        }
        return $n;
    }
    // VPS  Sắp hết hạn
    public function total_vps_before_termination()
    {
        $list_vps = $this->vps->where('status',  'Active')
                ->where('location', 'cloudzone')
                ->where(function($query)
                {
                    return $query
                      ->whereIn('status_vps', [ 'on', 'off', 'expire' ]);
                })->orderBy('id', 'desc')->get();
        $total = 0;
        $status_vps = config('status_vps');
        $billing = config('billing');
        if ( $list_vps->count() > 0 ) {
            foreach ($list_vps as $key => $vps) {
              if (!empty($vps->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $next_due_date = new Carbon($vps->next_due_date);
                if ($next_due_date->isPast()) {
                  $total++;
                }
                elseif ( $next_due_date->diffInDays($now) <= 7 ) {
                  $total++;
                }
              }

            }
        }
        return $total;
    }
    // VPS US Sắp hết hạn
    public function total_vps_us_before_termination()
    {
        $list_vps = $this->vps->where('status',  'Active')
                ->where('location', 'us')
                ->where(function($query)
                {
                    return $query
                      ->whereIn('status_vps', [ 'on', 'off', 'expire' ]);
                })->orderBy('id', 'desc')->get();
        $total = 0;
        $status_vps = config('status_vps');
        $billing = config('billing');
        if ( $list_vps->count() > 0 ) {
            foreach ($list_vps as $key => $vps) {
              if (!empty($vps->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $next_due_date = new Carbon($vps->next_due_date);
                if ($next_due_date->isPast()) {
                  $total++;
                }
                elseif ( $next_due_date->diffInDays($now) <= 7 ) {
                  $total++;
                }
              }

            }
        }
        return $total;
    }
    // Server Sắp hết hạn
    public function total_server_before_termination()
    {
        $list_vps = $this->server->where('status',  'Active')
                ->where(function($query)
                {
                    return $query
                      ->whereIn('status_server', [ 'on', 'off', 'expire' ]);
                })->orderBy('id', 'desc')->get();
        $total = 0;
        if ( $list_vps->count() > 0 ) {
            foreach ($list_vps as $key => $vps) {
              if (!empty($vps->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $next_due_date = new Carbon($vps->next_due_date);
                if ($next_due_date->isPast()) {
                  $total++;
                }
                elseif ( $next_due_date->diffInDays($now) <= 7 ) {
                  $total++;
                }
              }

            }
        }
        return $total;
    }
    // Colocation Sắp hết hạn
    public function total_colo_before_termination()
    {
        $list_vps = $this->colocation->where('status',  'Active')
                ->where(function($query)
                {
                    return $query
                    ->whereIn('status_colo', [ 'on', 'off', 'expire' ]);
                })->orderBy('id', 'desc')->get();
        $total = 0;
        $status_vps = config('status_vps');
        $billing = config('billing');
        if ( $list_vps->count() > 0 ) {
            foreach ($list_vps as $key => $vps) {
              if (!empty($vps->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $next_due_date = new Carbon($vps->next_due_date);
                if ($next_due_date->isPast()) {
                  $total++;
                }
                elseif ( $next_due_date->diffInDays($now) <= 7 ) {
                  $total++;
                }
              }

            }
        }
        return $total;
    }
    // Hositng sắp hết hạn
    public function total_hosting_before_termination()
    {
        $list_hosting = $this->hosting->where('status',  'Active')
                ->where(function($query)
                {
                    return $query
                    ->whereIn('status_hosting', [ 'on', 'off', 'expire' ]);
                })->orderBy('id', 'desc')->get();
        $n = 0;
        $status_vps = config('status_vps');
        $billing = config('billing');
        if ( $list_hosting->count() > 0 ) {
            foreach ($list_hosting as $key => $hosting) {
              $check = false;
              if (!empty($hosting->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $next_due_date = new Carbon($hosting->next_due_date);
                if ($next_due_date->isPast()) {
                  $n++;
                }
                elseif ( $next_due_date->diffInDays($now) <= 15 ) {
                  $n++;
                }
              }

            }
        }
        return $n;
    }
    // Email Hositng sắp hết hạn
    public function total_email_hosting_termination()
    {
        $list_hosting = $this->email_hosting->where('status',  'Active')
                ->where(function($query)
                {
                    return $query
                    ->whereIn('status_hosting', [ 'on', 'off', 'expire' ]);
                })->orderBy('id', 'desc')->get();
        $n = 0;
        $status_vps = config('status_vps');
        $billing = config('billing');
        if ( $list_hosting->count() > 0 ) {
            foreach ($list_hosting as $key => $hosting) {
              $check = false;
              if (!empty($hosting->next_due_date)) {
                $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                $next_due_date = new Carbon($hosting->next_due_date);
                if ($next_due_date->isPast()) {
                  $n++;
                }
                elseif ( $next_due_date->diffInDays($now) <= 15 ) {
                  $n++;
                }
              }

            }
        }
        return $n;
    }
    // Chưa thanh toán
    public function total_order_unpain()
    {
      $detail_orders = $this->detail_order->where('status', 'unpaid')->count();
      return $detail_orders;
    }

    public function users()
    {
        $users = $this->user->get();
        return $users;
    }

    public function create_order_vps($data)
    {
        $product = $this->product->find($data['product_id']);
        $add_on = !empty($data['addon'])? 1 : '';
        $status = $data['status'];
        if (!empty($data['total_price_override'])) {
            $total = $data['total_price_override'];
            $sub_total = $total / $data['quantity'];
        }
        else {
            $sub_total = $product->pricing[$data['billing_cycle']];
            if (!empty($data['check_price_override'])) {
                $total = $data['total_price_override'];
            } else {
                $total = $product->pricing[$data['billing_cycle']] * $data['quantity'];
                if (!empty($add_on)) {
                    $product_addon_cpu = $this->product->find($data['id-addon-cpu']);
                    $product_addon_ram = $this->product->find($data['id-addon-ram']);
                    $product_addon_disk = $this->product->find($data['id-addon-disk']);
                    if ( $data['addon_cpu'] == 0  && $data['addon_ram'] == 0 && $data['addon_disk'] ==0 ) {
                      $add_on = 0;
                    }
                    // TInh total va subtotal
                    if ($data['addon_cpu'] > 0) {
                        $sub_total += $product_addon_cpu->pricing[$data['billing_cycle']] * $data['addon_cpu'];
                        $total += $product_addon_cpu->pricing[$data['billing_cycle']] * $data['addon_cpu'] * $data['quantity'];
                    }
                    // dd($sub_total, $total,$data['addon_ram']);
                    if ($data['addon_ram'] > 0) {
                        $sub_total += $product_addon_ram->pricing[$data['billing_cycle']] * $data['addon_ram'];
                        $total += $product_addon_ram->pricing[$data['billing_cycle']] * $data['addon_ram'] * $data['quantity'];
                    }
                    // dd($sub_total, $total,$data['addon_ram']);
                    if ($data['addon_disk'] > 0) {
                        $sub_total += $product_addon_disk->pricing[$data['billing_cycle']] * ($data['addon_disk']/10);
                        $total += $product_addon_disk->pricing[$data['billing_cycle']] * ($data['addon_disk']/10) * $data['quantity'];
                    }
                    // dd($sub_total, $total,$data['addon_disk']/10);
                }
            }
        }
        // tao order
        $data_order = [
            'user_id' => $data['user_id'],
            'promotion_id' => $data['promotion_id'],
            'total' => $total,
            'status' => $data['status'],
            'description' => !empty($data['description'])? $data['description'] : '',
            'verify' => true,
        ];
        $create_order = $this->order->create($data_order);
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tạo',
          'model' => 'Admin/Order',
          'description' => ' Order VPS #' . $create_order->id,
        ];
        $this->log_activity->create($data_log);
        // $create_order = 1;
        // ket thuc tao order
        // tao order detail
        if ($status == 'Active' || $status == 'Finish') {
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $date = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date . $billing[$data['billing_cycle']] ));
            $data_order_detail = [
                'order_id' => $create_order->id,
                'type' => $data['type_products'],
                'due_date' => $due_date,
                'description' => !empty($data['description'])? $data['description'] : '',
                'payment_method' => 1,
                'status' => 'paid',
                'sub_total' => $data['sub_total'],
                'quantity' => $data['quantity'],
                'user_id' => $data['user_id'],
                'paid_date' => date('Y-m-d'),
                'addon_id' => !empty($data['addon'])? '1' : '0',
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            $user = $this->user->find($data['user_id']);
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDODV' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '2',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Active',
                'method_gd_invoice' => 'credit',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            // ket thuc tao order detail
            $qtt = $data['quantity'];
            $list_vps = [];
            // Tao order VPS
            for ($i=0; $i < $qtt; $i++) {
                $data_vps = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' => $data['user_id'],
                    'product_id' => $data['product_id'],
                    'next_due_date' => $due_date,
                    'ip' => !empty($data['ip'])? $data['ip'] : '',
                    'os' => $data['os'],
                    'billing_cycle' => $data['billing_cycle'],
                    'status' => 'Active',
                    'date_create' => date('Y-m-d'),
                    'user' => !empty($data['user'])? $data['user'] : '',
                    'password' => !empty($data['password'])? $data['password'] : '',
                    'paid' => 'paid',
                    'addon_id' => !empty($data['addon'])? $data['addon'] : '0',
                    'security' => !empty($data['check_security']) ? true : false,
                    'price_override' => !empty($data['check_price_override']) ? $data['total_price_override'] : '',
                    'type_vps' => ($data['type_products'] == 'NAT-VPS') ? 'nat_vps' : 'vps',
                ];
                $create_vps = $this->vps->create($data_vps);
                $list_vps[] = $create_vps;
                if (!empty($data['addon'])) {
                    if ($data['addon_ram'] > 0 || $data['addon_cpu'] > 0 || $data['addon_disk'] > 0  ) {
                        $data_vps_config = [
                            'vps_id' => $create_vps->id,
                            'ip' => '0',
                            'cpu' => !empty($data['addon_cpu']) ? $data['addon_cpu'] : 0,
                            'ram' => !empty($data['addon_ram']) ? $data['addon_ram'] : 0,
                            'disk' => !empty($data['addon_disk']) ? $data['addon_disk'] : 0,
                        ];
                        $this->vps_config->create($data_vps_config);
                    }
                }
            }
            // ghi vào tổng thu
            if (!empty($this->total_price->first())) {
                $total_price = $this->total_price->first();
                $total_price->vps += $total;
                $total_price->save();
            } else {
                $data_total_price = [
                    'vps' => $total,
                    'hosting' => '0',
                    'server' => '0',
                ];
                $this->total_price->create($data_total_price);
            }
            if ($data['type_products'] == 'VPS') {
              $reques_vcenter = $this->dashboard->createVPS($list_vps, $due_date); //request đên Vcenter
            } else {
              $reques_vcenter = $this->dashboard->createNatVPS($list_vps, $due_date); //request đên Vcenter
            }
            // $reques_vcenter = true;
            if ($reques_vcenter) {
                $create_order->status = 'Finish';
                $create_order->save();
                try {
                  $this->send_mail_finish('vps',$create_order->id, $detail_order ,$list_vps);
                } catch (\Exception $e) {
                    report($e);
                    if($create_vps) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
        else {
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $create_order->id,
                'type' => $data['type_products'],
                'due_date' => $date,
                'description' => !empty($data['description'])? $data['description'] : '',
                'payment_method' => !empty($data['payment_method'])? $data['payment_method'] : '',
                'status' => 'unpaid',
                'sub_total' => $data['sub_total'],
                'quantity' => $data['quantity'],
                'user_id' => $data['user_id'],
                'addon_id' => !empty($data['addon'])? '1' : '0',
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            $user = $this->user->find($data['user_id']);
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDODV' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '2',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            // ket thuc tao order detail
            $qtt = $data['quantity'];
            // Tao order VPS
            for ($i=0; $i < $qtt; $i++) {
                $data_vps = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' => $data['user_id'],
                    'product_id' => $data['product_id'],
                    'ip' => !empty($data['ip'])? $data['ip'] : '',
                    'os' => $data['os'],
                    'billing_cycle' => $data['billing_cycle'],
                    'status' => 'Pending',
                    'user' => !empty($data['user'])? $data['user'] : '',
                    'password' => !empty($data['password'])? $data['password'] : '',
                    'addon_id' => !empty($data['addon'])? $data['addon'] : '0',
                    'security' => !empty($data['check_security']) ? true : false,
                    'price_override' => !empty($data['check_price_override']) ? $data['total_price_override'] : '',
                    'type_vps' => ($data['type_products'] == 'NAT-VPS') ? 'nat_vps' : 'vps',
                ];
                $create_vps = $this->vps->create($data_vps);

                if (!empty($data['addon'])) {
                    if ($data['addon_ram'] > 0 || $data['addon_cpu'] > 0 || $data['addon_disk'] > 0  ) {
                        $data_vps_config = [
                            'vps_id' => $create_vps->id,
                            'ip' => '0',
                            'cpu' => !empty($data['addon_cpu']) ? $data['addon_cpu'] : 0,
                            'ram' => !empty($data['addon_ram']) ? $data['addon_ram'] : 0,
                            'disk' => !empty($data['addon_disk']) ? $data['addon_disk'] : 0,
                        ];
                        $this->vps_config->create($data_vps_config);
                    }
                }
                $type_os = $data['os'];
                if ($type_os == 1) {
                    $type_os = 'Windows 10 64bit';
                }
                elseif ($type_os == 2 || $type_os == 30) {
                    $type_os = 'Windows Server 2012 R2';
                }
                elseif ($type_os == 3) {
                    $type_os = 'Windows Server 2016';
                }
                elseif ($type_os == 31) {
                    $type_os = 'Linux Ubuntu-20.04';
                }
                elseif ($type_os == 15) {
                    $type_os = 'Windows Server 2019';
                }
                elseif ($type_os == 31) {
                    $type_os = 'Linux Ubuntu-20.04';
                }
                elseif ($type_os == 15) {
                    $type_os = 'Windows Server 2019';
                }
                else {
                    $type_os = 'Linux CentOS 7 64bit';
                }

                $billings = config('billing');

                $email = $this->email->find($product->meta_product->email_create);
                $url = url('');
                $url = str_replace(['http://','https://'], ['', ''], $url);
                $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $create_order->id,  $product->name , '', '' ,$billings[$data_vps['billing_cycle']], '', $create_order->created_at, '', '',number_format($sub_total,0,",",".") . ' VNĐ' ,number_format($total,0,",",".") . ' VNĐ' , $data['quantity'], $type_os , '', $url];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'subject' => !empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận đặt hàng vps'
                ];

                try {
                  // $subject = !empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận đặt hàng vps';
                  $data_tb_send_mail = [
                    'type' => 'mail_order_vps',
                    'type_service' => 'vps',
                    'user_id' => $user->id,
                    'status' => false,
                    'content' => serialize($data),
                  ];
                  $this->send_mail->create($data_tb_send_mail);
                } catch (\Exception $e) {
                    report($e);
                    if($create_vps) {
                        return true;
                    } else {
                        return false;
                    }
                }

            }

        }

        // Ket thuc tao order vps
        if($create_vps) {
            return true;
        } else {
            return false;
        }
    }

    public function create_order_vps_us($data)
    {
        $product = $this->product->find($data['product_id']);
        $add_on = !empty($data['addon'])? 1 : '';
        $status = $data['status'];
        if (!empty($data['total_price_override'])) {
            $total = $data['total_price_override'];
            $sub_total = $total / $data['quantity'];
        }
        else {
            $sub_total = $product->pricing[$data['billing_cycle']];
            if (!empty($data['check_price_override'])) {
                $total = $data['total_price_override'];
            } else {
                $total = $product->pricing[$data['billing_cycle']] * $data['quantity'];
                if (!empty($add_on)) {
                    $product_addon_cpu = $this->product->find($data['id-addon-cpu']);
                    $product_addon_ram = $this->product->find($data['id-addon-ram']);
                    $product_addon_disk = $this->product->find($data['id-addon-disk']);
                    if ( $data['addon_cpu'] == 0  && $data['addon_ram'] == 0 && $data['addon_disk'] ==0 ) {
                      $add_on = 0;
                    }
                    // TInh total va subtotal
                    if ($data['addon_cpu'] > 0) {
                        $sub_total += $product_addon_cpu->pricing[$data['billing_cycle']] * $data['addon_cpu'];
                        $total += $product_addon_cpu->pricing[$data['billing_cycle']] * $data['addon_cpu'] * $data['quantity'];
                    }
                    // dd($sub_total, $total,$data['addon_ram']);
                    if ($data['addon_ram'] > 0) {
                        $sub_total += $product_addon_ram->pricing[$data['billing_cycle']] * $data['addon_ram'];
                        $total += $product_addon_ram->pricing[$data['billing_cycle']] * $data['addon_ram'] * $data['quantity'];
                    }
                    // dd($sub_total, $total,$data['addon_ram']);
                    if ($data['addon_disk'] > 0) {
                        $sub_total += $product_addon_disk->pricing[$data['billing_cycle']] * ($data['addon_disk']/10);
                        $total += $product_addon_disk->pricing[$data['billing_cycle']] * ($data['addon_disk']/10) * $data['quantity'];
                    }
                    // dd($sub_total, $total,$data['addon_disk']/10);
                }
            }
        }
        // tao order
        $data_order = [
            'user_id' => $data['user_id'],
            'promotion_id' => $data['promotion_id'],
            'total' => $total,
            'status' => $data['status'],
            'description' => !empty($data['description'])? $data['description'] : '',
            'verify' => true,
        ];
        $create_order = $this->order->create($data_order);
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tạo',
          'model' => 'Admin/Order',
          'description' => ' Order VPS US #' . $create_order->id,
        ];
        $this->log_activity->create($data_log);
        // $create_order = 1;
        // ket thuc tao order
        // tao order detail
        if ($status == 'Active' || $status == 'Finish') {
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $date = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date . $billing[$data['billing_cycle']] ));
            $data_order_detail = [
                'order_id' => $create_order->id,
                'type' => $data['type_products'],
                'due_date' => $due_date,
                'description' => !empty($data['description'])? $data['description'] : '',
                'payment_method' => 1,
                'status' => 'paid',
                'sub_total' => $data['sub_total'],
                'quantity' => $data['quantity'],
                'user_id' => $data['user_id'],
                'paid_date' => date('Y-m-d'),
                'addon_id' => !empty($data['addon'])? '1' : '0',
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            $user = $this->user->find($data['user_id']);
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDODV' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '2',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Active',
                'method_gd_invoice' => 'credit',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            // ket thuc tao order detail
            $qtt = $data['quantity'];
            $list_vps = [];
            // Tao order VPS
            $type_product_vps = $data['type_products'];
            for ($i=0; $i < $qtt; $i++) {
                if ($type_product_vps == 'NAT-VPS') {
                  $type_vps = 'nat_vps';
                } else {
                  $type_vps = 'vps';
                }
                $data_vps = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' => $data['user_id'],
                    'product_id' => $data['product_id'],
                    'next_due_date' => $due_date,
                    'ip' => !empty($data['ip'])? $data['ip'] : '',
                    'os' => $data['os'],
                    'billing_cycle' => $data['billing_cycle'],
                    'status' => 'Active',
                    'date_create' => date('Y-m-d'),
                    'user' => !empty($data['user'])? $data['user'] : '',
                    'password' => !empty($data['password'])? $data['password'] : '',
                    'paid' => 'paid',
                    'addon_id' => !empty($data['addon'])? $data['addon'] : '0',
                    'security' => !empty($data['check_security']) ? true : false,
                    'price_override' => !empty($data['check_price_override']) ? $data['total_price_override'] : '',
                    'type_vps' => $type_vps,
                    'location' => 'us',
                ];
                $create_vps = $this->vps->create($data_vps);
                $list_vps[] = $create_vps;
                if (!empty($data['addon'])) {
                    if ($data['addon_ram'] > 0 || $data['addon_cpu'] > 0 || $data['addon_disk'] > 0  ) {
                        $data_vps_config = [
                            'vps_id' => $create_vps->id,
                            'ip' => '0',
                            'cpu' => !empty($data['addon_cpu']) ? $data['addon_cpu'] : 0,
                            'ram' => !empty($data['addon_ram']) ? $data['addon_ram'] : 0,
                            'disk' => !empty($data['addon_disk']) ? $data['addon_disk'] : 0,
                        ];
                        $this->vps_config->create($data_vps_config);
                    }
                }
            }
            // ghi vào tổng thu
            if (!empty($this->total_price->first())) {
                $total_price = $this->total_price->first();
                $total_price->vps += $total;
                $total_price->save();
            } else {
                $data_total_price = [
                    'vps' => $total,
                    'hosting' => '0',
                    'server' => '0',
                ];
                $this->total_price->create($data_total_price);
            }
            $reques_vcenter = $this->dashboard->createVpsUs($list_vps); //request đên Vcenter
            if ($reques_vcenter) {
                $create_order->status = 'Finish';
                $create_order->save();
                try {
                  $this->send_mail_finish('vps',$create_order->id, $detail_order ,$list_vps);
                } catch (\Exception $e) {
                    report($e);
                    if($create_vps) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
        else {
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $create_order->id,
                'type' => $data['type_products'],
                'due_date' => $date,
                'description' => !empty($data['description'])? $data['description'] : '',
                'payment_method' => !empty($data['payment_method'])? $data['payment_method'] : '',
                'status' => 'unpaid',
                'sub_total' => $data['sub_total'],
                'quantity' => $data['quantity'],
                'user_id' => $data['user_id'],
                'addon_id' => !empty($data['addon'])? '1' : '0',
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            $user = $this->user->find($data['user_id']);
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDOVU' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '2',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            // ket thuc tao order detail
            $qtt = $data['quantity'];
            // Tao order VPS
            $type_product_vps = $data['type_products'];
            for ($i=0; $i < $qtt; $i++) {
                if ($type_product_vps == 'NAT-VPS') {
                  $type_vps = 'nat_vps';
                } else {
                  $type_vps = 'vps';
                }
                $data_vps = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' => $data['user_id'],
                    'product_id' => $data['product_id'],
                    'ip' => !empty($data['ip'])? $data['ip'] : '',
                    'os' => $data['os'],
                    'billing_cycle' => $data['billing_cycle'],
                    'status' => 'Pending',
                    'user' => !empty($data['user'])? $data['user'] : '',
                    'password' => !empty($data['password'])? $data['password'] : '',
                    'addon_id' => !empty($data['addon'])? $data['addon'] : '0',
                    'security' => !empty($data['check_security']) ? true : false,
                    'price_override' => !empty($data['check_price_override']) ? $data['total_price_override'] : '',
                    'type_vps' => $type_vps,
                    'location' => 'us',
                ];
                $create_vps = $this->vps->create($data_vps);

                if (!empty($data['addon'])) {
                    if ($data['addon_ram'] > 0 || $data['addon_cpu'] > 0 || $data['addon_disk'] > 0  ) {
                        $data_vps_config = [
                            'vps_id' => $create_vps->id,
                            'ip' => '0',
                            'cpu' => !empty($data['addon_cpu']) ? $data['addon_cpu'] : 0,
                            'ram' => !empty($data['addon_ram']) ? $data['addon_ram'] : 0,
                            'disk' => !empty($data['addon_disk']) ? $data['addon_disk'] : 0,
                        ];
                        $this->vps_config->create($data_vps_config);
                    }
                }
                $type_os = $data['os'];
                if ($type_os == 1) {
                    $type_os = 'Windows 10 64bit';
                }
                elseif ($type_os == 2 || $type_os == 30) {
                    $type_os = 'Windows Server 2012 R2';
                }
                elseif ($type_os == 3) {
                    $type_os = 'Windows Server 2016';
                }
                elseif ($type_os == 31) {
                    $type_os = 'Linux Ubuntu-20.04';
                }
                elseif ($type_os == 15) {
                    $type_os = 'Windows Server 2019';
                }
                else {
                    $type_os = 'Linux CentOS 7 64bit';
                }

                $billings = config('billing');

                $email = $this->email->find($product->meta_product->email_create);
                $url = url('');
                $url = str_replace(['http://','https://'], ['', ''], $url);
                $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $create_order->id,  $product->name , '', '' ,$billings[$data_vps['billing_cycle']], '', $create_order->created_at, '', '',number_format($sub_total,0,",",".") . ' VNĐ' ,number_format($total,0,",",".") . ' VNĐ' , $data['quantity'], $type_os , '', $url];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'subject' => !empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận đặt hàng vps'
                ];

                try {
                    // $this->send_mail(!empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận đặt hàng vps', $data, $user);
                    $data_tb_send_mail = [
                      'type' => 'mail_order_vps_us',
                      'type_service' => 'vps_us',
                      'user_id' => $user->id,
                      'status' => false,
                      'content' => serialize($data),
                    ];
                    $this->send_mail->create($data_tb_send_mail);
                } catch (\Exception $e) {
                    report($e);
                    if($create_vps) {
                        return true;
                    } else {
                        return false;
                    }
                }

            }

        }

        // Ket thuc tao order vps
        if($create_vps) {
            return true;
        } else {
            return false;
        }
    }

    public function create_order_server($data)
    {
        // dd($data);
        $user = $this->user->find($data['user_id']);
        // tao order
        $total = $data['amount'] * $data['quantity'];
        $data_order = [
            'user_id' => $data['user_id'],
            'promotion_id' => $data['promotion_id'],
            'total' => $total,
            'status' => $data['status'],
            'description' => !empty($data['description'])? $data['description'] : '',
            'verify' => true,
        ];
        $create_order = $this->order->create($data_order);
        // ket thuc tao order
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tạo',
          'model' => 'Admin/Order',
          'description' => ' Order Server #' . $create_order->id,
        ];
        $this->log_activity->create($data_log);
        // tao order detail
        $status = $data['status'];
        if ($status == 'Active' || $status == 'Finish') {
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $date = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date . $billing[$data['billing_cycle']] ));
            $data_order_detail = [
                'order_id' => $create_order->id,
                'type' => $data['type_products'],
                'due_date' => $due_date,
                'description' => !empty($data['description'])? $data['description'] : '',
                'payment_method' => 1,
                'status' => 'paid',
                'sub_total' => $data['amount'],
                'quantity' => $data['quantity'],
                'user_id' => $data['user_id'],
                'paid_date' => date('Y-m-d'),
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // ket thuc tao order detail $this->server
            // history_pay
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDODS' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '2',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Active',
                'detail_order_id' => $detail_order->id,
                'method_gd_invoice' => 'credit',
            ];
            $this->history_pay->create($data_history);
            // Tao order server
            $qtt = $data['quantity'];

            for ($i=0; $i < $qtt; $i++) {
                $data_server = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' => $data['user_id'],
                    'ip' => !empty($data['ip'])? $data['ip'] : '',
                    'next_due_date' => $due_date,
                    'os' => !empty($data['os']) ? $data['os'] : '',
                    'type' => $data['type_server'],
                    'server_name' => $data['server_name'],
                    'chip' => $data['chip'],
                    'ram' => $data['ram'],
                    'disk' => $data['disk'],
                    'location' => $data['location'],
                    'amount' => $data['amount'],
                    'billing_cycle' => $data['billing_cycle'],
                    'status' => 'Active',
                    'status_server' => 'on',
                    'user_name' => !empty($data['user_name'])? $data['user_name'] : '',
                    'password' => !empty($data['password'])? $data['password'] : '',
                    'paid' => 'paid',
                    'date_create' => date('Y-m-d'),
                    'description' => $data['description'],
                ];
                $create_server = $this->server->create($data_server);
                // gởi mail
            }

        } else {
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $create_order->id,
                'type' => $data['type_products'],
                'due_date' => $date,
                'description' => !empty($data['description'])? $data['description'] : '',
                'payment_method' => !empty($data['payment_method'])? $data['payment_method'] : '',
                'status' => 'unpaid',
                'sub_total' => $data['amount'],
                'quantity' => $data['quantity'],
                'user_id' => $data['user_id'],
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // ket thuc tao order detail $this->server
            // history_pay
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDODS' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '2',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            // Tao order VPS
            $qtt = $data['quantity'];
            for ($i=0; $i < $qtt; $i++) {
                $data_server = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' => $data['user_id'],
                    'ip' => !empty($data['ip'])? $data['ip'] : '',
                    'os' => !empty($data['os']) ? $data['os'] : '',
                    'type' => $data['type_server'],
                    'server_name' => $data['server_name'],
                    'chip' => $data['chip'],
                    'ram' => $data['ram'],
                    'disk' => $data['disk'],
                    'location' => $data['location'],
                    'amount' => $data['amount'],
                    'billing_cycle' => $data['billing_cycle'],
                    'status' => 'Pending',
                    'user_name' => !empty($data['user_name'])? $data['user_name'] : '',
                    'password' => !empty($data['password'])? $data['password'] : '',
                    'description' => $data['description'],
                ];
                $create_server = $this->server->create($data_server);
                // gởi mail
            }
        }

        // Ket thuc tao order server
        if($create_server) {
            return true;
        } else {
            return false;
        }
    }
    // Tao don hang colocation
    public function create_order_colocation($data)
    {
        // dd($data);
        $user = $this->user->find($data['user_id']);
        // tao order
        $total = $data['amount'] * $data['quantity'];
        $data_order = [
            'user_id' => $data['user_id'],
            'promotion_id' => $data['promotion_id'],
            'total' => $total,
            'status' => $data['status'],
            'description' => !empty($data['description'])? $data['description'] : '',
            'verify' => true,
        ];
        $create_order = $this->order->create($data_order);
        // ket thuc tao order
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tạo',
          'model' => 'Admin/Order',
          'description' => ' Order Colocation #' . $create_order->id,
        ];
        $this->log_activity->create($data_log);
        // tao order detail
        $status = $data['status'];
        if ($status == 'Active' || $status == 'Finish') {
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $date = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date . $billing[$data['billing_cycle']] ));
            $data_order_detail = [
                'order_id' => $create_order->id,
                'type' => $data['type_products'],
                'due_date' => $due_date,
                'description' => !empty($data['description'])? $data['description'] : '',
                'payment_method' => 1,
                'status' => 'paid',
                'sub_total' => $data['amount'],
                'quantity' => $data['quantity'],
                'user_id' => $data['user_id'],
                'paid_date' => date('Y-m-d'),
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // ket thuc tao order detail $this->server
            // history_pay
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDODC' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '2',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Active',
                'detail_order_id' => $detail_order->id,
                'method_gd_invoice' => 'credit',
            ];
            $this->history_pay->create($data_history);
            // Tao order server
            $qtt = $data['quantity'];

            for ($i=0; $i < $qtt; $i++) {
                $data_colocation = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' => $data['user_id'],
                    'ip' => !empty($data['ip'])? $data['ip'] : '',
                    'next_due_date' => $due_date,
                    'location' => $data['location'],
                    'amount' => $data['amount'],
                    'billing_cycle' => $data['billing_cycle'],
                    'status' => 'Active',
                    'paid' => 'paid',
                    'date_create' => date('Y-m-d h:i:s'),
                    'type_colo' => $data['type_colo'],
                    'bandwidth' => $data['bandwidth'],
                    'power' => $data['power'],
                    'status_colo' => 'on',
                ];
                $create_colocation = $this->colocation->create($data_colocation);
                // gởi mail
            }

        } else {
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $create_order->id,
                'type' => $data['type_products'],
                'due_date' => $date,
                'description' => !empty($data['description'])? $data['description'] : '',
                'payment_method' => !empty($data['payment_method'])? $data['payment_method'] : '',
                'status' => 'unpaid',
                'sub_total' => $data['amount'],
                'quantity' => $data['quantity'],
                'user_id' => $data['user_id'],
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // ket thuc tao order detail $this->server
            // history_pay
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDODC' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '2',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            // Tao order VPS
            $qtt = $data['quantity'];
            for ($i=0; $i < $qtt; $i++) {
                $data_colocation = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' => $data['user_id'],
                    'ip' => !empty($data['ip'])? $data['ip'] : '',
                    'type_colo' => $data['type_colo'],
                    'bandwidth' => $data['bandwidth'],
                    'location' => $data['location'],
                    'power' => $data['power'],
                    'amount' => $data['amount'],
                    'billing_cycle' => $data['billing_cycle'],
                    'status' => 'Pending',
                ];
                $create_colocation = $this->colocation->create($data_colocation);
                // gởi mail
            }
        }

        // Ket thuc tao order server
        if($create_colocation) {
            return true;
        } else {
            return false;
        }
    }
    // Tao don hang 1 hosting
    public function create_order_hosting($data)
    {
        $user = $this->user->find($data['user_id']);
        $product = $this->product->find($data['product_id']);
        if (!empty($data['total_price_override'])) {
            $total = $data['total_price_override'];
            $sub_total = $total / $data['quantity'];
        } else {
            $sub_total = $product->pricing[$data['billing_cycle']];
            $total = $product->pricing[$data['billing_cycle']];
        }
        // tao order
        $data_order = [
            'user_id' => $data['user_id'],
            'promotion_id' => $data['promotion_id'],
            'total' => $total,
            'status' => $data['status'],
            'description' => !empty($data['description'])? $data['description'] : '',
        ];
        $create_order = $this->order->create($data_order);
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tạo',
          'model' => 'Admin/Order',
          'description' => ' Order Hosting #' . $create_order->id,
        ];
        $this->log_activity->create($data_log);
        // ket thuc tao order
        // tao order detail
        $status = $data['status'];
        if ($status == 'Active' || $status == 'Finish') {
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $date = date('Y-m-d');
            if ($data['billing_cycle'] == 'one_time_pay') {
              $due_date = '2099-12-31';
            } else {
              $due_date = date('Y-m-d', strtotime( $date . $billing[$data['billing_cycle']] ));
            }
            $data_order_detail = [
                'order_id' => $create_order->id,
                'type' => $data['type_products'],
                'due_date' => $due_date,
                'description' => !empty($data['description'])? $data['description'] : '',
                'payment_method' => 1,
                'status' => 'paid',
                'sub_total' => $data['sub_total'],
                'quantity' => 1,
                'user_id' => $data['user_id'],
                'paid_date' => date('Y-m-d'),
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // history_pay
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDODH' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '2',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Active',
                'detail_order_id' => $detail_order->id,
                'method_gd_invoice' => 'credit',
            ];
            $this->history_pay->create($data_history);
            // ket thuc tao order detail $this->hosting
            $domain_hosting = strtolower($data['domain']);
            $user_hosting = str_replace('.', '', $domain_hosting); //uthien.com -> uthiencom
            $check_user_hosting = substr($user_hosting, 0, 1);
            if ( is_numeric($check_user_hosting) ) {
              $user_hosting = 'a' . substr($user_hosting, 0, 6); //123uthiencom -> a123uthienco
            }
            else {
              $user_hosting = substr($user_hosting, 0, 7); //uthiencom -> uthienco
            }
            // Tao order Hosting
            $data_hosting = [
                'detail_order_id' => $detail_order->id,
                'user_id' => $data['user_id'],
                'product_id' => $data['product_id'],
                'next_due_date' => $due_date,
                'domain' => !empty($data['domain'])? $data['domain'] : '',
                'billing_cycle' => $data['billing_cycle'],
                'status' => 'Active',
                'user' => $user_hosting,
                'password' => $this->rand_string(),
                'paid' => 'paid',
                'server_hosting_id' => !empty($data['server_hosting'])? $data['server_hosting'] : 0,
                'location' => $data['type_products'] == 'Hosting-Singapore' ? 'si' : 'vn',
                'date_create' => date('Y-m-d'),
            ];
            $create_hosting = $this->hosting->create($data_hosting);
            $list_hosting[] = $create_hosting;
            if ($data['billing_cycle'] == 'one_time_pay') {
              if ($data['type_products'] == 'Hosting-Singapore') {
                $request_da = $this->da_si->adminCreateHosting($create_hosting, 'one_time_pay');
              } else {
                $request_da = $this->da->adminCreateHosting($create_hosting, 'one_time_pay');
              }
            } else {
              if ($data['type_products'] == 'Hosting-Singapore') {
                $request_da = $this->da_si->adminCreateHosting($create_hosting, $due_date);
              } else {
                $request_da = $this->da->adminCreateHosting($create_hosting, $due_date);
              }
            }
            if (!empty($this->total_price->first())) {
                $total_price = $this->total_price->first();
                $total_price->hosting += $total;
                $total_price->save();
            } else {
                $data_total_price = [
                    'vps' => '0',
                    'hosting' => $total,
                    'server' => '0',
                ];
                $this->total_price->create($data_total_price);
            }
            try {
              $this->send_mail_finish('hosting',$create_order->id, $detail_order ,$list_hosting);
            } catch (\Exception $e) {
              report($e);
              if($create_hosting) {
                  return true;
              } else {
                  return false;
              }
            }
        }
        else {
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $create_order->id,
                'type' => $data['type_products'],
                'due_date' => $date,
                'description' => !empty($data['description'])? $data['description'] : '',
                'payment_method' => !empty($data['payment_method'])? $data['payment_method'] : '',
                'status' => 'unpaid',
                'sub_total' => $sub_total,
                'quantity' => 1,
                'user_id' => $data['user_id'],
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // history_pay
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDODH' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '2',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            // ket thuc tao order detail $this->hosting
            $domain_hosting = strtolower($data['domain']);
            $user_hosting = str_replace('.', '', $domain_hosting); //uthien.com -> uthiencom
            $check_user_hosting = substr($user_hosting, 0, 1);
            if ( is_numeric($check_user_hosting) ) {
              $user_hosting = 'a' . substr($user_hosting, 0, 6); //123uthiencom -> a123uthienco
            }
            else {
              $user_hosting = substr($user_hosting, 0, 7); //uthiencom -> uthienco
            }
            // Tao order Hosting
            $data_hosting = [
                'detail_order_id' => $detail_order->id,
                'user_id' => $data['user_id'],
                'product_id' => $data['product_id'],
                'domain' => !empty($data['domain'])? $data['domain'] : '',
                'billing_cycle' => $data['billing_cycle'],
                'status' => 'Pending',
                'user' => $user_hosting,
                'password' => $this->rand_string(),
                'server_hosting_id' => !empty($data['server_hosting'])? $data['server_hosting'] : 0,
                'location' => $data['type_products'] == 'Hosting-Singapore' ? 'si' : 'vn',
            ];
            $create_hosting = $this->hosting->create($data_hosting);

            $email = $this->email->find($product->meta_product->email_create);

            $billings = config('billing');
            $url = url('');
            $url = str_replace(['http://','https://'], ['', ''], $url);
            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $create_order->id,  $product->name , $data_hosting['domain'], '' ,$billings[$data_hosting['billing_cycle']], '', $create_order->created_at, '', '',number_format($sub_total,0,",",".") . ' VNĐ' ,number_format($total ,0,",",".") . ' VNĐ' , 1, '', '', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

            $data = [
                'content' => $content,
                'user_name' => $user->name,
                'subject' => !empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận đặt hàng hosting'
            ];
            try {
                // $this->send_mail(!empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận đặt hàng hosting', $data, $user);
              $data_tb_send_mail = [
                  'type' => 'mail_order_hosting',
                  'type_service' => 'hosting',
                  'user_id' => $user->id,
                  'status' => false,
                  'content' => serialize($data),
              ];
              $this->send_mail->create($data_tb_send_mail);
            } catch (\Exception $e) {
                if($create_hosting) {
                    return true;
                } else {
                    return false;
                }
            }
        }
        // Ket thuc tao order hosting
        if($create_hosting) {
            return true;
        } else {
            return false;
        }
    }

    public function send_mail($subject, $data, $user)
    {
        $this->user_email = $user->email;
        $this->subject = $subject;
        $mail = Mail::send('users.mails.orders', $data, function($message){
            $message->from('support@cloudzone.vn', 'Cloudzone Portal');
            $message->to($this->user_email)->subject($this->subject);
        });
    }
    // Tao don hang nhieu hosting
    public function create_order_hostings($data)
    {
        $user = $this->user->find($data['user_id']);
        $product = $this->product->find($data['product_id']);
        $billing_cycle = $data['billing_cycle'];
        $type_hosting = $data['type_products'];
        if (!empty($data['total_price_override'])) {
            $total = $data['total_price_override'];
            $sub_total = $total / $data['quantity'];
        } else {
            $sub_total = $product->pricing[$data['billing_cycle']];
            $total = $product->pricing[$data['billing_cycle']] * $data['quantity'];
        }

        // tao order
        $data_order = [
            'user_id' => $data['user_id'],
            'promotion_id' => $data['promotion_id'],
            'total' => $data['total'],
            'status' => $data['status'],
            'description' => !empty($data['description'])? $data['description'] : '',
        ];
        $create_order = $this->order->create($data_order);
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tạo',
          'model' => 'Admin/Order',
          'description' => ' Order Hosting #' . $create_order->id,
        ];
        $this->log_activity->create($data_log);
        // ket thuc tao order
        // tao order detail
        $status = $data['status'];
        if ($status == 'Active' || $status == 'Finish') {
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $date = date('Y-m-d');
            if ($data['billing_cycle'] == 'one_time_pay') {
              $due_date = '2099-12-31';
            } else {
              $due_date = date('Y-m-d', strtotime( $date . $billing[$data['billing_cycle']] ));
            }
            $data_order_detail = [
                'order_id' => $create_order->id,
                'type' => $data['type_products'],
                'due_date' => $due_date,
                'description' => !empty($data['description'])? $data['description'] : '',
                'payment_method' => 1,
                'status' => 'paid',
                'sub_total' => $sub_total,
                'quantity' => $data['quantity'],
                'user_id' => $data['user_id'],
                'paid_date' => date('Y-m-d'),
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // history_pay
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDODH' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '2',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'method_gd_invoice' => 'credit',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            $list_domain = [];
            $list_hosting = [];
            // ket thuc tao order detail $this->hosting
            foreach ($data['domain'] as $key => $domain) {
                $domain_hosting = strtolower($domain);
                $user_hosting = str_replace('.', '', $domain_hosting); //uthien.com -> uthiencom
                $check_user_hosting = substr($user_hosting, 0, 1);
                if ( is_numeric($check_user_hosting) ) {
                  $user_hosting = 'a' . substr($user_hosting, 0, 6); //123uthiencom -> a123uthienco
                }
                else {
                  $user_hosting = substr($user_hosting, 0, 7); //uthiencom -> uthienco
                }
                // Tao order Hosting
                $data_hosting = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' => $user->id,
                    'product_id' => $product->id,
                    'next_due_date' => $due_date,
                    'domain' => $domain,
                    'billing_cycle' => $billing_cycle,
                    'status' => 'Active',
                    'user' => $user_hosting,
                    'password' => $this->rand_string(),
                    'paid' => 'paid',
                    'server_hosting_id' => !empty($data['server_hosting'])? $data['server_hosting'] : 0,
                    'location' => $type_hosting == 'Hosting-Singapore' ? 'si' : 'vn',
                    'date_create' => date('Y-m-d'),
                ];
                $create_hosting = $this->hosting->create($data_hosting);
                if(!$create_hosting) {
                    return false;
                }
                if ($billing_cycle == 'one_time_pay') {
                  if ($type_hosting == 'Hosting-Singapore') {
                    $request_da = $this->da_si->adminCreateHosting($create_hosting, 'one_time_pay');
                  } else {
                    $request_da = $this->da->adminCreateHosting($create_hosting, 'one_time_pay');
                  }
                } else {
                  if ($type_hosting == 'Hosting-Singapore') {
                    $request_da = $this->da_si->adminCreateHosting($create_hosting, $due_date);
                  } else {
                    $request_da = $this->da->adminCreateHosting($create_hosting, $due_date);
                  }
                }
            }
            if (!empty($this->total_price->first())) {
                $total_price = $this->total_price->first();
                $total_price->hosting += $total;
                $total_price->save();
            } else {
                $data_total_price = [
                    'vps' => '0',
                    'hosting' => $total,
                    'server' => '0',
                ];
                $this->total_price->create($data_total_price);
            }
            try {
              $this->send_mail_finish('hosting',$create_order->id, $detail_order ,$list_domain);
            } catch (\Exception $e) {
              return true;
            }
        }
        else {
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $create_order->id,
                'type' => $data['type_products'],
                'due_date' => $date,
                'description' => !empty($data['description'])? $data['description'] : '',
                'payment_method' => !empty($data['payment_method'])? $data['payment_method'] : '',
                'status' => 'unpaid',
                'sub_total' => $sub_total,
                'quantity' => $data['quantity'],
                'user_id' => $data['user_id'],
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // history_pay
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDODH' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '2',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            // ket thuc tao order detail $this->hosting
            foreach ($data['domain'] as $key => $domain) {
                $domain_hosting = strtolower($domain);
                $user_hosting = str_replace('.', '', $domain_hosting); //uthien.com -> uthiencom
                $check_user_hosting = substr($user_hosting, 0, 1);
                if ( is_numeric($check_user_hosting) ) {
                  $user_hosting = 'a' . substr($user_hosting, 0, 6); //123uthiencom -> a123uthienco
                }
                else {
                  $user_hosting = substr($user_hosting, 0, 7); //uthiencom -> uthienco
                }
                // Tao order Hosting
                $data_hosting = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' => $user->id,
                    'product_id' => $product->id,
                    'domain' => $domain,
                    'billing_cycle' => $billing_cycle,
                    'status' => 'Pending',
                    'user' => $user_hosting,
                    'password' => $this->rand_string(),
                    'server_hosting_id' => !empty($data['server_hosting'])? $data['server_hosting'] : 0,
                    'location' => $type_hosting == 'Hosting-Singapore' ? 'si' : 'vn',
                ];
                $create_hosting = $this->hosting->create($data_hosting);

                if(!$create_hosting) {
                    return false;
                }
                // gui mail order
                $billings = config('billing');
                $email = $this->email->find($product->meta_product->email_create);
                $url = url('');
                $url = str_replace(['http://','https://'], ['', ''], $url);
                $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $create_order->id,  $product->name , $data_hosting['domain'] , '' ,$billings[$data_hosting['billing_cycle']], '', $create_order->created_at, '', '',number_format($sub_total,0,",",".") . ' VNĐ' ,number_format($total ,0,",",".") . ' VNĐ' , 1, '', '', $url];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'subject' => !empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận đặt hàng hosting'
                ];
                try {
                  $data_tb_send_mail = [
                    'type' => 'mail_order_hosting',
                    'type_service' => 'hosting',
                    'user_id' => $user->id,
                    'status' => false,
                    'content' => serialize($data),
                  ];
                  $this->send_mail->create($data_tb_send_mail);
                } catch (\Exception $e) {
                    continue;
                }
            }
        }
        return true;

    }

    // Tao don hang email hosting
    public function create_order_email_hosting($data)
    {
        $user = $this->user->find($data['user_id']);
        $product = $this->product->find($data['product_id']);
        $billing_cycle = $data['billing_cycle'];
        $type_hosting = $data['type_products'];
        if (!empty($data['total_price_override'])) {
            $total = $data['total_price_override'];
            $sub_total = $total / $data['quantity'];
        } else {
            $sub_total = $product->pricing[$data['billing_cycle']];
            $total = $product->pricing[$data['billing_cycle']] * $data['quantity'];
        }

        // tao order
        $data_order = [
            'user_id' => $data['user_id'],
            'promotion_id' => $data['promotion_id'],
            'total' => $data['total'],
            'status' => $data['status'],
            'description' => !empty($data['description'])? $data['description'] : '',
        ];
        $create_order = $this->order->create($data_order);
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tạo',
          'model' => 'Admin/Order',
          'description' => ' Order Email Hosting #' . $create_order->id,
        ];
        $this->log_activity->create($data_log);
        // ket thuc tao order
        // tao order detail
        $status = $data['status'];
        if ($status == 'Active' || $status == 'Finish') {
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $date = date('Y-m-d');
            if ($data['billing_cycle'] == 'one_time_pay') {
              $due_date = '2099-12-31';
            } else {
              $due_date = date('Y-m-d', strtotime( $date . $billing[$data['billing_cycle']] ));
            }
            $data_order_detail = [
                'order_id' => $create_order->id,
                'type' => $data['type_products'],
                'due_date' => $due_date,
                'description' => !empty($data['description'])? $data['description'] : '',
                'payment_method' => 1,
                'status' => 'paid',
                'sub_total' => $sub_total,
                'quantity' => $data['quantity'],
                'user_id' => $data['user_id'],
                'paid_date' => date('Y-m-d'),
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // history_pay
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDOEH' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '2',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Active',
                'method_gd_invoice' => 'credit',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            $list_domain = [];
            $list_hosting = [];
            // ket thuc tao order detail $this->hosting
            $domain = $data['domain'];
            $data_hosting = [
                'detail_order_id' => $detail_order->id,
                'user_id' => $user->id,
                'product_id' => $product->id,
                'next_due_date' => $due_date,
                'domain' => $domain,
                'billing_cycle' => $billing_cycle,
                'status' => 'Active',
                'user' => !empty($data['user_name']) ? $data['user_name'] : '',
                'password' => !empty($data['password']) ? $data['password'] : '',
                'paid' => 'paid',
                'date_create' => date('Y-m-d'),
                'status_hosting' => 'on',
            ];
            $create_hosting = $this->email_hosting->create($data_hosting);
            if (!empty($this->total_price->first())) {
                $total_price = $this->total_price->first();
                $total_price->hosting += $total;
                $total_price->save();
            } else {
                $data_total_price = [
                    'vps' => '0',
                    'hosting' => $total,
                    'server' => '0',
                ];
                $this->total_price->create($data_total_price);
            }
            try {
              $this->send_mail_finish('email',$create_order->id, $detail_order ,$create_hosting);
            } catch (\Exception $e) {
                report($e);
              return true;
            }
        }
        else {
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $create_order->id,
                'type' => $data['type_products'],
                'due_date' => $date,
                'description' => !empty($data['description'])? $data['description'] : '',
                'payment_method' => !empty($data['payment_method'])? $data['payment_method'] : '',
                'status' => 'unpaid',
                'sub_total' => $sub_total,
                'quantity' => $data['quantity'],
                'user_id' => $data['user_id'],
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // history_pay
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDOEH' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '2',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            // ket thuc tao order detail $this->hosting
            // Tao order Hosting
            $domain = $data['domain'];
            $data_hosting = [
                'detail_order_id' => $detail_order->id,
                'user_id' => $user->id,
                'product_id' => $product->id,
                'domain' => $domain,
                'billing_cycle' => $billing_cycle,
                'status' => 'Pending',
                'user' => !empty($data['user_name']) ? $data['user_name'] : '',
                'password' => !empty($data['password']) ? $data['password'] : '',
            ];
            $create_hosting = $this->email_hosting->create($data_hosting);

            if(!$create_hosting) {
                return false;
            }
                // gui mail order
            $billings = config('billing');
            $email = $this->email->find($product->meta_product->email_create);
            $url = url('');
            $url = str_replace(['http://','https://'], ['', ''], $url);
            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $create_order->id,  $product->name , $data_hosting['domain'] , '' ,$billings[$data_hosting['billing_cycle']], '', $create_order->created_at, '', '',number_format($sub_total,0,",",".") . ' VNĐ' ,number_format($total ,0,",",".") . ' VNĐ' , 1, '', '', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

            $data = [
                'content' => $content,
                'user_name' => $user->name,
                'user_id' => $user->id,
                'user_email' => $user->email,
                'user_addpress' => $user->user_meta->address,
                'user_phone' => $user->user_meta->user_phone,
                'order_id' => $create_order->id,
                'product_name' => $product->name,
                'domain' => '',
                'ip' => '',
                'billing_cycle' => $data_hosting['billing_cycle'],
                'next_due_date' => '',
                'order_created_at' => $create_order->created_at,
                'services_username' => '',
                'services_password' => '',
                'sub_total' => $sub_total,
                'total' => $total,
                'qtt' => 1,
                'os' => '',
                'token' => '',
            ];
            try {
                $this->send_mail(!empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận đặt hàng Email Hosting', $data, $user);
            } catch (\Exception $e) {
                report($e);
                return true;
            }
        }
        return true;

    }

    public function create_order_upgrade_hosting($data)
    {
        $hosting = $this->hosting->find($data['hosting_id']);
        $product_upgrade = $this->product->find($data['product_upgradet']);

        $sub_total_hosting = $hosting->detail_order->sub_total;
        $sub_total_hosting_upgrade = $product_upgrade->pricing[$hosting->billing_cycle];

        $date_create_hosting = new Carbon($hosting->date_create);
        $next_due_date = new Carbon($hosting->next_due_date);
        if ($next_due_date->isPast()) {
            return false;
        } else {
             $total_day = $next_due_date->diffInDays($date_create_hosting);
             $now = new Carbon;
             $next_due_date = new Carbon($hosting->next_due_date);
             $total = ($sub_total_hosting_upgrade - $sub_total_hosting) / ($total_day / $next_due_date->diffInDays($now));
             $total = (int) round($total, -3);
             if (!empty($data['total_price_override'])) {
                 $total = $data['total_price_override'];
             }

             $data_order = [
                 'user_id' => $data['user_id'],
                 'total' => $total,
                 'status' => $data['status'],
                 'description' => 'upgrade hosting',
                 'makh' => '',
             ];
             $order = $this->order->create($data_order);
             // ghi log
             $data_log = [
               'user_id' => Auth::user()->id,
               'action' => 'tạo',
               'model' => 'Admin/Order',
               'description' => ' Order nâng cấp Hosting #' . $order->id,
             ];
             $this->log_activity->create($data_log);
             //create order detail
             $status = $data['status'];
             if ($status == 'Active' || $status == 'Finish') {
                 $date = date('Y-m-d');
                 $date = date('Y-m-d', strtotime( $date . '+300 Day' ));
                 $data_order_detail = [
                     'order_id' => $order->id,
                     'type' => 'upgrade_hosting',
                     'due_date' => $date,
                     'description' => 'upgrade hosting',
                     'status' => 'paid',
                     'sub_total' => $total,
                     'quantity' => 1,
                     'payment_method' => 1,
                     'user_id' => $data['user_id'],
                     'paid_date' => date('Y-m-d'),
                     'addon_id' => '0',
                 ];
                 $detail_order = $this->detail_order->create($data_order_detail);
                 $user = $this->user->find($data['user_id']);
                 $data_history = [
                     'user_id' => $user->id,
                     'ma_gd' => 'GDUGH' . strtoupper(substr(sha1(time()), 34, 39)),
                     'discription' => 'Hóa đơn số ' . $detail_order->id,
                     'type_gd' => '8',
                     'method_gd' => 'invoice',
                     'date_gd' => date('Y-m-d'),
                     'money'=> $total,
                     'status' => 'Active',
                     'method_gd_invoice' => 'credit',
                     'detail_order_id' => $detail_order->id,
                 ];
                 $this->history_pay->create($data_history);
                 $data_upgrade = [
                   'detail_order_id'  => $detail_order->id,
                   'hosting_id' => $hosting->id,
                   'product_id' => $data['product_upgradet'],
                 ];
                 $upgrade = $this->order_upgrade_hosting->create($data_upgrade);
                 if ($hosting->location == 'vn') {
                   $this->da->upgrade($hosting, $upgrade->product->package);
                 }
                 $hosting->product_id = $upgrade->product_id;
                 if (!empty($hosting->price_override)) {
                    $hosting->price_override += $total;
                 }
                 $hosting->save();
                 // gui mail
                 $data = [
                     'name' => $user->name,
                     'email' => $user->email,
                     'domain' => $hosting->domain,
                     'product_name' => $upgrade->product->name,
                     'subject' => 'Hoàn thành nâng cấp dịch vụ Hosting'
                 ];
                 try {
                      $data_tb_send_mail = [
                        'type' => 'finish_order_upgarde_hosting',
                        'type_service' => 'hosting',
                        'user_id' => $user->id,
                        'status' => false,
                        'content' => serialize($data),
                    ];
                    $this->send_mail->create($data_tb_send_mail);
                 } catch (\Exception $e) {
                     report($e);
                     return true;
                 }
             }
             else {
                 $date = date('Y-m-d');
                 $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
                 $data_order_detail = [
                     'order_id' => $order->id,
                     'type' => 'upgrade_hosting',
                     'due_date' => $date,
                     'description' => 'upgrade hosting',
                     'status' => 'unpaid',
                     'sub_total' => $total,
                     'quantity' => 1,
                     'user_id' => $data['user_id'],
                     'addon_id' => '0',
                 ];
                 $detail_order = $this->detail_order->create($data_order_detail);
                 $user = $this->user->find($data['user_id']);
                 $data_history = [
                     'user_id' => $user->id,
                     'ma_gd' => 'GDUGH' . strtoupper(substr(sha1(time()), 34, 39)),
                     'discription' => 'Hóa đơn số ' . $detail_order->id,
                     'type_gd' => '8',
                     'method_gd' => 'invoice',
                     'date_gd' => date('Y-m-d'),
                     'money'=> $total,
                     'status' => 'Pending',
                     'detail_order_id' => $detail_order->id,
                 ];
                 $this->history_pay->create($data_history);
                 // vetify order
                 $data_upgrade = [
                   'detail_order_id'  => $detail_order->id,
                   'hosting_id' => $hosting->id,
                   'product_id' => $data['product_upgradet'],
                 ];
                 $this->order_upgrade_hosting->create($data_upgrade);
                 // gửi mail
                 $data = [
                     'name' => $user->name,
                     'domain' => $hosting->domain,
                     'product_name' => $product_upgrade->name,
                     'amount' => $total,
                     'subject' => 'Xác nhận đặt hàng nâng cấp Hosting'
                 ];
                 try {
                  $data_tb_send_mail = [
                      'type' => 'order_upgrade_hosting',
                      'type_service' => 'hosting',
                      'user_id' => $user->id,
                      'status' => false,
                      'content' => serialize($data),
                  ];
                  $this->send_mail->create($data_tb_send_mail);
                 } catch (\Exception $e) {
                   report($e);
                   return true;
                 }
             }
             return true;
         }
         return true;
    }


    public function detail($id)
    {
        return $this->order->find($id);
    }

    public function detail_order($order_id)
    {
        return $this->detail_order->where('order_id', $order_id)->with('vps', 'servers', 'hostings')->first();
    }

    public function edit_order_vps($data)
    {
        // update order
        $order_id = $data['order_id'];
        $order = $this->order->find($order_id);
        $data_order = [
            'user_id' => $data['user_id'],
            'promotion_id' => $data['promotion_id'],
            'total' => $data['total'],
            'status' => $data['status'],
            'description' => !empty($data['description'])? $data['description'] : '',
        ];
        $update_order = $order->update($data_order);
        // update order detail
        $detail_order = $this->detail_order->find($data['detail_order_id']);
        $status = $data['status'];
        if ($status == 'Active') {
            if ($detail_order->status != 'paid') {
                $billing = [
                    'monthly' => '1 Month',
                    'twomonthly' => '2 Month',
                    'quarterly' => '3 Month',
                    'semi_annually' => '6 Month',
                    'annually' => '1 Year',
                    'biennially' => '2 Year',
                    'triennially' => '3 Year'
                ];
                $date = $detail_order->created_at;
                $due_date = date('Y-m-d', strtotime( $date . $billing[$data['billing_cycle']] ));
                $data_order_detail = [
                    'order_id' => $order_id,
                    'type' => $data['type_products'],
                    'status' => 'paid',
                    'sub_total' => $data['sub_total'],
                    'quantity' => $data['quantity'],
                    'due_date' => $due_date,
                    'user_id' => $data['user_id'],
                    'paid_date' => date('Y-m-d'),
                ];
                $update_detail = $detail_order->update($data_order_detail);
                // update vps
                foreach ($data['vps_id'] as $key => $vps_id) {
                    $vps = $this->vps->find($vps_id);
                    $data_vps = [
                        'user_id' => $data['user_id'],
                        'product_id' => $data['product_id'],
                        'next_due_date' => $due_date,
                        'ip' => !empty($data['ip'])? $data['ip'] : '',
                        'os' => $data['os'],
                        'billing_cycle' => $data['billing_cycle'],
                        'user' => !empty($data['user'][$key])? $data['user'][$key] : '',
                        'password' => !empty($data['password'][$key])? $data['password'][$key] : '',
                        'paid' => 'paid',
                    ];
                    $update_vps = $vps->update($data_vps);
                }

            } else {
                $data_order_detail = [
                    'order_id' => $order_id,
                    'type' => $data['type_products'],
                    'sub_total' => $data['sub_total'],
                    'quantity' => $data['quantity'],
                ];
                $update_detail = $detail_order->update($data_order_detail);
                // update vps
                foreach ($data['vps_id'] as $key => $vps_id) {
                    $vps = $this->vps->find($vps_id);
                    $data_vps = [
                        'user_id' => $data['user_id'],
                        'product_id' => $data['product_id'],
                        'ip' => !empty($data['ip'])? $data['ip'] : '',
                        'os' => $data['os'],
                        'billing_cycle' => $data['billing_cycle'],
                        'user' => !empty($data['user'][$key])? $data['user'][$key] : '',
                        'password' => !empty($data['password'][$key])? $data['password'][$key] : '',
                    ];
                    $update_vps = $vps->update($data_vps);
                }
            }

        } else {
            $data_order_detail = [
                'order_id' => $order_id,
                'type' => $data['type_products'],
                'sub_total' => $data['sub_total'],
                'quantity' => $data['quantity'],
                'user_id' => $data['user_id'],
            ];
            $update_detail = $detail_order->update($data_order_detail);
            // update vps
            foreach ($data['vps_id'] as $key => $vps_id) {
                $vps = $this->vps->find($vps_id);
                $data_vps = [
                    'user_id' => $data['user_id'],
                    'product_id' => $data['product_id'],
                    'ip' => !empty($data['ip'])? $data['ip'] : '',
                    'os' => $data['os'],
                    'billing_cycle' => $data['billing_cycle'],
                    'user' => !empty($data['user'][$key])? $data['user'][$key] : '',
                    'password' => !empty($data['password'][$key])? $data['password'][$key] : '',
                ];
                $update_vps = $vps->update($data_vps);
            }
        }

        if($update_detail && $update_order && $update_vps) {
            return true;
        } else {
            return false;
        }

    }

    public function edit_order_server($data)
    {
        // update order
        $order_id = $data['order_id'];
        $order = $this->order->find($order_id);
        $data_order = [
            'user_id' => $data['user_id'],
            'promotion_id' => $data['promotion_id'],
            'total' => $data['total'],
            'status' => $data['status'],
            'description' => !empty($data['description'])? $data['description'] : '',
        ];
        $update_order = $order->update($data_order);
        // update order detail
        $detail_order = $this->detail_order->find($data['detail_order_id']);
        $status = $data['status'];
        if ($status == 'Active') {

            if ($detail_order->status != 'paid') {
                $billing = [
                    'monthly' => '1 Month',
                    'twomonthly' => '2 Month',
                    'quarterly' => '3 Month',
                    'semi_annually' => '6 Month',
                    'annually' => '1 Year',
                    'biennially' => '2 Year',
                    'triennially' => '3 Year'
                ];
                $date = $detail_order->created_at;
                $due_date = date('Y-m-d', strtotime( $date . $billing[$data['billing_cycle']] ));
                $data_order_detail = [
                    'order_id' => $order_id,
                    'type' => $data['type_products'],
                    'status' => 'paid',
                    'sub_total' => $data['sub_total'],
                    'quantity' => $data['quantity'],
                    'due_date' => $due_date,
                    'user_id' => $data['user_id'],
                    'paid_date' => date('Y-m-d'),
                ];
                $update_detail = $detail_order->update($data_order_detail);
                // update server
                foreach ($data['server_id'] as $key => $server_id) {
                    $server = $this->server->find($server_id);
                    $data_server = [
                        'user_id' => $data['user_id'],
                        'product_id' => $data['product_id'],
                        'next_due_date' => $due_date,
                        'ip' => !empty($data['ip'])? $data['ip'] : '',
                        'os' => $data['os'],
                        'billing_cycle' => $data['billing_cycle'],
                        'user' => !empty($data['user'][$key])? $data['user'][$key] : '',
                        'password' => !empty($data['password'][$key])? $data['password'][$key] : '',
                        'paid' => 'paid',
                    ];
                    $update_server = $server->update($data_server);
                }

	        } else {
                $data_order_detail = [
                    'order_id' => $order_id,
                    'type' => $data['type_products'],
                    'sub_total' => $data['sub_total'],
                    'quantity' => $data['quantity'],
                    'user_id' => $data['user_id'],
                ];
                $update_detail = $detail_order->update($data_order_detail);
                // update server
                foreach ($data['server_id'] as $key => $server_id) {
                    $server = $this->server->find($server_id);
                    $data_server = [
                        'user_id' => $data['user_id'],
                        'product_id' => $data['product_id'],
                        'ip' => !empty($data['ip'])? $data['ip'] : '',
                        'os' => $data['os'],
                        'billing_cycle' => $data['billing_cycle'],
                        'user' => !empty($data['user'][$key])? $data['user'][$key] : '',
                        'password' => !empty($data['password'][$key])? $data['password'][$key] : '',
                    ];
                    $update_server = $server->update($data_server);
                }
	        }
        } else {
            $detail_order = $this->detail_order->find($data['detail_order_id']);
            $data_order_detail = [
                'order_id' => $order_id,
                'type' => $data['type_products'],
                'sub_total' => $data['sub_total'],
                'quantity' => $data['quantity'],
                'user_id' => $data['user_id'],
            ];
            $update_detail = $detail_order->update($data_order_detail);
            // update server
            foreach ($data['server_id'] as $key => $server_id) {
                $server = $this->server->find($server_id);
                $data_server = [
                    'user_id' => $data['user_id'],
                    'product_id' => $data['product_id'],
                    'ip' => !empty($data['ip'])? $data['ip'] : '',
                    'os' => $data['os'],
                    'billing_cycle' => $data['billing_cycle'],
                    'user' => !empty($data['user'][$key])? $data['user'][$key] : '',
                    'password' => !empty($data['password'][$key])? $data['password'][$key] : '',
                ];
                $update_server = $server->update($data_server);
            }
        }

        if($update_detail && $update_order && $update_server) {
            return true;
        } else {
            return false;
        }

    }

    public function edit_order_hosting($data)
    {
        // update order
        $order_id = $data['order_id'];
        $order = $this->order->find($order_id);
        $data_order = [
            'user_id' => $data['user_id'],
            'promotion_id' => $data['promotion_id'],
            'total' => $data['total'],
            'status' => $data['status'],
            'description' => !empty($data['description'])? $data['description'] : '',
        ];
        $update_order = $order->update($data_order);
        // update order detail
        $detail_order = $this->detail_order->find($data['detail_order_id']);
        $status = $data['status'];
        if ($status == 'Active') {

            if ($detail_order->status != 'paid') {
                $billing = [
                    'monthly' => '1 Month',
                    'twomonthly' => '2 Month',
                    'quarterly' => '3 Month',
                    'semi_annually' => '6 Month',
                    'annually' => '1 Year',
                    'biennially' => '2 Year',
                    'triennially' => '3 Year'
                ];
                $date = $detail_order->created_at;
                $due_date = date('Y-m-d', strtotime( $date . $billing[$data['billing_cycle']] ));
                $data_order_detail = [
                    'order_id' => $order_id,
                    'type' => $data['type_products'],
                    'status' => 'paid',
                    'sub_total' => $data['sub_total'],
                    'quantity' => $data['quantity'],
                    'due_date' => $due_date,
                    'user_id' => $data['user_id'],
                    'paid_date' => date('Y-m-d'),
                ];
                $update_detail = $detail_order->update($data_order_detail);
                // update server
                foreach ($data['domain'] as $key => $domain) {

                    $hosting = $this->hosting->find($data['hosting_id'][$key]);
                    // Tao order Hosting
                    $data_hosting = [
                        'detail_order_id' => $detail_order->id,
                        'user_id' => $data['user_id'],
                        'next_due_date' => $due_date,
                        'product_id' => $data['product_id'],
                        'domain' => $domain,
                        'billing_cycle' => $data['billing_cycle'],
                        'user' => $data['user'][$key],
                        'password' => $data['password'][$key],
                        'paid' => 'paid',
                    ];
                    $update_hosting = $hosting->update($data_hosting);

                    if(!$update_hosting) {
                        return false;
                    }
                }
	        } else {
                $data_order_detail = [
                    'order_id' => $order_id,
                    'type' => $data['type_products'],
                    'sub_total' => $data['sub_total'],
                    'quantity' => $data['quantity'],
                    'user_id' => $data['user_id'],
                ];
                $update_detail = $detail_order->update($data_order_detail);
                // update server
                foreach ($data['domain'] as $key => $domain) {

                    $hosting = $this->hosting->find($data['hosting_id'][$key]);
                    // Tao order Hosting
                    $data_hosting = [
                        'detail_order_id' => $detail_order->id,
                        'user_id' => $data['user_id'],
                        'product_id' => $data['product_id'],
                        'domain' => $domain,
                        'billing_cycle' => $data['billing_cycle'],
                        'user' => $data['user'][$key],
                        'password' => $data['password'][$key],
                    ];
                    $update_hosting = $hosting->update($data_hosting);

                    if(!$update_hosting) {
                        return false;
                    }
                }
	        }
        } else {
            $data_order_detail = [
                'order_id' => $order_id,
                'type' => $data['type_products'],
                'sub_total' => $data['sub_total'],
                'quantity' => $data['quantity'],
                'user_id' => $data['user_id'],
            ];
            $update_detail = $detail_order->update($data_order_detail);
            // update server
            foreach ($data['domain'] as $key => $domain) {

                $hosting = $this->hosting->find($data['hosting_id'][$key]);
                // Tao order Hosting
                $data_hosting = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' => $data['user_id'],
                    'product_id' => $data['product_id'],
                    'domain' => $domain,
                    'billing_cycle' => $data['billing_cycle'],
                    'user' => $data['user'][$key],
                    'password' => $data['password'][$key],
                ];
                $update_hosting = $hosting->update($data_hosting);

                if(!$update_hosting) {
                    return false;
                }
            }
        }

        if($update_detail && $update_order) {
            return true;
        } else {
            return false;
        }

    }

    public function delete($id)
    {
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'xóa',
          'model' => 'Admin/Order',
          'description' => ' Order #' . $id,
        ];
        $this->log_activity->create($data_log);
        $delete = false;
        $detail_order = $this->detail_order->where('order_id', $id)->first();
        if(isset($detail_order)) {
            if ($detail_order->type == 'Hosting') {
                $hostings = $this->hosting->where('detail_order_id', $detail_order->id)->get();
                foreach ($hostings as $key => $hosting) {
                    $this->da->deleteAccount($hosting->user);
                }
                $this->hosting->where('detail_order_id', $detail_order->id)->delete();
                $detail_order->delete();
                $delete = $this->order->where('id', $id)->delete();
            } else if ($detail_order->type == 'Server') {
                $this->server->where('detail_order_id', $detail_order->id)->delete();
                $detail_order->delete();
                $delete = $this->order->where('id', $id)->delete();
            } else {
                $this->vps->where('detail_order_id', $detail_order->id)->delete();
                $detail_order->delete();
                $delete = $this->order->where('id', $id)->delete();
            }
        } else {
            $delete = $this->order->where('id', $id)->delete();
        }

        if($delete) {
            return true;
        } else {
            return false;
        }
    }

    public function get_order_confirm($id)
    {
        $detail_order = $this->order->find($id);
        return $detail_order;
    }

    function rand_string() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen( $chars );
        $str = '';
        for( $i = 0; $i < 16; $i++ ) {
            $str .= $chars[ rand( 0, $size - 1 ) ];
        }
        return $str;
    }

    public function confirm_order($data)
    {
        $order = $this->order->find($data['id']);
        $order->status = 'Active';
        $update = $order->save();
        $data_request = [];
        $detail_order = $order->detail_orders[0];
        // invoice
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        if($data['type'] == 'VPS') {
            $list_vps = $this->vps->where('detail_order_id', $detail_order->id)->get();
            $billing_cycle = $list_vps[0]->billing_cycle;
            $date_star = $detail_order->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$list_vps[0]->billing_cycle] ));

            if(!empty($list_vps)) {
                $reques_vcenter = $this->dashboard->createVPS($list_vps, $due_date); //request đên Vcenter
                if ($reques_vcenter) {
                    $order->status = 'Finish';
                    $update = $order->save();
                    $this->send_mail_finish('vps',$data['id'], $detail_order ,$list_vps);
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } elseif ($data['type']  == 'Hosting') {
            $hostings = $this->hosting->where('detail_order_id', $detail_order->id)->get();
            $billing_cycle = $hostings[0]->billing_cycle;
            $date_star = $detail_order->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$hostings[0]->billing_cycle] ));
            $request_da = $this->da->orderCreateAccount($hostings, $due_date); //request đên Direct Admin
            if ($request_da) {
                $order->status = 'Finish';
                $update = $order->save();
                $this->send_mail_finish('hosting',$data['id'], $detail_order ,$hostings);
            } else {
                return false;
            }
        }
        return true;
    }

    public function send_mail_finish($type, $order_id , $detail_order, $list_sevices)
    {
        $billings = config('billing');
        $order = $this->order->find($order_id);
        $product = $this->product->find($list_sevices[0]->product_id);
        $email = $this->email->find($product->meta_product->email_id);
        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $add_on =  '';
        if (!empty($detail_order->addon_id)) {
            $add_on = $this->product->find($list_sevices[0]->addon_id);
        }
        if ($type == 'vps') {
            foreach ($list_sevices as $key => $services) {
                $user = $this->user->find($services->user_id);
                $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], '', $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, $services->os, '', $url];
                $content = str_replace($search, $replace, $email->meta_email->content);

                $this->user_email = $this->user->find($services->user_id)->email;
                $this->subject = $email->meta_email->subject;

                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'subject' => $email->meta_email->subject,
                ];

                $data_tb_send_mail = [
                  'type' => 'mail_finish_order_vps',
                  'type_service' => 'vps',
                  'user_id' => $user->id,
                  'status' => false,
                  'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);
            }
        }
        elseif ($type == 'hosting') {
            foreach ($list_sevices as $key => $services) {
                  if (!empty($services->server_hosting_id)) {
                     $link_control_panel = '<a href="https://' . $services->server_hosting->host . ':' . $services->server_hosting->port . '" target="_blank" >https://'. $services->server_hosting->host . ':' . $services->server_hosting->port . '</a>' ;
                  } else {
                     $link_control_panel = '<a href="https://daportal.cloudzone.vn:2222/" target="_blank">https://daportal.cloudzone.vn:2222/</a>';
                  }
                  $user = $this->user->find($services->user_id);
                  $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}', '{$url_control_panel}'];
                  $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], '', $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url, $link_control_panel];
                  $content = str_replace($search, $replace, $email->meta_email->content);

                  $data = [
                      'content' => $content,
                      'user_name' => $user->name,
                      'subject' => $email->meta_email->subject
                  ];

                  $data_tb_send_mail = [
                    'type' => 'mail_finish_order_hosting',
                    'type_service' => 'hosting',
                    'user_id' => $user->id,
                    'status' => false,
                    'content' => serialize($data),
                  ];
                  $this->send_mail->create($data_tb_send_mail);
            }
        }
        elseif ($type == 'email') {
            $services = $list_sevices;
            $user = $this->user->find($services->user_id);
            $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}',];
            $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], '', $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url, ];
            $content = str_replace($search, $replace, $email->meta_email->content);

            $this->user_email = $this->user->find($services->user_id)->email;
            $this->subject = $email->meta_email->subject;

            $data = [
                  'content' => $content,
                  'user_name' => $user->name,
                  'subject' => $email->meta_email->subject
            ];
            $data_tb_send_mail = [
              'type' => 'mail_finish_order_email_hosting',
              'type_service' => 'email_hosting',
              'user_id' => $user->id,
              'status' => false,
              'content' => serialize($data),
            ];
            $this->send_mail->create($data_tb_send_mail);
        }
    }

    public function filter_user($user_id, $status)
    {
        $orders = $this->order->where('user_id', $user_id)->with('detail_orders', 'user')->orderBy('id', 'desc')->paginate(30);
        $data = [];
        $data['data'] = [];
        if ($orders->count() > 0) {
            if ($status != 'all') {
                if ($status == 'Active') {
                  $orders = $orders->where('status', 'Active');
                } elseif ($status == 'Finish') {
                  $orders = $orders->where('status', 'Finish');
                } elseif ($status == 'Pending') {
                  $orders = $orders->where('status', 'Pending');
                } else {
                  $orders = $orders->where('status', 'cancel');
                }
            }
            foreach ($orders as $key => $order) {
                $order->date_create = date('H:i:s d-m-Y', strtotime($order->created_at));
                $order->username = $order->user->name;
                if ($order->detail_orders->count() > 0) {
                    $order->deeta = 1;
                    $order->id_invoice = $order->detail_orders[0]->id;
                } else {
                    $order->deeta = 0;
                }
                if ( $order->type == 'expired' ) {
                  if ( !empty($order->detail_orders) ) {
                    foreach ( $order->detail_orders as $detail_order ) {
                      if ( $detail_order->type == 'VPS' ) {
                        if ( !empty($detail_order->order_expireds) ) {
                          foreach ( $detail_order->order_expireds as $order_expired ) {
                            $order->text_type_order .= 'Gia hạn VPS <a target="_blank" href=" '. route('admin.vps.detail' , $order_expired->expired_id ) .' "> ';
                            if (!empty($order_expired->vps->ip)) {
                              $order->text_type_order .= $order_expired->vps->ip;
                            } else {
                              $order->text_type_order .= 'đã xóa';
                            }
                            $order->text_type_order .= ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'VPS-US' || $detail_order->type == 'VPS US' ) {
                        if ( !empty($detail_order->order_expireds) ) {
                          foreach ( $detail_order->order_expireds as $order_expired ) {
                            $order->text_type_order .= 'Gia hạn VPS US <a target="_blank" href=" '. route('admin.vps.detail' , $order_expired->expired_id ) .' "> ';
                            if (!empty($order_expired->vps->ip)) {
                              $order->text_type_order .= $order_expired->vps->ip;
                            } else {
                              $order->text_type_order .= 'đã xóa';
                            }
                            $order->text_type_order .= ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'hosting' ) {
                        if ( !empty($detail_order->order_expireds) ) {
                          foreach ( $detail_order->order_expireds as $order_expired ) {
                            $order->text_type_order .= 'Gia hạn Hosting <a target="_blank" href=" '. route('admin.hosting.detail' , $order_expired->expired_id ) .' "> ';
                            if (!empty($order_expired->hosting->domain)) {
                              $order->text_type_order .= $order_expired->hosting->domain;
                            } else {
                              $order->text_type_order .= 'đã xóa';
                            }
                            $order->text_type_order .= ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'Email Hosting' ) {
                        if ( !empty($detail_order->order_expireds) ) {
                          foreach ( $detail_order->order_expireds as $order_expired ) {
                            $order->text_type_order .= 'Gia hạn Email Hosting <a target="_blank" href=" '. route('admin.email_hosting.detail' , $order_expired->expired_id ) .' "> ';
                            if (!empty($order_expired->email_hosting->domain)) {
                              $order->text_type_order .= $order_expired->email_hosting->domain;
                            } else {
                              $order->text_type_order .= 'đã xóa';
                            }
                            $order->text_type_order .= ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'Domain' ) {
                        if ( !empty($detail_order->order_expireds) ) {
                          foreach ( $detail_order->order_expireds as $order_expired ) {
                            $order->text_type_order .= 'Gia hạn Domain <a target="_blank" href=" '. route('admin.domain.detail' , $order_expired->expired_id ) .' "> ';
                            if (!empty($order_expired->domain->domain)) {
                              $order->text_type_order .= $order_expired->domain->domain;
                            } else {
                              $order->text_type_order .= 'đã xóa';
                            }
                            $order->text_type_order .= ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'Server' ) {
                        if ( !empty($detail_order->order_expireds) ) {
                          foreach ( $detail_order->order_expireds as $order_expired ) {
                            $order->text_type_order .= 'Gia hạn Server <a target="_blank" href=" '. route('admin.server.detail' , $order_expired->expired_id ) .' "> ';
                            if ( !empty($order_expired->server->ip) ) {
                              $order->text_type_order .= $order_expired->server->ip;
                            } else {
                              $order->text_type_order .= 'không có';
                            }
                            $order->text_type_order .= ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'Colocation' ) {
                        if ( !empty($detail_order->order_expireds) ) {
                          foreach ( $detail_order->order_expireds as $order_expired ) {
                            $order->text_type_order .= 'Gia hạn Colocation <a target="_blank" href=" '. route('admin.colocation.detail' , $order_expired->expired_id ) .' "> ';
                            if ( !empty($order_expired->colocation->ip) ) {
                              $order->text_type_order .= $order_expired->colocation->ip;
                            } else {
                              $order->text_type_order .= 'không có';
                            }
                            $order->text_type_order .= ' </a><br>';
                          }
                        }
                      }
                    }
                  }
                } else {
                  if ( !empty($order->detail_orders) ) {
                    foreach ( $order->detail_orders as $detail_order ) {
                      if ( $detail_order->type == 'addon_vps' ) {
                        if ( !empty($detail_order->order_addon_vps) ) {
                          foreach ( $detail_order->order_addon_vps as $order_addon_vps ) {
                            $vps = $order_addon_vps->vps;
                            if (!empty($vps->ip)) {
                              $order->text_type_order .= 'Cấu hình thêm VPS <a target="_blank" href=" '. route('admin.vps.detail' , $order_addon_vps->vps_id ) .' "> ' . $vps->ip . ' </a><br>';
                            } else {
                              $order->text_type_order .= 'Cấu hình thêm VPS';
                            }
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'addon_server' ) {
                        if ( !empty($detail_order->order_addon_servers) ) {
                          foreach ( $detail_order->order_addon_servers as $order_addon_server ) {
                            $server = $order_addon_server->server;
                            if (!empty($server->ip)) {
                              $order->text_type_order .= 'Cấu hình thêm Server <a target="_blank" href=" '. route('admin.server.detail' , $server->id ) .' "> ' . $server->ip . ' </a><br>';
                            } else {
                              $order->text_type_order .= 'Cấu hình thêm Server';
                            }
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'change_ip' ) {
                        if ( !empty($detail_order->order_change_vps) ) {
                          foreach ( $detail_order->order_change_vps as $order_change_vps ) {
                            if ( !empty($order_change_vps->vps_id) ) {
                              $vps = $order_change_vps->vps;
                              if (!empty($vps->ip)) {
                                $order->text_type_order .= 'Đổi IP VPS <a target="_blank" href=" '. route('admin.vps.detail' , $order_change_vps->vps_id ) .' "> ' . $vps->ip . ' </a><br>';
                              } else {
                                $order->text_type_order .= 'Đổi IP VPS';
                              }
                            }
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'VPS' ) {
                        if ( !empty($detail_order->vps) ) {
                          foreach ( $detail_order->vps as $vps ) {
                            $order->text_type_order .= 'Tạo VPS <a target="_blank" href=" '. route('admin.vps.detail' , $vps->id ) .' "> ' . $vps->ip . ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'VPS-US' || $detail_order->type == 'VPS US' ) {
                        if ( !empty($detail_order->vps) ) {
                          foreach ( $detail_order->vps as $vps ) {
                            $order->text_type_order .= 'Tạo VPS US <a target="_blank" href=" '. route('admin.vps.detail' , $vps->id ) .' "> ' . $vps->ip . ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'NAT-VPS' ) {
                        if ( !empty($detail_order->vps) ) {
                          foreach ( $detail_order->vps as $vps ) {
                            $order->text_type_order .= 'Tạo VPS <a target="_blank" href=" '. route('admin.vps.detail' , $vps->id ) .' "> ' . $vps->ip . ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'Server' ) {
                        if ( !empty($detail_order->servers) ) {
                          foreach ( $detail_order->servers as $server ) {
                            $order->text_type_order .= 'Tạo Server <a target="_blank" href=" '. route('admin.server.detail' , $server->id ) .' "> ' . $server->ip . ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'Colocation' ) {
                        if ( !empty($detail_order->colocations) ) {
                          foreach ( $detail_order->colocations as $server ) {
                            $order->text_type_order .= 'Tạo Colocation <a target="_blank" href=" '. route('admin.colocation.detail' , $server->id ) .' "> ' . $server->ip . ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'Hosting' ) {
                        if ( !empty($detail_order->hostings) ) {
                          foreach ( $detail_order->hostings as $hosting ) {
                            $order->text_type_order .= 'Tạo Hosting <a target="_blank" href=" '. route('admin.hosting.detail' , $hosting->id ) .' "> ' . $hosting->domain . ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'Email Hosting' ) {
                        if ( !empty($detail_order->email_hostings) ) {
                          foreach ( $detail_order->email_hostings as $hosting ) {
                            $order->text_type_order .= 'Tạo Email Hosting <a target="_blank" href=" '. route('admin.email_hosting.detail' , $hosting->id ) .' "> ' . $hosting->domain . ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'Domain' ) {
                        if ( !empty($detail_order->domain) ) {
                          foreach ( $detail_order->domain as $domain ) {
                            $order->text_type_order .= 'Tạo Domain <a target="_blank" href=" '. route('admin.domain.detail' , $domain->id ) .' "> ' . $domain->domain . ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'upgrade_hosting' ) {
                        if ( !empty($detail_order->order_upgrade_hosting) ) {
                          $hosting = $detail_order->order_upgrade_hosting->hosting;
                          if ( !empty($hosting->domain) ) {
                            $order->text_type_order .= 'Nâng cấp Hosting <a target="_blank" href=" '. route('admin.hosting.detail' , $hosting->id ) .' "> ' . $hosting->domain . ' </a><br>';
                          }
                        }
                      }
                    }
                  }
                }
                $data['data'][] = $order;
            }
        }
        $data['total'] = $orders->total();
        $data['perPage'] = $orders->perPage();
        $data['current_page'] = $orders->currentPage();
        return $data;
    }

    public function filter_status($user_id, $status)
    {
        $orders = $this->order->where('status', $status)->with('detail_orders', 'user')->orderBy('id', 'desc')->paginate(30);
        $data = [];
        $data['data'] = [];
        if ($orders->count() > 0) {
            if (!empty($user_id)) {
                $orders = $orders->where('user_id', $user_id);
            }
            if ($orders->count() > 0) {
                if (!empty($user_id)) {
                    $orders = $orders->where('user_id', $user_id);
                }
                foreach ($orders as $key => $order) {
                    $order->date_create = date('H:i:s d-m-Y', strtotime($order->created_at));
                    $order->username = !empty($order->user->name) ? $order->user->name : '<span class="text-danger">Đã xóa</span>';
                    if ($order->detail_orders->count() > 0) {
                        $order->deeta = 1;
                        $order->id_invoice = $order->detail_orders[0]->id;
                    } else {
                        $order->deeta = 0;
                    }
                    $order->text_type_order = '';
                    if ( $order->type == 'expired' ) {
                      if ( !empty($order->detail_orders) ) {
                        foreach ( $order->detail_orders as $detail_order ) {
                          if ( $detail_order->type == 'VPS' ) {
                            if ( !empty($detail_order->order_expireds) ) {
                              foreach ( $detail_order->order_expireds as $order_expired ) {
                                $order->text_type_order .= 'Gia hạn VPS <a target="_blank" href=" '. route('admin.vps.detail' , $order_expired->expired_id ) .' "> ';
                                if (!empty($order_expired->vps->ip)) {
                                  $order->text_type_order .= $order_expired->vps->ip;
                                } else {
                                  $order->text_type_order .= 'đã xóa';
                                }
                                $order->text_type_order .= ' </a><br>';
                              }
                            }
                          }
                          elseif ( $detail_order->type == 'hosting' ) {
                            if ( !empty($detail_order->order_expireds) ) {
                              foreach ( $detail_order->order_expireds as $order_expired ) {
                                $order->text_type_order .= 'Gia hạn Hosting <a target="_blank" href=" '. route('admin.hosting.detail' , $order_expired->expired_id ) .' "> ';
                                if (!empty($order_expired->hosting->domain)) {
                                  $order->text_type_order .= $order_expired->hosting->domain;
                                } else {
                                  $order->text_type_order .= 'đã xóa';
                                }
                                $order->text_type_order .= ' </a><br>';
                              }
                            }
                          }
                          elseif ( $detail_order->type == 'Email Hosting' ) {
                            if ( !empty($detail_order->order_expireds) ) {
                              foreach ( $detail_order->order_expireds as $order_expired ) {
                                $order->text_type_order .= 'Gia hạn Email Hosting <a target="_blank" href=" '. route('admin.email_hosting.detail' , $order_expired->expired_id ) .' "> ';
                                if (!empty($order_expired->email_hosting->domain)) {
                                  $order->text_type_order .= $order_expired->email_hosting->domain;
                                } else {
                                  $order->text_type_order .= 'đã xóa';
                                }
                                $order->text_type_order .= ' </a><br>';
                              }
                            }
                          }
                          elseif ( $detail_order->type == 'Domain' ) {
                            if ( !empty($detail_order->order_expireds) ) {
                              foreach ( $detail_order->order_expireds as $order_expired ) {
                                $order->text_type_order .= 'Gia hạn Domain <a target="_blank" href=" '. route('admin.domain.detail' , $order_expired->expired_id ) .' "> ';
                                if (!empty($order_expired->domain->domain)) {
                                  $order->text_type_order .= $order_expired->domain->domain;
                                } else {
                                  $order->text_type_order .= 'đã xóa';
                                }
                                $order->text_type_order .= ' </a><br>';
                              }
                            }
                          }
                          elseif ( $detail_order->type == 'Server' ) {
                            if ( !empty($detail_order->order_expireds) ) {
                              foreach ( $detail_order->order_expireds as $order_expired ) {
                                $order->text_type_order .= 'Gia hạn Server <a target="_blank" href=" '. route('admin.server.detail' , $order_expired->expired_id ) .' "> ';
                                if ( !empty($order_expired->server->ip) ) {
                                  $order->text_type_order .= $order_expired->server->ip;
                                } else {
                                  $order->text_type_order .= 'không có';
                                }
                                $order->text_type_order .= ' </a><br>';
                              }
                            }
                          }
                          elseif ( $detail_order->type == 'Colocation' ) {
                            if ( !empty($detail_order->order_expireds) ) {
                              foreach ( $detail_order->order_expireds as $order_expired ) {
                                $order->text_type_order .= 'Gia hạn Colocation <a target="_blank" href=" '. route('admin.colocation.detail' , $order_expired->expired_id ) .' "> ';
                                if ( !empty($order_expired->colocation->ip) ) {
                                  $order->text_type_order .= $order_expired->colocation->ip;
                                } else {
                                  $order->text_type_order .= 'không có';
                                }
                                $order->text_type_order .= ' </a><br>';
                              }
                            }
                          }
                        }
                      }
                    } else {
                      if ( !empty($order->detail_orders) ) {
                        foreach ( $order->detail_orders as $detail_order ) {
                          if ( $detail_order->type == 'addon_vps' ) {
                            if ( !empty($detail_order->order_addon_vps) ) {
                              foreach ( $detail_order->order_addon_vps as $order_addon_vps ) {
                                $vps = $order_addon_vps->vps;
                                if (!empty($vps->ip)) {
                                  $order->text_type_order .= 'Cấu hình thêm <a target="_blank" href=" '. route('admin.vps.detail' , $order_addon_vps->vps_id ) .' "> ' . $vps->ip . ' </a><br>';
                                } else {
                                  $order->text_type_order .= 'Cấu hình thêm';
                                }
                              }
                            }
                          }
                          elseif ( $detail_order->type == 'addon_server' ) {
                            if ( !empty($detail_order->order_addon_servers) ) {
                              foreach ( $detail_order->order_addon_servers as $order_addon_server ) {
                                $server = $order_addon_server->server;
                                if (!empty($server->ip)) {
                                  $order->text_type_order .= 'Cấu hình thêm Server <a target="_blank" href=" '. route('admin.server.detail' , $server->id ) .' "> ' . $server->ip . ' </a><br>';
                                } else {
                                  $order->text_type_order .= 'Cấu hình thêm Server';
                                }
                              }
                            }
                          }
                          elseif ( $detail_order->type == 'change_ip' ) {
                            if ( !empty($detail_order->order_change_vps) ) {
                              foreach ( $detail_order->order_change_vps as $order_change_vps ) {
                                if ( !empty($order_change_vps->vps_id) ) {
                                  $vps = $order_change_vps->vps;
                                  if (!empty($vps->ip)) {
                                    $order->text_type_order .= 'Đổi IP VPS <a target="_blank" href=" '. route('admin.vps.detail' , $order_change_vps->vps_id ) .' "> ' . $vps->ip . ' </a><br>';
                                  } else {
                                    $order->text_type_order .= 'Đổi IP VPS';
                                  }
                                }
                              }
                            }
                          }
                          elseif ( $detail_order->type == 'VPS' ) {
                            if ( !empty($detail_order->vps) ) {
                              foreach ( $detail_order->vps as $vps ) {
                                $order->text_type_order .= 'Tạo VPS <a target="_blank" href=" '. route('admin.vps.detail' , $vps->id ) .' "> ' . $vps->ip . ' </a><br>';
                              }
                            }
                          }
                          elseif ( $detail_order->type == 'Hosting' ) {
                            if ( !empty($detail_order->hostings) ) {
                              foreach ( $detail_order->hostings as $hosting ) {
                                $order->text_type_order .= 'Tạo Hosting <a target="_blank" href=" '. route('admin.hosting.detail' , $hosting->id ) .' "> ' . $hosting->domain . ' </a><br>';
                              }
                            }
                          }
                          elseif ( $detail_order->type == 'Email Hosting' ) {
                            if ( !empty($detail_order->email_hostings) ) {
                              foreach ( $detail_order->email_hostings as $hosting ) {
                                $order->text_type_order .= 'Tạo Email Hosting <a target="_blank" href=" '. route('admin.email_hosting.detail' , $hosting->id ) .' "> ' . $hosting->domain . ' </a><br>';
                              }
                            }
                          }
                          elseif ( $detail_order->type == 'NAT-VPS' ) {
                            if ( !empty($detail_order->vps) ) {
                              foreach ( $detail_order->vps as $vps ) {
                                $order->text_type_order .= 'Tạo VPS <a target="_blank" href=" '. route('admin.vps.detail' , $vps->id ) .' "> ' . $vps->ip . ' </a><br>';
                              }
                            }
                          }
                          elseif ( $detail_order->type == 'Server' ) {
                            if ( !empty($detail_order->servers) ) {
                              foreach ( $detail_order->servers as $server ) {
                                $order->text_type_order .= 'Tạo Server <a target="_blank" href=" '. route('admin.server.detail' , $server->id ) .' "> ' . $server->ip . ' </a><br>';
                              }
                            }
                          }
                          elseif ( $detail_order->type == 'Colocation' ) {
                            if ( !empty($detail_order->colocations) ) {
                              foreach ( $detail_order->colocations as $server ) {
                                $order->text_type_order .= 'Tạo Colocation <a target="_blank" href=" '. route('admin.colocation.detail' , $server->id ) .' "> ' . $server->ip . ' </a><br>';
                              }
                            }
                          }
                          elseif ( $detail_order->type == 'Domain' ) {
                            if ( !empty($detail_order->domain) ) {
                              foreach ( $detail_order->domain as $domain ) {
                                $order->text_type_order .= 'Tạo Domain <a target="_blank" href=" '. route('admin.domain.detail' , $domain->id ) .' "> ' . $domain->domain . ' </a><br>';
                              }
                            }
                          }
                          elseif ( $detail_order->type == 'upgrade_hosting' ) {
                            if ( !empty($detail_order->order_upgrade_hosting) ) {
                              $hosting = $detail_order->order_upgrade_hosting->hosting;
                              if ( !empty($hosting->domain) ) {
                                $order->text_type_order .= 'Nâng cấp Hosting <a target="_blank" href=" '. route('admin.hosting.detail' , $hosting->id ) .' "> ' . $hosting->domain . ' </a><br>';
                              }
                            }
                          }
                        }
                      }
                    }
                    $data['data'][] = $order;
                }
            }

        }
        $data['total'] = $orders->total();
        $data['perPage'] = $orders->perPage();
        $data['current_page'] = $orders->currentPage();
        return $data;
    }

    public function list_order_of_personal()
    {
        $orders = $this->order->from('orders as o')->select(['o.*'])->join('users as u','o.user_id','=','u.id')->where('u.enterprise', false)->with('detail_orders', 'user')->orderBy('o.id', 'desc')->paginate(30);
        // dd($orders);
        $data = [];
        if ($orders->count() > 0) {
            foreach ($orders as $key => $order) {
                $order->date_create = date('H:i:s d-m-Y', strtotime($order->created_at));
                $order->username = $order->user->name;
                if ($order->detail_orders->count() > 0) {
                    $order->deeta = 1;
                    $order->id_invoice = $order->detail_orders[0]->id;
                } else {
                    $order->deeta = 0;
                }

                $order->text_type_order = '';
                if ( $order->type == 'expired' ) {
                    if ( !empty($order->detail_orders) ) {
                        foreach ( $order->detail_orders as $detail_order ) {
                            if ( $detail_order->type == 'VPS' ) {
                                if ( !empty($detail_order->order_expireds) ) {
                                    foreach ( $detail_order->order_expireds as $order_expired ) {
                                        $order->text_type_order .= 'Gia hạn VPS <a target="_blank" href=" '. route('admin.vps.detail' , $order_expired->expired_id ) .' "> ';
                                        if (!empty($order_expired->vps->ip)) {
                                          $order->text_type_order .= $order_expired->vps->ip;
                                        } else {
                                          $order->text_type_order .= 'đã xóa';
                                        }
                                        $order->text_type_order .= ' </a><br>';
                                    }
                                }
                            }
                            elseif ( $detail_order->type == 'hosting' ) {
                                if ( !empty($detail_order->order_expireds) ) {
                                    foreach ( $detail_order->order_expireds as $order_expired ) {
                                        $order->text_type_order .= 'Gia hạn Hosting <a target="_blank" href=" '. route('admin.hosting.detail' , $order_expired->expired_id ) .' "> ';
                                        if (!empty($order_expired->hosting->domain)) {
                                          $order->text_type_order .= $order_expired->hosting->domain;
                                        } else {
                                          $order->text_type_order .= 'đã xóa';
                                        }
                                        $order->text_type_order .= ' </a><br>';
                                    }
                                }
                            }
                            elseif ( $detail_order->type == 'Email Hosting' ) {
                                if ( !empty($detail_order->order_expireds) ) {
                                    foreach ( $detail_order->order_expireds as $order_expired ) {
                                        $order->text_type_order .= 'Gia hạn Email Hosting <a target="_blank" href=" '. route('admin.hosting.detail' , $order_expired->expired_id ) .' "> ';
                                        if (!empty($order_expired->email_hosting->domain)) {
                                          $order->text_type_order .= $order_expired->email_hosting->domain;
                                        } else {
                                          $order->text_type_order .= 'đã xóa';
                                        }
                                        $order->text_type_order .= ' </a><br>';
                                    }
                                }
                            }
                            elseif ( $detail_order->type == 'Domain' ) {
                                if ( !empty($detail_order->order_expireds) ) {
                                    foreach ( $detail_order->order_expireds as $order_expired ) {
                                        $order->text_type_order .= 'Gia hạn <a target="_blank" href=" '. route('admin.domain.detail' , $order_expired->expired_id ) .' "> ';
                                        if (!empty($order_expired->domain->domain)) {
                                          $order->text_type_order .= $order_expired->domain->domain;
                                        } else {
                                          $order->text_type_order .= 'đã xóa';
                                        }
                                        $order->text_type_order .= ' </a><br>';
                                    }
                                }
                            }
                            elseif ( $detail_order->type == 'Server' ) {
                              if ( !empty($detail_order->order_expireds) ) {
                                foreach ( $detail_order->order_expireds as $order_expired ) {
                                  $order->text_type_order .= 'Gia hạn Server <a target="_blank" href=" '. route('admin.server.detail' , $order_expired->expired_id ) .' "> ';
                                  if ( !empty($order_expired->server->ip) ) {
                                    $order->text_type_order .= $order_expired->server->ip;
                                  } else {
                                    $order->text_type_order .= 'không có';
                                  }
                                  $order->text_type_order .= ' </a><br>';
                                }
                              }
                            }
                            elseif ( $detail_order->type == 'Colocation' ) {
                              if ( !empty($detail_order->order_expireds) ) {
                                foreach ( $detail_order->order_expireds as $order_expired ) {
                                  $order->text_type_order .= 'Gia hạn Colocation <a target="_blank" href=" '. route('admin.colocation.detail' , $order_expired->expired_id ) .' "> ';
                                  if ( !empty($order_expired->colocation->ip) ) {
                                    $order->text_type_order .= $order_expired->colocation->ip;
                                  } else {
                                    $order->text_type_order .= 'không có';
                                  }
                                  $order->text_type_order .= ' </a><br>';
                                }
                              }
                            }
                        }
                    }
                }
                else {
                    if ( !empty($order->detail_orders) ) {
                        foreach ( $order->detail_orders as $detail_order ) {
                            if ( $detail_order->type == 'addon_vps' ) {
                                if ( !empty($detail_order->order_addon_vps) ) {
                                    foreach ( $detail_order->order_addon_vps as $order_addon_vps ) {
                                          $vps = $order_addon_vps->vps;
                                          if (!empty($vps->ip)) {
                                              $order->text_type_order .= 'Cấu hình thêm <a target="_blank" href=" '. route('admin.vps.detail' , $order_addon_vps->vps_id ) .' "> ' . $vps->ip . ' </a><br>';
                                          } else {
                                              $order->text_type_order .= 'Cấu hình thêm';
                                          }
                                    }
                                }
                            }
                            elseif ( $detail_order->type == 'change_ip' ) {
                                if ( !empty($detail_order->order_change_vps) ) {
                                    foreach ( $detail_order->order_change_vps as $order_change_vps ) {
                                        if ( !empty($order_change_vps->vps_id) ) {
                                            $vps = $order_change_vps->vps;
                                            if (!empty($vps->ip)) {
                                                $order->text_type_order .= 'Đổi IP VPS <a target="_blank" href=" '. route('admin.vps.detail' , $order_change_vps->vps_id ) .' "> ' . $vps->ip . ' </a><br>';
                                            } else {
                                                $order->text_type_order .= 'Đổi IP VPS';
                                            }
                                        }
                                    }
                                }
                            }
                            elseif ( $detail_order->type == 'VPS' ) {
                                if ( !empty($detail_order->vps) ) {
                                    foreach ( $detail_order->vps as $vps ) {
                                          $order->text_type_order .= 'Tạo VPS <a target="_blank" href=" '. route('admin.vps.detail' , $vps->id ) .' "> ' . $vps->ip . ' </a><br>';
                                    }
                                }
                            }
                            elseif ( $detail_order->type == 'addon_server' ) {
                              if ( !empty($detail_order->order_addon_servers) ) {
                                foreach ( $detail_order->order_addon_servers as $order_addon_server ) {
                                  $server = $order_addon_server->server;
                                  if (!empty($server->ip)) {
                                    $order->text_type_order .= 'Cấu hình thêm Server <a target="_blank" href=" '. route('admin.server.detail' , $server->id ) .' "> ' . $server->ip . ' </a><br>';
                                  } else {
                                    $order->text_type_order .= 'Cấu hình thêm Server';
                                  }
                                }
                              }
                            }
                            elseif ( $detail_order->type == 'NAT-VPS' ) {
                              if ( !empty($detail_order->vps) ) {
                                foreach ( $detail_order->vps as $vps ) {
                                  $order->text_type_order .= 'Tạo VPS <a target="_blank" href=" '. route('admin.vps.detail' , $vps->id ) .' "> ' . $vps->ip . ' </a><br>';
                                }
                              }
                            }
                            elseif ( $detail_order->type == 'Server' ) {
                              if ( !empty($detail_order->servers) ) {
                                foreach ( $detail_order->servers as $server ) {
                                  $order->text_type_order .= 'Tạo Server <a target="_blank" href=" '. route('admin.server.detail' , $server->id ) .' "> ' . $server->ip . ' </a><br>';
                                }
                              }
                            }
                            elseif ( $detail_order->type == 'Colocation' ) {
                              if ( !empty($detail_order->colocations) ) {
                                foreach ( $detail_order->colocations as $server ) {
                                  $order->text_type_order .= 'Tạo Colocation <a target="_blank" href=" '. route('admin.colocation.detail' , $server->id ) .' "> ' . $server->ip . ' </a><br>';
                                }
                              }
                            }
                            elseif ( $detail_order->type == 'Hosting' ) {
                                if ( !empty($detail_order->hostings) ) {
                                    foreach ( $detail_order->hostings as $hosting ) {
                                          $order->text_type_order .= 'Tạo Hosting <a target="_blank" href=" '. route('admin.hosting.detail' , $hosting->id ) .' "> ' . $hosting->domain . ' </a><br>';
                                    }
                                }
                            }
                            elseif ( $detail_order->type == 'Email Hosting' ) {
                                if ( !empty($detail_order->email_hostings) ) {
                                    foreach ( $detail_order->email_hostings as $hosting ) {
                                          $order->text_type_order .= 'Tạo Email Hosting <a target="_blank" href=" '. route('admin.email_hosting.detail' , $hosting->id ) .' "> ' . $hosting->domain . ' </a><br>';
                                    }
                                }
                            }
                            elseif ( $detail_order->type == 'Domain' ) {
                                if ( !empty($detail_order->domain) ) {
                                    foreach ( $detail_order->domain as $domain ) {
                                          $order->text_type_order .= 'Tạo Domain <a target="_blank" href=" '. route('admin.domain.detail' , $domain->id ) .' "> ' . $domain->domain . ' </a><br>';
                                    }
                                }
                            }
                            elseif ( $detail_order->type == 'upgrade_hosting' ) {
                                if ( !empty($detail_order->order_upgrade_hosting) ) {
                                    $hosting = $detail_order->order_upgrade_hosting->hosting;
                                    if ( !empty($hosting->domain) ) {
                                      $order->text_type_order .= 'Nâng cấp Hosting <a target="_blank" href=" '. route('admin.hosting.detail' , $hosting->id ) .' "> ' . $hosting->domain . ' </a><br>';
                                    }
                                }
                            }
                        }
                    }
                }
                $data['data'][] = $order;
            }
        }
        $data['total'] = $orders->total();
        $data['perPage'] = $orders->perPage();
        $data['current_page'] = $orders->currentPage();
        return $data;
    }

    public function list_order_of_enterprise()
    {
        $orders = $this->order->from('orders as o')->select(['o.*'])->join('users as u','o.user_id','=','u.id')->where('u.enterprise', true)->with('detail_orders', 'user')->orderBy('o.id', 'desc')->paginate(30);
        $data = [];
        $data['data'] = [];
        if ($orders->count() > 0) {
            if (!empty($user_id)) {
                $orders = $orders->where('user_id', $user_id);
            }
            foreach ($orders as $key => $order) {
                $order->date_create = date('H:i:s d-m-Y', strtotime($order->created_at));
                $order->username = $order->user->name;
                if ($order->detail_orders->count() > 0) {
                    $order->deeta = 1;
                    $order->id_invoice = $order->detail_orders[0]->id;
                } else {
                    $order->deeta = 0;
                }
                $order->text_type_order = '';
                if ( $order->type == 'expired' ) {
                  if ( !empty($order->detail_orders) ) {
                    foreach ( $order->detail_orders as $detail_order ) {
                      if ( $detail_order->type == 'VPS' ) {
                        if ( !empty($detail_order->order_expireds) ) {
                          foreach ( $detail_order->order_expireds as $order_expired ) {
                            $order->text_type_order .= 'Gia hạn <a target="_blank" href=" '. route('admin.vps.detail' , $order_expired->expired_id ) .' "> ';
                            if (!empty($order_expired->vps->ip)) {
                              $order->text_type_order .= $order_expired->vps->ip;
                            } else {
                              $order->text_type_order .= 'đã xóa';
                            }
                            $order->text_type_order .= ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'hosting' ) {
                        if ( !empty($detail_order->order_expireds) ) {
                          foreach ( $detail_order->order_expireds as $order_expired ) {
                            $order->text_type_order .= 'Gia hạn <a target="_blank" href=" '. route('admin.hosting.detail' , $order_expired->expired_id ) .' "> ';
                            if (!empty($order_expired->hosting->domain)) {
                              $order->text_type_order .= $order_expired->hosting->domain;
                            } else {
                              $order->text_type_order .= 'đã xóa';
                            }
                            $order->text_type_order .= ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'Email Hosting' ) {
                        if ( !empty($detail_order->order_expireds) ) {
                            foreach ( $detail_order->order_expireds as $order_expired ) {
                                $order->text_type_order .= 'Gia hạn Email Hosting <a target="_blank" href=" '. route('admin.hosting.detail' , $order_expired->expired_id ) .' "> ';
                                if (!empty($order_expired->email_hosting->domain)) {
                                    $order->text_type_order .= $order_expired->email_hosting->domain;
                                } else {
                                  $order->text_type_order .= 'đã xóa';
                                }
                                $order->text_type_order .= ' </a><br>';
                            }
                        }
                      }
                      elseif ( $detail_order->type == 'Domain' ) {
                        if ( !empty($detail_order->order_expireds) ) {
                          foreach ( $detail_order->order_expireds as $order_expired ) {
                            $order->text_type_order .= 'Gia hạn <a target="_blank" href=" '. route('admin.domain.detail' , $order_expired->expired_id ) .' "> ';
                            if (!empty($order_expired->domain->domain)) {
                              $order->text_type_order .= $order_expired->domain->domain;
                            } else {
                              $order->text_type_order .= 'đã xóa';
                            }
                            $order->text_type_order .= ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'Server' ) {
                        if ( !empty($detail_order->order_expireds) ) {
                          foreach ( $detail_order->order_expireds as $order_expired ) {
                            $order->text_type_order .= 'Gia hạn Server <a target="_blank" href=" '. route('admin.server.detail' , $order_expired->expired_id ) .' "> ';
                            if ( !empty($order_expired->server->ip) ) {
                              $order->text_type_order .= $order_expired->server->ip;
                            } else {
                              $order->text_type_order .= 'không có';
                            }
                            $order->text_type_order .= ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'Colocation' ) {
                          if ( !empty($detail_order->order_expireds) ) {
                            foreach ( $detail_order->order_expireds as $order_expired ) {
                                $order->text_type_order .= 'Gia hạn Colocation <a target="_blank" href=" '. route('admin.colocation.detail' , $order_expired->expired_id ) .' "> ';
                                if ( !empty($order_expired->colocation->ip) ) {
                                    $order->text_type_order .= $order_expired->colocation->ip;
                                } else {
                                    $order->text_type_order .= 'không có';
                                }
                                $order->text_type_order .= ' </a><br>';
                            }
                          }
                      }
                    }
                  }
                } else {
                  if ( !empty($order->detail_orders) ) {
                    foreach ( $order->detail_orders as $detail_order ) {
                      if ( $detail_order->type == 'addon_vps' ) {
                        if ( !empty($detail_order->order_addon_vps) ) {
                          foreach ( $detail_order->order_addon_vps as $order_addon_vps ) {
                            $order->text_type_order .= 'Cấu hình thêm <a target="_blank" href=" '. route('admin.vps.detail' , $order_addon_vps->vps_id ) .' "> ';
                            if (!empty($order_addon_vps->vps->ip)) {
                              $order->text_type_order .= $order_addon_vps->vps->ip;
                            } else {
                              $order->text_type_order .= 'đã xóa';
                            }
                            $order->text_type_order .= ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'change_ip' ) {
                        if ( !empty($detail_order->order_change_vps) ) {
                          foreach ( $detail_order->order_change_vps as $order_change_vps ) {
                            $order->text_type_order .= 'Đổi IP VPS <a target="_blank" href=" '. route('admin.vps.detail' , $order_change_vps->vps_id ) .' "> ';
                            if (!empty($order_change_vps->vps->ip)) {
                              $order->text_type_order .= $order_change_vps->vps->ip;
                            } else {
                              $order->text_type_order .= 'đã xóa';
                            }
                            $order->text_type_order .= ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'VPS' ) {
                        if ( !empty($detail_order->vps) ) {
                          foreach ( $detail_order->vps as $vps ) {
                            $order->text_type_order .= 'Tạo VPS <a target="_blank" href=" '. route('admin.vps.detail' , $vps->id ) .' "> ' . $vps->ip . ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'Hosting' ) {
                        if ( !empty($detail_order->hostings) ) {
                          foreach ( $detail_order->hostings as $hosting ) {
                            $order->text_type_order .= 'Tạo Hosting <a target="_blank" href=" '. route('admin.hosting.detail' , $hosting->id ) .' "> ' . $hosting->domain . ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'Email Hosting' ) {
                        if ( !empty($detail_order->email_hostings) ) {
                            foreach ( $detail_order->email_hostings as $hosting ) {
                              $order->text_type_order .= 'Tạo Email Hosting <a target="_blank" href=" '. route('admin.email_hosting.detail' , $hosting->id ) .' "> ' . $hosting->domain . ' </a><br>';
                            }
                        }
                      }
                      elseif ( $detail_order->type == 'addon_server' ) {
                        if ( !empty($detail_order->order_addon_servers) ) {
                          foreach ( $detail_order->order_addon_servers as $order_addon_server ) {
                            $server = $order_addon_server->server;
                            if (!empty($server->ip)) {
                              $order->text_type_order .= 'Cấu hình thêm Server <a target="_blank" href=" '. route('admin.server.detail' , $server->id ) .' "> ' . $server->ip . ' </a><br>';
                            } else {
                              $order->text_type_order .= 'Cấu hình thêm Server';
                            }
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'NAT-VPS' ) {
                        if ( !empty($detail_order->vps) ) {
                          foreach ( $detail_order->vps as $vps ) {
                            $order->text_type_order .= 'Tạo VPS <a target="_blank" href=" '. route('admin.vps.detail' , $vps->id ) .' "> ' . $vps->ip . ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'Server' ) {
                        if ( !empty($detail_order->servers) ) {
                          foreach ( $detail_order->servers as $server ) {
                            $order->text_type_order .= 'Tạo Server <a target="_blank" href=" '. route('admin.server.detail' , $server->id ) .' "> ' . $server->ip . ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'Colocation' ) {
                          if ( !empty($detail_order->colocations) ) {
                            foreach ( $detail_order->colocations as $server ) {
                              $order->text_type_order .= 'Tạo Colocation <a target="_blank" href=" '. route('admin.colocation.detail' , $server->id ) .' "> ' . $server->ip . ' </a><br>';
                            }
                          }
                      }
                      elseif ( $detail_order->type == 'Domain' ) {
                        if ( !empty($detail_order->domain) ) {
                          foreach ( $detail_order->domain as $domain ) {
                            $order->text_type_order .= 'Tạo Domain <a target="_blank" href=" '. route('admin.domain.detail' , $domain->id ) .' "> ' . $domain->domain . ' </a><br>';
                          }
                        }
                      }
                      elseif ( $detail_order->type == 'upgrade_hosting' ) {
                        if ( !empty($detail_order->order_upgrade_hosting) ) {
                          $hosting = $detail_order->order_upgrade_hosting->hosting;
                          if ( !empty($hosting->domain) ) {
                            $order->text_type_order .= 'Nâng cấp Hosting <a target="_blank" href=" '. route('admin.hosting.detail' , $hosting->id ) .' "> ' . $hosting->domain . ' </a><br>';
                          }
                        }
                      }
                    }
                  }
                }
                $data['data'][] = $order;
            }
        }
        $data['total'] = $orders->total();
        $data['perPage'] = $orders->perPage();
        $data['current_page'] = $orders->currentPage();
        return $data;
    }

    // list dịch vụ đang on
    public function get_vps_with_user($user_id)
    {
        $list_vps = $this->vps->where('user_id', $user_id)
              ->where(function($query)
              {
                  return $query
                    ->orwhere('status_vps', 'on')
                    ->orWhere('status_vps', 'off')
                    ->orWhere('status_vps', 'progressing')
                    ->orWhere('status_vps', 'change_ip')
                    ->orWhere('status_vps', 'rebuild');
              })
              ->orderBy('id', 'desc')->with('product', 'vps_config')->get();
        $data = [];
        if ($list_vps->count() > 0) {
            foreach ($list_vps as $key => $vps) {
                $product = $vps->product;

                if(!empty($product->meta_product)) {
                  $cpu = $product->meta_product->cpu;
                  $ram = $product->meta_product->memory;
                  $disk = $product->meta_product->disk;
                } else {
                  $cpu = 1;
                  $ram = 1;
                  $disk = 20;
                }
                if (!empty($product->meta_product->product_special)) {
                  // Addon
                  if (!empty($vps->vps_config)) {
                    $vps_config = $vps->vps_config;
                    if (!empty($vps_config)) {

                      $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                      $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                      $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                    }
                  }
                }
                $vps->config = $cpu . ' CPU ' . $ram . ' RAM ' . $disk . ' DISK';
                $data[] = $vps;
            }
        }
        return $data;
    }

    public function get_vps_with_user_expire($user_id)
    {
      $list_vps = $this->vps->where('user_id', $user_id)
            ->where(function($query)
            {
                return $query
                  ->orwhere('status_vps', 'on')
                  ->orWhere('status_vps', 'off')
                  ->orWhere('status_vps', 'progressing')
                  ->orWhere('status_vps', 'change_ip')
                  ->orWhere('status_vps', 'expire')
                  ->orWhere('status_vps', 'rebuild');
            })
            ->where('location', 'cloudzone')
            ->orderBy('id', 'desc')->with('product', 'vps_config')->get();
        $data = [];
        $billings = config('billing');
        if ($list_vps->count() > 0) {
            foreach ($list_vps as $key => $vps) {
                if ($vps->status_vps != 'delete_vps') {
                   if ($vps->status_vps != 'cancel') {
                       $product = $vps->product;
                       if(!empty($product->meta_product)) {
                         $cpu = $product->meta_product->cpu;
                         $ram = $product->meta_product->memory;
                         $disk = $product->meta_product->disk;
                       } else {
                         $cpu = 1;
                         $ram = 1;
                         $disk = 20;
                       }
                       if (!empty($product->meta_product->product_special)) {
                         // Addon
                         if (!empty($vps->vps_config)) {
                           $vps_config = $vps->vps_config;
                           if (!empty($vps_config)) {

                             $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                             $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                             $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                           }
                         }
                       }
                       $vps->config = $cpu . ' CPU ' . $ram . ' RAM ' . $disk . ' DISK';
                       if (!empty($vps->price_override)) {
                         if ($vps->billing_cycle == 'monthly') {
                           $vps->text_price = ' <span class="text-danger">(' . number_format($vps->price_override,0,",",".") . ' VNĐ / 1 Tháng)</span>';
                           $vps->total_vps = $vps->price_override;
                         } else {
                           $billingDashBoard = config('billingDashBoard');
                           $vps->text_price = ' <span class="text-danger">(' . number_format($vps->price_override / $billingDashBoard[$vps->billing_cycle],0,",",".") . ' VNĐ / 1 Tháng)</span>';
                           $vps->total_vps = $vps->price_override / $billingDashBoard[$vps->billing_cycle];
                         }
                       } else {
                         $product = $vps->product;
                         $sub_total = !empty( $product->pricing['monthly'] ) ? $product->pricing['monthly'] : 0;
                         if (!empty($vps->vps_config)) {
                             $addon_vps = $vps->vps_config;
                             $add_on_products = $this->get_addon_product_private(Auth::user()->id);
                             $pricing_addon = 0;
                             foreach ($add_on_products as $key => $add_on_product) {
                                 if (!empty($add_on_product->meta_product->type_addon)) {
                                       if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                         $sub_total += $addon_vps->cpu * $add_on_product->pricing['monthly'];
                                       }
                                       if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                         $sub_total += $addon_vps->ram * $add_on_product->pricing['monthly'];
                                       }
                                       if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                         $sub_total += $addon_vps->disk * $add_on_product->pricing['monthly'] / 10;
                                       }
                                 }
                             }
                         }
                         $vps->text_price = ' <span>(' . number_format($sub_total,0,",",".") . ' VNĐ / 1 Tháng)</span>';
                         $vps->total_vps = $sub_total;
                       }
                       $data[] = $vps;
                   }
                }
            }
        }
        return $data;
    }

    public function get_vps_us_with_user_expire($user_id)
    {
      $list_vps = $this->vps->where('user_id', $user_id)
            ->where(function($query)
            {
                return $query
                  ->orwhere('status_vps', 'on')
                  ->orWhere('status_vps', 'off')
                  ->orWhere('status_vps', 'progressing')
                  ->orWhere('status_vps', 'change_ip')
                  ->orWhere('status_vps', 'expire')
                  ->orWhere('status_vps', 'rebuild');
            })
            ->where('location', 'us')
            ->orderBy('id', 'desc')->with('product', 'vps_config')->get();
        $data = [];
        $billings = config('billing');
        if ($list_vps->count() > 0) {
            foreach ($list_vps as $key => $vps) {
                if ($vps->status_vps != 'delete_vps') {
                   if ($vps->status_vps != 'cancel') {
                       $product = $vps->product;
                       if(!empty($product->meta_product)) {
                         $cpu = $product->meta_product->cpu;
                         $ram = $product->meta_product->memory;
                         $disk = $product->meta_product->disk;
                       } else {
                         $cpu = 1;
                         $ram = 1;
                         $disk = 20;
                       }
                       if (!empty($product->meta_product->product_special)) {
                         // Addon
                         if (!empty($vps->vps_config)) {
                           $vps_config = $vps->vps_config;
                           if (!empty($vps_config)) {

                             $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                             $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                             $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                           }
                         }
                       }
                       $vps->config = $cpu . ' CPU ' . $ram . ' RAM ' . $disk . ' DISK';
                       if (!empty($vps->price_override)) {
                         $vps->text_price = ' <span class="text-danger">(' . number_format($vps->price_override,0,",",".") . ' VNĐ / ' . $billings[$vps->billing_cycle] . ')</span>';
                         $vps->total_vps = $vps->price_override;
                       } else {
                         $product = $vps->product;
                         $sub_total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                         if (!empty($vps->vps_config)) {
                             $addon_vps = $vps->vps_config;
                             $add_on_products = $this->get_addon_product_private(Auth::user()->id);
                             $pricing_addon = 0;
                             foreach ($add_on_products as $key => $add_on_product) {
                                 if (!empty($add_on_product->meta_product->type_addon)) {
                                       if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                         $sub_total += $addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                                       }
                                       if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                         $sub_total += $addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                                       }
                                       if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                         $sub_total += $addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                                       }
                                 }
                             }
                         }
                         $vps->text_price = ' <span>(' . number_format($sub_total,0,",",".") . ' VNĐ / ' . $billings[$vps->billing_cycle] . ')</span>';
                         $vps->total_vps = $sub_total;
                       }
                       $data[] = $vps;
                   }
                }
            }
        }
        return $data;
    }

    public function get_server_with_user_expire($user_id)
    {
      $list_servers = $this->server->where('user_id', $user_id)
            ->where(function($query)
            {
                return $query
                  ->orwhere('status_server', 'on')
                  ->orWhere('status_server', 'off');
            })
            ->orderBy('id', 'desc')->with('server_config')->get();
        $data = [];
        $billings = config('billing');
        if ($list_servers->count() > 0) {
            foreach ($list_servers as $key => $server) {
                if ($server->status_server != 'delete_server') {
                   if ($server->status_server != 'cancel') {
                       $ip = !empty($server->ip) ? 1 : 0;
                       $ram = $server->ram;
                       $disk = $server->disk;
                       // Addon
                       if (!empty($server->server_config)) {
                         $server_config = $server->server_config;
                         if (!empty($server_config)) {
                           $ip += (int) !empty($server_config->ip) ? $server_config->ip : 0;
                           $ram += (int) !empty($server_config->ram) ? $server_config->ram : 0;
                           $disk += (int) !empty($server_config->disk) ? $server_config->disk : 0;
                         }
                       }
                       $server->config = $ip . ' IP ' . $ram . ' RAM ' . $disk . ' DISK';
                       if (!empty($server->amount)) {
                         $server->text_price = ' <span class="text-danger">(' . number_format($server->amount,0,",",".") . ' VNĐ / ' . $billings[$server->billing_cycle] . ')</span>';
                         $server->total_vps = $server->amount;
                       }
                       $data[] = $server;
                   }
                }
            }
        }
        return $data;
    }

    public function list_expire_colocation($user_id)
    {
      $list_colocations = $this->colocation->where('user_id', $user_id)
            ->where(function($query)
            {
                return $query
                  ->orwhere('status_colo', 'on')
                  ->orWhere('status_colo', 'off');
            })
            ->orderBy('id', 'desc')->get();
        $data = [];
        $billings = config('billing');
        if ($list_colocations->count() > 0) {
            foreach ($list_colocations as $key => $colocation) {
                if ($colocation->status_colo != 'delete_colo') {
                   if ($colocation->status_colo != 'cancel') {
                       // Addon
                       if (!empty($colocation->amount)) {
                         $colocation->text_price = ' <span class="text-danger">(' . number_format($colocation->amount,0,",",".") . ' VNĐ / ' . $billings[$colocation->billing_cycle] . ')</span>';
                         $colocation->total_colo = $colocation->amount;
                       }
                       $data[] = $colocation;
                   }
                }
            }
        }
        return $data;
    }

    public function get_hosting_with_user_expire($user_id)
    {
      $list_hosting = $this->hosting->where('user_id', $user_id)->with('product')
                ->where(function($query)
                {
                    return $query
                      ->orwhere('status_hosting', 'on')
                      ->orwhere('status_hosting', 'expire')
                      ->orWhere('status_hosting', 'off');
                })
                ->orderBy('id', 'desc')->with('product')->get();
        $data = [];
        $billings = config('billing');
        if ($list_hosting->count() > 0) {
            foreach ($list_hosting as $key => $hosting) {
                if ($hosting->status_hosting != 'delete_hosting') {
                   if ($hosting->status_hosting != 'cancel') {
                       $product = $hosting->product;
                       if (!empty($hosting->price_override)) {
                         $hosting->text_price = ' <span class="text-danger">(' . number_format($hosting->price_override,0,",",".") . ' VNĐ / ' . $billings[$hosting->billing_cycle] . ')</span>';
                         $hosting->total_hosting = $hosting->price_override;
                       } else {
                         $sub_total = !empty( $product->pricing[$hosting->billing_cycle] ) ? $product->pricing[$hosting->billing_cycle] : 0;
                         $hosting->text_price = ' <span>(' . number_format($sub_total,0,",",".") . ' VNĐ / ' . $billings[$hosting->billing_cycle] . ')</span>';
                         $hosting->total_hosting = $sub_total;
                       }
                       $data[] = $hosting;
                   }
                }
            }
        }
        return $data;
    }

    public function get_email_hosting_with_user_expire($user_id)
    {
      $list_hosting = $this->email_hosting->where('user_id', $user_id)->with('product')
                ->where(function($query)
                {
                    return $query
                      ->orwhere('status_hosting', 'on')
                      ->orwhere('status_hosting', 'expire')
                      ->orWhere('status_hosting', 'off');
                })
                ->orderBy('id', 'desc')->with('product')->get();
        $data = [];
        $billings = config('billing');
        if ($list_hosting->count() > 0) {
            foreach ($list_hosting as $key => $hosting) {
                if ($hosting->status_hosting != 'delete_hosting') {
                   if ($hosting->status_hosting != 'cancel') {
                       $product = $hosting->product;
                       if (!empty($hosting->price_override)) {
                         $hosting->text_price = ' <span class="text-danger">(' . number_format($hosting->price_override,0,",",".") . ' VNĐ / ' . $billings[$hosting->billing_cycle] . ')</span>';
                         $hosting->total_hosting = $hosting->price_override;
                       } else {
                         $sub_total = !empty( $product->pricing[$hosting->billing_cycle] ) ? $product->pricing[$hosting->billing_cycle] : 0;
                         $hosting->text_price = ' <span>(' . number_format($sub_total,0,",",".") . ' VNĐ / ' . $billings[$hosting->billing_cycle] . ')</span>';
                         $hosting->total_hosting = $sub_total;
                       }
                       $data[] = $hosting;
                   }
                }
            }
        }
        return $data;
    }

    public function get_total_vps_expire($vps_id , $billing_cycle)
    {
        $vps = $this->vps->find($vps_id);
        $total = 0;
        if ( $vps->billing_cycle == $billing_cycle ) {
            if (!empty($vps->price_override)) {
                $total = $vps->price_override;
            } else {
                $product = $vps->product;
                $total = !empty( $product->pricing[$vps->billing_cycle] ) ? $product->pricing[$vps->billing_cycle] : 0;
                $pricing_addon = 0;
                if (!empty($vps->vps_config)) {
                    $addon_vps = $vps->vps_config;
                    $add_on_products = $this->get_addon_product_private($vps->user_id);
                    foreach ($add_on_products as $key => $add_on_product) {
                        if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                            }
                        }
                    }
                }
                $total += $pricing_addon;
            }
        }
        else {
            if (!empty($vps->price_override)) {
              $billing = config('billingDashBoard');
              $total = ($vps->price_override * $billing[$billing_cycle]) / $billing[$vps->billing_cycle];
              $total = (int) round( $total , -3);
            }
            else {
                $product = $vps->product;
                $total = !empty($product->pricing[$billing_cycle]) ? $product->pricing[$billing_cycle] : 0;
                $pricing_addon = 0;
                if (!empty($vps->vps_config)) {
                    $addon_vps = $vps->vps_config;
                    $add_on_products = $this->get_addon_product_private($vps->user_id);
                    foreach ($add_on_products as $key => $add_on_product) {
                        if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                            }
                        }
                    }
                }
                $total += $pricing_addon;
            }
        }
        return $total;
    }

    public function get_total_hosting_expire($hosting_id , $billing_cycle)
    {
        $hosting = $this->hosting->find($hosting_id);
        $total = 0;
        if ( $hosting->billing_cycle == $billing_cycle ) {
            if (!empty($hosting->price_override)) {
                $total = $hosting->price_override;
            } else {
                $product = $hosting->product;
                $total = !empty( $product->pricing[$hosting->billing_cycle] ) ? $product->pricing[$hosting->billing_cycle] : 0;
            }
        }
        else {
            $product = $hosting->product;
            $total = !empty($product->pricing[$billing_cycle]) ? $product->pricing[$billing_cycle] : 0;
        }
        return $total;
    }

    public function get_total_email_hosting_expire($hosting_id , $billing_cycle)
    {
        $hosting = $this->email_hosting->find($hosting_id);
        $total = 0;
        if ( $hosting->billing_cycle == $billing_cycle ) {
            if (!empty($hosting->price_override)) {
                $total = $hosting->price_override;
            } else {
                $product = $hosting->product;
                $total = !empty( $product->pricing[$hosting->billing_cycle] ) ? $product->pricing[$hosting->billing_cycle] : 0;
            }
        }
        else {
            $product = $hosting->product;
            $total = !empty($product->pricing[$billing_cycle]) ? $product->pricing[$billing_cycle] : 0;
        }
        return $total;
    }

    public function get_product_upgrade_hosting($user_id)
    {
        return $this->hosting->where('user_id', $user_id)->with('product')
            ->where(function($query)
            {
                return $query
                  ->orwhere('status_hosting', 'on')
                  ->orWhere('status_hosting', 'off');
            })
            ->orderBy('id', 'desc')->get();

    }


    public function choose_addon_vps($user_id, $vps_id)
    {
      $products = [];
      $user = $this->user->find($user_id);
      // dd($user);
      if ($user->group_user_id != 0) {
        $group_user = $user->group_user;
        $group_products = $group_user->group_products;
        foreach ($group_products as $key => $group_product) {
          if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
            $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
          }
        }
      } else {
        $group_products = $this->group_product->where('private', 0)->get();
        foreach ($group_products as $key => $group_product) {
          if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
            $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
          }
        }
      }
      $vps = $this->vps->find($vps_id);
      foreach ($products as $key => $product) {
        $now = Carbon::now();
        $next_due_date = new Carbon($vps->next_due_date);
        if ($next_due_date->diffInMonths($now) == 0) {
          $total = (int) round($next_due_date->diffInDays($now)  * $product->pricing[$vps->billing_cycle] / 30, -3);
        } elseif ($next_due_date->diffInMonths($now) > 0) {
          // dd( $now->addMonths($next_due_date->diffInMonths($now)) );
          $month = $next_due_date->diffInMonths($now);
          $now = $now->addMonths(1);
          $total =  $month  * $product->pricing[$vps->billing_cycle]  + (int) round($next_due_date->diffInDays($now) * $product->pricing[$vps->billing_cycle] / 30, -3);
        }
        $product->total = $total;
      }
      return $products;
    }


    public function create_order_addon_vps($data)
    {
        $vps_id = $data['vps_id'];
        $id_addon_cpu = $data['id-addon-cpu'];
        $id_addon_ram = $data['id-addon-ram'];
        $id_addon_disk = $data['id-addon-disk'];
        $qtt_addon_cpu = $data['addon_cpu'];
        $qtt_addon_ram = $data['addon_ram'];
        $qtt_addon_disk = $data['addon_disk'];
        $billing_cycle = 'monthly';
        $product_addon_cpu = $this->product->find($id_addon_cpu);
        $product_addon_ram = $this->product->find($id_addon_ram);
        $product_addon_disk = $this->product->find($id_addon_disk);
        $total = 0;
        $vps = $this->vps->find($vps_id);
        $now = Carbon::now();
        $next_due_date = new Carbon($vps->next_due_date);

        // dd($now , $next_due_date);
        if ($qtt_addon_cpu > 0) {
            if ($next_due_date->diffInMonths($now) == 0) {
              $total += (int) round($next_due_date->diffInDays($now) * $qtt_addon_cpu * $product_addon_cpu->pricing[$billing_cycle] / 30, -3);
            } elseif ($next_due_date->diffInMonths($now) > 0) {
              // dd( $now->addMonths($next_due_date->diffInMonths($now)) );
              $month = $next_due_date->diffInMonths($now);
              $now = $now->addMonths(1);
              $total +=  $month * $qtt_addon_cpu * $product_addon_cpu->pricing[$billing_cycle]  + (int) round($next_due_date->diffInDays($now) * $qtt_addon_cpu * $product_addon_cpu->pricing[$billing_cycle] / 30, -3);
            }
        }

        $now = Carbon::now();
        $next_due_date = new Carbon($vps->next_due_date);

        if ($qtt_addon_ram > 0) {

            if ($next_due_date->diffInMonths($now) == 0) {
              $total += (int) round($next_due_date->diffInDays($now) * $qtt_addon_ram * $product_addon_ram->pricing[$billing_cycle] / 30, -3);
            } elseif ($next_due_date->diffInMonths($now) > 0) {
              // dd( $now->addMonths($next_due_date->diffInMonths($now)) );
              $month = $next_due_date->diffInMonths($now);
              $now = $now->addMonths($next_due_date->diffInMonths($now));
              $total +=  $month * $qtt_addon_ram * $product_addon_ram->pricing[$billing_cycle]  + (int) round($next_due_date->diffInDays($now) * $qtt_addon_ram * $product_addon_ram->pricing[$billing_cycle] / 30, -3);
            }

        }

        $now = Carbon::now();
        $next_due_date = new Carbon($vps->next_due_date);

        if ($qtt_addon_disk > 0) {

            if ($next_due_date->diffInMonths($now) == 0) {
              $total += (int) round($next_due_date->diffInDays($now) * $qtt_addon_disk / 10 * $product_addon_disk->pricing[$billing_cycle] / 30, -3);
            } elseif ($next_due_date->diffInMonths($now) > 0) {
              // dd( $now->addMonths($next_due_date->diffInMonths($now)) );
              $month = $next_due_date->diffInMonths($now);
              $now = $now->addMonths($next_due_date->diffInMonths($now));
              $total +=  $month * $qtt_addon_disk * $product_addon_disk->pricing[$billing_cycle] / 10  + (int) round($next_due_date->diffInDays($now) * $qtt_addon_disk * $product_addon_disk->pricing[$billing_cycle] / 30, -3) / 10;
            }

        }

        $now = Carbon::now();
        $next_due_date = new Carbon($vps->next_due_date);

        if ($next_due_date->diffInMonths($now) == 0) {
          $day = $next_due_date->diffInDays($now);
          $month = 0;
        } else {
          $month = $next_due_date->diffInMonths($now);
          $now = $now->addMonths($next_due_date->diffInMonths($now));
          $day = $next_due_date->diffInDays($now);
        }
        if (!empty($data['total_price_override'])) {
            $total = $data['total_price_override'];
        }
        $status = $data['status'];
        $data_order = [
            'user_id' => $data['user_id'],
            'total' => $total,
            'status' => $status,
            'description' => 'addon vps',
            'makh' => '',
        ];
        $order = $this->order->create($data_order);
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tạo',
          'model' => 'Admin/Order',
          'description' => ' Order addon vps #' . $order->id,
        ];
        $this->log_activity->create($data_log);
        if ($status == 'Active' || $status == 'Finish') {
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $date = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date . $billing[$vps->billing_cycle] ));
            $data_order_detail = [
                'order_id' => $order->id,
                'type' => 'addon_vps',
                'due_date' => $date,
                'description' => 'addon vps',
                'status' => 'paid',
                'sub_total' => $total,
                'quantity' => 1,
                'user_id' => $data['user_id'],
                'paid_date' => date('Y-m-d'),
                'addon_id' => '0',
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            $user = $this->user->find($data['user_id']);
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDADV' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '4',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Active',
                'method_gd_invoice' => 'credit',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            // vetify order
            $data_order_addon_vps = [
              'detail_order_id'  => $detail_order->id,
              'vps_id' => $vps_id,
              'cpu' => !empty($qtt_addon_cpu) ? $qtt_addon_cpu : 0,
              'ram' => !empty($qtt_addon_ram) ? $qtt_addon_ram : 0,
              'disk' => !empty($qtt_addon_disk) ? $qtt_addon_disk : 0,
              'ip' => !empty($qtt_addon_ip) ? $qtt_addon_ip : 0,
              'month' => $month,
              'day' => $day,
            ];
            $order_addon_vps = $this->order_addon_vps->create($data_order_addon_vps);
            $data_addon = [
                'mob' => 'upgrade', // action
                'name' => $vps->vm_id,  // vm_id
                'extra_cpu' => !empty($order_addon_vps->cpu) ? $order_addon_vps->cpu : 0, // so CPU them
                'extra_ram' => !empty($order_addon_vps->ram) ? $order_addon_vps->ram : 0, // so RAM them
                'extra_disk' => !empty($order_addon_vps->disk) ? $order_addon_vps->disk : 0, // so DISK them
                'package' => $total, // tong tien
                'type' => $vps->type_vps == 'vps' ? 0 : 5,
            ];
            $reques_vcenter = $this->dashboard->upgrade_vps($data_addon, $vps->vm_id, $vps); //request đên Vcenter
            // $reques_vcenter= true;
            if ($reques_vcenter) {
              $order->status = 'Finish';
              $order->save();
              if ( !empty($vps->price_override) ) {
                  $pricing_addon = 0;
                  $add_on_products = $this->get_addon_product_private(Auth::user()->id);
                  foreach ($add_on_products as $key => $add_on_product) {
                      if (!empty($add_on_product->meta_product->type_addon)) {
                        if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                          $pricing_addon += $order_addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                          $pricing_addon += $order_addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                        }
                        if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                            $pricing_addon += $order_addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                        }
                      }
                  }
                  $vps->price_override += $pricing_addon;
                  $vps->save();
              }
              $vps_config = $vps->vps_config;
              if (!empty($vps_config)) {
                $vps_config->ram += $order_addon_vps->ram;
                $vps_config->cpu += $order_addon_vps->cpu;
                $vps_config->disk += $order_addon_vps->disk;
                $vps_config->save();
              } else {
                $data_config = [
                  'vps_id' => $vps->id,
                  'ram' => $order_addon_vps->ram,
                  'cpu' => $order_addon_vps->cpu,
                  'disk' => $order_addon_vps->disk,
                  'ip' => 0,
                ];
                $this->vps_config->create($data_config);
              }
            }
            // gữi mail

            if (!empty($month)) {
                $time = $month . ' Tháng ' . $day . ' Ngày';
            } else {
                $time = $day . ' Ngày';
            }
            $mail_system = $this->email->where('type', 3)->where('type_service', 'finish_payment')->first();
            if ( isset($mail_system) ) {
              $search = [ '{$user_id}', '{$user_name}',  '{$user_email}', '{$user_credit}', '{$url}', '{$token}', '{$ip}',
                  '{$services_username}', '{$services_password}', '{$total}', '{$addon_cpu}', '{$addon_ram}',
                  '{$addon_disk}', '{$addon_time}' ];
              $replace = [ $user->id, $user->name, $user->email, !empty($user->credit->value) ? $user->credit->value : 0,
                  url(''), '', $vps->ip, $vps->user, $vps->password, number_format($total,0,",","."),
                  !empty($order_addon_vps->cpu) ? $order_addon_vps->cpu : 0, !empty($order_addon_vps->ram) ? $order_addon_vps->ram : 0,
                  !empty($order_addon_vps->disk) ? $order_addon_vps->disk : 0, $time];
              $content = str_replace($search, $replace, !empty($mail_system->meta_email->content)? $mail_system->meta_email->content : '');
              $data = [
                  'content' => $content,
                  'subject' => !empty($mail_system->meta_email->subject)? $mail_system->meta_email->subject : 'Hoàn thành đặt hàng nâng cấp cấu hình VPS',
              ];
            }
            else {
              $data = [
                'name' => $user->name,
                'email' => $user->email,
                'amount' => $total,
                'ip' => $vps->ip,
                'cpu' => !empty($qtt_addon_cpu) ? $qtt_addon_cpu : 0,
                'ram' => !empty($qtt_addon_ram) ? $qtt_addon_ram : 0,
                'disk' => !empty($qtt_addon_disk) ? $qtt_addon_disk : 0,
                'product_cpu' => !empty($product_addon_cpu) ? $product_addon_cpu : 0,
                'product_ram' => !empty($product_addon_ram) ? $product_addon_ram : 0,
                'product_disk' => !empty($product_addon_disk) ? $product_addon_disk : 0,
                'billing_cycle' => $billing_cycle,
                'time' =>  $time,
                'subject' => 'Hoàn thành đặt hàng nâng cấp cấu hình VPS'
              ];
            }
            try {
                $data_tb_send_mail = [
                  'type' => 'finish_order_addon_vps',
                  'type_service' => 'vps',
                  'user_id' => $user->id,
                  'status' => false,
                  'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);
            } catch (\Exception $e) {
                report($e);
                return true;
            }
        }
        else {
            //create order detail
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $order->id,
                'type' => 'addon_vps',
                'due_date' => $date,
                'description' => 'addon vps',
                'status' => 'unpaid',
                'sub_total' => $total,
                'quantity' => 1,
                'user_id' => $data['user_id'],
                'addon_id' => '0',
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            $user = $this->user->find($data['user_id']);
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDADV' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '4',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            // vetify order
            $data_order_addon_vps = [
              'detail_order_id'  => $detail_order->id,
              'vps_id' => $vps_id,
              'cpu' => !empty($qtt_addon_cpu) ? $qtt_addon_cpu : 0,
              'ram' => !empty($qtt_addon_ram) ? $qtt_addon_ram : 0,
              'disk' => !empty($qtt_addon_disk) ? $qtt_addon_disk : 0,
              'ip' => !empty($qtt_addon_ip) ? $qtt_addon_ip : 0,
              'month' => $month,
              'day' => $day,
            ];
            $this->order_addon_vps->create($data_order_addon_vps);
            // gữi mail

            if (!empty($month)) {
                $time = $month . ' Tháng ' . $day . ' Ngày';
            } else {
                $time = $day . ' Ngày';
            }
            $data = [
                'name' => $user->name,
                'amount' => $total,
                'ip' => $vps->ip,
                'cpu' => !empty($qtt_addon_cpu) ? $qtt_addon_cpu : 0,
                'ram' => !empty($qtt_addon_ram) ? $qtt_addon_ram : 0,
                'disk' => !empty($qtt_addon_disk) ? $qtt_addon_disk : 0,
                'product_cpu' => !empty($product_addon_cpu) ? $product_addon_cpu : 0,
                'product_ram' => !empty($product_addon_ram) ? $product_addon_ram : 0,
                'product_disk' => !empty($product_addon_disk) ? $product_addon_disk : 0,
                'billing_cycle' => $billing_cycle,
                'time' =>  $time,
                'subject' => 'Xác nhận đặt hàng nâng cấp cấu hình VPS'
            ];
            try {
                $data_tb_send_mail = [
                  'type' => 'order_addon_vps',
                  'type_service' => 'vps',
                  'user_id' => $user->id,
                  'status' => false,
                  'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);
            } catch (\Exception $e) {
                report($e);
                return true;
            }
        }
        return true;
    }

    public function create_order_addon_server($data)
    {
        $server = $this->server->find($data['server_id']);
        $total = $data['total_addon_server'];

        $status = $data['status'];
        $data_order = [
            'user_id' => $data['user_id'],
            'total' => $total,
            'status' => $status,
            'description' => 'addon server',
            'makh' => '',
        ];
        $order = $this->order->create($data_order);
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tạo',
          'model' => 'Admin/Order',
          'description' => ' Order addon server #' . $order->id,
        ];
        $this->log_activity->create($data_log);
        if ($status == 'Active' || $status == 'Finish') {
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $date = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date . $billing[$server->billing_cycle] ));
            $data_order_detail = [
                'order_id' => $order->id,
                'type' => 'addon_server',
                'due_date' => $date,
                'description' => 'addon server',
                'status' => 'paid',
                'sub_total' => $total,
                'quantity' => 1,
                'user_id' => $data['user_id'],
                'paid_date' => date('Y-m-d'),
                'addon_id' => '0',
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            $user = $this->user->find($data['user_id']);
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDADS' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '9',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Active',
                'method_gd_invoice' => 'credit',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            // vetify order
            $data_order_addon_server = [
              'detail_order_id'  => $detail_order->id,
              'server_id' => $server->id,
              'ip' => !empty($data['addon_ip_server']) ? $data['addon_ip_server'] : 0,
              'ram' => !empty($data['addon_ram_server']) ? $data['addon_ram_server'] : 0,
              'disk' => !empty($data['addon_disk_server']) ? $data['addon_disk_server'] : 0,
            ];
            $this->order_addon_server->create($data_order_addon_server);
            $server->amount += $total;
            $server->save();
            $server_config = $server->server_config;
              if (!empty($server_config)) {
                $server_config->ram += !empty($data['addon_ram_server']) ? $data['addon_ram_server'] : 0;
                $server_config->ip += !empty($data['addon_ip_server']) ? $data['addon_ip_server'] : 0;
                $server_config->disk += !empty($data['addon_disk_server']) ? $data['addon_disk_server'] : 0;
                $vps_config->save();
              } else {
                $data_config = [
                  'server_id' => $server->id,
                  'ram' => !empty($data['addon_ram_server']) ? $data['addon_ram_server'] : 0,
                  'disk' => !empty($data['addon_disk_server']) ? $data['addon_disk_server'] : 0,
                  'ip' => !empty($data['addon_ip_server']) ? $data['addon_ip_server'] : 0,
                ];
                $this->server_config->create($data_config);
              }
            // gữi mail
        }
        else {
            //create order detail
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $order->id,
                'type' => 'addon_server',
                'due_date' => $date,
                'description' => 'addon server',
                'status' => 'unpaid',
                'sub_total' => $total,
                'quantity' => 1,
                'user_id' => $data['user_id'],
                'addon_id' => '0',
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            $user = $this->user->find($data['user_id']);
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDADS' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '9',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            // vetify order
            $data_order_addon_server = [
              'detail_order_id'  => $detail_order->id,
              'server_id' => $server->id,
              'ip' => !empty($data['addon_ip_server']) ? $data['addon_ip_server'] : 0,
              'ram' => !empty($data['addon_ram_server']) ? $data['addon_ram_server'] : 0,
              'disk' => !empty($data['addon_disk_server']) ? $data['addon_disk_server'] : 0,
            ];
            $this->order_addon_server->create($data_order_addon_server);
            // gữi mail
        }
        return true;
    }

    public function choose_upgrade_hosting($user_id, $hosting_id)
    {
      $products = [];
      $user = $this->user->find($user_id);
      $hosting = $this->hosting->find($hosting_id);
      $product_upgrades = $this->product_upgrate->where("product_default_id", $hosting->product_id)->orderBy("product_upgrate_id", 'asc')->get();
      // dd($products);
      foreach ($product_upgrades as $key => $product_upgrade) {
        $sub_total_hosting = $hosting->detail_order->sub_total;
        $sub_total_hosting_upgrade = $product_upgrade->product_upgrate->pricing[$hosting->billing_cycle];
        $date_create_hosting = new Carbon($hosting->date_create);
        $next_due_date = new Carbon($hosting->next_due_date);
        $total = 0;
        if ($next_due_date->isPast() && $next_due_date->diffInDays($date_create_hosting) == 0) {
           $total = 0;
        } else {
          $date_create_hosting = new Carbon($hosting->date_create);
          $next_due_date = new Carbon($hosting->next_due_date);
           $total_day = $next_due_date->diffInDays($date_create_hosting);
           $now = new Carbon;
           $next_due_date = new Carbon($hosting->next_due_date);
           $total = ($sub_total_hosting_upgrade - $sub_total_hosting) / ($total_day / $next_due_date->diffInDays($now));
           $total = (int) round($total, -3);
        }
        $product_upgrade->product_upgrate->total = $total;
        $products[] = $product_upgrade->product_upgrate;
      }
      return $products;
    }

    public function create_order_change_ip($data)
    {
        $user = $this->user->find($data['user_id']);
        $total = 0;
        $product_change = [];
        if ($user->group_user_id == 0) {
            $group_products = $this->group_product->where('private', false)->get();
            foreach ($group_products as $key => $group_product) {
               foreach ($group_product->products as $key => $product) {
                   if ($product->type_product == 'Change-IP') {
                      $product_change = $product;
                      $total = $product->pricing->one_time_pay;
                   }
               }
            }
        } else {
            $group_user = $user->group_user();
            $group_products = $group_user->group_products;
            foreach ($group_products as $key => $group_product) {
                foreach ($group_product->products as $key => $product) {
                    if ($product->type_product == 'Change-IP') {
                       $product_change = $product;
                       $total = $product->pricing->one_time_pay;
                    }
                }
            }
        }
        if (!empty($data['total_price_override'])) {
            $total = $data['total_price_override'];
        }
        $data_order = [
            'user_id' => $user->id,
            'total' => $total,
            'status' => $data['status'],
            'description' => 'change vps',
            'makh' => '',
        ];
        $order = $this->order->create($data_order);
        // ghi log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'tạo',
          'model' => 'Admin/Order',
          'description' => ' Order đổi IP #' . $order->id,
        ];
        $this->log_activity->create($data_log);

        $status = $data['status'];
        if ($status == 'Active' || $status == 'Finish') {
            //create order detail
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $order->id,
                'type' => 'change_ip',
                'due_date' => $date,
                'description' => 'change vps',
                'status' => 'paid',
                'sub_total' => $total,
                'quantity' => 1,
                'user_id' => $user->id,
                'paid_date' => date('Y-m-d'),
                'addon_id' => '0',
                'payment_method' => 1,
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDCI' . strtoupper(substr(sha1(time()), 33, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '5',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'method_gd_invoice' => 'credit',
                'money'=> $total,
                'status' => 'Active',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            $vps = $this->vps->find($data['vps_id']);
            $data_change_ip = [
                'detail_order_id' => $detail_order->id,
                'vps_id' => $vps->id,
                'ip' => $vps->ip,
            ];
            $order_change_vps = $this->order_change_ip->create($data_change_ip);
            $reques_vcenter = $this->dashboard->change_ip($vps, $total, $order_change_vps);
            // $reques_vcenter = true;
            $ip = $vps->ip;
            if ($reques_vcenter) {
                $order->status = 'Finish';
                $order->save();
                // Gữi email khi hoàn thành order change IP
                $this->user_email = $user->email;
                $this->subject = 'Hoàn thành thanh toán thay đổi IP cho VPS';

                $data = [
                    'name' => $user->name,
                    'email' => $user->email,
                    'amount' => $total,
                    'ip' => $ip,
                    'ip_change' => $vps->ip,
                    'amount' => $total,
                ];

                try {
                    $mail = Mail::send('users.mails.finish_order_change_ip_vps', $data, function($message){
                        $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                        $message->to($this->user_email)->subject($this->subject);
                    });
                } catch (\Exception $e) {
                    report($e);
                    return true;
                }
            }
        }
        else {
            //create order detail
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $order->id,
                'type' => 'change_ip',
                'due_date' => $date,
                'description' => 'change vps',
                'status' => 'unpaid',
                'sub_total' => $total,
                'quantity' => 1,
                'user_id' => $user->id,
                'addon_id' => '0',
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDCI' . strtoupper(substr(sha1(time()), 33, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '5',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            $vps = $this->vps->find($data['vps_id']);
            $data_change_ip = [
                'detail_order_id' => $detail_order->id,
                'vps_id' => $vps->id,
                'ip' => $vps->ip,
            ];
            $this->order_change_ip->create($data_change_ip);
            // gữi mail
            $string_list_ip_vps = $vps->ip . '<br>';
            $email = $this->email->find($product_change->meta_product->email_create);
            $this->user_email = $user->email;
            $this->subject = $email->meta_email->subject;
            // Thông tin gữi mail
            $url = url('');
            $url = str_replace(['http://','https://'], ['', ''], $url);
            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}', '{$list_ip_vps}', '{$list_ip_changed_vps}'];
            $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product_change->name , '', '' , '', '', $order->created_at, '', '',number_format($product_change->pricing->one_time_pay,0,",",".") . ' VNĐ' ,number_format($total,0,",",".") . ' VNĐ' , 1 , '' , '', $url , $string_list_ip_vps , ''];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
            $data = [
                'content' => $content,
                'user_name' => $user->name,
                'user_id' => $user->id,
                'user_email' => $user->email,
                'user_addpress' => $user->user_meta->address,
                'user_phone' => $user->user_meta->user_phone,
                'order_id' => $order->id,
                'product_name' => $product_change->name,
                'domain' => '',
                'ip' => '',
                'billing_cycle' => '',
                'next_due_date' => '',
                'order_created_at' => $order->created_at,
                'services_username' => '',
                'services_password' => '',
                'sub_total' => $product_change->pricing->one_time_pay,
                'total' => $total,
                'qtt' => 1,
                'os' => '',
                'token' => '',
                'security' => '',
                'list_ip_vps' => $string_list_ip_vps,
            ];

            try {
              $mail = Mail::send('users.mails.orders', $data, function($message){
                  $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                  $message->to($this->user_email)->subject($this->subject);
              });
            } catch (\Exception $e) {
                report($e);
                return true;
            }
        }
        return true;
    }

    public function edit_total_order($data)
    {
         $order = $this->order->find($data['order_id']);
         // ghi log
         $data_log = [
           'user_id' => Auth::user()->id,
           'action' => 'chỉnh sửa',
           'model' => 'Admin/Order',
           'description' => ' tổng tiền Order #' . $order->id,
         ];
         $this->log_activity->create($data_log);
         $total = $data["total"];
         $order->total = $total;
         $sub_total = 0;
         $quantity = 0;
         foreach ($order->detail_orders as $key => $detail_order) {
           $quantity += $detail_order->quantity;
         }
         if ($quantity == 0) {
            return false;
         }
         else {

             $sub_total = $total / $quantity;
             foreach ($order->detail_orders as $key => $detail_order) {
                $detail_order->sub_total = $sub_total;
                foreach ( $detail_order->history_pay as $history_pay) {
                  if (!empty($history_pay->money)) {
                    $history_pay->money = $total;
                    $history_pay->save();
                  }
                }
                $detail_order->save();
                if ($detail_order->type == 'VPS' && $order->type == 'create') {
                   foreach ($detail_order->vps as $key => $vps) {
                      $vps->price_override = $sub_total;
                      $vps->save();
                   }
                }
                elseif ($detail_order->type == 'NAT-VPS' && $order->type == 'create') {
                    foreach ($detail_order->vps as $key => $vps) {
                       $vps->price_override = $sub_total;
                       $vps->save();
                    }
                }
                elseif ($detail_order->type == 'Hosting' && $order->type == 'create') {
                    foreach ($detail_order->hostings as $key => $hosting) {
                       $hosting->price_override = $sub_total;
                       $hosting->save();
                    }
                }
             }
             $update = $order->save();
             return $update;
         }

    }

    public function create_order_expire_vps($data)
    {
        $billings = config('billing');
        $billing_price = config('billingDashBoard');
        $user = $this->user->find($data['user_id']);
        $billing_cycle = $data['billing_cycle'];
        $total = 0;
        $list_vps_id = $data['vps_id'];
        foreach ($list_vps_id as $key => $vps_id) {
            $vps = $this->vps->find($vps_id);
            $product = $this->product->find($vps->product_id);
            $invoiced = $vps->detail_order;
            if (!empty($vps)) {
              if (!empty($data['total_price_override'])) {
                  $total = $data['total_price_override'];
              }
              else {
                  if ( !empty($vps->price_override) ) {
                      if ($vps->billing_cycle == $billing_cycle) {
                          $total += $vps->price_override;
                      } else {
                        $total_price_1 = ($vps->price_override * $billing_price[$billing_cycle]) / $billing_price[$vps->billing_cycle];
                        $total += (int) round( $total_price_1 , -3);
                      }
                  } else {
                      $total += $product->pricing[$billing_cycle];
                      if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($user->id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                          if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                            }
                          }
                        }
                        $total += $pricing_addon;
                      }
                  }
              }
            }
            else {
                return false;
            }
        }
        $status = $data['status'];
        if ($status == 'Active' || $status == 'Finish') {
            $config_billing_DashBoard = config('billingDashBoard');
            $data_expired = [];
            foreach ($list_vps_id as $key => $vps_id) {
                $vps = $this->vps->find($vps_id);
                $date_star = $vps->next_due_date;
                $due_date = date('Y-m-d', strtotime( $date_star . $billings[$billing_cycle] ));
                $data_expired[] = [
                    'package' => (int) !empty($vps->price_override) ? $vps->price_override : $vps->detail_order->sub_total,
                    'leased_time' => $config_billing_DashBoard[$billing_cycle],
                    'type' => ($vps->type_vps == 'nat_vps') ? 5 : 0,
                    'vm_id' => $vps->vm_id,
                    'end_date' => $due_date
                ];
            }
            $reques_vcenter = $this->dashboard->expired_vps($data_expired, $vps->vm_id); //request đên Vcenter
            // $reques_vcenter = true;
            if ($reques_vcenter) {
              // $reques_vcenter->error == 0
              if ($reques_vcenter->error == 0) {
                // create order
                $data_order = [
                    'user_id' => $user->id,
                    'total' => $total,
                    'status' => $data['status'],
                    'description' => 'expired vps',
                    'type' => 'expired',
                    'makh' => '',
                ];
                $order = $this->order->create($data_order);
                if (!empty($data['check_remove_credit'])) {
                    $credit = $user->credit;
                    if (!empty($credit)) {
                        $credit->value = $credit->value - $total;
                        $credit->save();
                    }
                }
                //create order detail
                $date = date('Y-m-d');
                $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
                $data_order_detail = [
                    'order_id' => $order->id,
                    'type' => 'VPS',
                    'due_date' => $date,
                    'paid_date' => date('Y-m-d'),
                    'description' => 'Gia hạn VPS',
                    'status' => 'paid',
                    'sub_total' => $total,
                    'quantity' => count($list_vps_id),
                    'user_id' => $user->id,
                    'paid_date' => date('Y-m-d'),
                    'addon_id' => '0',
                    'payment_method' => 1,
                ];
                $detail_order = $this->detail_order->create($data_order_detail);
                $data_history = [
                    'user_id' => $user->id,
                    'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
                    'discription' => 'Hóa đơn số ' . $detail_order->id,
                    'type_gd' => '3',
                    'method_gd' => 'invoice',
                    'date_gd' => date('Y-m-d'),
                    'money'=> $total,
                    'status' => 'Active',
                    'method_gd_invoice' => 'credit',
                    'detail_order_id' => $detail_order->id,
                ];
                $this->history_pay->create($data_history);
                // Tạo lưu trữ khi gia hạn
                foreach ($list_vps_id as $key => $vps_id) {
                  $vps = $this->vps->find($vps_id);
                  $data_order_expired = [
                      'detail_order_id' => $detail_order->id,
                      'expired_id' => $vps->id,
                      'billing_cycle' => $billing_cycle,
                      'type' => 'vps',
                  ];
                  $order_expired_vps = $this->order_expired->create($data_order_expired);
                  // invoice
                  $billing = [
                      'monthly' => '1 Month',
                      'twomonthly' => '2 Month',
                      'quarterly' => '3 Month',
                      'semi_annually' => '6 Month',
                      'annually' => '1 Year',
                      'biennially' => '2 Year',
                      'triennially' => '3 Year'
                  ];
                  $date_star = $vps->next_due_date;
                  $due_date = date('Y-m-d', strtotime( $date_star . $billing[$billing_cycle] ));
                  if ( !empty($data['total_price_override']) ) {
                      $vps->price_override = $data['total_price_override'];
                      $vps->next_due_date = $due_date;
                      $vps->billing_cycle = $billing_cycle;
                      $vps->save();
                  } else {
                      if ( !empty($vps->price_override) ) {
                          if ( $vps->billing_cycle ==  $billing_cycle ) {
                              $vps->next_due_date = $due_date;
                              $vps->save();
                          }
                      } else {
                          $vps->price_override = 0;
                          $vps->next_due_date = $due_date;
                          $vps->billing_cycle = $billing_cycle;
                          $vps->save();
                      }
                  }
                }
                $order->status = 'Finish';
                $order->save();
                try {
                    // ghi log
                    $data_log = [
                      'user_id' => $user->id,
                      'action' => 'tạo',
                      'model' => 'Admin/Order',
                      'description' => '  hoàn thành gia hạn VPS <a href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                    // gui mail
                    $this->send_mail_finish_expired('vps',$order, $detail_order ,$order_expired_vps);
                } catch (\Exception $e) {
                    report($e);
                    return 1;
                }
                return 1;
              }
              else {
                  $list_success = $reques_vcenter->list_success;
                  $list_error = $reques_vcenter->list_error;
                  $list_vps_success = [];
                  $list_vps_error = [];
                  foreach ($list_success as $key => $vm_id) {
                      $vps = $this->vps->where('vm_id', $vm_id)->first();
                      if (!empty($vps)) {
                          $list_vps_success[] = $vps->ip;
                      }
                  }
                  foreach ($list_error as $key => $vm_id) {
                      $vps = $this->vps->where('vm_id', $vm_id)->first();
                      if (!empty($vps)) {
                          $list_vps_error[] = $vps->ip;
                      }
                  }
                  $this->expired_success($list_success, $billing_cycle);
                  $data = [
                    "error" => 2,
                    "list_vps_success" => $list_vps_success,
                    "list_vps_error" => $list_vps_error,
                  ];
                  return $data;
              }
            }
            else {
              return 3;
            }
        }
        else {
            // create order
            $data_order = [
                'user_id' => $user->id,
                'total' => $total,
                'status' => $data['status'],
                'description' => 'expired vps',
                'type' => 'expired',
                'makh' => '',
            ];
            $order = $this->order->create($data_order);
            // ghi log
            $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'tạo',
                'model' => 'Admin/Order',
                'description' => ' Order gia hạn VPS #' . $order->id,
            ];
            //create order detail
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $order->id,
                'type' => 'VPS',
                'due_date' => $date,
                'description' => 'Gia hạn VPS',
                'status' => 'unpaid',
                'sub_total' => $total,
                'quantity' => count($list_vps_id),
                'user_id' => $user->id,
                'addon_id' => !empty($invoiced->addon_id) ? 1 : 0,
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // Tạo lưu trữ khi gia hạn
            foreach ($list_vps_id as $key => $vps_id) {
                $vps = $this->vps->find($vps_id);
                $data_order_expired = [
                    'detail_order_id' => $detail_order->id,
                    'expired_id' => $vps->id,
                    'billing_cycle' => $billing_cycle,
                    'type' => 'vps',
                ];
                $this->order_expired->create($data_order_expired);
            }
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
                'type_gd' => '3',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);

            $billings = config('billing');
            // gui mail
            $email = $this->email->find($product->meta_product->email_expired);
            $url = url('');
            $url = str_replace(['http://','https://'], ['', ''], $url);
            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $vps->ip ,$billings[$vps->billing_cycle], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , 1, '', 'token', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
            $data = [
                'content' => $content,
                'user_name' => $user->name,
                'subject' => (!empty($email->meta_email->subject)) ? $email->meta_email->subject : 'Thư xác nhận gia hạn vps',
            ];
            try {
              // ghi log
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'tạo',
                'model' => 'Admin/Order',
                'description' => '  hoàn thành gia hạn VPS <a href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                'service' =>  $vps->ip,
              ];
              $this->log_activity->create($data_log);
              $data_tb_send_mail = [
                'type' => 'mail_order_expire_vps',
                'type_service' => 'vps',
                'user_id' => $user->id,
                'status' => false,
                'content' => serialize($data),
              ];
              $this->send_mail->create($data_tb_send_mail);
            } catch (\Exception $e) {
              return $detail_order->id;
            }
        }
        return true;
    }
    public function expired_success($list_vm_id, $billing_cycle)
    {
        $total = 0;
        $quantity = 0;
        $billing_price = config('billingDashBoard');
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        foreach ($list_vm_id as $key => $vm_id) {
            $vps = $this->vps->where('vm_id', $vm_id)->first();
            if (!empty($vps)) {
                $product = $this->product->find($vps->product_id);
                if (!empty($vps->price_override)) {
                    if ($vps->billing_cycle == $billing_cycle) {
                        $total += $vps->price_override;
                    } else {
                      $total_price_1 = ($vps->price_override * $billing_price[$billing_cycle]) / $billing_price[$vps->billing_cycle];
                      $total += (int) round( $total_price_1 , -3);
                    }
                } else {
                    $total += $product->pricing[$billing_cycle];
                    if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($user->id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                            if (!empty($add_on_product->meta_product->type_addon)) {
                                if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                  $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                  $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                  $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                                }
                            }
                        }
                        $total += $pricing_addon;
                    }
                }
            }
            $quantity++;
        }
        $user = $this->user->find($vps->user_id);
        $data_order = [
            'user_id' => $user->id,
            'total' => $total,
            'status' => 'Finish',
            'description' => 'expired vps',
            'type' => 'expired',
            'makh' => '',
        ];
        $order = $this->order->create($data_order);
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $type_detail_order = 'VPS';
        $type_detail_order .= ($vps->location == 'us') ? " US" : "";
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => $type_detail_order,
            'due_date' => $date,
            'paid_date' => date('Y-m-d'),
            'description' => 'Gia hạn VPS' . ($vps->location == 'us') ? " US" : "",
            'status' => 'paid',
            'sub_total' => $total / $quantity,
            'quantity' => $quantity,
            'user_id' => $user->id,
            'paid_date' => date('Y-m-d'),
            'addon_id' => '0',
            'payment_method' => 1,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '3',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Active',
            'method_gd_invoice' => 'credit',
            'detail_order_id' => $detail_order->id,
        ];
        $history_pay = $this->history_pay->create($data_history);
        // ghi log giao dịch
        $credit = $this->credit->where('user_id', $user->id)->first();
        if (!isset($credit)) {
            return false;
        }
        $data_log_payment = [
          'history_pay_id' => $history_pay->id,
          'before' => $credit->value,
          'after' => $credit->value - $total,
        ];
        $this->log_payment->create($data_log_payment);
        // credit
        $credit->value = $credit->value - $total;
        $credit->save();
        $credit = $this->credit->where('user_id', $user->id)->first();
        foreach ($list_vm_id as $key => $vm_id) {
            $vps = $this->vps->where('vm_id', $vm_id)->first();
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $vps->id,
                'billing_cycle' => $billing_cycle,
                'type' => 'vps',
            ];
            $this->order_expired->create($data_order_expired);

            $date_star = $vps->next_due_date;
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$billing_cycle] ));

            $vps->next_due_date = $due_date;
            $vps->billing_cycle = $billing_cycle;
            $vps->save();
            // ghi log
            $billings = config('billing');
            $data_log = [
              'user_id' => $user->id,
              'action' => 'thanh toán',
              'model' => 'User/HistoryPay',
              'description' => ' gia hạn VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$billing_cycle],
              'description_user' => ' gia hạn VPS <a target="_blank" href="/order/check-invoices/' . $detail_order->id . '">' . $detail_order->id . '</a> '  . $billings[$billing_cycle] . ' thành công',
              'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log);
            // ghi log user
            $data_log_user = [
              'user_id' => $vps->user_id,
              'action' => 'gia hạn',
              'model' => 'User/Services',
              'description_user' => ' VPS <a target="_blank" href="/service/detail/' . $vps->id  . '?type=vps">' . $vps->ip . '</a>',
              'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log_user);
        }
        try {
          $this->send_mail_finish_expired('vps',$order, $detail_order , $detail_order->order_expireds);
        } catch (\Exception $e) {
          return $detail_order->id;
        }
        return $detail_order->id;
    }
    public function create_order_expire_vps_us($data)
    {
        $billings = config('billing');
        $user = $this->user->find($data['user_id']);
        $billing_cycle = $data['billing_cycle'];
        $list_vps_id = $data['vps_id'];
        $total = 0;
        foreach ($list_vps_id as $key => $vps_id) {
            $vps = $this->vps->find($vps_id);
            $product = $this->product->find($vps->product_id);
            $invoiced = $vps->detail_order;
            if (!empty($vps)) {
              if (!empty($data['total_price_override'])) {
                  $total = $data['total_price_override'];
              }
              else {
                  if ( !empty($vps->price_override) ) {
                      if ($vps->billing_cycle == $billing_cycle) {
                          $total += $vps->price_override;
                      } else {
                        $total_price_1 = ($vps->price_override * $billing_price[$billing_cycle]) / $billing_price[$vps->billing_cycle];
                        $total += (int) round( $total_price_1 , -3);
                      }
                  } else {
                      $total += $product->pricing[$billing_cycle];
                      if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private($user->id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                          if (!empty($add_on_product->meta_product->type_addon)) {
                            if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                              $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                              $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                            }
                            if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                              $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                            }
                          }
                        }
                        $total += $pricing_addon;
                      }
                  }
              }
            }
            else {
                return false;
            }
        }
        // create order
        $status = $data['status'];
        if ($status == 'Active' || $status == 'Finish') {
            $config_billing_DashBoard = config('billingDashBoard');
            $data_expired = [];
            foreach ($list_vps_id as $key => $vps_id) {
                $vps = $this->vps->find($vps_id);
                $date_star = $vps->next_due_date;
                $due_date = date('Y-m-d', strtotime( $date_star . $billings[$billing_cycle] ));
                $data_expired[] = [
                    'package' => (int) !empty($vps->price_override) ? $vps->price_override : $vps->detail_order->sub_total,
                    'leased_time' => $config_billing_DashBoard[$billing_cycle],
                    'type' => ($vps->type_vps == 'nat_vps') ? 5 : 0,
                    'vm_id' => $vps->vm_id,
                    'end_date' => $due_date
                ];
            }
            $reques_vcenter = $this->dashboard->expired_vps_us($data_expired, $vps->vm_id); //request đên Vcenter
            // $reques_vcenter = true;
            if ($reques_vcenter) {
              // $reques_vcenter->error == 0
              if ($reques_vcenter->error == 0) {
                // create order
                $data_order = [
                    'user_id' => $user->id,
                    'total' => $total,
                    'status' => $data['status'],
                    'description' => 'expired vps us',
                    'type' => 'expired',
                    'makh' => '',
                ];
                $order = $this->order->create($data_order);
                if (!empty($data['check_remove_credit'])) {
                    $credit = $user->credit;
                    if (!empty($credit)) {
                        $credit->value = $credit->value - $total;
                        $credit->save();
                    }
                }
                //create order detail
                $date = date('Y-m-d');
                $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
                $data_order_detail = [
                    'order_id' => $order->id,
                    'paid_date' => date('Y-m-d'),
                    'type' => 'VPS US',
                    'due_date' => $date,
                    'description' => 'Gia hạn VPS US',
                    'status' => 'paid',
                    'sub_total' => $total,
                    'quantity' => count($list_vps_id),
                    'user_id' => $user->id,
                    'paid_date' => date('Y-m-d'),
                    'addon_id' => '0',
                    'payment_method' => 1,
                ];
                $detail_order = $this->detail_order->create($data_order_detail);
                $data_history = [
                    'user_id' => $user->id,
                    'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
                    'discription' => 'Hóa đơn số ' . $detail_order->id,
                    'type_gd' => '3',
                    'method_gd' => 'invoice',
                    'date_gd' => date('Y-m-d'),
                    'money'=> $total,
                    'status' => 'Active',
                    'method_gd_invoice' => 'credit',
                    'detail_order_id' => $detail_order->id,
                ];
                $this->history_pay->create($data_history);
                // Tạo lưu trữ khi gia hạn
                foreach ($list_vps_id as $key => $vps_id) {
                  $vps = $this->vps->find($vps_id);
                  $data_order_expired = [
                      'detail_order_id' => $detail_order->id,
                      'expired_id' => $vps->id,
                      'billing_cycle' => $billing_cycle,
                      'type' => 'vps',
                  ];
                  $order_expired_vps = $this->order_expired->create($data_order_expired);
                  // invoice
                  $billing = [
                      'monthly' => '1 Month',
                      'twomonthly' => '2 Month',
                      'quarterly' => '3 Month',
                      'semi_annually' => '6 Month',
                      'annually' => '1 Year',
                      'biennially' => '2 Year',
                      'triennially' => '3 Year'
                  ];
                  $date_star = $vps->next_due_date;
                  $due_date = date('Y-m-d', strtotime( $date_star . $billing[$billing_cycle] ));
                  if ( !empty($data['total_price_override']) ) {
                      $vps->price_override = $data['total_price_override'];
                      $vps->next_due_date = $due_date;
                      $vps->billing_cycle = $billing_cycle;
                      $vps->save();
                  } else {
                      if ( !empty($vps->price_override) ) {
                          if ( $vps->billing_cycle ==  $billing_cycle ) {
                              $vps->next_due_date = $due_date;
                              $vps->save();
                          }
                      } else {
                          $vps->price_override = 0;
                          $vps->next_due_date = $due_date;
                          $vps->billing_cycle = $billing_cycle;
                          $vps->save();
                      }
                  }
                }
                $order->status = 'Finish';
                $order->save();
                try {
                    // ghi log
                    $data_log = [
                      'user_id' => $user->id,
                      'action' => 'tạo',
                      'model' => 'Admin/Order',
                      'description' => '  hoàn thành gia hạn VPS <a href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                    // gui mail
                    $this->send_mail_finish_expired('vps',$order, $detail_order ,$order_expired_vps);
                } catch (\Exception $e) {
                    report($e);
                    return 1;
                }
                return 1;
              }
              else {
                  $list_success = $reques_vcenter->list_success;
                  $list_error = $reques_vcenter->list_error;
                  $list_vps_success = [];
                  $list_vps_error = [];
                  foreach ($list_success as $key => $vm_id) {
                      $vps = $this->vps->where('vm_id', $vm_id)->first();
                      if (!empty($vps)) {
                          $list_vps_success[] = $vps->ip;
                      }
                  }
                  foreach ($list_error as $key => $vm_id) {
                      $vps = $this->vps->where('vm_id', $vm_id)->first();
                      if (!empty($vps)) {
                          $list_vps_error[] = $vps->ip;
                      }
                  }
                  $this->expired_success($list_success, $billing_cycle);
                  $data = [
                    "error" => 2,
                    "list_vps_success" => $list_vps_success,
                    "list_vps_error" => $list_vps_error,
                  ];
                  return $data;
              }
            }
            else {
              return 3;
            }
        }
        else {
            // create order
            $data_order = [
                'user_id' => $user->id,
                'total' => $total,
                'status' => $data['status'],
                'description' => 'expired vps us',
                'type' => 'expired',
                'makh' => '',
            ];
            $order = $this->order->create($data_order);
            //create order detail
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $order->id,
                'type' => 'VPS US',
                'due_date' => $date,
                'description' => 'Gia hạn VPS US',
                'status' => 'unpaid',
                'sub_total' => $total,
                'quantity' => 1,
                'user_id' => $user->id,
                'addon_id' => !empty($invoiced->addon_id) ? 1 : 0,
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // Tạo lưu trữ khi gia hạn
            foreach ($list_vps_id as $key => $vps_id) {
                $vps = $this->vps->find($vps_id);
                $data_order_expired = [
                    'detail_order_id' => $detail_order->id,
                    'expired_id' => $vps->id,
                    'billing_cycle' => $billing_cycle,
                    'type' => 'vps',
                ];
                $this->order_expired->create($data_order_expired);
            }
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
                'type_gd' => '3',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);

            $billings = config('billing');
            // gui mail
            $email = $this->email->find($product->meta_product->email_expired);
            $url = url('');
            $url = str_replace(['http://','https://'], ['', ''], $url);
            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $vps->ip ,$billings[$vps->billing_cycle], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , 1, '', 'token', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
            $data = [
                'content' => $content,
                'user_name' => $user->name,
                'subject' => (!empty($email->meta_email->subject)) ? $email->meta_email->subject : 'Thư xác nhận gia hạn vps us'
            ];
            try {
              // ghi log
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'tạo',
                'model' => 'Admin/Order',
                'description' => '  hoàn thành gia hạn VPS US <a href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                'service' =>  $vps->ip,
              ];
              $this->log_activity->create($data_log);
              $data_tb_send_mail = [
                'type' => 'mail_order_expire_vps_us',
                'type_service' => 'vps',
                'user_id' => $user->id,
                'status' => false,
                'content' => serialize($data),
              ];
              $this->send_mail->create($data_tb_send_mail);
            } catch (\Exception $e) {
              return $detail_order->id;
            }
        }
        return true;
    }

    public function create_order_expire_server($data)
    {
        $user = $this->user->find($data['user_id']);
        $server = $this->server->find($data['server_id']);
        $billing_cycle = $data['billing_cycle'];
        $total = $data['total_override_server'];

        if (empty($server)) {
          return false;
        }
        // create order
        $data_order = [
            'user_id' => $user->id,
            'total' => $total,
            'status' => $data['status'],
            'description' => 'expired server',
            'type' => 'expired',
            'makh' => '',
        ];
        $order = $this->order->create($data_order);
        $status = $data['status'];
        if ($status == 'Active' || $status == 'Finish') {
            if (!empty($data['check_remove_credit'])) {
                $credit = $user->credit;
                if (!empty($credit)) {
                    $credit->value = $credit->value - $total;
                    $credit->save();
                }
            }
            //create order detail
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $order->id,
                'type' => 'Server',
                'due_date' => $date,
                'paid_date' => date('Y-m-d'),
                'description' => 'Gia hạn Server',
                'status' => 'paid',
                'sub_total' => $total,
                'quantity' => 1,
                'user_id' => $user->id,
                'paid_date' => date('Y-m-d'),
                'addon_id' => '0',
                'payment_method' => 1,
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDEXS' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '3',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Active',
                'method_gd_invoice' => 'credit',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            // Tạo lưu trữ khi gia hạn
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $server->id,
                'billing_cycle' => $billing_cycle,
                'type' => 'server',
            ];
            $order_expired_vps = $this->order_expired->create($data_order_expired);
            // invoice
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $date_star = $server->next_due_date;
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$billing_cycle] ));
            $server->billing_cycle = $billing_cycle;
            $server->next_due_date = $due_date;
            $server->amount = $total;
            $server->save();

            // $reques_vcenter = true;
            $order->status = 'Finish';
            $order->save();
            try {
                $billings = config('billing');
                // ghi log
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'tạo',
                  'model' => 'Admin/Order',
                  'description' => '  hoàn thành gia hạn Server <a href="/admin/server/detail/' . $server->id . '">' . $server->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                  'service' =>  $server->ip,
                ];
                $this->log_activity->create($data_log);
                // gui mail
                // $this->send_mail_finish_expired('vps',$order, $detail_order ,$order_expired_vps);
            } catch (\Exception $e) {
                report($e);
                return true;
            }
        }
        else {
            //create order detail
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $order->id,
                'type' => 'Server',
                'due_date' => $date,
                'description' => 'Gia hạn Server',
                'status' => 'unpaid',
                'sub_total' => $total,
                'quantity' => 1,
                'user_id' => $user->id,
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // Tạo lưu trữ khi gia hạn
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $server->id,
                'billing_cycle' => $billing_cycle,
                'type' => 'server',
            ];
            $this->order_expired->create($data_order_expired);

            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDEXS' . strtoupper(substr(sha1(time()), 34, 39)),
                'type_gd' => '3',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);

            $billings = config('billing');

            try {
              // ghi log
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'tạo',
                'model' => 'Admin/Order',
                'description' => ' gia hạn Server <a href="/admin/server/detail/' . $server->id . '">' . $server->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                'service' =>  $server->ip,
              ];
              $this->log_activity->create($data_log);

              // $this->send_mail(!empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận gia hạn vps', $data, $user);
            } catch (\Exception $e) {
              return true;
            }
        }
        return true;
    }

    public function create_order_expire_colocation($data)
    {
        $user = $this->user->find($data['user_id']);
        $colocation = $this->colocation->find($data['colo_id']);
        $billing_cycle = $data['billing_cycle'];
        $total = $data['col_amount'];

        if (empty($colocation)) {
          return false;
        }
        // create order
        $data_order = [
            'user_id' => $user->id,
            'total' => $total,
            'status' => $data['status'],
            'description' => 'expired colocation',
            'type' => 'expired',
            'makh' => '',
        ];
        $order = $this->order->create($data_order);
        $status = $data['status'];
        if ($status == 'Active' || $status == 'Finish') {
            if (!empty($data['check_remove_credit'])) {
                $credit = $user->credit;
                if (!empty($credit)) {
                    $credit->value = $credit->value - $total;
                    $credit->save();
                }
            }
            //create order detail
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $order->id,
                'type' => 'Colocation',
                'due_date' => $date,
                'paid_date' => date('Y-m-d'),
                'description' => 'Gia hạn Colocation',
                'status' => 'paid',
                'sub_total' => $total,
                'quantity' => 1,
                'user_id' => $user->id,
                'paid_date' => date('Y-m-d'),
                'addon_id' => '0',
                'payment_method' => 1,
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDEXC' . strtoupper(substr(sha1(time()), 34, 39)),
                'discription' => 'Hóa đơn số ' . $detail_order->id,
                'type_gd' => '3',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Active',
                'method_gd_invoice' => 'credit',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);
            // Tạo lưu trữ khi gia hạn
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $colocation->id,
                'billing_cycle' => $billing_cycle,
                'type' => 'colocation',
            ];
            $order_expired_vps = $this->order_expired->create($data_order_expired);
            // invoice
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $date_star = $colocation->next_due_date;
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$billing_cycle] ));
            $colocation->billing_cycle = $billing_cycle;
            $colocation->next_due_date = $due_date;
            $colocation->amount = $total;
            $colocation->save();

            // $reques_vcenter = true;
            $order->status = 'Finish';
            $order->save();
            try {
                $billings = config('billing');
                // ghi log
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'tạo',
                  'model' => 'Admin/Order',
                  'description' => '  hoàn thành gia hạn Server <a href="/admin/colocation/detail/' . $colocation->id . '">' . $colocation->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                  'service' =>  $colocation->ip,
                ];
                $this->log_activity->create($data_log);
                // gui mail
                // $this->send_mail_finish_expired('vps',$order, $detail_order ,$order_expired_vps);
            } catch (\Exception $e) {
                report($e);
                return true;
            }
        }
        else {
            //create order detail
            $date = date('Y-m-d');
            $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
            $data_order_detail = [
                'order_id' => $order->id,
                'type' => 'Colocation',
                'due_date' => $date,
                'description' => 'Gia hạn Colocation',
                'status' => 'unpaid',
                'sub_total' => $total,
                'quantity' => 1,
                'user_id' => $user->id,
            ];
            $detail_order = $this->detail_order->create($data_order_detail);
            // Tạo lưu trữ khi gia hạn
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $colocation->id,
                'billing_cycle' => $billing_cycle,
                'type' => 'colocation',
            ];
            $this->order_expired->create($data_order_expired);

            $data_history = [
                'user_id' => $user->id,
                'ma_gd' => 'GDEXC' . strtoupper(substr(sha1(time()), 34, 39)),
                'type_gd' => '3',
                'method_gd' => 'invoice',
                'date_gd' => date('Y-m-d'),
                'money'=> $total,
                'status' => 'Pending',
                'detail_order_id' => $detail_order->id,
            ];
            $this->history_pay->create($data_history);

            $billings = config('billing');

            try {
              // ghi log
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'tạo',
                'model' => 'Admin/Order',
                'description' => ' gia hạn Colocation <a href="/admin/colocation/detail/' . $colocation->id . '">' . $colocation->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                'service' =>  $colocation->ip,
              ];
              $this->log_activity->create($data_log);

              // $this->send_mail(!empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận gia hạn vps', $data, $user);
            } catch (\Exception $e) {
              return true;
            }
        }
        return true;
    }

    public function get_addon_product_private($user_id)
    {
        $products = [];
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_user = $user->group_user;
            $group_products = $group_user->group_products;
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->get();
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        }
        return $products;
    }

    public function create_order_expire_hosting($data)
    {
        $user = $this->user->find($data['user_id']);
        $hosting = $this->hosting->find($data['hosting_id']);
        $billing_cycle = $data['billing_cycle'];
        $total = 0;
        $product = $this->product->find($hosting->product_id);
        $invoiced = $hosting->detail_order;
        if (!empty($hosting)) {
            if (!empty($hosting->price_override)) {
              $total = $hosting->price_override;
            } else {
              $total = $product->pricing[$billing_cycle];
            }
            // create order
            $data_order = [
                'user_id' => $user->id,
                'total' =>  $total,
                'status' => 'Pending',
                'description' => 'Gia hạn',//thiếu status gia hạn
                'type' => 'expired',
                'makh' => !empty($invoiced->order->makh) ? $invoiced->order->makh : ''
            ];
            $order = $this->order->create($data_order);
            $status = $data['status'];
            if ($status == 'Active' || $status == 'Finish') {
                if (!empty($data['check_remove_credit'])) {
                    $credit = $user->credit;
                    if (!empty($credit)) {
                        $credit->value = $credit->value - $total;
                        $credit->save();
                    }
                }
                //create order detail
                $date = date('Y-m-d');
                $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
                $data_order_detail = [
                    'order_id' => $order->id,
                    'type' => 'hosting',
                    'due_date' => $date,
                    'paid_date' => date('Y-m-d'),
                    'description' => 'Gia hạn Hosting',
                    'status' => 'paid',
                    'sub_total' => $total,
                    'quantity' => 1,
                    'user_id' => $user->id,
                    'paid_date' => date('Y-m-d'),
                    'addon_id' => '0',
                    'payment_method' => 1,
                ];
                $detail_order = $this->detail_order->create($data_order_detail);
                $data_history = [
                    'user_id' => $user->id,
                    'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
                    'discription' => 'Hóa đơn số ' . $detail_order->id,
                    'type_gd' => '3',
                    'method_gd' => 'invoice',
                    'date_gd' => date('Y-m-d'),
                    'money'=> $total,
                    'status' => 'Active',
                    'method_gd_invoice' => 'credit',
                    'detail_order_id' => $detail_order->id,
                ];
                $this->history_pay->create($data_history);
                // Tạo lưu trữ khi gia hạn
                $data_order_expired = [
                    'detail_order_id' => $detail_order->id,
                    'expired_id' => $hosting->id,
                    'billing_cycle' => $billing_cycle,
                    'type' => 'hosting',
                ];
                $order_expired_hosting = $this->order_expired->create($data_order_expired);
                // invoice
                $billing = [
                    'monthly' => '1 Month',
                    'twomonthly' => '2 Month',
                    'quarterly' => '3 Month',
                    'semi_annually' => '6 Month',
                    'annually' => '1 Year',
                    'biennially' => '2 Year',
                    'triennially' => '3 Year'
                ];
                $date_star = $hosting->next_due_date;
                $due_date = date('Y-m-d', strtotime( $date_star . $billing[$billing_cycle] ));
                if ( !empty($data['total_price_override']) ) {
                    $hosting->price_override = $data['total_price_override'];
                    $hosting->next_due_date = $due_date;
                    $hosting->billing_cycle = $billing_cycle;
                    $hosting->save();
                } else {
                    $hosting->price_override = null;
                    $hosting->next_due_date = $due_date;
                    $hosting->billing_cycle = $billing_cycle;
                    $hosting->save();
                }
                $order->status = 'Finish';
                $order->save();
                try {
                    $billings = config('billing');
                    // ghi log
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'tạo',
                      'model' => 'Admin/Order',
                      'description' => ' hoàn thành gia hạn Hosting <a href="/admin/hosting/detail/' . $hosting->id . '">' . $hosting->domain . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                      'service' =>  $hosting->ip,
                    ];
                    $this->log_activity->create($data_log);
                    // gui mail
                    $this->send_mail_finish_expired('hosting',$order, $detail_order ,$order_expired_hosting);
                } catch (\Exception $e) {
                    report($e);
                    return true;
                }
                return true;
            } else {
                //create order detail
                $date = date('Y-m-d');
                $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
                $data_order_detail = [
                    'order_id' => $order->id,
                    'type' => 'hosting',
                    'due_date' => $date,
                    'description' => 'Gia hạn Hosting',
                    'status' => 'unpaid',
                    'sub_total' => $total,
                    'quantity' => 1,
                    'user_id' => $user->id,
                    'addon_id' => 0,
                ];
                $detail_order = $this->detail_order->create($data_order_detail);
                // create history pay
                $data_history = [
                    'user_id' => $user->id,
                    'ma_gd' => 'GDEXH' . strtoupper(substr(sha1(time()), 34, 39)),
                    'discription' => 'Hóa đơn số ' . $detail_order->id,
                    'type_gd' => '3',
                    'method_gd' => 'invoice',
                    'date_gd' => date('Y-m-d'),
                    'money'=> $total,
                    'status' => 'Pending',
                    'detail_order_id' => $detail_order->id,
                ];
                $history_pay = $this->history_pay->create($data_history);
                // gui mail
                if ($order && $detail_order && $data_history) {
                    // Tạo lưu trữ khi gia hạn
                    $data_order_expired = [
                        'detail_order_id' => $detail_order->id,
                        'expired_id' => $hosting->id,
                        'billing_cycle' => $billing_cycle,
                        'type' => 'hosting',
                    ];
                    $this->order_expired->create($data_order_expired);

                    $hosting->expired_id = $detail_order->id;
                    $hosting->save();

                    $email = $this->email->find($product->meta_product->email_expired);

                    $billings = config('billing');
                    $url = url('');
                    $url = str_replace(['http://','https://'], ['', ''], $url);
                    $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                    $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $hosting->domain, '' ,$billings[$billing_cycle], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , 1, '', 'token', $url];
                    $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
                    $data = [
                        'content' => $content,
                        'user_name' => $user->name,
                        'subject' => (!empty($email->meta_email->subject)) ? $email->meta_email->subject : 'Thư xác nhận gia hạn hosting'
                    ];

                    try {
                      // ghi log
                      $data_log = [
                        'user_id' => Auth::user()->id,
                        'action' => 'tạo',
                        'model' => 'Admin/Order',
                        'description' => ' gia hạn Hosting <a href="/admin/hosting/detail/' . $hosting->id . '">' . $hosting->domain . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                        'service' =>  $hosting->ip,
                      ];
                      $this->log_activity->create($data_log);

                      $data_tb_send_mail = [
                        'type' => 'mail_order_expire_hosting',
                        'type_service' => 'hosting',
                        'user_id' => $user->id,
                        'status' => false,
                        'content' => serialize($data),
                      ];
                      $this->send_mail->create($data_tb_send_mail);
                    } catch (\Exception $e) {
                      report($e);
                      return true;
                    }
                    return true;
                }
            }

        } else {
          return false;
        }
        return true;
    }

    public function create_order_email_expire_hosting($data)
    {
        $user = $this->user->find($data['user_id']);
        $hosting = $this->email_hosting->find($data['email_hosting_id']);
        $billing_cycle = $data['billing_cycle'];
        $total = 0;
        $product = $this->product->find($hosting->product_id);
        $invoiced = $hosting->detail_order;
        if (!empty($hosting)) {
            if (!empty($hosting->price_override)) {
              $total = $hosting->price_override;
            } else {
              $total = $product->pricing[$billing_cycle];
            }
            // create order
            $data_order = [
                'user_id' => $user->id,
                'total' =>  $total,
                'status' => 'Pending',
                'description' => 'Gia hạn',//thiếu status gia hạn
                'type' => 'expired',
                'makh' => !empty($invoiced->order->makh) ? $invoiced->order->makh : ''
            ];
            $order = $this->order->create($data_order);
            $status = $data['status'];
            if ($status == 'Active' || $status == 'Finish') {
                if (!empty($data['check_remove_credit'])) {
                    $credit = $user->credit;
                    if (!empty($credit)) {
                        $credit->value = $credit->value - $total;
                        $credit->save();
                    }
                }
                //create order detail
                $date = date('Y-m-d');
                $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
                $data_order_detail = [
                    'order_id' => $order->id,
                    'type' => 'Email Hosting',
                    'due_date' => $date,
                    'paid_date' => date('Y-m-d'),
                    'description' => 'Gia hạn Email Hosting',
                    'status' => 'paid',
                    'sub_total' => $total,
                    'quantity' => 1,
                    'user_id' => $user->id,
                    'paid_date' => date('Y-m-d'),
                    'addon_id' => '0',
                    'payment_method' => 1,
                ];
                $detail_order = $this->detail_order->create($data_order_detail);
                $data_history = [
                    'user_id' => $user->id,
                    'ma_gd' => 'GDEEM' . strtoupper(substr(sha1(time()), 34, 39)),
                    'discription' => 'Hóa đơn số ' . $detail_order->id,
                    'type_gd' => '3',
                    'method_gd' => 'invoice',
                    'date_gd' => date('Y-m-d'),
                    'money'=> $total,
                    'status' => 'Active',
                    'method_gd_invoice' => 'credit',
                    'detail_order_id' => $detail_order->id,
                ];
                $this->history_pay->create($data_history);
                // Tạo lưu trữ khi gia hạn
                $data_order_expired = [
                    'detail_order_id' => $detail_order->id,
                    'expired_id' => $hosting->id,
                    'billing_cycle' => $billing_cycle,
                    'type' => 'EmailHosting',
                ];
                $order_expired_hosting = $this->order_expired->create($data_order_expired);
                // invoice
                $billing = [
                    'monthly' => '1 Month',
                    'twomonthly' => '2 Month',
                    'quarterly' => '3 Month',
                    'semi_annually' => '6 Month',
                    'annually' => '1 Year',
                    'biennially' => '2 Year',
                    'triennially' => '3 Year'
                ];
                $date_star = $hosting->next_due_date;
                $due_date = date('Y-m-d', strtotime( $date_star . $billing[$billing_cycle] ));
                if ( !empty($data['total_price_override']) ) {
                    $hosting->price_override = $data['total_price_override'];
                    $hosting->next_due_date = $due_date;
                    $hosting->billing_cycle = $billing_cycle;
                    $hosting->save();
                } else {
                    // $hosting->price_override = null;
                    $hosting->next_due_date = $due_date;
                    $hosting->billing_cycle = $billing_cycle;
                    $hosting->save();
                }
                $order->status = 'Finish';
                $order->save();
                try {
                    $billings = config('billing');
                    // ghi log
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'tạo',
                      'model' => 'Admin/Order',
                      'description' => ' hoàn thành gia hạn Hosting <a href="/admin/hosting/detail/' . $hosting->id . '">' . $hosting->domain . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                      'service' =>  $hosting->ip,
                    ];
                    $this->log_activity->create($data_log);
                    // gui mail
                    $this->send_mail_finish_expired('email_hosting',$order, $detail_order ,$order_expired_hosting);
                } catch (\Exception $e) {
                    report($e);
                    return true;
                }
                return true;
            } else {
                //create order detail
                $date = date('Y-m-d');
                $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
                $data_order_detail = [
                    'order_id' => $order->id,
                    'type' => 'Email Hosting',
                    'due_date' => $date,
                    'description' => 'Gia hạn Email Hosting',
                    'status' => 'unpaid',
                    'sub_total' => $total,
                    'quantity' => 1,
                    'user_id' => $user->id,
                    'addon_id' => 0,
                ];
                $detail_order = $this->detail_order->create($data_order_detail);
                // create history pay
                $data_history = [
                    'user_id' => $user->id,
                    'ma_gd' => 'GDEEM' . strtoupper(substr(sha1(time()), 34, 39)),
                    'discription' => 'Hóa đơn số ' . $detail_order->id,
                    'type_gd' => '3',
                    'method_gd' => 'invoice',
                    'date_gd' => date('Y-m-d'),
                    'money'=> $total,
                    'status' => 'Pending',
                    'detail_order_id' => $detail_order->id,
                ];
                $history_pay = $this->history_pay->create($data_history);
                // gui mail
                if ($order && $detail_order && $data_history) {
                    // Tạo lưu trữ khi gia hạn
                    $data_order_expired = [
                        'detail_order_id' => $detail_order->id,
                        'expired_id' => $hosting->id,
                        'billing_cycle' => $billing_cycle,
                        'type' => 'EmailHosting',
                    ];
                    $this->order_expired->create($data_order_expired);

                    $email = $this->email->find($product->meta_product->email_expired);

                    $billings = config('billing');
                    $url = url('');
                    $url = str_replace(['http://','https://'], ['', ''], $url);
                    $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                    $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $hosting->domain, '' ,$billings[$billing_cycle], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , 1, '', 'token', $url];
                    $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
                    $data = [
                        'content' => $content,
                        'user_name' => $user->name,
                    ];

                    try {
                      // ghi log
                      $data_log = [
                        'user_id' => Auth::user()->id,
                        'action' => 'tạo',
                        'model' => 'Admin/Order',
                        'description' => ' gia hạn Hosting <a href="/admin/hosting/detail/' . $hosting->id . '">' . $hosting->domain . '</a> '  . $billings[$data_order_expired['billing_cycle']],
                        'service' =>  $hosting->ip,
                      ];
                      $this->log_activity->create($data_log);

                      $this->send_mail(!empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận gia hạn Email Hosting', $data, $user);
                    } catch (\Exception $e) {
                      report($e);
                      return true;
                    }
                    return true;
                }
            }

        } else {
          return false;
        }
        return true;
    }

    public function send_mail_finish_expired($type, $order , $detail_order, $order_expired)
    {
        $billings = config('billing');
        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $add_on =  '';
        if ($type == 'vps') {
            $services = $order_expired->vps;
            $product = $this->product->find($services->product_id);
            $email = $this->email->find($product->meta_product->email_expired_finish);

            $user = $this->user->find($services->user_id);
            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' , $detail_order->quantity, $services->os, '', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

            $data = [
                'content' => $content,
                'user_name' => $user->name,
                'subject' => $email->meta_email->subject
            ];
            $data_tb_send_mail = [
              'type' => 'mail_finish_order_expire_vps',
              'type_service' => 'vps',
              'user_id' => $user->id,
              'status' => false,
              'content' => serialize($data),
            ];
            $this->send_mail->create($data_tb_send_mail);

            // $mail = Mail::send('users.mails.orders', $data, function($message){
            //     $message->from('support@cloudzone.vn', 'Cloudzone Portal');
            //     $message->to($this->user_send_email)->subject($this->subject);
            // });
        }
        elseif ($type == 'hosting') {
            $services = $order_expired->hosting;
            $product = $this->product->find($services->product_id);
            $email = $this->email->find($product->meta_product->email_expired_finish);
            $user = $this->user->find($services->user_id);
            $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

            $data = [
                'content' => $content,
                'user_name' => $user->name,
                'subject' => $email->meta_email->subject
            ];
            $data_tb_send_mail = [
              'type' => 'mail_finish_order_expire_hosting',
              'type_service' => 'hosting',
              'user_id' => $user->id,
              'status' => false,
              'content' => serialize($data),
            ];
            $this->send_mail->create($data_tb_send_mail);
        }
        elseif ($type == 'email_hosting') {
            $services = $order_expired->email_hosting;
            $product = $this->product->find($services->product_id);
            $email = $this->email->find($product->meta_product->email_expired_finish);
            $user = $this->user->find($services->user_id);
            $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

            $this->user_send_email = $this->user->find($services->user_id)->email;
            $this->subject = !empty($email->meta_email->subject) ? $email->meta_email->subject : 'Gia hạn dịch vụ Email Hosting';

            $data = [
                'content' => $content,
                'user_name' => $user->name,
                'subject' => $email->meta_email->subject
            ];

            $mail = Mail::send('users.mails.orders', $data, function($message){
                $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                $message->to($this->user_send_email)->subject($this->subject);
            });
        }
    }

}
