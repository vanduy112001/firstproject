<?php

namespace App\Repositories\Admin;

use App\Model\User;
use App\Model\LogActivity;
use App\Model\Colocation;
use App\Model\EmailHosting;
use App\Model\Product;
use App\Model\DetailOrder;

class EmailHostingRepositories {

    protected $user;
    protected $colo;
    protected $product;
    protected $detail_order;
    protected $log;

    public function __construct()
    {
        $this->user = new User;
        $this->colo = new Colocation;
        $this->log = new LogActivity;
        $this->product = new Product;
        $this->detail_order = new DetailOrder;
        $this->email_hosting = new EmailHosting;
    }

    public function list_email_hostings()
    {
        $list_email_hostings = $this->email_hosting->orderBy('id', 'desc')->get();
        return $list_email_hostings;
    }

    public function detail($id)
    {
        $list_email_hosting = $this->email_hosting->find($id);
        return $list_email_hosting;
    }

    public function update($data)
    {
        $product = $this->product->find($data['product_id']);
        $detail_order = $this->detail_order->find($data['invoice_id']);
        $order = $detail_order->order;
        $email_hosting = $this->email_hosting->find($data['id']);
        $data_update = [
            'user_id' => $data['user_id'],
            'product_id' => $data['product_id'],
            'domain' => $data['domain'],
            'user' => $data['sevices_username'],
            'password' => $data['sevices_password'],
            'status' => $data['status'],
            'expire_billing_cycle' => !empty($data['expire_billing_cycle']) ? true : false,
        ];
        if ( !empty($data['date_create']) ) {
            $data_update['date_create'] = date('Y-m-d', strtotime( $data['date_create'] ));
            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $data_update['next_due_date'] = date('Y-m-d', strtotime( $data['date_create'] . $billing[$email_hosting->billing_cycle] ));
        }
        if ( !empty($data['billing_cycle']) ) {
            $data_update['billing_cycle'] = $data['billing_cycle'];
            $data_update['next_due_date'] = date('Y-m-d', strtotime( $data['date_create'] . $billing[$email_hosting->billing_cycle] ));
        }
        if ( !empty($data['next_due_date']) ) {
            $data_update['next_due_date'] = date('Y-m-d', strtotime( $data['next_due_date'] ));
        }
        if ( !empty($data['sub_total']) ) {
          $detail_order->sub_total = $data['sub_total'];
          $detail_order->save();
          $order->total = $data['sub_total'];
          $order->save();
        }
        if ( !empty($data['price_override']) ) {
          $data_update['price_override'] = !empty($data['sub_total']) ? $data['sub_total'] : '';
        }
        $update = $email_hosting->update($data_update);
        return $update;
    }

    public function delete($id)
    {
        $delete = $this->email_hosting->where('id', $id)->delete();
        return $delete;
    }

}

?>
