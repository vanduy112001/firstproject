<?php

namespace App\Repositories\User;

use App\Model\User;
use App\Model\UserMeta;
use App\Model\GroupUser;
use App\Model\Domain;
use App\Model\DomainProduct;
use App\Model\DetailOrder;
use App\Model\EventPromotion;
use Illuminate\Support\Facades\Auth;
// API
use App\Services\DomainPA;

class domainRepositories
{

    protected $domain;
    protected $domain_product;
    protected $detail_order;
    protected $pavietnam;
    protected $event_promotion;
    protected $user;
    protected $user_meta;

    public function __construct()
    {
        $this->domain = new Domain;
        $this->domain_product = new DomainProduct;
        $this->detail_order = new DetailOrder;
        $this->pavietnam = new DomainPA();
        $this->event_promotion = new EventPromotion();
        $this->user = new User();
        $this->user_meta = new UserMeta();
    }

    public function get_domain($user_id)
    {
        return $this->domain->where('user_id', $user_id)->with('domain_product', 'detail_order')->orderBy('id', 'desc')->paginate(15);
    }
    public function get_product_domain()
    {
        return $this->domain_product->get();
    }
    public function search_domain()
    {
        return $domains = $this->domain_product->get();
    }
    public function get_product_domain_by_id($id)
    {
        $user_id = Auth::user()->id;
        $user_meta = $this->user_meta->where('user_id', $user_id)->first();
        if($user_meta->role === 'admin') {
          $domain = $this->domain->find($id);
        } else {
          $domain = $this->domain->where('user_id', $user_id)->find($id);
        }
        if($domain) {
          $domain_product = $this->domain_product->where('type', $domain->domain_ext)->first();
          $billings = config('billing_domain_exp');
          $array_value = [
              'domain' => $domain,
              'domain_product' => $domain_product,
              'billings' => $billings,
          ];
        } else {
          $array_value = null;
        }
        return $array_value;
    }

    public function check_event($id)
    {
        $event = $this->event_promotion->where('id', $id)->where('user_id', Auth::user()->id)->where('child_id', null)->first();
        if (isset($event)) {
           return true;
        } else {
           return false;
        }
    }

    public function get_domain_by_id($domain_id)
     {
         return $domain = $this->domain->find($domain_id);
     }

    public function info($id)
    {
        $user_id = Auth::user()->id;
        $info = $this->domain->where('user_id', $user_id)->find($id);
        if($info) {
          if(strtotime(date('Y-m-d')) < strtotime($info->next_due_date)) {
            return json_encode($info);
          } else {
            return $info = null;
          }
        } else {
          return $info = null;
        }     
    }

    public function change_pass_domain($id, $pass)
    {
      $user_id = Auth::user()->id;
      $get_domain = $this->domain->where('user_id', $user_id)->find($id);
      if(!$get_domain) {
        return $data = null;
      }
      $old_pass =  $get_domain->password_domain;
      $update_pass = $get_domain->update(['password_domain' => $pass]);
      if($update_pass) {
        $domain = $get_domain->domain;
        $request_panhanhoa = $this->pavietnam->change_pass_domain($domain, $pass);
        if ($request_panhanhoa->{'ReturnCode'} == 200) {
        // if ($request_panhanhoa['ReturnCode'] == 200) {
          $data = [
            'status' => true,
            'message' => 'Đổi mật khẩu thành công',
            'password' => $get_domain->password_domain,
          ];
        } else { //Nếu cập nhập qua PA lỗi thì trả lại pass domain cũ cho portal
          $get_domain->update(['password_domain' => $old_pass]);
          $data = [
            'status' => false,
            'message' => $request_panhanhoa->{'ReturnText'},
            'password' => $get_domain->password_domain,
          ];
        }

      } else {
        $data = [
          'status' => false,
          'message' => 'Đổi mật khẩu thất bại',
          'password' => $get_domain->password_domain,
        ];
      }
      return json_encode($data);
    }

    public function get_data_user($user_id) {
      return $this->user->where('id', $user_id)->with('user_meta')->first();
    }
}
