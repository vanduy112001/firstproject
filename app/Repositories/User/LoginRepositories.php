<?php

namespace App\Repositories\User;

use App\Model\User;
use App\Model\UserMeta;
use App\Model\Verify;
use App\Model\SocialAccount;
use App\Model\Credit;
use App\Model\Email;
use App\Jobs\SendMailForgot;
use App\Model\SendMail;
use Illuminate\Support\Facades\Hash;
use App\Jobs\SendVerifyEmail;
use Mail;
use Auth;
use GuzzleHttp\Client;
use http\Env\Request;
// thư viện
use App\Services\SocialAccountService;
use Illuminate\Support\Facades\Log;
use Socialite;
use Laravel\Socialite\Contracts\Provider;
use Laravel\Socialite\Contracts\User as ProviderUser;
// use Illiminate\Http\Response;
use App\Factories\AdminFactories;

class LoginRepositories
{

    protected $user;
    protected $user_meta;
    protected $verify;
    protected $email;
    protected $social;
    protected $credit;
    protected $dashboard;
    protected $send_mail;

    public function __construct()
    {
        $this->user = new User;
        $this->email = new Email;
        $this->user_meta = new UserMeta;
        $this->verify = new Verify;
        $this->social = new SocialAccount;
        $this->credit = new Credit();
        $this->send_mail = new SendMail;
        // API
        $this->dashboard = AdminFactories::dashBoardRepositories();
    }

    public function detail($id)
    {
        return $this->user->with('user_meta')->find($id);
    }

    public function check_recapcha($data)
    {
        $client = new Client();
        $capcha = config('capcha');
        $url = 'https://www.google.com/recaptcha/api/siteverify';
        $data_response = [
          'secret' => $capcha['secrep_key'],
          'response' => $data['recaptcha'],
        ];
        $response = $client->post(
          $url ,
          [
            'form_params' => $data_response,
          ]
        );
        $body = json_decode((string)$response->getBody());
        return $body->success;
    }

    public function createUser($data)
    {
        $data_user = [
            'name' => $data['last_name'] . ' ' . $data['first_name'],
            'email' => $data['email'],
            'password' => $data['password'],
        ];
        $user = $this->user->create($data_user);

        if ($user) {
            try {
                $dashboard = $this->dashboard->sendUserDashBoard($user);
            } catch (Exception $e) {
                report($e);
                $user->delete();
                return [ "error" => 5 ];
            }
            $data_meta_user = [
                'user_id' => $user->id,
                'role' => 'user',
                'address' => $data['address'],
                'phone' => $data['phone'],
                'gender' => $data['gender'],
                'date' => $data['date'],
                'affiliate_token' => sha1(time()),
                'makh' => 'MAKH' . $this->rand_string(6),
                'company' => !empty($data['company']) ? $data['company'] : '',
            ];
            $user_meta = $this->user_meta->create($data_meta_user);

            $data_verify = [
                'user_id' => $user->id,
                'token' => $this->rand_string(32),
            ];
            $verify = $this->verify->create($data_verify);

            $data_credit = [
                'user_id' => $user->id,
                'total' => '0',
                'value' => '0',
            ];
            $this->credit->create($data_credit);

            try {
                $mail_system = $this->email->where('type', 3)->where('type_service', 'create_user')->first();
                if ( isset($mail_system) ) {
                    $search = [ '{$user_id}', '{$user_name}',  '{$user_email}', '{$user_credit}', '{$url}', '{$token}', '{$link_verify}' ];
                    $replace = [ $user->id, $user->name, $user->email, !empty($user->credit->value) ? $user->credit->value : 0, url(''), $data_verify['token'], url('check-verify' , $data_verify['token'])];
                    $content = str_replace($search, $replace, !empty($mail_system->meta_email->content)? $mail_system->meta_email->content : '');
                    $data_user = [
                        'content' => $content,
                        'subject' => !empty($mail_system->meta_email->subject)? $mail_system->meta_email->subject : 'XÁC NHẬN ĐĂNG KÍ TẠO TÀI KHOẢN WEB PORTAL',
                    ];
                } else {
                    $data_user = [
                        'name' => $user->name,
                        'email' => $user->email,
                        'token' => $data_verify['token'],
                        'subject' => 'XÁC NHẬN ĐĂNG KÍ TẠO TÀI KHOẢN WEB PORTAL'
                    ];
                }

                $data_tb_send_mail = [
                    'type' => 'mail_verify_user',
                    'user_id' => $user->id,
                    'status' => false,
                    'content' => serialize($data_user),
                ];
                $this->send_mail->create($data_tb_send_mail);
            } catch (\Throwable $th) {
                //throw $th;
                report($th);
                return [ "error" => 1 ];
            }

        } else {
            return [ "error" => 2 ];
        }

        if ($user_meta && $verify) {
            return [ "error" => 0 ];
        } else {
            return [ "error" => 2 ];
        }
    }

    public function verify($token)
    {
        $verify = $this->verify->where('token', $token)->first();
        if (isset($verify->user)) {
            $user = $verify->user;
            if (!empty($user->email_verified_at)) {
                // đã verify rồi thì k đc verify nữa
                return false;
            }
            $mail_system = $this->email->where('type', 3)->where('type_service', 'verify_user')->first();
            if ( isset($mail_system) ) {
                $search = [ '{$user_id}', '{$user_name}',  '{$user_email}', '{$user_credit}', '{$url}', '{$token}', '{$link_verify}' ];
                $replace = [ $user->id, $user->name, $user->email, !empty($user->credit->value) ? $user->credit->value : 0, url(''), $token, url('check-verify' , $token)];
                $content = str_replace($search, $replace, !empty($mail_system->meta_email->content)? $mail_system->meta_email->content : '');
                $data_user = [
                    'content' => $content,
                    'subject' => !empty($mail_system->meta_email->subject)? $mail_system->meta_email->subject : 'KÍCH HOẠT THÀNH CÔNG TÀI KHOẢN WEB PORTAL',
                ];
            }
            else {
                $data_user = [
                    'name' => $user->name,
                    'subject' => 'KÍCH HOẠT THÀNH CÔNG TÀI KHOẢN WEB PORTAL'
                ];
            }
            $data_tb_send_mail = [
                'type' => 'mail_tutorial_user',
                'user_id' => $user->id,
                'status' => false,
                'content' => serialize($data_user),
            ];
            $this->send_mail->create($data_tb_send_mail);

            $user->tutorial  = true;
            $user->email_verified_at = date("Y-m-d H:i:s");
            $update = $user->save();
            if ($update) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function registerSocial(ProviderUser  $provider, $social)
    {
        try {
          $accout = $this->social->where('provider_user_id', $provider->getId())->where('provider', $social)->first();
          if (isset($accout)) {
              // dd($accout->user);
              return [
                  'error' => 0,
                  'user' => $accout->user
              ];
          } else {
            return [
                'error' => 1,
                'user' => ''
            ];
            //   $user = $this->user->whereEmail($provider->getEmail())->first();
            //   if (!isset($user)) {
            //       // dd('chua co account');
            //       // tạo user
            //       $value_user = [
            //           'name' => $provider->getName(),
            //           'email' => $provider->getEmail(),
            //           'email_verified_at' => date("Y-m-d H:i:s"),
            //           'password' => $this->rand_string(12),
            //       ];
            //       $user = $this->user->create($value_user);
            //       // Tạo meta user
            //       $value_meta_user = [
            //           'user_id' => $user->id,
            //           'role' => 'user',
            //           'phone' => '',
            //           'address' => '',
            //           'gender' => '',
            //           'date' => '',
            //           'affiliate_token' => sha1(time()),
            //           'avatar' => $provider->getAvatar(),
            //           'makh' => 'MAKH' . $this->rand_string(6),
            //       ];
            //       $user_meta = $this->user_meta->create($value_meta_user);

            //       // Lưu id social của user
            //       $value_social = [
            //           'user_id' => $user->id,
            //           'provider_user_id' => $provider->getId(),
            //           'provider' => $social,
            //       ];
            //       $create_social = $this->social->create($value_social);
            //       // Tao credit
            //       $value_credit = [
            //           'user_id' => $user->id,
            //           'value' => '0',
            //           'total' => '0',
            //       ];
            //       $this->credit->create($value_credit);

            //       $dashboard = $this->dashboard->sendUserDashBoard($user);

            //       if ($user && $user_meta && $create_social) {
            //           try {
            //             $mail_system = $this->email->where('type', 3)->where('type_service', 'verify_user')->first();
            //             if ( isset($mail_system) ) {
            //                 $search = [ '{$user_id}', '{$user_name}',  '{$user_email}', '{$user_credit}', '{$url}', '{$token}', '{$link_verify}' ];
            //                 $replace = [ $user->id, $user->name, $user->email, !empty($user->credit->value) ? $user->credit->value : 0, url(''), $token, url('check-verify' , $token)];
            //                 $content = str_replace($search, $replace, !empty($mail_system->meta_email->content)? $mail_system->meta_email->content : '');
            //                 $data_user = [
            //                     'content' => $content,
            //                     'subject' => !empty($mail_system->meta_email->subject)? $mail_system->meta_email->subject : 'KÍCH HOẠT THÀNH CÔNG TÀI KHOẢN WEB PORTAL',
            //                 ];
            //             }
            //             else {
            //                 $data_user = [
            //                     'name' => $user->name,
            //                     'subject' => 'KÍCH HOẠT THÀNH CÔNG TÀI KHOẢN WEB PORTAL'
            //                 ];
            //             }
            //             $data_tb_send_mail = [
            //                 'type' => 'mail_tutorial_user',
            //                 'user_id' => $user->id,
            //                 'status' => false,
            //                 'content' => serialize($data_user),
            //             ];
            //             $this->send_mail->create($data_tb_send_mail);

            //             $user->tutorial  = true;
            //             $user->save();
            //           } catch (\Throwable $th) {
            //               //throw $th;
            //               report($th);
            //           }
            //           return $user;
            //       } else {
            //           return false;
            //       }
            //   } else {
            //       // dd('da co user');
            //       // Lưu id social của user
            //       return false;
            //   }
          }
        } catch (\Exception $e) {
          return false;
        }

    }

    public function rand_string($n)
    {
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen($chars);
        $str = '';
        for ($i = 0; $i < $n; $i++) {
            $str .= $chars[rand(0, $size - 1)];
        }
        return $str;
    }

    // Gửi link tạo lại mật khẩu qua mail (sử dụng hậu tố token trong link)
    public function send_email_forgot($email)
    {
        $send_mail = false;
        $user = $this->user->where('email', $email)->first();
        if (isset($user)) {
            // $data_verify = [
            //     'user_id' => $user->id,
            //     'token' => sha1(time())
            // ];
            $verify_user = $this->verify->where('user_id', $user->id)->first();

            //Cập nhập lại token và gửi mail nếu đã có token
            if ( !empty($verify_user) ) {
                $token = $verify_user->token;
                $mail_system = $this->email->where('type', 3)->where('type_service', 'forgot_password')->first();
                if ( isset($mail_system) ) {
                    $search = [ '{$user_id}', '{$user_name}',  '{$user_email}', '{$user_credit}', '{$url}', '{$token}', '{$link_verify}', '{$link_fogot_password}' ];
                    $replace = [ $user->id, $user->name, $user->email, !empty($user->credit->value) ? $user->credit->value : 0, url(''), $token, url('check-verify' , $token), route('reset_password_forgot_form', $token) ];
                    $content = str_replace($search, $replace, !empty($mail_system->meta_email->content)? $mail_system->meta_email->content : '');
                    $data_user = [
                        'content' => $content,
                        'subject' => !empty($mail_system->meta_email->subject)? $mail_system->meta_email->subject : 'Đặt lại mật khẩu tài khoản',
                    ];
                    $data_tb_send_mail = [
                        'type' => 'mail_tutorial_user',
                        'user_id' => $user->id,
                        'status' => false,
                        'content' => serialize($data_user),
                    ];
                } else {
                    $data_tb_send_mail = [
                        'type' => 'mail_forgot_password',
                        'user_id' => $user->id,
                        'status' => false,
                        'content' => '',
                    ];
                }                
                $this->send_mail->create($data_tb_send_mail);

                return true;
            } else {
                //Tạo mới token nếu người dùng không có
                $data_verify = [
                    'user_id' => $user->id,
                    'token' => $this->rand_string(32),
                ];
                $verify_create = $this->verify->create($data_verify);
                if ($verify_create) {
                    $token = $verify_create->token;
                    $mail_system = $this->email->where('type', 3)->where('type_service', 'forgot_password')->first();
                    if ( isset($mail_system) ) {
                        $search = [ '{$user_id}', '{$user_name}',  '{$user_email}', '{$user_credit}', '{$url}', '{$token}', '{$link_verify}', '{$link_fogot_password}' ];
                        $replace = [ $user->id, $user->name, $user->email, !empty($user->credit->value) ? $user->credit->value : 0, url(''), $token, url('check-verify' , $token), route('reset_password_forgot_form', $token) ];
                        $content = str_replace($search, $replace, !empty($mail_system->meta_email->content)? $mail_system->meta_email->content : '');
                        $data_user = [
                            'content' => $content,
                            'subject' => !empty($mail_system->meta_email->subject)? $mail_system->meta_email->subject : 'Đặt lại mật khẩu tài khoản',
                        ];
                        $data_tb_send_mail = [
                            'type' => 'mail_tutorial_user',
                            'user_id' => $user->id,
                            'status' => false,
                            'content' => serialize($data_user),
                        ];
                    } else {
                        $data_tb_send_mail = [
                            'type' => 'mail_forgot_password',
                            'user_id' => $user->id,
                            'status' => false,
                            'content' => '',
                        ];
                    } 
                    $this->send_mail->create($data_tb_send_mail);

                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    // Xác thực lại token khi đặt lại mật khẩu
    public function view_forgot_password($token)
    {
        $verify_user = $this->verify->where('token', $token)->first();

        if (isset($verify_user)) {
            if (!empty($verify_user->user)) {
                return $verify_user->user;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public function change_password($data)
    {
        $user_id = $this->verify->where('token', $data['token'])->first()->user_id;
        if (isset($user_id)) {
          $update_new_password = $this->user->find($user_id)->update(['password' => $data['new_password']]);

          //Cập nhập lại token
          $data_verify = [
            'user_id' => $user_id,
            'token' => sha1(time())
          ];
          $this->verify->where('user_id', $user_id)->update($data_verify);

          if ($update_new_password) {
            return true;
          } else {
            return false;
          }
        } else {
          return false;
        }
    }

    public function checkToken($token) {
      if ($this->verify->where('token', $token)->first()) {
        return true;
      } else {
        return false;
      }
    }

    public function updateProfile($data)
    {
        $data_user =  [
            'name' => $data['name'],
        ];
        $this->user->find($data['user_id'])->update($data_user);

        try {
          $data_meta_user = [
              'phone' => $data['phone'],
              'gender' => $data['gender'],
              'address' => $data['address'],
              'company' => $data['company'],
              'date' => date('Y-m-d', strtotime($data['date'])),
          ];
        } catch (\Exception $e) {
          return false;
        }

        $update = $this->user_meta->where('user_id', $data['user_id'])->update($data_meta_user);

        return $update;

    }
}
