<?php

namespace App\Repositories\User;

use App\Model\User;
use App\Model\UserMeta;
use App\Model\Verify;
use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\Email;
use Illuminate\Support\Facades\Auth;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\Server;
use App\Model\Hosting;
use App\Model\OrderVetify;
use App\Model\Credit;
use Mail;
use App\Model\MetaGroupProduct;
use App\Model\DomainProduct;

class HomeRepositories {

    protected $user;
    protected $user_meta;
    protected $verify;
    protected $email;
    protected $group_product;
    protected $product;
    protected $order;
    protected $detail_order;
    protected $vps;
    protected $server;
    protected $hosting;
    protected $user_email;
    protected $subject;
    protected $order_vetify;
    protected $credit;
    protected $meta_group_product;
    protected $domain_product;


    public function __construct()
    {
        $this->user = new User;
        $this->email = new Email;
        $this->user_meta = new UserMeta;
        $this->verify = new Verify;
        $this->group_product = new GroupProduct;
        $this->product = new Product;
        $this->order = new Order;
        $this->detail_order = new DetailOrder;
        $this->vps = new Vps;
        $this->server = new Server;
        $this->hosting = new Hosting;
        $this->domain_product = new DomainProduct;
        $this->order_vetify = new OrderVetify;
        $this->credit = new Credit;
        $this->meta_group_product = new MetaGroupProduct;
    }



    public function get_group_product()
    {
        $group_products = $this->group_product->with('products')->get();
        return $group_products;
    }

    public function detail_product($product_id)
    {
        $product = $this->product->find($product_id);
        return $product;
    }
    public function detail_product_domain($product_id)
    {
        $product = $this->product->find($product_id);
        return $product;
    }

//    Tổng số product dang co
    public function total_product($user_id) {
        $total_vps = $this->vps->where('user_id', $user_id)->count();
        $total_server = $this->server->where('user_id', $user_id)->count();
        $total_hosting = $this->hosting->where('user_id', $user_id)->count();
        return $total_vps + $total_hosting + $total_server;
    }
//    Tổng số hóa đơn chưa thanh toán
    public function total_invoice_pending($user_id) {
        $total = $this->detail_order->where('user_id', $user_id)->where('status', 'unpaid')->count();
        return $total;
    }
//  Tổng số server
    public function total_server($user_id) {
        $total_server = $this->server->where('user_id', $user_id)->count();
        return $total_server;
    }
    //  Tổng số vps
    public function total_vps($user_id) {
        $total_vps = $this->vps->where('user_id', $user_id)->count();
        return $total_vps;
    }
    //  Tổng số server
    public function total_hosting($user_id) {
        $total_hosting = $this->hosting->where('user_id', $user_id)->count();
        return $total_hosting;
    }
//  Tổng số server
    public function get_total_sevices() {
        $user_id = Auth::user()->id;
        $data = [];
        $total_server = $this->server->where('user_id', $user_id)->get();
        $total_vps = $this->vps->where('user_id', $user_id)->get();
        $total_hosting = $this->hosting->where('user_id', $user_id)->get();
        foreach ($total_vps as $vps) {
            $data[] = $vps;
        }
        foreach ($total_hosting as $hosting) {
            $data[] = $hosting;
        }
        foreach ($total_server as $server) {
            $data[] = $server;
        }
        if (isset($data)) {
            foreach ($data as $key => $value) {
                for ($i=1; $i < count($data) - 1; $i++) {
                    if (strtotime($data[$i]['created_at']) < strtotime($data[$key]['created_at'])) {
                        $t = $data[$i];
                        $data[$i] = $data[$key];
                        $data[$key] = $t;
                    }
                }
            }
        }

        return $data;
    }
//    Tổng số hóa đơn hết hạn
    public function get_invoice_pending_home() {
        $invoices = $this->detail_order->where('user_id', Auth::user()->id)->where('status', 'Pending')->get();
        return $invoices;
    }

    public function order_vps($data)
    {
      // dd(' da den');
        //create order
        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $data['sub_total'] * $data['quantity'],
            'status' => 'Pending',
            'description' => $data['description'],
            'makh' => $data['makh'],
        ];
        // dd($data['makh']);
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => $data['type_product'],
            'due_date' => $date,
            'description' => !empty($data['description'])? $data['description'] : '',
            'status' => 'unpaid',
            'sub_total' => $data['sub_total'],
            'quantity' => $data['quantity'],
            'user_id' => Auth::user()->id,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        // vetify order
        $data_vetify_order = [
            'order_id' => $order->id,
            'token' => sha1(time()),
        ];
        $vetify = $this->order_vetify->create($data_vetify_order);
        //create vps
        if ($detail_order && $data_order) {
            for ($i=0; $i < $data['quantity']; $i++) {
                $data_vps = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' =>  Auth::user()->id,
                    'product_id' => $data['product_id'],
                    'ip' => !empty($data['ip'])? $data['ip'] : '',
                    'os' => $data['os'],
                    'billing_cycle' => $data['billing'],
                    'status' => 'Pending',
                    'user' => !empty($data['user'])? $data['user'] : '',
                    'password' => !empty($data['password'])? $data['password'] : '',
                ];
                $create_vps = $this->vps->create($data_vps);
                if(!$create_vps) {
                    return false;
                }
            }
        } else {
            return false;
        }
        $billings = config('billing');
        $user = $this->user->find(Auth::user()->id);
        $product = $this->product->find($data['product_id']);
        $email = $this->email->find($product->meta_product->email_create);
        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
        $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', '' ,$billings[$data_vps['billing_cycle']], '', $order->created_at, '', '',$data['sub_total'] , $data['sub_total'] * $data['quantity'], $data['quantity'], $data['os'], $data_vetify_order['token'], $url];
        $content = str_replace($search, $replace, $email->meta_email->content);
        $data = [
            'content' => $content,
            'user_name' => $user->name,
            'user_id' => $user->id,
            'user_email' => $user->email,
            'user_addpress' => $user->user_meta->address,
            'user_phone' => $user->user_meta->user_phone,
            'order_id' => $order->id,
            'product_name' => $product->name,
            'domain' => '',
            'ip' => '',
            'billing_cycle' => $data_vps['billing_cycle'],
            'next_due_date' => '',
            'order_created_at' => $order->created_at,
            'services_username' => '',
            'services_password' => '',
            'sub_total' => $data['sub_total'],
            'total' => $data['sub_total'] * $data['quantity'],
            'qtt' => $data['quantity'],
            'os' => $data['os'],
            'token' => $data_vetify_order['token'],
        ];

        $this->send_mail($email->meta_email->subject, $data);
        return true;
    }

    public function order_server($data)
    {
        $user = $this->user->find(Auth::user()->id);
        $product = $this->product->find($data['product_id']);
        $email = $this->email->find($product->meta_product->email_create);
        //create order
        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $data['sub_total'] * $data['quantity'],
            'status' => 'Pending',
            'description' => $data['description'],
            'makh' => $data['makh'],
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => $data['type_product'],
            'due_date' => $date,
            'description' => !empty($data['description'])? $data['description'] : '',
            'status' => 'unpaid',
            'sub_total' => $data['sub_total'],
            'quantity' => $data['quantity'],
            'user_id' => Auth::user()->id,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        // vetify order
        $data_vetify_order = [
            'order_id' => $order->id,
            'token' => sha1(time()),
        ];
        $vetify = $this->order_vetify->create($data_vetify_order);
        //create vps
        if ($detail_order && $data_order) {
            for ($i=0; $i < $data['quantity']; $i++) {
                $data_server = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' =>  Auth::user()->id,
                    'product_id' => $data['product_id'],
                    'ip' => !empty($data['ip'])? $data['ip'] : '',
                    'os' => $data['os'],
                    'type' => '0',
                    'billing_cycle' => $data['billing'],
                    'status' => 'Pending',
                    'user' => !empty($data['user'])? $data['user'] : '',
                    'password' => !empty($data['password'])? $data['password'] : '',
                ];
                $create_vps = $this->server->create($data_server);
                if(!$create_vps) {
                    return false;
                }
            }
        } else {
            return false;
        }

        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
        $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', '' ,$data_server['billing_cycle'], '', $order->created_at, '', '',$data['sub_total'] , $data['sub_total'] * $data['quantity'], $data['quantity'], $data['os'], $data_vetify_order['token'], $url];
        $content = str_replace($search, $replace, $email->meta_email->content);
        $data = [
            'content' => $content,
            'user_name' => $user->name,
            'user_id' => $user->id,
            'user_email' => $user->email,
            'user_addpress' => $user->user_meta->address,
            'user_phone' => $user->user_meta->user_phone,
            'order_id' => $order->id,
            'product_name' => $product->name,
            'domain' => '',
            'ip' => '',
            'billing_cycle' => $data_server['billing_cycle'],
            'next_due_date' => '',
            'order_created_at' => $order->created_at,
            'services_username' => '',
            'services_password' => '',
            'sub_total' => $data['sub_total'],
            'total' => $data['sub_total'] * $data['quantity'],
            'qtt' => $data['quantity'],
            'os' => $data['os'],
            'token' => $data_vetify_order['token'],
        ];

        $this->send_mail($email->meta_email->subject, $data);
        return true;
    }

    public function order_hosting($data)
    {
        $user = $this->user->find(Auth::user()->id);
        $product = $this->product->find($data['product_id']);
        $email = $this->email->find($product->meta_product->email_create);
        // dd($email);
        //create order
        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $data['sub_total'] * $data['quantity'],
            'status' => 'Pending',
            'description' => $data['description'],
            'makh' => $data['makh'],
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => $data['type_product'],
            'due_date' => $date,
            'description' => !empty($data['description'])? $data['description'] : '',
            'status' => 'unpaid',
            'sub_total' => $data['sub_total'],
            'quantity' => $data['quantity'],
            'user_id' => Auth::user()->id,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        // vetify order
        $data_vetify_order = [
            'order_id' => $order->id,
            'token' => sha1(time()),
        ];
        $vetify = $this->order_vetify->create($data_vetify_order);
        // $detail_order= 1;$data_order=1;
        //create vps
        if ($detail_order && $data_order) {
            for ($i=0; $i < $data['quantity']; $i++) {
                $domain_hosting = $data['domain'][$i];
                $user_hosting = str_replace('.', '', $domain_hosting);
                $user_hosting = substr($user_hosting, 0, 8);

                $data_hosting = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' => Auth::user()->id,
                    'product_id' => $data['product_id'],
                    'domain' => !empty($data['domain'][$i])? $data['domain'][$i] : '',
                    'billing_cycle' => $data['billing'],
                    'status' => 'Pending',
                    'user' => $user_hosting,
                    'password' => rand_string(),
                ];
                $create_vps = $this->hosting->create($data_hosting);
                if(!$create_vps) {
                    return false;
                }
            }
        } else {
            return false;
        }

        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
        $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', '' ,$data_hosting['billing_cycle'], '', $order->created_at, '', '',$data['sub_total'] , $data['sub_total'] * $data['quantity'], $data['quantity'], '', $data_vetify_order['token'], $url];
        $content = str_replace($search, $replace, $email->meta_email->content);
        $data = [
            'content' => $content,
            'user_name' => $user->name,
            'user_id' => $user->id,
            'user_email' => $user->email,
            'user_addpress' => $user->user_meta->address,
            'user_phone' => $user->user_meta->user_phone,
            'order_id' => $order->id,
            'product_name' => $product->name,
            'domain' => '',
            'ip' => '',
            'billing_cycle' => $data_hosting['billing_cycle'],
            'next_due_date' => '',
            'order_created_at' => $order->created_at,
            'services_username' => '',
            'services_password' => '',
            'sub_total' => $data['sub_total'],
            'total' => $data['sub_total'] * $data['quantity'],
            'qtt' => $data['quantity'],
            'os' => '',
            'token' => $data_vetify_order['token'],
        ];

        $this->send_mail($email->meta_email->subject, $data);
        return true;
    }

    public function send_mail($subject, $data)
    {
        $this->user_email = Auth::user()->email;
        $mail = Mail::send('users.mails.orders', $data, function($message){
            $message->from('uthienth92@gmail.com', 'Cloudzone');
            $message->to($this->user_email)->subject('Xác thực đơn hàng Cloudzone');
        });
    }

    public function verify_order($token)
    {
        $verify = $this->order_vetify->where('token', $token)->with('order')->first();
        if (isset($verify)) {
            $order = $verify->order;
            $order->verify = true;
            $order->save();
            $detail_order = $this->detail_order->where('order_id', $order->id)->first();
            return $detail_order;
        } else {
            return false;
        }

    }

    public function list_invoices_with_user()
    {
        $invoices = $this->detail_order->where('user_id', Auth::user()->id)->with('order')->paginate(30);
        return $invoices;
    }


    public function list_invoices_paid_with_user()
    {
        $invoices = $this->detail_order->where('status', 'paid')->where('user_id', Auth::user()->id)->with('order')->paginate(30);
        return $invoices;
    }


    public function list_invoices_unpaid_with_user()
    {
        $invoices = $this->detail_order->where('status', 'unpaid')->where('user_id', Auth::user()->id)->with('order')->paginate(30);
        return $invoices;
    }



    public function check_invoice($id)
    {
        $invoice = $this->detail_order->find($id);
        return $invoice;
    }

    public function check_credit($invoice_id)
    {
        $credit = $this->credit->where('user_id', Auth::user()->id)->first();
        if (!isset($credit)) {
            return false;
        }
        $invoice = $this->detail_order->find($invoice_id);
        // dd($invoice);
        $total = $invoice->sub_total * $invoice->quantity;
        if ($credit->value >= $total) {
            return true;
        } else {
            return false;
        }

    }

    public function payment($invoice_id)
    {
        $credit = $this->credit->where('user_id', Auth::user()->id)->first();
        if (!isset($credit)) {
            return false;
        }
        $invoice = $this->detail_order->find($invoice_id);
        $total = $invoice->sub_total * $invoice->quantity;
        // credit
        $credit->value = $credit->value - $total;
        $credit->save();
        // invoice
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        if ($invoice->type == 'VPS') {
            $list_vps = $this->vps->where('detail_order_id', $invoice_id)->get();
            $billing_cycle = $list_vps[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$list_vps[0]->billing_cycle] ));
            // dd($due_date);
            foreach ($list_vps as $key => $vps) {
                $vps->next_due_date = $due_date;
                $vps->paid = 'paid';
                $vps->save();
            }
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $paid = $invoice->save();
        } elseif ($invoice->type == 'Server') {
            $servers = $this->server->where('detail_order_id', $invoice_id)->get();
            $billing_cycle = $servers[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$servers[0]->billing_cycle] ));
            foreach ($servers as $key => $server) {
                $server->next_due_date = $due_date;
                $server->paid = 'paid';
                $server->save();
            }
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $paid = $invoice->save();
        } else {
            $hostings = $this->hosting->where('detail_order_id', $invoice_id)->get();
            $billing_cycle = $hostings[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$hostings[0]->billing_cycle] ));
            foreach ($hostings as $key => $hosting) {
                $hosting->next_due_date = $due_date;
                $hosting->paid = 'paid';
                $hosting->save();
            }
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $paid = $invoice->save();
        }
        return $paid;
    }


    function rand_string() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $size = strlen( $chars );
        $str = '';
        for( $i = 0; $i < 16; $i++ ) {
            $str .= $chars[ rand( 0, $size - 1 ) ];
        }
        return $str;
    }
    
}
