<?php

namespace App\Repositories\User;

use App\Model\User;
use App\Model\UserMeta;
use App\Model\GroupUser;
use App\Model\DomainProduct;
//API DOMAIN
use App\Services\DomainPA;

class domainProductRepositories
{

    protected $user;
    protected $user_meta;
    protected $group_user;
    protected $domain;
    protected $domain_product;

    public function __construct()
    {
        $this->domain_product = new DomainProduct;
        $this->domain = new DomainPA();
    }

    public function get_domain_product($id)
    {
        return $this->domain_product->where('id', $id)->first();
    }

    public function get_domain_product_by_ext($ext)
    {
        return $this->domain_product->where('type', $ext)->first();
    }
    
}
