<?php

namespace App\Repositories\User;

use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\User;
use App\Model\UserMeta;
use App\Model\GroupUser;
use App\Model\DomainProduct;
use App\Model\LogActivity;
use Illuminate\Support\Facades\Auth;
//API DOMAIN
use App\Services\DomainPA;

class clientRepositories
{

    protected $group_product;
    protected $product;
    protected $user;
    protected $user_meta;
    protected $group_user;
    protected $domain;
    protected $domain_product;
    protected $log;

    public function __construct()
    {
        $this->group_product = new GroupProduct;
        $this->product = new Product;
        $this->domain_product = new DomainProduct;
        $this->domain = new DomainPA();
        $this->log = new LogActivity;
    }

    public function get_group_product_id($name)
    {
        $group_product = $this->group_product->where('name', 'LIKE', "%{$name}%")->first();
        $id = $group_product->id;
        return $id;
    }

    public function get_group_product_at_client()
    {
        $group_products = $this->group_product->with('products')->get();
        return $group_products;
    }

    public function get_detail_group_product($id)
    {
        $group_product = $this->group_product->find($id);
        return $group_product;
    }

    public function get_product_with_group_product($id)
    {
        $products = $this->product->where('group_product_id', $id)->orderBy('stt', 'asc')->with('meta_product', 'pricing', 'vps', 'hostings', 'servers')->get();
        return $products;
    }

    public function get_domain()
    {
        return $domains = $this->domain_product->get();
    }

    public function get_result($data, $exts)
    {
        return $get_result = $this->domain->get_result_domain($data, $exts);
    }

    public function list_log()
    {
        $logs = $this->log->where('user_id', Auth::user()->id)->orderBy('id', 'desc')->paginate(30);
        $data = [];
        foreach ($logs as $key => $log) {
            $log->text_created_at = date('h:i:s d-m-Y', strtotime($log->created_at));
            $log->description = '';
            $data[] = $log;
        }
        return $data;
    }

    public function list_log_with_action($qtt, $action ,$service)
    {
        if (empty($qtt)) {
           $qtt = 30;
        }
        if ( !empty($action) && !empty($service) ) {
          $logs = $this->log->where('user_id', Auth::user()->id)->where('action', $action)->where('service', 'like' , '%' . $service . '%')->orderBy('id', 'desc')->paginate($qtt);
        }
        elseif ( !empty($service) ) {
          $logs = $this->log->where('user_id', Auth::user()->id)->where('service', 'like' , '%' . $service . '%')->orderBy('id', 'desc')->paginate($qtt);
        }
        elseif ( !empty($action) ) {
          $logs = $this->log->where('user_id', Auth::user()->id)->where('action', $action)->orderBy('id', 'desc')->paginate($qtt);
        }
        else {
           $logs = $this->log->where('user_id', Auth::user()->id)->orderBy('id', 'desc')->paginate($qtt);
        }
        $data = [];
        foreach ($logs as $key => $log) {
            $log->text_created_at = date('h:i:s d-m-Y', strtotime($log->created_at));
            $log->description = '';
            $data[] = $log;
        }
        return $data;
    }

}
