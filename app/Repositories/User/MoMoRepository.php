<?php

namespace App\Repositories\User;

use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\User;
use App\Model\UserMeta;
use App\Model\GroupUser;
use App\Model\DomainProduct;
use App\Model\Hosting;
use App\Model\Vps;
use Carbon\Carbon;
use App\Model\ReadNotification;
use App\Model\Notification;
use App\Model\Vietcombank;
use App\Model\HistoryPay;
use App\Model\DetailOrder;
use App\Factories\AdminFactories;
use App\Model\LogActivity;
use App\Model\Momo;
use App\Model\Config;
//API DOMAIN
use App\Services\Domain;
use App\Services\ClassMoMo;
use Mail;
use GuzzleHttp\Client;
use http\Env\Request;

class MoMoRepository
{

    protected $group_product;
    protected $product;
    protected $user;
    protected $user_meta;
    protected $group_user;
    protected $domain;
    protected $domain_product;
    protected $subject;
    protected $user_email;
    protected $readnotification;
    protected $notification;
    protected $vcb;
    protected $da;
    protected $history_pay;
    protected $admin_payment;
    protected $config_vcb;
    protected $detail_order;
    protected $log_activity;
    protected $momo;
    protected $config_momo;

    protected $classMoMo;
    protected $configSystem;

    public function __construct()
    {
        $this->group_product = new GroupProduct;
        $this->product = new Product;
        $this->domain_product = new DomainProduct;
        $this->vps = new Vps;
        $this->hosting = new Hosting;
        $this->user = new User;
        $this->notification = new Notification();
        $this->readnotification = new ReadNotification();
        $this->vcb = new Vietcombank();
        $this->momo = new Momo();
        $this->detail_order = new DetailOrder();
        $this->history_pay = new HistoryPay;
        $this->log_activity = new LogActivity();
        $this->config_vcb = config('vcb');
        $this->config_momo = config('config_momo');
        $this->admin_payment = AdminFactories::paymentRepositories();
        $this->da = AdminFactories::directAdminRepositories();

        $this->configSystem = new Config();
        // $this->classMoMo = new ClassMoMo();
    }

    public function getTrans()
    {
        // dd($this->onesignal());
        $configMoMo = $this->configSystem->where('type' , 'momo')->first();
        if ( !isset($configMoMo) ) {
            $dataMoMo = [
              'type' => 'momo',
              'account' => '0905091805',
              'password' => '161285'
            ];
            $configMoMo = $this->configSystem->create($dataMoMo);
        }
        if ( empty($configMoMo->otp) ) {
            $otp = $this->getOtp();
            return true;
        }
        if ( !$configMoMo->confirm ) {
            $confirm = $this->confirmOtp();
            if ( !$confirm ) {
                return false;
            }
        }
        // Tiến hành login.
        $classMomo = new ClassMoMo( $configMoMo->account , $configMoMo->password, $configMoMo->otp,
                      $configMoMo->rkey, $configMoMo->setupkey, $configMoMo->imei, '', $configMoMo->onesignal);
        //lấy lịch sử.
        $trans = $classMomo->history(1);
        $data = json_decode($trans,true);
        if ( !empty($data['momoMsg']['tranList']) ) {
            return $data['momoMsg']['tranList'];
        } else {
            return false;
        }
    }

    private function getOtp()
    {
        $configMoMo = $this->configSystem->where('type' , 'momo')->first();
        if ( !isset($configMoMo) ) {
            $dataMoMo = [
              'type' => 'momo',
              'account' => '0905091805',
              'password' => '161285'
            ];
            $configMoMo = $this->configSystem->create($dataMoMo);
        }
        // dd('da den get otp momo');
        $rkey = $this->rkey();
        $onesignal = $this->onesignal();
        $get_imei = $this->get_imei();
        $phone = $configMoMo['account']; //sdt momo
        // dd($rkey , $onesignal, $get_imei, $phone);
        $otp = $this->sendOTP($phone, $rkey, $onesignal, $get_imei); // lay otp
        if ($otp) {
            $configMoMo->imei = $get_imei;
            $configMoMo->rkey = $rkey;
            $configMoMo->onesignal = $onesignal;
            $configMoMo->save();
            // dd($get_imei, $rkey, $onesignal);
            return true;
        }
        return false;
    }

    function sendOTP($phone,$rkey,$onesignal,$imei) {
        $config = config('momoConfig');
        // dd($config);
      	$type = 'SEND_OTP_MSG';
      	$data_body = [
      		'user' => $phone,
      		'msgType' => $type,
      		'cmdId' => $this->get_microtime() . '000000',
      		'lang' => $config['lang'],
      		'channel' => $config['channel'],
      		'time' => $this->get_microtime(),
      		'appVer' => $config['appVer'],
      		'appCode' => $config['appCode'],
      		'deviceOS' => $config['deviceOS'],
      		'result' => true,
      		'errorCode' => 0,
      		'errorDesc' => '',
      		'extra' => [
      			'action' => 'SEND',
      			'rkey' => $rkey,
      			'AAID' => '',
      			'IDFA' => '',
      			'TOKEN' => '',
      			'ONESIGNAL_TOKEN' => $onesignal,
      			'SIMULATOR' => 'false',
      			'isVoice' => 'true',
      			'REQUIRE_HASH_STRING_OTP' => false,
      		],
      		'momoMsg' => [
      			'_class' => 'mservice.backend.entity.msg.RegDeviceMsg',
      			'number' => $phone,
      			'imei' => $imei,
      			'cname' => 'Vietnam',
      			'ccode' => '084',
      			'device' => 'iPhone',
      			'firmware' => '13.5.1',
      			'hardware' => 'iPhone',
      			'manufacture' => 'Apple',
      			'csp' => 'Mobifone',
      			'icc' => '',
      			'mcc' => '452',
      			'mnc' => '04',
      			'device_os' => 'IOS',
      		],
      	];
      	$curl = curl_init();
      	curl_setopt_array($curl, array(
      		CURLOPT_URL => "https://owa.momo.vn/public",
      		CURLOPT_RETURNTRANSFER => true,
      		CURLOPT_ENCODING => "",
      		CURLOPT_MAXREDIRS => 10,
      		CURLOPT_TIMEOUT => 30,
      		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      		CURLOPT_CUSTOMREQUEST => "POST",
      		CURLOPT_POSTFIELDS => json_encode($data_body),
      		CURLOPT_HTTPHEADER => array(
      			'User-Agent' => "MoMoApp-Release/%s CFNetwork/978.0.7 Darwin/18.6.0",
      			'Msgtype' => "USER_LOGIN_MSG",
      			'Accept' => 'application/json',
      			'Content-Type' => 'application/json',
      			'Userhash' => md5($phone),
      		),
      	));
      	$response = curl_exec($curl);
      	if (!$response) {
      		return false;
      	}
      	return $response;
    }

    public function confirmOtp()
    {
        $configMoMo = $this->configSystem->where('type' , 'momo')->first();
        $phone = $configMoMo['account'];
        $otp = $configMoMo['otp'];
        $rkey = $configMoMo['rkey'];
        $onesignal = $configMoMo['onesignal'];
        $get_imei = $configMoMo['imei'];
        $comfirm = json_decode($this->comfirm_otp($phone,$otp,$rkey,$onesignal,$get_imei));
        if ( $comfirm ) {
          // dd($comfirm->extra);
          if ( !empty($comfirm->extra->setupKey) ) {
            $configMoMo->setupkey = $comfirm->extra->setupKey;
            $configMoMo->confirm = true;
            $configMoMo->save();
            return true;
          }
          return false;
        }
        return false;
    }

    function comfirm_otp($phone,$otp,$rkey,$onesignalToken,$imei) {
      $config = config('momoConfig');
    	$type = 'REG_DEVICE_MSG';
    	$data_body = [
    		'user' => $phone,
    		'msgType' => $type,
    		'cmdId' => $this->get_microtime() . '000000',
    		'lang' => $config['lang'],
    		'channel' => $config['channel'],
    		'time' => $this->get_microtime(),
    		'appVer' => $config['appVer'],
    		'appCode' => $config['appCode'],
    		'deviceOS' => $config['deviceOS'],
    		'result' => true,
    		'errorCode' => 0,
    		'errorDesc' => '',
    		'extra' => [
    			'ohash' => hash('sha256', $phone . $rkey . $otp),
    			'AAID' => '',
    			'IDFA' => '',
    			'TOKEN' => '',
    			'ONESIGNAL_TOKEN' => $onesignalToken,
    			'SIMULATOR' => 'false',
    		],
    		'momoMsg' => [
    			'_class' => 'mservice.backend.entity.msg.RegDeviceMsg',
    			'number' => $phone,
    			'imei' => $imei,
    			'cname' => 'Vietnam',
    			'ccode' => '084',
    			'device' => 'iPhone',
    			'firmware' => '13.5.1',
    			'hardware' => 'iPhone',
    			'manufacture' => 'Apple',
    			'csp' => 'Mobifone',
    			'icc' => '',
    			'mcc' => '452',
    			'mnc' => '04',
    			'device_os' => 'IOS',
    		],
    	];
    	$curl = curl_init();
    	curl_setopt_array($curl, array(
    		CURLOPT_URL => "https://owa.momo.vn/public",
    		CURLOPT_RETURNTRANSFER => true,
    		CURLOPT_ENCODING => "",
    		CURLOPT_MAXREDIRS => 10,
    		CURLOPT_TIMEOUT => 30,
    		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    		CURLOPT_CUSTOMREQUEST => "POST",
    		CURLOPT_POSTFIELDS => json_encode($data_body),
    		CURLOPT_HTTPHEADER => array(
    			'User-Agent' => "MoMoApp-Release/%s CFNetwork/978.0.7 Darwin/18.6.0",
    			'Msgtype' => "USER_LOGIN_MSG",
    			'Accept' => 'application/json',
    			'Content-Type' => 'application/json',
    			'Userhash' => md5($phone),
    		),
    	));
    	$response = curl_exec($curl);
    	if (!$response) {
    		return false;
    	}
    	return $response;
    }

    /*
    * Thư viện
    */
    function rkey($length = 20) {
    	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    	$size = strlen($chars);
    	$str = '';
    	for ($i = 0; $i < $length; $i++) {
    		$str .= $chars[rand(0, $size - 1)];
    	}
    	return $str;
    }

    function get_microtime(){
    	return floor(microtime(true) * 1000);
    }
    function get_imei() {
    	$time = md5($this->get_microtime());
    	$text = substr($time, 0, 8) . '-';
    	$text .= substr($time, 8, 4) . '-';
    	$text .= substr($time, 12, 4) . '-';
    	$text .= substr($time, 16, 4) . '-';
    	$text .= substr($time, 17, 12);
    	$text = strtoupper($text);
    	return $text;
    }
    function onesignal() {
    	$time = md5($this->get_microtime() . '000000');
    	$text = substr($time, 0, 8) . '-';
    	$text .= substr($time, 8, 4) . '-';
    	$text .= substr($time, 12, 4) . '-';
    	$text .= substr($time, 16, 4) . '-';
    	$text .= substr($time, 17, 12);
    	return $text;
    }

}
