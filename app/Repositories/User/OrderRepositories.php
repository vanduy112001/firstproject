<?php

namespace App\Repositories\User;

use App\Model\User;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\Server;
use App\Model\Hosting;
use App\Model\Email;
use App\Model\Product;
use App\Model\HistoryPay;
use App\Model\OrderAddonVps;
use App\Model\Domain;
use App\Model\GroupProduct;
use App\Model\LogActivity;
use Mail;
use Auth;
use App\Factories\UserFactories;
use App\Factories\AdminFactories;
use App\Http\Controllers\Admin\DirectAdminController;
use App\Model\DomainExpired;

class OrderRepositories
{

    protected $user;
    protected $order;
    protected $detail_order;
    protected $vps;
    protected $server;
    protected $hosting;
    protected $email;
    protected $user_email;
    protected $subject;
    protected $product;
    protected $history_pay;
    protected $addon_vps;
    protected $domain;
    protected $group_product;
    protected $domain_expired;
    protected $log_activity;
    // API
    protected $da;
    protected $dashboard;


    public function __construct()
    {
        $this->user = new User;
        $this->order = new Order;
        $this->detail_order = new DetailOrder;
        $this->vps = new Vps;
        $this->server = new Server;
        $this->hosting = new Hosting;
        $this->email = new Email;
        $this->product = new Product;
        $this->history_pay = new HistoryPay;
        $this->addon_vps = new OrderAddonVps;
        $this->domain = new Domain;
        $this->log_activity = new LogActivity();
        $this->domain_expired = new DomainExpired;
        $this->group_product = new GroupProduct;
        $this->dashboard = AdminFactories::dashBoardRepositories();
        $this->da = AdminFactories::directAdminRepositories();
    }

    public function list_order()
    {
        return $this->order->where('user_id', Auth::user()->id)->with('detail_orders')->orderBy('id', 'desc')->paginate(20);

    }
    public function cancel_order($id)
    {
        $get_order = $this->order->find($id);
        foreach ($get_order->detail_orders as $key => $detail_order) {
           if ($get_order->order == 'order') {
               foreach ($detail_order->history_pay as $key => $history_pay) {
                  $check =  $history_pay->delete();
               }
               if ($detail_order->type == 'VPS' || $detail_order->type == 'NAT-VPS') {
                  foreach ($detail_order->vps as $key => $vps) {
                      if ($detail_order->vps->count() > 0) {
                          if ( !empty($vps->vps_config) ) {
                            $vps_config = $vps->vps_config;
                            $vps_config->delete();
                          }
                          $vps->delete();
                      }
                  }
               }
               elseif ( $detail_order->type == 'Hosting' || $detail_order->type == 'Hosting-Singapore' ) {
                 if ($detail_order->hostings->count() > 0) {
                   foreach ($detail_order->hostings as $key => $hosting) {
                      $hosting->delete();
                   }
                 }
               }
               elseif ( $detail_order->type == 'Domain' ) {
                 if ($detail_order->domains->count() > 0) {
                   foreach ($detail_order->domains as $key => $domain) {
                      $domain->delete();
                   }
                 }
               }
               elseif ( $detail_order->type == 'addon_vps' ) {
                 if ($detail_order->order_addon_vps->count() > 0) {
                   foreach ($detail_order->order_addon_vps as $key => $order_addon_vps) {
                      $order_addon_vps->delete();
                   }
                 }
               }
               elseif ( $detail_order->type == 'change_ip' ) {
                 if ($detail_order->order_change_vps->count() > 0) {
                   foreach ($detail_order->order_change_vps as $key => $order_change_vps) {
                      $order_change_vps->delete();
                   }
                 }
               }
               elseif ( $detail_order->type == 'upgrade_hosting' ) {
                 if (!empty($detail_order->order_upgrade_hosting)) {
                   $order_upgrade_hosting = $detail_order->order_upgrade_hosting;
                   $order_upgrade_hosting->delete();
                 }
               }
           }
           else {
               if ( $detail_order->type == 'VPS' || $detail_order->type == 'NAT-VPS' ) {
                 if ($detail_order->expired_vps->count() > 0) {
                    foreach ($detail_order->expired_vps as $key => $expired_vps) {
                       $expired_vps->delete();
                    }
                 }
               }
               elseif ( $detail_order->type == 'hosting' || $detail_order->type == 'Hosting-Singapore' ) {
                 if ($detail_order->expired_hostings->count() > 0) {
                   foreach ($detail_order->expired_hostings as $key => $expired_hosting) {
                      $expired_hosting->delete();
                   }
                 }
               }
               elseif ( $detail_order->type == 'Domain_exp' ) {
                 if ($detail_order->domain_expired->count() > 0) {
                   foreach ($detail_order->domain_expired as $key => $domain_expired) {
                      $domain_expired->delete();
                   }
                 }
               }
           }
           $detail_order->delete();
        }
        // Tạo log
        $data_log = [
          'user_id' => Auth::user()->id,
          'action' => 'xóa',
          'model' => 'User/Delete_Order',
          'description' => ' đơn hàng (Order ID: '. $get_order->id .')',
        ];
        $this->log_activity->create($data_log);

        $delete_order  = $get_order->delete();
        return $delete_order;
    }

    public function detail_invoice($id)
    {
       $billings = config('billing');
       $detail_order = $this->detail_order->find($id);
       if ($detail_order->status == 'paid') {
          $detail_order->paid_date = date('d-m-Y', strtotime($detail_order->paid_date));
       }
       else {
          $detail_order->paid_date = '';
       }
       $detail_order->date_create = date('H:i:s d-m-Y', strtotime($detail_order->created_at));
       $detail_order->due_date = date('d-m-Y', strtotime($detail_order->due_date));
       $detail_order->username = $detail_order->user->name;
       if ($detail_order->order->type == 'expired') {
           $detail_order->type_order = 'expired';
           if ($detail_order->type == 'VPS' || $detail_order->type == 'VPS US' || $detail_order->type == ' US') {
              $order_expireds = $detail_order->order_expireds;
              $services = [];
              foreach ($order_expireds as $key => $order_expired) {
                $services[$key]['sevice_id'] = $order_expired->vps->id;
                $services[$key]['sevice_name'] = $order_expired->vps->ip;
                $product = $order_expired->vps->product;
                $vps = $order_expired->vps;
                if(!empty($product->meta_product)) {
                  $cpu = $product->meta_product->cpu;
                  $ram = $product->meta_product->memory;
                  $disk = $product->meta_product->disk;
                } else {
                  $cpu = 1;
                  $ram = 1;
                  $disk = 20;
                }
                // Addon
                if (!empty($vps->vps_config)) {
                    $vps_config = $vps->vps_config;
                    if (!empty($vps_config)) {
                        $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                        $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                        $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                    }
                }
                $services[$key]['config'] = $cpu . ' CPU - ' . $ram . ' GB RAM - ' . $disk . ' GB DISK';
                $services[$key]['billing_cycle'] = !empty($order_expired->billing_cycle) ? $billings[$order_expired->billing_cycle] : '1 Tháng';
                $services[$key]['amount'] = !empty($vps->price_override) ? number_format($vps->price_override,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
              }
              $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'hosting') {
               $order_expireds = $detail_order->order_expireds;
               $services = [];
               foreach ($order_expireds as $key => $order_expired) {
                 $services[$key]['sevice_id'] = $order_expired->hosting->id;
                 $services[$key]['sevice_name'] = $order_expired->hosting->domain;
                 $product = $order_expired->hosting->product;
                 $hosting = $order_expired->hosting;
                 $services[$key]['config'] = $product->name;
                 $services[$key]['billing_cycle'] = !empty($order_expired->billing_cycle) ? $billings[$order_expired->billing_cycle] : '1 Tháng';
                 $services[$key]['amount'] = !empty($hosting->price_override) ? number_format($hosting->price_override,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
               }
               $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'Domain_exp') {
               $billing_domain_exp = [
                   'monthly_exp' => '1 Tháng',
                   'twomonthly_exp' => '2 Tháng',
                   'quarterly_exp' => '3 Tháng',
                   'semi_annually_exp' => '6 Tháng',
                   'annually_exp' => '1 Năm',
                   'biennially_exp' => '2 Năm',
                   'triennially_exp' => '3 Năm',
               ];
               $detail_order->type = 'Domain';
               $order_expireds = $detail_order->domain_expireds;
               $services = [];
               foreach ($order_expireds as $key => $order_expired) {
                 $services[$key]['sevice_id'] = $order_expired->domain->id;
                 $services[$key]['sevice_name'] = $order_expired->domain->domain;
                 $product = $order_expired->domain->domain_product;
                 $domain = $order_expired->domain;
                 $services[$key]['config'] = 'Domain .' . $product->type;
                 $services[$key]['billing_cycle'] = !empty($order_expired->billing_cycle) ? $billing_domain_exp[$order_expired->billing_cycle] : '1 Tháng';
                 $services[$key]['amount'] = number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
               }
               $detail_order->services = $services;
           }
       }
       else {
         $detail_order->type_order = 'order';
           if ($detail_order->type == 'VPS' || $detail_order->type == 'NAT-VPS' || $detail_order->type == 'VPS-US') {
              $list_vps = $detail_order->vps;
              $services = [];
              foreach ($list_vps as $key => $vps) {
                $services[$key]['sevice_id'] = $vps->id;
                $services[$key]['sevice_name'] = $vps->ip;
                $product = $vps->product;
                if(!empty($product->meta_product)) {
                  $cpu = $product->meta_product->cpu;
                  $ram = $product->meta_product->memory;
                  $disk = $product->meta_product->disk;
                } else {
                  $cpu = 1;
                  $ram = 1;
                  $disk = 20;
                }
                // Addon
                if (!empty($vps->vps_config)) {
                    $vps_config = $vps->vps_config;
                    if (!empty($vps_config)) {
                        $cpu += (int) !empty($vps_config->cpu) ? $vps_config->cpu : 0;
                        $ram += (int) !empty($vps_config->ram) ? $vps_config->ram : 0;
                        $disk += (int) !empty($vps_config->disk) ? $vps_config->disk : 0;
                    }
                }
                $services[$key]['config'] = $cpu . ' CPU - ' . $ram . ' GB RAM - ' . $disk . ' GB DISK';
                $services[$key]['billing_cycle'] = !empty($vps->billing_cycle) ? $billings[$vps->billing_cycle] : '1 Tháng';
                $services[$key]['state'] = !empty($vps->state) ? $vps->state : '';
                $services[$key]['amount'] = !empty($vps->price_override) ? number_format($vps->price_override,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
              }
              $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'Hosting' || $detail_order->type == 'Hosting-Singapore') {
               $list_hosting = $detail_order->hostings;
               $services = [];
               foreach ($list_hosting as $key => $hosting) {
                 $services[$key]['sevice_id'] = $hosting->id;
                 $services[$key]['sevice_name'] = $hosting->domain;
                 $product = $hosting->product;
                 if (!empty($product)) {
                   $services[$key]['config'] = $product->name;
                 } else {
                   $services[$key]['config'] = '<span class="text-danger">Đã xóa</span>';
                 }
                 $services[$key]['billing_cycle'] = !empty($hosting->billing_cycle) ? $billings[$hosting->billing_cycle] : '1 Tháng';
                 $services[$key]['amount'] = !empty($hosting->price_override) ? number_format($hosting->price_override,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
               }
               $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'Domain') {
               $list_domain = $detail_order->domains;
               $services = [];
               foreach ($list_domain as $key => $domain) {
                 $services[$key]['sevice_id'] = $domain->id;
                 $services[$key]['sevice_name'] = $domain->domain;
                 $product = $domain->product;
                 if (!empty($product)) {
                   $services[$key]['config'] = $product->name;
                 } else {
                   $services[$key]['config'] = '<span class="text-danger">Đã xóa</span>';
                 }
                 $services[$key]['billing_cycle'] = !empty($domain->billing_cycle) ? $billings[$domain->billing_cycle] : '1 Tháng';
                 $services[$key]['amount'] = !empty($domain->price_override) ? number_format($domain->price_override,0,",",".") . ' VNĐ' : number_format($detail_order->sub_total,0,",",".") . ' VNĐ';
               }
               $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'addon_vps') {
              $order_addon_vps = $detail_order->order_addon_vps;
              $services = [];
              $billing_cycle = 'monthly';
              $detail_order->type_order = 'addon_vps';
              foreach ($order_addon_vps as $key => $addon_vps) {
                  $services[$key]['sevice_id'] = $addon_vps->vps->id;
                  $services[$key]['sevice_name'] = $addon_vps->vps->ip;
                  $services[$key]['cpu'] = $addon_vps->cpu;
                  $services[$key]['ram'] = $addon_vps->ram;
                  $services[$key]['disk'] = $addon_vps->disk;
                  $services[$key]['time'] = !empty($addon_vps->month) ? $addon_vps->month . ' Tháng ' : ' ';
                  $services[$key]['time'] .= !empty($addon_vps->day) ? $addon_vps->day .' Ngày' : '';
                  $add_on_products = $this->get_addon_product_private($detail_order->user_id);
                  $pricing_addon = 0;
                  foreach ($add_on_products as $key1 => $add_on_product) {
                      if (!empty($add_on_product->meta_product->type_addon)) {
                          if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                            $pricing_addon += $addon_vps->month * $addon_vps->cpu * $add_on_product->pricing[$billing_cycle] + (int) round($addon_vps->day  * $addon_vps->cpu * $add_on_product->pricing[$billing_cycle] / 30, -3);
                          }
                      }
                  }
                  foreach ($add_on_products as $key2 => $add_on_product) {
                      if (!empty($add_on_product->meta_product->type_addon)) {
                          if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                            $pricing_addon += $addon_vps->month * $addon_vps->ram * $add_on_product->pricing[$billing_cycle] + (int) round($addon_vps->day  * $addon_vps->ram * $add_on_product->pricing[$billing_cycle] / 30, -3);
                          }
                      }
                  }
                  foreach ($add_on_products as $key3 => $add_on_product) {
                      if (!empty($add_on_product->meta_product->type_addon)) {
                          if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                            $pricing_addon += $addon_vps->month * $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10 + (int) round($addon_vps->day  * $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 30, -3)/10;
                          }
                      }
                  }
                  $services[$key]['amount'] = number_format( $pricing_addon,0,",",".") . ' VNĐ';
              }
              $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'change_ip') {
              $order_change_vps = $detail_order->order_change_vps;
              $services = [];
              $detail_order->type_order = 'change_ip';
              foreach ($order_change_vps as $key => $change_vps) {
                  $services[$key]['sevice_id'] = $change_vps->id;
                  $services[$key]['changed_ip'] = !empty($change_vps->changed_ip) ? $change_vps->changed_ip : '';
                  $services[$key]['sevice_name'] = $change_vps->vps->ip;
                  $services[$key]['amount'] = number_format( $detail_order->sub_total,0,",",".") . ' VNĐ';
              }
              $detail_order->services = $services;
           }
           elseif ($detail_order->type == 'upgrade_hosting') {
              $order_upgrade_hosting = $detail_order->order_upgrade_hosting;
              $services = [];
              $detail_order->type_order = 'upgrade_hosting';
              $services['sevice_id'] = $order_upgrade_hosting->hosting->id;
              $services['sevice_name'] = $order_upgrade_hosting->hosting->domain;
              $product = $order_upgrade_hosting->product;
              if ($product->group_product->private) {
                $services['upgrade'] = '<a href="/admin/product_privates/edit/' . $product->id . '" target="_blank">' . $product->name . '</a>';
              } else {
                $services['upgrade'] = '<a href="/admin/products/edit/' . $product->id . '" target="_blank">' . $product->name . '</a>';
              }
              $services['amount'] = number_format( $detail_order->sub_total,0,",",".") . ' VNĐ';

              $detail_order->services = $services;
           }
       }
       return $detail_order;
    }

    public function get_addon_product_private($user_id)
    {
        $products = [];
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_user = $user->group_user;
            $group_products = $group_user->group_products;
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->get();
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        }
        return $products;
    }

}
