<?php

namespace App\Repositories\User;

use App\Model\Notification;
use App\Model\User;
use App\Model\ReadNotification;
use Illuminate\Support\Facades\Auth;

class notificationRepositories
{

    protected $notification;
    protected $readnotification;
    protected $user;

    public function __construct()
    {
        $this->notification = new Notification;
        $this->user = new User;
        $this->readnotification = new ReadNotification;
    }

    public function all($params = [])
    {
      $readnotifications = $this->readnotification->where('user_id',  Auth::user()->id)->orderBy('id', 'desc')->with('notification')->paginate(20);
      return $readnotifications;
    }

    public function get_notification($id) {
        $read_at = $this->readnotification->where('notification_id', $id)->where('user_id',  Auth::user()->id)->first();
        if (isset($read_at)) {
            $read_at->read_at = date('Y-m-d H:i:s');
            $read_at->save();
            return $this->notification->find($id);
        } else {
            return false;
        }
    }
    public function get_notifications() {
        return $this->readnotification->where('user_id',  Auth::user()->id)->with('notification')->take(5)->orderBy('updated_at', 'desc')->get();
    }
    public function read_notification($id) {
        $read_at =  date('Y-m-d H:i:s', time());
        try {
          $result = [
            'status' => false,
            'message' => 'Có lỗi xảy ra, vui lòng thử lại.',
          ];
          $update_read = $this->readnotification->where('user_id', Auth::user()->id)->where('notification_id', $id)->first();
          if ( isset( $update_read ) ) {
            $update_read->read_at = $read_at;
            $update_read->save();
          }
          if($update_read) {
            $result = [
              'status' => true,
              'message' => 'Cập nhập thành công.',
            ];
          }
          return $result;
        }
        catch (\Exception $exception) {
          return [
            'status' => false,
            'message' => $exception->getMessage(),
          ];
        }
    }

    public function ajax_read($data)
    {
        try {
            $read_at =  date('Y-m-d H:i:s', time());
            foreach ($data['list_id'] as $key => $id) {
                $update_read = $this->readnotification->where('user_id', Auth::user()->id)->where('notification_id', $id)->first();
                // dd($id);
                if ( isset( $update_read ) ) {
                    $update_read->read_at = $read_at;
                    $update_read->save();
                }
            }
            return true;
        } catch (Exception $e) {
            report($e);
            return false;
        }
    }

    public function get_unread_notifications_number() {
      $notifications_read = $this->readnotification->where('user_id', Auth::user()->id )->where('read_at', null )->count();
      return $notifications_read;
    }

    public function get_total_notificationRepositories($user_id)
    {
        $notifications_read = $this->readnotification->where('user_id', $user_id )->where('read_at', null )->orderBy('id', 'desc')->with('notification')->get();
        return $notifications_read;
    }

}
