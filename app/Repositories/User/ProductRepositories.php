<?php

namespace App\Repositories\User;

use App\Model\User;
use App\Model\UserMeta;
use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\EventPromotion;
use App\Model\State;
use App\Model\ProxyState;

use Illuminate\Support\Facades\Auth;

class ProductRepositories {

    protected $user;
    protected $user_meta;
    protected $email;
    protected $group_product;
    protected $product;
    protected $event;
    protected $state;
    protected $state_proxy;

    public function __construct()
    {
        $this->user = new User;
        $this->user_meta = new UserMeta;
        $this->group_product = new GroupProduct;
        $this->product = new Product;
        $this->event = new EventPromotion;
        $this->state = new State();
        $this->state_proxy = new ProxyState;
    }
    
    public function check_group_product($id)
    {
        $group_product = $this->group_product->find($id);
        $user = Auth::user();
        $check = false;
        if ( !empty( $group_product ) && $group_product->group_user_id == $user->group_user_id ) {
            return true;
        }
        return false;
    }

    public function get_detail_group_product($id)
    {
        $group_product = $this->group_product->find($id);
        return $group_product;
    }

    public function get_product_with_group_product($id)
    {
        $products = $this->product->where('group_product_id', $id)->where('hidden', false)->orderBy('stt', 'asc')->with('meta_product', 'pricing', 'vps', 'hostings', 'servers')->get();
        return $products;
    }

    public function check_product_with_user($product_id)
    {
        $user = $this->user->find(Auth::user()->id);
        if ($user->group_user_id == 0) {
            $group_products = $this->group_product->where('private', 0)->get();
            foreach ($group_products as $key => $group_product) {
                foreach ($group_product->products as $key => $product) {
                    if ($product_id  == $product->id) {
                        return false;
                    }
                }
            }
        } else {
            $group_user = $user->group_user;
            foreach ($group_user->group_products as $key => $group_product) {
                foreach ($group_product->products as $key => $product) {
                    if ($product_id  == $product->id) {
                        return false;
                    }
                }
            }
        }
        return true;

    }

    public function get_event_promotion($id, $billing_cycle)
    {
        $event = $this->event->where('product_id', $id)->where('billing_cycle', $billing_cycle)->first();
        return $event;
    }

    public function list_state()
    {
        return $this->state->where('hidden', false)->get();
    }

    public function list_state_by_location($product_special)
    {
        if ( !empty($product_special) ) {
            return $this->state->where('hidden', false)->where('location', 'cloudzone')->get();
        } else {
            return $this->state->where('hidden', false)->where('location', 'us')->get();
        }
    }

    public function list_state_proxy()
    {
        return $this->state_proxy->where('hidden', false)->get();
    }

}
