<?php

namespace App\Repositories\User;

use App\Model\Ticket;
use App\Model\TicketMessage;
use App\Model\LogActivity;
use Illuminate\Support\Facades\Auth;
use Mail;
class TicketRepository
{

    protected $model;
    protected $modelMessage;
    protected $log_activity;

    public function __construct()
    {
        $this->model = new Ticket();
        $this->modelMessage = new TicketMessage();
        $this->log_activity = new LogActivity();
    }

    public function getList($userId, $params)
    {
        $limit = !empty($params['limit']) ? $params['limit'] : 20;

        $query = $this->model->where('user_id', $userId)->orderBy('id', 'desc');

        $result = $query->paginate($limit);

        return $result;
    }

    public function getDetail($id)
    {
        $result = $this->model->find($id);

        return $result;
    }

    public function save($id, $data)
    {
        $find = [];
        if (!empty($id)) {
            $find = $this->model->find($id);
        }
        if (empty($find)) {
            $data['id'] = $id;

            $save = $this->model->create($data);

            try {
                $data['user_name'] = Auth::user()->name;
                $data['user_email'] = Auth::user()->email;
                $mail = Mail::send('users.mails.send_mail_ticket', $data, function($message){
                    $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                    $message->to('support@cloudzone.vn')->subject('Khách hàng yêu cầu hổ trợ');
                });
            } catch (\Exception $e) {
                return $save;
            }

//            if ($save) {
//                $this->clearCacheDetail($save->id);
//                if (!empty($save->store_id)) {
//                    $this->clearCachePaginate($save->store_id);
//                }
//            }
        } else {
            $save = $find->update($data);
//            if ($save) {
//                $this->clearCacheDetail($find->id);
//                if (!empty($find->store_id)) {
//                    $this->clearCachePaginate($find->store_id);
//                }
//            }
        }

        return $save;
    }

    public function saveMessage($data)
    {
        $save = $this->modelMessage->create($data);

        return $save;
    }
}
