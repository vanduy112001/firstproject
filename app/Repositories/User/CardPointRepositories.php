<?php

namespace App\Repositories\User;

use App\Model\User;
use App\Model\UserMeta;
use App\Model\Verify;
use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\Email;
use Illuminate\Support\Facades\Auth;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\VpsConfig;
use App\Model\Server;
use App\Model\Hosting;
use App\Model\OrderVetify;
use App\Model\Credit;
use App\Model\Customer;
use App\Model\GroupUser;
use App\Model\ReadNotification;
use App\Model\Notification;
use App\Model\EventPromotion;
use App\Model\LogActivity;
use Mail;
use App\Jobs\SendMailOrderDomain;
use App\Model\MetaGroupProduct;
use App\Model\TotalPrice;
use App\Model\HistoryPay;
use App\Factories\AdminFactories;
use App\Http\Controllers\Admin\DirectAdminController;
use App\Model\DomainProduct;
use App\Model\Domain;
use App\Model\DomainExpired;
use Mockery\CountValidator\AtMost;
use Carbon\Carbon;

class CardPointRepositories {

    protected $user;
    protected $user_meta;
    protected $group_user;
    protected $verify;
    protected $email;
    protected $group_product;
    protected $product;
    protected $order;
    protected $detail_order;
    protected $vps;
    protected $vps_config;
    protected $server;
    protected $hosting;
    protected $user_email;
    protected $subject;
    protected $order_vetify;
    protected $credit;
    protected $meta_group_product;
    protected $total_price;
    protected $history_pay;
    protected $customer;
    protected $user_send_email;
    protected $domain_product;
    protected $domain;
    protected $domain_exp;
    protected $readnotification;
    protected $notification;
    protected $event_promotion;
    protected $log_activity;
    // API
    protected $da;
    protected $dashboard;
    protected $domain_pa;
    protected $da_si;

    public function __construct()
    {
        $this->user = new User;
        $this->email = new Email;
        $this->user_meta = new UserMeta;
        $this->group_user = new GroupUser;
        $this->verify = new Verify;
        $this->group_product = new GroupProduct;
        $this->product = new Product;
        $this->order = new Order;
        $this->detail_order = new DetailOrder;
        $this->vps = new Vps;
        $this->vps_config = new VpsConfig;
        $this->server = new Server;
        $this->hosting = new Hosting;
        $this->order_vetify = new OrderVetify;
        $this->credit = new Credit;
        $this->meta_group_product = new MetaGroupProduct;
        $this->total_price = new TotalPrice;
        $this->history_pay = new HistoryPay;
        $this->customer = new Customer;
        $this->domain_product = new DomainProduct;
        $this->domain = new Domain;
        $this->domain_exp = new DomainExpired;
        $this->notification = new Notification();
        $this->readnotification = new ReadNotification();
        $this->event_promotion = new EventPromotion();
        $this->log_activity = new LogActivity();
        // API
        $this->dashboard = AdminFactories::dashBoardRepositories();
        $this->da = AdminFactories::directAdminRepositories();
        $this->domain_pa = AdminFactories::domainRepositories();
        $this->da_si = AdminFactories::directAdminSingaporeRepositories();
        // $this->domain_api = new Domain();
    }


    // Thanh toán khi order
    public function payment_order_with_point_cloudzone($invoice_id)
    {
        $invoice = $this->detail_order->find($invoice_id);
        $total = $invoice->sub_total * $invoice->quantity;
        $order = $this->order->find($invoice->order->id);
        $order->status = 'Active';
        $order->save();
        $user_meta = $this->user_meta->where('user_id', Auth::user()->id)->first();
        if (!isset($user_meta)) {
           return false;
        }
        // point
        $user_meta->point = $user_meta->point - ($total / 1000);
        $user_meta->point_used += ($total / 1000);
        $user_meta->save();
        // HistoryPay
        $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
        $history_pay->status = 'Active';
        $history_pay->method_gd_invoice = 'cloudzone_point';
        $history_pay->save();
        // invoice
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        if ($invoice->type == 'VPS') {
            $list_vps = $this->vps->where('detail_order_id', $invoice_id)->get();
            $billing_cycle = $list_vps[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$list_vps[0]->billing_cycle] ));


            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            // ghi vào tổng thu
            if (!empty($this->total_price->first())) {
                $total_price = $this->total_price->first();
                $total_price->vps += $total;
                $total_price->save();
            } else {
                $data_total_price = [
                    'vps' => $total,
                    'hosting' => '0',
                    'server' => '0',
                ];
                $this->total_price->create($data_total_price);
            }
            $reques_vcenter = $this->dashboard->createVPSWithPoint($list_vps, $due_date); //request đên Vcenter
            // $reques_vcenter = true;
            if ($reques_vcenter) {
                $order->status = 'Finish';
                $order->save();
                // Tạo log
                $data_log = [
                   'user_id' => Auth::user()->id,
                   'action' => 'thanh toán',
                   'model' => 'User/HistoryPay_CloudzonePoint',
                   'description' => 'hóa đơn VPS <a  target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> bằng Cloudzone Point thành công',
                   'description_user' => 'hóa đơn VPS <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> bằng Cloudzone Point thành công',
                ];
                $this->log_activity->create($data_log);

                try {
                  $this->send_mail_finish_cloudzone_point('vps',$order, $invoice ,$list_vps);
                } catch (\Exception $e) {
                  return $paid;
                }

            }

        }
        elseif ($invoice->type == 'NAT-VPS') {
            $list_vps = $this->vps->where('detail_order_id', $invoice_id)->get();
            $billing_cycle = $list_vps[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$list_vps[0]->billing_cycle] ));


            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            // ghi vào tổng thu
            if (!empty($this->total_price->first())) {
                $total_price = $this->total_price->first();
                $total_price->vps += $total;
                $total_price->save();
            } else {
                $data_total_price = [
                    'vps' => $total,
                    'hosting' => '0',
                    'server' => '0',
                ];
                $this->total_price->create($data_total_price);
            }
            $reques_vcenter = $this->dashboard->createNatVPSWithPoint($list_vps, $due_date); //request đên Vcenter
            // $reques_vcenter = true;
            if ($reques_vcenter) {
                $order->status = 'Finish';
                $order->save();
                // Tạo log
                $data_log = [
                   'user_id' => Auth::user()->id,
                   'action' => 'thanh toán',
                   'model' => 'User/HistoryPay_CloudzonePoint',
                   'description' => 'hóa đơn NAT VPS <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> bằng Cloudzone Point thành công',
                   'description_user' => 'hóa đơn NAT VPS <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> bằng Cloudzone Point thành công',
                ];
                $this->log_activity->create($data_log);
                try {
                  $this->send_mail_finish_cloudzone_point('nat_vps',$order, $invoice ,$list_vps);
                } catch (\Exception $e) {
                  return $paid;
                }

            }
        }
        elseif ($invoice->type == 'Server') {
            $servers = $this->server->where('detail_order_id', $invoice_id)->get();
            $billing_cycle = $servers[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$servers[0]->billing_cycle] ));
            foreach ($servers as $key => $server) {
                $server->next_due_date = $due_date;
                $server->paid = 'paid';
                $server->save();
            }
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
        }
        elseif ($invoice->type == 'Domain') {
            $domains = $this->domain->where('detail_order_id', $invoice_id)->get();
            $billing_cycle = $domains[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$domains[0]->billing_cycle] ));
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            foreach ($domains as $key => $domain) {
                $request_panhanhoa = $this->domain_pa->registerDomain($domain, $due_date); //Request API PAVietNam
                //Út Hiển làm tổng thu
                $domain->update(['status' => 'Active']);
                $domain->next_due_date = $due_date;
                $domain->save();
                if($request_panhanhoa->{'ReturnCode'} == 200) {
                    $order->status = 'Finish';
                    $order->save();
                    // $this->send_mail_finish('hosting',$order, $invoice ,$hostings);
                    // $this->send_mail_finish('domain',$order, $invoice ,$domain);
                    $data_reg = [
                        'user_id' => Auth::user()->id,
                        'domain' => $domain['domain'],
                        'due_date' => date('d/m/Y', strtotime( $due_date )),
                        'total' => $invoice->sub_total
                    ];
                    try {
                      // Tạo log
                      $data_log = [
                         'user_id' => Auth::user()->id,
                         'action' => 'thanh toán',
                         'model' => 'User/HistoryPay_CloudzonePoint',
                         'description' => 'hóa đơn Domain <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> bằng Cloudzone Point thành công',
                         'description_user' => 'hóa đơn Domain <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> bằng Cloudzone Point thành công',
                      ];
                      $this->log_activity->create($data_log);

                      $mail = Mail::send('users.mails.finish_reg_domain', $data_reg, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to(Auth::user()->email)->subject('Hoàn thành đăng ký domain');
                      });
                      $mail_to_admin = Mail::send('users.mails.finish_reg_domain_to_admin', $data_reg, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to('tuansinhtk@gmail.com')->subject('Hoàn thành đăng ký domain');
                      });
                    } catch (\Exception $e) {
                      return true;
                    }

                    return true;
                } else {
                    $data_error = [
                        'user_id' => Auth::user()->id,
                        'domain' => $domain['domain'],
                        'error_code' => $request_panhanhoa->{'ReturnCode'},
                        'error' => $request_panhanhoa->{'ReturnText'},
                    ];
                    // dd($data_error);
                    try {
                      $mail = Mail::send('users.mails.error_reg_domain', $data_error, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to('tuansinhtk@gmail.com')->subject('Lỗi khi tạo domain');
                      });
                    } catch (\Exception $e) {
                      return true;
                    }

                    return true;
                }
            }


        }
        elseif ($invoice->type == 'Hosting-Singapore') {
            $hostings = $this->hosting->where('detail_order_id', $invoice_id)->get();
            $billing_cycle = $hostings[0]->billing_cycle;
            // Dành cho những hosting one_time_pay
            if ($billing_cycle == 'one_time_pay') {
                $date_paid = date('Y-m-d');
                $invoice->due_date = '2099-12-31';
                $invoice->paid_date = $date_paid;
                $invoice->status = 'paid';
                $invoice->payment_method = 1;
                $request_da = $this->da_si->orderCreateAccountWithPoint($hostings, 'one_time_pay'); //request đên Direct Admin
                // $request_da = true;
                $paid = $invoice->save();

                if ($request_da) {
                    $order->status = 'Finish';
                    $order->save();
                    // ghi vào tổng thu
                    if (!empty($this->total_price->first())) {
                        $total_price = $this->total_price->first();
                        $total_price->hosting += $total;
                        $total_price->save();
                    } else {
                        $data_total_price = [
                            'vps' => '0',
                            'hosting' => $total,
                            'server' => '0',
                        ];
                        $this->total_price->create($data_total_price);
                    }
                    try {
                      $this->send_mail_finish_cloudzone_point('hosting',$order, $invoice ,$hostings);
                    } catch (\Exception $e) {
                      return true;
                    }

                }
                return true;
            }
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$hostings[0]->billing_cycle] ));
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;

            $request_da = $this->da_si->orderCreateAccountWithPoint($hostings,$due_date); //request đên Direct Admin
            // $request_da = true;
            $paid = $invoice->save();
            if ($request_da) {
                $order->status = 'Finish';
                $order->save();
                // ghi vào tổng thu
                if (!empty($this->total_price->first())) {
                    $total_price = $this->total_price->first();
                    $total_price->hosting += $total;
                    $total_price->save();
                } else {
                    $data_total_price = [
                        'vps' => '0',
                        'hosting' => $total,
                        'server' => '0',
                    ];
                    $this->total_price->create($data_total_price);
                }
                try {
                  $this->send_mail_finish_cloudzone_point('hosting',$order, $invoice ,$hostings);
                } catch (\Exception $e) {
                  return $paid;
                }

            }
        }
        else {
            $hostings = $this->hosting->where('detail_order_id', $invoice_id)->get();

            $billing_cycle = $hostings[0]->billing_cycle;
            // Dành cho những hosting one_time_pay
            if ($billing_cycle == 'one_time_pay') {
                $date_paid = date('Y-m-d');
                $invoice->due_date = '2099-12-31';
                $invoice->paid_date = $date_paid;
                $invoice->status = 'paid';
                $invoice->payment_method = 1;

                $request_da = $this->da->orderCreateAccountWithPoint($hostings, 'one_time_pay'); //request đên Direct Admin
                // $request_da = true;
                $paid = $invoice->save();

                if ($request_da) {
                    $order->status = 'Finish';
                    $order->save();
                    $this->send_mail_finish_cloudzone_point('hosting',$order, $invoice ,$hostings);
                }
                return true;
            }
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$hostings[0]->billing_cycle] ));
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;

            $request_da = $this->da->orderCreateAccountWithPoint($hostings,$due_date); //request đên Direct Admin
            // $request_da = true;
            $paid = $invoice->save();
            if ($request_da) {
                $order->status = 'Finish';
                $order->save();
                // Tạo log
                $data_log = [
                   'user_id' => Auth::user()->id,
                   'action' => 'thanh toán',
                   'model' => 'User/HistoryPay_CloudzonePoint',
                   'description' => 'hóa đơn Hosting <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> bằng Cloudzone Point thành công',
                   'description_user' => 'hóa đơn Hosting <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> bằng Cloudzone Point thành công',
                ];
                $this->log_activity->create($data_log);
                // ghi vào tổng thu
                if (!empty($this->total_price->first())) {
                    $total_price = $this->total_price->first();
                    $total_price->hosting += $total;
                    $total_price->save();
                } else {
                    $data_total_price = [
                        'vps' => '0',
                        'hosting' => $total,
                        'server' => '0',
                    ];
                    $this->total_price->create($data_total_price);
                }
                try {
                  $this->send_mail_finish_cloudzone_point('hosting',$order, $invoice ,$hostings);
                } catch (\Exception $e) {
                  return $paid;
                }

            }


        }
        return $paid;
    }

    public function send_mail_finish_cloudzone_point($type, $order , $detail_order, $list_sevices)
    {
        $billings = config('billing');
        $product = $this->product->find($list_sevices[0]->product_id);
        $email = $this->email->find($product->meta_product->email_id);
        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $add_on =  '';
        if (!empty($detail_order->addon_id)) {
            $add_on = $this->product->find($list_sevices[0]->addon_id);
        }
        if ($type == 'vps') {
            foreach ($list_sevices as $key => $services) {
                $type_os = $services->os;
                if ($type_os == 1) {
                    $type_os = 'Windows 7 64bit';
                }
                elseif ($type_os == 2) {
                    $type_os = 'Windows Server 2012 R2';
                }
                elseif ($type_os == 3) {
                    $type_os = 'Windows Server 2016';
                }
                else {
                    $type_os = 'Linux CentOS 7 64bit';
                }

                $user = $this->user->find($services->user_id);
                $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone,
                        $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)),
                        $order->created_at, $services->user, $services->password, $detail_order->sub_total / 1000 . ' Cloudzone Point' ,
                        $order->total / 1000 . ' Cloudzone Point' , $detail_order->quantity, $type_os, '', $url];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                $this->user_send_email = $this->user->find($services->user_id)->email;
                $this->subject = $email->meta_email->subject;

                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'user_id' => $user->id,
                    'user_email' => $user->email,
                    'user_addpress' => $user->user_meta->address,
                    'user_phone' => $user->user_meta->user_phone,
                    'order_id' => $order->id,
                    'product_name' => $product->name,
                    'domain' => '',
                    'ip' => $services->ip,
                    'billing_cycle' => $services->billing_cycle,
                    'next_due_date' => $services->next_due_date,
                    'order_created_at' => $order->created_at,
                    'services_username' => $services->user,
                    'services_password' => $services->password,
                    'sub_total' => $detail_order->sub_total / 1000 . ' Cloudzone Point',
                    'total' => $order->total / 1000 . ' Cloudzone Point',
                    'qtt' => $detail_order->quantity,
                    'os' => $services->os,
                    'token' => '',
                ];

                $mail = Mail::send('users.mails.orders', $data, function($message){
                    $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                    $message->to($this->user_send_email)->subject($this->subject);
                });

                try {
                  $data['type'] = 'Khách hàng hoàn thành thanh toán VPS bằng Cloudzone Point';
                  $mail = Mail::send('users.mails.send_mail_support', $data, function($message){
                      $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                      $message->to('uthienth92@gmail.com')->subject('Khách hàng hoàn thành thanh toán VPS bằng Cloudzone Point');
                      $message->cc('support@cloudzone.vn')->subject('Khách hàng hoàn thành thanh toán VPS bằng Cloudzone Point');
                  });
                } catch (\Exception $e) {

                }


            }
        }
        elseif ($type == 'nat_vps') {
            foreach ($list_sevices as $key => $services) {
                $type_os = $services->os;
                if ($type_os == 1) {
                    $type_os = 'Windows 7 64bit';
                }
                elseif ($type_os == 2) {
                    $type_os = 'Windows Server 2012 R2';
                }
                elseif ($type_os == 3) {
                    $type_os = 'Windows Server 2016';
                }
                else {
                    $type_os = 'Linux CentOS 7 64bit';
                }

                $user = $this->user->find($services->user_id);
                $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone,
                          $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)),
                           $order->created_at, $services->user, $services->password, $detail_order->sub_total / 1000 . ' Cloudzone Point',
                           $order->total / 1000 . ' Cloudzone Point' , $detail_order->quantity, $type_os, '', $url];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                $this->user_send_email = $this->user->find($services->user_id)->email;
                $this->subject = $email->meta_email->subject;

                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'user_id' => $user->id,
                    'user_email' => $user->email,
                    'user_addpress' => $user->user_meta->address,
                    'user_phone' => $user->user_meta->user_phone,
                    'order_id' => $order->id,
                    'product_name' => $product->name,
                    'domain' => '',
                    'ip' => $services->ip,
                    'billing_cycle' => $services->billing_cycle,
                    'next_due_date' => $services->next_due_date,
                    'order_created_at' => $order->created_at,
                    'services_username' => $services->user,
                    'services_password' => $services->password,
                    'sub_total' => $detail_order->sub_total / 1000 . ' Cloudzone Point',
                    'total' => $order->total / 1000 . ' Cloudzone Point',
                    'qtt' => $detail_order->quantity,
                    'os' => $services->os,
                    'token' => '',
                ];

                $mail = Mail::send('users.mails.orders', $data, function($message){
                    $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                    $message->to($this->user_send_email)->subject($this->subject);
                });

                try {
                  $data['type'] = 'Khách hàng hoàn thành thanh toán NAT VPS bằng Cloudzone Point';
                  $mail = Mail::send('users.mails.send_mail_support', $data, function($message){
                      $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                      $message->to('uthienth92@gmail.com')->subject('Khách hàng hoàn thành thanh toán VPS bằng Cloudzone Point');
                      $message->cc('support@cloudzone.vn')->subject('Khách hàng hoàn thành thanh toán VPS bằng Cloudzone Point');
                  });
                } catch (\Exception $e) {

                }


            }
        }
        elseif ($type == 'hosting') {
            foreach ($list_sevices as $key => $services) {
                  $user = $this->user->find($services->user_id);
                  if (!empty($services->server_hosting_id)) {
                     $link_control_panel = '<a href="https://' . $services->server_hosting->host . ':' . $services->server_hosting->port . '" target="_blank" >https://'. $services->server_hosting->host . ':' . $services->server_hosting->port . '</a>' ;
                  } else {
                     $link_control_panel = '<a href="https://daportal.cloudzone.vn:2222/" target="_blank">https://daportal.cloudzone.vn:2222/</a>';
                  }
                  $search = ['{$url_control_panel}' , '{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}', '{$control_panel_url}'];
                  $replace = [$link_control_panel, !empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address ,
                          $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle],
                          date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,
                          $detail_order->sub_total / 1000 . ' Cloudzone Point' , $order->total / 1000 . ' Cloudzone Point', $detail_order->quantity, '', '', $url, $link_control_panel];
                  $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                  $this->user_send_email = $this->user->find($services->user_id)->email;
                  $this->subject = $email->meta_email->subject;

                  $data = [
                      'content' => $content,
                      'user_name' => $user->name,
                      'user_id' => $user->id,
                      'user_email' => $user->email,
                      'user_addpress' => $user->user_meta->address,
                      'user_phone' => $user->user_meta->user_phone,
                      'order_id' => $order->id,
                      'product_name' => $product->name,
                      'domain' => $services->domain,
                      'ip' => '',
                      'billing_cycle' => $services->billing_cycle,
                      'next_due_date' => $services->next_due_date,
                      'order_created_at' => $order->created_at,
                      'services_username' => $services->user,
                      'services_password' => $services->password,
                      'sub_total' => $detail_order->sub_total / 1000 . ' Cloudzone Point',
                      'total' => $order->total / 1000 . ' Cloudzone Point',
                      'qtt' => $detail_order->quantity,
                      'os' => $services->os,
                      'token' => '',
                  ];

                  $mail = Mail::send('users.mails.orders', $data, function($message){
                      $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                      $message->to($this->user_send_email)->subject($this->subject);
                  });

                  try {
                    $data['type'] = 'Khách hàng hoàn thành thanh toán Hosting bằng Cloudzone Point';
                    $mail = Mail::send('users.mails.send_mail_expired_hosting_support', $data, function($message){
                        $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                        $message->to('uthienth92@gmail.com')->subject('Khách hàng hoàn thành thanh toán Hosting bằng Cloudzone Point');
                        $message->cc('support@cloudzone.vn')->subject('Khách hàng hoàn thành thanh toán Hosting bằng Cloudzone Point');
                    });
                  } catch (\Exception $e) {

                  }
            }
        }
    }

    public function payment_expired_cloudzone_point($invoice_id)
    {
        $invoice = $this->detail_order->find($invoice_id);
        $total = $invoice->sub_total * $invoice->quantity;
        $order = $this->order->find($invoice->order->id);
        $order->status = 'Active';
        $order->save();
        $user_meta = $this->user_meta->where('user_id', Auth::user()->id)->first();
        if (!isset($user_meta)) {
           return false;
        }
        // point
        $user_meta->point = $user_meta->point - ($total / 1000);
        $user_meta->point_used += ($total / 1000);
        $user_meta->save();
        // invoice
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        $order_expireds = $invoice->order_expireds;

        if ($invoice->type == 'VPS') {
            $billing_cycle = $order_expireds[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expireds[0]->billing_cycle] ));
            // dd($due_date);
            // HistoryPay
            $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
            $history_pay->status = 'Active';
            $history_pay->method_gd_invoice = 'cloudzone_point';
            $history_pay->save();
            // Invoice
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            //
            foreach ($order_expireds as $key => $order_expired) {
                $vps = $order_expired->vps;
                $date_star = $vps->next_due_date;
                $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expired->billing_cycle] ));

                $vps->next_due_date = $due_date;
                $vps->save();

                $config_billing_DashBoard = config('billingDashBoard');
                $data_expired = [
                    'mob' => 'renew',
                    'package' => (int) $invoice->sub_total,
                    'leased_time' => $config_billing_DashBoard[$order_expired->billing_cycle],
                    'end_date' => $due_date,
                    'type' => 0,
                ];
                $reques_vcenter = $this->dashboard->expired_vps_point_cloudzone($data_expired, $vps->vm_id); //request đên Vcenter
                // ghi log
                $billings = config('billing');
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'thanh toán',
                  'model' => 'User/HistoryPay_CloudzonePoint',
                  'description' => ' gia hạn VPS <a target="_blank" href="admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billingss[$order_expired->billing_cycle] . ' bằng Cloudzone Point',
                  'description_user' => ' hóa đơn gia hạn VPS <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">' . $invoice->ip . '</a> '  . $billingss[$order_expired->billing_cycle] . ' bằng Cloudzone Point',
                  'service' =>  $vps->ip,
                ];
                $this->log_activity->create($data_log);
            }
            $order->status = 'Finish';
            $order->save();
            try {
              $this->send_mail_finish_expired_point_cloudzone('vps',$order, $invoice ,$order_expireds);
            } catch (\Exception $e) {
              return $paid;
            }

        }

        elseif ($invoice->type == 'Server') {
            $billing_cycle = $order_expireds[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expireds[0]->billing_cycle] ));
            foreach ($order_expireds as $key => $order_expired) {
                $server = $order_expired->server;
                $server->next_due_date = $due_date;
                $server->paid = 'paid';
                $server->save();
            }
            // HistoryPay
            $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
            $history_pay->status = 'Active';
            $history_pay->method_gd_invoice = 'cloudzone_point';
            $history_pay->save();
            // invoice
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            $order->status = 'Finish';
            $order->save();
        }

        else {
            // HistoryPay
            $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
            $history_pay->status = 'Active';
            $history_pay->method_gd_invoice = 'cloudzone_point';
            $history_pay->save();
            // hosting
            // dd($hostings);
            $billing_cycle = $order_expireds[0]->billing_cycle;
            // Dành cho những hosting one_time_pay
            // invoice
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expireds[0]->billing_cycle] ));
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            // $request_da = $this->da->orderCreateAccount($hostings,$due_date); //request đên Direct Admin
            $paid = $invoice->save();

            foreach ($order_expireds as $key => $order_expired) {
                $hosting = $order_expired->hosting;

                $date_star = $hosting->next_due_date;
                $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expired->billing_cycle] ));

                $hosting->next_due_date = $due_date;
                $hosting->save();
                // ghi log
                $billings = config('billing');
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'thanh toán',
                  'model' => 'User/HistoryPay_CloudzonePoint',
                  'description' => ' gia hạn Hosting <a target="_blank" href="/admin/hostings/detail/' . $hosting->id . '">' . $hosting->domain . '</a> '  . $billings[$order_expired->billing_cycle] . ' bằng Cloudzone Point',
                  'description_user' => ' hóa đơn gia hạn Hosting <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">' . $invoice->id . '</a> '  . $billings[$order_expired->billing_cycle] . ' bằng Cloudzone Point',
                  'service' =>  $hosting->domain,
                ];
                $this->log_activity->create($data_log);
            }

            $order->status = 'Finish';
            $order->save();
            try {
              $this->send_mail_finish_expired_point_cloudzone('hosting',$order, $invoice ,$order_expireds);
            } catch (\Exception $e) {
              return $paid;
            }


        }
        return $paid;
    }

    public function send_mail_finish_expired_point_cloudzone($type, $order , $detail_order, $order_expireds)
    {
        $billings = config('billing');
        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $add_on =  '';
        if ($type == 'vps') {
            foreach ($order_expireds as $key => $order_expired) {
                $services = $order_expired->vps;
                $product = $this->product->find($services->product_id);
                $email = $this->email->find($product->meta_product->email_expired_finish);

                $type_os = $services->os;
                if ($type_os == 1) {
                    $type_os = 'Windows 7 64bit';
                }
                elseif ($type_os == 2) {
                    $type_os = 'Windows Server 2012 R2';
                }
                elseif ($type_os == 3) {
                    $type_os = 'Windows Server 2016';
                }
                else {
                    $type_os = 'Linux CentOS 7 64bit';
                }

                $user = $this->user->find($services->user_id);
                $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone,
                        $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)),
                        $order->created_at, $services->user, $services->password, $detail_order->sub_total / 1000 . ' Cloudzone Point' ,
                        $order->total / 1000 . ' Cloudzone Point' , $detail_order->quantity, $type_os, '', $url];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                $this->user_send_email = $this->user->find($services->user_id)->email;
                $this->subject = $email->meta_email->subject;

                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'user_id' => $user->id,
                    'user_email' => $user->email,
                    'user_addpress' => $user->user_meta->address,
                    'user_phone' => $user->user_meta->user_phone,
                    'order_id' => $order->id,
                    'product_name' => $product->name,
                    'domain' => '',
                    'ip' => $services->ip,
                    'billing_cycle' => $services->billing_cycle,
                    'next_due_date' => $services->next_due_date,
                    'order_created_at' => $order->created_at,
                    'services_username' => $services->user,
                    'services_password' => $services->password,
                    'sub_total' => $detail_order->sub_total,
                    'total' => $order->total / 1000  . ' Cloudzone Point',
                    'qtt' => $detail_order->quantity,
                    'os' => $services->os,
                    'token' => '',
                ];

                $mail = Mail::send('users.mails.orders', $data, function($message){
                    $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                    $message->to($this->user_send_email)->subject($this->subject);
                });

                try {
                  $data['type'] = 'Khách hàng hoàn thành thanh toán khi gia hạn VPS bằng Cloudzone Point';
                  $mail = Mail::send('users.mails.send_mail_support', $data, function($message){
                      $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                      $message->to('uthienth92@gmail.com')->subject('Khách hàng hoàn thành thanh toán khi gia hạn VPS bằng Cloudzone Point');
                      $message->cc('support@cloudzone.vn')->subject('Khách hàng hoàn thành thanh toán khi gia hạn VPS bằng Cloudzone Point');
                  });
                } catch (\Exception $e) {

                }

            }
        } elseif ($type == 'hosting') {
            foreach ($order_expireds as $key => $order_expired) {
                  $services = $order_expired->hosting;
                  $product = $this->product->find($services->product_id);
                  $email = $this->email->find($product->meta_product->email_expired_finish);
                  $user = $this->user->find($services->user_id);
                  $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                  $replace = [ !empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address ,
                          $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle],
                          date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,
                          $detail_order->sub_total / 1000 . ' Cloudzone Point' , $order->total / 1000 . ' Cloudzone Point', $detail_order->quantity, '', '', $url];
                  $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                  $this->user_send_email = $this->user->find($services->user_id)->email;
                  $this->subject = !empty($email->meta_email->subject) ? $email->meta_email->subject : 'Gia hạn dịch vụ';

                  $data = [
                      'content' => $content,
                      'user_name' => $user->name,
                      'user_id' => $user->id,
                      'user_email' => $user->email,
                      'user_addpress' => $user->user_meta->address,
                      'user_phone' => $user->user_meta->user_phone,
                      'order_id' => $order->id,
                      'product_name' => $product->name,
                      'domain' => $services->domain,
                      'ip' => '',
                      'billing_cycle' => $services->billing_cycle,
                      'next_due_date' => $services->next_due_date,
                      'order_created_at' => $order->created_at,
                      'services_username' => $services->user,
                      'services_password' => $services->password,
                      'sub_total' => $detail_order->sub_total,
                      'total' => $order->total / 1000  . ' Cloudzone Point',
                      'qtt' => $detail_order->quantity,
                      'os' => $services->os,
                      'token' => '',
                  ];

                  $mail = Mail::send('users.mails.orders', $data, function($message){
                      $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                      $message->to($this->user_send_email)->subject($this->subject);
                  });

                  try {
                    $data['type'] = 'Khách hàng hoàn thành thanh toán khi gia hạn Hosting bằng Cloudzone Point';
                    $mail = Mail::send('users.mails.send_mail_expired_hosting_support', $data, function($message){
                        $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                        $message->to('uthienth92@gmail.com')->subject('Khách hàng hoàn thành thanh toán khi gia hạn Hosting bằng Cloudzone Point');
                        $message->cc('support@cloudzone.vn')->subject('Khách hàng hoàn thành thanh toán khi gia hạn Hosting bằng Cloudzone Point');
                    });
                  } catch (\Exception $e) {

                  }


            }
        }
    }

    // Thanh toán khi order addon
    public function payment_addon_cloudzone_point($invoice_id)
    {
        $invoice = $this->detail_order->find($invoice_id);
        $total = $invoice->sub_total * $invoice->quantity;
        $order = $this->order->find($invoice->order->id);
        $order->status = 'Active';
        $order->save();
        $user_meta = $this->user_meta->where('user_id', Auth::user()->id)->first();
        if (!isset($user_meta)) {
           return false;
        }
        // point
        $user_meta->point = (int) $user_meta->point - ($total / 1000);
        $user_meta->point_used += (int) ($total / 1000);
        $user_meta->save();
        // invoice
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        foreach ($invoice->order_addon_vps as $key => $order_addon_vps) {
            $vps = $order_addon_vps->vps;
            $billing_cycle = $vps->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$vps->billing_cycle] ));
            // dd($due_date);
            // HistoryPay
            $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
            $history_pay->status = 'Active';
            $history_pay->method_gd_invoice = 'cloudzone_point';
            $history_pay->save();
            // Invoice
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            // ghi vào tổng thu
            if (!empty($this->total_price->first())) {
                $total_price = $this->total_price->first();
                $total_price->vps += $total;
                $total_price->save();
            } else {
                $data_total_price = [
                    'vps' => $total,
                    'hosting' => '0',
                    'server' => '0',
                ];
                $this->total_price->create($data_total_price);
            }

            $data_addon = [
                'mob' => 'upgrade', // action
                'name' => $vps->vm_id,  // vm_id
                'extra_cpu' => !empty($order_addon_vps->cpu) ? $order_addon_vps->cpu : 0, // so CPU them
                'extra_ram' => !empty($order_addon_vps->ram) ? $order_addon_vps->ram : 0, // so RAM them
                'extra_disk' => !empty($order_addon_vps->disk) ? $order_addon_vps->disk : 0, // so DISK them
                'package' => $total, // tong tien
                'type' => $vps->type_vps == 'vps' ? 0 : 5,
            ];
            $reques_vcenter = $this->dashboard->upgrade_vps($data_addon, $vps->vm_id); //request đên Vcenter
            // $reques_vcenter= true;
            if ($reques_vcenter) {
                $order->status = 'Finish';
                $order->save();
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {
                    $vps_config->ram += $order_addon_vps->ram;
                    $vps_config->cpu += $order_addon_vps->cpu;
                    $vps_config->disk += $order_addon_vps->disk;
                    $vps_config->save();
                } else {
                    $data_config = [
                        'vps_id' => $vps->id,
                        'ram' => $order_addon_vps->ram,
                        'cpu' => $order_addon_vps->cpu,
                        'disk' => $order_addon_vps->disk,
                        'ip' => 0,
                    ];
                    $this->vps_config->create($data_config);
                }
                $text_addon = !empty($order_addon_vps->cpu) ? $order_addon_vps->cpu : 0;
                $text_addon .= ' CPU - ';
                $text_addon .= !empty($order_addon_vps->ram) ? $order_addon_vps->ram : 0;
                $text_addon .= ' RAM - ';
                $text_addon .= !empty($order_addon_vps->disk) ? $order_addon_vps->disk : 0;
                $text_addon .= ' DISK';
                // ghi log
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'thanh toán',
                  'model' => 'User/HistoryPay_CloudzonePoint',
                  'description' => ' addon VPS <a target="_blank" href="admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ' . $text_addon . ' bằng Cloudzone Point',
                  'description_user' => ' hóa đơn addon VPS <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">' . $invoice->id . '</a> ' . $text_addon . ' bằng Cloudzone Point',
                  'service' =>  $vps->ip,
                ];
                $this->log_activity->create($data_log);
                // Gữi email khi hoàn thành order addon
                $this->user_email = Auth::user()->email;
                $this->subject = 'Hoàn thành nâng cấp cấu hình VPS';
                $product_addons = $this->get_addon_product_private(Auth::user()->id);
                foreach ($product_addons as $key => $product_addon) {
                    if (!empty($product_addon->meta_product->type_addon)) {
                        if ($product_addon->meta_product->type_addon == 'addon_cpu') {
                            $product_addon_cpu = $product_addon;
                        }
                        if ($product_addon->meta_product->type_addon == 'addon_ram') {
                            $product_addon_ram = $product_addon;
                        }
                        if ($product_addon->meta_product->type_addon == 'addon_disk') {
                            $product_addon_disk = $product_addon;
                        }
                    }
                }
                if (!empty($order_addon_vps->month)) {
                    $time = $order_addon_vps->month . ' Tháng ' . $order_addon_vps->day . ' Ngày';
                } else {
                    $time = $order_addon_vps->day . ' Ngày';
                }
                $data = [
                    'name' => Auth::user()->name,
                    'email' => Auth::user()->email,
                    'amount' => $total,
                    'ip' => $vps->ip,
                    'cpu' => !empty($order_addon_vps->cpu) ? $order_addon_vps->cpu : 0,
                    'ram' => !empty($order_addon_vps->ram) ? $order_addon_vps->ram : 0,
                    'disk' => !empty($order_addon_vps->disk) ? $order_addon_vps->disk : 0,
                    'product_cpu' => !empty($product_addon_cpu) ? $product_addon_cpu : 0,
                    'product_ram' => !empty($product_addon_ram) ? $product_addon_ram : 0,
                    'product_disk' => !empty($product_addon_disk) ? $product_addon_disk : 0,
                    'billing_cycle' => $billing_cycle,
                    'time' =>  $time ,
                ];

                try {
                    $mail = Mail::send('users.mails.finish_order_addon_vps', $data, function($message){
                        $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                        $message->to($this->user_email)->subject($this->subject);
                    });

                    $data['type'] = 'Khách hàng hoàn thành đặt hàng khi nâng cấp cấu hình VPS';
                    $mail = Mail::send('users.mails.send_mail_addon_support', $data, function($message){
                        $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                        $message->to('uthienth92@gmail.com')->subject('Khách hàng hoàn thành đặt hàng khi nâng cấp cấu hình VPS');
                        $message->cc('support@cloudzone.vn')->subject('Khách hàng hoàn thành đặt hàng khi nâng cấp cấu hình VPS');
                    });
                } catch (\Exception $e) {
                    return $paid;
                }


            }
        }
        return $paid;
    }


    public function payment_change_ip_cloudzone_point($invoice_id)
    {
      $invoice = $this->detail_order->find($invoice_id);
      $total = $invoice->sub_total * $invoice->quantity;
      $order = $this->order->find($invoice->order->id);
      $order->status = 'Active';
      $order->save();
      $user_meta = $this->user_meta->where('user_id', Auth::user()->id)->first();
      if (!isset($user_meta)) {
         return false;
      }
      // point
      $user_meta->point = $user_meta->point - ($total / 1000);
      $user_meta->point_used += ($total / 1000);
      $user_meta->save();
      // invoice
      $billing = [
        'monthly' => '1 Month',
        'twomonthly' => '2 Month',
        'quarterly' => '3 Month',
        'semi_annually' => '6 Month',
        'annually' => '1 Year',
        'biennially' => '2 Year',
        'triennially' => '3 Year'
      ];
      foreach ($invoice->order_change_vps as $key => $order_change_vps) {
        $vps = $order_change_vps->vps;
        $billing_cycle = $vps->billing_cycle;
        $date_star = $invoice->created_at;
        $date_paid = date('Y-m-d');
        $due_date = date('Y-m-d', strtotime( $date_star . $billing[$vps->billing_cycle] ));
        // dd($due_date);;
        // HistoryPay
        $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
        $history_pay->status = 'Active';
        $history_pay->method_gd_invoice = 'cloudzone_point';
        $history_pay->save();
        // Invoice
        $invoice->due_date = $due_date;
        $invoice->paid_date = $date_paid;
        $invoice->status = 'paid';
        $invoice->payment_method = 1;
        $paid = $invoice->save();
        // ghi vào tổng thu
        if (!empty($this->total_price->first())) {
          $total_price = $this->total_price->first();
          $total_price->vps += $total;
          $total_price->save();
        } else {
          $data_total_price = [
            'vps' => $total,
            'hosting' => '0',
            'server' => '0',
          ];
          $this->total_price->create($data_total_price);
        }
        $ip = $vps->ip;
        $reques_vcenter = $this->dashboard->change_ip($vps, $invoice->sub_total); //request đên Vcenter
        // $reques_vcenter= true;
        if ($reques_vcenter) {
          $order->status = 'Finish';
          $order->save();
          // ghi log
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'thanh toán ',
            'model' => 'User/HistoryPay_CloudzonePoint',
            'description' => 'đổi IP VPS ' . $ip . ' thành <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
            'description_user' => 'hóa đơn đổi IP VPS ' . $ip . ' thành <a href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a>',
            'service' =>  $ip . ' thành ' . $vps->ip,
          ];
          $this->log_activity->create($data_log);
          // Gữi email khi hoàn thành order change IP
          $this->user_email = Auth::user()->email;
          $this->subject = 'Hoàn thành thanh toán thay đổi IP cho VPS';


          $data = [
            'name' => Auth::user()->name,
            'email' => Auth::user()->email,
            'amount' => $total,
            'ip' => $ip,
            'ip_change' => $vps->ip,
            'amount' => $invoice->sub_total,
          ];

          try {
            $mail = Mail::send('users.mails.finish_order_change_ip_vps', $data, function($message){
              $message->from('support@cloudzone.vn', 'Cloudzone Portal');
              $message->to($this->user_email)->subject($this->subject);
            });
            $data['type'] = 'Khách hàng hoàn thành đặt hàng thay đổi IP cho VPS';
            $mail = Mail::send('users.mails.send_mail_change_ip_vps', $data, function($message){
              $message->from('support@cloudzone.vn', 'Cloudzone Portal');
              $message->to('uthienth92@gmail.com')->subject('Khách hàng thanh toán đổi IP cho VPS');
              $message->cc('support@cloudzone.vn')->subject('Khách hàng thanh toán đổi IP cho VPS');
            });
          } catch (\Exception $e) {
            return $paid;
          }


        }
      }
      return $paid;
    }

    public function payment_domain_expired_point_cloudzone($invoice_id) {
        $invoice = $this->detail_order->find($invoice_id);
        $total = $invoice->sub_total * $invoice->quantity;
        $order = $this->order->find($invoice->order->id);
        $order->status = 'Active';
        $order->save();
        $credit = $this->credit->where('user_id', Auth::user()->id)->first();
        if (!isset($credit)) {
            return false;
        }
        // invoice
        $billing = config('billing_domain_exp');
        $domain_expireds = $invoice->domain_expireds;
        if ($invoice->type == 'Domain_exp') {
            // credit
            $credit->value = $credit->value - $total;
            $credit->save();
            // HistoryPay
            $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
            $history_pay->status = 'Active';
            $history_pay->save();
            // domain
            $billing_cycle = $domain_expireds[0]->billing_cycle;
            // invoice
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$domain_expireds[0]->billing_cycle] . ' Year' ));
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();

            foreach ($domain_expireds as $key => $domain_expired) {
                $domain = $domain_expired->domain;
                // $date_star = $domain->next_due_date;
                $date_star = date('Y-m-d');
                $due_date = date('Y-m-d', strtotime( $date_star . $billing[$domain_expired->billing_cycle] . ' Year'));
                $domain->next_due_date = $due_date;
                $domain->save();
                $year = $billing[$domain_expired->billing_cycle];
                $request_panhanhoa = $this->domain_pa->renewDomain($domain, $year);
                if($request_panhanhoa->{'ReturnCode'} == 200) {
                // $request_panhanhoa = true;
                // if($request_panhanhoa) {
                    $order->status = 'Finish';
                    $order->save();
                    // tạo log
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' =>  'thanh toán',
                      'model' => 'User/HistoryPay_CloudzonePoint',
                      'description' => 'gia hạn Domain <a target="_blank" href="/admin/domain/detail/' . $domain->id . '">#' . $domain->domain . '</a> bằng Cloudzone Point',
                      'description_user' => 'hóa đơn gia hạn Domain <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> bằng Cloudzone Point',
                      'service' =>  $domain->domain,
                    ];
                    $this->log_activity->create($data_log);
                    // $this->send_mail_finish('hosting',$order, $invoice ,$hostings);
                    // $this->send_mail_finish('domain',$order, $invoice ,$domain);
                    $data_reg = [
                        'user_id' => Auth::user()->id,
                        'domain' => $domain['domain'],
                        'due_date' => date('d/m/Y', strtotime( $due_date )),
                        'total' => $invoice->sub_total
                    ];
                    try {
                      $mail = Mail::send('users.mails.finish_renew_domain', $data_reg, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to(Auth::user()->email)->subject('Hoàn thành gia hạn domain');
                      });
                      $mail_to_admin = Mail::send('users.mails.finish_renew_domain_to_admin', $data_reg, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to('tuansinhtk@gmail.com')->subject('Hoàn thành gia hạn domain');
                      });
                    } catch (\Exception $e) {
                      return true;
                    }

                    return true;
                } else {
                    $data_error = [
                        'user_id' => Auth::user()->id,
                        'domain' => $domain['domain'],
                        'error_code' => $request_panhanhoa->{'ReturnCode'},
                        'error' => $request_panhanhoa->{'ReturnText'},
                    ];
                    // dd($data_error);
                    try {
                      $mail = Mail::send('users.mails.error_renew_domain', $data_error, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to('tuansinhtk@gmail.com')->subject('Lỗi khi gia hạn domain');
                      });
                    } catch (\Exception $e) {
                      return true;
                    }

                    return true;
                }
            }
            $order->status = 'Finish';
            $order->save();
        }
        return $paid;
    }

    public function get_addon_product_private($user_id)
    {
        $products = [];
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_user = $user->group_user;
            $group_products = $group_user->group_products;
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->get();
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        }
        return $products;
    }

    // Thanh toán khi order
    public function payment_order_upgrade_with_point_cloudzone($invoice_id)
    {
        $invoice = $this->detail_order->find($invoice_id);
        $total = $invoice->sub_total * $invoice->quantity;
        $order = $this->order->find($invoice->order->id);
        $order->status = 'Active';
        $order->save();
        $user_meta = $this->user_meta->where('user_id', Auth::user()->id)->first();
        if (!isset($user_meta)) {
           return false;
        }
        // point
        $user_meta->point = $user_meta->point - ($total / 1000);
        $user_meta->point_used += ($total / 1000);
        $user_meta->save();
        // HistoryPay
        $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
        $history_pay->status = 'Active';
        $history_pay->method_gd_invoice = 'cloudzone_point';
        $history_pay->save();
        // invoice
        $invoice->paid_date = date('Y-m-d');
        $invoice->status = 'paid';
        $invoice->payment_method = 1;
        $paid = $invoice->save();
        // nang cap hosting
        $upgrade = $invoice->order_upgrade_hosting;
        $hosting = $upgrade->hosting;
        if ($hosting->location == 'vn') {
          $this->da->upgrade($hosting, $upgrade->product->package);
        }
        elseif ($hosting->location == 'si') {
          $this->da_si->upgrade($hosting, $upgrade->product->package);
        }
        $hosting->product_id = $upgrade->product_id;
        $hosting->save();
        // ghi log
        $product = $upgrade->product;
        if ($product->group_product->private == 0) {
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'thanh toán',
            'model' => 'User/HistoryPay_CloudzonePoint',
            'description' => ' gói nâng cấp Hosting <a target="_blank" href="/admin/hostings/detail/">' . $hosting->domain . '</a> lên gói Hosting <a href="admin/products/edit/' . $product->id . '">' . $product->name . '</a> bằng Cloudzone Point',
          ];
        } else {
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'thanh toán',
            'model' => 'User/HistoryPay_CloudzonePoint',
            'description' => ' gói nâng cấp Hosting <a target="_blank" href="/admin/hostings/detail/">' . $hosting->domain . '</a> lên gói Hosting <a href="admin/product_privates/edit/' . $product->id . '">' . $product->name . '</a> bằng Cloudzone Point',
            'description_user' => ' hóa đơn nâng cấp Hosting <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">' . $invoice->id . '</a> bằng Cloudzone Point',
            'service' =>  $hosting->domain,
          ];
        }
        $this->log_activity->create($data_log);

        $data = [
            'name' => Auth::user()->name,
            'email' => Auth::user()->email,
            'domain' => $hosting->domain,
            'product_name' => $upgrade->product->name,
        ];
        $this->user_email = Auth::user()->email;
        $this->subject = 'Hoàn thành nâng cấp dịch vụ Hosting';


        try {
            $mail = Mail::send('users.mails.finish_order_upgarde_hosting', $data, function($message){
                $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                $message->to($this->user_email)->subject($this->subject);
            });
            $data['type'] = 'Khách hàng hoàn thành đặt hàng khi nâng cấp dịch vụ hosting bằng Cloudzone Point';
            $mail = Mail::send('users.mails.send_mail_upgrade_support', $data, function($message){
                $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                $message->to('uthienth92@gmail.com')->subject('Khách hàng hoàn thành đặt hàng khi nâng cấp dịch vụ hosting bằng Cloudzone Point');
                $message->cc('support@cloudzone.vn')->subject('Khách hàng hoàn thành đặt hàng khi nâng cấp dịch vụ hosting bằng Cloudzone Point');
            });
        } catch (\Exception $e) {
            return $paid;
        }

        return $paid;
    }


}
