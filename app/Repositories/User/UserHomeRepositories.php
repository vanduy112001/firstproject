<?php

namespace App\Repositories\User;

use App\CmndVerify as AppCmndVerify;
use App\Model\User;
use App\Model\UserMeta;
use App\Model\Verify;
use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\Email;
use Illuminate\Support\Facades\Auth;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\VpsConfig;
use App\Model\Server;
use App\Model\Hosting;
use App\Model\OrderVetify;
use App\Model\Credit;
use App\Model\Customer;
use App\Model\GroupUser;
use App\Model\ReadNotification;
use App\Model\Notification;
use App\Model\EventPromotion;
use App\Model\VpsOs;
use App\Model\Promotion;
use App\Model\PromotionMeta;
use App\Model\LogActivity;
use App\Model\Tutorial;
use App\Model\Colocation;
use App\Model\LogPayment;
use App\Model\SessionPayment;
use App\Model\ProductDatacenter;
use App\Model\ServerManagement;
use App\Model\ProductRaid;
use App\Model\ServerConfig;
use App\Model\Proxy;
use App\Model\ColocationConfigIp;
use App\Model\ColocationConfig;
use Mail;
use App\Jobs\SendMailOrderDomain;
use App\Model\MetaGroupProduct;
use App\Model\TotalPrice;
use App\Model\HistoryPay;
use App\Factories\AdminFactories;
use App\Http\Controllers\Admin\DirectAdminController;
use App\Model\DomainProduct;
use App\Model\Domain;
use App\Model\DomainExpired;
use Mockery\CountValidator\AtMost;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use App\Model\CmndVerify;
use App\Model\OrderExpired;
use App\Model\SendMail;
use App\Model\ServerConfigRam;
use App\Model\ServerConfigIp;

class UserHomeRepositories {

    protected $user;
    protected $user_meta;
    protected $group_user;
    protected $verify;
    protected $email;
    protected $group_product;
    protected $product;
    protected $order;
    protected $detail_order;
    protected $vps;
    protected $vps_config;
    protected $server;
    protected $server_config;
    protected $hosting;
    protected $user_email;
    protected $subject;
    protected $order_vetify;
    protected $credit;
    protected $meta_group_product;
    protected $total_price;
    protected $history_pay;
    protected $customer;
    protected $user_send_email;
    protected $domain_product;
    protected $domain;
    protected $domain_exp;
    protected $readnotification;
    protected $notification;
    protected $event_promotion;
    protected $vps_os;
    protected $log_activity;
    protected $tutorial;
    protected $cmnd_verify;
    protected $promotion;
    protected $promotion_meta;
    protected $colocation;
    protected $log_payment;
    protected $order_expired;
    protected $session_payment;
    protected $send_mail;
    protected $product_datacenter;
    protected $server_management;
    protected $product_raid;
    protected $proxy;
    protected $server_config_ram;
    protected $server_config_ip;
    protected $colocation_config_ip;
    protected $colocation_config;
    // API
    protected $da;
    protected $dashboard;
    protected $domain_pa;
    protected $da_si;

    public function __construct()
    {
        $this->user = new User;
        $this->email = new Email;
        $this->promotion = new Promotion;
        $this->promotion_meta = new PromotionMeta;
        $this->user_meta = new UserMeta;
        $this->group_user = new GroupUser;
        $this->verify = new Verify;
        $this->group_product = new GroupProduct;
        $this->product = new Product;
        $this->order = new Order;
        $this->detail_order = new DetailOrder;
        $this->vps = new Vps;
        $this->vps_config = new VpsConfig;
        $this->server = new Server;
        $this->server_config = new ServerConfig;
        $this->hosting = new Hosting;
        $this->order_vetify = new OrderVetify;
        $this->credit = new Credit;
        $this->meta_group_product = new MetaGroupProduct;
        $this->total_price = new TotalPrice;
        $this->history_pay = new HistoryPay;
        $this->customer = new Customer;
        $this->domain_product = new DomainProduct;
        $this->domain = new Domain;
        $this->domain_exp = new DomainExpired;
        $this->notification = new Notification();
        $this->readnotification = new ReadNotification();
        $this->event_promotion = new EventPromotion();
        $this->log_activity = new LogActivity();
        $this->vps_os = new VpsOs;
        $this->tutorial = new Tutorial;
        $this->cmnd_verify = new CmndVerify;
        $this->colocation = new Colocation;
        $this->log_payment = new LogPayment;
        $this->order_expired = new OrderExpired;
        $this->session_payment = new SessionPayment;
        $this->send_mail = new SendMail;
        $this->product_datacenter = new ProductDatacenter();
        $this->server_management = new ServerManagement();
        $this->product_raid = new ProductRaid();
        $this->proxy = new Proxy();
        $this->server_config_ram = new ServerConfigRam();
        $this->server_config_ip = new ServerConfigIp();
        $this->colocation_config_ip = new ColocationConfigIp();
        $this->colocation_config = new ColocationConfig();
        // API
        $this->dashboard = AdminFactories::dashBoardRepositories();
        $this->da = AdminFactories::directAdminRepositories();
        $this->domain_pa = AdminFactories::domainRepositories();
        $this->da_si = AdminFactories::directAdminSingaporeRepositories();
        // $this->domain_api = new Domain();
    }

    public function check_session_payment($user_id)
    {
      $session = $this->session_payment->where('user_id', $user_id)->first();
      if ( isset($session) ) {
        if ( $session->payment ) {
          return true;
        } else {
          $session->payment = true;
          $session->save();
          return false;
        }
      } else {
        $data = [
          'user_id' => $user_id,
          'payment' => true
        ];
        $this->session_payment->create($data);
        return false;
      }
    }

    public function remove_session_payment($user_id)
    {
      $session = $this->session_payment->where('user_id', $user_id)->first();
      if ( isset($session) ) {
        if ( $session->payment ) {
          $session->payment = false;
          $session->save();
          return true;
        }
      } else {
        $data = [
          'user_id' => $user_id,
          'payment' => false
        ];
        $this->session_payment->create($data);
        return true;
      }
    }
    // lấy hosting theo domain
    public function get_hosting_with_domain($domain)
    {
        $hosting = $this->hosting->where('domain', $domain)->first();
        if (isset($hosting)) {
            return $hosting;
        } else {
            return false;
        }
    }

    public function get_tutorials()
    {
        return $this->tutorial->orderBy('id', 'desc')->get();
    }

    public function list_vps_os($product_id)
    {
       return $this->vps_os->where('product_id', $product_id)->get();
    }

    public function list_datacenter($product_id)
    {
       return $this->product_datacenter->where('product_id', $product_id)->get();
    }

    public function list_server_management($product_id)
    {
       return $this->server_management->where('product_id', $product_id)->get();
    }

    public function list_raid($product_id)
    {
        return $this->product_raid->where('product_id', $product_id)->get();
    }
    // ADDON DISK
    public function list_add_on_disk_server()
    {
        $user = $this->user->find(Auth::id());
        $group_products = $this->group_product->where('group_user_id', $user->group_user_id)->where('hidden', false)->where('type', 'addon_server')->get();
        $data = [];
        foreach ($group_products as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                if ( $product->meta_product->type_addon == 'addon_disk' ) {
                    $product->pricing = $product->pricing;
                    $data[] = $product;
                }
            }
        }
        return $data;
    }
    // ADDON RAM
    public function list_add_on_disk_ram()
    {
        $user = $this->user->find(Auth::id());
        $group_products = $this->group_product->where('group_user_id', $user->group_user_id)->where('hidden', false)->where('type', 'addon_server')->get();
        $data = [];
        foreach ($group_products as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                if ( $product->meta_product->type_addon == 'addon_ram' ) {
                    $product->pricing = $product->pricing;
                    $data[] = $product;
                }
            }
        }
        return $data;
    }
    // ADDON IP
    public function list_add_on_ip()
    {
        $user = $this->user->find(Auth::id());
        $group_products = $this->group_product->where('group_user_id', $user->group_user_id)->where('hidden', false)->where('type', 'addon_server')->get();
        $data = [];
        foreach ($group_products as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                if ( $product->meta_product->type_addon == 'addon_ip' ) {
                    $product->pricing = $product->pricing;
                    $data[] = $product;
                }
            }
        }
        return $data;
    }
    // ADDON Colocation IP
    public function list_add_on_ip_colo()
    {
        $user = $this->user->find(Auth::id());
        $group_products = $this->group_product->where('group_user_id', $user->group_user_id)->where('hidden', false)->where('type', 'addon_colo')->get();
        $data = [];
        foreach ($group_products as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                if ( $product->meta_product->type_addon == 'addon_ip' ) {
                    $product->pricing = $product->pricing;
                    $data[] = $product;
                }
            }
        }
        return $data;
    }

    // Tổng tiền đã chi của user
    public function total_price_use($user_id)
    {
       $total = 0;
       $payments = $this->history_pay->where('user_id', $user_id)->where('status', 'Active')->get();
       foreach ($payments as $key => $payment) {
          if ($payment->method_gd_invoice != 'cloudzone_point') {
            if ($payment->method_gd == 'invoice' || $payment->method_gd == 'domain' || $payment->method_gd == 'domain_exp') {
              $total += $payment->money;
            }
          }
       }
       return $total;
    }

    public function get_group_product()
    {
        $group_products = $this->group_product->with('products')->get();
        return $group_products;
    }
    // get meta group product
    public function get_group_product_private($user_id)
    {
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_user = $user->group_user;
            $group_products = $group_user->group_products;
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('hidden', false)->where('private', 0)->get();
        }

        return $group_products;

    }
    // get meta group product vps
    public function get_group_product_vps_private($user_id)
    {
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_products = $this->group_product->where('group_user_id', $user->group_user_id)->where('type', 'vps')->where('hidden', false)->orderBy('stt', 'asc')->get();
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('type', 'vps')->where('hidden', false)->where('private', 0)->orderBy('stt', 'asc')->get();
            // dd($group_products);
        }

        return $group_products;

    }
    // get meta group product server
    public function get_group_product_server($user_id)
    {
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_products = $this->group_product->where('group_user_id', $user->group_user_id)->where('type', 'server')->where('hidden', false)->first();
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('type', 'server')->where('hidden', false)->where('private', 0)->first();
            // dd($group_products);
        }
        return $group_products;
    }
    // get meta group product server
    public function get_group_product_proxy($user_id)
    {
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_products = $this->group_product->where('group_user_id', $user->group_user_id)->where('type', 'proxy')->where('hidden', false)->first();
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('type', 'proxy')->where('hidden', false)->where('private', 0)->first();
            // dd($group_products);
        }
        return $group_products;
    }
    // get meta group product vps_us
    public function get_group_product_vps_us_private($user_id)
    {
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_products = $this->group_product->where('group_user_id', $user->group_user_id)->where('type', 'vps_us')->where('hidden', false)->orderBy('stt', 'asc')->get();
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('type', 'vps_us')->where('hidden', false)->where('private', 0)->orderBy('stt', 'asc')->get();
            // dd($group_products);
        }

        return $group_products;

    }
    // get meta group product vps_us
    public function list_group_products_vps_us($user_id)
    {
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_products = $this->group_product->where('group_user_id', $user->group_user_id)
              ->where(function($query)
              {
                  return $query
                  ->whereIn('type', [ 'vps_us', 'vps_uk', 'vps_sing', 'vps_hl', 'vps_uc', 'vps_ca' ]);
              })
              ->where('hidden', false)->orderBy('stt', 'asc')->get();
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)
              ->where(function($query)
              {
                  return $query
                  ->whereIn('type', [ 'vps_us', 'vps_uk', 'vps_sing', 'vps_hl', 'vps_uc', 'vps_ca' ]);
              })
              ->where('hidden', false)->where('private', 0)->orderBy('stt', 'asc')->get();

        }
        // dd($group_products);
        return $group_products;

    }
    // get meta group product hosting
    public function get_group_product_hosting_private($user_id)
    {
        $user = $this->user->find($user_id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_products = $this->group_product->where('group_user_id', $user->group_user_id)->where('type', 'hosting')->where('hidden', false)->orderBy('stt', 'asc')->get();
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('type', 'hosting')->where('hidden', false)->where('private', 0)->orderBy('stt', 'asc')->get();
            // dd($group_products);
        }

        return $group_products;

    }

    public function check_customer($makh)
    {
        $customer = $this->customer->where('ma_customer' , $makh)->first();
        return $customer;
    }

    public function get_addon_product_private($user_id)
    {
        $products = [];
        $user = $this->user->find($user_id);
        // dd($user);
        if ( !empty($user->group_user_id) ) {
            $group_user = $user->group_user;
            $group_products = $group_user->group_products;
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->get();
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        }
        return $products;
    }

    public function get_product_change_ip($user_id)
    {
        $user = Auth::user();
        $product = [];
        if (!empty($user->group_user_id)) {
            $group_user = $user->group_user;
            $group_products = $group_user->group_products;
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Change-IP')->where('group_product_id', $group_product->id)->count() > 0) {
                    $product = $this->product->where('type_product', 'Change-IP')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->first();
                }
            }
            if (empty($product)) {
                $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->get();
                foreach ($group_products as $key => $group_product) {
                    if ($this->product->where('type_product', 'Change-IP')->where('group_product_id', $group_product->id)->count() > 0) {
                        $product = $this->product->where('type_product', 'Change-IP')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->first();
                    }
                }
            }
        } else {
            $group_products = $this->group_product->where('group_user_id', 0)->where('private', 0)->get();
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Change-IP')->where('group_product_id', $group_product->id)->count() > 0) {
                    $product = $this->product->where('type_product', 'Change-IP')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->first();
                }
            }
        }
        return $product;
    }

    public function get_hostings($detail_order_id)
    {
        $hostings = $this->hosting->where('expired_id', $detail_order_id)->get();
        return $hostings;
    }

    public function get_vps_expired($detail_order_id)
    {
        $list_vps = $this->vps->where('expired_id', $detail_order_id)->get();
        return $list_vps;
    }

    public function get_customer_with_hosting($id)
    {
        $hosting = $this->hosting->find($id);
        $invoice = $hosting->detail_order;
        $customer = $this->customer->where('ma_customer', $invoice->order->makh)->first();
        if (isset($customer)) {
            return $customer;
        } else {
            return false;
        }
    }

    public function get_customer_with_vps($id)
    {
        $vps = $this->vps->find($id);
        $invoice = $vps->detail_order;
        if ( !empty($invoice->order->makh) ) {
            $customer = $this->customer->where('ma_customer', $invoice->order->makh)->first();
            if (isset($customer)) {
                return $customer;
            } else {
                return false;
            }
        }
    }

    public function check_promotion($type_order , $ma_km, $product_id, $billing_cycle)
    {
        $promotion = $this->promotion->where('code' , $ma_km)->first();
        if ( !empty($promotion) ) {
          if ( $promotion->product_promotions->count() > 0 || $promotion->billing_promotions->count() > 0 ) {
              $data = [
                'error' => 5,
              ];
              if ( $promotion->product_promotions->count() > 0 && $promotion->billing_promotions->count() > 0 ) {
                  $check_product = false;
                  $check_billing = false;
                  foreach ($promotion->product_promotions as $key => $product_promotion) {
                      if ($product_promotion->product_id == $product_id) {
                        $check_product = true;
                      }
                  }
                  foreach ($promotion->billing_promotions as $key => $billing_promotion) {
                      if ($billing_promotion->billing == $billing_cycle) {
                        $check_billing = true;
                      }
                  }
                  if ($check_product && $check_billing) {
                      if ($promotion->life_time) {
                          if ( $promotion->apply_one ) {
                              $promotion_meta = $this->promotion_meta->where('user_id', Auth::user()->id)->where('promotion_id',$promotion_id)->first();
                              if ( !empty($promotion_meta) ) {
                                  $data = [
                                      'error' => 2,
                                  ];
                              } else {
                                  $start_date = $promotion->start_date;
                                  $end_date = $promotion->end_date;
                                  $check_date = $this->check_date_promotion($start_date, $end_date);
                                  if ( $check_date ) {
                                      if ($promotion->max_uses > $promotion->uses) {
                                          $data = [
                                              'error' => 0,
                                              'type' => $promotion->type,
                                              'value' => $promotion->value,
                                          ];
                                      } else {
                                          $data = [
                                              'error' => 3,
                                          ];
                                      }
                                  } else {
                                    $data = [
                                        'error' => 1,
                                    ];
                                  }
                              }
                          } else {
                              $start_date = $promotion->start_date;
                              $end_date = $promotion->end_date;
                              $check_date = $this->check_date_promotion($start_date, $end_date);
                              if ( $check_date ) {
                                  if ($promotion->max_uses > $promotion->uses) {
                                      $data = [
                                          'error' => 0,
                                          'type' => $promotion->type,
                                          'value' => $promotion->value,
                                      ];
                                  } else {
                                      $data = [
                                          'error' => 3,
                                      ];
                                  }
                              } else {
                                $data = [
                                    'error' => 1,
                                ];
                              }
                          }
                      }
                      else {
                          if ( $promotion->type_order ==  $type_order ) {
                              if ( $promotion->apply_one ) {
                                  $promotion_meta = $this->promotion_meta->where('user_id', Auth::user()->id)->where('promotion_id',$promotion_id)->first();
                                  if ( !empty($promotion_meta) ) {
                                      $data = [
                                          'error' => 2,
                                      ];
                                  } else {
                                      $start_date = $promotion->start_date;
                                      $end_date = $promotion->end_date;
                                      $check_date = $this->check_date_promotion($start_date, $end_date);
                                      if ( $check_date ) {
                                          if ($promotion->max_uses > $promotion->uses) {
                                              $data = [
                                                  'error' => 0,
                                                  'type' => $promotion->type,
                                                  'value' => $promotion->value,
                                              ];
                                          } else {
                                              $data = [
                                                  'error' => 3,
                                              ];
                                          }
                                      } else {
                                        $data = [
                                            'error' => 1,
                                        ];
                                      }
                                  }
                              } else {
                                  $start_date = $promotion->start_date;
                                  $end_date = $promotion->end_date;
                                  $check_date = $this->check_date_promotion($start_date, $end_date);
                                  if ( $check_date ) {
                                      if ($promotion->max_uses > $promotion->uses) {
                                          $data = [
                                              'error' => 0,
                                              'type' => $promotion->type,
                                              'value' => $promotion->value,
                                          ];
                                      } else {
                                          $data = [
                                              'error' => 3,
                                          ];
                                      }
                                  } else {
                                    $data = [
                                        'error' => 1,
                                    ];
                                  }
                              }
                          } else {
                              $data = [
                                'error' => 5,
                              ];
                          }
                      }
                  }
              }
              else if ( $promotion->product_promotions->count() > 0 ) {
                  $check_product = false;
                  foreach ($promotion->product_promotions as $key => $product_promotion) {
                      if ($product_promotion->product_id == $product_id) {
                        $check_product = true;
                      }
                  }
                  if ($check_product) {
                      if ($promotion->life_time) {
                          if ( $promotion->apply_one ) {
                              $promotion_meta = $this->promotion_meta->where('user_id', Auth::user()->id)->where('promotion_id',$promotion_id)->first();
                              if ( !empty($promotion_meta) ) {
                                  $data = [
                                      'error' => 2,
                                  ];
                              } else {
                                  $start_date = $promotion->start_date;
                                  $end_date = $promotion->end_date;
                                  $check_date = $this->check_date_promotion($start_date, $end_date);
                                  if ( $check_date ) {
                                      if ($promotion->max_uses > $promotion->uses) {
                                          $data = [
                                              'error' => 0,
                                              'type' => $promotion->type,
                                              'value' => $promotion->value,
                                          ];
                                      } else {
                                          $data = [
                                              'error' => 3,
                                          ];
                                      }
                                  } else {
                                    $data = [
                                        'error' => 1,
                                    ];
                                  }
                              }
                          } else {
                              $start_date = $promotion->start_date;
                              $end_date = $promotion->end_date;
                              $check_date = $this->check_date_promotion($start_date, $end_date);
                              if ( $check_date ) {
                                  if ($promotion->max_uses > $promotion->uses) {
                                      $data = [
                                          'error' => 0,
                                          'type' => $promotion->type,
                                          'value' => $promotion->value,
                                      ];
                                  } else {
                                      $data = [
                                          'error' => 3,
                                      ];
                                  }
                              } else {
                                $data = [
                                    'error' => 1,
                                ];
                              }
                          }
                      }
                      else {
                          if ( $promotion->type_order ==  $type_order ) {
                              if ( $promotion->apply_one ) {
                                  $promotion_meta = $this->promotion_meta->where('user_id', Auth::user()->id)->where('promotion_id',$promotion_id)->first();
                                  if ( !empty($promotion_meta) ) {
                                      $data = [
                                          'error' => 2,
                                      ];
                                  } else {
                                      $start_date = $promotion->start_date;
                                      $end_date = $promotion->end_date;
                                      $check_date = $this->check_date_promotion($start_date, $end_date);
                                      if ( $check_date ) {
                                          if ($promotion->max_uses > $promotion->uses) {
                                              $data = [
                                                  'error' => 0,
                                                  'type' => $promotion->type,
                                                  'value' => $promotion->value,
                                              ];
                                          } else {
                                              $data = [
                                                  'error' => 3,
                                              ];
                                          }
                                      } else {
                                        $data = [
                                            'error' => 1,
                                        ];
                                      }
                                  }
                              } else {
                                  $start_date = $promotion->start_date;
                                  $end_date = $promotion->end_date;
                                  $check_date = $this->check_date_promotion($start_date, $end_date);
                                  if ( $check_date ) {
                                      if ($promotion->max_uses > $promotion->uses) {
                                          $data = [
                                              'error' => 0,
                                              'type' => $promotion->type,
                                              'value' => $promotion->value,
                                          ];
                                      } else {
                                          $data = [
                                              'error' => 3,
                                          ];
                                      }
                                  } else {
                                    $data = [
                                        'error' => 1,
                                    ];
                                  }
                              }
                          } else {
                              $data = [
                                'error' => 5,
                              ];
                          }
                      }
                  }
              }
              else if ( $promotion->billing_promotions->count() > 0 ) {
                  $check_billing = false;
                  foreach ($promotion->billing_promotions as $key => $billing_promotion) {
                      if ($billing_promotion->billing == $billing_cycle) {
                        $check_billing = true;
                      }
                  }
                  if ($check_billing) {
                      if ($promotion->life_time) {
                          if ( $promotion->apply_one ) {
                              $promotion_meta = $this->promotion_meta->where('user_id', Auth::user()->id)->where('promotion_id',$promotion_id)->first();
                              if ( !empty($promotion_meta) ) {
                                  $data = [
                                      'error' => 2,
                                  ];
                              } else {
                                  $start_date = $promotion->start_date;
                                  $end_date = $promotion->end_date;
                                  $check_date = $this->check_date_promotion($start_date, $end_date);
                                  if ( $check_date ) {
                                      if ($promotion->max_uses > $promotion->uses) {
                                          $data = [
                                              'error' => 0,
                                              'type' => $promotion->type,
                                              'value' => $promotion->value,
                                          ];
                                      } else {
                                          $data = [
                                              'error' => 3,
                                          ];
                                      }
                                  } else {
                                    $data = [
                                        'error' => 1,
                                    ];
                                  }
                              }
                          } else {
                              $start_date = $promotion->start_date;
                              $end_date = $promotion->end_date;
                              $check_date = $this->check_date_promotion($start_date, $end_date);
                              if ( $check_date ) {
                                  if ($promotion->max_uses > $promotion->uses) {
                                      $data = [
                                          'error' => 0,
                                          'type' => $promotion->type,
                                          'value' => $promotion->value,
                                      ];
                                  } else {
                                      $data = [
                                          'error' => 3,
                                      ];
                                  }
                              } else {
                                $data = [
                                    'error' => 1,
                                ];
                              }
                          }
                      }
                      else {
                          if ( $promotion->type_order ==  $type_order ) {
                              if ( $promotion->apply_one ) {
                                  $promotion_meta = $this->promotion_meta->where('user_id', Auth::user()->id)->where('promotion_id',$promotion_id)->first();
                                  if ( !empty($promotion_meta) ) {
                                      $data = [
                                          'error' => 2,
                                      ];
                                  } else {
                                      $start_date = $promotion->start_date;
                                      $end_date = $promotion->end_date;
                                      $check_date = $this->check_date_promotion($start_date, $end_date);
                                      if ( $check_date ) {
                                          if ($promotion->max_uses > $promotion->uses) {
                                              $data = [
                                                  'error' => 0,
                                                  'type' => $promotion->type,
                                                  'value' => $promotion->value,
                                              ];
                                          } else {
                                              $data = [
                                                  'error' => 3,
                                              ];
                                          }
                                      } else {
                                        $data = [
                                            'error' => 1,
                                        ];
                                      }
                                  }
                              } else {
                                  $start_date = $promotion->start_date;
                                  $end_date = $promotion->end_date;
                                  $check_date = $this->check_date_promotion($start_date, $end_date);
                                  if ( $check_date ) {
                                      if ($promotion->max_uses > $promotion->uses) {
                                          $data = [
                                              'error' => 0,
                                              'type' => $promotion->type,
                                              'value' => $promotion->value,
                                          ];
                                      } else {
                                          $data = [
                                              'error' => 3,
                                          ];
                                      }
                                  } else {
                                    $data = [
                                        'error' => 1,
                                    ];
                                  }
                              }
                          } else {
                              $data = [
                                'error' => 5,
                              ];
                          }
                      }
                  }
              }
          }
          else {
              if ($promotion->life_time) {
                  if ( $promotion->apply_one ) {
                      $promotion_meta = $this->promotion_meta->where('user_id', Auth::user()->id)->where('promotion_id',$promotion_id)->first();
                      if ( !empty($promotion_meta) ) {
                          $data = [
                              'error' => 2,
                          ];
                      } else {
                          $start_date = $promotion->start_date;
                          $end_date = $promotion->end_date;
                          $check_date = $this->check_date_promotion($start_date, $end_date);
                          if ( $check_date ) {
                              if ($promotion->max_uses > $promotion->uses) {
                                  $data = [
                                      'error' => 0,
                                      'type' => $promotion->type,
                                      'value' => $promotion->value,
                                  ];
                              } else {
                                  $data = [
                                      'error' => 3,
                                  ];
                              }
                          } else {
                            $data = [
                                'error' => 1,
                            ];
                          }
                      }
                  } else {
                      $start_date = $promotion->start_date;
                      $end_date = $promotion->end_date;
                      $check_date = $this->check_date_promotion($start_date, $end_date);
                      if ( $check_date ) {
                          if ($promotion->max_uses > $promotion->uses) {
                              $data = [
                                  'error' => 0,
                                  'type' => $promotion->type,
                                  'value' => $promotion->value,
                              ];
                          } else {
                              $data = [
                                  'error' => 3,
                              ];
                          }
                      } else {
                        $data = [
                            'error' => 1,
                        ];
                      }
                  }
              }
              else {
                  if ( $promotion->type_order ==  $type_order ) {
                      if ( $promotion->apply_one ) {
                          $promotion_meta = $this->promotion_meta->where('user_id', Auth::user()->id)->where('promotion_id',$promotion_id)->first();
                          if ( !empty($promotion_meta) ) {
                              $data = [
                                  'error' => 2,
                              ];
                          } else {
                              $start_date = $promotion->start_date;
                              $end_date = $promotion->end_date;
                              $check_date = $this->check_date_promotion($start_date, $end_date);
                              if ( $check_date ) {
                                  if ($promotion->max_uses > $promotion->uses) {
                                      $data = [
                                          'error' => 0,
                                          'type' => $promotion->type,
                                          'value' => $promotion->value,
                                      ];
                                  } else {
                                      $data = [
                                          'error' => 3,
                                      ];
                                  }
                              } else {
                                $data = [
                                    'error' => 1,
                                ];
                              }
                          }
                      } else {
                          $start_date = $promotion->start_date;
                          $end_date = $promotion->end_date;
                          $check_date = $this->check_date_promotion($start_date, $end_date);
                          if ( $check_date ) {
                              if ($promotion->max_uses > $promotion->uses) {
                                  $data = [
                                      'error' => 0,
                                      'type' => $promotion->type,
                                      'value' => $promotion->value,
                                  ];
                              } else {
                                  $data = [
                                      'error' => 3,
                                  ];
                              }
                          } else {
                            $data = [
                                'error' => 1,
                            ];
                          }
                      }
                  } else {
                      $data = [
                        'error' => 5,
                      ];
                  }
              }

          }
        } else {
          $data = [
            'error' => 5,
          ];
        }
        return $data;
    }

    public function check_date_promotion($start_date, $end_date)
    {
        $start_date = new Carbon($start_date);
        $end_date = new Carbon($end_date);
        $check = false;
        if ( $start_date->isToday() ) {
          $check = true;
        }
        elseif ( $end_date->isToday() ) {
          $check = true;
        }
        elseif ( $start_date->isPast() ) {
           if ( ! $end_date->isPast() ) {
             $check = true;
           }
        }
        return $check;
    }

    public function update_profile($data)
    {
        try {
          $result = [
              'fail' => 'Chỉnh sửa thông tin thất bại',
          ];
          $data_user = [
              'name' => $data['name'],
              // 'password' => !empty($data['password']) ? $data['password'] : '',
          ];
          $update_user = $this->user->find(Auth::user()->id)->update($data_user);

          $user_meta = $this->user_meta->where('user_id', Auth::user()->id)->first();
          if (isset($user_meta)) {
              if (!empty($data['avatar'])) {
                  $avatar = $data['avatar'];
                  $path = Storage::disk('public')->put('avatars/' . Auth::user()->id, $avatar);
              } else {
                  $avatar  = $user_meta->avatar;
              }

              //thêm chứng minh nhân dân
              if (!empty($data['cmnd_after'])) {
                  $cmnd_after = preg_replace('/\s+/', '', $data['cmnd_after']->getClientOriginalName());
                  $file_name_after = date('d-m-Y').'-'.'scan-after-' . $cmnd_after;
                  $path1 = $data['cmnd_after']->storeAs('public/cmnd/' . Auth::user()->id, $file_name_after);
              } else {
                    $cmnd_after  = $user_meta->cmnd_after;
              }
              if (!empty($data['cmnd_before'])) {
                  $cmnd_before = preg_replace('/\s+/', '', $data['cmnd_before']->getClientOriginalName());
                  $file_name_before = date('d-m-Y').'-'.'scan-before-' . $cmnd_before;
                  $path2 = $data['cmnd_before']->storeAs('public/cmnd/' . Auth::user()->id, $file_name_before);
              } else {
                    $cmnd_before  = $user_meta->cmnd_before;
              }
              $path_connect = 'storage/cmnd/' . Auth::user()->id . '/';

              $data_user_meta = [
                  'date' => !empty($data['date']) ? $data['date'] : '',
                  'address' => !empty($data['address']) ? $data['address'] : '',
                  'phone'  => !empty($data['phone']) ? $data['phone'] : '',
                  'gender' => !empty($data['gender']) ? $data['gender'] : '',
                  'avatar' => !empty($path) ? $path : $user_meta->avatar,
                  'cmnd' => !empty($data['cmnd']) ? $data['cmnd'] : '',
              ];
              if ( !empty($path1) || !empty($path2)) {
                  $data_cmnd = [
                      'user_id' => Auth::user()->id,
                      'cmnd_after' => !empty($path1) ? $path_connect.$file_name_after : $user_meta->cmnd_after,
                      'cmnd_before' => !empty($path2) ? $path_connect.$file_name_before : $user_meta->cmnd_before,
                  ];
                  $create_cmnd = $this->cmnd_verify->create($data_cmnd);
              }

              $update_user_meta = $user_meta->update($data_user_meta);

              if ($update_user && $update_user_meta && !empty($create_cmnd)) {
                  return $results = [
                      'success' => 'Chỉnh sửa thông tin thành công',
                      'warning' => 'Thông tin chứng minh nhân dân của quí khách đang được kiểm duyệt',
                  ];
              } elseif ($update_user && $update_user_meta) {
                  return $result = [
                      'success' => 'Chỉnh sửa thông tin thành công',
                  ];
              } else {
                return $result = [
                    'fail' => 'Chỉnh sửa thông tin thất bại',
                ];
              }
          } else {
              return $result = [
                  'fail' => 'Chỉnh sửa thông tin thất bại',
              ];
          }
          return $result;
        } catch (\Exception $e) {
          report($e);
          return $result = [
              'fail' => 'Chỉnh sửa thông tin thất bại',
          ];
        }

    }

    public function update_pass($data)
    {
        $data_user = [
            'password' => $data['new_password']
        ];
        $update_pass = $this->user->find(Auth::user()->id)->update($data_user);
        if ($update_pass) {
            return true;
        } else {
            return false;
        }
        return true;
    }

    public function detail_product($product_id)
    {
        $product = $this->product->find($product_id);
        return $product;
    }

    public function total_order_server($request)
    {
        $product = $this->product->find($request['product_id']);
        if ( !empty( $product->pricing[ $request['billing'] ] ) ) {
            $product_disk_2 = $this->product->find($request['disk2']);
            $amount_disk2 = !empty( $product_disk_2->pricing[ $request['billing'] ] ) ? $product_disk_2->pricing[ $request['billing'] ] : 0;
            // disk3
            $product_disk_3 = $this->product->find($request['disk3']);
            $amount_disk3 = !empty( $product_disk_3->pricing[ $request['billing'] ] ) ? $product_disk_3->pricing[ $request['billing'] ] : 0;
            // disk4
            $product_disk_4 = $this->product->find($request['disk4']);
            $amount_disk4 = !empty( $product_disk_4->pricing[ $request['billing'] ] ) ? $product_disk_4->pricing[ $request['billing'] ] : 0;
            // disk5
            $product_disk_5 = $this->product->find($request['disk5']);
            $amount_disk5 = !empty( $product_disk_5->pricing[ $request['billing'] ] ) ? $product_disk_5->pricing[ $request['billing'] ] : 0;
            // disk6
            $product_disk_6 = $this->product->find($request['disk6']);
            $amount_disk6 = !empty( $product_disk_6->pricing[ $request['billing'] ] ) ? $product_disk_6->pricing[ $request['billing'] ] : 0;
            // disk4
            $product_disk_7 = $this->product->find($request['disk7']);
            $amount_disk7 = !empty( $product_disk_7->pricing[ $request['billing'] ] ) ? $product_disk_7->pricing[ $request['billing'] ] : 0;
            // disk4
            $product_disk_8 = $this->product->find($request['disk8']);
            $amount_disk8 = !empty( $product_disk_8->pricing[ $request['billing'] ] ) ? $product_disk_8->pricing[ $request['billing'] ] : 0;
            // addon ram
            $amount_addon_ram = 0;
            $addon_ram = '';
            if ( !empty($request['addon_ram']) ) {
              $addon_ram = $this->product->find($request['addon_ram']);
              $amount_addon_ram = !empty( $addon_ram->pricing[ $request['billing'] ] ) ? $addon_ram->pricing[ $request['billing'] ] : 0;
            }
            // addon ram
            $addon_ip = '';
            $amount_addon_ip = 0;
            if (!empty($request['addon_ip'])) {
              $addon_ip = $this->product->find($request['addon_ip']);
              $amount_addon_ip = !empty( $addon_ip->pricing[ $request['billing'] ] ) ? $addon_ip->pricing[ $request['billing'] ] : 0;
            }
            // dd($addon_ram);
            //product
            $amount = $product->pricing[ $request['billing'] ];

            $total = $amount * $request['qtt'] + $amount_disk3 * $request['qtt'] + $amount_disk4 * $request['qtt'] + $amount_disk5 * $request['qtt']
            + $amount_disk6 * $request['qtt'] + $amount_disk7 * $request['qtt'] + $amount_disk8 * $request['qtt'] + $amount_disk2 * $request['qtt']
            + $amount_addon_ram * $request['qtt'] + $amount_addon_ip * $request['qtt'];
            return [
                'error' => 0,
                'total' => $total,
                'amount' => $amount,
                'message' => '',
                'disk2' => [
                    'name' => !empty($product_disk_2->name) ? $product_disk_2->name : '',
                    'pricing' => $amount_disk2,
                ],
                'disk3' => [
                    'name' => !empty($product_disk_3->name) ? $product_disk_3->name : '',
                    'pricing' => $amount_disk3,
                ],
                'disk4' => [
                    'name' => !empty($product_disk_4->name) ? $product_disk_4->name : '',
                    'pricing' => $amount_disk4,
                ],
                'disk5' => [
                    'name' => !empty($product_disk_5->name) ? $product_disk_5->name : '',
                    'pricing' => $amount_disk5,
                ],
                'disk6' => [
                    'name' => !empty($product_disk_6->name) ? $product_disk_6->name : '',
                    'pricing' => $amount_disk6,
                ],
                'disk7' => [
                    'name' => !empty($product_disk_7->name) ? $product_disk_7->name : '',
                    'pricing' => $amount_disk7,
                ],
                'disk8' => [
                    'name' => !empty($product_disk_8->name) ? $product_disk_8->name : '',
                    'pricing' => $amount_disk8,
                ],
                'addon_ram' => [
                    'name' => !empty($addon_ram->name) ? $addon_ram->name : '',
                    'pricing' => $amount_addon_ram,
                ],
                'addon_ip' => [
                    'name' => !empty($addon_ip->name) ? $addon_ip->name : '',
                    'pricing' => $amount_addon_ip,
                ],
                'list_add_on_disk_server' => $this->list_add_on_disk_server(),
                'list_add_on_disk_ram' => $this->list_add_on_disk_ram(),
                'list_add_on_ip_server' => $this->list_add_on_ip(),
            ];
        }
        else {
            return [
                'error' => 1,
                'total' => 0,
                'amount' => 0,
                'message' => 'Sản phẩm và thời gian thanh toán không trùng khớp, Quý khách vui lòng kiểm tra lại'
            ];
        }
    }

    public function total_order_colocation($request)
    {
      $product = $this->product->find($request['product_id']);
      if ( !empty( $product->pricing[ $request['billing'] ] ) ) {
        // sp addon ip
        $product_addon_ip = $this->product->find($request['addon_ip']);
        $amount_addon_ip = !empty( $product_addon_ip->pricing[ $request['billing'] ] ) ? $product_addon_ip->pricing[ $request['billing'] ] : 0;
        // tien sp
        $amount = $product->pricing[ $request['billing'] ];
        $total = $amount * $request['qtt'] + $amount_addon_ip * $request['qtt'];
        return [
          'error' => 0,
          'total' => $total,
          'amount' => $amount,
          'message' => '',
          'addon_ip' => [
              'name' => !empty($product_addon_ip->name) ? $product_addon_ip->name : '',
              'pricing' => $amount_addon_ip,
          ],
          'list_addon_ip' => $this->list_addon_ip_colo($request['billing']),
        ];
      }
      else {
          return [
              'error' => 1,
              'total' => 0,
              'amount' => 0,
              'message' => 'Sản phẩm và thời gian thanh toán không trùng khớp, Quý khách vui lòng kiểm tra lại'
          ];
      }
    }

    public function list_addon_ip_colo($billing_cycle)
    {
      $user = $this->user->find(Auth::id());
        $group_products = $this->group_product->where('group_user_id', $user->group_user_id)->where('hidden', false)->where('type', 'addon_colo')->get();
        $data = [];
        $billings = config('billing');
        foreach ($group_products as $key => $group_product) {
            foreach ($group_product->products as $key => $product) {
                if ( $product->meta_product->type_addon == 'addon_ip' ) {
                    $product->text_pricing = !empty($product->pricing[$billing_cycle]) ? $product->pricing[$billing_cycle] : 0;
                    $product->text_billing_cycle = !empty($billings[$billing_cycle]) ? $billings[$billing_cycle] : '1 Tháng';
                    $data[] = $product;
                }
            }
        }
        return $data;
    }
//    Tổng số product dang co
    public function total_product($user_id) {
        $total_vps = $this->vps->where('user_id', Auth::user()->id)
              ->where(function($query)
              {
                  return $query
                    ->orwhere('status_vps', 'on')
                    ->orWhere('status_vps', 'off')
                    ->orWhere('status_vps', 'admin_off')
                    ->orWhere('status_vps', 'progressing')
                    ->orWhere('status_vps', 'change_ip')
                    ->orWhere('status_vps', 'rebuild');
              })->count();
        $total_server = $this->server->where('user_id', $user_id)->count();
        $total_hosting = $this->hosting->where('user_id', Auth::user()->id)
                         ->where('date_create', '!=', '')
                         ->where(function($query)
                             {
                                 return $query
                                   ->orwhere('status_hosting', 'on')
                                   ->orWhere('status_hosting', 'off');
                             })->count();
        return $total_vps + $total_hosting + $total_server;
    }
//    Tổng số hóa đơn chưa thanh toán
    public function total_invoice_pending($user_id) {
        $total = $this->detail_order->where('user_id', $user_id)->where('status', 'unpaid')->count();
        return $total;
    }
//  Tổng số server
    public function total_server($user_id) {
        $total_server = $this->server->where('user_id', $user_id)->count();
        return $total_server;
    }
    //  Tổng số vps
    public function total_vps($user_id) {
        $total_vps = $this->vps->where('user_id', $user_id)->count();
        return $total_vps;
    }
    //  Tổng số server
    public function total_hosting($user_id) {
        $total_hosting = $this->hosting->where('user_id', $user_id)->count();
        return $total_hosting;
    }
//  Tổng số server
    public function get_total_sevices() {
        $user_id = Auth::user()->id;
        $data = [];
        $total_server = $this->server->where('user_id', $user_id)->orderBy('id', 'desc')->get();
        $total_vps = $this->vps->where('user_id', Auth::user()->id)
                          ->where(function($query)
                          {
                              return $query
                                ->orwhere('status_vps', 'on')
                                ->orWhere('status_vps', 'off')
                                ->orWhere('status_vps', 'admin_off')
                                ->orWhere('status_vps', 'progressing')
                                ->orWhere('status_vps', 'change_ip')
                                ->orWhere('status_vps', 'rebuild');
                          })->get();
        $total_hosting = $this->hosting->where('user_id', Auth::user()->id)
                             ->where('date_create', '!=', '')
                             ->where(function($query)
                                 {
                                     return $query
                                       ->orwhere('status_hosting', 'on')
                                       ->orWhere('status_hosting', 'off');
                                 })->get();
        foreach ($total_vps as $vps) {
            $data[] = $vps;
        }
        foreach ($total_hosting as $hosting) {
            $data[] = $hosting;
        }
        foreach ($total_server as $server) {
            $data[] = $server;
        }
        if (isset($data)) {
            foreach ($data as $key => $value) {
                for ($i=1; $i < count($data) - 1; $i++) {
                    if (strtotime($data[$i]['created_at']) < strtotime($data[$key]['created_at'])) {
                        $t = $data[$i];
                        $data[$i] = $data[$key];
                        $data[$key] = $t;
                    }
                }
            }
        }

        return $data;
    }
//    Tổng số hóa đơn hết hạn
    public function get_invoice_pending_home() {
        $invoices = $this->detail_order->where('user_id', Auth::user()->id)->get();
        $qtt = 0;
        if (!empty($invoices)) {
            foreach ($invoices as $key => $invoice) {
                if ($invoice->order->type = 'order' && $invoice->type = 'VPS') {
                    $list_vps = $invoice->vps;
                    foreach ($list_vps as $key => $vps) {
                        if (!empty($vps->next_due_date)) {
                            $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
                            $next_due_date = new Carbon($vps->next_due_date);
                            if ( $next_due_date->diffInDays($now) <= 7 ) {
                                $qtt++;
                            }
                        }
                    }
                }
            }
        }
        return $qtt;
    }

    /*
    * Order VPS
    */
    public function order_vps($data)
    {
        // dd($data);
        //create order
        $product = $this->product->find($data['product_id']);
        $add_on = !empty($data['addon'])? 1 : '';
        $sub_total = $product->pricing[$data['billing']];
        $total = $product->pricing[$data['billing']] * $data['quantity'];
        // dd($sub_total, $total);
        if (!empty($add_on)) {
            $product_addon_cpu = $this->product->find($data['id-addon-cpu']);
            $product_addon_ram = $this->product->find($data['id-addon-ram']);
            $product_addon_disk = $this->product->find($data['id-addon-disk']);
            if ( $data['addon_cpu'] == 0  && $data['addon_ram'] == 0 && $data['addon_disk'] ==0 ) {
              $add_on = 0;
            }
            // TInh total va subtotal
            if ($data['addon_cpu'] > 0) {
                $sub_total += $product_addon_cpu->pricing[$data['billing']] * $data['addon_cpu'];
                $total += $product_addon_cpu->pricing[$data['billing']] * $data['addon_cpu'] * $data['quantity'];
            }
            // dd($sub_total, $total,$data['addon_ram']);
            if ($data['addon_ram'] > 0) {
                $sub_total += $product_addon_ram->pricing[$data['billing']] * $data['addon_ram'];
                $total += $product_addon_ram->pricing[$data['billing']] * $data['addon_ram'] * $data['quantity'];
            }
            // dd($sub_total, $total,$data['addon_ram']);
            if ($data['addon_disk'] > 0) {
                $sub_total += $product_addon_disk->pricing[$data['billing']] * ($data['addon_disk']/10);
                $total += $product_addon_disk->pricing[$data['billing']] * ($data['addon_disk']/10) * $data['quantity'];
            }
            // dd($sub_total, $total,$data['addon_disk']/10);
        }
        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            'promotion_id' => !empty($promotion->id) ? $promotion->id : null,
            // 'makh' => $data['makh'],
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => $data['type_product'],
            'due_date' => $date,
            'description' => !empty($data['description'])? $data['description'] : '',
            'status' => 'unpaid',
            'sub_total' => $sub_total,
            'quantity' => $data['quantity'],
            'user_id' => Auth::user()->id,
            'addon_id' => !empty($data['addon'])? '1' : '0',
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODV' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '2',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);
        // vetify order
        $data_vetify_order = [
            'order_id' => $order->id,
            'token' => sha1(time()),
        ];
        $vetify = $this->order_vetify->create($data_vetify_order);
        //create vps
        if ($detail_order && $data_order) {
            for ($i=0; $i < $data['quantity']; $i++) {
                $data_vps = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' =>  Auth::user()->id,
                    'product_id' => $data['product_id'],
                    'addon_id' => !empty($data['addon'])? $data['addon'] : '0',
                    'ip' => !empty($data['ip'])? $data['ip'] : '',
                    'os' => $data['os'],
                    'billing_cycle' => $data['billing'],
                    'status' => 'Pending',
                    'user' => !empty($data['user'])? $data['user'] : '',
                    'password' => !empty($data['password'])? $data['password'] : '',
                    'addon_qtt' => !empty($data['qtt_addon']) ? $data['qtt_addon'] : '0',
                    'security' => !empty($data['security']) ? true : false,
                    // 'description' => $data['description'],
                    // 'ma_kh' => $data['makh'],
                ];
                $create_vps = $this->vps->create($data_vps);

                if(!$create_vps) {
                    return false;
                } else {
                    if (!empty($data['addon'])) {
                        if ($data['addon_ram'] > 0 || $data['addon_cpu'] > 0 || $data['addon_disk'] > 0  ) {
                            $data_vps_config = [
                                'vps_id' => $create_vps->id,
                                'ip' => '0',
                                'cpu' => !empty($data['addon_cpu']) ? $data['addon_cpu'] : 0,
                                'ram' => !empty($data['addon_ram']) ? $data['addon_ram'] : 0,
                                'disk' => !empty($data['addon_disk']) ? $data['addon_disk'] : 0,
                            ];
                            $this->vps_config->create($data_vps_config);
                        }
                    }
                }
            }
        } else {
            return false;
        }
        $type_os = $data['os'];
        if ($type_os == 1) {
            $type_os = 'Windows 10 64bit';
        }
        elseif ($type_os == 2 || $type_os == 30) {
            $type_os = 'Windows Server 2012 R2';
        }
        elseif ($type_os == 3) {
            $type_os = 'Windows Server 2016';
        }
        elseif ($type_os == 31) {
            $type_os = 'Linux Ubuntu-20.04';
        }
        elseif ($type_os == 15) {
            $type_os = 'Windows Server 2019';
        }
        else {
            $type_os = 'Linux CentOS 7 64bit';
        }
        $billings = config('billing');
        // Tạo log
        if ($product->group_product->private == 0) {
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'đặt hàng',
            'model' => 'User/Order',
            'description' => $data['quantity'] . ' VPS <a target="_blank" href="/admin/products/edit/' . $product->id . '">' . $product->name . '</a> ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']],
            'description_user' => $data['quantity'] . '  ' . $product->name . '  ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']],
          ];
        } else {
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'đặt hàng',
            'model' => 'User/Order',
            'description' => $data['quantity'] . ' VPS <a target="_blank" target="_blank" href="/admin/product_privates/edit/' . $product->id . '">' . $product->name . '</a> ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']],
            'description_user' => $data['quantity'] . '  ' . $product->name . '  ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']],
          ];
        }
        $this->log_activity->create($data_log);

        return $detail_order;
    }
    /*
    * Order VPS US
    */
    public function order_vps_us($data)
    {
        // dd($data);
        //create order
        $product = $this->product->find($data['product_id']);
        $add_on = !empty($data['addon'])? 1 : '';
        $sub_total = $product->pricing[$data['billing']];
        $qtt = $data['quantity'];
        if ( !empty($data['add_state']) ) {
            foreach ($data['add_state_quantity'] as $key => $add_qtt) {
                $qtt += $add_qtt;
            }
        }
        $total = $product->pricing[$data['billing']] * $qtt;

        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            // 'description' => $data['description'],
            'promotion_id' => !empty($promotion->id) ? $promotion->id : null,
            // 'makh' => $data['makh'],
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => $data['type_product'],
            'due_date' => $date,
            'description' => !empty($data['description'])? $data['description'] : '',
            'status' => 'unpaid',
            'sub_total' => $sub_total,
            'quantity' => $qtt,
            'user_id' => Auth::user()->id,
            'addon_id' => 0,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODVU' . strtoupper(substr(sha1(time()), 35, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '2',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);
        // vetify order
        $data_vetify_order = [
            'order_id' => $order->id,
            'token' => sha1(time()),
        ];
        $vetify = $this->order_vetify->create($data_vetify_order);
        //create vps
        if ($detail_order && $data_order) {
            for ($i=0; $i < $data['quantity']; $i++) {
                $data_vps = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' =>  Auth::user()->id,
                    'product_id' => $data['product_id'],
                    'addon_id' => !empty($data['addon'])? $data['addon'] : '0',
                    'ip' => !empty($data['ip'])? $data['ip'] : '',
                    'os' => $data['os'],
                    'billing_cycle' => $data['billing'],
                    'status' => 'Pending',
                    'user' => !empty($data['user'])? $data['user'] : '',
                    'password' => !empty($data['password'])? $data['password'] : '',
                    'addon_qtt' => 0,
                    'security' => true,
                    // 'description' => $data['description'],
                    // 'ma_kh' => $data['makh'],
                    'state' => $data['state'],
                    'location' => 'us',
                ];
                $create_vps = $this->vps->create($data_vps);

                if(!$create_vps) {
                    return false;
                }
            }
            if ( !empty($data['add_state']) ) {
                foreach ($data['add_state_type_country'] as $key => $type_country) {
                    if ( $type_country != 'US' ) {
                        for ($i=0; $i < $data['add_state_quantity'][$key]; $i++) {
                            $data_vps = [
                                'detail_order_id' => $detail_order->id,
                                'user_id' =>  Auth::user()->id,
                                'product_id' => $data['product_id'],
                                'addon_id' => !empty($data['addon'])? $data['addon'] : '0',
                                'ip' => !empty($data['ip'])? $data['ip'] : '',
                                'os' => $data['os'],
                                'billing_cycle' => $data['billing'],
                                'status' => 'Pending',
                                'addon_qtt' => 0,
                                'security' => true,
                                // 'description' => $data['description'],
                                // 'ma_kh' => $data['makh'],
                                'state' => $type_country,
                                'location' => 'us',
                            ];
                            $create_vps = $this->vps->create($data_vps);

                            if(!$create_vps) {
                                return false;
                            }
                        }
                    } else {
                        for ($i=0; $i < $data['add_state_quantity'][$key]; $i++) {
                            $data_vps = [
                                'detail_order_id' => $detail_order->id,
                                'user_id' =>  Auth::user()->id,
                                'product_id' => $data['product_id'],
                                'addon_id' => !empty($data['addon'])? $data['addon'] : '0',
                                'ip' => !empty($data['ip'])? $data['ip'] : '',
                                'os' => $data['os'],
                                'billing_cycle' => $data['billing'],
                                'status' => 'Pending',
                                'addon_qtt' => 0,
                                'security' => true,
                                // 'description' => $data['description'],
                                // 'ma_kh' => $data['makh'],
                                'state' => $data['add_state_name'][$key],
                                'location' => 'us',
                            ];
                            $create_vps = $this->vps->create($data_vps);

                            if(!$create_vps) {
                                return false;
                            }
                        }
                    }
                }
            }
        } else {
            return false;
        }
        $type_os = 'Windows Server 2012 R2';
        $billings = config('billing');
        // Tạo log
        if ($product->group_product->private == 0) {
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'đặt hàng',
            'model' => 'User/Order',
            'description' => $data['quantity'] . ' VPS <a target="_blank" href="/admin/products/edit/' . $product->id . '">' . $product->name . '</a> ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']],
            'description_user' => $data['quantity'] . '  ' . $product->name . '  ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']],
          ];
        } else {
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'đặt hàng',
            'model' => 'User/Order',
            'description' => $data['quantity'] . ' VPS <a target="_blank" target="_blank" href="/admin/product_privates/edit/' . $product->id . '">' . $product->name . '</a> ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']],
            'description_user' => $data['quantity'] . '  ' . $product->name . '  ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']],
          ];
        }
        $this->log_activity->create($data_log);

        return $detail_order;
    }
    /*
    * Order NAT VPS
    */
    public function order_nat_vps($data)
    {
        // dd($data);
        /*
        * Order VPS
        */
        // dd('da den');
        //create order
        $product = $this->product->find($data['product_id']);
        $add_on = !empty($data['addon'])? 1 : '';
        $sub_total = $product->pricing[$data['billing']];
        $total = $product->pricing[$data['billing']] * $data['quantity'];
        // dd($sub_total, $total);
        if (!empty($add_on)) {
            $product_addon_cpu = $this->product->find($data['id-addon-cpu']);
            $product_addon_ram = $this->product->find($data['id-addon-ram']);
            $product_addon_disk = $this->product->find($data['id-addon-disk']);
            if ( $data['addon_cpu'] == 0  && $data['addon_ram'] == 0 && $data['addon_disk'] ==0 ) {
              $add_on = 0;
            }
            // TInh total va subtotal
            if ($data['addon_cpu'] > 0) {
                $sub_total += $product_addon_cpu->pricing[$data['billing']] * $data['addon_cpu'];
                $total += $product_addon_cpu->pricing[$data['billing']] * $data['addon_cpu'] * $data['quantity'];
            }
            // dd($sub_total, $total,$data['addon_ram']);
            if ($data['addon_ram'] > 0) {
                $sub_total += $product_addon_ram->pricing[$data['billing']] * $data['addon_ram'];
                $total += $product_addon_ram->pricing[$data['billing']] * $data['addon_ram'] * $data['quantity'];
            }
            // dd($sub_total, $total,$data['addon_ram']);
            if ($data['addon_disk'] > 0) {
                $sub_total += $product_addon_disk->pricing[$data['billing']] * ($data['addon_disk']/10);
                $total += $product_addon_disk->pricing[$data['billing']] * ($data['addon_disk']/10) * $data['quantity'];
            }
            // dd($sub_total, $total,$data['addon_disk']/10);
        }

        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            // 'description' => $data['description'],
            'promotion_id' => !empty($promotion->id) ? $promotion->id : null,
            // 'makh' => $data['makh'],
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => $data['type_product'],
            'due_date' => $date,
            'description' => !empty($data['description'])? $data['description'] : '',
            'status' => 'unpaid',
            'sub_total' => $sub_total,
            'quantity' => $data['quantity'],
            'user_id' => Auth::user()->id,
            'addon_id' => !empty($data['addon'])? '1' : '0',
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDONV' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '2',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);
        // vetify order
        $data_vetify_order = [
            'order_id' => $order->id,
            'token' => sha1(time()),
        ];
        $vetify = $this->order_vetify->create($data_vetify_order);
        //create vps
        if ($detail_order && $data_order) {
            for ($i=0; $i < $data['quantity']; $i++) {
                $data_vps = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' =>  Auth::user()->id,
                    'product_id' => $data['product_id'],
                    'addon_id' => !empty($data['addon'])? $data['addon'] : '0',
                    'ip' => !empty($data['ip'])? $data['ip'] : '',
                    'os' => $data['os'],
                    'billing_cycle' => $data['billing'],
                    'status' => 'Pending',
                    'user' => !empty($data['user'])? $data['user'] : '',
                    'password' => !empty($data['password'])? $data['password'] : '',
                    'addon_qtt' => !empty($data['qtt_addon']) ? $data['qtt_addon'] : '0',
                    'security' => !empty($data['security']) ? true : false,
                    // 'description' => $data['description'],
                    // 'ma_kh' => $data['makh'],
                    'type_vps' => 'nat_vps',
                ];
                $create_vps = $this->vps->create($data_vps);

                if(!$create_vps) {
                    return false;
                } else {
                    if (!empty($data['addon'])) {
                        if ($data['addon_ram'] > 0 || $data['addon_cpu'] > 0 || $data['addon_disk'] > 0  ) {
                            $data_vps_config = [
                                'vps_id' => $create_vps->id,
                                'ip' => '0',
                                'cpu' => !empty($data['addon_cpu']) ? $data['addon_cpu'] : 0,
                                'ram' => !empty($data['addon_ram']) ? $data['addon_ram'] : 0,
                                'disk' => !empty($data['addon_disk']) ? $data['addon_disk'] : 0,
                            ];
                            $this->vps_config->create($data_vps_config);
                        }
                    }
                }
            }
        } else {
            return false;
        }
        $type_os = $data['os'];
        if ($type_os == 1) {
            $type_os = 'Windows 10 64bit';
        }
        elseif ($type_os == 2 || $type_os == 30) {
            $type_os = 'Windows Server 2012 R2';
        }
        elseif ($type_os == 3) {
            $type_os = 'Windows Server 2016';
        }
        elseif ($type_os == 31) {
            $type_os = 'Linux Ubuntu-20.04';
        }
        elseif ($type_os == 15) {
            $type_os = 'Windows Server 2019';
        }
        else {
            $type_os = 'Linux CentOS 7 64bit';
        }

        $billings = config('billing');
        // Tạo log
        if ($product->group_product->private == 0) {
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'đặt hàng',
            'model' => 'User/Order',
            'description' => $data['quantity'] . ' NAT-VPS <a target="_blank" target="_blank" href="/admin/products/edit/' . $product->id . '">' . $product->name . '</a> ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']],
            'description_user' => $data['quantity'] . '  ' . $product->name . '  ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']],
          ];
        } else {
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'đặt hàng',
            'model' => 'User/Order',
            'description' => $data['quantity'] . ' NAT-VPS <a target="_blank" target="_blank" href="/admin/product_privates/edit/' . $product->id . '">' . $product->name . '</a> ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']],
            'description_user' => $data['quantity'] . '  ' . $product->name . '  ' . $type_os . ' ' . $billings[$data_vps['billing_cycle']],
          ];
        }
        $this->log_activity->create($data_log);

        return $detail_order;
    }
    /*
    * Order PROXY
    */
    public function check_proxy($qtt, $state)
    {
      $data_check = [
        'customer' => Auth::user()->customer_id,
        'port' => $qtt,
        'state' => $state,
      ];
      $check_proxy = $this->dashboard->check_qtt_proxy($data_check);
      return $check_proxy;
    }

    public function order_proxy($data)
    {
        $check_proxy = $this->check_proxy($data['quantity'], $data['state']);
        if ( $check_proxy->error ) {
          return [
            'error' => $check_proxy->error,
            'qtt' => $check_proxy->qtt,
          ];
        }
        // dd($check_proxy);
        //create order
        $product = $this->product->find($data['product_id']);
        $sub_total = $product->pricing[$data['billing']];
        $qtt = $data['quantity'];
        $total = $product->pricing[$data['billing']] * $qtt;

        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            // 'description' => $data['description'],
            'promotion_id' => !empty($promotion->id) ? $promotion->id : null,
            // 'makh' => $data['makh'],
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => $data['type_product'],
            'due_date' => $date,
            'description' => !empty($data['description'])? $data['description'] : '',
            'status' => 'unpaid',
            'sub_total' => $sub_total,
            'quantity' => $qtt,
            'user_id' => Auth::user()->id,
            'addon_id' => 0,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODP' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '2',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);
        //create vps
        if ($detail_order && $data_order) {
            for ($i=0; $i < $data['quantity']; $i++) {
                $data_proxy = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' =>  Auth::user()->id,
                    'product_id' => $data['product_id'],
                    'billing_cycle' => $data['billing'],
                    'status' => 'Pending',
                    'state' => $data['state'],
                    'location' => 'us',
                    'username' => !empty($data['username']) ? $data['username'] : '',
                    'password' => !empty($data['password']) ? $data['password'] : '',
                ];
                $create_proxy = $this->proxy->create($data_proxy);

                if(!$create_proxy) {
                    return false;
                }
            }
        } else {
            return false;
        }
        $billings = config('billing');
        // Tạo log
        if ($product->group_product->private == 0) {
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'đặt hàng',
            'model' => 'User/Order',
            'description' => $data['quantity'] . ' proxy <a target="_blank" href="/admin/products/edit/' . $product->id . '">' . $product->name . '</a> ' . $billings[$data_proxy['billing_cycle']],
            'description_user' => $data['quantity'] . '  ' . $product->name . ' ' . $billings[$data_proxy['billing_cycle']],
          ];
        } else {
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'đặt hàng',
            'model' => 'User/Order',
            'description' => $data['quantity'] . ' proxy <a target="_blank" target="_blank" href="/admin/product_privates/edit/' . $product->id . '">' . $product->name . '</a> ' . $billings[$data_proxy['billing_cycle']],
            'description_user' => $data['quantity'] . '  ' . $product->name . ' ' . $billings[$data_proxy['billing_cycle']],
          ];
        }
        $this->log_activity->create($data_log);

        return [
          'error' => 0,
          'detail_order' => $detail_order
        ];
    }
    /*
    * Order Server
    */
    public function order_server($data)
    {
        $user = $this->user->find(Auth::user()->id);
        $product = $this->product->find($data['product_id']);
        $email = $this->email->find($product->meta_product->email_create);
        // dd($product->meta_product, $email);
        //product
        $amount = $product->pricing[ $data['billing'] ];
        $total = $amount * $data['quantity'];
        $subtotal = $amount;
        // disk2
        if ( !empty($data['disk2']) ) {
          $product_disk_2 = $this->product->find($data['disk2']);
          $amount_disk2 = !empty( $product_disk_2->pricing[ $data['billing'] ] ) ? $product_disk_2->pricing[ $data['billing'] ] : 0;
          $total +=  $amount_disk2 * $data['quantity'];
          $subtotal += $amount_disk2;
        }
        // disk3
        if ( !empty($data['disk3']) ) {
          $product_disk_3 = $this->product->find($data['disk3']);
          $amount_disk3 = !empty( $product_disk_3->pricing[ $data['billing'] ] ) ? $product_disk_3->pricing[ $data['billing'] ] : 0;
          $total +=  $amount_disk3 * $data['quantity'];
          $subtotal += $amount_disk3;
        }
        // disk4
        if ( !empty($data['disk4']) ) {
          $product_disk_4 = $this->product->find($data['disk4']);
          $amount_disk4 = !empty( $product_disk_4->pricing[ $data['billing'] ] ) ? $product_disk_4->pricing[ $data['billing'] ] : 0;
          $total +=  $amount_disk4 * $data['quantity'];
          $subtotal += $amount_disk4;
        }
        // disk5
        if ( !empty($data['disk5']) ) {
          $product_disk_5 = $this->product->find($data['disk5']);
          $amount_disk5 = !empty( $product_disk_5->pricing[ $data['billing'] ] ) ? $product_disk_5->pricing[ $data['billing'] ] : 0;
          $total +=  $amount_disk5 * $data['quantity'];
          $subtotal += $amount_disk5;
        }
        // disk6
        if ( !empty($data['disk6']) ) {
          $product_disk_6 = $this->product->find($data['disk6']);
          $amount_disk6 = !empty( $product_disk_6->pricing[ $data['billing'] ] ) ? $product_disk_6->pricing[ $data['billing'] ] : 0;
          $total +=  $amount_disk6 * $data['quantity'];
          $subtotal += $amount_disk6;
        }
        // disk4
        if ( !empty($data['disk7']) ) {
          $product_disk_7 = $this->product->find($data['disk7']);
          $amount_disk7 = !empty( $product_disk_7->pricing[ $data['billing'] ] ) ? $product_disk_7->pricing[ $data['billing'] ] : 0;
          $total +=  $amount_disk7 * $data['quantity'];
          $subtotal += $amount_disk7;
        }
        // disk4
        if ( !empty($data['disk8']) ) {
          $product_disk_8 = $this->product->find($data['disk8']);
          $amount_disk8 = !empty( $product_disk_8->pricing[ $data['billing'] ] ) ? $product_disk_8->pricing[ $data['billing'] ] : 0;
          $total +=  $amount_disk8 * $data['quantity'];
          $subtotal += $amount_disk8;
        }
        // addon ram
        if ( !empty($data['addon_ram']) ) {
          $addon_ram = $this->product->find($data['addon_ram']);
          $amount_addon_ram = !empty( $addon_ram->pricing[ $data['billing'] ] ) ? $addon_ram->pricing[ $data['billing'] ] : 0;
          $total +=  $amount_addon_ram * $data['quantity'];
          $subtotal += $amount_addon_ram;
        }
        // addon ip
        if ( !empty($data['addon_ip']) ) {
          $addon_ip = $this->product->find($data['addon_ip']);
          $amount_addon_ip = !empty( $addon_ip->pricing[ $data['billing'] ] ) ? $addon_ip->pricing[ $data['billing'] ] : 0;
          $total +=  $amount_addon_ip * $data['quantity'];
          $subtotal += $amount_addon_ip;
        }

        $data_order = [
            'user_id' => Auth::id(),
            'total' => $total,
            'status' => 'Pending',
            'promotion_id' => !empty($promotion->id) ? $promotion->id : null,
        ];
        $order = $this->order->create($data_order);

        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => 'Server',
            'due_date' => $date,
            'description' => !empty($data['description'])? $data['description'] : '',
            'status' => 'unpaid',
            'sub_total' => $subtotal,
            'quantity' => $data['quantity'],
            'user_id' => Auth::id(),
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODS' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '2',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);
        for ($i=0; $i < $data['quantity']; $i++) {
          $data_server = [
            'detail_order_id' => $detail_order->id,
            'user_id' => $user->id,
            'type' => 'Server',
            'product_id' => $data['product_id'],
            'os' => $data['os'],
            'billing_cycle' => $data['billing'],
            'status' => 'Pending',
            'location' => $data['datacenter'],
            'raid' => $data['raid'],
            'server_management' => $data['server_management'],
          ];
          $server = $this->server->create($data_server);
          if ( !empty($data['disk3']) || !empty($data['disk4']) || !empty($data['disk5'])
            || !empty($data['disk6']) || !empty($data['disk7']) || !empty($data['disk8'])
            || !empty($data['addon_ram']) ) {
              $data_server_config = [
                'server_id' => $server->id,
                'disk2' => !empty($data['disk2']) ? $data['disk2'] : 0,
                'disk3' => !empty($data['disk3']) ? $data['disk3'] : 0,
                'disk4' => !empty($data['disk4']) ? $data['disk4'] : 0,
                'disk5' => !empty($data['disk5']) ? $data['disk5'] : 0,
                'disk6' => !empty($data['disk6']) ? $data['disk6'] : 0,
                'disk7' => !empty($data['disk7']) ? $data['disk7'] : 0,
                'disk8' => !empty($data['disk8']) ? $data['disk8'] : 0,
                'ram' => !empty($data['addon_ram']) ? 1 : 0,
                'ip' => !empty($data['addon_ip']) ? 1 : 0,
              ];
              $this->server_config->create($data_server_config);
              if ( !empty($data['addon_ram']) ) {
                $data_server_config_ram = [
                  'server_id' => $server->id,
                  'product_id' => $data['addon_ram'],
                ];
                $this->server_config_ram->create($data_server_config_ram);
              }
              if ( !empty($data['addon_ip']) ) {
                $data_server_config_ip = [
                  'server_id' => $server->id,
                  'product_id' => $data['addon_ip'],
                ];
                $this->server_config_ip->create($data_server_config_ip);
              }
          }
        }
        //SEND MAIL
        $billings = config('billing');
        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        // CPU
        $server_cpu = $product->meta_product->cpu . ' (' . $product->meta_product->cores . ')';
        // DISK
        $server_disk = $product->meta_product->disk . ' ';
        $server_disk .= $this->get_addon_disk_server($server->id);
        // RAM
        $server_ram = $product->meta_product->memory;
        if (!empty($data['addon_ram'])) {
          $product_addon_ram = $this->product->find($data['addon_ram']);
          $server_ram .= ' + Addon ' . $product_addon_ram->name . '';
        }
        // IP
        $server_ip = $product->meta_product->ip;
        if ( !empty($data['addon_ip']) ) {
          $product_addon_ip = $this->product->find($data['addon_ip']);
          $server_ip .= ' IP Public + Addon ' . $product_addon_ip->name;
        }
        $search = ['{$user_name}',  '{$user_email}' , '{$order_id}',  '{$product_name}', '{$billing_cycle}',
          '{$order_created_at}', '{$sub_total}', '{$total}', '{$qtt}', '{$server_os}', '{$url}', '{$server_cpu}', '{$server_ram}',
          '{$server_disk}', '{$server_qtt_ip}', '{$server_raid}', '{$server_datacenter}', '{$server_management}',
        ];
        $replace = [ $user->name,  $user->email , $order->id,  $product->name ,$billings[$server->billing_cycle], $order->created_at,
          number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' ,
          $detail_order->quantity, $data['os'], $url, $server_cpu, $server_ram, $server_disk,
          $server_ip, $data['raid'], $data['datacenter'], $data['server_management']
        ];
        $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
        try {
            $data = [
                'content' => $content,
                'user_name' => $user->name,
                'subject' => $email->meta_email->subject,
            ];
            $data_tb_send_mail = [
                'type' => 'mail_order_server',
                'type_service' => 'server',
                'user_id' => $user->id,
                'status' => false,
                'content' => serialize($data),
            ];
            $this->send_mail->create($data_tb_send_mail);
        } catch (\Exception $e) {
            report($e);
        }
        return $detail_order;
    }

    public function get_addon_disk_server($serverId)
    {
        $server = $this->server->find($serverId);
        if ( !empty($server->server_config) ) {
          $text_config = '';
          $server_config = $server->server_config;
          $data_config = [];
          if ( !empty($server_config->disk2) ) {
            $data_config[] = [
              'id' => $server_config->disk2,
              'name' => $server_config->product_disk2->name,
              'qtt' => 1
            ];
          }
          if ( !empty($server_config->disk3) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk3) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk3,
                'name' => $server_config->product_disk3->name,
                'qtt' => 1
              ];
            }
          }
          if ( !empty($server_config->disk4) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk4) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk4,
                'name' => $server_config->product_disk4->name,
                'qtt' => 1
              ];
            }
          }
          if ( !empty($server_config->disk5) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk5) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk5,
                'name' => $server_config->product_disk5->name,
                'qtt' => 1
              ];
            }
          }
          if ( !empty($server_config->disk6) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk6) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk6,
                'name' => $server_config->product_disk6->name,
                'qtt' => 1
              ];
            }
          }
          if ( !empty($server_config->disk7) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk7) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk7,
                'name' => $server_config->product_disk7->name,
                'qtt' => 1
              ];
            }
          }
          if ( !empty($server_config->disk8) ) {
            $check = true;
            foreach ($data_config as $key => $data) {
              if ($data['id'] == $server_config->disk8) {
                $data_config[$key]['qtt'] = $data['qtt'] + 1;
                $check = false;
              }
            }
            if ( $check ) {
              $data_config[] = [
                'id' => $server_config->disk8,
                'name' => $server_config->product_disk8->name,
                'qtt' => 1
              ];
            }
          }
          if ( count($data_config) ) {
            $text_config = '(Addon ';
            foreach ($data_config as $key => $data) {
              $text_config .= ( $data['qtt'] . ' x ' . $data['name']  );
              if ( count( $data_config ) - 1 > $key ) {
                $text_config .= ' - ';
              }
            }
            $text_config .= ')';
          }
          return $text_config;
        } else {
          return '';
        }
    }

    public function order_colocation($data)
    {
        $user = $this->user->find(Auth::user()->id);
        $product = $this->product->find($data['product_id']);
        $email = $this->email->find($product->meta_product->email_create);
        //product
        $amount = $product->pricing[ $data['billing'] ];
        $total = $amount * $data['quantity'];
        if ( !empty($data['addon_ip']) ) {
          $product_addon_ip = $this->product->find($data['addon_ip']);
          if ( isset($product_addon_ip) ) {
            $amount += $product_addon_ip->pricing[ $data['billing'] ];
            $total += $product_addon_ip->pricing[ $data['billing'] ] * $data['quantity'];
          }
        }
        $subtotal = $amount;
        $data_order = [
            'user_id' => Auth::id(),
            'total' => $total,
            'status' => 'Pending',
            'promotion_id' => !empty($promotion->id) ? $promotion->id : null,
        ];
        $order = $this->order->create($data_order);

        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => 'Colocation',
            'due_date' => $date,
            'description' => !empty($data['description'])? $data['description'] : '',
            'status' => 'unpaid',
            'sub_total' => $subtotal,
            'quantity' => $data['quantity'],
            'user_id' => Auth::id(),
            'addon_id' => !empty($data['addon_ip']) ? true : false,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODC' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '2',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);
        for ($i=0; $i < $data['quantity']; $i++) {
          $data_colocation = [
            'detail_order_id' => $detail_order->id,
            'user_id' => $user->id,
            'type_colo' => !empty($product->meta_product->os) ? $product->meta_product->os : '1 U',
            'product_id' => $data['product_id'],
            'billing_cycle' => $data['billing'],
            'status' => 'Pending',
            'location' => $data['datacenter'],
          ];
          $colocation = $this->colocation->create($data_colocation);
          if (!empty($data['addon_ip'])) {
            $product_addon_ip = $this->product->find($data['addon_ip']);
            if ( isset($product_addon_ip) ) {
              $data_config = [
                'colocation_id' => $colocation->id,
                'disk' => 0, 
                'ram' => 0, 
                'cpu' => 0, 
                'ip' => $product_addon_ip->meta_product->ip
              ];
              $this->colocation_config->create($data_config);
              $data_config_ip = [
                'colocation_id' => $colocation->id,
                'product_id' => $product_addon_ip->id,
              ];
              $this->colocation_config_ip->create($data_config_ip);
            }
          }
        }
        // SEND MAIL
        $billings = config('billing');
        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $colocation_ip = $product->meta_product->ip . ' IP Public';
        if ( !empty($data['addon_ip']) ) {
          $product_addon_ip = $this->product->find($data['addon_ip']);
          $colocation_ip .= ' + Addon ' . $product_addon_ip->meta_product->ip . ' IP Public';
        }
        $search = ['{$user_name}',  '{$user_email}' , '{$order_id}',  '{$product_name}', '{$billing_cycle}',
          '{$order_created_at}', '{$sub_total}', '{$total}', '{$qtt}', '{$url}', '{$type_colocation}', '{$colocation_datacenter}',
          '{$colocation_ip}'
        ];
        $replace = [ $user->name,  $user->email , $order->id,  $product->name ,$billings[$colocation->billing_cycle], $order->created_at,
          number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' ,
          $detail_order->quantity, $url, $colocation->type_colo, $data['datacenter'], $colocation_ip
        ];
        $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
        try {
            $data = [
                'content' => $content,
                'user_name' => $user->name,
                'subject' => $email->meta_email->subject,
            ];
            $data_tb_send_mail = [
                'type' => 'mail_order_colo',
                'type_service' => 'colo',
                'user_id' => $user->id,
                'status' => false,
                'content' => serialize($data),
            ];
            $this->send_mail->create($data_tb_send_mail);
        } catch (\Exception $e) {
            report($e);
        }
        return $detail_order;
    }

    public function order_hosting($data)
    {
        $user = $this->user->find(Auth::user()->id);
        $product = $this->product->find($data['product_id']);
        $email = $this->email->find($product->meta_product->email_create);
        $add_on = !empty($data['addon'])? 1 : '';
        $sub_total = $product->pricing[$data['billing']];
        $total = $product->pricing[$data['billing']] * $data['quantity'];

        if (!empty($add_on)) {
            $product_addon_database = $this->product->find($data['id-addon-database']);
            $product_addon_storage = $this->product->find($data['id-addon-storage']);
            // TInh total va subtotal
            if ($data['addon_storage'] > 0) {
                $sub_total += $product_addon_storage->pricing[$data['billing']] * $data['addon_storage'];
                $total += $product_addon_storage->pricing[$data['billing']] * $data['addon_storage'] * $data['quantity'];
            }
            // dd($sub_total, $total,$data['addon_ram']);
            if ($data['addon_database'] > 0) {
                $sub_total += $product_addon_database->pricing[$data['billing']] * $data['addon_database'];
                $total += $product_addon_database->pricing[$data['billing']] * $data['addon_database'] * $data['quantity'];
            }
        }
        // dd($email);
        //create order
        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            // 'description' => $data['description'],
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => $data['type_product'],
            'due_date' => $date,
            'description' => !empty($data['description'])? $data['description'] : '',
            'status' => 'unpaid',
            'sub_total' => $sub_total,
            'quantity' => $data['quantity'],
            'user_id' => Auth::user()->id,
            'addon_id' => !empty($data['addon'])? '1' : '0',
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODH' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '2',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);
        // vetify order
        $data_vetify_order = [
            'order_id' => $order->id,
            'token' => sha1(time()),
        ];
        $vetify = $this->order_vetify->create($data_vetify_order);
        // $detail_order= 1;$data_order=1;
        //create vps
        if ($detail_order && $data_order) {
            $quantity_hosting = $data['quantity'];
            $product_id = $data['product_id'];
            $billing_cycle_order = $data['billing'];
            $data_domain = $data['domain'];
            for ($i=0; $i < $quantity_hosting; $i++) {
                $domain_hosting = strtolower($data_domain[$i]);
                $user_hosting = str_replace('.', '', $domain_hosting); //uthien.com -> uthiencom
                $check_user_hosting = substr($user_hosting, 0, 1);
                if ( is_numeric($check_user_hosting) ) {
                  $user_hosting = 'a' . substr($user_hosting, 0, 6); //uthiencom -> uthienco
                }
                else {
                  $user_hosting = substr($user_hosting, 0, 7); //uthiencom -> uthienco
                }

                $data_hosting = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' => Auth::user()->id,
                    'product_id' => $product_id,
                    'addon_id' => !empty($data['addon'])? $data['addon'] : '0',
                    'domain' => !empty($data_domain[$i])? $data_domain[$i] : '',
                    'billing_cycle' => $billing_cycle_order,
                    'status' => 'Pending',
                    'user' => $user_hosting,
                    'password' => $this->rand_string(),
                    'addon_qtt' => !empty($data['qtt_addon']) ? $data['qtt_addon'] : '0',
                ];
                $create_vps = $this->hosting->create($data_hosting);
                if(!$create_vps) {
                    return false;
                }
                $billings = config('billing');
                // Tạo log
                if ($product->group_product->private == 0) {
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'đặt hàng',
                      'model' => 'User/Order',
                      'description' => ' gói Hosting <a target="_blank" href="/admin/products/edit/' . $product->id . '">' . $product->name . '</a> ' . $data_domain[$i] . ' ' . $billings[$data_hosting['billing_cycle']],
                      'description_user' => ' dịch vụ ' . $product->name . ' ' . $data_domain[$i] . ' ' . $billings[$data_hosting['billing_cycle']],
                    ];
                } else {
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'đặt hàng',
                      'model' => 'User/Order',
                      'description' => ' gói Hosting <a target="_blank" href="/admin/product_privates/edit/' . $product->id . '">' . $product->name . '</a> ' . $data_domain[$i] . ' ' . $billings[$data_hosting['billing_cycle']],
                      'description_user' => ' dịch vụ ' . $product->name . ' ' . $data_domain[$i] . ' ' . $billings[$data_hosting['billing_cycle']],
                    ];
                }
                $this->log_activity->create($data_log);

            }
        } else {
            return false;
        }
        return $detail_order;
    }

    public function order_hosting_singapore($data)
    {
        $user = $this->user->find(Auth::user()->id);
        $product = $this->product->find($data['product_id']);
        $email = $this->email->find($product->meta_product->email_create);
        $add_on = !empty($data['addon'])? 1 : '';
        $sub_total = $product->pricing[$data['billing']];
        $total = $product->pricing[$data['billing']] * $data['quantity'];

        if (!empty($add_on)) {
            $product_addon_database = $this->product->find($data['id-addon-database']);
            $product_addon_storage = $this->product->find($data['id-addon-storage']);
            // TInh total va subtotal
            if ($data['addon_storage'] > 0) {
                $sub_total += $product_addon_storage->pricing[$data['billing']] * $data['addon_storage'];
                $total += $product_addon_storage->pricing[$data['billing']] * $data['addon_storage'] * $data['quantity'];
            }
            // dd($sub_total, $total,$data['addon_ram']);
            if ($data['addon_database'] > 0) {
                $sub_total += $product_addon_database->pricing[$data['billing']] * $data['addon_database'];
                $total += $product_addon_database->pricing[$data['billing']] * $data['addon_database'] * $data['quantity'];
            }
        }
        // dd($email);
        //create order
        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            // 'description' => $data['description'],
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => $data['type_product'],
            'due_date' => $date,
            'description' => !empty($data['description'])? $data['description'] : '',
            'status' => 'unpaid',
            'sub_total' => $sub_total,
            'quantity' => $data['quantity'],
            'user_id' => Auth::user()->id,
            'addon_id' => !empty($data['addon'])? '1' : '0',
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODHS' . strtoupper(substr(sha1(time()), 35, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '2',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);
        // vetify order
        $data_vetify_order = [
            'order_id' => $order->id,
            'token' => sha1(time()),
        ];
        $vetify = $this->order_vetify->create($data_vetify_order);
        // $detail_order= 1;$data_order=1;
        //create vps
        if ($detail_order && $data_order) {
            $quantity_hosting = $data['quantity'];
            for ($i=0; $i < $quantity_hosting; $i++) {
                $domain_hosting = strtolower($data['domain'][$i]);
                $user_hosting = str_replace('.', '', $domain_hosting); //uthien.com -> uthiencom
                $user_hosting = substr($user_hosting, 0, 7); //uthiencom -> uthienco

                $data_hosting = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' => Auth::user()->id,
                    'product_id' => $data['product_id'],
                    'addon_id' => !empty($data['addon'])? $data['addon'] : '0',
                    'domain' => !empty($data['domain'][$i])? $data['domain'][$i] : '',
                    'billing_cycle' => $data['billing'],
                    'status' => 'Pending',
                    'user' => $user_hosting,
                    'password' => $this->rand_string(),
                    'addon_qtt' => !empty($data['qtt_addon']) ? $data['qtt_addon'] : '0',
                    'location' => 'si',
                ];
                $create_vps = $this->hosting->create($data_hosting);
                if(!$create_vps) {
                    return false;
                }
                $billings = config('billing');
                $url = url('');
                $url = str_replace(['http://','https://'], ['', ''], $url);
                $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $data['domain'][$i], '' ,$billings[$data_hosting['billing_cycle']], '', $order->created_at, '', '',number_format($sub_total,0,",",".") . ' VNĐ' ,number_format($total ,0,",",".") . ' VNĐ' , $data['quantity'], '', $data_vetify_order['token'], $url];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'user_id' => $user->id,
                    'user_email' => $user->email,
                    'user_addpress' => $user->user_meta->address,
                    'user_phone' => $user->user_meta->user_phone,
                    'order_id' => $order->id,
                    'product_name' => $product->name,
                    'domain' => '',
                    'ip' => '',
                    'billing_cycle' => $data_hosting['billing_cycle'],
                    'next_due_date' => '',
                    'order_created_at' => $order->created_at,
                    'services_username' => '',
                    'services_password' => '',
                    'sub_total' => $sub_total,
                    'total' => $total,
                    'qtt' => $data['quantity'],
                    'os' => '',
                    'token' => $data_vetify_order['token'],
                ];
                try {
                    $this->send_mail(!empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận đặt hàng hosting', $data);

                    $data['type'] = 'Hosting';
                    $data['domain'] = $data_hosting['domain'];
                    $mail = Mail::send('users.mails.send_mail_order_hosting_support', $data, function($message){
                        $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                        $message->to('uthienth92@gmail.com')->subject('Order Hosting');
                        $message->cc('support@cloudzone.vn')->subject('Order Hosting');
                    });
                } catch (\Exception $e) {
                    continue;
                }

            }
        } else {
            return false;
        }
        return $detail_order;
    }

    public function order_hosting_one_time_pay($data)
    {
        $user = $this->user->find(Auth::user()->id);
        $product = $this->product->find($data['product_id']);
        $email = $this->email->find($product->meta_product->email_create);
        $sub_total = $product->pricing->one_time_pay;
        $total = $product->pricing->one_time_pay * $data['quantity'];
        // dd($email);
        //create order
        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            // 'description' => $data['description'],
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => $data['type_product'],
            'due_date' => $date,
            'description' => !empty($data['description'])? $data['description'] : '',
            'status' => 'unpaid',
            'sub_total' => $sub_total,
            'quantity' => $data['quantity'],
            'user_id' => Auth::user()->id,
            'addon_id' => !empty($data['addon'])? '1' : '0',
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODH' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '2',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);
        // vetify order
        $data_vetify_order = [
            'order_id' => $order->id,
            'token' => sha1(time()),
        ];
        $vetify = $this->order_vetify->create($data_vetify_order);
        // $detail_order= 1;$data_order=1;
        //create vps
        if ($detail_order && $data_order) {
            $quantity_hosting = $data['quantity'];
            $product_id = $data['product_id'];
            $billing_cycle_order = 'one_time_pay';
            $data_domain = $data['domain'];
            for ($i=0; $i < $quantity_hosting; $i++) {
                $domain_hosting = strtolower($data_domain[$i]);
                $user_hosting = str_replace('.', '', $domain_hosting); //uthien.com -> uthiencom
                $check_user_hosting = substr($user_hosting, 0, 1);
                if ( is_numeric($check_user_hosting) ) {
                  $user_hosting = 'a' . substr($user_hosting, 0, 6); //uthiencom -> uthienco
                }
                else {
                  $user_hosting = substr($user_hosting, 0, 7); //uthiencom -> uthienco
                }

                $data_hosting = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' => Auth::user()->id,
                    'product_id' => $product_id,
                    'addon_id' => !empty($data['addon'])? $data['addon'] : '0',
                    'domain' => !empty($data_domain[$i])? $data_domain[$i] : '',
                    'billing_cycle' => 'one_time_pay',
                    'status' => 'Pending',
                    'user' => $user_hosting,
                    'password' => $this->rand_string(),
                    'addon_qtt' => !empty($data['qtt_addon']) ? $data['qtt_addon'] : '0',
                ];
                $create_vps = $this->hosting->create($data_hosting);
                if(!$create_vps) {
                    return false;
                }
                $billings = config('billing');
                // Tạo log
                if ($product->group_product->private == 0) {
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'đặt hàng',
                      'model' => 'User/Order',
                      'description' => ' gói Hosting Lifetime <a target="_blank" href="/admin/products/edit/' . $product->id . '">' . $product->name . '</a> ' . $data_domain[$i] . ' ' . $billings[$data_hosting['billing_cycle']],
                      'description_user' => ' dịch vụ ' . $product->name . ' ' . $data_domain[$i] . ' ' . $billings[$data_hosting['billing_cycle']],
                    ];
                } else {
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'đặt hàng',
                      'model' => 'User/Order',
                      'description' => ' gói Hosting Lifetime <a target="_blank" href="/admin/product_privates/edit/' . $product->id . '">' . $product->name . '</a> ' . $data_domain[$i] . ' ' . $billings[$data_hosting['billing_cycle']],
                      'description_user' => ' dịch vụ ' . $product->name . ' ' . $data_domain[$i] . ' ' . $billings[$data_hosting['billing_cycle']],
                    ];
                }
                $this->log_activity->create($data_log);
            }
        } else {
            return false;
        }
        return $detail_order;
    }

    public function order_hosting_one_time_pay_singapore($data)
    {
        $user = $this->user->find(Auth::user()->id);
        $product = $this->product->find($data['product_id']);
        $email = $this->email->find($product->meta_product->email_create);
        $sub_total = $product->pricing->one_time_pay;
        $total = $product->pricing->one_time_pay * $data['quantity'];
        // dd($email);
        //create order
        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            // 'description' => $data['description'],
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => $data['type_product'],
            'due_date' => $date,
            'description' => !empty($data['description'])? $data['description'] : '',
            'status' => 'unpaid',
            'sub_total' => $sub_total,
            'quantity' => $data['quantity'],
            'user_id' => Auth::user()->id,
            'addon_id' => !empty($data['addon'])? '1' : '0',
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODHS' . strtoupper(substr(sha1(time()), 35, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '2',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);
        // vetify order
        $data_vetify_order = [
            'order_id' => $order->id,
            'token' => sha1(time()),
        ];
        $vetify = $this->order_vetify->create($data_vetify_order);
        // $detail_order= 1;$data_order=1;
        //create vps
        if ($detail_order && $data_order) {
            $quantity_hosting = $data['quantity'];
            for ($i=0; $i < $quantity_hosting; $i++) {
                $domain_hosting = strtolower($data['domain'][$i]);
                $user_hosting = str_replace('.', '', $domain_hosting); //uthien.com -> uthiencom
                $user_hosting = substr($user_hosting, 0, 7); //uthiencom -> uthienco

                $data_hosting = [
                    'detail_order_id' => $detail_order->id,
                    'user_id' => Auth::user()->id,
                    'product_id' => $data['product_id'],
                    'addon_id' => !empty($data['addon'])? $data['addon'] : '0',
                    'domain' => !empty($data['domain'][$i])? $data['domain'][$i] : '',
                    'billing_cycle' => 'one_time_pay',
                    'status' => 'Pending',
                    'user' => $user_hosting,
                    'password' => $this->rand_string(),
                    'addon_qtt' => !empty($data['qtt_addon']) ? $data['qtt_addon'] : '0',
                    'location' => 'si',
                ];
                $create_vps = $this->hosting->create($data_hosting);
                if(!$create_vps) {
                    return false;
                }
                $billings = config('billing');
                $url = url('');
                $url = str_replace(['http://','https://'], ['', ''], $url);
                $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $data['domain'][$i], '' , 'Vĩnh viễn', '', $order->created_at, '', '',number_format($sub_total,0,",",".") . ' VNĐ' ,number_format($total ,0,",",".") . ' VNĐ' , $data['quantity'], '', $data_vetify_order['token'], $url];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'user_id' => $user->id,
                    'user_email' => $user->email,
                    'user_addpress' => $user->user_meta->address,
                    'user_phone' => $user->user_meta->user_phone,
                    'order_id' => $order->id,
                    'product_name' => $product->name,
                    'domain' => '',
                    'ip' => '',
                    'billing_cycle' => 'Vĩnh viễn',
                    'next_due_date' => '',
                    'order_created_at' => $order->created_at,
                    'services_username' => '',
                    'services_password' => '',
                    'sub_total' => $sub_total,
                    'total' => $total,
                    'qtt' => $data['quantity'],
                    'os' => '',
                    'token' => $data_vetify_order['token'],
                ];
                try {
                  $this->send_mail(!empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận đặt hàng hosting', $data);

                  $data['type'] = 'Hosting Lifetime';
                  $data['domain'] = $data_hosting['domain'];
                  $mail = Mail::send('users.mails.send_mail_order_hosting_support', $data, function($message){
                      $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                      $message->to('uthienth92@gmail.com')->subject('Order Hosting Lifetime');
                      $message->cc('support@cloudzone.vn')->subject('Order Hosting Lifetime');
                  });
                } catch (\Exception $e) {
                  continue;
                }


            }
        } else {
            return false;
        }
        return $detail_order;
    }

    public function send_mail($subject, $data)
    {
        $this->user_email = Auth::user()->email;
        $this->subject = $subject;
        $mail = Mail::send('users.mails.orders', $data, function($message){
            $message->from('support@cloudzone.vn', 'Cloudzone Portal');
            $message->to($this->user_email)->subject($this->subject);
        });
    }

    public function verify_order($token)
    {
        $verify = $this->order_vetify->where('token', $token)->with('order')->first();
        if (isset($verify)) {
            $order = $verify->order;
            $order->verify = true;
            $order->save();
            $detail_order = $this->detail_order->where('order_id', $order->id)->first();
            return $detail_order;
        } else {
            return false;
        }

    }

    public function list_invoices_with_user()
    {
        $invoices = $this->detail_order->where('user_id', Auth::user()->id)->orderBy('id', 'desc')->with('order')->paginate(30);
        return $invoices;
    }

    public function list_invoices_progressing_with_user()
    {
        $invoices = $this->detail_order->where('user_id', Auth::user()->id)->where('status', 'unpaid')->orderBy('id', 'desc')->with('order')->paginate(30);
        // dd($invoices);
        return $invoices;
    }


    public function list_invoices_paid_with_user()
    {
        $invoices = $this->detail_order->where('status', 'paid')->where('user_id', Auth::user()->id)->with('order')->orderBy('id', 'desc')->paginate(30);
        return $invoices;
    }


    public function list_invoices_unpaid_with_user()
    {
        $invoices = $this->detail_order->where('status', 'unpaid')->where('user_id', Auth::user()->id)->with('order')->orderBy('id', 'desc')->paginate(30);
        return $invoices;
    }



    public function check_invoice($id)
    {
        $invoice = $this->detail_order->where('id', $id)->where('user_id', Auth::user()->id)->first();
        return $invoice;
    }

    public function check_credit($invoice_id)
    {
        $credit = $this->credit->where('user_id', Auth::user()->id)->first();
        if (!isset($credit)) {
            return false;
        }
        $invoice = $this->detail_order->find($invoice_id);
        $order = $this->order->find($invoice->order->id);
        $total = $order->total;
        // dd($total);
        if ($credit->value >= $total) {
            return true;
        } else {
            return false;
        }

    }

    public function check_point($invoice_id)
    {
        $user_meta = $this->user_meta->where('user_id', Auth::user()->id)->first();
        $invoice = $this->detail_order->find($invoice_id);
        if (!isset($user_meta) || empty($user_meta->point) ) {
            return false;
        }
        if ( $user_meta->point < ($invoice->sub_total * $invoice->quantity) / 1000 ) {
            return false;
        }
        return true;
    }

    // Thanh toán khi order
    public function payment($invoice_id)
    {
        $invoice = $this->detail_order->find($invoice_id);
        // dd($invoice);
        // HistoryPay
        $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
        if ( $history_pay->status == 'Active' ) {
          return 3;
        }
        // dd('da den');
        $credit = $this->credit->where('user_id', Auth::user()->id)->first();
        if (!isset($credit)) {
            return false;
        }
        //   kiểm tra sdtk
        $check_credit = $this->check_credit($invoice->id);
        if (!$check_credit) {
            return 5;
        }
        // kiểm tra tài khoản đả order chưa
        $user = Auth::user();
        if ( !empty($user->order) ) {
          return 9999;
        } else {
          $user->order = true;
          $user->save();
        }
        $order = $this->order->find($invoice->order_id);
        $total = $order->total;
        $order->status = 'Active';
        $order->save();

        $history_pay->status = 'Active';
        $history_pay->method_gd_invoice = 'credit';
        $history_pay->save();
        // ghi log giao dịch
        $data_log_payment = [
          'history_pay_id' => $history_pay->id,
          'before' => $credit->value,
          'after' => $credit->value - $total,
        ];
        $this->log_payment->create($data_log_payment);
        // credit
        $credit->value = $credit->value - $total;
        $credit->save();

        // invoice
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        if ($invoice->type == 'Server') {
            $servers = $this->server->where('detail_order_id', $invoice_id)->get();
            $billing_cycle = $servers[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$servers[0]->billing_cycle] ));
            foreach ($servers as $key => $server) {
                $server->next_due_date = $due_date;
                $server->paid = 'paid';
                $server->status = 'Active';
                $server->status_server = 'progressing';
                $server->date_create = date('Y-m-d');
                $server->save();
            }
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            $order->status = 'Finish';
            $order->save();
            // bỏ session thanh toán
            $user->order = false;
            $user->save();
            // Tạo log
            $data_log = [
               'user_id' => Auth::user()->id,
               'action' => 'thanh toán',
               'model' => 'User/HistoryPay',
               'description' => 'hóa đơn Server <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> thành công',
               'description_user' => 'hóa đơn Server <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> thành công',
            ];
            $this->log_activity->create($data_log);
            try {
              $this->send_mail_finish('server',$order, $invoice ,$servers);
            } catch (\Exception $e) {
              report($e);
              return $paid;
            }
        }
        elseif ($invoice->type == 'Colocation') {
            $colocations = $this->colocation->where('detail_order_id', $invoice_id)->get();
            $billing_cycle = $colocations[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$colocations[0]->billing_cycle] ));
            foreach ($colocations as $key => $colocation) {
                $colocation->next_due_date = $due_date;
                $colocation->paid = 'paid';
                $colocation->status = 'Active';
                $colocation->status_colo = 'progressing';
                $colocation->date_create = date('Y-m-d');
                $colocation->save();
            }
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            $order->status = 'Finish';
            $order->save();
            // bỏ session thanh toán
            $user->order = false;
            $user->save();
            // Tạo log
            $data_log = [
               'user_id' => Auth::user()->id,
               'action' => 'thanh toán',
               'model' => 'User/HistoryPay',
               'description' => 'hóa đơn Colocation <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> thành công',
               'description_user' => 'hóa đơn Colocation <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> thành công',
            ];
            $this->log_activity->create($data_log);
            // ghi vào tổng thu
            try {
              $this->send_mail_finish('colo',$order, $invoice ,$colocations);
            } catch (\Throwable $th) {
              //throw $th;
              report($th);
              return $paid;
            }
        }
        elseif ($invoice->type == 'Domain') {
            $domains = $this->domain->where('detail_order_id', $invoice_id)->get();
            $billing_cycle = $domains[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$domains[0]->billing_cycle] ));
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            foreach ($domains as $key => $domain) {
                $request_panhanhoa = $this->domain_pa->registerDomain($domain, $due_date); //Request API PAVietNam
                // $request_panhanhoa = 200;
                //Út Hiển làm tổng thu
                $domain->update(['status' => 'Active']);
                $domain->next_due_date = $due_date;
                $domain->save();
                //->{'ReturnCode'}
                if($request_panhanhoa->{'ReturnCode'} == 200) {
                    $order->status = 'Finish';
                    $order->save();
                    // bỏ session thanh toán
                    $user->order = false;
                    $user->save();
                    // $this->send_mail_finish('hosting',$order, $invoice ,$hostings);
                    // Tạo log
                    $data_log = [
                       'user_id' => Auth::user()->id,
                       'action' => 'thanh toán',
                       'model' => 'User/HistoryPay',
                       'description' => 'hóa đơn Domain <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> thành công',
                       'description_user' => 'hóa đơn Domain <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> thành công',
                    ];
                    $this->log_activity->create($data_log);
                    // $this->send_mail_finish('domain',$order, $invoice ,$domain);
                    $data_reg = [
                        'user_id' => Auth::user()->id,
                        'domain' => $domain['domain'],
                        'due_date' => date('d/m/Y', strtotime( $due_date )),
                        'total' => $invoice->sub_total
                    ];
                    try {
                      $mail = Mail::send('users.mails.finish_reg_domain', $data_reg, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to(Auth::user()->email)->subject('Hoàn thành đăng ký domain');
                      });
                      $mail_to_admin = Mail::send('users.mails.finish_reg_domain_to_admin', $data_reg, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to('tuansinhtk@gmail.com')->subject('Hoàn thành đăng ký domain');
                      });
                    } catch (\Exception $e) {
                      return true;
                    }

                    return true;
                }
                else {
                    $data_error = [
                        'user_id' => Auth::user()->id,
                        'domain' => $domain['domain'],
                        'error_code' => $request_panhanhoa->{'ReturnCode'},
                        'error' => $request_panhanhoa->{'ReturnText'},
                    ];
                    // bỏ session thanh toán
                    $user->order = false;
                    $user->save();
                    // dd($data_error);
                    try {
                      $mail = Mail::send('users.mails.error_reg_domain', $data_error, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to('tuansinhtk@gmail.com')->subject('Lỗi khi tạo domain');
                      });
                    } catch (\Exception $e) {
                      return true;
                    }

                    return true;
                }
            }


        }
        elseif ($invoice->type == 'Hosting-Singapore') {
            $hostings = $this->hosting->where('detail_order_id', $invoice_id)->get();
            $billing_cycle = $hostings[0]->billing_cycle;
            // Dành cho những hosting one_time_pay
            if ($billing_cycle == 'one_time_pay') {
                $date_paid = date('Y-m-d');
                $invoice->due_date = '2099-12-31';
                $invoice->paid_date = $date_paid;
                $invoice->status = 'paid';
                $invoice->payment_method = 1;
                $request_da = $this->da_si->orderCreateAccount($hostings, 'one_time_pay'); //request đên Direct Admin
                // $request_da = true;
                $paid = $invoice->save();

                if ($request_da) {
                    $order->status = 'Finish';
                    $order->save();
                    $this->send_mail_finish('hosting',$order, $invoice ,$hostings);
                    // ghi vào tổng thu
                    if (!empty($this->total_price->first())) {
                        $total_price = $this->total_price->first();
                        $total_price->hosting += $total;
                        $total_price->save();
                    } else {
                        $data_total_price = [
                            'vps' => '0',
                            'hosting' => $total,
                            'server' => '0',
                        ];
                        $this->total_price->create($data_total_price);
                    }
                }
                return true;
            }
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$hostings[0]->billing_cycle] ));
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;

            $request_da = $this->da_si->orderCreateAccount($hostings,$due_date); //request đên Direct Admin
            // $request_da = true;
            $paid = $invoice->save();
            if ($request_da) {
                $order->status = 'Finish';
                $order->save();

                // ghi vào tổng thu
                if (!empty($this->total_price->first())) {
                    $total_price = $this->total_price->first();
                    $total_price->hosting += $total;
                    $total_price->save();
                } else {
                    $data_total_price = [
                        'vps' => '0',
                        'hosting' => $total,
                        'server' => '0',
                    ];
                    $this->total_price->create($data_total_price);
                }
                try {
                  $this->send_mail_finish('hosting',$order, $invoice ,$hostings);
                } catch (\Exception $e) {
                  return $paid;
                }
            }
        }
        else {
            $hostings = $this->hosting->where('detail_order_id', $invoice_id)->get();

            $billing_cycle = $hostings[0]->billing_cycle;
            // Dành cho những hosting one_time_pay
            if ($billing_cycle == 'one_time_pay') {
                $date_paid = date('Y-m-d');
                $invoice->due_date = '2099-12-31';
                $invoice->paid_date = $date_paid;
                $invoice->status = 'paid';
                $invoice->payment_method = 1;

                $request_da = $this->da->orderCreateAccount($hostings, 'one_time_pay'); //request đên Direct Admin
                // $request_da = true;
                $paid = $invoice->save();
                // bỏ session thanh toán
                $user->order = false;
                $user->save();
                if ($request_da) {
                    $order->status = 'Finish';
                    $order->save();
                    // Tạo log
                    $data_log = [
                       'user_id' => Auth::user()->id,
                       'action' => 'thanh toán',
                       'model' => 'User/HistoryPay',
                       'description' => 'hóa đơn gói Hosting Lifetime <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> thành công',
                       'description_user' => 'hóa đơn gói Hosting Lifetime <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> thành công',
                    ];
                    $this->log_activity->create($data_log);
                    // ghi vào tổng thu
                    if (!empty($this->total_price->first())) {
                        $total_price = $this->total_price->first();
                        $total_price->hosting += $total;
                        $total_price->save();
                    } else {
                        $data_total_price = [
                            'vps' => '0',
                            'hosting' => $total,
                            'server' => '0',
                        ];
                        $this->total_price->create($data_total_price);
                    }
                    try {
                      $this->send_mail_finish('hosting',$order, $invoice ,$hostings);
                    } catch (\Exception $e) {
                      return true;
                    }

                }
                return true;
            }
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$hostings[0]->billing_cycle] ));
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;

            $request_da = $this->da->orderCreateAccount($hostings,$due_date); //request đên Direct Admin
            // $request_da = true;
            $paid = $invoice->save();
            // bỏ session thanh toán
            $user->order = false;
            $user->save();
            if ($request_da) {
                $order->status = 'Finish';
                $order->save();
                // Tạo log
                $data_log = [
                   'user_id' => Auth::user()->id,
                   'action' => 'thanh toán',
                   'model' => 'User/HistoryPay',
                   'description' => ' hóa đơn gói Hosting <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> thành công',
                   'description_user' => ' hóa đơn gói Hosting <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> thành công',
                ];
                $this->log_activity->create($data_log);
                // ghi vào tổng thu
                if (!empty($this->total_price->first())) {
                    $total_price = $this->total_price->first();
                    $total_price->hosting += $total;
                    $total_price->save();
                } else {
                    $data_total_price = [
                        'vps' => '0',
                        'hosting' => $total,
                        'server' => '0',
                    ];
                    $this->total_price->create($data_total_price);
                }
                try {
                  $this->send_mail_finish('hosting',$order, $invoice ,$hostings);
                } catch (\Exception $e) {
                  return $paid;
                }

            }
        }
        return $paid;
    }
    // check_vps_dashboard($invoice_id)
    public function check_vps_dashboard($invoice_id)
    {
        // return 3;
        $invoice = $this->detail_order->find($invoice_id);
        $credit = $this->credit->where('user_id', Auth::user()->id)->first();
        if (!isset($credit)) {
          return false;
        }
        $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
        if ( $history_pay->status == 'Active' ) {
          return 3;
        }
        // dd('da qua');
        //   kiểm tra sdtk
        $check_credit = $this->check_credit($invoice->id);
        if (!$check_credit) {
            return 5;
        }
        // kiểm tra tài khoản đả order chưa
        $user = Auth::user();
        if ( !empty($user->order) ) {
          return 9999;
        } else {
          $user->order = true;
          $user->save();
        }
        if ( $invoice->type == 'VPS' || $invoice->type == 'NAT-VPS' ) {
            $order = $this->order->find($invoice->order->id);
            $total = $order->total;
            $order->status = 'Active';
            $order->save();

            $billing = [
                'monthly' => '1 Month',
                'twomonthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            $list_vps = $this->vps->where('detail_order_id', $invoice_id)->get();
            $billing_cycle = $list_vps[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$list_vps[0]->billing_cycle] ));
            $history_pay->status = 'Active';
            $history_pay->method_gd_invoice = 'credit';
            $history_pay->save();
            // ghi log giao dịch
            $data_log_payment = [
              'history_pay_id' => $history_pay->id,
              'before' => $credit->value,
              'after' => $credit->value - $total,
            ];
            $this->log_payment->create($data_log_payment);
            // credit
            $credit->value = $credit->value - $total;
            $credit->save();

            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();

            if ( $invoice->type == 'VPS') {
              $reques_vcenter = $this->dashboard->createVPS($list_vps, $due_date);
            } else {
              $reques_vcenter = $this->dashboard->createNatVPS($list_vps, $due_date);
            }
            // $reques_vcenter = true;
            if ($reques_vcenter) {
              // bỏ session thanh toán
              $user->order = false;
              $user->save();

              $order->status = 'Finish';
              $order->save();
              // Tạo log
              $data_log = [
                 'user_id' => Auth::user()->id,
                 'action' => 'thanh toán',
                 'model' => 'User/HistoryPay',
                 'description' => 'hóa đơn VPS <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> thành công',
                 'description_user' => 'hóa đơn VPS <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> thành công',
              ];
              $this->log_activity->create($data_log);

              try {
                if ( $invoice->type == 'VPS') {
                  $this->send_mail_finish('vps',$order, $invoice ,$list_vps);
                } else {
                  $this->send_mail_finish('nat-vps',$order, $invoice ,$list_vps);
                }
              } catch (\Exception $e) {
                return 3;
              }
              return 3;
            }
            else {
              // bỏ session thanh toán
                $user->order = false;
                $user->save();
                return 2;
            }
        }
        elseif ( $invoice->type == 'VPS-US' ) {
          $date_paid = date('Y-m-d');
          $order = $this->order->find($invoice->order->id);
          $total = $order->total;
          $order->status = 'Active';
          $order->save();

          $history_pay->status = 'Active';
          $history_pay->method_gd_invoice = 'credit';
          $history_pay->save();
          // ghi log giao dịch
          $data_log_payment = [
            'history_pay_id' => $history_pay->id,
            'before' => $credit->value,
            'after' => $credit->value - $total,
          ];
          $this->log_payment->create($data_log_payment);
          $credit->value = $credit->value - $total;
          $credit->save();
          $invoice->paid_date = $date_paid;
          $invoice->status = 'paid';
          $invoice->payment_method = 1;
          $paid = $invoice->save();


          $list_vps = $this->vps->where('detail_order_id', $invoice_id)->get();
          $billing_cycle = $list_vps[0]->billing_cycle;
          $reques_vcenter = $this->dashboard->createVpsUs($list_vps);
            // $reques_vcenter['error'] = 0;
          if ( $reques_vcenter['error'] == 0 ) {
              // credit
              // $credit = $this->credit->where('user_id', Auth::user()->id)->first();
              // $credit->value = $credit->value - $total;
              // $credit->save();
              // bỏ session thanh toán
              $user->order = false;
              $user->save();
              // ghi vào tổng thu
              $order->status = 'Finish';
              $order->save();
              // Tạo log
              $data_log = [
                 'user_id' => Auth::user()->id,
                 'action' => 'thanh toán',
                 'model' => 'User/HistoryPay',
                 'description' => 'hóa đơn VPS US <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> thành công',
                 'description_user' => 'hóa đơn VPS US <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> thành công',
              ];
              $this->log_activity->create($data_log);
              try {
                $this->send_mail_finish('vps',$order, $invoice ,$list_vps);
              } catch (\Exception $e) {
                return 3;
              }
              return 3;
          }
          elseif ( $reques_vcenter['error'] == 1 ) {
              // bỏ session thanh toán
              $user->order = false;
              $user->save();
              $order->status = 'Finish';
              $order->save();
              // Tạo log
              $data_log = [
                 'user_id' => Auth::user()->id,
                 'action' => 'thanh toán',
                 'model' => 'User/HistoryPay',
                 'description' => 'hóa đơn VPS US <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> thành công',
                 'description_user' => 'hóa đơn VPS US <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> thành công',
              ];
              $this->log_activity->create($data_log);

              $dataResponse = [
                'error' => 1
              ];

              if ( $list_vps->count() == count($reques_vcenter['content']) ) {
                $dataResponse = [
                  'error' => 1,
                  'success' => 0,
                  'progressing' => count($reques_vcenter['content'])
                ];
              } else {
                $dataResponse = [
                  'error' => 1,
                  'success' => $list_vps->count - count($reques_vcenter['content']),
                  'progressing' => count($reques_vcenter['content']),
                ];
              }
              try {
                $this->send_mail_finish('vps_us',$order, $invoice ,$list_vps);
              } catch (\Exception $e) {
                return $dataResponse;
              }
              return $dataResponse;
          }
          else {
            // bỏ session thanh toán
            $user->order = false;
            $user->save();
            return 2;
          }
        }
        elseif ( $invoice->type == 'Proxy' ) {
          $date_paid = date('Y-m-d');
          $order = $this->order->find($invoice->order->id);
          $total = $order->total;
          $order->status = 'Active';
          $order->save();

          $history_pay->status = 'Active';
          $history_pay->method_gd_invoice = 'credit';
          $history_pay->save();
          // ghi log giao dịch
          $data_log_payment = [
            'history_pay_id' => $history_pay->id,
            'before' => $credit->value,
            'after' => $credit->value - $total,
          ];
          $this->log_payment->create($data_log_payment);
          $credit->value = $credit->value - $total;
          $credit->save();
          // credit
          // $credit = $this->credit->where('user_id', Auth::user()->id)->first();
          // $credit->value = $credit->value - $total;
          // $credit->save();
          $invoice->paid_date = $date_paid;
          $invoice->status = 'paid';
          $invoice->payment_method = 1;
          $paid = $invoice->save();

          $list_proxy = $this->proxy->where('detail_order_id', $invoice_id)->get();
          $billing_cycle = $list_proxy[0]->billing_cycle;
          $reques_vcenter = $this->dashboard->createProxy($list_proxy);
          // $reques_vcenter['error'] = 0;
          if ( $reques_vcenter['error'] == 0 ) {
              // bỏ session thanh toán
              $user->order = false;
              $user->save();
              $order->status = 'Finish';
              $order->save();
              // Tạo log
              $data_log = [
                 'user_id' => Auth::user()->id,
                 'action' => 'thanh toán',
                 'model' => 'User/HistoryPay',
                 'description' => 'hóa đơn Proxy <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> thành công',
                 'description_user' => 'hóa đơn Proxy <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> thành công',
              ];
              $this->log_activity->create($data_log);
              try {
                $this->send_mail_finish('proxy',$order, $invoice ,$list_proxy);
              } catch (\Exception $e) {
                report($e);
                return 3;
              }
              return 3;
          }
          else {
            // bỏ session thanh toán
            $user->order = false;
            $user->save();
            return 2;
          }
        }
        else {
          // bỏ session thanh toán
          $user->order = false;
          $user->save();
          return 1;
        }
    }

    // Thanh toán khi order
    public function payment_upgrade($invoice_id)
    {
      // return true;
        $invoice = $this->detail_order->find($invoice_id);
        $order = $this->order->find($invoice->order->id);
        // HistoryPay
        $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
        if ( $history_pay->status == 'Active' ) {
          return true;
        }
        $total = $order->total;
        $order->status = 'Active';
        $order->save();
        $credit = $this->credit->where('user_id', Auth::user()->id)->first();
        if (!isset($credit)) {
            return false;
        }
        // HistoryPay
        $history_pay->status = 'Active';
        $history_pay->method_gd_invoice = 'credit';
        $history_pay->save();
        // ghi log giao dịch
        $data_log_payment = [
          'history_pay_id' => $history_pay->id,
          'before' => $credit->value,
          'after' => $credit->value - $total,
        ];
        $this->log_payment->create($data_log_payment);
        // credit
        $credit->value = $credit->value - $total;
        $credit->save();
        // invoice
        $invoice->paid_date = date('Y-m-d');
        $invoice->status = 'paid';
        $invoice->payment_method = 1;
        $paid = $invoice->save();
        // nang cap hosting
        $upgrade = $invoice->order_upgrade_hosting;
        $hosting = $upgrade->hosting;
        if ($hosting->location == 'vn') {
          $this->da->upgrade($hosting, $upgrade->product->package);
        }
        elseif ($hosting->location == 'si') {
          $this->da_si->upgrade($hosting, $upgrade->product->package);
        }
        $hosting->product_id = $upgrade->product_id;
        if (!empty($hosting->price_override)) {
           $hosting->price_override += $total;
        }
        $hosting->save();
        // ghi log
        $product = $upgrade->product;
        if ($product->group_product->private == 0) {
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'thanh toán',
            'model' => 'User/HistoryPay',
            'description' => ' gói nâng cấp Hosting <a target="_blank" href="/admin/hostings/detail/' . $hosting->id . '">' . $hosting->domain . '</a> lên gói Hosting <a href="admin/products/edit/' . $product->id . '">' . $product->name . '</a> ',
            'description_user' => ' gói nâng cấp Hosting <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">' . $invoice->domain . '</a> thành công',
            'service' =>  $hosting->domain,
          ];
        } else {
          $data_log = [
            'user_id' => Auth::user()->id,
            'action' => 'thanh toán',
            'model' => 'User/HistoryPay',
            'description' => ' gói nâng cấp Hosting <a target="_blank" href="/admin/hostings/detail/' . $hosting->id . '">' . $hosting->domain . '</a> lên gói Hosting <a href="admin/product_privates/edit/' . $product->id . '">' . $product->name . '</a> ',
            'description_user' => ' gói nâng cấp Hosting <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">' . $invoice->id . '</a> thành công',
            'service' =>  $hosting->domain,
          ];
        }
        $this->log_activity->create($data_log);
        // ghi vào tổng thu
        if (!empty($this->total_price->first())) {
            $total_price = $this->total_price->first();
            $total_price->hosting += $total;
            $total_price->save();
        } else {
            $data_total_price = [
                'vps' => '0',
                'hosting' => $total,
                'server' => '0',
            ];
            $this->total_price->create($data_total_price);
        }
        $data = [
            'name' => Auth::user()->name,
            'email' => Auth::user()->email,
            'domain' => $hosting->domain,
            'product_name' => $upgrade->product->name,
            'subject' => 'Hoàn thành nâng cấp dịch vụ Hosting'
        ];

        try {
            $data_tb_send_mail = [
                'type' => 'finish_order_upgarde_hosting',
                'type_service' => 'hosting',
                'user_id' => Auth::id(),
                'status' => false,
                'content' => serialize($data),
              ];
            $this->send_mail->create($data_tb_send_mail);
        } catch (\Exception $e) {
            report($e);
            return $paid;
        }
        return $paid;
    }

    // Thanh toán khi gia hạn
    public function payment_expired($invoice_id)
    {
        $invoice = $this->detail_order->find($invoice_id);
        $billingDashBoard = config('billingDashBoard');
        $billings = config('billing');
        // HistoryPay
        $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
        if ( $history_pay->status == 'Active' || $history_pay->status == 'Confirm_Expired' ) {
          return true;
        }
        $credit = $this->credit->where('user_id', Auth::user()->id)->first();
        if (!isset($credit)) {
            return false;
        }
        //   kiểm tra sdtk
        $check_credit = $this->check_credit($invoice->id);
        if (!$check_credit) {
            return 5;
        }
        // dd('da den');
        $order = $this->order->find($invoice->order->id);
        $total = $order->total;
        $order->status = 'Active';
        $order->save();
        // invoice
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        $order_expireds = $invoice->order_expireds;

        if ($invoice->type == 'Server') {
            $billing_cycle = $order_expireds[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expireds[0]->billing_cycle] ));
            // HistoryPay
            $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
            $history_pay->status = 'Active';
            $history_pay->method_gd_invoice = 'credit';
            $history_pay->save();
            // ghi log giao dịch
            $data_log_payment = [
              'history_pay_id' => $history_pay->id,
              'before' => $credit->value,
              'after' => $credit->value - $total,
            ];
            $this->log_payment->create($data_log_payment);
            // credit
            $credit = $this->credit->where('user_id', Auth::user()->id)->first();
            $credit->value = $credit->value - $total;
            $credit->save();
            // invoice
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            $order->status = 'Finish';
            $order->save();
            foreach ($order_expireds as $key => $order_expired) {
                $server = $order_expired->server;
                $date_start_server = $server->next_due_date;
                $due_date_server = date('Y-m-d', strtotime( $date_start_server . $billing[$order_expired->billing_cycle] ));
                $server->next_due_date = $due_date_server;
                $server->billing_cycle = $order_expired->billing_cycle;
                if ( $server->status_server == 'expire' ) {
                    $server->status_server = 'on';
                }
                $server->save();
                try {
                  $data_log = [
                    'user_id' => Auth::user()->id,
                    'action' => 'thanh toán',
                    'model' => 'User/HistoryPay',
                    'description' => ' gia hạn Server <a target="_blank" href="/admin/servers/detail/' . $server->id . '">' . $server->ip . '</a> '  . $billings[$server->billing_cycle],
                    'description_user' => ' gia hạn Server <a target="_blank" href="/order/check-invoices/' . $server->id . '">' . $server->ip . '</a> '  . $billings[$server->billing_cycle] . ' thành công',
                    'service' =>  $server->ip,
                  ];
                  $this->log_activity->create($data_log);
                } catch (\Exception $e) {
                  report($e);
                }
            }
            try {
              $this->send_mail_finish_expired('server',$order, $invoice ,$order_expireds);
            } catch (\Exception $e) {
              return $paid;
            }
        }

        elseif ($invoice->type == 'Colocation') {
            $billing_cycle = $order_expireds[0]->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expireds[0]->billing_cycle] ));
            // HistoryPay
            $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
            $history_pay->status = 'Active';
            $history_pay->method_gd_invoice = 'credit';
            $history_pay->save();
            // ghi log giao dịch
            $data_log_payment = [
              'history_pay_id' => $history_pay->id,
              'before' => $credit->value,
              'after' => $credit->value - $total,
            ];
            $this->log_payment->create($data_log_payment);
            // credit
            $credit = $this->credit->where('user_id', Auth::user()->id)->first();
            $credit->value = $credit->value - $total;
            $credit->save();
            // invoice
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            $order->status = 'Finish';
            $order->save();
            // ghi vào tổng thu
            foreach ($order_expireds as $key => $order_expired) {
                $colocation = $order_expired->colocation;
                $date_start_colocation = $colocation->next_due_date;
                $due_date_colocation = date('Y-m-d', strtotime( $date_start_colocation . $billing[$order_expired->billing_cycle] ));
                $amount = $colocation->amount;
                if ( $colocation->billing_cycle == $order_expired->billing_cycle ) {
                    $sub_total_colocation = $amount;
                } else {
                    $sub_total_colocation = $amount *  ( $billingDashBoard[$order_expired->billing_cycle] ) / ( $billingDashBoard[$colocation->billing_cycle] ) ;
                }
                $colocation->next_due_date = $due_date_colocation;
                $colocation->billing_cycle = $order_expired->billing_cycle;
                $colocation->amount = $sub_total_colocation;
                $colocation->save();
                try {
                  $data_log = [
                    'user_id' => Auth::user()->id,
                    'action' => 'thanh toán',
                    'model' => 'User/HistoryPay',
                    'description' => ' gia hạn Colocation <a target="_blank" href="/admin/colocations/detail/' . $colocation->id . '">' . $colocation->ip . '</a> '  . $billings[$colocation->billing_cycle],
                    'description_user' => ' gia hạn Colocation <a target="_blank" href="/order/check-invoices/' . $colocation->id . '">' . $colocation->ip . '</a> '  . $billings[$colocation->billing_cycle] . ' thành công',
                    'service' =>  $colocation->ip,
                  ];
                  $this->log_activity->create($data_log);
                } catch (\Exception $e) {
                  report($e);
                }
            }
            try {
              $this->send_mail_finish_expired('colo',$order, $invoice ,$order_expireds);
            } catch (\Exception $e) {
              return $paid;
            }
        }

        elseif ( $invoice->type == 'Email Hosting' ) {
            // HistoryPay
            $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
            $history_pay->status = 'Active';
            $history_pay->method_gd_invoice = 'credit';
            $history_pay->save();
            // ghi log giao dịch
            $data_log_payment = [
              'history_pay_id' => $history_pay->id,
              'before' => $credit->value,
              'after' => $credit->value - $total,
            ];
            $this->log_payment->create($data_log_payment);
            // credit
            $credit = $this->credit->where('user_id', Auth::user()->id)->first();
            $credit->value = $credit->value - $total;
            $credit->save();
            // hosting
            // dd($hostings);
            $billing_cycle = $order_expireds[0]->billing_cycle;
            // Dành cho những hosting one_time_pay
            // invoice
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expireds[0]->billing_cycle] ));
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            // $request_da = $this->da->orderCreateAccount($hostings,$due_date); //request đên Direct Admin
            $paid = $invoice->save();

            foreach ($order_expireds as $key => $order_expired) {
                $email_hosting = $order_expired->email_hosting;

                $date_star = $email_hosting->next_due_date;
                $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expired->billing_cycle] ));

                $email_hosting->billing_cycle = $order_expired->billing_cycle;
                $email_hosting->next_due_date = $due_date;
                $email_hosting->save();
                // ghi log
                $billings = config('billing');
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'thanh toán',
                  'model' => 'User/HistoryPay',
                  'description' => ' gia hạn Email Hosting <a target="_blank" href="/admin/hostings/detail/' . $email_hosting->id . '">' . $email_hosting->domain . '</a> '  . $billings[$order_expired->billing_cycle],
                  'description_user' => ' gia hạn Email Hosting <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> '  . $billings[$order_expired->billing_cycle] . ' thành công',
                  'service' =>  $email_hosting->domain,
                ];
                $this->log_activity->create($data_log);
                // Kiem tra product co diem point ko
                $event = $this->event_promotion->where('product_id', $email_hosting->product_id)->where('billing_cycle', $email_hosting->billing_cycle)->first();
                if ( isset($event) ) {
                   $user = $email_hosting->user_email_hosting;
                   $meta_user = $user->user_meta;
                   $meta_user->point += $event->point;
                   $meta_user->save();
                   // Tạo thông báo cho user
                   $content = '';
                   $content .= '<p>Bạn được tặng ' . $event->point . ' Cloudzone Point vào tài khoản khi gia hạn gói dịch vụ ' . $email_hosting->product->name . '</p>';

                   $data_notification = [
                       'name' => "Chúc mừng bạn đã nhận được KHUYẾN MÃI từ Cloudzone!",
                       'content' => $content,
                       'status' => 'Active',
                   ];
                   $infoNotification = $this->notification->create($data_notification);
                   $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
                }

            }

            $order->status = 'Finish';
            $order->save();
            // ghi vào tổng thu
            if (!empty($this->total_price->first())) {
                $total_price = $this->total_price->first();
                $total_price->hosting += $total;
                $total_price->save();
            } else {
                $data_total_price = [
                    'vps' => '0',
                    'hosting' => $total,
                    'server' => '0',
                ];
                $this->total_price->create($data_total_price);
            }
            try {
              $this->send_mail_finish_expired('hosting',$order, $invoice ,$order_expireds);
            } catch (\Exception $e) {
              return $paid;
            }
        }

        else {
            // HistoryPay
            $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
            $history_pay->status = 'Active';
            $history_pay->method_gd_invoice = 'credit';
            $history_pay->save();
            // ghi log giao dịch
            $data_log_payment = [
              'history_pay_id' => $history_pay->id,
              'before' => $credit->value,
              'after' => $credit->value - $total,
            ];
            $this->log_payment->create($data_log_payment);
            // credit
            $credit = $this->credit->where('user_id', Auth::user()->id)->first();
            $credit->value = $credit->value - $total;
            $credit->save();
            // hosting
            // dd($hostings);
            $billing_cycle = $order_expireds[0]->billing_cycle;
            // Dành cho những hosting one_time_pay
            // invoice
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expireds[0]->billing_cycle] ));
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            // $request_da = $this->da->orderCreateAccount($hostings,$due_date); //request đên Direct Admin
            $paid = $invoice->save();

            foreach ($order_expireds as $key => $order_expired) {
                $hosting = $order_expired->hosting;

                $date_star = $hosting->next_due_date;
                $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expired->billing_cycle] ));

                $hosting->billing_cycle = $order_expired->billing_cycle;
                $hosting->next_due_date = $due_date;
                $hosting->save();
                // ghi log
                $billings = config('billing');
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'thanh toán',
                  'model' => 'User/HistoryPay',
                  'description' => ' gia hạn Hosting <a target="_blank" href="/admin/hostings/detail/' . $hosting->id . '">' . $hosting->domain . '</a> '  . $billings[$order_expired->billing_cycle],
                  'description_user' => ' gia hạn Hosting <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">' . $hosting->domain . '</a> '  . $billings[$order_expired->billing_cycle] . ' thành công',
                  'service' =>  $hosting->domain,
                ];
                $this->log_activity->create($data_log);
                // Kiem tra product co diem point ko
                $event = $this->event_promotion->where('product_id', $hosting->product_id)->where('billing_cycle', $hosting->billing_cycle)->first();
                if ( isset($event) ) {
                   $user = $hosting->user_hosting;
                   $meta_user = $user->user_meta;
                   $meta_user->point += $event->point;
                   $meta_user->save();
                   // Tạo thông báo cho user
                   $content = '';
                   $content .= '<p>Bạn được tặng ' . $event->point . ' Cloudzone Point vào tài khoản khi gia hạn gói dịch vụ ' . $hosting->product->name . '</p>';

                   $data_notification = [
                       'name' => "Chúc mừng bạn đã nhận được KHUYẾN MÃI từ Cloudzone!",
                       'content' => $content,
                       'status' => 'Active',
                   ];
                   $infoNotification = $this->notification->create($data_notification);
                   $this->readnotification->create(['user_id' => $user->id, 'notification_id' => $infoNotification->id ]);
                }

            }

            $order->status = 'Finish';
            $order->save();
            // ghi vào tổng thu
            if (!empty($this->total_price->first())) {
                $total_price = $this->total_price->first();
                $total_price->hosting += $total;
                $total_price->save();
            } else {
                $data_total_price = [
                    'vps' => '0',
                    'hosting' => $total,
                    'server' => '0',
                ];
                $this->total_price->create($data_total_price);
            }
            try {
              $this->send_mail_finish_expired('hosting',$order, $invoice ,$order_expireds);
            } catch (\Exception $e) {
              return $paid;
            }


        }
        return $paid;
    }
    // check_expired_dashboard
    public function check_expired_dashboard($invoice_id)
    {
        // return 3;
        $invoice = $this->detail_order->find($invoice_id);
        $billings = config('billing');
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        $billingDashBoard = config('billingDashBoard');
        $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
        // dd($history_pay);
        if ( $history_pay->status == 'Active' || $history_pay->status == 'Confirm_Expired' ) {
            // dd($history_pay, 1);
          return 3;
        }
        $credit = $this->credit->where('user_id', Auth::user()->id)->first();
        if (!isset($credit)) {
            return false;
        }
        //   kiểm tra sdtk
        $check_credit = $this->check_credit($invoice->id);
        if (!$check_credit) {
            return 5;
        }
            // dd($history_pay);
        $history_pay->status = 'Confirm_Expired';
        $history_pay->save();
        $data_expired = [];
        $order_expireds = $invoice->order_expireds;
        if ( $invoice->type == 'VPS') {
            // dd('da den');
            foreach ($order_expireds as $key => $order_expired) {
                $vps = $order_expired->vps;
                $date_star = $vps->next_due_date;
                $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expired->billing_cycle] ));

                // $vps->next_due_date = $due_date;
                // $vps->billing_cycle = $order_expired->billing_cycle;
                // $vps->save();

                $config_billing_DashBoard = config('billingDashBoard');
                $data_expired[] = [
                    'package' => (int) $this->checkTotalVpsExpire($vps, $order_expired->billing_cycle),
                    'leased_time' => $config_billing_DashBoard[$order_expired->billing_cycle],
                    'type' => ($vps->type_vps == 'nat_vps') ? 5 : 0,
                    'vm_id' => $vps->vm_id,
                    'end_date' => $due_date
                ];
            }
            // dd($data_expired);
            $reques_vcenter = $this->dashboard->expired_vps($data_expired, $vps->vm_id); //request đên Vcenter
            // $reques_vcenter = true;
            if ( $reques_vcenter ) {
              // $reques_vcenter->error == 0
              if ( $reques_vcenter->error == 0 ) {
                  foreach ($order_expireds as $key => $order_expired) {
                      $vps = $order_expired->vps;
                      $date_star = $vps->next_due_date;
                      $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expired->billing_cycle] ));

                      $vps->next_due_date = $due_date;
                      $vps->billing_cycle = $order_expired->billing_cycle;
                      if ( $vps->status_vps == 'expire' ) {
                          $vps->status_vps = 'on';
                      }
                      $vps->save();
                      try {
                        $data_log = [
                          'user_id' => Auth::user()->id,
                          'action' => 'thanh toán',
                          'model' => 'User/HistoryPay',
                          'description' => ' gia hạn VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$vps->billing_cycle],
                          'description_user' => ' gia hạn VPS <a target="_blank" href="/order/check-invoices/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$vps->billing_cycle] . ' thành công',
                          'service' =>  $vps->ip,
                        ];
                        $this->log_activity->create($data_log);
                      } catch (\Exception $e) {
                        report($e);
                      }
                  }
                  $invoice = $this->detail_order->find($invoice_id);
                  $order = $this->order->find($invoice->order->id);
                  $total = $order->total;
                  $order->status = 'Finish';
                  $order->save();
                  $billing_cycle = $order_expireds[0]->billing_cycle;
                  $date_star = $invoice->created_at;
                  $date_paid = date('Y-m-d');
                  $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expireds[0]->billing_cycle] ));
                  // dd($due_date);
                  // HistoryPay
                  $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
                  $history_pay->status = 'Active';
                  $history_pay->method_gd_invoice = 'credit';
                  $history_pay->save();
                  // ghi log giao dịch
                  $data_log_payment = [
                    'history_pay_id' => $history_pay->id,
                    'before' => $credit->value,
                    'after' => $credit->value - $total,
                  ];
                  $this->log_payment->create($data_log_payment);
                  // credit
                  $credit = $this->credit->where('user_id', Auth::user()->id)->first();
                  $credit->value = $credit->value - $total;
                  $credit->save();
                  // Invoice
                  $invoice->due_date = $due_date;
                  $invoice->paid_date = $date_paid;
                  $invoice->status = 'paid';
                  $invoice->payment_method = 1;
                  $paid = $invoice->save();
                  // ghi vào tổng thu
                  if (!empty($this->total_price->first())) {
                      $total_price = $this->total_price->first();
                      $total_price->vps += $total;
                      $total_price->save();
                  } else {
                      $data_total_price = [
                          'vps' => $total,
                          'hosting' => '0',
                          'server' => '0',
                      ];
                      $this->total_price->create($data_total_price);
                  }
                  try {
                        $this->send_mail_finish_expired('vps',$order, $invoice ,$order_expireds);
                  } catch (\Exception $e) {
                        return 3;
                  }
                  return 3;
              }
              else {
                  $list_success = $reques_vcenter->list_success;
                  $list_error = $reques_vcenter->list_error;
                  $list_vps_success = [];
                  $list_vps_error = [];
                  foreach ($list_success as $key => $vm_id) {
                      $vps = $this->vps->where('vm_id', $vm_id)->first();
                      if (!empty($vps)) {
                          $list_vps_success[] = $vps->ip;
                      }
                  }
                  foreach ($list_error as $key => $vm_id) {
                      $vps = $this->vps->where('vm_id', $vm_id)->first();
                      if (!empty($vps)) {
                          $list_vps_error[] = $vps->ip;
                      }
                  }
                  $data = [
                    "error" => 1,
                    "idSuccess" => $this->expired_success($list_success, $invoice->order_expireds[0]->billing_cycle),
                    "idError" => $this->expired_error($list_error, $invoice->order_expireds[0]->billing_cycle),
                    "list_vps_success" => $list_vps_success,
                    "list_vps_error" => $list_vps_error,
                  ];
                  try {
                    $invoice->order->delete();
                    foreach ($invoice->history_pay as $key => $history_pay) {
                        $history_pay->delete();
                    }
                    foreach ($order_expireds as $key => $order_expired) {
                        $order_expired->delete();
                    }
                    $invoice->delete();
                  } catch (\Exception $e) {
                    return $data;
                  }
                  return $data;
              }
            }
            else {
              $history_pay->status = 'Pending';
              $history_pay->save();
              return 2;
            }
        }
        elseif ( $invoice->type == 'VPS US' || $invoice->type == ' US' ) {
          foreach ($order_expireds as $key => $order_expired) {
              $vps = $order_expired->vps;
              $date_star = $vps->next_due_date;
              $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expired->billing_cycle] ));

              // $vps->next_due_date = $due_date;
              // $vps->billing_cycle = $order_expired->billing_cycle;
              // $vps->save();

              $config_billing_DashBoard = config('billingDashBoard');
              $data_expired[] = [
                  'package' => (int) $this->checkTotalVpsExpire($vps, $order_expired->billing_cycle),
                  'leased_time' => $config_billing_DashBoard[$order_expired->billing_cycle],
                  'type' => 0,
                  'vm_id' => $vps->vm_id,
                  'end_date' => $due_date
              ];
          }
          // dd($data_expired);

          $reques_vcenter = $this->dashboard->expired_vps_us($data_expired); //request đên Vcenter
          //   $reques_vcenter = true;
          if ( $reques_vcenter ) {
              // $reques_vcenter->error == 0
              if ( $reques_vcenter->error == 0 ) {
                  foreach ($order_expireds as $key => $order_expired) {
                      $vps = $order_expired->vps;
                      $date_star = $vps->next_due_date;
                      $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expired->billing_cycle] ));

                      $vps->next_due_date = $due_date;
                      $vps->billing_cycle = $order_expired->billing_cycle;
                      if ( $vps->status_vps == 'expire' ) {
                          $vps->status_vps = 'on';
                      }
                      $vps->save();
                      try {
                        $data_log = [
                          'user_id' => Auth::user()->id,
                          'action' => 'thanh toán',
                          'model' => 'User/HistoryPay',
                          'description' => ' gia hạn VPS US <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$vps->billing_cycle],
                          'description_user' => ' gia hạn VPS US <a target="_blank" href="/order/check-invoices/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$vps->billing_cycle] . ' thành công',
                          'service' =>  $vps->ip,
                        ];
                        $this->log_activity->create($data_log);
                      } catch (\Exception $e) {
                        report($e);
                      }
                  }
                  $invoice = $this->detail_order->find($invoice_id);
                  $order = $this->order->find($invoice->order->id);
                  $total = $order->total;
                  $order->status = 'Finish';
                  $order->save();
                  $billing_cycle = $order_expireds[0]->billing_cycle;
                  $date_star = $invoice->created_at;
                  $date_paid = date('Y-m-d');
                  $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expireds[0]->billing_cycle] ));
                  // dd($due_date);
                  // HistoryPay
                  $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
                  $history_pay->status = 'Active';
                  $history_pay->method_gd_invoice = 'credit';
                  $history_pay->save();
                  // ghi log giao dịch
                  $data_log_payment = [
                    'history_pay_id' => $history_pay->id,
                    'before' => $credit->value,
                    'after' => $credit->value - $total,
                  ];
                  $this->log_payment->create($data_log_payment);
                  // credit
                  $credit = $this->credit->where('user_id', Auth::user()->id)->first();
                  $credit->value = $credit->value - $total;
                  $credit->save();
                  // Invoice
                  $invoice->due_date = $due_date;
                  $invoice->paid_date = $date_paid;
                  $invoice->status = 'paid';
                  $invoice->payment_method = 1;
                  $paid = $invoice->save();
                  // ghi vào tổng thu
                  if (!empty($this->total_price->first())) {
                      $total_price = $this->total_price->first();
                      $total_price->vps += $total;
                      $total_price->save();
                  } else {
                      $data_total_price = [
                          'vps' => $total,
                          'hosting' => '0',
                          'server' => '0',
                      ];
                      $this->total_price->create($data_total_price);
                  }
                  try {
                    $this->send_mail_finish_expired('vps',$order, $invoice , $order_expireds);
                  } catch (\Exception $e) {
                    return 3;
                  }
                  return 3;
              }
              else {
                  $list_success = $reques_vcenter->list_success;
                  $list_error = $reques_vcenter->list_error;
                  $list_vps_success = [];
                  $list_vps_error = [];
                  foreach ($list_success as $key => $vm_id) {
                      $vps = $this->vps->where('vm_id', $vm_id)->first();
                      if (!empty($vps)) {
                          $list_vps_success[] = $vps->ip;
                      }
                  }
                  foreach ($list_vps_error as $key => $vm_id) {
                      $vps = $this->vps->where('vm_id', $vm_id)->first();
                      if (!empty($vps)) {
                          $list_vps_error[] = $vps->ip;
                      }
                  }
                  $data = [
                    "error" => 1,
                    "idSuccess" => $this->expired_success($list_success, $invoice->order_expireds[0]->billing_cycle),
                    "idError" => $this->expired_error($list_error, $invoice->order_expireds[0]->billing_cycle),
                    "list_vps_success" => $list_vps_success,
                    "list_vps_error" => $list_vps_error,
                  ];
                  return $data;
              }
          }
          else {
              $history_pay->status = 'Pending';
              $history_pay->save();
              return 2;
          }
        }
        elseif ( $invoice->type == 'Proxy' ) {
          foreach ($order_expireds as $key => $order_expired) {
            $proxy = $order_expired->proxy;

            $config_billing_DashBoard = config('billingDashBoard');
            $data_expired[] = [
                'leased_time' => $config_billing_DashBoard[$order_expired->billing_cycle],
                'proxy_id' => $proxy->proxy_id,
            ];
          }
          $reques_vcenter = $this->dashboard->expired_proxy($data_expired); //request đên Vcenter
          // $reques_vcenter = true;
          // if ( $reques_vcenter ) {
          if ( $reques_vcenter->error == 0 ) {
            foreach ($order_expireds as $key => $order_expired) {
              $proxy = $order_expired->proxy;
              $date_star = $proxy->next_due_date;
              $due_date = date('Y-m-d', strtotime( $date_star . $billing[$order_expired->billing_cycle] ));

              $proxy->next_due_date = $due_date;
              $proxy->billing_cycle = $order_expired->billing_cycle;
              if ( $proxy->status == 'expire' ) {
                  $proxy->status = 'on';
              }
              $proxy->save();
              try {
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'thanh toán',
                  'model' => 'User/HistoryPay',
                  'description' => ' gia hạn Proxy <a target="_blank" href="/admin/proxy/detail/' . $proxy->id . '">' . $proxy->ip . '</a> '  . $billings[$proxy->billing_cycle],
                  'description_user' => ' gia hạn Proxy <a target="_blank" href="/order/check-invoices/' . $proxy->id . '">' . $proxy->ip . '</a> '  . $billings[$proxy->billing_cycle] . ' thành công',
                  'service' =>  $proxy->ip,
                ];
                $this->log_activity->create($data_log);
              } catch (\Exception $e) {
                report($e);
              }
            }
            $invoice = $this->detail_order->find($invoice_id);
            $order = $this->order->find($invoice->order->id);
            $total = $order->total;
            $order->status = 'Finish';
            $order->save();
            // dd($due_date);
            // HistoryPay
            $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
            $history_pay->status = 'Active';
            $history_pay->method_gd_invoice = 'credit';
            $history_pay->save();
            // credit
            $credit = $this->credit->where('user_id', Auth::user()->id)->first();
            // ghi log giao dịch
            $data_log_payment = [
              'history_pay_id' => $history_pay->id,
              'before' => $credit->value,
              'after' => $credit->value - $total,
            ];
            $this->log_payment->create($data_log_payment);
            // trừ tiền
            $credit->value = $credit->value - $total;
            $credit->save();
            // Invoice
            $invoice->paid_date = date('Y-m-d');
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            // ghi vào tổng thu
            try {
                $this->send_mail_finish_expired('proxy',$order, $invoice ,$order_expireds);
            } catch (\Exception $e) {
                return 3;
            }
            return 3;
          } else {
            $history_pay->status = 'Pending';
            $history_pay->save();
            return 2;
          }
        }
        else {
          $history_pay->status = 'Pending';
          $history_pay->save();
          return 1;
        }
    }

    public function checkTotalVpsExpire($vps, $billing_cycle)
    {
      $total = 0;
      if (!empty($vps)) {
          $product = $this->product->find($vps->product_id);
          if (!empty($vps->price_override)) {
              if ( $vps->billing_cycle == $billing_cycle ) {
                $total += $vps->price_override;
              } else {
                $billing_price = config('billingDashBoard');
                $total_price_1 = ($vps->price_override * $billing_price[$billing_cycle]) / $billing_price[$vps->billing_cycle];
                $total = (int) round( $total_price_1 , -3);
              }
          } else {
              $total += $product->pricing[$billing_cycle];
              if (!empty($vps->vps_config)) {
                  $addon_vps = $vps->vps_config;
                  $add_on_products = $this->get_addon_product_private(Auth::user()->id);
                  $pricing_addon = 0;
                  foreach ($add_on_products as $key => $add_on_product) {
                      if (!empty($add_on_product->meta_product->type_addon)) {
                          if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                            $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                          }
                          if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                            $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                          }
                          if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                            $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                          }
                      }
                  }
                  $total += $pricing_addon;
              }
          }
      }
      return  $total;
    }
    // tạo 1 order khi gia hạn thành công
    public function expired_success($list_vm_id, $billing_cycle)
    {
        $total = 0;
        $quantity = 0;
        $billing_price = config('billingDashBoard');
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        foreach ($list_vm_id as $key => $vm_id) {
            $vps = $this->vps->where('vm_id', $vm_id)->first();
            if (!empty($vps)) {
                $product = $this->product->find($vps->product_id);
                if (!empty($vps->price_override)) {
                    if ($vps->billing_cycle == $billing_cycle) {
                        $total += $vps->price_override;
                    } else {
                      $total_price_1 = ($vps->price_override * $billing_price[$billing_cycle]) / $billing_price[$vps->billing_cycle];
                      $total += (int) round( $total_price_1 , -3);
                    }
                } else {
                    $total += $product->pricing[$billing_cycle];
                    if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private(Auth::user()->id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                            if (!empty($add_on_product->meta_product->type_addon)) {
                                if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                  $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                  $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                  $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                                }
                            }
                        }
                        $total += $pricing_addon;
                    }
                }
            }
            $quantity++;
        }
        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Finish',
            'description' => 'expired vps',
            'type' => 'expired',
            'makh' => '',
        ];
        $location = ($vps->location == 'us') ? " US" : "";
        $order = $this->order->create($data_order);
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => 'VPS' . $location,
            'due_date' => $date,
            'paid_date' => date('Y-m-d'),
            'description' => 'Gia hạn VPS' . $location,
            'status' => 'paid',
            'sub_total' => $total / $quantity,
            'quantity' => $quantity,
            'user_id' => Auth::user()->id,
            'paid_date' => date('Y-m-d'),
            'addon_id' => '0',
            'payment_method' => 1,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        $data_history = [
            'user_id' => Auth::user()->id,
            'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '3',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Active',
            'method_gd_invoice' => 'credit',
            'detail_order_id' => $detail_order->id,
        ];
        $history_pay = $this->history_pay->create($data_history);
        // ghi log giao dịch
        $credit = $this->credit->where('user_id', Auth::user()->id)->first();
        if (!isset($credit)) {
            return false;
        }
        $data_log_payment = [
          'history_pay_id' => $history_pay->id,
          'before' => $credit->value,
          'after' => $credit->value - $total,
        ];
        $this->log_payment->create($data_log_payment);
        // credit
        $credit->value = $credit->value - $total;
        $credit->save();
        foreach ($list_vm_id as $key => $vm_id) {
            $vps = $this->vps->where('vm_id', $vm_id)->first();
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $vps->id,
                'billing_cycle' => $billing_cycle,
                'type' => 'vps',
            ];
            $this->order_expired->create($data_order_expired);

            $date_star = $vps->next_due_date;
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$billing_cycle] ));

            $vps->next_due_date = $due_date;
            $vps->status_vps = 'on';
            $vps->billing_cycle = $billing_cycle;
            $vps->save();
            // ghi log
            $billings = config('billing');
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'thanh toán',
              'model' => 'User/HistoryPay',
              'description' => ' gia hạn VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$billing_cycle],
              'description_user' => ' gia hạn VPS <a target="_blank" href="/order/check-invoices/' . $detail_order->id . '">' . $detail_order->id . '</a> '  . $billings[$billing_cycle] . ' thành công',
              'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log);
            // ghi log user
            $data_log_user = [
              'user_id' => $vps->user_id,
              'action' => 'gia hạn',
              'model' => 'User/Services',
              'description_user' => ' VPS <a target="_blank" href="/service/detail/' . $vps->id  . '?type=vps">' . $vps->ip . '</a>',
              'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log_user);
        }
        try {
          $this->send_mail_finish_expired('vps',$order, $detail_order , $detail_order->order_expireds);
        } catch (\Exception $e) {
          return $detail_order->id;
        }
        return $detail_order->id;
    }
    // tạo 1 order khi gia hạn thất bại
    public function expired_error($list_vm_id, $billing_cycle)
    {
        $total = 0;
        $quantity = 0;
        $billing_price = config('billingDashBoard');
        foreach ($list_vm_id as $key => $vm_id) {
            $vps = $this->vps->where('vm_id', $vm_id)->first();
            if (!empty($vps)) {
                $product = $this->product->find($vps->product_id);
                if (!empty($vps->price_override)) {
                    if ($vps->billing_cycle == $billing_cycle) {
                        $total += $vps->price_override;
                    } else {
                      $total_price_1 = ($vps->price_override * $billing_price[$billing_cycle]) / $billing_price[$vps->billing_cycle];
                      $total += (int) round( $total_price_1 , -3);
                    }
                } else {
                    $total += $product->pricing[$billing_cycle];
                    if (!empty($vps->vps_config)) {
                        $addon_vps = $vps->vps_config;
                        $add_on_products = $this->get_addon_product_private(Auth::user()->id);
                        $pricing_addon = 0;
                        foreach ($add_on_products as $key => $add_on_product) {
                            if (!empty($add_on_product->meta_product->type_addon)) {
                                if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                                  $pricing_addon += $addon_vps->cpu * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                                  $pricing_addon += $addon_vps->ram * $add_on_product->pricing[$billing_cycle];
                                }
                                if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                                  $pricing_addon += $addon_vps->disk * $add_on_product->pricing[$billing_cycle] / 10;
                                }
                            }
                        }
                        $total += $pricing_addon;
                    }
                }
            }
            $quantity++;
        }

        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            'description' => 'Gia hạn',//thiếu status gia hạn
            'type' => 'expired',
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $type_detail_order = 'VPS';
        $type_detail_order .= ($vps->location == 'us') ? " US" : "";
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => $type_detail_order,
            'due_date' => $date,
            'description' => 'Gia hạn',
            'status' => 'unpaid',
            'sub_total' => $total / $quantity,
            'quantity' => $quantity,
            'user_id' => Auth::user()->id,
            'addon_id' => 0,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        foreach ($list_vm_id as $key => $vm_id) {
            $vps = $this->vps->where('vm_id', $vm_id)->first();
            // Tạo lưu trữ khi gia hạn
            $data_order_expired = [
                'detail_order_id' => $detail_order->id,
                'expired_id' => $vps->id,
                'billing_cycle' => $billing_cycle,
                'type' => 'vps',
            ];
            $this->order_expired->create($data_order_expired);

            // if (!empty($vps)) {
            //     $vps->expired_id = $detail_order->id;
            //     $vps->billing_cycle = $billing_cycle;
            //     $vps->save();
            // }
        }
        // create history pay
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => Auth::user()->id,
            'ma_gd' => 'GDEXV' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '3',
            'method_gd' => 'invoice',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $history_pay = $this->history_pay->create($data_history);
        // gui mail
        if ($order && $detail_order && $data_history) {

            $email = $this->email->find($product->meta_product->email_expired);

            $billings = config('billing');
            // ghi log
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'đặt hàng',
              'model' => 'User/Order_Services',
              'description' => ' gia hạn VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
              'description_user' => ' gia hạn VPS <a target="_blank" href="/service/detail/' . $vps->id . '?type=vps">' . $vps->ip . '</a> '  . $billings[$data_order_expired['billing_cycle']],
              'service' =>  $vps->ip,
            ];
            $this->log_activity->create($data_log);

            $url = url('');
            $url = str_replace(['http://','https://'], ['', ''], $url);
            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', '' ,$billings[$data_order_expired['billing_cycle']], '', $order->created_at, '', '',number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total ,0,",",".") . ' VNĐ' , $quantity, '', 'token', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
            $data = [
                'content' => $content,
                'user_name' => $user->name,
            ];
            try {
              $this->send_mail(!empty($email->meta_email->subject) ? $email->meta_email->subject : 'Thư xác nhận gia hạn VPS', $data);
            } catch (\Exception $e) {
              return $detail_order->id;
            }

            return $detail_order->id;
        } else {
            return false;
        }
    }
    // Thanh toán khi order addon
    public function payment_addon($invoice_id)
    {
        // return true;
        $invoice = $this->detail_order->find($invoice_id);
        // HistoryPay
        $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
        if ( $history_pay->status == 'Active' ) {
          return true;
        }
        $order = $this->order->find($invoice->order->id);
        $total = $order->total;
        $order->status = 'Active';
        $order->save();
        $credit = $this->credit->where('user_id', Auth::user()->id)->first();
        if (!isset($credit)) {
            return false;
        }
        // invoice
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        foreach ($invoice->order_addon_vps as $key => $order_addon_vps) {
            $vps = $order_addon_vps->vps;
            $billing_cycle = $vps->billing_cycle;
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $date_star . $billing[$vps->billing_cycle] ));
            // dd($due_date);
            // HistoryPay
            $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
            $history_pay->status = 'Active';
            $history_pay->method_gd_invoice = 'credit';
            $history_pay->save();
            // ghi log giao dịch
            $data_log_payment = [
              'history_pay_id' => $history_pay->id,
              'before' => $credit->value,
              'after' => $credit->value - $total,
            ];
            $this->log_payment->create($data_log_payment);
            // credit
            $credit = $this->credit->where('user_id', Auth::user()->id)->first();
            $credit->value = $credit->value - $total;
            $credit->save();
            // Invoice
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();
            // ghi vào tổng thu
            if ( !empty($vps->price_override) ) {
                $pricing_addon = 0;
                $add_on_products = $this->get_addon_product_private(Auth::user()->id);
                foreach ($add_on_products as $key => $add_on_product) {
                    if (!empty($add_on_product->meta_product->type_addon)) {
                      if ($add_on_product->meta_product->type_addon == 'addon_cpu') {
                        $pricing_addon += $order_addon_vps->cpu * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_ram') {
                        $pricing_addon += $order_addon_vps->ram * $add_on_product->pricing[$vps->billing_cycle];
                      }
                      if ($add_on_product->meta_product->type_addon == 'addon_disk') {
                          $pricing_addon += $order_addon_vps->disk * $add_on_product->pricing[$vps->billing_cycle] / 10;
                      }
                    }
                }
                $vps->price_override += $pricing_addon;
                $vps->save();
            }
            if (!empty($this->total_price->first())) {
                $total_price = $this->total_price->first();
                $total_price->vps += $total;
                $total_price->save();
            } else {
                $data_total_price = [
                    'vps' => $total,
                    'hosting' => '0',
                    'server' => '0',
                ];
                $this->total_price->create($data_total_price);
            }

            $data_addon = [
                'mob' => 'upgrade', // action
                'name' => $vps->vm_id,  // vm_id
                'extra_cpu' => !empty($order_addon_vps->cpu) ? $order_addon_vps->cpu : 0, // so CPU them
                'extra_ram' => !empty($order_addon_vps->ram) ? $order_addon_vps->ram : 0, // so RAM them
                'extra_disk' => !empty($order_addon_vps->disk) ? $order_addon_vps->disk : 0, // so DISK them
                'package' => $total, // tong tien
                'type' => $vps->type_vps == 'vps' ? 0 : 5,
            ];
            $reques_vcenter = $this->dashboard->upgrade_vps($data_addon, $vps->vm_id, $vps); //request đên Vcenter
            // $reques_vcenter = true;
            if ($reques_vcenter) {
                $order->status = 'Finish';
                $order->save();
                $vps_config = $vps->vps_config;
                if (!empty($vps_config)) {
                    $vps_config->ram += $order_addon_vps->ram;
                    $vps_config->cpu += $order_addon_vps->cpu;
                    $vps_config->disk += $order_addon_vps->disk;
                    $vps_config->save();
                } else {
                    $data_config = [
                        'vps_id' => $vps->id,
                        'ram' => $order_addon_vps->ram,
                        'cpu' => $order_addon_vps->cpu,
                        'disk' => $order_addon_vps->disk,
                        'ip' => 0,
                    ];
                    $this->vps_config->create($data_config);
                }
                // Gữi email khi hoàn thành order addon
                $this->user_email = Auth::user()->email;
                $this->subject = 'Hoàn thành nâng cấp cấu hình VPS';
                $product_addons = $this->get_addon_product_private(Auth::user()->id);
                foreach ($product_addons as $key => $product_addon) {
                    if (!empty($product_addon->meta_product->type_addon)) {
                        if ($product_addon->meta_product->type_addon == 'addon_cpu') {
                            $product_addon_cpu = $product_addon;
                        }
                        if ($product_addon->meta_product->type_addon == 'addon_ram') {
                            $product_addon_ram = $product_addon;
                        }
                        if ($product_addon->meta_product->type_addon == 'addon_disk') {
                            $product_addon_disk = $product_addon;
                        }
                    }
                }
                if (!empty($order_addon_vps->month)) {
                    $time = $order_addon_vps->month . ' Tháng ' . $order_addon_vps->day . ' Ngày';
                } else {
                    $time = $order_addon_vps->day . ' Ngày';
                }
                $mail_system = $this->email->where('type', 3)->where('type_service', 'finish_addon_vps')->first();
                if ( isset($mail_system) ) {
                    $user = Auth::user();
                    $search = [ '{$user_id}', '{$user_name}',  '{$user_email}', '{$user_credit}', '{$url}', '{$token}', '{$ip}',
                        '{$services_username}', '{$services_password}', '{$total}', '{$addon_cpu}', '{$addon_ram}',
                        '{$addon_disk}', '{$addon_time}' ];
                    $replace = [ $user->id, $user->name, $user->email, !empty($user->credit->value) ? $user->credit->value : 0,
                        url(''), '', $vps->ip, $vps->user, $vps->password, number_format($total,0,",","."),
                        !empty($order_addon_vps->cpu) ? $order_addon_vps->cpu : 0, !empty($order_addon_vps->ram) ? $order_addon_vps->ram : 0,
                        !empty($order_addon_vps->disk) ? $order_addon_vps->disk : 0, $time];
                    $content = str_replace($search, $replace, !empty($mail_system->meta_email->content)? $mail_system->meta_email->content : '');
                    $data = [
                        'content' => $content,
                        'subject' => !empty($mail_system->meta_email->subject)? $mail_system->meta_email->subject : 'Hoàn thành đặt hàng nâng cấp cấu hình VPS',
                    ];
                }
                else {
                    $data = [
                        'name' => Auth::user()->name,
                        'email' => Auth::user()->email,
                        'amount' => $total,
                        'ip' => $vps->ip,
                        'cpu' => !empty($order_addon_vps->cpu) ? $order_addon_vps->cpu : 0,
                        'ram' => !empty($order_addon_vps->ram) ? $order_addon_vps->ram : 0,
                        'disk' => !empty($order_addon_vps->disk) ? $order_addon_vps->disk : 0,
                        'product_cpu' => !empty($product_addon_cpu) ? $product_addon_cpu : 0,
                        'product_ram' => !empty($product_addon_ram) ? $product_addon_ram : 0,
                        'product_disk' => !empty($product_addon_disk) ? $product_addon_disk : 0,
                        'billing_cycle' => $billing_cycle,
                        'time' =>  $time ,
                        'subject' => 'Hoàn thành đặt hàng nâng cấp cấu hình VPS'
                    ];
                }
                $text_addon = !empty($order_addon_vps->cpu) ? $order_addon_vps->cpu : 0;
                $text_addon .= ' CPU - ';
                $text_addon .= !empty($order_addon_vps->ram) ? $order_addon_vps->ram : 0;
                $text_addon .= ' RAM - ';
                $text_addon .= !empty($order_addon_vps->disk) ? $order_addon_vps->disk : 0;
                $text_addon .= ' DISK';
                try {
                    // ghi log
                    $data_log = [
                      'user_id' => Auth::user()->id,
                      'action' => 'thanh toán',
                      'model' => 'User/HistoryPay',
                      'description' => ' hóa đơn addon VPS <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a> ' . $text_addon ,
                      'description_user' => ' hóa đơn addon VPS <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> thành công' ,
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log);
                    // ghi log user
                    $data_log_user = [
                      'user_id' => $vps->user_id,
                      'action' => 'cấu hình thêm',
                      'model' => 'User/Services',
                      'description_user' => ' VPS <a target="_blank" href="/service/detail/' . $vps->id  . '?type=vps">' . $vps->ip . '</a> ' . $text_addon,
                      'service' =>  $vps->ip,
                    ];
                    $this->log_activity->create($data_log_user);

                    $data_tb_send_mail = [
                        'type' => 'finish_order_addon_vps',
                        'type_service' => 'vps',
                        'user_id' => $vps->user_id,
                        'status' => false,
                        'content' => serialize($data),
                    ];
                    $this->send_mail->create($data_tb_send_mail);
                } catch (\Exception $e) {
                    report($e);
                    return $paid;
                }


            }
        }
        return $paid;
    }

    // check credit khi order
    public function check_credit_order($qtt, $sub_total)
    {
        $credit = $this->credit->where('user_id', Auth::user()->id)->first();
        if ( $credit->value >= ($qtt * $sub_total) ) {
           return true;
        }
        return false;
    }

    public function loadAddon($type)
    {
        $products = [];
        $user = $this->user->find(Auth::user()->id);
        // dd($user);
        if ($user->group_user_id != 0) {
            $group_user = $user->group_user;
            $group_products = $group_user->group_products;
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        } else {
            $group_products = $this->group_product->where('private', 0)->get();
            foreach ($group_products as $key => $group_product) {
                if ($this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->count() > 0) {
                    $products = $this->product->where('type_product', 'Addon-VPS')->where('group_product_id', $group_product->id)->with('meta_product', 'pricing')->get();
                }
            }
        }
        return $products;
    }

    function rand_string() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*";
        $size = strlen( $chars );
        $str = '';
        for( $i = 0; $i < 16; $i++ ) {
            $str .= $chars[ rand( 0, $size - 1 ) ];
        }
        return $str;
    }

    public function send_mail_finish($type, $order , $detail_order, $list_sevices)
    {
        $billings = config('billing');
        $product = $this->product->find($list_sevices[0]->product_id);
        $email = $this->email->find($product->meta_product->email_id);
        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $add_on =  '';
        if (!empty($detail_order->addon_id)) {
            $add_on = $this->product->find($list_sevices[0]->addon_id);
        }
        if ($type == 'vps') {
            foreach ($list_sevices as $key => $services) {
                $type_os = $services->os;
                if ($type_os == 1) {
                    $type_os = 'Windows 10 64bit';
                }
                elseif ($type_os == 2 || $type_os == 30) {
                    $type_os = 'Windows Server 2012 R2';
                }
                elseif ($type_os == 3) {
                    $type_os = 'Windows Server 2016';
                }
                elseif ($type_os == 31) {
                    $type_os = 'Linux Ubuntu-20.04';
                }
                elseif ($type_os == 15) {
                    $type_os = 'Windows Server 2019';
                }
                else {
                    $type_os = 'Linux CentOS 7 64bit';
                }

                $user = $this->user->find($services->user_id);
                $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' , $detail_order->quantity, $type_os, '', $url];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                try {
                    $data = [
                        'content' => $content,
                        'user_name' => $user->name,
                        'subject' => $email->meta_email->subject,
                    ];
                    $data_tb_send_mail = [
                        'type' => 'mail_finish_order_vps',
                        'type_service' => 'vps',
                        'user_id' => $user->id,
                        'status' => false,
                        'content' => serialize($data),
                    ];
                    $this->send_mail->create($data_tb_send_mail);
                } catch (\Exception $e) {
                    report($e);
                }
            }
        }
        elseif ($type == 'proxy') {
            foreach ($list_sevices as $key => $services) {
                $type_os = 'Linux CentOS 7 64bit';

                $user = $this->user->find($services->user_id);
                $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}', '{$port}'];
                $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->username, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' , $detail_order->quantity, $type_os, '', $url, $services->port];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                try {
                    $data = [
                        'content' => $content,
                        'user_name' => $user->name,
                        'subject' => $email->meta_email->subject,
                    ];
                    $data_tb_send_mail = [
                        'type' => 'mail_finish_order_proxy',
                        'type_service' => 'proxy',
                        'user_id' => $user->id,
                        'status' => false,
                        'content' => serialize($data),
                    ];
                    $this->send_mail->create($data_tb_send_mail);
                } catch (\Exception $e) {
                    report($e);
                }
            }
        }
        elseif ($type == 'nat-vps') {
            foreach ($list_sevices as $key => $services) {
                $type_os = $services->os;
                if ($type_os == 1) {
                    $type_os = 'Windows 10 64bit';
                }
                elseif ($type_os == 2 || $type_os == 30) {
                    $type_os = 'Windows Server 2012 R2';
                }
                elseif ($type_os == 3) {
                    $type_os = 'Windows Server 2016';
                }
                elseif ($type_os == 31) {
                    $type_os = 'Linux Ubuntu-20.04';
                }
                elseif ($type_os == 15) {
                    $type_os = 'Windows Server 2019';
                }
                else {
                    $type_os = 'Linux CentOS 7 64bit';
                }

                $user = $this->user->find($services->user_id);
                $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' , $detail_order->quantity, $type_os, '', $url];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                try {
                    $data = [
                        'content' => $content,
                        'user_name' => $user->name,
                        'subject' => $email->meta_email->subject,
                    ];
                    $data_tb_send_mail = [
                        'type' => 'mail_finish_order_vps',
                        'type_service' => 'vps',
                        'user_id' => $user->id,
                        'status' => false,
                        'content' => serialize($data),
                    ];
                    $this->send_mail->create($data_tb_send_mail);
                } catch (\Exception $e) {
                    report($e);
                }


            }
        }
        elseif ($type == 'hosting') {
            foreach ($list_sevices as $key => $services) {
                  $user = $this->user->find($services->user_id);
                  if (!empty($services->server_hosting_id)) {
                     $link_control_panel = '<a href="https://' . $services->server_hosting->host . ':' . $services->server_hosting->port . '" target="_blank" >https://'. $services->server_hosting->host . ':' . $services->server_hosting->port . '</a>' ;
                  } else {
                     $link_control_panel = '<a href="https://daportal.cloudzone.vn:2222/" target="_blank">https://daportal.cloudzone.vn:2222/</a>';
                  }
                  $search = ['{$url_control_panel}' , '{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}', '{$url_control_panel}'];
                  $replace = [$link_control_panel, !empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url, $link_control_panel];
                  $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                  try {
                    $data = [
                        'content' => $content,
                        'user_name' => $user->name,
                        'subject' => $email->meta_email->subject
                    ];

                    $data_tb_send_mail = [
                      'type' => 'mail_finish_order_hosting',
                      'type_service' => 'hosting',
                      'user_id' => $user->id,
                      'status' => false,
                      'content' => serialize($data),
                    ];
                    $this->send_mail->create($data_tb_send_mail);
                  } catch (\Exception $e) {
                    report($e);
                  }
            }
        }
        elseif ( $type == 'server' ) {
          $user = Auth::user();
          // CPU
          $server_cpu = $product->meta_product->cpu . ' (' . $product->meta_product->cores . ')';
          // DISK
          $server_disk = $product->meta_product->disk . ' ';
          $server_disk .= $this->get_addon_disk_server($list_sevices[0]->id);
          // RAM
          $server_ram = $product->meta_product->memory;
          if ( !empty( $list_sevices[0]->server_config->ram ) ) {
            foreach ($list_sevices[0]->server_config_rams as $key => $server_config_ram) {
              $product_addon_ram = $server_config_ram->product;
            }
            $server_ram .= ' + Addon ' . $product_addon_ram->name . '';
          }
          // IP
          $server_ip = $product->meta_product->ip . ' IP Public';
          if ( !empty( $list_sevices[0]->server_config->ip ) ) {
            foreach ($list_sevices[0]->server_config_ips as $key => $server_config_ip) {
              $product_addon_ip = $server_config_ip->product;
            }
            $server_ip .= ' + Addon ' . $product_addon_ip->name . '';
          }

          $search = ['{$user_name}',  '{$user_email}' , '{$order_id}',  '{$product_name}', '{$billing_cycle}',
            '{$order_created_at}', '{$sub_total}', '{$total}', '{$qtt}', '{$server_os}', '{$url}', '{$server_cpu}', '{$server_ram}',
            '{$server_disk}', '{$server_qtt_ip}', '{$server_raid}', '{$server_datacenter}', '{$server_management}', '{$next_due_date}'
          ];
          $replace = [ $user->name,  $user->email , $order->id,  $product->name ,$billings[$list_sevices[0]->billing_cycle], $order->created_at,
            number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' ,
            $detail_order->quantity, $list_sevices[0]->os, $url, $server_cpu, $server_ram, $server_disk,
            $server_ip, $list_sevices[0]->raid, $list_sevices[0]->location, $list_sevices[0]->server_management,
            date('d-m-Y', strtotime($list_sevices[0]->next_due_date))
          ];
          $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
          try {
              $data = [
                  'content' => $content,
                  'user_name' => $user->name,
                  'subject' => $email->meta_email->subject,
              ];
              $data_tb_send_mail = [
                  'type' => 'mail_finish_order_server',
                  'type_service' => 'server',
                  'user_id' => $user->id,
                  'status' => false,
                  'content' => serialize($data),
              ];
              $this->send_mail->create($data_tb_send_mail);
          } catch (\Exception $e) {
              report($e);
          }
        }
        elseif ( $type == 'proxy' ) {
          foreach ($list_sevices as $key => $services) {
            $type_os = $services->os;
            if ($type_os == 1) {
                $type_os = 'Windows 10 64bit';
            }
            elseif ($type_os == 2 || $type_os == 30) {
                $type_os = 'Windows Server 2012 R2';
            }
            elseif ($type_os == 3) {
                $type_os = 'Windows Server 2016';
            }
            elseif ($type_os == 31) {
                $type_os = 'Linux Ubuntu-20.04';
            }
            elseif ($type_os == 15) {
                $type_os = 'Windows Server 2019';
            }
            else {
                $type_os = 'Linux CentOS 7 64bit';
            }

            $user = $this->user->find($services->user_id);
            $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' , $detail_order->quantity, $type_os, '', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

            try {
                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'subject' => $email->meta_email->subject,
                ];
                $data_tb_send_mail = [
                    'type' => 'mail_finish_order_proxy',
                    'type_service' => 'proxy',
                    'user_id' => $user->id,
                    'status' => false,
                    'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);
            } catch (\Exception $e) {
                report($e);
            }
          }
        }
        elseif ( $type == 'colo' ) {
          $user = Auth::user();
          $colocation_ip = $product->meta_product->ip . ' IP Public';
          $colocation_config_ip = !empty($list_sevices[0]->colocation_config_ips[0]) ? $list_sevices[0]->colocation_config_ips[0] : null;
          if ( !empty($colocation_config_ip) ) {
            $colocation_ip .= ' + Addon ' . $colocation_config_ip->product->meta_product->ip . ' IP Public';
          }
          $search = ['{$user_name}',  '{$user_email}' , '{$order_id}',  '{$product_name}', '{$billing_cycle}',
            '{$order_created_at}', '{$sub_total}', '{$total}', '{$qtt}', '{$url}', '{$type_colocation}', '{$colocation_datacenter}',
             '{$next_due_date}', '{$colocation_ip}'
          ];
          $replace = [ $user->name,  $user->email , $order->id,  $product->name ,$billings[$list_sevices[0]->billing_cycle], $order->created_at,
            number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' ,
            $detail_order->quantity, $url, $list_sevices[0]->type_colo, $list_sevices[0]->location,
            date('d-m-Y', strtotime($list_sevices[0]->next_due_date)), $colocation_ip
          ];
          $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
          try {
              $data = [
                  'content' => $content,
                  'user_name' => $user->name,
                  'subject' => $email->meta_email->subject,
              ];
              $data_tb_send_mail = [
                  'type' => 'mail_finish_order_colo',
                  'type_service' => 'colo',
                  'user_id' => $user->id,
                  'status' => false,
                  'content' => serialize($data),
              ];
              $this->send_mail->create($data_tb_send_mail);
          } catch (\Exception $e) {
              report($e);
          }
        }
    }

    public function send_mail_finish_expired($type, $order , $detail_order, $order_expireds)
    {
        $billings = config('billing');
        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $add_on =  '';
        if ($type == 'vps') {
            foreach ($order_expireds as $key => $order_expired) {
                $services = $order_expired->vps;
                $product = $this->product->find($services->product_id);
                $email = $this->email->find($product->meta_product->email_expired_finish);

                $user = $this->user->find($services->user_id);
                $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' , $detail_order->quantity, $services->os, '', $url];
                $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                try {
                    $data = [
                        'content' => $content,
                        'user_name' => $user->name,
                        'subject' => $email->meta_email->subject
                    ];
                    $data_tb_send_mail = [
                        'type' => 'mail_finish_order_expire_vps',
                        'type_service' => 'vps',
                        'user_id' => $user->id,
                        'status' => false,
                        'content' => serialize($data),
                    ];
                    $this->send_mail->create($data_tb_send_mail);
                } catch (\Exception $e) {
                    report($e);
                }

            }
        }
        elseif ($type == 'hosting') {
            foreach ($order_expireds as $key => $order_expired) {
                  $services = $order_expired->hosting;
                  $product = $this->product->find($services->product_id);
                  $email = $this->email->find($product->meta_product->email_expired_finish);
                  $user = $this->user->find($services->user_id);
                  $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                  $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url];
                  $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                  try {
                    $data = [
                        'content' => $content,
                        'user_name' => $user->name,
                        'subject' => $email->meta_email->subject
                    ];
                    $data_tb_send_mail = [
                        'type' => 'mail_finish_order_expire_hosting',
                        'type_service' => 'hosting',
                        'user_id' => $user->id,
                        'status' => false,
                        'content' => serialize($data),
                    ];
                    $this->send_mail->create($data_tb_send_mail);
                  } catch (\Exception $e) {
                    report($e);
                  }


            }
        }
        elseif ($type == 'email_hosting') {
            foreach ($order_expireds as $key => $order_expired) {
                  $services = $order_expired->email_hosting;
                  $product = $this->product->find($services->product_id);
                  $email = $this->email->find($product->meta_product->email_expired_finish);
                  $user = $this->user->find($services->user_id);
                  $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                  $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url];
                  $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

                  $this->user_send_email = $this->user->find($services->user_id)->email;
                  $this->subject = !empty($email->meta_email->subject) ? $email->meta_email->subject : 'Gia hạn dịch vụ';

                  $data = [
                      'content' => $content,
                      'user_name' => $user->name,
                      'user_id' => $user->id,
                      'user_email' => $user->email,
                      'user_addpress' => $user->user_meta->address,
                      'user_phone' => $user->user_meta->user_phone,
                      'order_id' => $order->id,
                      'product_name' => $product->name,
                      'domain' => $services->domain,
                      'ip' => '',
                      'billing_cycle' => $services->billing_cycle,
                      'next_due_date' => $services->next_due_date,
                      'order_created_at' => $order->created_at,
                      'services_username' => $services->user,
                      'services_password' => $services->password,
                      'sub_total' => $detail_order->sub_total,
                      'total' => $order->total,
                      'qtt' => $detail_order->quantity,
                      'os' => $services->os,
                      'token' => '',
                  ];

                  $mail = Mail::send('users.mails.orders', $data, function($message){
                      $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                      $message->to($this->user_send_email)->subject($this->subject);
                  });

                  try {
                    $data['type'] = 'Khách hàng hoàn thành thanh toán khi gia hạn Email Hosting';
                    $mail = Mail::send('users.mails.send_mail_expired_hosting_support', $data, function($message){
                        $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                        $message->to('uthienth92@gmail.com')->subject('Khách hàng hoàn thành thanh toán khi gia hạn Email Hosting');
                        $message->cc('support@cloudzone.vn')->subject('Khách hàng hoàn thành thanh toán khi gia hạn Email Hosting');
                    });
                  } catch (\Exception $e) {

                  }


            }
        }
        elseif ($type == 'server') {
          $user = Auth::user();
          foreach ($order_expireds as $key => $order_expired) {
            $server = $order_expired->server;
            $product = $server->product;
            $email = $this->email->find($product->meta_product->email_expired_finish);
            $server_cpu = $product->meta_product->cpu . ' (' . $product->meta_product->cores . ')';
            $server_disk = $product->meta_product->disk . ' ';
            $server_disk .= $this->get_addon_disk_server($server->id);
            $search = ['{$user_name}',  '{$user_email}' , '{$order_id}',  '{$product_name}', '{$billing_cycle}',
              '{$order_created_at}', '{$sub_total}', '{$total}', '{$qtt}', '{$server_os}', '{$url}', '{$server_cpu}', '{$server_ram}',
              '{$server_disk}', '{$server_qtt_ip}', '{$server_raid}', '{$server_datacenter}', '{$server_management}', '{$server_ip}',
              '{$server_ip2}', '{$next_due_date}'
            ];
            $replace = [ $user->name,  $user->email , $order->id,  $product->name ,$billings[$server->billing_cycle], $order->created_at,
              number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' ,
              $detail_order->quantity, $server->os, $url, $server_cpu, $product->meta_product->memory, $server_disk,
              $product->meta_product->ip, $server->raid, $server->location, $server->server_management,
              $server->ip, $server->ip2, date('d-m-Y', strtotime($server->next_due_date))
            ];
            $content = str_replace($search, $replace, !empty($email->meta_email->content) ? $email->meta_email->content : '');
            try {
                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'subject' => $email->meta_email->subject,
                ];
                $data_tb_send_mail = [
                    'type' => 'mail_finish_order_expire_server',
                    'type_service' => 'server',
                    'user_id' => $user->id,
                    'status' => false,
                    'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);
            } catch (\Exception $e) {
                report($e);
            }
          }
        }
        elseif ($type == 'proxy') {
          foreach ($order_expireds as $key => $order_expired) {
            $services = $order_expired->proxy;
            $product = $this->product->find($services->product_id);
            $email = $this->email->find($product->meta_product->email_expired_finish);

            $user = $this->user->find($services->user_id);
            $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
            $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], date('d-m-Y', strtotime($services->next_due_date)), $order->created_at, $services->user, $services->password,number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' , $detail_order->quantity, $services->os, '', $url];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');

            try {
                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'subject' => $email->meta_email->subject
                ];
                $data_tb_send_mail = [
                    'type' => 'mail_finish_order_expire_proxy',
                    'type_service' => 'proxy',
                    'user_id' => $user->id,
                    'status' => false,
                    'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);
            } catch (\Exception $e) {
                report($e);
            }

          }
        }
        elseif ( $type == 'colo' ) {
          $user = Auth::user();
          foreach ($order_expireds as $key => $order_expired) {
            $colocation = $order_expired->colocation;
            $product = $colocation->product;
            $email = $this->email->find($product->meta_product->email_expired_finish);

            $search = ['{$user_name}',  '{$user_email}' , '{$order_id}',  '{$product_name}', '{$billing_cycle}',
              '{$order_created_at}', '{$sub_total}', '{$total}', '{$qtt}', '{$url}', '{$type_colocation}', '{$colocation_datacenter}',
              '{$next_due_date}', '{$colocation_ip}'
            ];
            $replace = [ $user->name,  $user->email , $order->id,  $product->name ,$billings[$colocation->billing_cycle], $order->created_at,
              number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' ,
              $detail_order->quantity, $url, $colocation->type_colo, $colocation->location,
              date('d-m-Y', strtotime($colocation->next_due_date)), $colocation->ip
            ];
            $content = str_replace($search, $replace, !empty($email->meta_email->content)? $email->meta_email->content : '');
            try {
                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'subject' => $email->meta_email->subject,
                ];
                $data_tb_send_mail = [
                    'type' => 'mail_finish_payment_colo',
                    'type_service' => 'colo',
                    'user_id' => $user->id,
                    'status' => false,
                    'content' => serialize($data),
                ];
                $this->send_mail->create($data_tb_send_mail);
            } catch (\Exception $e) {
                report($e);
            }
          }
        }
    }



    public function payment_off($type, $invoice_id)
    {
        try {
            $invoice = $this->detail_order->find($invoice_id);
            $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
            $history_pay->status = 'confirm';
            $history_pay->method_gd_invoice = $type;
            $history_pay->save();
            // ghi log
            if ($type == 'pay_in_office') {
               $text_type = 'Trực tiếp tại văn phòng';
            }
            elseif ($type == 'momo') {
               $text_type = 'MoMo';
            }
            elseif ($type == 'bank_transfer_vietcombank') {
               $text_type = 'VietComBank';
            }
            elseif ($type == 'bank_transfer_techcombank') {
               $text_type = 'Techcombank';
            }
            if ($invoice->type == 'VPS') {
                 $data_log = [
                   'user_id' => Auth::user()->id,
                   'action' => 'xác nhận',
                   'model' => 'User/Confirm_HistoryPay',
                   'description' => ' thanh toán đơn đặt hàng VPS <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> qua ' . $text_type,
                   'description_user' => ' thanh toán đơn đặt hàng VPS <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> qua ' . $text_type,
                 ];
                 $this->log_activity->create($data_log);
            }
            elseif ($invoice->type == 'Hosting') {
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'xác nhận',
                  'model' => 'User/Confirm_HistoryPay',
                  'description' => ' thanh toán đơn đặt hàng Hosting <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> qua ' . $text_type,
                  'description_user' => ' thanh toán đơn đặt hàng Hosting <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> qua ' . $text_type,
                ];
                $this->log_activity->create($data_log);
            }
            elseif ($invoice->type == 'Email Hosting') {
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'xác nhận',
                  'model' => 'User/Confirm_HistoryPay',
                  'description' => ' thanh toán đơn đặt hàng Email Hosting <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> qua ' . $text_type,
                  'description_user' => ' thanh toán đơn đặt hàng Email Hosting <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> qua ' . $text_type,
                ];
                $this->log_activity->create($data_log);
            }
            elseif ($invoice->type == 'NAT-VPS') {
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'xác nhận',
                  'model' => 'User/Confirm_HistoryPay',
                  'description' => ' thanh toán đơn đặt hàng NAT-VPS <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> qua ' . $text_type,
                  'description_user' => ' thanh toán đơn đặt hàng NAT-VPS <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> qua ' . $text_type,
                ];
                $this->log_activity->create($data_log);
            }
            elseif ($invoice->type == 'Server') {
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'xác nhận',
                  'model' => 'User/Confirm_HistoryPay',
                  'description' => ' thanh toán đơn đặt hàng Server <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> qua ' . $text_type,
                  'description_user' => ' thanh toán đơn đặt hàng Server <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> qua ' . $text_type,
                ];
                $this->log_activity->create($data_log);
            }
            elseif ($invoice->type == 'Colocation') {
                $data_log = [
                  'user_id' => Auth::user()->id,
                  'action' => 'xác nhận',
                  'model' => 'User/Confirm_HistoryPay',
                  'description' => ' thanh toán đơn đặt hàng Colocation <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> qua ' . $text_type,
                  'description_user' => ' thanh toán đơn đặt hàng Colocation <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> qua ' . $text_type,
                ];
                $this->log_activity->create($data_log);
            }

            try {
              $data = [
                'user_name' => Auth::user()->name,
                'user_email' => Auth::user()->email,
                'invoice_id' => $invoice_id,
                'history_pay_id' => $history_pay->id,
                'magd' => $history_pay->ma_gd,
                'type' => $text_type,
              ];
              $mail = Mail::send('users.mails.send_mail_yeu_cau_expired_support', $data, function($message){
                  $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                  $message->to('uthienth92@gmail.com')->subject('Khách hàng yêu cầu thanh toán');
                  $message->cc('support@cloudzone.vn')->subject('Khách hàng yêu cầu thanh toán');
              });
            } catch (\Exception $e) {
                report($e);
                return $history_pay->id;
            }

            return $history_pay->id;
        } catch (\Exception $e) {
            return false;
        }

    }

    public function payment_upgrade_off($type, $invoice_id)
    {
        try {
            $invoice = $this->detail_order->find($invoice_id);
            $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
            $history_pay->status = 'confirm';
            $history_pay->method_gd_invoice = $type;
            $history_pay->save();
            // ghi log
            if ($type == 'pay_in_office') {
              $text_type = 'Trực tiếp tại văn phòng';
            }
            elseif ($type == 'momo') {
              $text_type = 'MoMo';
            }
            elseif ($type == 'bank_transfer_vietcombank') {
              $text_type = 'VietComBank';
            }
            elseif ($type == 'bank_transfer_techcombank') {
              $text_type = 'Techcombank';
            }
            $data_log = [
              'user_id' => Auth::user()->id,
              'action' => 'xác nhận',
              'model' => 'User/Confirm_HistoryPay',
              'description' => ' thanh toán gói nâng cấp Hosting <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> qua ' . $text_type,
              'description_user' => ' thanh toán gói nâng cấp Hosting <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> qua ' . $text_type,
            ];
            $this->log_activity->create($data_log);

            try {
              $data = [
                'user_name' => Auth::user()->name,
                'user_email' => Auth::user()->email,
                'invoice_id' => $invoice_id,
                'history_pay_id' => $history_pay->id,
                'magd' => $history_pay->ma_gd,
              ];
              $mail = Mail::send('users.mails.send_mail_yeu_cau_expired_support', $data, function($message){
                  $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                  $message->to('uthienth92@gmail.com')->subject('Khách hàng yêu cầu thanh toán nâng cấp hosting');
                  $message->cc('support@cloudzone.vn')->subject('Khách hàng yêu cầu thanh toán nâng cấp hosting');
              });
            } catch (\Exception $e) {
                return $history_pay->id;
            }

            return $history_pay->id;
        } catch (\Exception $e) {
            return false;
        }

    }

    public function payment_change_ip_form($invoice_id)
    {
        $invoice = $this->detail_order->find($invoice_id);
        $credit = $this->credit->where('user_id', Auth::user()->id)->first();
        if (!isset($credit)) {
          return false;
        }
        $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
        if ( $history_pay->status == 'Active' ) {
          return 3;
        }
        $check_credit = $this->check_credit($invoice->id);
        if (!$check_credit) {
            return 5;
        }

        $order = $this->order->find($invoice->order->id);
        $total = $order->total;
        $order->status = 'Active';
        $order->save();
        // invoice
        $billing = [
            'monthly' => '1 Month',
            'twomonthly' => '2 Month',
            'quarterly' => '3 Month',
            'semi_annually' => '6 Month',
            'annually' => '1 Year',
            'biennially' => '2 Year',
            'triennially' => '3 Year'
        ];
        // kiểm tra tài khoản đả order chưa
        $user = Auth::user();
        if ( $order->type  == 'vps_us' ) {
            if ( !empty($invoice->order_change_vps) ) {
                foreach ($invoice->order_change_vps as $key => $order_change_vps) {
                    $vps = $order_change_vps->vps;
                    $billing_cycle = $vps->billing_cycle;
                    $date_star = $invoice->created_at;
                    $date_paid = date('Y-m-d');
                    $due_date = date('Y-m-d', strtotime( $date_star . $billing[$vps->billing_cycle] ));

                    $reques_vcenter = $this->dashboard->change_ip_vps_us($vps);
                    if ( $reques_vcenter ) {
                        // order
                        $order->status = 'Finish';
                        $order->save();
                        // payment
                        $history_pay->status = 'Active';
                        $history_pay->method_gd_invoice = 'credit';
                        $history_pay->save();
                        // ghi log giao dịch
                        $data_log_payment = [
                            'history_pay_id' => $history_pay->id,
                            'before' => $credit->value,
                            'after' => $credit->value - $total,
                        ];
                        $this->log_payment->create($data_log_payment);
                        // trừ tiền
                        $credit->value = $credit->value - $total;
                        $credit->save();
                        $invoice->due_date = $due_date;
                        $invoice->paid_date = $date_paid;
                        $invoice->status = 'paid';
                        $invoice->payment_method = 1;
                        $paid = $invoice->save();
                        //
                        $vps->status_vps = 'change_ip';
                        $vps->save();
                        // Tạo log
                        $data_log = [
                           'user_id' => Auth::user()->id,
                           'action' => 'thanh toán',
                           'model' => 'User/HistoryPay',
                           'description' => 'hóa đơn đổi IP VPS US <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> thành công',
                           'description_user' => 'hóa đơn đổi IP VPS US <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> thành công',
                        ];
                        $this->log_activity->create($data_log);
                        // Gữi email khi hoàn thành order change IP
                        try {
                            $data = [
                                'name' => Auth::user()->name,
                                'email' => Auth::user()->email,
                                'amount' => $total,
                                'ip' => $vps->ip,
                                'amount' => $invoice->sub_total,
                                'subject' => 'Hoàn thành thanh toán thay đổi IP cho VPS US',
                            ];
                            $data_tb_send_mail = [
                                'type' => 'mail_payment_change_ip',
                                'type_service' => 'vps_us',
                                'user_id' => $user->id,
                                'status' => false,
                                'content' => serialize($data),
                            ];
                            $this->send_mail->create($data_tb_send_mail);
                        } catch (Exception $e) {
                            report($e);
                        }
                    } else {
                        return 2;
                    }
                }
                return 9;
            } else {
                return 2;
            }
        }
        else {
            foreach ($invoice->order_change_vps as $key => $order_change_vps) {
                $vps = $order_change_vps->vps;
                $billing_cycle = $vps->billing_cycle;
                $date_star = $invoice->created_at;
                $date_paid = date('Y-m-d');
                $due_date = date('Y-m-d', strtotime( $date_star . $billing[$vps->billing_cycle] ));
                // dd($due_date);
                // HistoryPay
                $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
                $history_pay->status = 'Active';
                $history_pay->method_gd_invoice = 'credit';
                $history_pay->save();
                // ghi log giao dịch
                $data_log_payment = [
                  'history_pay_id' => $history_pay->id,
                  'before' => $credit->value,
                  'after' => $credit->value - $total,
                ];
                $this->log_payment->create($data_log_payment);
                // credit
                $credit->value = $credit->value - $total;
                $credit->save();
                // Invoice
                $invoice->due_date = $due_date;
                $invoice->paid_date = $date_paid;
                $invoice->status = 'paid';
                $invoice->payment_method = 1;
                $paid = $invoice->save();
                // ghi vào tổng thu
                if (!empty($this->total_price->first())) {
                    $total_price = $this->total_price->first();
                    $total_price->vps += $total;
                    $total_price->save();
                } else {
                    $data_total_price = [
                        'vps' => $total,
                        'hosting' => '0',
                        'server' => '0',
                    ];
                    $this->total_price->create($data_total_price);
                }
                $ip = $vps->ip;
                $reques_vcenter = $this->dashboard->change_ip($vps, $invoice->sub_total, $order_change_vps); //request đên Vcenter
                // $reques_vcenter= true;
                if ($reques_vcenter) {
                    $order->status = 'Finish';
                    $order->save();
                    // Gữi email khi hoàn thành order change IP
                    $this->user_email = Auth::user()->email;
                    $this->subject = 'Hoàn thành thanh toán thay đổi IP cho VPS';


                    $data = [
                        'name' => Auth::user()->name,
                        'email' => Auth::user()->email,
                        'amount' => $total,
                        'ip' => $ip,
                        'ip_change' => $vps->ip,
                        'amount' => $invoice->sub_total,
                    ];

                    try {
                        // ghi log
                        $data_log = [
                          'user_id' => Auth::user()->id,
                          'action' => 'thanh toán',
                          'model' => 'User/HistoryPay',
                          'description' => ' đổi IP VPS ' . $ip . ' thành <a target="_blank" href="/admin/vps/detail/' . $vps->id . '">' . $vps->ip . '</a>',
                          'description_user' => ' hóa đơn đổi IP VPS <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">' . $invoice->id . '</a> thành công',
                          'service' => $ip . ' thành ' . $vps->ip,
                        ];
                        $this->log_activity->create($data_log);
                        // ghi log user
                        $data_log_user = [
                          'user_id' => $vps->user_id,
                          'action' => 'đổi IP',
                          'model' => 'User/Services',
                          'description_user' => ' VPS ' . $ip . ' <a target="_blank" href="/service/detail/' . $vps->id  . '?type=vps">' . $vps->ip . '</a>',
                          'service' =>  $vps->ip,
                        ];
                        $this->log_activity->create($data_log_user);

                        $mail = Mail::send('users.mails.finish_order_change_ip_vps', $data, function($message){
                            $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                            $message->to($this->user_email)->subject($this->subject);
                        });
                        $data['type'] = 'Khách hàng hoàn thành đặt hàng thay đổi IP cho VPS';
                        $mail = Mail::send('users.mails.send_mail_change_ip_vps', $data, function($message){
                            $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                            $message->to('uthienth92@gmail.com')->subject('Khách hàng thanh toán đổi IP cho VPS');
                            $message->cc('support@cloudzone.vn')->subject('Khách hàng thanh toán đổi IP cho VPS');
                        });
                    } catch (\Exception $e) {
                        return $paid;
                    }


                }
            }
        }
        return $paid;
    }

    public function order_domain($data) {

        $unicode = array(
          'a'=>'á|à|ả|ã|ạ|ă|ắ|ặ|ằ|ẳ|ẵ|â|ấ|ầ|ẩ|ẫ|ậ|Á|À|Ả|Ã|Ạ|Ă|Ắ|Ặ|Ằ|Ẳ|Ẵ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ',
          'd'=>'đ|Đ',
          'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ',
          'i'=>'í|ì|ỉ|ĩ|ị|I|Ì|Í|Ị|Ỉ|Ĩ',
          'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|Ọ|Ó|Ò|Ỏ|Õ|Ô|Ồ|Ố|Ổ|Ỗ|Ộ|Ơ|Ờ|Ớ|Ở|Ỡ|Ợ',
          'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|Ú|Ù|Ủ|Ụ|Ũ|Ư|Ừ|Ứ|Ử|Ữ|Ự',
          'y'=>'ý|ỳ|ỷ|ỹ|ỵ|Ý|Ỳ|Ỷ|Ỹ|Ỵ',
        );
        $str_name = strtolower($data['ui-name']);
        foreach($unicode as $nonUnicode => $uni){
            $str_name = preg_replace( "/($uni)/i" , $nonUnicode , $str_name);
        }
        $user = $this->user->find(Auth::user()->id);
        $path = 'storage/cmnd/'. $user->id . '/';

        //Kiểm tra và lưu thông tin chứng minh nhân dân mặt trước (lưu mới hoặc lấy từ profile)
        if(!empty($data['cmnd_before'])) {
            $cmnd_before = preg_replace('/\s+/', '', $data['cmnd_before']->getClientOriginalName());
            $file_name_before = date('d-m-Y').'-'.'scan-before-' . $cmnd_before;
            $path2 = $data['cmnd_before']->storeAs('public/cmnd/' . $user->id , $file_name_before);
            $cmnd_before_storage_link = $path . $file_name_before;
        } elseif (!empty($data['cmnd_before_in_profile']) && empty($data['cmnd_before'])) {
            $cmnd_before_storage_link = $user->user_meta->cmnd_before;
        } else {
            return false;
        }

        //Kiểm tra và lưu thông tin chứng minh nhân dân mặt sau (lưu mới hoặc lấy từ profile)
        if(!empty($data['cmnd_after'])) {
            $cmnd_after = preg_replace('/\s+/', '', $data['cmnd_after']->getClientOriginalName());
            $file_name_after = date('d-m-Y').'-'.'scan-after-' . $cmnd_after;
            $path3 = $data['cmnd_after']->storeAs('public/cmnd/'. $user->id, $file_name_after);
            $cmnd_after_storage_link = $path . $file_name_after;
        } elseif (!empty($data['cmnd_after_in_profile']) && empty($data['cmnd_after'])) {
            $cmnd_after_storage_link = $user->user_meta->cmnd_after;
        } else {
            return false;
        }
        $product = $this->domain_product->find($data['product_id']);
        $total =  $product[$data['billing']];

        //create order
        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => $total,
            'status' => 'Pending',
            // 'description' => $data['description'],
        ];
        $order = $this->order->create($data_order);

        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => $data['type_product'],
            'due_date' => $date,
            'description' => !empty($data['description'])? $data['description'] : '',
            'status' => 'unpaid',
            'sub_total' => $total,
            'quantity' => '1',
            'user_id' => Auth::user()->id,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);

        //create order history
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODD' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '6',
            'method_gd' => 'domain',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);

        // vetify order
        $data_vetify_order = [
            'order_id' => $order->id,
            'token' => sha1(time()),
        ];
        $vetify = $this->order_vetify->create($data_vetify_order);

        //register domain
        if($detail_order && $data_order) {
            $data_domain = [
                'detail_order_id' => $detail_order->id,
                'user_id' => Auth::user()->id,
                'product_domain_id' => $data['product_id'],
                'status' => 'Pending',
                'billing_cycle' => $data['billing'],
                'customer_domain' => $data['customer-domain'],
                'owner_name' => $data['owner-name'],
                'owner_taxcode' => !empty($data['owner-taxcode']) ? $data['owner-taxcode'] : '',
                'ownerid_number' => !empty($data['ownerid-number']) ? $data['ownerid-number'] : '',
                'owner_address' => $data['owner-address'],
                'owner_email' => $data['owner-email'],
                'owner_phone' => $data['owner-phone'],
                'ui_name' => $data['ui-name'],
                'uiid_number' => !empty($data['uiid-number']) ? $data['uiid-number'] : '',
                'uiid_taxcode' => !empty($data['ui-taxcode']) ? $data['ui-taxcode'] : '',
                'ui_gender' => !empty($data['ui-gender']) ? $data['ui-gender'] : '',
                'ui_birthdate' => !empty($data['ui-birthdate']) ? Carbon::createFromFormat('d/m/Y', $data['ui-birthdate'])->format('Y-m-d') : NULL,
                'ui_address' => $data['ui-address'],
                'ui_province' => $data['ui-province'],
                'ui_email' => $data['ui-email'],
                'ui_phone' => $data['ui-phone'],
                'admin_name' => !empty($data['admin-name']) ? $data['admin-name'] : '',
                'adminid_number' => !empty($data['adminid-number']) ? $data['adminid-number'] : '',
                'admin_gender' => !empty($data['admin-gender']) ? $data['admin-gender'] : '',
                'admin_birthdate' => !empty($data['admin-birthdate']) ? Carbon::createFromFormat('d/m/Y', $data['admin-birthdate'])->format('Y-m-d') : NULL,
                'admin_address' => !empty($data['admin-address']) ? $data['admin-address'] : '',
                'admin_province' => !empty($data['admin-province']) ? $data['admin-province'] : '',
                'admin_email' => !empty($data['admin-email']) ? $data['admin-email'] : '',
                'admin_phone' => !empty($data['admin-phone']) ? $data['admin-phone'] : '',
                'domain' => $data['domain'],
                'domain_ext' => $data['domain-ext'],
                'domain_name' => $data['domain-name'],
                'domain_year' => $data['domain-year'],
                'password_domain' => $data['password-domain'],
                'cmnd_before' => $cmnd_before_storage_link,
                'cmnd_after' => $cmnd_after_storage_link,
            ];
            $create_domain = $this->domain->create($data_domain);
            if(!$create_domain) {
                return false;
            }
            $data_mail = [
                'user_name' => $user->name,
                'user_id' => $user->id,
                'user_email' => $user->email,
                'user_addpress' => $user->user_meta->address,
                'user_phone' => $user->user_meta->user_phone,
                'order_id' => $order->id,
                'order_created_at' => $order->created_at,
                'domain' => $data['domain'],
                'password_domain' => $data['password-domain'],
                'domain_year' => $data['domain-year'].' Năm',
                'total' => $total,
                'token' => $data_vetify_order['token'],
            ];
            $this->user_email = $user->email;
            $this->domain = $data_mail['domain'];

            // dd($this->user_email);
            try {
              $send_mail =  Mail::send('users.mails.order-domain-confirm', $data_mail, function($message){
                  $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                  $message->to($this->user_email)->subject('Xác thực đơn hàng Cloudzone - Domain '.$this->domain);
              });
            } catch (\Exception $e) {
              return $detail_order;
            }
            $billings = config('billing');

            // tạo log
            try {
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'đặt hàng',
                'model' => 'User/Order',
                'description' => ' Domain <a target="_blank" href="/admin/domain/detail/' . $create_domain->id . '">' . $create_domain->domain . '</a> ' . $billings[$create_domain->billing_cycle],
                'description_user' => ' Domain <a target="_blank" href="/order/check-invoices/' . $detail_order->id . '">' . $detail_order->id . '</a> ' . $billings[$create_domain->billing_cycle],
              ];
              $this->log_activity->create($data_log);
            } catch (\Exception $e) {
              return $detail_order;
            }

        } else {
            return false;
        }
        return $detail_order;

    }
    //domain khuyến mãi
    public function order_domain_promotion($data) {
        $user = $this->user->find(Auth::user()->id);
        $product = $this->domain_product->find($data['product_id']);
        // dd($total);
         //create order
        $data_order = [
            'user_id' => Auth::user()->id,
            'total' => 0,
            'status' => 'Finish',
            // 'description' => $data['description'],
        ];

        $order = $this->order->create($data_order);
        //create order detail
        $now = Carbon::now();
        $date = date('Y-m-d');
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => $data['type_product'],
            'due_date' => date('Y-m-d', strtotime($now->addYear()) ),
            'description' => !empty($data['description'])? $data['description'] : '',
            'status' => 'paid',
            'sub_total' => 0,
            'paid_date' => date('Y-m-d'),
            'payment_method' => '1',
            'quantity' => '1',
            'user_id' => Auth::user()->id,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        //create order history
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDODF' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '6',
            'method_gd' => 'domain',
            'date_gd' => date('Y-m-d'),
            'money'=> '0',
            'status' => 'Active',
            'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);
        //register domain
        if($detail_order && $order) {
            $data_domain = [
                'detail_order_id' => $detail_order->id,
                'user_id' => Auth::user()->id,
                'product_domain_id' => $data['product_id'],
                'status' => 'Pending',
                'billing_cycle' => $data['billing'],
                'customer_domain' => $data['customer-domain'],
                'owner_name' => $data['owner-name'],
                'owner_taxcode' => !empty($data['owner-taxcode']) ? $data['owner-taxcode'] : '',
                'ownerid_number' => !empty($data['ownerid-number']) ? $data['ownerid-number'] : '',
                'owner_address' => $data['owner-address'],
                'owner_email' => $data['owner-email'],
                'owner_phone' => $data['owner-phone'],
                'ui_name' => $data['ui-name'],
                'uiid_number' => !empty($data['uiid-number']) ? $data['uiid-number'] : '',
                'uiid_taxcode' => !empty($data['ui-taxcode']) ? $data['ui-taxcode'] : '',
                'ui_gender' => !empty($data['ui-gender']) ? $data['ui-gender'] : '',
                'ui_birthdate' => !empty($data['ui-birthdate']) ? Carbon::createFromFormat('d/m/Y', $data['ui-birthdate'])->format('Y-m-d') : NULL,
                'ui_address' => $data['ui-address'],
                'ui_province' => $data['ui-province'],
                'ui_email' => $data['ui-email'],
                'ui_phone' => $data['ui-phone'],
                'admin_name' => !empty($data['admin-name']) ? $data['admin-name'] : '',
                'adminid_number' => !empty($data['adminid-number']) ? $data['adminid-number'] : '',
                'admin_gender' => !empty($data['admin-gender']) ? $data['admin-gender'] : '',
                'admin_birthdate' => !empty($data['admin-birthdate']) ? Carbon::createFromFormat('d/m/Y', $data['admin-birthdate'])->format('Y-m-d') : NULL,
                'admin_address' => !empty($data['admin-address']) ? $data['admin-address'] : '',
                'admin_province' => !empty($data['admin-province']) ? $data['admin-province'] : '',
                'admin_email' => !empty($data['admin-email']) ? $data['admin-email'] : '',
                'admin_phone' => !empty($data['admin-phone']) ? $data['admin-phone'] : '',
                'domain' => $data['domain'],
                'domain_ext' => $data['domain-ext'],
                'domain_name' => $data['domain-name'],
                'domain_year' => 1,
                'password_domain' => $data['password-domain'],
                'status' => 'Active',
                'next_due_date' => Carbon::now()->addYear(),
                'promotion' => true,
            ];
            $create_domain = $this->domain->create($data_domain);
            if (!empty($data['event_id'])) {
                $event_promotion = $this->event_promotion->where('user_id', Auth::user()->id)->where('id',$data['event_id'] )->first();
                if (isset($event_promotion)) {
                  $event_promotion->child_id = $create_domain->id;
                  $event_promotion->save();
                } else {
                  return false;
                }
            } else {
                return false;
            }
            $request_panhanhoa = $this->domain_pa->registerDomain($create_domain, Carbon::now()->addYear()); //Request API PAVietNam
            //$request_panhanhoa->{'ReturnCode'} == 200
            // $request =  true;
            if($request_panhanhoa->{'ReturnCode'} == 200) {
                    $data_reg = [
                        'user_id' => Auth::user()->id,
                        'domain' => $create_domain->domain,
                        'due_date' => Carbon::now()->addYear(),
                        'total' => 'miễn phí'
                    ];
                    $mail = Mail::send('users.mails.finish_reg_domain', $data_reg, function($message){
                        $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                        $message->to(Auth::user()->email)->subject('Hoàn thành đăng ký domain');
                    });
                    $mail_to_admin = Mail::send('users.mails.finish_reg_domain_to_admin', $data_reg, function($message){
                        $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                        $message->to('tuansinhtk@gmail.com')->subject('Hoàn thành đăng ký domain free');
                        $message->cc('uthienth92@gmail.com')->subject('Hoàn thành đăng ký domain free');
                    });
                    return $detail_order;
                } else {
                    $data_reg = [
                        'user_id' => Auth::user()->id,
                        'domain' => $create_domain->domain,
                        'error_code' => $request_panhanhoa->{'ReturnCode'},
                        'error' => $request_panhanhoa->{'ReturnText'},
                    ];
                    // dd($data_error);
                    $mail = Mail::send('users.mails.error_reg_domain', $data_reg, function($message){
                        $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                        $message->to('tuansinhtk@gmail.com')->subject('Lỗi khi tạo domain');
                        $message->cc('uthienth92@gmail.com')->subject('Lỗi khi tạo domain');
                    });
                    return $detail_order;
                }


        } else {
            return false;
        }
        return $detail_order;

    }

    public function detail_domain($id) {
        return $this->domain->where('detail_order_id', $id)->first();
    }
    public function detail_product_domain($id) {
        return $this->domain_product->find($id);
    }

    public function payment_domain_off($type, $invoice_id)
    {
        try {
            $invoice = $this->detail_order->find($invoice_id);
            $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
            $history_pay->status = 'confirm';
            $history_pay->method_gd_invoice = $type;
            $history_pay->save();
            try {
              // ghi log
              if ($type == 'pay_in_office') {
                 $text_type = 'Trực tiếp tại văn phòng';
              }
              elseif ($type == 'momo') {
                 $text_type = 'MoMo';
              }
              elseif ($type == 'bank_transfer_vietcombank') {
                 $text_type = 'VietComBank';
              }
              elseif ($type == 'bank_transfer_techcombank') {
                 $text_type = 'Techcombank';
              }
              // tạo log
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'yêu cầu',
                'model' => 'User/Confirm_HistoryPay',
                'description' => ' thanh toán Domain <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> qua ' . $text_type,
                'description_user' => ' thanh toán Domain <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> qua ' . $text_type,
              ];
              $this->log_activity->create($data_log);

              $data = [
                'user_name' => Auth::user()->name,
                'user_email' => Auth::user()->email,
                'invoice_id' => $invoice_id,
                'history_pay_id' => $history_pay->id,
                'magd' => $history_pay->ma_gd,
                'total' => $invoice->sub_total,
                'type' => $invoice->type,
              ];
              $mail = Mail::send('users.mails.send_mail_reg_domain', $data, function($message){
                  $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                  $message->to('uthienth92@gmail.com')->subject('Khách hàng yêu cầu thanh toán');
              });
            } catch (\Exception $e) {
                return $history_pay->id;
            }

            return $history_pay->id;
        } catch (\Exception $e) {
            return false;
        }

    }
    public function payment_domain_expired_off($type, $invoice_id)
    {
        try {
            $invoice = $this->detail_order->find($invoice_id);
            $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
            $history_pay->status = 'confirm';
            $history_pay->method_gd_invoice = $type;
            $history_pay->save();
            try {
              // ghi log
              if ($type == 'pay_in_office') {
                 $text_type = 'Trực tiếp tại văn phòng';
              }
              elseif ($type == 'momo') {
                 $text_type = 'MoMo';
              }
              elseif ($type == 'bank_transfer_vietcombank') {
                 $text_type = 'VietComBank';
              }
              elseif ($type == 'bank_transfer_techcombank') {
                 $text_type = 'Techcombank';
              }
              // tạo log
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' => 'yêu cầu',
                'model' => 'User/Confirm_HistoryPay',
                'description' => ' thanh toán gia hạn Domain <a target="_blank" href="/admin/payment/detail/' . $history_pay->id . '">#' . $history_pay->id . '</a> qua ' . $text_type,
                'description_user' => ' thanh toán gia hạn Domain <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a> qua ' . $text_type,
              ];
              $this->log_activity->create($data_log);

              $data = [
                'user_name' => Auth::user()->name,
                'user_email' => Auth::user()->email,
                'invoice_id' => $invoice_id,
                'history_pay_id' => $history_pay->id,
                'magd' => $history_pay->ma_gd,
                'total' => $invoice->sub_total,
                'type' => $invoice->type,
              ];
              $mail = Mail::send('users.mails.send_mail_renew_domain', $data, function($message){
                  $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                  $message->to('tuansinhtk@gmail.com')->subject('Khách hàng yêu cầu thanh toán');
              });
            } catch (\Exception $e) {
                return $history_pay->id;
            }

            return $history_pay->id;
        } catch (\Exception $e) {
            return false;
        }

    }

    public function order_domain_expired($data)
    {
        $user = $this->user->find(Auth::user()->id);
        $billing = config('billing_domain_exp');
        $domain = $this->domain->where('domain', $data['domain'])->first();
        $product = $this->domain_product->where('type', $domain->domain_ext)->first();
        $total =  $product[$data['billing']];
        $data_order = [
            'user_id' => $user->id,
            'total' => $total,
            'status' => 'Pending',
            'description' => 'Gia hạn domain',
            'type' => 'expired',
        ];
        $order = $this->order->create($data_order);
        //create order detail
        $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime( $date . '+3 Day' ));
        $data_order_detail = [
            'order_id' => $order->id,
            'type' => 'Domain_exp',
            'due_date' => $date,
            'description' => 'Gia hạn domain',
            'status' => 'unpaid',
            'sub_total' => $total,
            'quantity' => '1',
            'user_id' => $user->id,
        ];
        $detail_order = $this->detail_order->create($data_order_detail);
        //create order history
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => 'GDEXD' . strtoupper(substr(sha1(time()), 34, 39)),
            'discription' => 'Hóa đơn số ' . $detail_order->id,
            'type_gd' => '7',
            'method_gd' => 'domain_exp',
            'date_gd' => date('Y-m-d'),
            'money'=> $total,
            'status' => 'Pending',
            'detail_order_id' => $detail_order->id,
        ];
        $this->history_pay->create($data_history);
        // vetify order
        $data_vetify_order = [
            'order_id' => $order->id,
            'token' => sha1(time()),
        ];
        $vetify = $this->order_vetify->create($data_vetify_order);
        if($detail_order && $data_order) {
            $data_domain_exp = [
                'billing_cycle' => $data['billing'],
                'detail_order_id' => $detail_order->id,
                'domain_id' => $domain->id,
            ];
            $create_domain_exp = $this->domain_exp->create($data_domain_exp);
            if (!$create_domain_exp) {
                return false;
            }
            $data_mail = [
                'user_name' => $user->name,
                'user_id' => $user->id,
                'user_email' => $user->email,
                'order_id' => $order->id,
                'order_created_at' => $order->created_at,
                'domain' => $domain->domain,
                'domain_year' => $billing[$data['billing']].' Năm',
                'total' => $total,
                'token' => $data_vetify_order['token'],
            ];
            $this->user_email = $user->email;
            $this->domain = $data_mail['domain'];

            // dd($this->user_email);
            try {
              // tạo log
              $data_log = [
                'user_id' => Auth::user()->id,
                'action' =>  'đặt hàng',
                'model' => 'User/Order',
                'description' => 'gia hạn Domain <a target="_blank" href="/admin/domain/detail/' . $domain->id . '">#' . $domain->domain . '</a>',
                'description_user' => 'gia hạn Domain <a target="_blank" href="/order/check-invoices/' . $detail_order->id . '">#' . $detail_order->id . '</a>',
              ];
              $this->log_activity->create($data_log);

              $send_mail =  Mail::send('users.mails.order-domain-expired-confirm', $data_mail, function($message){
                  $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                  $message->to($this->user_email)->subject('Xác thực gia hạn tên miền - Domain '.$this->domain);
              });
            } catch (\Exception $e) {
              return $detail_order;
            }


        } else {
            return false;
        }
        return $detail_order;
    }

    public function payment_domain_expired($invoice_id) {
        // return true;
        $invoice = $this->detail_order->find($invoice_id);
        $domain_exp = $this->domain_exp->where('detail_order_id', $invoice_id)->first();
        $domain = $this->domain->where('id', $domain_exp->domain_id)->first();
        $next_due_date = $domain->next_due_date;
        if(!$next_due_date) {
            return false;
        }
        // HistoryPay
        $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
        if ( $history_pay->status == 'Active' ) {
          return true;
        }
        $total = $invoice->sub_total * $invoice->quantity;
        $order = $this->order->find($invoice->order->id);
        $order->status = 'Finish';
        $order->save();
        $credit = $this->credit->where('user_id', Auth::user()->id)->first();
        if (!isset($credit)) {
            return false;
        }
        // invoice
        $billing = config('billing_domain_exp');
        $domain_expireds = $invoice->domain_expireds;
        if ($invoice->type == 'Domain_exp') {
            // HistoryPay
            $history_pay = $this->history_pay->where('detail_order_id', $invoice_id)->first();
            $history_pay->status = 'Active';
            $history_pay->method_gd_invoice = 'credit';
            $history_pay->save();
            // ghi log giao dịch
            $data_log_payment = [
              'history_pay_id' => $history_pay->id,
              'before' => $credit->value,
              'after' => $credit->value - $total,
            ];
            $this->log_payment->create($data_log_payment);
            // credit
            $credit = $this->credit->where('user_id', Auth::user()->id)->first();
            $credit->value = $credit->value - $total;
            $credit->save();
            // domain
            $billing_cycle = $domain_expireds[0]->billing_cycle;
            // invoice
            $date_star = $invoice->created_at;
            $date_paid = date('Y-m-d');
            $due_date = date('Y-m-d', strtotime( $next_due_date . $billing[$domain_expireds[0]->billing_cycle] . ' Year' ));
            $invoice->due_date = $due_date;
            $invoice->paid_date = $date_paid;
            $invoice->status = 'paid';
            $invoice->payment_method = 1;
            $paid = $invoice->save();

            foreach ($domain_expireds as $key => $domain_expired) {
                $domain = $domain_expired->domain;
                // $date_star = $domain->next_due_date;
                $date_star = date('Y-m-d');
                $due_date = date('Y-m-d', strtotime( $next_due_date . $billing[$domain_expired->billing_cycle] . ' Year'));
                $domain->next_due_date = $due_date;
                $domain->save();
                $year = $billing[$domain_expired->billing_cycle];
                $request_panhanhoa = $this->domain_pa->renewDomain($domain, $year);
                if($request_panhanhoa->{'ReturnCode'} == 200) {
                    // $order->status = 'Finish';
                    // $order->save();
                    // $this->send_mail_finish('hosting',$order, $invoice ,$hostings);
                    // $this->send_mail_finish('domain',$order, $invoice ,$domain);
                    $data_reg = [
                        'user_id' => Auth::user()->id,
                        'domain' => $domain['domain'],
                        'due_date' => date('d/m/Y', strtotime( $due_date )),
                        'total' => $invoice->sub_total
                    ];
                    try {
                      // tạo log
                      $data_log = [
                        'user_id' => Auth::user()->id,
                        'action' =>  'thanh toán',
                        'model' => 'User/HistoryPay',
                        'description' => 'gia hạn Domain <a target="_blank" href="/admin/domain/detail/' . $domain->id . '">#' . $domain->domain . '</a>',
                        'description_user' => 'gia hạn Domain <a target="_blank" href="/order/check-invoices/' . $invoice->id . '">#' . $invoice->id . '</a>',
                        'service' => $domain->domain,
                      ];
                      $this->log_activity->create($data_log);

                      $mail = Mail::send('users.mails.finish_renew_domain', $data_reg, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to(Auth::user()->email)->subject('Hoàn thành gia hạn domain');
                      });
                      $mail_to_admin = Mail::send('users.mails.finish_renew_domain_to_admin', $data_reg, function($message){
                          $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                          $message->to('tuansinhtk@gmail.com')->subject('Hoàn thành gia hạn domain');
                      });
                    } catch (\Exception $e) {
                      continue;
                    }

                    return true;
                } else {
                    $data_error = [
                        'user_id' => Auth::user()->id,
                        'domain' => $domain['domain'],
                        'error_code' => $request_panhanhoa->{'ReturnCode'},
                        'error' => $request_panhanhoa->{'ReturnText'},
                    ];
                    // dd($data_error);
                    $mail = Mail::send('users.mails.error_renew_domain', $data_error, function($message){
                        $message->from('support@cloudzone.vn', 'Cloudzone Portal');
                        $message->to('tuansinhtk@gmail.com')->subject('Lỗi khi gia hạn domain');
                    });
                    return true;
                }
            }
            $order->status = 'Finish';
            $order->save();
        }
        return $paid;
    }


    public function list_invoices_with_sl($sl)
    {
        $invoices = $this->detail_order->where('user_id', Auth::user()->id)->orderBy('id', 'desc')->with('order')->paginate($sl);
        $data = [];
        $data['data'] = [];
        foreach ($invoices as $key => $invoice) {
            $type = '';
            if(!empty( $invoice->order->type == 'expired')) {
                if ($invoice->type == 'Domain_exp') {
                    $type = 'Gia hạn Domain';
                    $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                    $type .=  '<i class="fas fa-plus"></i></button>';
                    $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                    foreach ($invoice->order_expireds as $key => $order_expired) {
                      if(!empty($order_expired->domain->domain)) {
                        $type .= $order_expired->domain->domain;
                      }
                      $type .= '<br>';
                    }
                    $type .= '</div>';
                }
                else {
                  if($invoice->type == 'VPS') {
                      $type = 'Gia hạn VPS';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->vps->ip)) {
                          $type .= $order_expired->vps->ip;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  elseif($invoice->type == 'NAT-VPS') {
                      $type = 'Gia hạn NAT VPS';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->vps->ip)) {
                          $type .= $order_expired->vps->ip;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  elseif($invoice->type == 'VPS US' || $invoice->type == ' US') {
                      $type = 'Gia hạn VPS US';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->vps->ip)) {
                          $type .= $order_expired->vps->ip;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  elseif($invoice->type == 'Server') {
                      $type = 'Gia hạn Server';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->server->ip)) {
                          $type .= $order_expired->server->ip;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  elseif($invoice->type == 'Colocation') {
                      $type = 'Gia hạn Colocation';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->colocation->ip)) {
                          $type .= $order_expired->colocation->ip;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  elseif($invoice->type == 'Email Hosting') {
                      $type = 'Gia hạn Email Hosting';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->email_hosting->domain)) {
                          $type .= $order_expired->email_hosting->domain;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  else {
                      $type = 'Gia hạn Hosting';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->hosting->domain)) {
                          $type .= $order_expired->hosting->domain;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                }
            }
            elseif($invoice->type == 'change_ip') {
               $type = 'Đổi IP';
               $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
               $type .=  '<i class="fas fa-plus"></i></button>';
               $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
               foreach ($invoice->order_change_vps as $key => $order_change_vps) {
                 if(!empty($order_change_vps->vps->ip)) {
                   $type .= $order_change_vps->vps->ip;
                 }
                 else {
                   $type .= '<span class="text-danger">Chưa tạo</span>';
                 }
                 $type .= '<br>';
               }
               $type .= '</div>';
            }
            elseif($invoice->type == 'addon_vps') {
               $type = 'Cấu hình thêm VPS';
               $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
               $type .=  '<i class="fas fa-plus"></i></button>';
               $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
               foreach ($invoice->order_addon_vps as $key => $order_addon_vps) {
                 if(!empty($order_addon_vps->vps->ip)) {
                   $type .= $order_addon_vps->vps->ip;
                 }
                 else {
                   $type .= '<span class="text-danger">Chưa tạo</span>';
                 }
                 $type .= '<br>';
               }
               $type .= '</div>';
            }
            elseif($invoice->type == 'VPS') {
                $type = 'Đặt hàng VPS';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->vps as $key => $vps) {
                  if(!empty($vps->ip)) {
                    $type .= $vps->ip;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'NAT-VPS') {
                $type = 'Đặt hàng NAT VPS';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->vps as $key => $vps) {
                  if(!empty($vps->ip)) {
                    $type .= $vps->ip;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'VPS-US') {
                $type = 'Đặt hàng VPS US';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->vps as $key => $vps) {
                  if(!empty($vps->ip)) {
                    $type .= $vps->ip;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'Hosting') {
                $type = 'Đặt hàng Hosting';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->hostings as $key => $hosting) {
                  if(!empty($hosting->domain)) {
                    $type .= $hosting->domain;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'Email Hosting') {
                $type = 'Đặt hàng Email Hosting';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->email_hostings as $key => $hosting) {
                  if(!empty($hosting->domain)) {
                    $type .= $hosting->domain;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'Server')  {
                $type = 'Đặt hàng Server';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->servers as $key => $vps) {
                  if(!empty($vps->ip)) {
                    $type .= $vps->ip;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'Colocation')  {
                $type = 'Đặt hàng Colocation';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->colocations as $key => $vps) {
                  if(!empty($vps->ip)) {
                    $type .= $vps->ip;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'Domain')  {
                $type = 'Đặt hàng Domain';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->domains as $key => $hosting) {
                  if(!empty($hosting->domain)) {
                    $type .= $hosting->domain;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'upgrade_hosting') {
                $type = 'Nâng cấp Hosting';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                if( !empty($invoice->order_upgrade_hosting->hosting->domain) ) {
                    $type .= $invoice->order_upgrade_hosting->hosting->domain;
                }
                else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                }
                $type .= '<br>';
                $type .= '</div>';
            }
            else {
                $type = 'Đặt hàng';
            }
            $invoice->text_type = $type;
            $invoice->date_create = date('d-m-Y', strtotime($invoice->created_at));
            $invoice->next_due_date = date('d-m-Y', strtotime($invoice->due_date));
            $invoice->total = $invoice->sub_total * $invoice->quantity;
            $data['data'][] = $invoice;
        }
        $data['total'] = $invoices->total();
        $data['perPage'] = $invoices->perPage();
        $data['current_page'] = $invoices->currentPage();
        return $data;
    }

    public function list_invoices_paid_with_sl($sl)
    {
        $invoices = $this->detail_order->where('status', 'paid')->where('user_id', Auth::user()->id)->with('order')->orderBy('id', 'desc')->paginate($sl);
        $data = [];
        $data['data'] = [];
        foreach ($invoices as $key => $invoice) {
            $type = '';
            if(!empty( $invoice->order->type == 'expired')) {
                if ($invoice->type == 'Domain_exp') {
                    $type = 'Gia hạn Domain';
                    $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                    $type .=  '<i class="fas fa-plus"></i></button>';
                    $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                    foreach ($invoice->order_expireds as $key => $order_expired) {
                      if(!empty($order_expired->domain->domain)) {
                        $type .= $order_expired->domain->domain;
                      }
                      $type .= '<br>';
                    }
                    $type .= '</div>';
                }
                else {
                  if($invoice->type == 'VPS') {
                      $type = 'Gia hạn VPS';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->vps->ip)) {
                          $type .= $order_expired->vps->ip;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  elseif($invoice->type == 'NAT-VPS') {
                      $type = 'Gia hạn NAT VPS';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->vps->ip)) {
                          $type .= $order_expired->vps->ip;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  elseif($invoice->type == 'VPS US' || $invoice->type == ' US') {
                      $type = 'Gia hạn VPS US';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->vps->ip)) {
                          $type .= $order_expired->vps->ip;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  elseif($invoice->type == 'Server') {
                      $type = 'Gia hạn Server';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->server->ip)) {
                          $type .= $order_expired->server->ip;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  elseif($invoice->type == 'Colocation') {
                      $type = 'Gia hạn Colocation';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->colocation->ip)) {
                          $type .= $order_expired->colocation->ip;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  elseif($invoice->type == 'Email Hosting') {
                      $type = 'Gia hạn Email Hosting';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->email_hosting->domain)) {
                          $type .= $order_expired->email_hosting->domain;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  else {
                      $type = 'Gia hạn Hosting';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->hosting->domain)) {
                          $type .= $order_expired->hosting->domain;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                }
            }
            elseif($invoice->type == 'change_ip') {
               $type = 'Đổi IP';
               $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
               $type .=  '<i class="fas fa-plus"></i></button>';
               $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
               foreach ($invoice->order_change_vps as $key => $order_change_vps) {
                 if(!empty($order_change_vps->vps->ip)) {
                   $type .= $order_change_vps->vps->ip;
                 }
                 else {
                   $type .= '<span class="text-danger">Chưa tạo</span>';
                 }
                 $type .= '<br>';
               }
               $type .= '</div>';
            }
            elseif($invoice->type == 'addon_vps') {
               $type = 'Cấu hình thêm VPS';
               $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
               $type .=  '<i class="fas fa-plus"></i></button>';
               $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
               foreach ($invoice->order_addon_vps as $key => $order_addon_vps) {
                 if(!empty($order_addon_vps->vps->ip)) {
                   $type .= $order_addon_vps->vps->ip;
                 }
                 else {
                   $type .= '<span class="text-danger">Chưa tạo</span>';
                 }
                 $type .= '<br>';
               }
               $type .= '</div>';
            }
            elseif($invoice->type == 'VPS') {
                $type = 'Đặt hàng VPS';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->vps as $key => $vps) {
                  if(!empty($vps->ip)) {
                    $type .= $vps->ip;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'NAT-VPS') {
                $type = 'Đặt hàng NAT VPS';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->vps as $key => $vps) {
                  if(!empty($vps->ip)) {
                    $type .= $vps->ip;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'VPS-US') {
                $type = 'Đặt hàng VPS US';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->vps as $key => $vps) {
                  if(!empty($vps->ip)) {
                    $type .= $vps->ip;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'Hosting') {
                $type = 'Đặt hàng Hosting';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->hostings as $key => $hosting) {
                  if(!empty($hosting->domain)) {
                    $type .= $hosting->domain;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'Email Hosting') {
                $type = 'Đặt hàng Email Hosting';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->email_hostings as $key => $hosting) {
                  if(!empty($hosting->domain)) {
                    $type .= $hosting->domain;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'Server')  {
                $type = 'Đặt hàng Server';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->servers as $key => $vps) {
                  if(!empty($vps->ip)) {
                    $type .= $vps->ip;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'Colocation')  {
                $type = 'Đặt hàng Colocation';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->colocations as $key => $vps) {
                  if(!empty($vps->ip)) {
                    $type .= $vps->ip;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'Domain')  {
                $type = 'Đặt hàng Domain';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->domains as $key => $hosting) {
                  if(!empty($hosting->domain)) {
                    $type .= $hosting->domain;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'upgrade_hosting') {
                $type = 'Nâng cấp Hosting';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                if( !empty($invoice->order_upgrade_hosting->hosting->domain) ) {
                    $type .= $invoice->order_upgrade_hosting->hosting->domain;
                }
                else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                }
                $type .= '<br>';
                $type .= '</div>';
            }
            else {
                $type = 'Đặt hàng';
            }
            $invoice->text_type = $type;
            $invoice->date_create = date('d-m-Y', strtotime($invoice->created_at));
            $invoice->next_due_date = date('d-m-Y', strtotime($invoice->due_date));
            $invoice->total = $invoice->sub_total * $invoice->quantity;
            $data['data'][] = $invoice;
        }
        $data['total'] = $invoices->total();
        $data['perPage'] = $invoices->perPage();
        $data['current_page'] = $invoices->currentPage();
        return $data;
    }

    public function list_invoices_unpaid_with_sl($sl)
    {
        $invoices = $this->detail_order->where('status', 'unpaid')->where('user_id', Auth::user()->id)->with('order')->orderBy('id', 'desc')->paginate($sl);
        $data = [];
        $data['data'] = [];
        foreach ($invoices as $key => $invoice) {
            $type = '';
            if(!empty( $invoice->order->type == 'expired')) {
                if ($invoice->type == 'Domain_exp') {
                    $type = 'Gia hạn Domain';
                    $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                    $type .=  '<i class="fas fa-plus"></i></button>';
                    $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                    foreach ($invoice->order_expireds as $key => $order_expired) {
                      if(!empty($order_expired->domain->domain)) {
                        $type .= $order_expired->domain->domain;
                      }
                      $type .= '<br>';
                    }
                    $type .= '</div>';
                }
                else {
                  if($invoice->type == 'VPS') {
                      $type = 'Gia hạn VPS';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->vps->ip)) {
                          $type .= $order_expired->vps->ip;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  elseif($invoice->type == 'NAT-VPS') {
                      $type = 'Gia hạn NAT VPS';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->vps->ip)) {
                          $type .= $order_expired->vps->ip;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  elseif($invoice->type == 'VPS US' || $invoice->type == ' US') {
                      $type = 'Gia hạn VPS US';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->vps->ip)) {
                          $type .= $order_expired->vps->ip;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  elseif($invoice->type == 'Server') {
                      $type = 'Gia hạn Server';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->server->ip)) {
                          $type .= $order_expired->server->ip;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  elseif($invoice->type == 'Colocation') {
                      $type = 'Gia hạn Colocation';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->colocation->ip)) {
                          $type .= $order_expired->colocation->ip;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  elseif($invoice->type == 'Email Hosting') {
                      $type = 'Gia hạn Email Hosting';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->email_hosting->domain)) {
                          $type .= $order_expired->email_hosting->domain;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                  else {
                      $type = 'Gia hạn Hosting';
                      $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                      $type .=  '<i class="fas fa-plus"></i></button>';
                      $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                      foreach ($invoice->order_expireds as $key => $order_expired) {
                        if(!empty($order_expired->hosting->domain)) {
                          $type .= $order_expired->hosting->domain;
                        }
                        else {
                          $type .= '<span class="text-danger">Chưa tạo</span>';
                        }
                        $type .= '<br>';
                      }
                      $type .= '</div>';
                  }
                }
            }
            elseif($invoice->type == 'change_ip') {
               $type = 'Đổi IP';
               $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
               $type .=  '<i class="fas fa-plus"></i></button>';
               $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
               foreach ($invoice->order_change_vps as $key => $order_change_vps) {
                 if(!empty($order_change_vps->vps->ip)) {
                   $type .= $order_change_vps->vps->ip;
                 }
                 else {
                   $type .= '<span class="text-danger">Chưa tạo</span>';
                 }
                 $type .= '<br>';
               }
               $type .= '</div>';
            }
            elseif($invoice->type == 'addon_vps') {
               $type = 'Cấu hình thêm VPS';
               $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
               $type .=  '<i class="fas fa-plus"></i></button>';
               $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
               foreach ($invoice->order_addon_vps as $key => $order_addon_vps) {
                 if(!empty($order_addon_vps->vps->ip)) {
                   $type .= $order_addon_vps->vps->ip;
                 }
                 else {
                   $type .= '<span class="text-danger">Chưa tạo</span>';
                 }
                 $type .= '<br>';
               }
               $type .= '</div>';
            }
            elseif($invoice->type == 'VPS') {
                $type = 'Đặt hàng VPS';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->vps as $key => $vps) {
                  if(!empty($vps->ip)) {
                    $type .= $vps->ip;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'NAT-VPS') {
                $type = 'Đặt hàng NAT VPS';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->vps as $key => $vps) {
                  if(!empty($vps->ip)) {
                    $type .= $vps->ip;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'VPS-US') {
                $type = 'Đặt hàng VPS US';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->vps as $key => $vps) {
                  if(!empty($vps->ip)) {
                    $type .= $vps->ip;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'Hosting') {
                $type = 'Đặt hàng Hosting';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->hostings as $key => $hosting) {
                  if(!empty($hosting->domain)) {
                    $type .= $hosting->domain;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'Email Hosting') {
                $type = 'Đặt hàng Email Hosting';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->email_hostings as $key => $hosting) {
                  if(!empty($hosting->domain)) {
                    $type .= $hosting->domain;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'Server')  {
                $type = 'Đặt hàng Server';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->servers as $key => $vps) {
                  if(!empty($vps->ip)) {
                    $type .= $vps->ip;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'Colocation')  {
                $type = 'Đặt hàng Colocation';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->colocations as $key => $vps) {
                  if(!empty($vps->ip)) {
                    $type .= $vps->ip;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'Domain')  {
                $type = 'Đặt hàng Domain';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                foreach ($invoice->domains as $key => $hosting) {
                  if(!empty($hosting->domain)) {
                    $type .= $hosting->domain;
                  }
                  else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                  }
                  $type .= '<br>';
                }
                $type .= '</div>';
            }
            elseif($invoice->type == 'upgrade_hosting') {
                $type = 'Nâng cấp Hosting';
                $type .= '<button type="button" class="btn btn-outline-success float-right tooggle-plus" data-toggle="collapse"   data-target="#service'. $invoice->id .'"   data-placement="top" title="Chi tiết">';
                $type .=  '<i class="fas fa-plus"></i></button>';
                $type .= '<div id="service'. $invoice->id .'" class="collapse mt-4">';
                if( !empty($invoice->order_upgrade_hosting->hosting->domain) ) {
                    $type .= $invoice->order_upgrade_hosting->hosting->domain;
                }
                else {
                    $type .= '<span class="text-danger">Chưa tạo</span>';
                }
                $type .= '<br>';
                $type .= '</div>';
            }
            else {
                $type = 'Đặt hàng';
            }
            $invoice->text_type = $type;
            $invoice->date_create = date('d-m-Y', strtotime($invoice->created_at));
            $invoice->next_due_date = date('d-m-Y', strtotime($invoice->due_date));
            $invoice->total = $invoice->sub_total * $invoice->quantity;
            $data['data'][] = $invoice;
        }
        $data['total'] = $invoices->total();
        $data['perPage'] = $invoices->perPage();
        $data['current_page'] = $invoices->currentPage();
        return $data;
    }

}
