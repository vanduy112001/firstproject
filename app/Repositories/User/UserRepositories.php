<?php

namespace App\Repositories\User;

use App\Model\User;
use App\Model\UserMeta;
use App\Model\Verify;
use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\Email;
use Illuminate\Support\Facades\Auth;
use App\Model\Order;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\VpsConfig;
use App\Model\Server;
use App\Model\Hosting;
use App\Model\OrderVetify;
use App\Model\Credit;
use App\Model\Customer;
use App\Model\GroupUser;
use App\Model\Notification;
use App\Model\Tutorial;
use App\Model\Colocation;
use App\Model\Domain;
use App\Model\HistoryPay;
use App\Model\EmailHosting;
use App\Model\Ticket;
use App\Model\ReadNotification;
use App\Model\Proxy;

use Carbon\Carbon;

class UserRepositories {

    protected $user;
    protected $user_meta;
    protected $verify;
    protected $email;
    protected $group_user;
    protected $group_product;
    protected $product;
    protected $order;
    protected $detail_order;
    protected $vps;
    protected $vps_config;
    protected $server;
    protected $hosting;
    protected $order_vetify;
    protected $credit;
    protected $history_pay;
    protected $customer;
    protected $domain;
    protected $notification;
    protected $tutorial;
    protected $colocation;
    protected $email_hosting;
    protected $ticket;
    protected $readnotification;
    protected $proxy;

    public function __construct()
    {
        $this->user = new User;
        $this->user_meta = new UserMeta;
        $this->verify = new Verify;
        $this->group_user = new GroupUser;
        $this->product = new Product;
        $this->order = new Order;
        $this->detail_order = new DetailOrder;
        $this->vps = new Vps;
        $this->vps_config = new VpsConfig;
        $this->server = new Server;
        $this->hosting = new Hosting;
        $this->credit = new Credit;
        $this->history_pay = new HistoryPay;
        $this->customer = new Customer;
        $this->domain = new Domain;
        $this->notification = new Notification();
        $this->tutorial = new Tutorial;
        $this->colocation = new Colocation;
        $this->email_hosting = new EmailHosting;
        $this->ticket = new Ticket;
        $this->readnotification = new ReadNotification;
        $this->proxy = new Proxy;
    }

    public function detail($id)
    {
        $user = $this->user->find($id);
        return $user;
    }

    // load khu vực siderbar_top
    public function load_siderbar_top() {
        $data = [
          'total_service' => 0,
        ];
        // kiểm tra số lượng dịch vụ của khách hàng
        $total_vps = $this->vps->where('user_id', Auth::user()->id)
              ->where('status',  'Active')
              ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])
              ->count();
        $total_server = $this->server->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->whereIn('status_server', [ 'on', 'off', 'progressing', 'rebuild', 'expire' ])
                ->count();
        $total_colocation = $this->colocation->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->whereIn('status_colo', [ 'on', 'off', 'progressing', 'expire' ])
                ->count();
        $total_hosting = $this->hosting->where('user_id', Auth::user()->id)
                         ->where('status',  'Active')
                         ->whereIn('status_hosting', [ 'on', 'off', 'expire' ])
                         ->count();
        $total_email_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
                         ->where('status',  'Active')
                         ->whereIn('status_hosting', [ 'on', 'off', 'expire' ])
                         ->count();
        $total_domain = $this->domain->where('user_id', Auth::user()->id)
                         ->where('status',  'Active')->count();
        $data['total_service'] = $total_vps + $total_hosting + $total_server + $total_email_hosting + $total_colocation + $total_domain;
        return $data;
    }
    // load khu vực load_siderbar_right
    public function load_siderbar_right() {
        $data = [];
        // kiểm tra số lượng dịch vụ của khách hàng
        // VPS VN
        $total_vps = $this->vps->where('user_id', Auth::user()->id)
              ->where('location', 'cloudzone')
              ->where('status',  'Active')
              ->where(function($query)
              {
                  return $query
                  ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire' ]);
              })->count();
        $total_vps_use = $this->vps->where('user_id', Auth::user()->id)
              ->where('location', 'cloudzone')
              ->where('status',  'Active')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild' ]);
              })->count();
        $total_vps_expire = $this->vps->where('user_id', Auth::user()->id)
              ->where('location', 'cloudzone')
              ->where('status',  'Active')
              ->where('status_vps', 'expire')->count();
        // VPS US
        $total_vps_us = $this->vps->where('user_id', Auth::user()->id)
              ->where('location', 'us')
              ->where('status',  'Active')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire' ]);
              })->count();
        $total_vps_us_use = $this->vps->where('user_id', Auth::user()->id)
              ->where('location', 'us')
              ->where('status',  'Active')
              ->where(function($query)
              {
                  return $query
                    ->whereIn('status_vps', [ 'on', 'off', 'admin_off', 'progressing' ]);
              })->count();
        $total_vps_us_expire = $this->vps->where('user_id', Auth::user()->id)
              ->where('location', 'us')
              ->where('status',  'Active')
              ->where('status_vps', 'expire')->count();
        // Proxy
        $total_proxy = $this->proxy->where('user_id', Auth::user()->id)
          ->whereIn('status', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'expire', 'reset_password' ])->count();
        $total_proxy_use = $this->proxy->where('user_id', Auth::user()->id)
          ->whereIn('status', [ 'on', 'off', 'admin_off', 'progressing', 'change_ip', 'rebuild', 'reset_password' ])->count();
        $total_proxy_expire = $this->proxy->where('user_id', Auth::user()->id)
          ->where('status', 'expire')->count();
        // server
        $total_server = $this->server->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where(function($query)
                {
                    return $query
                      ->whereIn('status_server', [ 'on', 'off', 'admin_off', 'progressing' ]);
                })->count();
        $total_colocation = $this->colocation->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where(function($query)
                {
                    return $query
                      ->whereIn('status_colo', [ 'on', 'off', 'admin_off', 'progressing' ]);
                })->count();
        $total_hosting = $this->hosting->where('user_id', Auth::user()->id)
                         ->where('status',  'Active')
                         ->where(function($query)
                             {
                                 return $query
                                   ->whereIn('status_hosting', [ 'on', 'off' ]);
                             })->count();
        $total_email_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
                         ->where('status',  'Active')
                         ->where(function($query)
                             {
                                 return $query
                                   ->whereIn('status_hosting', [ 'on', 'off' ]);
                             })->count();
        $total_domain = $this->domain->where('user_id', Auth::user()->id)
                         ->where('status',  'Active')->count();
        $data = [
          'total_vps' => $total_vps,
          'total_vps_use' => $total_vps_use,
          'total_vps_expire' => $total_vps_expire,
          'total_vps_us' => $total_vps_us,
          'total_vps_us_use' => $total_vps_us_use,
          'total_vps_us_expire' => $total_vps_us_expire,
          'total_server' => $total_server,
          'total_colocation' => $total_colocation,
          'total_hosting' => $total_hosting,
          'total_email_hosting' => $total_email_hosting,
          'total_domain' => $total_domain,
          'total_proxy' => $total_proxy,
          'total_proxy_use' => $total_proxy_use,
          'total_proxy_expire' => $total_proxy_expire,
        ];
        return $data;
    }

    public function total_ticket($user_id)
    {
        $total_ticket = $this->ticket->where('user_id', $user_id)->where('status', 'pending')->count();
        return $total_ticket;
    }
    //    Tổng số vps hết hạn
    public function total_vps_nearly() {
        $list_vps = [];
        for ($i=3; $i >= 0 ; $i--) {
          $now = Carbon::now();
          $list_vps['sub' . $i] = $this->vps->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where('location', 'cloudzone')
                ->whereIn('status_vps', [ 'on', 'off', 'rebuild', 'reset_password', 'expire' ])
                ->whereDate('next_due_date', date('Y-m-d', strtotime($now->subDays($i)) ))
                ->with('product')
                ->orderBy('id', 'desc')->get();
        }
        for ($i=1; $i <= 5 ; $i++) {
          $now = Carbon::now();
          $list_vps[$i] = $this->vps->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where('location', 'cloudzone')
                ->whereIn('status_vps', [ 'on', 'off', 'rebuild', 'reset_password', 'expire' ])
                ->whereDate('next_due_date', date('Y-m-d', strtotime($now->addDays($i)) ))
                ->with('product')
                ->orderBy('id', 'desc')->get();
        }
        $qtt = 0;
        $text = '';
        if ( count($list_vps) > 0 ) {
          foreach ($list_vps as $key_list => $data_vps) {
            foreach ($data_vps as $key => $vps) {
              $qtt++;
              $text .= $vps->ip;
              $text .= ' ('. date( 'd-m-Y', strtotime($vps->next_due_date) ) .')';
              if ( $key_list == 'sub3' ) {
                $text .=  ' - hết hạn 3 ngày';
              }
              elseif ( $key_list == 'sub2' ) {
                $text .=  ' - Hết hạn 2 ngày';
              }
              elseif ( $key_list == 'sub1' ) {
                $text .=  ' - hết hạn 1 ngày';
              }
              elseif ( $key_list == 'sub0' ) {
                $text .=  ' - hết hạn';
              }
              else {
                $text .= ' - còn hạn ' . $key_list . ' ngày';
              }
              $text .= '<br>';
            }
          }
        }
        return [
          'qtt' => $qtt,
          'text' => $text
        ];
    }
    //    Tổng số vps hết hạn
    public function total_vps_us_nearly() {
        $list_vps = [];
        for ($i=3; $i >= 0 ; $i--) {
          $now = Carbon::now();
          $list_vps['sub' . $i] = $this->vps->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where('location', 'us')
                ->whereIn('status_vps', [ 'on', 'off', 'rebuild', 'reset_password', 'expire' ])
                ->whereDate('next_due_date', date('Y-m-d', strtotime($now->subDays($i)) ))
                ->with('product')
                ->orderBy('id', 'desc')->get();
        }
        for ($i=1; $i <= 5 ; $i++) {
          $now = Carbon::now();
          $list_vps[$i] = $this->vps->where('user_id', Auth::user()->id)
                ->where('status',  'Active')
                ->where('location', 'us')
                ->whereIn('status_vps', [ 'on', 'off', 'rebuild', 'reset_password', 'expire' ])
                ->whereDate('next_due_date', date('Y-m-d', strtotime($now->addDays($i)) ))
                ->with('product')
                ->orderBy('id', 'desc')->get();
        }
        $qtt = 0;
        $text = '';
        if ( count($list_vps) > 0 ) {
          foreach ($list_vps as $key_list => $data_vps) {
            foreach ($data_vps as $key => $vps) {
              $qtt++;
              $text .= $vps->ip;
              $text .= ' ('. date( 'd-m-Y', strtotime($vps->next_due_date) ) .')';
              if ( $key_list == 'sub3' ) {
                $text .=  ' - hết hạn 3 ngày';
              }
              elseif ( $key_list == 'sub2' ) {
                $text .=  ' - Hết hạn 2 ngày';
              }
              elseif ( $key_list == 'sub1' ) {
                $text .=  ' - hết hạn 1 ngày';
              }
              elseif ( $key_list == 'sub0' ) {
                $text .=  ' - hết hạn 0 ngày';
              }
              else {
                $text .=  ' - còn hạn ' . $key_list . ' ngày';
              }
              $text .= '<br>';
            }
          }
        }
        return [
          'qtt' => $qtt,
          'text' => $text
        ];
    }
    //    Tổng số server hết hạn
    public function total_server_nearly() {
      $list_server = $this->server->where('user_id', Auth::user()->id)
              ->where('status',  'Active')
              ->where(function($query)
              {
                  return $query
                    ->orwhere('status_server', 'on')
                    ->orWhere('status_server', 'off')
                    ->orWhere('status_server', 'admin_off');
              })->get();
        $qtt = 0;
        if (!empty($list_server)) {
          foreach ($list_server as $key => $server) {
            if (!empty($server->next_due_date)) {
              $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
              $next_due_date = new Carbon($server->next_due_date);
              if ( $next_due_date->diffInDays($now) <= 7 ) {
                $qtt++;
              }
            }
          }
        }
        return $qtt;
    }
    //    Tổng số colocation hết hạn
    public function total_colocation_nearly() {
      $list_colocation = $this->colocation->where('user_id', Auth::user()->id)
              ->where('status',  'Active')
              ->where(function($query)
              {
                  return $query
                    ->orwhere('status_colo', 'on')
                    ->orWhere('status_colo', 'off')
                    ->orWhere('status_colo', 'admin_off');
              })->get();
        $qtt = 0;
        if (!empty($list_colocation)) {
          foreach ($list_colocation as $key => $colocation) {
            if (!empty($colocation->next_due_date)) {
              $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
              $next_due_date = new Carbon($colocation->next_due_date);
              if ( $next_due_date->diffInDays($now) <= 7 ) {
                $qtt++;
              }
            }
          }
        }
        return $qtt;
    }
    //    Tổng số hosting hết hạn
    public function total_hosting_nearly()
    {
      $list_hosting = $this->hosting->where('user_id', Auth::user()->id)
                       ->where('status',  'Active')
                       ->where(function($query)
                           {
                               return $query
                                 ->orwhere('status_hosting', 'on')
                                 ->orWhere('status_hosting', 'off');
                           })->get();
        $qtt = 0;
        if (!empty($list_hosting)) {
          foreach ($list_hosting as $key => $hosting) {
            if (!empty($hosting->next_due_date)) {
              $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
              $next_due_date = new Carbon($hosting->next_due_date);
              if ( $next_due_date->diffInDays($now) <= 15 ) {
                $qtt++;
              }
            }
          }
        }
        return $qtt;
    }
    //    Tổng số email_hosting hết hạn
    public function total_email_hosting_nearly()
    {
      $list_hosting = $this->email_hosting->where('user_id', Auth::user()->id)
                       ->where('status',  'Active')
                       ->where(function($query)
                           {
                               return $query
                                 ->orwhere('status_hosting', 'on')
                                 ->orWhere('status_hosting', 'off');
                           })->get();
        $qtt = 0;
        if (!empty($list_hosting)) {
          foreach ($list_hosting as $key => $hosting) {
            if (!empty($hosting->next_due_date)) {
              $now = Carbon::now()->year . '-' . Carbon::now()->month . '-' . Carbon::now()->day;
              $next_due_date = new Carbon($hosting->next_due_date);
              if ( $next_due_date->diffInDays($now) <= 15 ) {
                $qtt++;
              }
            }
          }
        }
        return $qtt;
    }

    public function total_notication()
    {
      $notifications_read = $this->readnotification->where('user_id', Auth::user()->id )->where('read_at', null )->orderBy('id', 'desc')->with('notification')->paginate(20);
      $total_notication = $this->readnotification->where('user_id', Auth::user()->id )->where('read_at', null )->orderBy('id', 'desc')->count();
      $data_read = [];
      foreach ($notifications_read as $key => $notification_read) {
        $notification_read->date_create = date('d-m-Y', strtotime($notification_read->created_at));
        $data_read[] = $notification_read;
      }
      $data = [
        'total' =>  $total_notication,
        'notication' =>  $data_read,
      ];
      return $data;
    }

    public function get_tutorials()
    {
      $tutorials =  $this->tutorial->orderBy('id', 'desc')->paginate(20);
      $data = [
        'total' => $tutorials->count(),
        'tutorials' => $tutorials,
      ];
      return $data;
    }

    public function total_detail_order_progressing()
    {
        return $this->detail_order->where('user_id', Auth::user()->id)->where('status', 'unpaid')->count();
    }

    public function detail_user_by_email($email)
    {
      $user = $this->user->where('email', $email)->first();
      if ( !isset($user) ) {
        // trường hợp không có user
        return [
          "error" => 1,
          'user' => '',
        ];
      }
      elseif ( $user->id == 1 ) {
        // trường hợp user là admin@gmail.com
        return [
          "error" => 2,
          'user' => '',
        ];
      }
      else {
        // kiểm tra là user có phải là user đang tiến hành chuyển hay không
        if ( $user->id == Auth::id() ) {
          return [
            "error" => 3,
            'user' => '',
          ];
        }
        return [
          "error" => 0,
          'user' => $user,
        ];
      }
    }

}
