<?php

namespace App\Repositories\User;

use App\Model\User;
use App\Model\Customer;
use App\Model\Order;
use Illuminate\Support\Facades\Auth;

class UserCustomerRepositories {

    protected $user;
    protected $order;
    protected $customer;

    public function __construct()
    {
        $this->user = new User;
        $this->customer = new Customer;
        $this->order = new Order;
    }

    public function listCustomer()
    {
        $list_customers = $this->customer->where( 'user_id' , Auth::user()->id )->orderBy('id', 'desc')->paginate(20);
        return $list_customers;
    }

    public function listCustomerHome()
    {
        $list_customers = $this->customer->where( 'user_id' , Auth::user()->id )->paginate(10);
        return $list_customers;
    }

    public function list_customer_with_user()
    {
      $list_customers = $this->customer->where( 'user_id' , Auth::user()->id )->get();
      return $list_customers;
    }

    public function detailCustomer($id)
    {
        $customer = $this->customer->where( 'user_id' , Auth::user()->id )->find($id);
        return $customer;
    }

    public function create_customer($data)
    {
        $data_customer = [
            'user_id' => Auth::user()->id,
            'type_customer' => $data['type_customer'],
            'ma_customer' => 'KH' . Auth::user()->id . substr(time(), 6 , 9),
            'customer_name' => !empty( $data['customer_name'] ) ? $data['customer_name'] : '',
            'customer_date' => !empty( $data['customer_date'] ) ? date('Y-m-d', strtotime($data['customer_date'])) : date('Y-m-d'),
            'customer_gender' => !empty( $data['customer_gender'] ) ? $data['customer_gender'] : '',
            'customer_phone' => !empty( $data['customer_phone'] ) ? $data['customer_phone'] : '',
            'customer_email' => !empty( $data['customer_email'] ) ? $data['customer_email'] : '',
            'customer_cmnd' => !empty( $data['customer_cmnd'] ) ? $data['customer_cmnd'] : '',
            'customer_address' => !empty( $data['customer_address'] ) ? $data['customer_address'] : '',
            'customer_city' => !empty( $data['customer_city'] ) ? $data['customer_city'] : '',
            // Hoàn thành data khách hàng cá nhân
            'customer_tc_name' => !empty( $data['customer_tc_name'] ) ? $data['customer_tc_name'] : '',
            'customer_tc_mst' => !empty( $data['customer_tc_mst'] ) ? $data['customer_tc_mst'] : '',
            'customer_tc_dctc' => !empty( $data['customer_tc_dctc'] ) ? $data['customer_tc_dctc'] : '',
            'customer_tc_sdt' => !empty( $data['customer_tc_sdt'] ) ? $data['customer_tc_sdt'] : '',
            'customer_tc_city' => !empty( $data['customer_tc_city'] ) ? $data['customer_tc_city'] : '',
            // Hoàn thành data tổ chức
            'customer_dk_name' => !empty( $data['customer_dk_name'] ) ? $data['customer_dk_name'] : '',
            'customer_dk_date' => !empty( $data['customer_dk_date'] ) ? date('Y-m-d', strtotime($data['customer_dk_date'])) : date('Y-m-d'),
            'customer_dk_gender' => !empty( $data['customer_dk_gender'] ) ? $data['customer_dk_gender'] : '',
            'customer_dk_phone' => !empty( $data['customer_dk_phone'] ) ? $data['customer_dk_phone'] : '',
            'customer_dk_cv' => !empty( $data['customer_dk_cv'] ) ? $data['customer_dk_cv'] : '',
            'customer_dk_cmnd' => !empty( $data['customer_dk_cmnd'] ) ? $data['customer_dk_cmnd'] : '',
            // Hoàn thành người dk
            'customer_dd_name' => !empty( $data['customer_dd_name'] ) ? $data['customer_dd_name'] : '',
            'customer_dd_cv' => !empty( $data['customer_dd_cv'] ) ? $data['customer_dd_cv'] : '',
            'customer_dd_gender' => !empty( $data['customer_dd_gender'] ) ? $data['customer_dd_gender'] : '',
            'customer_dd_date' => !empty( $data['customer_dd_date'] ) ? date('Y-m-d', strtotime($data['customer_dd_date'])) : date('Y-m-d'),
            'customer_dd_cmnd' => !empty( $data['customer_dd_cmnd'] ) ? $data['customer_dd_cmnd'] : '',
            'customer_dd_email' => !empty( $data['customer_dd_email'] ) ? $data['customer_dd_email'] : '',
            // Hoàn thành người đại diện
            'customer_tt_name' => !empty( $data['customer_tt_name'] ) ? $data['customer_tt_name'] : '',
            'customer_tt_gender' => !empty( $data['customer_tt_gender'] ) ? $data['customer_tt_gender'] : '',
            'customer_tt_date' => !empty( $data['customer_tt_date'] ) ? date('Y-m-d', strtotime($data['customer_tt_date'])) : date('Y-m-d'),
            'customer_tt_cmnd' => !empty( $data['customer_tt_cmnd'] ) ? $data['customer_tt_cmnd'] : '',
            'customer_tt_email' => !empty( $data['customer_tt_email'] ) ? $data['customer_tt_email'] : '',
        ];
        $create = $this->customer->create($data_customer);
        return $create;
    }

    public function card_add_customer($data)
    {
        $create = $this->create_customer($data);
        $data = [
          'error' => 1,
          'customer' => '',
        ];
        if ( $create ) {
          $data = [
            'error' => 0,
            'customer' => $this->list_customer_with_user(),
          ];
        }
        return $data;
    }

    public function detail_customer($id)
    {
        $customer = $this->customer->where('id', $id)->where('user_id', Auth::user()->id)->first();
        return $customer;
    }

    public function updateCustomer($data)
    {
        $id = $data['id'];
        $data_customer = [
            'type_customer' => $data['type_customer'],
            'customer_name' => !empty( $data['customer_name'] ) ? $data['customer_name'] : '',
            'customer_date' => !empty( $data['customer_date'] ) ? date('Y-m-d', strtotime($data['customer_date'])) : date('Y-m-d'),
            'customer_gender' => !empty( $data['customer_gender'] ) ? $data['customer_gender'] : '',
            'customer_phone' => !empty( $data['customer_phone'] ) ? $data['customer_phone'] : '',
            'customer_email' => !empty( $data['customer_email'] ) ? $data['customer_email'] : '',
            'customer_cmnd' => !empty( $data['customer_cmnd'] ) ? $data['customer_cmnd'] : '',
            'customer_address' => !empty( $data['customer_address'] ) ? $data['customer_address'] : '',
            'customer_city' => !empty( $data['customer_city'] ) ? $data['customer_city'] : '',
            // Hoàn thành data khách hàng cá nhân
            'customer_tc_name' => !empty( $data['customer_tc_name'] ) ? $data['customer_tc_name'] : '',
            'customer_tc_mst' => !empty( $data['customer_tc_mst'] ) ? $data['customer_tc_mst'] : '',
            'customer_tc_dctc' => !empty( $data['customer_tc_dctc'] ) ? $data['customer_tc_dctc'] : '',
            'customer_tc_sdt' => !empty( $data['customer_tc_sdt'] ) ? $data['customer_tc_sdt'] : '',
            'customer_tc_city' => !empty( $data['customer_tc_city'] ) ? $data['customer_tc_city'] : '',
            // Hoàn thành data tổ chức
            'customer_dk_name' => !empty( $data['customer_dk_name'] ) ? $data['customer_dk_name'] : '',
            'customer_dk_date' => !empty( $data['customer_dk_date'] ) ? date('Y-m-d', strtotime($data['customer_dk_date'])) : date('Y-m-d'),
            'customer_dk_gender' => !empty( $data['customer_dk_gender'] ) ? $data['customer_dk_gender'] : '',
            'customer_dk_phone' => !empty( $data['customer_dk_phone'] ) ? $data['customer_dk_phone'] : '',
            'customer_dk_cv' => !empty( $data['customer_dk_cv'] ) ? $data['customer_dk_cv'] : '',
            'customer_dk_cmnd' => !empty( $data['customer_dk_cmnd'] ) ? $data['customer_dk_cmnd'] : '',
            // Hoàn thành người dk
            'customer_dd_name' => !empty( $data['customer_dd_name'] ) ? $data['customer_dd_name'] : '',
            'customer_dd_cv' => !empty( $data['customer_dd_cv'] ) ? $data['customer_dd_cv'] : '',
            'customer_dd_gender' => !empty( $data['customer_dd_gender'] ) ? $data['customer_dd_gender'] : '',
            'customer_dd_date' => !empty( $data['customer_dd_date'] ) ? date('Y-m-d', strtotime($data['customer_dd_date'])) : date('Y-m-d'),
            'customer_dd_cmnd' => !empty( $data['customer_dd_cmnd'] ) ? $data['customer_dd_cmnd'] : '',
            'customer_dd_email' => !empty( $data['customer_dd_email'] ) ? $data['customer_dd_email'] : '',
            // Hoàn thành người đại diện
            'customer_tt_name' => !empty( $data['customer_tt_name'] ) ? $data['customer_tt_name'] : '',
            'customer_tt_gender' => !empty( $data['customer_tt_gender'] ) ? $data['customer_tt_gender'] : '',
            'customer_tt_date' => !empty( $data['customer_tt_date'] ) ? date('Y-m-d', strtotime($data['customer_tt_date'])) : date('Y-m-d'),
            'customer_tt_cmnd' => !empty( $data['customer_tt_cmnd'] ) ? $data['customer_tt_cmnd'] : '',
            'customer_tt_email' => !empty( $data['customer_tt_email'] ) ? $data['customer_tt_email'] : '',
        ];
        $update = $this->customer->where('id', $id)->where('user_id', Auth::user()->id)->update($data_customer);
        return $update;
    }

    public function deleteCustomer($id)
    {
        $delete = $this->customer->where('user_id', Auth::user()->id)->where('id', $id)->delete();
        return $delete;
    }

    public function list_order_with_customer($makh)
    {
        $orders = $this->order->where('user_id', Auth::user()->id)->where('makh', $makh)->with('detail_orders')->orderBy('id', 'desc')->paginate(20);
        return $orders;
    }


}
