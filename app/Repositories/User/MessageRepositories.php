<?php

namespace App\Repositories\User;

use App\Model\GroupProduct;
use App\Model\Product;
use App\Model\User;
use App\Model\UserMeta;
use App\Model\GroupUser;
use App\Model\LogActivity;
use App\Model\Message;
use App\Model\MessageMeta;
use Illuminate\Support\Facades\Auth;
use App\Events\UserChatEvent;

class MessageRepositories
{

    protected $group_product;
    protected $product;
    protected $user;
    protected $user_meta;
    protected $group_user;
    protected $domain;
    protected $domain_product;
    protected $log;
    protected $message;
    protected $message_meta;

    public function __construct()
    {
        $this->group_product = new GroupProduct;
        $this->product = new Product;
        $this->log = new LogActivity;
        $this->message = new Message;
        $this->message_meta = new MessageMeta;
    }

    public function load_chat_by_user()
    {
        $message = $this->message->where('user_id', Auth::id())->first();
        $data = [];
        if ( !isset( $message ) ) {
            $data = [
                'error' => 2,
                'content' => null,
            ];
        }
        else if ( $message->status == 'off' ) {
            $data = [
                'error' => 1,
                'content' => null,
            ];
        }
        else {
            $data['error'] = 0;
            $list_message_metas = $this->message_meta->where('message_id', $message->id)->with('user')->orderBy('id', 'desc')->paginate(20);
            foreach ($list_message_metas as $key => $message_meta) {
                $message_meta->avatar = !empty($message_meta->user->user_meta->avatar) ? $message_meta->user->user_meta->avatar : url('/images/user2-160x160.jpg');
                $message_meta->timeSend = date('H:i', strtotime($message_meta->created_at));
                $message_meta->dateSend = date('H:i d-m-Y', strtotime($message_meta->created_at));
            }
            $data['content'] = $list_message_metas;
        }
        return $data;
    }

    public function sendMessage($data)
    {
        try {
            $message = $this->message->where('user_id', Auth::id())->first();
            if ( !isset( $message ) ) {
                $data_message = [
                    "user_id" => Auth::id(),
                    "status" => 'pending'
                ];
                $message = $this->message->create($data_message);
            } else {
                $message->status = 'pending';
                $message->save();
            }

            $data_message_meta = [
                "message_id" => $message->id,
                "content" => $data['msgText'],
                "user_id" => Auth::id(),
                "status" => 'pending'
            ];

            $message_meta = $this->message_meta->create($data_message_meta);
            // event
            $data_event = [
              'user_id' => Auth::id(),
              'message_meta_id' => $message_meta->id
            ];
            event(new UserChatEvent( json_encode($data_event) ));

            return [
                "error" => 0
            ];
        } catch (Exception $e) {
            report($e);
            return [
                "error" => 1
            ];
        }
    }


}
