<?php

namespace App\Repositories\User;

use App\Model\User;
use App\Model\UserMeta;
use App\Model\Verify;
use App\Model\Credit;
use App\Model\Email;
use App\Model\HistoryPay;
use Illuminate\Support\Facades\Auth;
use App\Model\DetailOrder;
use App\Model\Vps;
use App\Model\Server;
use App\Model\Hosting;
use App\Model\TotalPrice;
use App\Model\Order;
use App\Model\Product;
use App\Model\LogActivity;
use App\Model\SendMail; 
use Mail;
use App\Factories\AdminFactories;
use App\Http\Controllers\Admin\DirectAdminController;


class PayInRepositories {

    protected $user;
    protected $user_meta;
    protected $credit;
    protected $email;
    protected $history_pay;
    protected $detail_order;
    protected $vps;
    protected $server;
    protected $hosting;
    protected $total_price;
    protected $order;
    protected $user_email;
    protected $subject;
    protected $product;
    protected $log_activity;
    protected $send_mail;
    // API
    protected $da;
    protected $dashboard;

    public function __construct()
    {
        $this->user = new User;
        $this->user_meta = new UserMeta;
        $this->credit = new Credit;
        $this->email = new Email;
        $this->history_pay = new HistoryPay;
        $this->detail_order = new DetailOrder;
        $this->vps = new Vps;
        $this->server = new Server;
        $this->hosting = new Hosting;
        $this->total_price = new TotalPrice;
        $this->order = new Order;
        $this->product = new Product;
        $this->log_activity = new LogActivity();
        $this->send_mail = new SendMail;
        $this->dashboard = AdminFactories::dashBoardRepositories();
        $this->da = AdminFactories::directAdminRepositories();
    }

    public function pay_in_office($data) {
        // $after = $this->history_pay->orderBy('id', 'desc')->first();
        $method_pay = $data['method_pay'];
        $ma_gd = $this->get_ma();
        // dd($ma_gd);
        $user = $this->user->find(Auth::user()->id);
        $data_history = [
            'user_id' => $user->id,
            'ma_gd' => $ma_gd,
            'discription' => !empty($data['discription']) ? $data['discription']:'',
            'type_gd' => '1',
            'method_gd' => $data['method_pay'],
            'date_gd' => date('Y-m-d'),
            'money'=> $data['money'],
            'status' => 'confirm',
        ];
        $create = $this->history_pay->create($data_history);
        // Tạo mã giao dịch
        // Tạo log
        if ($method_pay == 'pay_in_office') {
            $text_ma_gd = 'trực tiếp tại văn phòng';
        }
        elseif ($method_pay == 'bank_transfer_vietcombank') {
            $text_ma_gd = 'Vietcombank';
        }
        elseif ($method_pay == 'bank_transfer_techcombank') {
            $text_ma_gd = 'Techcombank';
        }
        else if ($method_pay == 'momo') {
            $text_ma_gd = 'MoMo';
        }
        $data_log = [
           'user_id' => Auth::user()->id,
           'action' => 'yêu cầu nạp tiền',
           'model' => 'User/Request_HistoryPay_Credit',
           'description' => 'vào tài khoản <a target="_blank" href="/admin/payment/detail/' . $create->id . '">#' . $create->ma_gd . '</a> bằng hình thức ' . $text_ma_gd,
           'description_user' => 'vào tài khoản bằng hình thức ' . $text_ma_gd,
        ];
        $this->log_activity->create($data_log);
        // gửi mail
        try {
            $user = Auth::user();
            $mail_request_payment = $this->email->where('type', 3)->where('type_service', 'request_payment')->first();
            if ( isset($mail_request_payment) ) {
                $search = [ '{$user_id}', '{$user_name}',  '{$user_email}', '{$user_credit}', '{$url}', '{$token}', '{$payment_code}', '{$payment_amount}', '{$payment_time}' ];
                $replace = [ Auth::id(), Auth::user()->name, $user->email, !empty($user->credit->value) ? number_format($user->credit->value,0,",",".") . ' VNĐ' : 0, url(''), '', $create->ma_gd, number_format($create->money,0,",",".") . ' VNĐ', date('d-m-Y') ];
                $content = str_replace($search, $replace, !empty($mail_request_payment->meta_email->content)? $mail_request_payment->meta_email->content : '');
                $data = [
                    'content' => $content
                ];
            } else {
                $data = [
                    'name' => Auth::user()->name,
                    'amount' => $create->money,
                    'ma_gd' => $create->ma_gd,
                    'created_at' => $create->created_at,
                    'credit' => (!empty($user->credit->value)) ? $user->credit->value : 0,
                    'subject' => 'Yêu cầu nạp tiền vào tài khoản tại CLOUDZONE'
                ];
            }
            $data_tb_send_mail = [
                'type' => 'request_payment',
                'type_service' => 'payment',
                'user_id' => Auth::id(),
                'content' => serialize($data),
                'status' => false,
            ];
            $this->send_mail->create($data_tb_send_mail);
        } catch (\Exception $e) {
            report($e);
            return $create;
        }
        return $create;
    }

    public function get_ma()
    {
        $after_ma_gd = '';
        $check = false;
        do {
            $random_ma = mt_rand(0,99999);
            $after_ma_gd = ($random_ma < 10000) ? $random_ma : $random_ma;
            if ( $random_ma > 10000 ) {
                $after_ma_gd = $random_ma;
            }
            elseif ( $random_ma > 1000 ) {
                $after_ma_gd = '0' . $random_ma;
            }
            elseif ( $random_ma > 100 ) {
                $after_ma_gd = '00' . $random_ma;
            }
            elseif ( $random_ma > 10 ) {
                $after_ma_gd = '000' . $random_ma;
            }
            else {
                $after_ma_gd = '0000' . $random_ma;
            }
            // dd('CLOUD' . $after_ma_gd);
            $check = $this->check_magd( 'CLOUD' . $after_ma_gd );
            // $check = $this->check_magd( 'CLOUD93065' );
            // dd($check);
        } while ( !$check );
        return 'CLOUD' . $after_ma_gd;
    }

    public function check_magd($magd)
    {
        $payment = $this->history_pay->where('ma_gd', $magd)->first();
        if ( isset($payment) ) {
            return false;
        }
        return true;
    }

    public function check_point()
    {
       $user_meta = $this->user_meta->where('user_id', Auth::user()->id)->first();
       return $user_meta->point;
    }

    public function update_history_pay($pay_in_msgd,$data)
    {
        $history_pay = $this->history_pay->where('ma_gd', $pay_in_msgd)->first();
        $history_pay->status = 'Active';
        $history_pay->content = serialize($data);
        $update = $history_pay->save();
        $credit = $this->credit->where('user_id', $history_pay->user_id)->first();
        if (isset($credit)) {
          $data_credit = [
              'value' => $credit->value + $history_pay->money,
              'total' => $credit->total + $history_pay->money,
          ];
          $credit->update($data_credit);
        } else {
          $data_credit = [
              'user_id' => $history_pay->user_id,
              'value' => $credit->value + $history_pay->money,
              'total' => $credit->total + $history_pay->money,
          ];
          $this->credit->create($data_credit);
        }
        if (!empty($history_pay->detail_order_id)) {
            $invoice = $this->detail_order->find($history_pay->detail_order_id);
            $total = $invoice->sub_total * $invoice->quantity;
            $order = $this->order->find($invoice->order->id);
            $order->status = 'Active';
            $order->save();
            // Gia hạn
            if ($order->type = 'expired') {
                $billing = [
                    'monthly' => '1 Month',
                    '2monthly' => '2 Month',
                    'quarterly' => '3 Month',
                    'semi_annually' => '6 Month',
                    'annually' => '1 Year',
                    'biennially' => '2 Year',
                    'triennially' => '3 Year'
                ];
                if ($invoice->type == 'Hosting') {
                    $hostings = $this->hosting->where('detail_order_id', $invoice->id)->get();
                    $billing_cycle = $hostings[0]->billing_cycle;
                    $date_star = $invoice->created_at;
                    $date_paid = date('Y-m-d');
                    $due_date = date('Y-m-d', strtotime( $date_star . $billing[$hostings[0]->billing_cycle] ));
                    // $reques_da = $this->vcenter->create_hosting($hostings); //request đên Direct Admin

                    foreach ($hostings as $key => $hosting) {
                        $hosting->next_due_date = $due_date;
                        $hosting->paid = 'paid';
                        $hosting->save();
                    }
                    $invoice->due_date = $due_date;
                    $invoice->paid_date = $date_paid;
                    $invoice->status = 'paid';
                    $invoice->payment_method = 2;
                    $paid = $invoice->save();

                    $order->status = 'Finish';
                    $order->save();
                    $this->send_mail_finish_expired('hosting',$order, $invoice ,$hostings);
                    // ghi vào tổng thu
                    if (!empty( $this->total_price->find(1) )) {
                        $total_price =  $this->total_price->find(1);
                        $total_price->hosting += $total;
                        $total_price->save();
                    } else {
                        $data_total_price = [
                            'vps' => '0',
                            'hosting' => $total,
                            'server' => '0',
                        ];
                        $this->total_price->create($data_total_price);
                    }
                }
            }
            // invoice
            $billing = [
                'monthly' => '1 Month',
                '2monthly' => '2 Month',
                'quarterly' => '3 Month',
                'semi_annually' => '6 Month',
                'annually' => '1 Year',
                'biennially' => '2 Year',
                'triennially' => '3 Year'
            ];
            if ($invoice->type == 'VPS') {
                $list_vps = $this->vps->where('detail_order_id', $invoice->id)->get();
                $billing_cycle = $list_vps[0]->billing_cycle;
                $date_star = $invoice->created_at;
                $date_paid = date('Y-m-d');
                $due_date = date('Y-m-d', strtotime( $date_star . $billing[$list_vps[0]->billing_cycle] ));

                $invoice->due_date = $due_date;
                $invoice->paid_date = $date_paid;
                $invoice->status = 'paid';
                $invoice->payment_method = 2;
                $paid = $invoice->save();
                $reques_vcenter = $this->dashboard->createVPS($list_vps, $due_date); //request đên Vcenter
                // dd($due_date);
                $reques_vcenter = true;
                if ($reques_vcenter) {
                    $order->status = 'Finish';
                    $order->save();
                    $this->send_mail_finish('vps',$order, $invoice ,$list_vps);
                }
                // $total_price = $this->total_price->get();
                // ghi vào tổng thu
                if (!empty( $this->total_price->find(1) )) {
                    $total_price = $this->total_price->find(1);
                    $total_price->vps += $total;
                    // dd($total_price);
                    $total_price->save();
                } else {
                    $data_total_price = [
                        'vps' => $total,
                        'hosting' => '0',
                        'server' => '0',
                    ];
                    $this->total_price->create($data_total_price);
                }
            } elseif ($invoice->type == 'Server') {
                $servers = $this->server->where('detail_order_id', $invoice->id)->get();
                $billing_cycle = $servers[0]->billing_cycle;
                $date_star = $invoice->created_at;
                $date_paid = date('Y-m-d');
                $due_date = date('Y-m-d', strtotime( $date_star . $billing[$servers[0]->billing_cycle] ));

                foreach ($servers as $key => $server) {
                    $server->next_due_date = $due_date;
                    $server->paid = 'paid';
                    $server->save();
                }
                $invoice->due_date = $due_date;
                $invoice->paid_date = $date_paid;
                $invoice->status = 'paid';
                $invoice->payment_method = 2;
                $paid = $invoice->save();
                // ghi vào tổng thu
                if (!empty( $this->total_price->find(1) )) {
                    $total_price =  $this->total_price->find(1);
                    $total_price->server += $total;
                    $total_price->save();
                } else {
                    $data_total_price = [
                        'vps' => '0',
                        'hosting' => '0',
                        'server' => $total,
                    ];
                    $this->total_price->create($data_total_price);
                }
            } else {
                $hostings = $this->hosting->where('detail_order_id', $invoice->id)->get();
                $billing_cycle = $hostings[0]->billing_cycle;
                $date_star = $invoice->created_at;
                $date_paid = date('Y-m-d');
                $due_date = date('Y-m-d', strtotime( $date_star . $billing[$hostings[0]->billing_cycle] ));
                // $reques_da = $this->vcenter->create_hosting($hostings); //request đên Direct Admin

                foreach ($hostings as $key => $hosting) {
                    $hosting->next_due_date = $due_date;
                    $hosting->paid = 'paid';
                    $hosting->save();
                }
                $invoice->due_date = $due_date;
                $invoice->paid_date = $date_paid;
                $invoice->status = 'paid';
                $invoice->payment_method = 2;
                $paid = $invoice->save();
                $request_da = $this->da->orderCreateAccount($hostings); //request đên Direct Admin
                if ($request_da) {
                    $order->status = 'Finish';
                    $order->save();
                    $this->send_mail_finish('hosting',$order, $invoice ,$hostings);
                }
                // ghi vào tổng thu
                if (!empty( $this->total_price->find(1) )) {
                    $total_price =  $this->total_price->find(1);
                    $total_price->hosting += $total;
                    $total_price->save();
                } else {
                    $data_total_price = [
                        'vps' => '0',
                        'hosting' => $total,
                        'server' => '0',
                    ];
                    $this->total_price->create($data_total_price);
                }
            }
        }
        return $update;
    }

    public function update_false_history_pay($pay_in_msgd,$data) {
        $history_pay = $this->history_pay->where('ma_gd', $pay_in_msgd)->first();
        $history_pay->status = 'Error';
        $history_pay->content = serialize($data);
        $update = $history_pay->save();
        return $update;
    }

    public function history_payment() {
        $payments = $this->history_pay->where('user_id', Auth::user()->id)
                ->where('status', 'Active')->orderByRaw('updated_at desc')
                ->with('user', 'detail_order')->paginate(20);
        return $payments;
    }

    public function check_invoice($invoice_id)
    {
        $history_pay = $this->history_pay->where('type_gd', 2)->where('detail_order_id', $invoice_id)->first();
        if (isset($history_pay)) {
            return $history_pay;
        } else {
            return null;
        }
    }

    public function check_invoice_upgrade($invoice_id)
    {
        $history_pay = $this->history_pay->where('type_gd', 8)->where('detail_order_id', $invoice_id)->first();
        if (isset($history_pay)) {
            return $history_pay;
        } else {
            return null;
        }
    }

    public function check_invoice_expired($invoice_id)
    {
        $history_pay = $this->history_pay->where('type_gd', 3)->where('detail_order_id', $invoice_id)->first();
        if (isset($history_pay)) {
            return $history_pay;
        } else {
            return null;
        }
    }

    public function check_invoice_addon($invoice_id)
    {
        $history_pay = $this->history_pay->where('type_gd', 4)->where('detail_order_id', $invoice_id)->first();
        if (isset($history_pay)) {
            return $history_pay;
        } else {
            return null;
        }
    }

    public function check_invoice_change_ip($invoice_id)
    {
        $history_pay = $this->history_pay->where('type_gd', 5)->where('detail_order_id', $invoice_id)->first();
        if (isset($history_pay)) {
            return $history_pay;
        } else {
            return null;
        }
    }
    public function check_invoice_domain($invoice_id)
    {
        $history_pay = $this->history_pay->where('type_gd', 6)->where('detail_order_id', $invoice_id)->first();
        if (isset($history_pay)) {
            return $history_pay;
        } else {
            return null;
        }
    }
    public function check_invoice_domain_expired($invoice_id)
    {
        $history_pay = $this->history_pay->where('type_gd', 7)->where('detail_order_id', $invoice_id)->first();
        if (isset($history_pay)) {
            return $history_pay;
        } else {
            return null;
        }
    }

    public function check_credit($invoice_id)
    {
        $invoice = $this->detail_order->find($invoice_id);
        $credit = $this->credit->where('user_id', $invoice->user_id)->first();
        return $credit;
    }

    public function detail_pay_in_with_id($id)
    {
      $history_pay = $this->history_pay->find($id);
      return $history_pay;
    }

    public function detail_invoice($invoice_id)
    {
        return $this->detail_order->find($invoice_id);
    }

    public function detail_pay_in($ma_gd)
    {
        $history_pay = $this->history_pay->where('ma_gd', $ma_gd)->first();
        return $history_pay;
    }

    public function send_mail_finish($type, $order , $detail_order, $list_sevices)
    {
        $billings = config('billing');
        $product = $this->product->find($list_sevices[0]->product_id);
        $email = $this->email->find($product->meta_product->email_id);
        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $add_on =  '';
        if (!empty($detail_order->addon_id)) {
            $add_on = $this->product->find($list_sevices[0]->addon_id);
        }
        if ($type == 'vps') {
            foreach ($list_sevices as $key => $services) {
                $user = $this->user->find($services->user_id);
                $search = ['{$addon_name}','{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [!empty($add_on)?$add_on->name : ''  ,$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], '', $order->created_at, $services->user, $services->password, number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' , $detail_order->quantity, $services->os, '', $url];
                $content = str_replace($search, $replace, $email->meta_email->content);

                $this->user_email = $this->user->find($services->user_id)->email;
                $this->subject = $email->meta_email->subject;

                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'user_id' => $user->id,
                    'user_email' => $user->email,
                    'user_addpress' => $user->user_meta->address,
                    'user_phone' => $user->user_meta->user_phone,
                    'order_id' => $order->id,
                    'product_name' => $product->name,
                    'domain' => '',
                    'ip' => $services->ip,
                    'billing_cycle' => $services->billing_cycle,
                    'next_due_date' => $services->next_due_date,
                    'order_created_at' => $order->created_at,
                    'services_username' => $services->user,
                    'services_password' => $services->password,
                    'sub_total' => $detail_order->sub_total,
                    'total' => $order->total,
                    'qtt' => $detail_order->quantity,
                    'os' => $services->os,
                    'token' => '',
                ];

                $mail = Mail::send('users.mails.orders', $data, function($message){
                    $message->from('support@goldlinkup.net', 'Portal Cloudzone');
                    $message->to($this->user_email)->subject($this->subject);
                });
            }
        } elseif ($type == 'hosting') {
            foreach ($list_sevices as $key => $services) {
                $user = $this->user->find($services->user_id);
                $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [ $user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], '', $order->created_at, $services->user, $services->password, number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url];
                $content = str_replace($search, $replace, $email->meta_email->content);

                $this->user_email = $this->user->find($services->user_id)->email;
                $this->subject = $email->meta_email->subject;

                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'user_id' => $user->id,
                    'user_email' => $user->email,
                    'user_addpress' => $user->user_meta->address,
                    'user_phone' => $user->user_meta->user_phone,
                    'order_id' => $order->id,
                    'product_name' => $product->name,
                    'domain' => '',
                    'ip' => $services->ip,
                    'billing_cycle' => $services->billing_cycle,
                    'next_due_date' => $services->next_due_date,
                    'order_created_at' => $order->created_at,
                    'services_username' => $services->user,
                    'services_password' => $services->password,
                    'sub_total' => $detail_order->sub_total,
                    'total' => $order->total,
                    'qtt' => $detail_order->quantity,
                    'os' => $services->os,
                    'token' => '',
                ];

                $mail = Mail::send('users.mails.orders', $data, function($message){
                    $message->from('support@goldlinkup.net', 'Portal Cloudzone');
                    $message->to($this->user_email)->subject($this->subject);
                });
            }
        }
    }

    public function send_mail_finish_expired($type, $order , $detail_order, $list_sevices)
    {
        $billings = config('billing');
        $product = $this->product->find($list_sevices[0]->product_id);
        $email = $this->email->find($product->meta_product->email_expired_finish);
        $url = url('');
        $url = str_replace(['http://','https://'], ['', ''], $url);
        $add_on =  '';

        if ($type == 'vps') {
            foreach ($list_sevices as $key => $services) {
                $user = $this->user->find($services->user_id);
                $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , '', $services->ip ,$billings[$services->billing_cycle], '', $order->created_at, $services->user, $services->password, number_format($detail_order->sub_total,0,",",".") . ' VNĐ' ,number_format($order->total,0,",",".") . ' VNĐ' , $detail_order->quantity, $services->os, '', $url];
                $content = str_replace($search, $replace, $email->meta_email->content);

                $this->user_email = $this->user->find($services->user_id)->email;
                $this->subject = $email->meta_email->subject;

                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'user_id' => $user->id,
                    'user_email' => $user->email,
                    'user_addpress' => $user->user_meta->address,
                    'user_phone' => $user->user_meta->user_phone,
                    'order_id' => $order->id,
                    'product_name' => $product->name,
                    'domain' => '',
                    'ip' => $services->ip,
                    'billing_cycle' => $services->billing_cycle,
                    'next_due_date' => $services->next_due_date,
                    'order_created_at' => $order->created_at,
                    'services_username' => $services->user,
                    'services_password' => $services->password,
                    'sub_total' => $detail_order->sub_total,
                    'total' => $order->total,
                    'qtt' => $detail_order->quantity,
                    'os' => $services->os,
                    'token' => '',
                ];

                $mail = Mail::send('users.mails.orders', $data, function($message){
                    $message->from('support@goldlinkup.net', 'Portal Cloudzone');
                    $message->to($this->user_email)->subject($this->subject);
                });
            }
        } elseif ($type == 'hosting') {
            foreach ($list_sevices as $key => $services) {
                $user = $this->user->find($services->user_id);
                $search = ['{$user_id}' , '{$user_name}',  '{$user_email}' , '{$user_addpress}' , '{$user_phone}', '{$order_id}',  '{$product_name}', '{$domain}', '{$ip}' ,'{$billing_cycle}', '{$next_due_date}', '{$order_created_at}', '{$services_username}', '{$services_password}','{$sub_total}' , '{$total}', '{$qtt}', '{$os}', '{$token}', '{$url}'];
                $replace = [$user->id , $user->name,  $user->email , $user->user_meta->address , $user->user_meta->user_phone, $order->id,  $product->name , $services->domain, '' ,$billings[$services->billing_cycle], '', $order->created_at, $services->user, $services->password, number_format($detail_order->sub_total,0,",",".") . ' VNĐ' , number_format($order->total,0,",",".") . ' VNĐ', $detail_order->quantity, '', '', $url];
                $content = str_replace($search, $replace, $email->meta_email->content);

                $this->user_email = $this->user->find($services->user_id)->email;
                $this->subject = $email->meta_email->subject;

                $data = [
                    'content' => $content,
                    'user_name' => $user->name,
                    'user_id' => $user->id,
                    'user_email' => $user->email,
                    'user_addpress' => $user->user_meta->address,
                    'user_phone' => $user->user_meta->user_phone,
                    'order_id' => $order->id,
                    'product_name' => $product->name,
                    'domain' => '',
                    'ip' => $services->ip,
                    'billing_cycle' => $services->billing_cycle,
                    'next_due_date' => $services->next_due_date,
                    'order_created_at' => $order->created_at,
                    'services_username' => $services->user,
                    'services_password' => $services->password,
                    'sub_total' => $detail_order->sub_total,
                    'total' => $order->total,
                    'qtt' => $detail_order->quantity,
                    'os' => $services->os,
                    'token' => '',
                ];

                $mail = Mail::send('users.mails.orders', $data, function($message){
                    $message->from('support@goldlinkup.net', 'Portal Cloudzone');
                    $message->to($this->user_email)->subject($this->subject);
                });
            }
        }
    }

    public function check_han_muc($amount)
    {
        $user  = $this->user->find(Auth::user()->id)->credit;
        if ($amount + $user->value > 100000000) {
            return $user;
        }
        return false;
    }


}
